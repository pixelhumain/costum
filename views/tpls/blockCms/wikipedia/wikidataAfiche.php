<?php
// 1643
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $assetsUrl = Yii::app()->getModule("co2")->getAssetsUrl();
?>
<link title="timeline-styles" rel="stylesheet" href="https://cdn.knightlab.com/libs/timeline3/latest/css/timeline.css">
<style id="<?= $kunik ?>wikidataAfiche">
	@keyframes fadein {
		0% {
			opacity: 0;
		}

		100% {
			opacity: 1;
		}
	}

    #ajax-modal {
        top: 150px !important;
    }

    .wikidataAfiche<?= $kunik ?> .imgMap {
        animation: fadein 3000ms 1;
    }

    .wikidataAfiche<?= $kunik ?> ul {
        list-style-type: none;
        padding: 0;
        margin: 0;
    }

    .wikidataAfiche<?= $kunik ?> li {
        background-color: white;
        padding: 15px;
        margin: 10px;
        border-radius: 5px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        overflow: hidden;
    }

    .wikidataAfiche<?= $kunik ?> .data-content {
        height:370px;
        background-color: white;
        cursor: pointer;
        padding: 15px;
        margin-top: 10px;
        border-radius: 5px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        overflow: hidden;
        display: grid;
    }

    .wikidataAfiche<?= $kunik ?> .data-content img {
        width: 70%;
        max-height: 220px;
    }

    .wikidataAfiche<?= $kunik ?> h2 {
        color: #333;
        text-transform: Capitalize;
    }
    .wikidataAfiche<?= $kunik ?> h4 {
        text-transform: Capitalize;
    }

    .wikidataAfiche<?= $kunik ?> p {
        color: #666;
        margin: 0; 
    }

    .wikidataAfiche<?= $kunik ?> ul img {
        max-width: 80%;
        height: auto;
        margin-top: 10px; 
    }

    .wikidataAfiche<?= $kunik ?> .map-container-xs {
        height: 200px; 
        margin-top: 10px;
    }

    .wikidataAfiche<?= $kunik ?> .showMore{
		padding: 5px 8px 8px 5px;
		font-weight: bolder !important;
		border-radius: 40px !important;
		margin-bottom: 20px;
        font-size: 10pt;
        color : white !important;
        border-color: white !important;
        background-color: "#092434";
	}

    .wikidataAfiche<?= $kunik ?> .text-resultat, .text-resultat-personne {
        font-size: 16pt; 
        font-weight: bold; 
        color: black; 
        text-transform:uppercase !important; 
        margin-left: 2%;
    }

    .wikidataAfiche<?= $kunik ?> #personne-content {
        margin-top:120px;
    }

    @media (max-width: 767px) {
        .wikidataAfiche<?= $kunik ?> .data-content img {
            width: 50%;
        }

        .wikidataAfiche<?= $kunik ?> .wikidata-menu-xs.menu-left{
            display: block;
            position: fixed;
            z-index: 1000000000;
            background: #092434;
            color: white;
            border-radius: 0px 10px 10px 0px;
            transform: translate(50%, 100%);
            padding: 15px 5px 15px 5px;
            writing-mode: vertical-lr;
            margin-top: -15%;
            font-weight: 900;
        }

        .wikidataAfiche<?= $kunik ?> .div-xs-menu-list {
            position: fixed;
            z-index: 100000000;
            left: -767px;
            background: white;
            height: 95%;
            width: 100%;
        }

        .wikidataAfiche<?= $kunik ?> .div-xs-menu-list.active{
            left: 0px;
        }

        .wikidataAfiche<?= $kunik ?> .div-xs-menu-list a {
            margin-right: 25px;
        }

        .wikidataAfiche<?= $kunik ?> .div-xs-menu-list ul li, li a i{
            background-color: #092434;
            color: white;
            font-weight: 900;
        }

        .wikidataAfiche<?= $kunik ?> .div-xs-menu-list ul {
            margin-left: 50px;
        }

        .wikidataAfiche<?= $kunik ?> #event-content-timeline {
            width: 100%;
        }

        .wikidataAfiche<?= $kunik ?> #all_wikidata_item {
            margin-top: -8%;
            margin-left: 4%;
            margin-right: 4%;
        }

        .wikidataAfiche<?= $kunik ?> .div_search_orga, .div_search_element, .div_search_personne, .div_search_event {
            text-align: center !important;
        }

        .wikidataAfiche<?= $kunik ?> .inputCherche {
            margin-top: 10px !important;
            margin-bottom: 10px;
            border-radius: 20px 0px 0px 20px
        }

        .wikidataAfiche<?= $kunik ?> #wikidata-content {
            margin-top: 30px;
        }

        .wikidataAfiche<?= $kunik ?> .div_search_xs {
            margin-top: -4%;
            box-shadow: none !important;
        }

        .wikidataAfiche<?= $kunik ?> .text-resultat {
            font-size: 12pt;
            text-align: left;
            margin-left: 10%;
        }

        .wikidataAfiche<?= $kunik ?> .text-resultat-personne {
            font-size: 12pt;
            text-align: left;
            margin-left: 20% !important;
        }

        .wikidataAfiche<?= $kunik ?> .result_elements {
            margin-top: 0px !important;
        }

        .wikidataAfiche<?= $kunik ?> .dropdown {
            margin-top: 0px !important;
        }

        .wikidataAfiche<?= $kunik ?> .dropbtn {
            width: 85% !important;
            height: 40px;
            margin-left: 5%;
        }

        .wikidataAfiche<?= $kunik ?> .result_personne, .result_elements, .result_event {
            margin-top: 0px !important;
        }

        .wikidataAfiche<?= $kunik ?> .affichagePersonne, .affichageEvent {
            margin-top: 3px !important;
            margin-left: 60%;
            height: 27px;
            font-size: 10px;
        }

        .wikidataAfiche<?= $kunik ?> .one_event {
            box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px; 
            margin: 8px; 
            height: auto !important;
            width: 100% !important;
            cursor: pointer;
        }

        .wikidataAfiche<?= $kunik ?> .one_event h5 {
            text-transform: none;
        }

        .wikidataAfiche<?= $kunik ?> .one_event p {
            font-size: 15px;
            margin-bottom: 3px; 
        }

        .wikidataAfiche<?= $kunik ?> #personne-content {
            margin-top:150px !important;
        }

        /* .affichageEvent {
            margin-top: 3px !important;
            margin-left: 20%;
        } */
    }

    .wikidataAfiche<?= $kunik ?> #data-xs-list {
        color: white;
    }

    .wikidataAfiche<?= $kunik ?> .affichage_noedit {
        margin-top: 80px;
        cursor: pointer;
    }

    .wikidataAfiche<?= $kunik ?> .affichage_edit {
        margin-top: 18px;
        margin-left: -40px;
        cursor: pointer;
    }

    .wikidataAfiche<?= $kunik ?> #orga-content {
        margin-top: 120px;
    }

    .wikidataAfiche<?= $kunik ?> .div_all_menu.active-btn, .wikidataAfiche<?= $kunik ?> .div_all_menu.active-btn .btn-discover   {
        background-color: white !important;
        color: #092434 !important;
    }  

    .wikidataAfiche<?= $kunik ?> .btn-discover {
        color: white;
    }

    .wikidataAfiche<?= $kunik ?> .div_all_menu {
        font-size:17px; 
        width: 150px; 
        font-weight: 300; 
        height: 57px;
        cursor: pointer;
    }

    .wikidataAfiche<?= $kunik ?> .div_one_menu {
        margin-top: 13px;
    }

    .wikidataAfiche<?= $kunik ?> #descript-table {
        font-size: 13pt;
        width: 95%;
    } 

    .wikidataAfiche<?= $kunik ?> #descript-table tr {
        border-bottom: 1px solid black;
        height: 40px;
    } 

    .wikidataAfiche<?= $kunik ?> #descript-table td {
        width: 190px;
    }

    .wikidataAfiche<?= $kunik ?> .event-detail li a {
        color: #01305E !important;
        font-weight: bold;
        font-size: 18px
    }
    .wikidataAfiche<?= $kunik ?> .program-organizer-image {
        display: inline-block;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        box-shadow: 0 0 10px rgba(0, 0, 0, .2);
        overflow: hidden;
    }

    .wikidataAfiche<?= $kunik ?> .program-organizer-image img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .wikidataAfiche<?= $kunik ?> .event-detail li {
        position: relative;
    }

    .wikidataAfiche<?= $kunik ?> .line-indicator {
        position: absolute;
        top: 15px;
        left: -65px;
        width: 60px;
        height: 5px;
        display: block;
        border-radius: 50px;
    }

    .wikidataAfiche<?= $kunik ?> .point-holder {
        display: block;
        height: 15px;
        width: 15px;
        border-radius: 50%;
        position: absolute;
        top: -5px;
        left: -5px;
    }

    .wikidataAfiche<?= $kunik ?> .rolesLabel {
        color: #01305E;
        font-size: 17px;
        font-weight: 500;
    }

    .wikidataAfiche<?= $kunik ?> .actorNames {
        color: #01305E;
        font-size: 16px;
        font-style: italic;
    }

    .wikidataAfiche<?= $kunik ?> .btn-participate {
        color: white;
        border: none;
        border-radius: 50px;
        padding: 0.5rem 2rem;
        margin-left: 30px;
        font-size: 18px;
    }

    .wikidataAfiche<?= $kunik ?> .btn-participate:hover {
        color:black;
        background-color: white;
    }

    
    .wikidataAfiche<?= $kunik ?>.descr-event {
        color: black;
        font-size: 16px;
    }

    .wikidataAfiche<?= $kunik ?> .div_search_orga, .div_search_element, .div_search_personne, .div_search_event {
        text-align: left;
        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
        width: 100%;
        position: fixed;
        z-index: 1111;
        background-color: white;
        margin-left: 14px;
        height: auto;
    }

    .wikidataAfiche<?= $kunik ?> .input_edit {
        margin-top : 11px; 
    }

    .wikidataAfiche<?= $kunik ?> .input_noedit {
        margin-top : 70px; 
    }

    .wikidataAfiche<?= $kunik ?> .inputCherche {
        margin-left : 20px; 
        height : 38px; 
        width : 78%;
        text-align: center;
        border-radius: 20px 0px 0px 20px !important;
        margin-bottom: 10px;
    }

    .wikidataAfiche<?= $kunik ?> .result_noedit {
        margin-top:68px;
    }

    .wikidataAfiche<?= $kunik ?> .result_edit {
        margin-top:10px;
    }

    .wikidataAfiche<?= $kunik ?> .input-group-addon {
        border: 1px solid black;
        border-radius: 0px 20px 20px 0px !important;
        background: #092434;
        color: white;
        height: 38px;
        display: inline-grid;
        margin-top: -4px;
        margin-left: -3px;
    }

    .wikidataAfiche<?= $kunik ?> .input-group-addon i {
        margin-top: 5px;
        margin-left: -5px;
    }
    
    .wikidataAfiche<?= $kunik ?> .tl-slide-background {
        background-size: 30% !important;    
    }

    .wikidataAfiche<?= $kunik ?> .tl-text .tl-headline-date, .tl-text h3.tl-headline-date  {
        font-size: 20px !important; 
    }

    .wikidataAfiche<?= $kunik ?> .tl-timeline p {
        font-size: 18px !important;
        text-align: justify;
    }

    .wikidataAfiche<?= $kunik ?> .tl-timemarker-text .tl-headline {
        color: black !important;
    }

    .wikidataAfiche<?= $kunik ?> .menu-noedit {
        position: fixed; 
    }

    .wikidataAfiche<?= $kunik ?> #main-map {
        height: 700px; 
        width: 800px; 
        margin-right: 10px; 
    }

    .wikidataAfiche<?= $kunik ?> .main-map_noedit {
        margin-top: 125px; 
        position: fixed !important;
    }

    .wikidataAfiche<?= $kunik ?> .wikidata-content_edit {
        margin-top: 10px;
    }

    .wikidataAfiche<?= $kunik ?> .wikidata-content_noedit {
        margin-top: 130px;
    }

    .wikidataAfiche<?= $kunik ?> .wikidata-menu {
        box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
        background-color: #092434; 
        color: white; 
        z-index: 11111; 
        height: 57px;
    }

    .wikidataAfiche<?= $kunik ?> .one_event {
        box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px; 
        margin: 8px; 
        height: 130px;
        width: 32%;
        cursor: pointer;
    }

    .wikidataAfiche<?= $kunik ?> .one_event h5 {
        text-transform: none;
    }

    /* dropdown */
    .wikidataAfiche<?= $kunik ?> .dropdown {
        position: relative;
        display: inline-block;
        width:100%;
    }

    .wikidataAfiche<?= $kunik ?> .dropbtn {
        background-color: transparent;
        color: grey;
        padding: 10px;
        font-size: 15px;
        cursor: pointer;
        width: 98%;
        border-radius: 20px;
    }

    .wikidataAfiche<?= $kunik ?> .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .wikidataAfiche<?= $kunik ?> .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .wikidataAfiche<?= $kunik ?> .dropdown-content a:hover {
        background-color: #ddd;
    }

    .wikidataAfiche<?= $kunik ?> .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown:hover .dropbtn {
        background-color: transparent;
    }
</style>
<div class="<?= $kunik ?> wikidataAfiche<?= $kunik ?>" id="wikidataAfiche<?= $kunik ?>">
    <div class="col-md-12 col-xs-12 no-padding wikidata-menu hidden-xs">
        <div class="col-md-1" style="width: 20%"></div>
        <div class="col-md-1 col-sm-2 center no-padding div_all_menu btn-discover-abstract" onclick="afficheDescriptions()">
            <div class="div_one_menu">
                <a class="btn btn-discover ">
                    <i class="fa fa-calendar"></i>
                </a>
                <span class="discover-subtitle"><?php echo Yii::t('cms', "About") ?>
                </span>
            </div>
        </div>

        <div class="col-md-1 col-sm-2 center no-padding div_all_menu btn-discover-orga"  onclick="afficheOrga()">
            <div class="div_one_menu">
                <a class="btn btn-discover ">
                    <i class="fa fa-group"></i>
                </a>
                <span class="discover-subtitle"><?php echo Yii::t('cms', "Organization") ?>
                </span>
            </div>

        </div>

        <div class="col-md-1 col-sm-2 center no-padding div_all_menu btn-discover-location"  onclick="afficheElements()">
            <div class="div_one_menu">
                <a class="btn btn-discover ">
                    <i class="fa fa-lightbulb-o"></i>
                </a>
                <span class="discover-subtitle"><?php echo Yii::t('cms', "Location") ?>
                </span>
            </div>
        </div>

        <div class="col-md-1 col-sm-2 center no-padding div_all_menu btn-discover-human"  onclick="affichePersonnes()">
            <div class="div_one_menu">
                <a class="btn btn-discover ">
                    <i class="fa fa-user"></i>
                </a>
                <span class="discover-subtitle"><?php echo Yii::t('cms', "Person") ?>
                </span>
            </div>
        </div>

        <div class="col-md-1 col-sm-2 center no-padding div_all_menu btn-discover-event"  onclick="afficheEvenement()">
            <div class="div_one_menu">
                <a class="btn btn-discover">
                    <i class="fa fa-calendar"></i>
                </a>
                <span class="discover-subtitle"><?php echo Yii::t('cms', "Events") ?>
                </span>
            </div>
        </div>
        <div class="col-md-1" style="width: 20%"></div>
    </div>
    <div class="wikidata-menu-xs visible-xs menu-left" style="cursor: pointer">
        <i class="fa fa-list-alt" ></i> Menu
    </div>
    <div class="div-xs-menu-list visible-xs">
        <ul>
            <li onclick="afficheDescriptions()" style="cursor: pointer"><a><i class="fa fa-calendar"></i></a><?php echo Yii::t('cms', "About") ?></li>
            <li onclick="afficheOrga()" style="cursor: pointer"><a><i class="fa fa-group"></i></a><?php echo Yii::t('cms', "Organization") ?></li>
            <li onclick="afficheElements()" style="cursor: pointer"><a><i class="fa fa-lightbulb-o"></i></a><?php echo Yii::t('cms', "Location") ?></li>
            <li onclick="affichePersonnes()" style="cursor: pointer"><a><i class="fa fa-user"></i></a><?php echo Yii::t('cms', "Person") ?></li>
            <li onclick="afficheEvenement()" style="cursor: pointer"><a><i class="fa fa-calendar"></i></a><?php echo Yii::t('cms', "Events") ?></li>
        </ul>
    </div>
    <div class="visible-xs text-center"><p id="data-xs-list" style="font-size: 30px;"></p></div>
    <div id="all_wikidata_item">
    </div>
</div>
<script src="https://cdn.knightlab.com/libs/timeline3/latest/js/timeline.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
<script>

    var addressOrga = [];
    var timeoutId;
    var mainMap = "";
    var wikidataId = "";
    var interval = 60;
    var startIndex = 0;
    const apiUrl_wikipedeia = `${baseUrl}/api/convert/wikipedia`;
    var datanull = `<br><br><br><br><br><h2 style="text-transform: none;" class="text-center"><?php echo Yii::t('cms', "There is no information about the city address of your element on Wikipedia") ?></h2>`;
    var htmlSpiner = '<h3 class="homestead text-dark padding-10" style="text-align:center"><?php echo Yii::t('cms', "Loading data") ?>... <br><i class="fa fa-spin fa-circle-o-notch"></i></h3>';
    var searcheNull = `<br><br><br><br><h3 style="text-transform:none; text-align:center">Aucun résultat pour le critère que vous cherchez.</h3>`;
    var dataPreviewElements = "";
    var filtersElements = ["Hopital","Université", "Ecole", "Institu", "Hôtel", "Musée", "stade","Pont", "Rue"];
    var selectedFiltersElements = [];
    var linksElements = [];
    var wikidatacontent= "";
    var fullText;
    var pageSize = 2200;
    var currentPage = 0;
    var locationCity = "";

    function removeActiveClassFromButtons() {   
        const buttons = document.querySelectorAll('.div_all_menu');
        buttons.forEach(button => button.classList.remove('active-btn'));
    }

    function getWikidataID(pageTitle) {
        const apiUrl = `${baseUrl}/api/convert/idwikidata`;
        locationCity = pageTitle.charAt(0).toUpperCase() + pageTitle.slice(1).toLowerCase();
        const params = {
            title: locationCity
        };
        const url = `${apiUrl}?${new URLSearchParams(params)}`;
        return fetch(url)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`Erreur HTTP : ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                wikidataId = data.wikidataId;
                afficheDescriptions();
                // const pages = data.query.pages;
                // for (const pageId in pages) {
                //     if (pages.hasOwnProperty(pageId)) {
                //         wikidataId = pages[pageId].pageprops.wikibase_item;
                //         afficheDescriptions();
                //     }
                // }
            })
            .catch(error => {
                $("#all_wikidata_item").html(datanull);
            });
    }

    function changePage(direction) {
        currentPage += direction;
        const start = currentPage * pageSize;
        const end = start + pageSize;
        const pageText = fullText.substring(start, end);

        document.getElementById("descript_text").textContent = pageText;

        document.getElementById("prevButton").style.display = currentPage === 0 ? "none" : "inline";
        document.getElementById("nextButton").style.display = end >= fullText.length ? "none" : "inline";

        const totalPages = Math.ceil(fullText.length / pageSize);
        const currentPageNumber = currentPage + 1;

        document.getElementById("pageNumber").textContent = `Page ${currentPageNumber} / ${totalPages}`;
    }

    function listDescription(data) {
        fullText = typeof data.descriptions != "undefined" ? data.descriptions : (data.apiData['description'] != null ? data.apiData['description'] : "Aucune résumer pour "+costum.addresseLocality);
        var lat = typeof data.latitude != "undefined" ? data.latitude : (data.apiData['latitude'] != null ? data.apiData['latitude'] : "");
        var long = typeof data.longitude != "undefined" ? data.longitude : (data.apiData['longitude'] != null ? data.apiData['longitude'] : "");
        var src = `
            <div class="row text-right" style="margin-top: 1%">
                <button class="btn btn-primary" style="margin-right: 1%; cursor:pointer;"  id="prevButton" onclick="changePage(-1)" title="Page précédente"><i class="fa fa-angle-left"></i></button>
                <button class="btn btn-primary" style="margin-right: 1%; cursor:pointer;" id="nextButton" onclick="changePage(1)" title="Page suivante"><i class="fa fa-angle-right"></i></button>
                <a id="pageNumber" style="margin-right: 1%;"></a>
            </div>
            <p style="text-align:justify" id="descript_text">${fullText}</p> <br>
            <div class="text-center">
                <a style='margin:10px;' target='_blank' class='btn btn-default' href='https://dbpedia.org/page/${locationCity}'><img style='max-height:50px;' src='${baseUrl}/<?= $assetsUrl ?>/images/logos/logo-dbpedia.png'/></a>
                <a style='margin:10px;' target='_blank' class='btn btn-default' href='https://fr.wikipedia.org/wiki/${locationCity}'><img style='max-height:50px;'' src='${baseUrl}/<?= $assetsUrl ?>/images/logos/Wikipedia-logo-en-big.png'/></a>
                <a style='margin:10px;' target='_blank' class='btn btn-default' href='https://www.wikidata.org/wiki/${wikidataId}'><img style='max-height:50px;' src='${baseUrl}/<?= $assetsUrl ?>/images/logos/logo-wikidata.png'/></a>
            </div>
            "
        `;
        var tab = "";
        if(typeof data.urlimage != "undefined") {
            tab += `<img src="${data.urlimage}" alt="" style="width:98%"> <br>`
        } else if(data.apiData['image'] != null){
            tab += `<img src="${data.apiData['image']}" alt="" style="width:98%"> <br>`
        }
        tab += "<table id='descript-table'>";
        if(typeof(data.country) != "undefined") {
            tab += "<tr><td><?php echo Yii::t('cms', "Country") ?></td><td>"+data.country+"</td></tr>";
        } 
        if(typeof(data.population) != "undefined") {
            tab += "<tr><td>Population</td><td>"+parseInt(data.population).toLocaleString()+" hab</td></tr>";
        } else if(data.apiData['population'] != null){
            tab += "<tr><td>Population</td><td>"+parseInt(data.apiData['population']).toLocaleString()+" hab</td></tr>";
        }
        if(typeof(data.area) != "undefined") tab += "<tr><td><?php echo Yii::t('cms', "Area") ?></td><td>"+data.area+" km2</td></tr>";
        if(typeof(data.population) != "undefined" && typeof(data.area) != "undefined") tab += "<tr><td><?php echo Yii::t('cms', "Density") ?></td><td>"+(parseInt(data.population) / parseInt(data.area)).toFixed(3)+" hab / km2</td></tr>";
        if(lat != "") tab += "<tr><td>Latitude</td><td>"+lat+"</td></tr>"
        if(long != "") tab += "<tr><td>Longitude</td><td>"+long+"</td></tr>";
        tab += "</table>";
        if(lat != "" && long != "") tab += `<div class="main-map-city map-container col-sm-12" id="main-map-city" style="height: 400px; width: 500px; margin-top: 25px; margin-bottom:30px;"></div>`

        $("#dbpedia-data").html(src);
        $("#wikidata-data").html(tab);

        changePage(0);

        if(lat != "" && long != "") {
            var mainMapCity = new CoMap({
                container : "#main-map-city",
                activePopUp : true,
                mapOpt:{
                    btnHide : false,
                    doubleClick : true,
                    scrollWheelZoom: false,
                    zoom : 3,
                },
                mapCustom:{
                    tile : "openstreetmap",
                    addMarker: function(context, params){
                        var latLon = [params.latitude, params.longitude];
                        var label = params.label;
            
                        var marker = L.marker(latLon, params.opt);
                        marker.addTo(context.getMap());
                        marker.bindPopup(label).openPopup();
                    }
                },
                elts : {}
            });

            var elts = {
                    latitude: lat,
                    longitude: long,
                    label: locationCity
                }
            mainMapCity.addMarker(elts);
        }

        $("#loading-spinner").html("");
    }

    function afficheDescriptions(){

        removeActiveClassFromButtons();
        document.querySelector('.btn-discover-abstract').classList.add('active-btn');
        $(".div-xs-menu-list").toggleClass("active");

        startIndex = 0;
        interval = 60;

        var src_content = ` <div class="row">
                                <div class="col-sm-8 dbpedia-data text-center" id="dbpedia-data" style="padding-left: 2%; margin-top:70px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); overflow: hidden;">

                                </div>
                                <div class="col-sm-4 table-wiki-detail" id="wikidata-data" style="margin-top:80px;">    
                                </div>
                                <div id="loading-spinner" style="margin-top:150px;"></div>
                            </div>`;
        $("#all_wikidata_item").html(src_content);

        $("#loading-spinner").html(htmlSpiner);

        $("#data-xs-list").html("A propos");

        var dataVide = `<h3 style="text-transform:none; text-align:center"><?php echo Yii::t('cms', "No summary available for this city on Wikipedia") ?> <br><?php echo Yii::t('cms', "Please try again later.") ?></h3>`;

        if(typeof costum.wikiDescription != "undefined") {
            listDescription(costum.wikiDescription);
        } else {
            const params = {
                url: ``,
                title : locationCity,
                type: "abstract",
                wikidataID: wikidataId
            }

            const url_abstract = `${apiUrl_wikipedeia}?${new URLSearchParams(params)}`;

            return fetch(url_abstract)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`Erreur HTTP : ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                if(data != ""){
                    $("#loading-spinner").html("");
                    costum.wikiDescription = data;
                    listDescription(data);
                } else {
                    $("#loading-spinner").html(dataVide);
                }
            })
            .catch(error => {
                $("#loading-spinner").html(dataVide);
            }) 
        }
    } 

    function getObjectsWithAttribute(arr, attributeName, searchChar) {
        var filteredArray = arr.filter(function(obj) {
            if (obj.hasOwnProperty(attributeName) && typeof obj[attributeName] === 'string') {
                var charMin = obj[attributeName].toLowerCase();
                return charMin.indexOf(searchChar) !== -1;
            }
            return false;
        });
        return filteredArray;
    }

    function addBtnPlusElements(data, wikidatacontent) {
        startIndex += interval;
        if (startIndex >= data.length) {
            $(".showMore<?= $kunik ?>-elements").html("");
        } else {
            var src = `<button class="showMore" id="showMoreElements" ><?php echo Yii::t('cms', "Show more") ?></button>`;
            $(".showMore<?= $kunik ?>-elements").html(src);

            $("#showMoreElements").on("click", function(){
                listElements(data, wikidatacontent, "plus");
            });
        }
    }

    // Fonction pour détecter la fin du défilement dans la ul
    function detectEndOfScroll() {
        var ulList = document.getElementById('wikidata-content');
        var lastLi = ulList.lastElementChild;

        var lastLiOffset = lastLi.offsetTop + lastLi.clientHeight;
        var pageOffset = window.pageYOffset + window.innerHeight;

        if (pageOffset > lastLiOffset) {
            var showMoreButton = document.getElementById('showMoreElements');
            showMoreButton.click();
        }
    }

    // TEST
    var selectedArray = [];
    function setDropdownFilters(filtersData, dropdownId, type, filtersContainerClass) {
        selectedArray = [];
        const dropdownContainer = document.getElementById(dropdownId);

        filtersData.forEach((filter) => {
            const optionElement = createDropdownElement(filter, dropdownContainer, type, dropdownId, filtersContainerClass);
            dropdownContainer.appendChild(optionElement);
        });
    }

    function createDropdownElement(filter, dropdownContainer, type, dropdownId, filtersContainerClass) {
        const optionElement = document.createElement("a");
        optionElement.textContent = filter;

        optionElement.addEventListener("click", () => {
            handleDropdownSelection(optionElement, filter, type, dropdownId, filtersContainerClass);
        });

        return optionElement;
    }

    function handleDropdownSelection(optionElement, filter, type, dropdownId, filtersContainerClass) {
        optionElement.remove();
        selectedArray.push(filter);
        setFiltreActifs(type, dropdownId, filtersContainerClass);
        type == "personne" ? recherchePersonne(costum.wikiPersonnes) : rechercheElements(costum.wikidataLieu, wikidatacontent);
    }

    function setFiltreActifs(type, dropdownId, filtersContainerClass) {
        const filtersContainer = $(filtersContainerClass);

        if (selectedArray.length > 0) {
            const src = selectedArray.map((filter) => {
                return `<button style="margin-left: 5px" class="btn btn-primary" data-field="${filter}">
                            <i class="fa fa-times-circle delete_actif_filter" data-field="${filter}"></i>
                            <span class="activeFilters">${filter}</span>
                        </button>`;
            }).join("");
            const filterText = '&nbsp;&nbsp;&nbsp;<?php echo Yii::t('cms', "Active filter") ?> : &nbsp;&nbsp;';
            filtersContainer.html(filterText + src);

            $(".delete_actif_filter").on("click", function() {
                const deletedFilter = $(this).attr("data-field");
                removeDropdownFromFilters(deletedFilter, type, dropdownId, filtersContainerClass);
            });
        } else {
            filtersContainer.html("");
        }
    }

    function removeDropdownFromFilters(filterToRemove, type, dropdownId, filtersContainerClass) {
        $(`.delete_actif_filter[data-field="${filterToRemove}"]`).parent().remove();

        const dropdownContainer = document.getElementById(dropdownId);
        const optionElement = createDropdownElement(filterToRemove, dropdownContainer, type);
        dropdownContainer.appendChild(optionElement);

        selectedArray = selectedArray.filter(occupation => occupation !== filterToRemove);

        selectedArray.length == 0 ? $(filtersContainerClass).html("") : "";

        type == "personne" ? recherchePersonne(costum.wikiPersonnes) : rechercheElements(costum.wikidataLieu, wikidatacontent);
    }
    // fin test

    function setResult(number, div) {
        var resultData = `<h4 class="text-resultat"> ${number} <?php echo Yii::t('cms', "Results") ?> </h4>`;
        $(`.${div}`).html(resultData);
    }

    function setResultPersonne(number, div) {
        var resultData = `<h4 class="text-resultat-personne"> ${number} <?php echo Yii::t('cms', "Results") ?> </h4>`;
        $(`.${div}`).html(resultData);
    }

    // function getElementWithAttribute(arr, searchChar) {
    //     var filteredArray = arr.filter(function(obj) {
    //         if (searchChar == "") {
    //             return (!selectedArray || selectedArray.length === 0) || (selectedArray.length > 0 &&
    //                 selectedArray.every(selectedLabel => obj.label.includes(selectedLabel.toLowerCase())));
    //         } else {
    //             var charMin = obj["label"].toLowerCase();
    //             var attributeMatch = charMin.includes(searchChar.toLowerCase());

    //             if (selectedArray.length === 0) {
    //                 return attributeMatch;
    //             } else {
    //                 var occupationMatch = selectedArray.every(selectedLabel => obj.label.includes(selectedLabel.toLowerCase()));
    //                 return attributeMatch && occupationMatch;
    //             }
    //         }
    //     });
    //     return filteredArray;
    // }

    function listElements(data, wikidatacontent, state) {
        
        state == "init" ? linksElements = [] : "";

        for (var i = startIndex; i < startIndex + interval && i < data.length; i++) {

            var value = data[i];

            if(!linksElements.includes(value.item)){

                linksElements.push(value.item);

                var lat = value.latitude;
                var lon = value.longitude;
                var lab = value.label;

                var elts = {
                        latitude: lat,
                        longitude: lon,
                        label: lab,
                        imgURL : value.urlimage,
                        itemID: `wikidata-item-${lab.replace(/\s/g, '-')}`
                    }
                mainMap.addMarker(elts);

                var listItem = document.createElement('li');

                listItem.innerHTML = `<div class="div-liste-${i}" style="cursor:pointer" data-img="${value.urlimage}" data-lab="${lab}" data-lat="${lat}" data-lon="${lon}" id="${elts.itemID}"><h2>${value.label}</h2>
                                        <div class="img-liste hidden-xs" style="display: none"></div>
                                        <div class="row visible-xs">
                                            <div class="col-sm-6 col-xs-12">
                                                <img src="${value.urlimage}" onerror="handleImageError(this)" alt="">
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="map-container-xs" data-lat="${lat}" data-lon="${lon}"></div>
                                            </div>
                                        </div>
                                        <h4>${value.description != null ? value.description : value.label}</h4>
                                        <p>Info sur Wikipedia : <a href="${value.item}" target="_blank">${value.item}</a> </p></div>`;
                                        
                wikidatacontent.appendChild(listItem);

                var divListe = listItem.querySelector(`.div-liste-${i}`);
                divListe.addEventListener('click', function() {
                    var clickedLat = parseFloat(this.getAttribute('data-lat'));
                    var clickedLon = parseFloat(this.getAttribute('data-lon'));
                    var imgURL = this.getAttribute('data-img');

                    document.querySelectorAll('.img-liste').forEach(img => img.style.display = 'none');
                    var divElement = this.querySelector('.img-liste');
                    divElement.style.display = 'block';
                    divElement.innerHTML = `<img class="imgMap" src="${imgURL}" onerror="handleImageError(this)" alt="" >`;
                    
                    mainMap.getMap().flyTo([clickedLat, clickedLon], 18, { animate: true, duration: 2.5 });
                    mainMap.getMap().eachLayer(function (layer) {
                        if (layer instanceof L.Marker) {
                            if (layer.getLatLng().lat === clickedLat && layer.getLatLng().lng === clickedLon) {
                                layer.openPopup();
                            }
                        }
                    });
                });

                // xs
                var mapContainer = listItem.querySelector('.map-container-xs');
                if (mapContainer) {
                    var latxs = parseFloat(mapContainer.getAttribute('data-lat'));
                    var lonxs = parseFloat(mapContainer.getAttribute('data-lon'));

                    var mapxs = L.map(mapContainer).setView([latxs, lonxs], 15);

                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                        attribution: '&copy; OpenStreetMap contributors'
                    }).addTo(mapxs);

                    L.marker([latxs, lonxs]).addTo(mapxs);
                }
            }
        }

        setResult(data.length, "result_elements");
        addBtnPlusElements(data, wikidatacontent);

        $("#loading-spinner-element").html("");

        if(state == "init") {

            // setDropdownFilters(filtersElements, "elementDropdown", "element", ".btn-select-critere");

            $("#searchInput").on("keyup", function(event) {
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function() {
                    rechercheElements(data, wikidatacontent);
                }, 2000);
            });
        }
    }
    
    function afficheElements()
    {
        removeActiveClassFromButtons();
        document.querySelector('.btn-discover-location').classList.add('active-btn');
        $(".div-xs-menu-list").toggleClass("active");

        var dataVide = `<h3 style="text-transform:none; text-align:center">Aucun Lieu n'est disponible pour cette ville sur Wikipédia. <br> <?php echo Yii::t('cms', "Please try again later.") ?></h3>`;

        interval = 100;
        startIndex = 0;
        $("#all_wikidata_item").html("");
        var src_content = ` <div class="row content wikidata-container text-center" id="wikidata-container">
                                <div class="div_search_xs div_search_element">
                                    <div class="row">
                                        <div class="visible-xs col-xs-12 text-center"><p style="font-size: 20px; font-weight: bold;">Lieu</p></div>
                                        <div class="col-md-2 col-sm-4 col-xs-12">
                                            <input type="text" class="inputCherche" id="searchInput" placeholder="     <?php echo Yii::t('cms', "What are you looking for") ?> ?">
                                            <span class="input-group-addon" data-icon="fa-arrow-circle-right"> <i class="fa fa-arrow-circle-right"></i> </span>
                                        </div>
                                        <div class="col-md-10 col-sm-8 result_elements">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">    
                                    <ul id="wikidata-content">

                                    </ul>
                                    <div id="loading-spinner-element" style="margin-top:190px;"></div>
                                    <div class="showMore<?= $kunik ?>-elements"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 content sp-cms-container hidden-xs">
                                    <div id="main-map" class="map-container hidden-xs"><div>
                                </div>
                            </div>`;
        
        $("#all_wikidata_item").html(src_content);

        $("#loading-spinner-element").html(htmlSpiner);

        $("#data-xs-list").html("Lieu");

        changeClassForEditMode("div_search_element", "inputCherche", "result_elements");

        if(!costum.editMode) {
            $("#main-map").addClass("main-map_noedit");
            $("#wikidata-content").addClass("wikidata-content_noedit");
        } else {
            $("#wikidata-content").addClass("wikidata-content_edit");
        }

        mainMap = new CoMap({
            container : "#main-map",
            activePopUp : true,
            mapOpt:{
                btnHide : false,
                doubleClick : true,
                scrollWheelZoom: false,
                zoom : 3,
            },
            mapCustom:{
                tile : "maptiler",
                addMarker: function(context, params){
                    var latLon = [params.latitude, params.longitude];
                    var label = params.label;
        
                    var marker = L.marker(latLon, params.opt);
                    marker.addTo(context.getMap());
                    marker.bindPopup(label).openPopup();

                    marker.on('click', function() {
                        var itemId = params.itemID;
                        var listItem = document.getElementById(itemId);
                        if (listItem) {
                            listItem.scrollIntoView({ behavior: 'smooth', block: 'center' });
                            $(`#${itemId}`).css("margin-top", "100px");

                            document.querySelectorAll('.img-liste').forEach(img => img.style.display = 'none');
                            var divElement = listItem.querySelector('.img-liste');
                            divElement.style.display = 'block';
                            divElement.innerHTML = `<img class="imgMap" src="${params.imgURL}" alt="" >`;

                            mainMap.getMap().flyTo([params.latitude, params.longitude], 18, { animate: true, duration: 2.5 });
                        }
                    });
                }
            },
            elts : {}
        });

        if(wikidataId != ""){
            wikidatacontent = document.getElementById('wikidata-content');
    
            if(typeof costum.wikidataLieu != "undefined") {
                listElements(costum.wikidataLieu, wikidatacontent, "init");
            } else {
                const params = {
                    url: `https://www.wikidata.org/wiki/Special:EntityData/${wikidataId}.json`,
                    title : locationCity,
                    type: "elements",
                    wikidataID: wikidataId
                }
                const url_elements = `${apiUrl_wikipedeia}?${new URLSearchParams(params)}`;
                return fetch(url_elements)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(`Erreur HTTP : ${response.status}`);
                        }
                        return response.json();
                    })
                    .then(data => {
                        if (data && data.length > 0) {
                            costum.wikidataLieu = data;
                            listElements(data, wikidatacontent, "init");
                        } else {
                            setResult("0", "result_elements");
                            $("#loading-spinner-element").html(dataVide);
                        }
                    })
                    .catch(error => {
                        $("#loading-spinner-element").html(dataVide);
                        setResult("0", "result_elements");
                    });
            }
        } else {
            setResult("0", "result_elements");
            $("#loading-spinner-element").html(dataVide);
        }
    }

    function rechercheElements(data, wikidatacontent) {
        $("#wikidata-content").html("");
        $(".showMore<?= $kunik ?>-elements").html("");
        var text = $("#searchInput").val();  
        linksElements = [];

        var textMin = text!= "" ? text.toLowerCase() : "";
        startIndex = 0;
        interval = 60;

        if(textMin != "" || selectedArray.length > 0) {
            var dataSearch = getObjectsWithAttribute(data, "label", textMin);
            listElements(dataSearch, wikidatacontent, "search");
        } else {
            listElements(data, wikidatacontent, "search");
        }
    }

    function imageExists(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                callback(xhr.status === 200);
            }
        };
        xhr.open('HEAD', url, true);
        xhr.send();
    }

    function handleImageError(img) {
        var defaultImagePath = `${baseUrl}/<?= $assetsUrl ?>/images/logos/default_img.png`;
        imageExists(defaultImagePath, function(exists) {
            if (exists) {
                img.src = defaultImagePath;
            } 
        });
    }

    var options = {
            root: null,
            rootMargin: "0px",
            threshold: 0.5
        };

    function addBtnPlusPersonnes(data) {
        startIndex += interval;
        if (startIndex >= data.length) {
            $(".showMore<?= $kunik ?>-personnes").html("");
        } else {
            var src = `<button class="showMore" id="showMorePersonnes"><?php echo Yii::t('cms', "Show more") ?></button>`;
            $(".showMore<?= $kunik ?>-personnes").html(src);

            $("#showMorePersonnes").on("click", function(){
                listePersonnes(data, "plus")
            });

            var observer = new IntersectionObserver(function(entries, observer) {
                entries.forEach(function(entry) {
                    if (entry.isIntersecting) {
                        $("#showMorePersonnes").click();
                        observer.disconnect();
                    }
                });
            }, options);

            var divContainingShowMoreButton = document.getElementById("showMore<?= $kunik ?>-personnes");
            observer.observe(divContainingShowMoreButton);
        }
    }

    var urls = [];
    var eventsTimes = [];
    function newtimelinelist(data) {
        var timelinsLinks = [];
        var timeline_knightlab = null;
        for (var i = 0; i < data.length; i++) {
            var value = data[i];
            if(!timelinsLinks.includes(value.links)){

                timelinsLinks.push(value.links);

                if (value.datedebut != "") {
                    var startdate = new Date(value.datedebut);
                    var enddate = value.datefin != "" ? new Date(value.datefin) : "";
                    var valueTimes = "";
                    if (enddate != "") {
                        valueTimes = {
                            start_date: {
                                year: startdate.getFullYear(),
                                month: startdate.getMonth(),
                                day: startdate.getDate()
                            },
                            end_date: {
                                year: enddate.getFullYear(),
                                month: enddate.getMonth(),
                                day: enddate.getDate()
                            },
                            text: {
                                headline: value.name,
                                text: value.occupation
                            },
                            media: {
                                url: value.urlImg,
                                caption: value.name
                            },
                            background: {}
                        };
                    } else {
                        valueTimes = {
                            start_date: {
                                year: startdate.getFullYear(),
                                month: startdate.getMonth(),
                                day: startdate.getDate()
                            },
                            text: {
                                headline: value.name,
                                text: value.occupation
                            },
                            media: {
                                url: value.urlImg,
                                caption: value.name
                            },
                            
                            background: {}
                        };
                    }

                    valueTimes['background']['color'] = "#092434";

                    eventsTimes.push(valueTimes);
                }
            }
        }
        const timelineConfig = {
            events: eventsTimes,
            script_path : `${baseUrl}/plugins/knightlab_timeline3/js/`,
            language : 'fr',
            local: 'fr',
            font : 'bevan-pontanosans',
            theme : 'contrast',
            scale_factor : 0.5,
            timenav_position : 'top'
        };
        var timeLineVide = `<h3 style="text-transform:none; text-align:center"><?php echo Yii::t('cms', "No results for timeline display") ?></h3>`;
        eventsTimes.length > 0 ? timeline_knightlab = new TL.Timeline('personne-content-timeline', timelineConfig) : $("#personne-content-timeline").html(timeLineVide);
    }

    function getOccupation(person) {
        if (typeof person.occupation !== 'string') {
            return person.occupation;
        }

        if (person.genre === 'masculin') {
            return person.occupation.split(' ou ')[0].trim();
        } else if (person.genre === 'féminin') {
            const occupations = person.occupation.split(' ou ');
            return occupations.length > 1 ? occupations[1].trim() : occupations[0].trim();
        } else {
            return person.occupation.split(' ou ')[0].trim();
        }
    }

    var top5Occupations = [];

    function topFullOccupation(resultatFinal) {
        const occurrences = {};
        resultatFinal.forEach(personne => {
            const occupations = personne.occupation;

            if (Array.isArray(occupations)) {
                occupations.forEach(occupation => {
                occurrences[occupation] = (occurrences[occupation] || 0) + 1;
                });
            } else {
                occurrences[occupations] = (occurrences[occupations] || 0) + 1;
            }
        });
        const sortedOccupations = Object.keys(occurrences).sort((a, b) => occurrences[b] - occurrences[a]);
        top5Occupations = sortedOccupations.slice(0, 10);
    }


    function inspectData(tableauAvecObjets) {
        const resultatUnique = {};
        tableauAvecObjets.forEach((objet) => {
            const links = objet.links;
            if (!resultatUnique[links]) {
                resultatUnique[links] = { ...objet, occupation: [getOccupation(objet)] };
            } else {
                const existingOccupation = resultatUnique[links].occupation;
                const ocpt = getOccupation(objet);
                if (!existingOccupation.includes(ocpt)) {
                    existingOccupation.push(ocpt);
                }
            }
        });
        const resultatFinal = Object.values(resultatUnique);
        topFullOccupation(resultatFinal);
        return resultatFinal;
    }

    function formaterDate(element) {

        var dateFormatee = "";

        if (isNaN(new Date(element))) {
            return null; 
        }
        const date = new Date(element);

        const annee = date.getFullYear();
        const mois = (date.getMonth() + 1).toString().padStart(2, '0'); // +1 car les mois commencent à 0
        const jour = date.getDate().toString().padStart(2, '0');

        dateFormatee = `${annee}-${mois}-${jour}`;

        return dateFormatee;
    }

    function getPersonnesWithAttribute(arr, searchChar) {
        var filteredArray = arr.filter(function(obj) {
            if (searchChar === "") {
                return (!selectedArray || selectedArray.length === 0) || (selectedArray.length > 0 &&
                    selectedArray.every(selectedOccupation => obj.occupation.includes(selectedOccupation.toLowerCase())));
            } else {
                var charMin = obj["name"].toLowerCase();
                var attributeMatch = charMin.includes(searchChar.toLowerCase());

                if (selectedArray.length === 0) {
                    return attributeMatch;
                } else {
                    var occupationMatch = selectedArray.every(selectedOccupation => obj.occupation.includes(selectedOccupation.toLowerCase()));
                    return attributeMatch && occupationMatch;
                }
            }
        });
        return filteredArray;
    }

    function listePersonnes(data, state) {

        for (var i = startIndex; i < startIndex + interval && i < data.length; i++) {

            var value = data[i];

            if(!urls.includes(value.links)){

                urls.push(value.links);

                dateDebut =  formaterDate(value.datedebut) != null ? formaterDate(value.datedebut) : "";
                dateFin =  formaterDate(value.datefin) != null ? formaterDate(value.datefin) : "";

                var date = "";

                if(dateDebut != "" && dateFin != "") {
                    date = `Né le <b>${dateDebut}</b> - mort le <b>${dateFin}</b>`;
                } else if(dateDebut != "" && dateFin == "") {
                    date = `Né le <b>${dateDebut}</b>`;
                } else if(dateDebut == "" && dateFin != ""){
                    date = `Décédé le <b>${dateFin}</b>`;
                }

                var src = `<div class="col-md-2 col-sm-4 col-xs-12 text-center data-content" 
                            onclick="openPreview('personne','${value.name.replaceAll("'", "\\'")}', '${value.urlImg.replaceAll("'", "\\'")}', '${value.links.replaceAll("'", "\\'")}', '${value.description.replaceAll("'", "\\'").replaceAll('"', "\\'")}', 
                                                            '${dateDebut}', '${dateFin}', '${value.lieudebut.replaceAll("'", "\\'")}', '${value.lieufin.replaceAll("'", "\\'")}', 
                                                            '${(value.occupation.join(', ')).replaceAll("'", "\\'")}', '${value.genre}', '${value.compagnon.replaceAll("'", "\\'")}')">
                            <h3 style="text-transform:none">${value.name}</h3>
                            <div>
                                <img src="${value.urlImg}" alt="" onerror="handleImageError(this)" class="centered-image">
                            </div>
                            <p>${date}</p>
                        </div>`;
                $("#personne-content").append(src);
            }
        }

        setResultPersonne(data.length, "result_personne");

        state != "plus" ? newtimelinelist(data) : "";

        if(state == "init") {

            topFullOccupation(data);

            top5Occupations.length > 0 ? setDropdownFilters(top5Occupations, "occupationDropdown", "personne", ".btn-select-filtre") : "";

            $("#loading-spinner").html("");
            $(".affichagePersonne").on("click", function(){
                var type = this.getAttribute("data-type");
                if(type == "simple") {
                    $("#personne-content").css("display", "block");
                    $("#personne-content-timeline").css("display", "none");
                    $("#btn_list").css("display", "none");
                    $("#btn_timeline").css("display", "block");
                    $("#showMore<?= $kunik ?>-personnes").css("display", "block")
                } else {
                    $("#personne-content").css("display", "none");
                    $("#personne-content-timeline").css("display", "block");
                    $("#btn_timeline").css("display", "none");
                    $("#btn_list").css("display", "block");
                    $("#showMore<?= $kunik ?>-personnes").css("display", "none")
                }
            });
            $("#searchInputPersonne").on("input", function(){
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function() {
                    recherchePersonne(data);
                }, 1000);
            });
        }
        addBtnPlusPersonnes(data);
    }

    function affichePersonnes(){
        removeActiveClassFromButtons();
        document.querySelector('.btn-discover-human').classList.add('active-btn');
        $(".div-xs-menu-list").toggleClass("active");

        var dataVide = `<h3 style="text-transform:none; text-align:center"><?php echo Yii::t('cms', "No one has a connection to this city on Wikipedia") ?>. <br> <?php echo Yii::t('cms', "Please try again later.") ?></h3>`;
        urls = []; eventsTimes = []; top5Occupations = []; startIndex = 0; interval = 100;
        $("#all_wikidata_item").html("");

        var src_content = `<div class="row">
                                <div class="div_search_xs div_search_personne">
                                    <div class="row"> 
                                        <div class="visible-xs col-xs-12 text-center"><p style="font-size: 20px; font-weight: bold;"><?php echo Yii::t('cms', "Person") ?></p></div>
                                        <div class="col-md-2 col-sm-4 col-xs-12">
                                            <input type="text" class="inputCherche" id="searchInputPersonne" placeholder="   <?php echo Yii::t('cms', "What are you looking for") ?> ?">
                                            <span class="input-group-addon" data-icon="fa-arrow-circle-right"> <i class="fa fa-arrow-circle-right"></i> </span>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                           <div class="dropdown">
                                                <button class="dropbtn">Occupation &nbsp;&nbsp;<i class="fa fa-angle-down"></i></button>
                                                <div class="dropdown-content" id="occupationDropdown">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 col-sm-4 col-xs-6 result_personne">
                                        </div>
                                        <div class="col-md-1 col-sm-2 col-xs-6 btn-diff-list">
                                            <button class="btn btn-primary affichagePersonne" id="btn_list" style="display: none;" data-type="simple"> <i class="fa fa-list"></i> <t class="hidden-xs"> Liste </t> </button>
                                            <button class="btn btn-primary affichagePersonne" id="btn_timeline" style="display: block;" data-type="timeline"> <i class="fa fa-calendar"></i> <t class="hidden-xs"> Timeline </t> </button>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 text-left btn-select-filtre" style="margin-bottom: 5px;">
                                        </div>
                                    </div>
                                </div>
                                <div id="personne-content" style="display: block;">
                                </div>
                                <div id="loading-spinner" style="margin-top:150px;"></div>
                                <div id="personne-content-timeline" style="width: 90%; margin-left:5%; margin-right:5%; height: 70vh; display: none;"></div>
                            </div>
                            <div class="showMore<?= $kunik ?>-personnes text-center" style="width: 100px;" id="showMore<?= $kunik ?>-personnes">
                            </div>`;
                            
        $("#all_wikidata_item").html(src_content);   
        $("#loading-spinner").html(htmlSpiner);
        $("#data-xs-list").html("Personnes");

        changeClassForEditMode("div_search_personne", "inputCherche", "result_personne");

        if(!costum.editMode) {
            $(".dropdown").addClass("input_noedit");
            $(".affichagePersonne").addClass("affichage_noedit");
            $("#personne-content").addClass("wikidata-content_noedit");
        } else {
            $(".dropdown").addClass("input_edit");
            $(".affichagePersonne").addClass("affichage_edit");
            $("#personne-content").addClass("wikidata-content_edit");
        }

        if(typeof costum.wikiPersonnes != "undefined"){
            listePersonnes(costum.wikiPersonnes, "init");
        } else {
            var params = {
                url: "",
                title : locationCity,
                type: "personnes",
                wikidataID: wikidataId
            }
            var url_personnes = `${apiUrl_wikipedeia}?${new URLSearchParams(params)}`;
            return fetch(url_personnes)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`Erreur HTTP : ${response.status}`);
                    }
                    return response.json();
                })
                .then(data => {
                    if(data && data.length > 0){
                        var dataInspect = inspectData(data);

                        costum.wikiPersonnes = dataInspect;

                        listePersonnes(dataInspect, "init");
                    } else {
                        setResultPersonne("0", "result_personne");
                        $(".affichagePersonne").css("display", "none");
                        $("#loading-spinner").html(dataVide);
                    }
                })
                .catch(error => {
                    $("#loading-spinner").html(dataVide);
                    setResultPersonne("0", "result_personne");
                    $(".affichagePersonne").css("display", "none");
                });
        }
    }

    function recherchePersonne(data) {
        urls = [];
        eventsTimes = [];
        $("#personne-content").html("");
        $(".showMore<?= $kunik ?>-personnes").html("");
        var text = $("#searchInputPersonne").val();

        startIndex = 0;
        interval = 100;
        var textMin = text!= "" ? text.toLowerCase() : "";

        if(textMin != "" || selectedArray != "") {
            var dataSearch = getPersonnesWithAttribute(data, textMin);
            listePersonnes(dataSearch, "search");
        } else {
            listePersonnes(data, "search");
        }
    }

    function addBtnPlusOrga(data) {
        startIndex += interval;
        if (startIndex >= data.length) {
            $(".showMore<?= $kunik ?>-orga").html("");
        } else {
            var src = `<button class="showMore" id="showMoreOrga"><?php echo Yii::t('cms', "Show more") ?></button>`;
            $(".showMore<?= $kunik ?>-orga").html(src);
            $("#showMoreOrga").on("click", function(){
                listOrga(data, "plus");
            });

            var observer = new IntersectionObserver(function(entries, observer) {
                entries.forEach(function(entry) {
                    if (entry.isIntersecting) {
                        $("#showMoreOrga").click();
                        observer.disconnect();
                    }
                });
            }, options);

            var divContainingShowMoreButton = document.getElementById("showMore<?= $kunik ?>-orga");
            observer.observe(divContainingShowMoreButton);
        }
    }

    var linksOrga = [];
    function listOrga(data, state) {

        state == "init" ? linksOrga = [] : "";

        for (var i = startIndex; i < startIndex + interval && i < data.length; i++) {

            var value = data[i];

            if(!linksOrga.includes(value.links)) {

                linksOrga.push(value.links);

                var src = `<div class="col-md-2 col-sm-4 col-xs-12 text-center data-content" 
                                onclick="openPreview('orga','${value.name.replaceAll("'", "\\'")}', '${value.urlImg.replaceAll("'", "\\'")}', '${value.links.replaceAll("'", "\\'")}', '${value.description.replaceAll("'", "\\'").replaceAll('"', "\\'")}')">
                                <h3 style="text-transform:none">${value.name}</h3>
                                <div>
                                    <img src="${value.urlImg}" alt="" onerror="handleImageError(this)" class="centered-image">
                                </div>
                            </div>`;

                $("#orga-content").append(src);
            }
        }

        setResult(data.length, "result_orga");
        addBtnPlusOrga(data);

        $("#loading-spinner").html(""); 

        if(state == "init") {
            $("#searchInputOrga").on("input", function(){
                var textcherche = $(this).val();
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function() {
                    rechercheOrga(data, textcherche);
                }, 1000);
            });
        }
    }

    function afficheOrga() {

        removeActiveClassFromButtons();
        document.querySelector('.btn-discover-orga').classList.add('active-btn');
        $(".div-xs-menu-list").toggleClass("active");
        var dataVide = `<h3 style="text-transform:none; text-align:center"><?php echo Yii::t('cms', "There is no organization in this city on Wikipedia") ?>. <br> <?php echo Yii::t('cms', "Please try again later.") ?></h3>`;
        startIndex = 0;
        $("#all_wikidata_item").html("");
        var src_content = `<div class="row">
                                <div class="div_search_xs div_search_orga">
                                    <div class="row">
                                        <div class="visible-xs col-xs-12 text-center"><p style="font-size: 20px; font-weight: bold;"><?php echo Yii::t('cms', "Organization") ?></p></div>
                                        <div class="col-md-2 col-sm-4 col-xs-12">
                                            <input type="text" class="inputCherche" id="searchInputOrga" placeholder="   <?php echo Yii::t('cms', "What are you looking for") ?> ?"><span class="input-group-addon" data-icon="fa-arrow-circle-right"> <i class="fa fa-arrow-circle-right"></i> </span>
                                        </div>
                                        <div class="col-md-10 col-sm-8 result_orga">
                                        </div>
                                    </div>
                                </div>
                                <div id="orga-content" >
                                </div><br>
                                <div id="loading-spinner" style="margin-top:150px;"></div>
                            </div>
                            <div class="showMore<?= $kunik ?>-orga text-center" id="showMore<?= $kunik ?>-orga">
                            </div>
                            `;

        $("#all_wikidata_item").html(src_content); 

        $("#loading-spinner").html(htmlSpiner);

        $("#data-xs-list").html("Organisations");

        changeClassForEditMode("div_search_orga", "inputCherche", "result_orga");

        interval += 12;

        if(typeof costum.wikiOrga != "undefined") {
            listOrga(costum.wikiOrga, "init");
        } else {
            var params = {
                url: "",
                title : locationCity,
                type: "orga",
                wikidataID: wikidataId
            }
            var url_orga = `${apiUrl_wikipedeia}?${new URLSearchParams(params)}`;
            return fetch(url_orga)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`Erreur HTTP : ${response.status}`);
                    }
                    return response.json();
                })
                .then(data => {
                    if(data.length > 0){
                        costum.wikiOrga = data;
                        listOrga(data, "init");
                    } else {
                        setResult("0", "result_orga");
                        $("#loading-spinner").html(dataVide);
                    }
                })
                .catch(error => {
                    setResult("0", "result_orga");
                    $("#loading-spinner").html(dataVide);
                })
        }
    }

    function rechercheOrga(data, text) {
        linksOrga = [];

        $("#orga-content").html("");
        $(".showMore<?= $kunik ?>-orga").html("");

        startIndex = 0;
        interval = 72;
        var textMin = text != "" ? text.toLowerCase() : "";

        var dataSearch = getObjectsWithAttribute(data, "name", textMin);
        
        textMin == "" ? listOrga(data, "search") : listOrga(dataSearch, "search");
    }

    function addBtnPlusEvent(data) {
        startIndex += interval;
        if (startIndex >= data.length) {
            $(".showMore<?= $kunik ?>-event").html("");
        } else {
            var src = `<button class="showMore" id="showMoreEvent"><?php echo Yii::t('cms', "Show more") ?></button>`;
            $(".showMore<?= $kunik ?>-event").html(src);
            $("#showMoreEvent").on("click", function(){
                listEvents(data, "plus");
            });

            var observer = new IntersectionObserver(function(entries, observer) {
                entries.forEach(function(entry) {
                    if (entry.isIntersecting) {
                        $("#showMoreEvent").click();
                        observer.disconnect();
                    }
                });
            }, options);

            var divContainingShowMoreButton = document.getElementById("showMore<?= $kunik ?>-event");
            observer.observe(divContainingShowMoreButton);
        }
    }

    var linksEvent = [];
    var events = [];

    function listEvents(data, state) {
        state != "plus" ? events = [] : "";
        for (let index = startIndex; index < startIndex + interval && index < data.length; index++) {

            const element = data[index];

            if (element.date != "" && element.label != "") {
                date = new Date(element.date);

                labelRetress = (element.label).substring(0, 50);
                labelRetress = (element.label).substring(50) != "" ? labelRetress + " ..." : labelRetress;

                var src = `<a href="${element.links}" target="_blank">
                        <div class="col-sm-3 col-xs-12 one_event">
                            <h4>${date.getDate()} - ${date.getMonth()} - ${date.getFullYear()}</h4>
                            <h4>${labelRetress}</h4>
                            <h5>${element.description}</h5>
                            <p>${element.links}</p>       
                        </div></a>`;
                $("#event-content").append(src);

                const value = {
                    start_date: {
                        year: date.getFullYear(),
                        month: date.getMonth(),
                        day: date.getDate(),
                        hour: date.getHours(),
                        minute: date.getMinutes()
                    },
                    text: {
                        headline: element.label,
                        text: element.description
                    },
                    media: {
                        url: element.imageUrl,
                        caption: element.label
                    },
                    background: {}
                };

                // if (element.imageUrl != "") {
                //     value['background']['url'] = element.imageUrl;
                // } else {
                    value['background']['color'] = "#092434";
                // }

                if (!events.includes(value)) {
                    events.push(value);
                }
            }
        }


        if(state == "init") {
            $(".affichageEvent").on("click", function(){
                var type = this.getAttribute("data-type");
                if(type == "simple") {
                    $("#event-content").css("display", "block");
                    $("#event-content-timeline").css("display", "none");
                    $("#btn_list_event").css("display", "none");
                    $("#btn_timeline_event").css("display", "block");
                    $("#showMore<?= $kunik ?>-event").css("display", "block");
                } else {
                    $("#event-content").css("display", "none");
                    $("#event-content-timeline").css("display", "block");
                    $("#btn_timeline_event").css("display", "none");
                    $("#btn_list_event").css("display", "block");
                    $("#showMore<?= $kunik ?>-event").css("display", "none");
                }
            });
            $("#searchInputEvent").on("input", function(){
                var textcherche = $(this).val();
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function() {
                    rechercheEvent(data, textcherche);
                }, 1000);
            });
        }

        setResultPersonne(data.length, "result_event");
        addBtnPlusEvent(data);

        $("#loading-spiner-event").html("");

        if(events.length > 0) {
            const timelineConfig = {
                events: events,
                script_path : `${baseUrl}/plugins/knightlab_timeline3/js/`,
                language : 'fr',
                local: 'fr',
                font : 'bevan-pontanosans',
                theme : 'contrast',
                scale_factor : 0.5,
                timenav_position : 'top'
            };
            const timeline = new TL.Timeline('event-content-timeline', timelineConfig);

            // timeline.on('change', function () {
            //     const currentSlide = timeline.getCurrentSlide();

            //     if(currentSlide.data.text.headline == events[events.length - 1].text.headline) {

            //         listEvents(data, "plus");

            //         //timeline.goToSlide(timeline.current_id);
            //     }
            // });
        } else {
            var timeLineVide = `<br><br><br><h3 style="text-transform:none; text-align:center"><?php echo Yii::t('cms', "No results for timeline display") ?></h3>`;
            $("#event-content-timeline").html(timeLineVide);
        }
    }

    function afficheEvenement() {

        removeActiveClassFromButtons();
        document.querySelector('.btn-discover-event').classList.add('active-btn');
        $(".div-xs-menu-list").toggleClass("active");
        startIndex = 0; interval = 100;
        $("#all_wikidata_item").html("");

        var dataVide = `<br><br><br><br><h3 style="text-transform:none; text-align:center"><?php echo Yii::t('cms', "There are no events linked to this city on Wikipedia") ?>. <br> <?php echo Yii::t('cms', "Please try again later.") ?></h3>`;
        var src_content = `<div class="row">
                                <div class="div_search_xs div_search_event">
                                    <div class="row">
                                        <div class="visible-xs col-xs-12 text-center"><p style="font-size: 20px; font-weight: bold;"><?php echo Yii::t('cms', "Events") ?></p></div>
                                        <div class="col-md-2 col-sm-4 col-xs-12">
                                            <input type="text" class="inputCherche" id="searchInputEvent" placeholder="<?php echo Yii::t('cms', "What are you looking for") ?> ?"><span class="input-group-addon" data-icon="fa-arrow-circle-right"> <i class="fa fa-arrow-circle-right"></i> </span>
                                        </div>
                                        <div class="col-md-9 col-sm-6 col-xs-6 result_event">
                                        </div>
                                        <div class="col-md-1 col-sm-2 col-xs-6">
                                            <button class="btn btn-primary affichageEvent" id="btn_list_event" style="display: block;" data-type="simple"><i class="fa fa-list"></i> <t class="hidden-xs"> Liste </t> </button>
                                            <button class="btn btn-primary affichageEvent" id="btn_timeline_event"  style="display: none;" data-type="timeline"><i class="fa fa-calendar"></i> <t class="hidden-xs"> Timeline </t> </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="event-content" style="width: 90%; margin-left:5%; margin-right:5%; height: 75vh; margin-top: 130px; display: none;">
                                    
                                </div>
                                <div id="event-content-timeline" style="width: 90%; margin-left:5%; margin-right:5%; height: 75vh; margin-top: 130px; display: block;">
                                    <div id="loading-spiner-event" style="margin-top: 150px;" class="text-center"></div>
                                </div>
                            </div>
                            <div class="showMore<?= $kunik ?>-event text-center" id="showMore<?= $kunik ?>-event" style="display: none">
                            </div>
                            `;
        $("#all_wikidata_item").html(src_content); 
        $("#loading-spiner-event").html(htmlSpiner);
        $("#data-xs-list").html("Evénements");

        changeClassForEditMode("div_search_event", "inputCherche", "result_event");
        !costum.editMode ? $(".affichageEvent").addClass("affichage_noedit") : $(".affichageEvent").addClass("affichage_edit");
    
        if(typeof costum.wikiEvenements != "undefined") {
            listEvents(costum.wikiEvenements, "init");
        } else {
            var params = {
                url: "",
                title : locationCity,
                type: "event",
                wikidataID: wikidataId
            }
            var url_event = `${apiUrl_wikipedeia}?${new URLSearchParams(params)}`;

            return fetch(url_event)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`Erreur HTTP : ${response.status}`);
                    }
                    return response.json();
                })
                .then(data => {
                    if(data.length > 0) {
                        costum.wikiEvenements = data;
                        listEvents(data, "init");
                    } else {
                        setResultPersonne("0", "result_event");
                        $("#loading-spiner-event").html(dataVide);
                        $(".affichageEvent").css("display" , "none");
                    }
                })
                .catch(error => {
                    setResultPersonne("0", "result_event");
                    $("#loading-spiner-event").html(dataVide);
                    $(".affichageEvent").css("display" , "none");
                })
        }
    }

    function rechercheEvent(data, text) {
        linksEvent = [];

        $("#event-content").html("");
        $("#event-content-timeline").html("");
        $(".showMore<?= $kunik ?>-event").html("");

        startIndex = 0;
        interval = 100;

        var textMin = text != "" ? text.toLowerCase() : "";

        var dataSearch = getObjectsWithAttribute(data, "label", textMin);

        if(dataSearch == "" || dataSearch == null) {
            $("#event-content").html(searcheNull);
            $("#event-content-timeline").html(searcheNull);
            setResultPersonne("0", "result_event");
        } else {
            textMin == "" ? listEvents(data, "search") : listEvents(dataSearch, "search");
        }
    }
    
    function openPreview(type, name, urlImg, links, description, datedebut = null, datefin = null, lieudebut = null, lieufin = null, occupation =null, genre = null, compagnon = null) {
        var objet = {
            "type" : type,
            "name" : name,
            "urlImg" : urlImg,
            "links" : links,
            "description" : description,
            "datedebut" : datedebut,
            "datefin" : datefin,
            "lieudebut" : lieudebut,
            "lieufin" : lieufin,
            "occupation" : occupation,
            "genre" : genre,
            "compagnon" : compagnon,
            "address" :  typeof costum.address != "undefined" ? costum.address : ""
        }
        urlCtrl.openPreview("/view/url/costum.views.custom.cocity.preview", objet);
    }

    function getDataCostum(){
        ajaxPost(
            null,
            baseUrl+"/co2/element/get/type/"+costum.contextType+"/id/"+costum.contextId,
            null,
            function(data){
                if(typeof data.map != "undefined" && typeof data.map.address != "undefined") {
                    data = data.map;
                    costum.address = data.address;
                    costum.addresseLocality = typeof data.address.addressLocality != "undefined" ? data.address.addressLocality : data.address.level3Name;
                    if(typeof data.address.addressLocality != "undefined") {
                        costum.addresseLocality = data.address.addressLocality
                    } else if(typeof data.address.level5Name != "undefined") {
                        costum.addresseLocality = data.address.level5Name
                    } else if(typeof data.address.level4Name != "undefined") {
                        costum.addresseLocality = data.address.level4Name
                    } else {
                        costum.addresseLocality = data.address.level3Name
                    }
                    addressOrga = [
                        {
                            "address" : data.address,
                            "geo" : data.geo,
                            "geoPosition" : data.geoPosition
                        }
                    ];

                    getWikidataID(costum.addresseLocality);
                } else {
                    var addressNull = `<br><br><br><br><br><br><br><br><br><br><br><br><h3 style="text-transform:none; text-align:center">Merci d'ajouter l'adresse de votre cocity dans l'information générale de votre Organisation.</h3>`;
                    $("#all_wikidata_item").html(addressNull);
                    $(".btn-discover").addClass("disabled");
                }
            }, 
            null,
            null,
            {async:false}
        );
    }

    function addOrgaforOrga(name, links, description, urlImg) {
        var dyfOrga={
            "beforeBuild":{
                "properties" : {
                    "name" : {
                        "label" : "Nom de l'organisation", 
                        "inputType" : "text",
                        "placeholder" :"Nom de l'organisation",
                        "value" : name,
                        "order" : 1
                    },
                    "shortDescription" : {
                        "inputType" : "textarea",
                        "value" : description
                    },
                    "url" : {
                        "inputType" : "text",
                        "value" : links
                    }
                }
            },
            "onload" : {
                "actions" : {
                    "setTitle" : "Creer votre organisation",
                    "src" : {
                        "infocustom" : "<br/>Remplir le champ"
                    },
                }
            },
            afterBuild : function(){
                $("#imageUploader_paste").val(urlImg);
            }  
        };
        dyfOrga.afterSave = function(orga) {
            dyFObj.commonAfterSave(orga, function(){
                window.open(baseUrl + "/#@" + orga.map.slug, "_blank");
            })
        };
        dyFObj.openForm('organization',null,Object.assign({ type: "GovernmentOrganization",role: "admin", addresses: addressOrga}, {}),null,dyfOrga);
    }

    function changeClassForEditMode(divSearch, inputSearch, resultDiv) {
        if(!costum.editMode) {
            $(`.${divSearch}`).addClass("menu-noedit");
            $(`.${inputSearch}`).addClass("input_noedit");
            $(`.${resultDiv}`).addClass("result_noedit");
        } else {
            $(`.${inputSearch}`).addClass("input_edit");
            $(`.${resultDiv}`).addClass("result_edit");
        }
    }

    jQuery(document).ready(function(){

        !costum.editMode ? $(".wikidata-menu").addClass("menu-noedit") : "";
        getDataCostum();

        $(".wikidata-menu-xs").off().on('click', function(){
            $(".div-xs-menu-list").toggleClass("active");
        });
    });
</script>