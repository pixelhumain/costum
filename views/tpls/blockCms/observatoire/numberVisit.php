<?php 
    $keyTpl = "numberVisit";
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss]; 
?>
<style id="css-<?= $kunik ?>">

</style>
<span class="col-xs-12 text-center <?= $kunik ?>"> <?php echo $visit ?></span>
<script>
    sectionDyf.<?php echo $kunik?>blockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
    if (costum.editMode){
		var numberVisitInput = {
			configTabs : {
				style : {
					inputsConfig : [
						"color",
						"fontSize",
						"fontFamily",
                        "fontWeight",
					]
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
			}
		}
		cmsConstructor.blocks["<?= $kunik ?>"] =  numberVisitInput;
		
	}
</script>