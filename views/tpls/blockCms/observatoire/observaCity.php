<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.1/chart.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js/dist/chart.umd.min.js"></script>

<style id="css_<?= $kunik ?>_observaCity">
  .<?= $kunik ?> .select2-container{
      width: 100%;
      font-size: 18px !important;
  }

  .<?= $kunik ?> .select2-container-multi .select2-choices {
    border: 3px solid rgb(16, 31, 46) !important;
    border-radius: 20px 20px 20px 20px;
  }

  .<?= $kunik ?> .select-search {
    margin-bottom: 4%;
  }


.sectionClass {
  padding: 20px 0px 50px 0px;
  position: relative;
  display: block;
}

.fullWidth {
  width: 100% !important;
  display: table;
  float: none;
  padding: 0;
  min-height: 1px;
  height: 100%;
  position: relative;
}

#all-title {
  margin: 100px 0 0px;
}
.sectiontitle {
  background-position: center;
  text-align: center;
  min-height: 20px;
}

.sectiontitle h2 {
  font-size: 30px;
  color: #222;
  margin-bottom: 0px;
  padding-right: 10px;
  padding-left: 10px;
}


#all-title .headerLine {
  width: 160px;
  height: 2px;
  display: inline-block;
  background: #101F2E;
}

#title-region .headerLine, #title-pays .headerLine {
  width: 80px;
  height: 2px;
  display: inline-block;
  background: #101F2E;
}


.projectFactsWrap {
    display: flex;
    margin-top: 30px;
    flex-direction: row;
    flex-wrap: wrap;
}
.projectFactsWrap #allCocity {
  cursor: pointer;
}


#projectFacts .fullWidth{
    padding-right: 12%;
    padding-left: 12%;
}

.projectFactsWrap .item{
  width: 25%;
  height: 100%;
  padding: 50px 0px;
  text-align: center;
}

.projectFactsWrap .item:nth-child(1){
  background: rgb(16, 31, 46);
}

.projectFactsWrap .item:nth-child(2){
  background: rgb(18, 34, 51);
}

.projectFactsWrap .item:nth-child(3){
  background: rgb(21, 38, 56);
}

.projectFactsWrap .item:nth-child(4){
  background: rgb(23, 44, 66);
}

.projectFactsWrap .item p.number{
  font-size: 40px;
  padding: 0;
  font-weight: bold;
}

.projectFactsWrap .item p{
  color: rgba(255, 255, 255, 0.8);
  font-size: 18px;
  margin: 0;
  padding: 10px;
  font-family: 'Open Sans';
}


.projectFactsWrap .item span{
  width: 60px;
  background: rgba(255, 255, 255, 0.8);
  height: 2px;
  display: block;
  margin: 0 auto;
}


.projectFactsWrap .item i{
  vertical-align: middle;
  font-size: 50px;
  color: rgba(255, 255, 255, 0.8);
}


.projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
  color: white;
}

.projectFactsWrap .item:hover span{
  background: white;
}

@media (max-width: 786px){
  .projectFactsWrap .item {
     flex: 0 0 50%;
  }
}
/* data city */
.data-city<?= $kunik ?> .card-counter{
    box-shadow: 2px 2px 10px #DADADA;
    margin: 5px;
    padding: 20px 10px;
    background-color: #fff;
    height: 100px;
    border-radius: 5px;
    transition: .3s linear all;
    color: #FFF;
  }

  .data-city<?= $kunik ?> .card-counter:hover{
    box-shadow: 4px 4px 20px #DADADA;
    transition: .3s linear all;
  }

  .data-city<?= $kunik ?> .card-counter.primary{
    background-color: #93C020;
  }

  .data-city<?= $kunik ?> .card-counter.pruple{
    background-color: #8C5AA1;
  }  

  .data-city<?= $kunik ?> .card-counter.success{
    background-color: #467ea4;
  }  

  .data-city<?= $kunik ?> .card-counter.info{
    background-color: #F9B21A;
  }  

  .data-city<?= $kunik ?> .card-counter.green{
    background-color: #20C06B;
  }  

  .data-city<?= $kunik ?> .card-counter.danger{
    background-color: #e6344d;
  }  

  .data-city<?= $kunik ?> .card-counter i{
    font-size: 5em;
    opacity: 0.3;
  }

  .data-city<?= $kunik ?> .card-counter .count-numbers{
    position: absolute;
    right: 35px;
    top: 20px;
    font-size: 32px;
    display: block;
  }

  .data-city<?= $kunik ?> .card-counter .count-name{
    position: absolute;
    right: 35px;
    top: 65px;
    /* font-style: italic; */
    text-transform: capitalize;
    opacity: 1;
    display: block;
    font-size: 18px;
  }

  .hide {
      opacity: 0;
      display: none;
  }

  .<?= $kunik ?> .data-city<?= $kunik ?> .container {
    width: 100% !important;
  }

  .<?= $kunik ?> .centered-columns {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap; /* Permet aux éléments de passer à la ligne si nécessaire */
  }

  .<?= $kunik ?> .title-card {
    display: flex;
    justify-content: center; /* Centre horizontalement */
    align-items: center;
    color: rgba(255, 255, 255, 0.8);
    text-align: center;
    margin-bottom: 10px;
  }

  .<?= $kunik ?> .title-card i{
    font-size: 30px;
    margin-top: 25px;
  }

  .<?= $kunik ?> .region-card {
    margin-top: 35px;
    font-family: 'Open Sans';
  }

  .<?= $kunik ?> .content-card {
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 50px;
    font-weight: bold;
    cursor: pointer;
  }

  .<?= $kunik ?> .region-card .item:nth-child(1){
    background: rgb(16, 31, 46);
  }

  .<?= $kunik ?> .region-card .item:nth-child(2){
    background: rgb(18, 34, 51);
  }

  .<?= $kunik ?> .region-card .item:nth-child(3){
    background: rgb(21, 38, 56);
  }

  .<?= $kunik ?> .region-card .item:nth-child(4){
    background: rgb(23, 44, 66);
  }

  .<?= $kunik ?> .region-card .item:nth-child(5){
    background: rgb(38, 57, 76);
  }

  .<?= $kunik ?> .region-card .item:nth-child(6){
    background: rgb(46, 65, 84);
  }

  .<?= $kunik ?> .foncer {
    background-color: #8080804f;
  }

  .<?= $kunik ?> .name_zone {
    margin-top: 10px;
  }

  .<?= $kunik ?> .chartCard {
    margin-top: 25px;
    margin-bottom: 35px;
  }

  .<?= $kunik ?> .chartMenu p {
    padding: 10px;
    font-size: 50px;
  }

  .<?= $kunik ?> .pieChart_city {
    /* background-color: #e5ded4; */
  }

  .<?= $kunik ?> .barchart-cumuldata-country, .<?= $kunik ?> .obsthema_city, .<?= $kunik ?> .pieChart_region {
    background-color: #e5ded4;
  }

  .<?= $kunik ?> .barchart-cumuldata-country {
    padding-top: 25px;
  }

  .<?= $kunik ?> .pieChart_city h3, .obsthema_city h3, .pieChart_region h3{
    color: #222;
    text-transform: none;
  }

  .<?= $kunik ?> #title_pie_chart h3{
    margin-top: 25px;
    margin-bottom: 40px;
  }
  .<?= $kunik ?> .subtitle {
    margin-top: 185px;
    margin-right: -109px;
  }

  .<?= $kunik ?> .oneCitypie_chart {
    justify-content: center;
    display: flex;
    margin-bottom: 50px;
  }
  
  /* fin css data city */
    /*  */
  .<?= $kunik ?> .card-box {
    position: relative;
    color: #fff;
    padding: 15px 10px 8px;
    margin: 20px 0px;
    border-radius: 10px;
  }
  .<?= $kunik ?> .card-box:hover {
      text-decoration: none;
      color: #f1f1f1;
  }
  .<?= $kunik ?> .card-box:hover .icon i {
      font-size: 80px;
      transition: 1s;
      -webkit-transition: 1s;
      color: rgb(255 255 255 / 96%);
  }
  .<?= $kunik ?> .card-box .inner {
      padding: 5px 10px 0 10px;
  }
  .<?= $kunik ?> .card-box h4 {
      font-size: 27px;
      font-weight: bold;
      margin: 0 0 8px 0;
      white-space: nowrap;
      padding: 0;
      text-align: left;
  }
  .<?= $kunik ?> .card-box p {
      font-size: 18px;
  }
  .<?= $kunik ?> .card-box .icon {
      position: absolute;
      top: auto;
      bottom: 5px;
      right: 5px;
      z-index: 0;
      font-size: 60px; 
      color: rgb(255 255 255 / 11%);
  }
  .<?= $kunik ?> .card-box .card-box-footer {
      position: absolute;
      left: 0px;
      bottom: 0px;
      text-align: center;
      padding: 3px 0;
      color: rgba(255, 255, 255, 0.8);
      background: rgba(0, 0, 0, 0.1);
      width: 100%;
      text-decoration: none;
  }
  .<?= $kunik ?> .card-box:hover .card-box-footer {
      background: rgba(0, 0, 0, 0.3);
  }
  .<?= $kunik ?> .bg-green {
      background-color: rgb(16, 31, 46) !important;
  }
/*  */
  .<?= $kunik ?> .div_search_elements {
    text-align: left;
    box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
    width: 100%;
    /* position: fixed; */
    z-index: 1111;
    background-color: white;
    /* margin-left: 14px; */
    height: auto;
    /* margin-top: -30px; */
  }
  .<?= $kunik ?> .dropdown {
    position: relative;
    display: inline-block;
    width: 100%;
    margin: 5px;
  }

  .<?= $kunik ?> .dropbtn {
    background-color: transparent;
    color: grey;
    padding: 10px;
    font-size: 15px;
    cursor: pointer;
    width: 100%; /* Largeur à 100% */
    border-radius: 20px;
    border: 2px solid #84d70f;
  }

  .<?= $kunik ?> .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: calc(100% - 4px); /* Largeur du dropdown moins la marge */
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    max-height: 220px; /* Limite la hauteur du dropdown */
    overflow-y: auto;
    top: 40px; /* Ajustez la position verticale */
    left: 0; /* Ajustez la position horizontale */
  }
  .<?= $kunik ?> .dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    cursor: pointer;
  }

  .<?= $kunik ?> .dropdown-content a:hover{
    background-color: #80808075;
  }

  .<?= $kunik ?> .search-input {
    position: absolute;
    top: 1px;
    left: 0;
    width: 100%;
    z-index: 2; /* Assurez-vous que le champ de recherche est au-dessus du dropdown */
    border-radius: 20px;
    border: 2px solid #84d70f;
    padding: 10px;
    box-sizing: border-box;
    background-color: transparent;
  }
/*  */
  .<?= $kunik ?> .data-regions-selected {
    margin-top: 40px;
  }
  .<?= $kunik ?> .btn-chart-regions {
    margin-top: 20px;
    margin-bottom: 30px;
  }
  .<?= $kunik ?> .btn-show-map {
    margin: 10px 3px !important;
  } 
  .<?= $kunik ?> .filter-btn-hide-map {
    position: absolute;
    top: 70px;
    right: 20px;
    z-index: 9999;
    border-radius: 100%;
    width: 40px;
    height: 40px;
    font-size: 20px;
    border: 2px solid red;
    outline: none;
    background: red;
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    transition: .3s;
  }
  .<?= $kunik ?> .main-map {
    margin-top: 45px; 
    height: 85vh; 
    position:fixed; 
    width: 100%;
  }
  .<?= $kunik ?> .main-map-filter {
    margin-top: 78px !important;
  }
  .<?= $kunik ?> .hidenMap-filter {
    top: 100px;
  }
  .<?= $kunik ?> .btn-success {
    background-color: #00b5c0 !important;
    border-color: #00b5c0 !important;
  }
  /* loader */
  .<?= $kunik ?> .loader-loader<?= $kunik ?> {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 42%;
    overflow: hidden;
    -webkit-overflow-scrolling: touch;
    outline: 0;
    z-index: 9999999999999999 !important;
    padding-top: 180px !important;
  }
  .<?= $kunik ?> .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> {
    width: auto;
    height: auto;
  }
  .<?= $kunik ?> .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> .processingLoader {
    width : auto !important;
    height : auto !important;
  }
  .<?= $kunik ?> .backdrop-loader<?= $kunik ?> {
    position: fixed;
    opacity: .3;
    background-color : #000;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height : 100vh;
    z-index: 999999999 !important;
  }

  .<?= $kunik ?> #myChart, #myChart-region {
    min-height: 467px;
    min-width: 931px;
    max-height: 950px;
    max-width: 1901px;
  }
</style>
<div id="<?= $kunik ?>ObservaCity" class="<?= $kunik ?>">

  <div class="loader-loader<?= $kunik ?> hidden" id="loader-loader<?= $kunik ?>" role="dialog">
		<div class="content-loader<?= $kunik ?>" id="content-loader<?= $kunik ?>"></div>
	</div>
	<div class="backdrop-loader<?= $kunik ?> hidden" id="backdrop-loader<?= $kunik ?>"></div>

  <div class="div_search_elements" id="champ-filters">
    <div class="row">
      <div class="col-md-2 col-sm-2 col-xs-12">
        <div class="dropdown">
          <button class="dropbtn">Pays &nbsp;&nbsp;<i class="fa fa-angle-down"></i></button>
          <div class="dropdown-content dropdown-content-pays" id="countryDropdown">
                                                    
          </div>
          <input type="text" id="searchInputPays" class="search-input search-input-pays">
        </div>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-12">
        <div class="dropdown">
          <button class="dropbtn">Régions &nbsp;&nbsp;<i class="fa fa-angle-down"></i></button>
          <div class="dropdown-content dropdown-content-regions" id="regionDropdown">
                                                    
          </div>
          <input type="text" id="searchInputRegions" class="search-input search-input-regions">
        </div>
      </div>
      <di class="col-md-5 col-sm-5 col-xs-12">
      </di>
      <div class="col-md-3 col-sm-3 col-xs-6">
        <button type="button" class="btn-show-map hidden-xs list-affiche" id="showMap">
          <i class="fa fa-map-marker"></i> &nbsp;&nbsp;&nbsp; Carte
        </button>
      </div>
      <div class="col-sm-12 col-xs-12 text-left btn-select-filtre" style="margin-bottom: 5px;">
      </div>
    </div>
  </div>
  <!--  -->
  <div id="projectFacts" class="sectionClass">
    <div class="map-affiche main-map hide" id="observaCity-map">
      
    </div>
    <button type="button" class="map-affiche filter-btn-hide-map hide" id="hidenMap">
      <i class="fa fa-times" aria-hidden="true"></i>
    </button>
    <div class="list-affiche sectiontitle" id="all-title">
      <h2>Observacity</h2>
      <span class="headerLine"></span>
    </div>
    <div class="list-affiche fullWidth eight columns">
      <div class="projectFactsWrap ">
        <div class="item wow fadeInUpBig animated animated" data-number="<?= $nomberCity ?>" style="visibility: visible;">
          <i class="fa fa-globe"></i>
          <p id="number1" class="number"><?= $nomberCity ?></p>
          <span></span>
          <p>Pays</p>
        </div>
        <div class="item wow fadeInUpBig animated animated" data-number="<?= $momberElements ?>" style="visibility: visible;">
          <i class="fa fa-cube"></i>
          <p id="number2" class="number"><?= $momberElements ?></p>
          <span></span>
          <p>Eléments</p>
        </div>
        <div class="item wow fadeInUpBig animated animated" data-number="<?= $momberCitoyen ?>" style="visibility: visible;">
          <i class="fa fa-user"></i>
          <p id="number3" class="number"><?= $momberCitoyen ?></p>
          <span></span>
          <p>Personnes</p>
        </div>
        <div class="item wow fadeInUpBig animated animated" onclick="getAllCocityMap()" id="allCocity" data-number="<?= $nomberCocity ?>" style="visibility: visible;">
          <i class="fa fa-building"></i>
          <p id="number4" class="number"><?= $nomberCocity ?></p>
          <span></span>
          <p>COcity</p>
        </div>
      </div>
    </div>
  </div>
  <!--  -->
  <!-- <div class="row select-search">
      <div class="col-md-4 col-sm-2 col-xs-2">
      </div>
      <div class="col-md-4 col-sm-8 col-xs-8 text-center menu-deroulant input-group">
        <select id="menuDeroulant" multiple="multiple" placeholder="Saisissez / Choisissez un pays">
        </select>
      </div>
      <div class="col-md-4 col-sm-2 col-xs-2">
      </div>
  </div> -->
  <!-- div data city -->
  <div id="dataCity<?= $kunik ?>" class="data-city<?= $kunik ?> list-affiche">
    <div class="sectiontitle hide" id="title-pays">
      <h2>PAYS</h2>
      <span class="headerLine"></span>
    </div>
    <div class="barchart-cumuldata-country hide">
      <div class="container">
        <div class="row centered-columns" id="cumul-data-country">

        </div>
      </div>

      <div class="chartCard hide hidden-xs">
        <div class="chartBox row">
          <div class="col-lg-2"></div>
          <div class="col-lg-8">
            <canvas id="myChart"></canvas>
          </div>
          <div class="col-lg-2"></div>
        </div>
      </div>
    </div>
    <div class="row pieChart_city centered-columns" id="pieChart_city">
      <div class="col-lg-12 col-sm-12 col-xs-12 text-center hide" id="title_pie_chart">
        <h3>Présentation détaillée de toutes les régions de chaque pays.</h3>
      </div>
    </div>

    <div class="row obsthema_city container" id="obsthema_city" style="margin: 10px">
      <div class="col-lg-12 col-sm-12 col-xs-12 text-center hide" id="title_obsthema">
        <h3>Thématiques communes</h3>
      </div>
      <div class="row centered-columns" id="obsthema_city_content" style="margin-right: 10px">

      </div>
    </div>
      <!-- <canvas id="chartId" aria-label="chart" height="350" width="580"></canvas> -->
    
    <div class="region-card hide" id="region-card">
      <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 row title-card">
          <div class="item col-lg-2">
            <i class="fa fa-globe"></i>
            <p>Régions</p>
          </div>
          <div class="item col-lg-2">
            <i class="fa fa-users"></i>
            <p>Organisations</p>
          </div>
          <div class="item col-lg-2">
            <i class="fa fa-rocket"></i>
            <p>Projets</p>
          </div>
          <div class="item col-lg-2">
            <i class="fa fa-user"></i>
            <p>Personnes</p>
          </div>
          <div class="item col-lg-2">
            <i class="fa fa-calendar"></i>
            <p>Evènements</p>
          </div>
          <div class="item col-lg-2">
            <i class="fa fa-map-marker"></i>
            <p>POI</p>
          </div>
        </div>
        <div class="col-lg-1"></div>
      </div>
      <div class="genericData" id="genericData">

      </div>
    </div>

    <div class="data-regions-selected hide" id="data-regions-selected">
      <div class="sectiontitle" id="title-region">
        <h2>REGIONS</h2>
        <span class="headerLine"></span>
      </div>
      <div class="centered-columns cumuldata" id="cumul-data-region">

      </div>
      <div class="btn-chart-regions hidden-xs" id="btn-chart-regions">
        <div class="chartBox row">
          <div class="col-lg-2"></div>
          <div class="col-lg-8">
            <canvas id="myChart-region"></canvas>
          </div>
          <div class="col-lg-2"></div>
        </div>
      </div>

      <div class="row pieChart_region centered-columns" id="pieChart_region">
        <div class="col-lg-12 col-sm-12 col-xs-12 text-center hide" id="title_pie_chart_region">
          <h3>Présentation détaillée de toutes les départements de chaque régions.</h3>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var countrys = <?php echo json_encode($country); ?>;
  var dataMaps = [];  
  var allDataResults = {};
  var dataCitySelecter = [];
  var allIdSelected = [];
  var selectedFiltersActifs = [];
  var allRegions = [];
  var isMap = false;
  var icons = {
    organizations: 'fa-users',
    citoyens: 'fa-user',
    poi: 'fa-map-marker',
    projects: 'fa-cogs',
    events: 'fa-database',
  };
  var colorClass = {
    organizations: 'primary',
    projects : 'pruple',
    poi: 'green',
    events: 'info',
    citoyens: 'success',
  }
  var nameClass = {
    organizations: 'Organisation',
    projects : 'Projets',
    poi: 'POI',
    events: 'Evenements',
    citoyens: 'Personnes',
  }

  var elementsTypes = ["Organisation", "Projets", "Evenements", "POI", "Personnes", "Spam"];

  var observaMap = new CoMap({
    container : "#observaCity-map",
    activePopUp : true,
    mapOpt:{
      menuRight : false,
      btnHide : false,
      doubleClick : true,
      zoom : 2,
          scrollWheelZoom: false
    }
  });

  function getAllCocityMap() {
    var params = {
      searchType: ["organizations"],
      notSourceKey : true,
      filters: {
        "costum.cocity" : {
            '$exists': true
        }
      },
      count: true,
      countType : ["organizations"],
      indexStep: 0
    }

    ajaxPost(
      null,
      baseUrl + "/" + moduleId + "/search/globalautocomplete",
      params, 
      function(response) {
        observaMap = new CoMap({
          container : "#observaCity-map",
          activePopUp : true,
          mapOpt:{
            menuRight : false,
            btnHide : false,
            doubleClick : true,
            zoom : 2,
                scrollWheelZoom: false
          },
          mapCustom: {
            markers: {
              getMarker : function(data){
                var imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/cocity/smarterritoire.png";	
                return imgM;
              }
            }
          },
          elts : []
        });
        $("#showMap").trigger('click');
        observaMap.clearMap();
        observaMap.addElts(response.results);
      }
    )
  }

  function setOptions() {
    var dropdown = document.getElementById("countryDropdown");
    dropdown.innerHTML = '';

    var keys = Object.keys(countrys).sort(function(a, b) {
      return countrys[a].name.localeCompare(countrys[b].name);
    });
    keys.forEach(function(key) {
      var paysInfo = countrys[key];

      var listItem = document.createElement("a");
      listItem.textContent = paysInfo.name;
      listItem.setAttribute("data-id", key);
      listItem.setAttribute("class", key);
      listItem.setAttribute("data-name", paysInfo.name);
      dropdown.appendChild(listItem);
    });
  }

  function getDataOneCity(cityId) {
    observaMap = new CoMap({
      container : "#observaCity-map",
      activePopUp : true,
      mapOpt:{
        menuRight : false,
        btnHide : false,
        doubleClick : true,
        zoom : 2,
            scrollWheelZoom: false
      }
    });
    var params = {
      searchType: ["organizations", "poi", "projects", "classifieds", "citoyens", "events"],
      notSourceKey : true,
      filters: {},
      count: true,
      countType : ["organizations", "poi", "projects", "classifieds", "citoyens", "events"],
      indexStep : 0
    }

    if(cityId != null && cityId != [] && cityId.length > 0) {

      if(!isMap) {
        $('#loader-loader<?= $kunik ?>').removeClass('hidden');	
        $('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
        coInterface.showLoader('#content-loader<?= $kunik ?>');
      }

      observaMap.clearMap();
      params.filters["address.level1"] = {$in: cityId};
      ajaxPost(
        null,
        baseUrl + "/" + moduleId + "/search/globalautocomplete",
        params,
        function(responses){
          observaMap.addElts(responses.results);
          dataMaps = responses.results;
          var counts = responses.count;
          var html = '';
          for(var key in counts) {
            if(counts[key] > 0 && key != "spam" && key != "classifieds") html += `<div class="col-md-2 col-xs-6 datacitysection datacitysection_country">
                      <div class="card-counter ${colorClass[key]}">
                        <i class="fa ${icons[key]}"></i>
                        <span class="count-numbers">${counts[key]}</span>
                        <span class="count-name">${nameClass[key]}</span>
                      </div>
                    </div>`
          }
          $("#cumul-data-country").html(html);
          $("#title-pays").removeClass('hide').hide().fadeIn();
        }
      )
      getMultidataChartCity(cityId);
    } else {
      initdatasection();
      observaMap.clearMap();
    }
  }

  function initdatasection(){
    $(".datacitysection_country").addClass("hide");
    $(".region-card").addClass("hide");
    $(".genericData").html("");
    $(".chartCard").addClass("hide");
    $("#title_pie_chart").addClass("hide");
    $("#title_obsthema").addClass("hide");
    $("#obsthema_city_content").html("");
    $(".barchart-cumuldata-country").addClass('hide');
    $("#title-pays").addClass('hide');
    $('#loader-loader<?= $kunik ?>').addClass('hidden');	
    $('#backdrop-loader<?= $kunik ?>').addClass('hidden');
    // observaMap.clearMap();
  }

  var idSelected = {};
  var allDataBarChart = {};

  var divClass = "claire";

  function getMultidataChartCity(cityId) {

    var dataD3Chart = {};

    var promises = cityId.map(async function(id) {

      var latest = await getSousZone(id, '3');
      allRegions[id]= latest;

      if(!allDataBarChart[dataCitySelecter[id]]) {
        var paramsFilters = {
          searchType: ["organizations", "poi", "projects", "citoyens", "events"],
          notSourceKey: true,
          filters: {
              "address.level1": id,
          },
          count: true,
          countType: ["organizations", "poi", "projects", "citoyens", "events"],
          onlyCount: true
        };

        if(latest != null && latest != "") setPieChartRegion(latest, id, 'pieChart_city', 'title_pie_chart', dataCitySelecter);

        return new Promise(function(resolve, reject) {
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                paramsFilters,
                function(results) {
                    dataD3Chart[dataCitySelecter[id]] = results.count;
                    allDataBarChart[dataCitySelecter[id]] = results.count;
                    resolve();
                }
            );
        });
      } else {

        if(latest != null && latest != "") setPieChartRegion(latest, id, 'pieChart_city', 'title_pie_chart', dataCitySelecter);

        return new Promise(function(resolve, reject) {
          dataD3Chart[dataCitySelecter[id]] = allDataBarChart[dataCitySelecter[id]];
          resolve();
        })
      }
    });

    Promise.all(promises)
      .then(function() {
        $(".barchart-cumuldata-country").removeClass('hide').hide().fadeIn();
        setChartCity(dataD3Chart, "myChart");
        classDataForThematique(cityId, allThem);
      });
  }

  function setChartCity(dataElements, type) {

    $(".chartCard").removeClass("hide");

    var canvas = document.getElementById(type);

    if (!canvas) {
        mylog.log("L'élément canvas avec l'ID 'myChart' n'a pas été trouvé.");
        return;
    }
    var existingChart = Chart.getChart(canvas);

    if (existingChart) {
        existingChart.destroy();
    }
    if (Object.keys(dataElements).length > 0) {
        var data = {
            labels: ['Organisations', 'Projets', 'Evenements', 'POI', 'Citoyens'],
            datasets: []
        };

        for (var country in dataElements) {
          if (dataElements.hasOwnProperty(country)) {
              var countryData = dataElements[country];
              var dataset = {
                  label: country,
                  data: [countryData.organizations, countryData.projects, countryData.events, countryData.poi, countryData.citoyens],
                  backgroundColor: getRandomColor()
              };
              data.datasets.push(dataset);
          }
      }

      const config = {
          type: 'bar',
          data,
          options: {
              scales: {
                  y: {
                      beginAtZero: true
                  }
              }
          }
      };

      const myChart = new Chart(  
        canvas,
        config
      );
      const chartVersion = document.getElementById('chartVersion');

      $('#loader-loader<?= $kunik ?>').addClass('hidden');	
			$('#backdrop-loader<?= $kunik ?>').addClass('hidden');
    }
  }

  var idpiechartActif = [];

  function setPieChartRegion(latest, id, divId, titleId, dataSelecter) {

    if(!idpiechartActif.includes(id)) {

      idpiechartActif.push(id);
      var allData = [];
      var allLabels = [];

      var promises = $.map(latest, function(value, key) {
        var paramsFilters = divId == 'pieChart_city' ? {
          'address.level1' : id,
          'address.level3' : key
        } : {
          'address.level3' : id,
          'address.level4' : key
        }
        return new Promise(function(resolve, reject) {
          var params = {
            searchType: ["organizations", "poi", "projects", "classifieds", "citoyens", "events"],
            notSourceKey: true,
            filters: paramsFilters,
            count: true,
            countType: ["organizations", "poi", "projects", "classifieds", "citoyens", "events"],
            onlyCount: true
          };

          ajaxPost(
            null,
            baseUrl + "/" + moduleId + "/search/globalautocomplete",
            params,
            function(responses) {
              var cmtp = responses.count;
              var somme = cmtp.organizations + cmtp.projects + cmtp.events + cmtp.citoyens + cmtp.poi;
              if (somme > 0) {
                  allLabels.push(value["name"]);
                  allData.push(somme);
              }
              resolve();
            }
          );
        });
      });

      Promise.all(promises)
        .then(function() {
          if (allData != []) {

            $("#"+titleId).removeClass("hide");

            var html = `<div class="row col-lg-6 col-sm-12 col-xs-12 oneCitypie_chart" id="${id}">
                          <h3 class="subtitle">${dataSelecter[id]}</h3>
                          <canvas id="chartId_${id}" aria-label="chart" height="350" width="580"></canvas>
                        </>`;
            
            $("#"+divId).append(html);

            var backgroundColors = allLabels.map(function() {
              return getRandomColor();
            });

            var chrt = document.getElementById("chartId_"+id).getContext("2d");
            var chartId = new Chart(chrt, {
              type: 'pie',
              data: {
                  labels: allLabels,
                  datasets: [{
                    label: "Nombre total d'éléments",
                    data: allData,
                    backgroundColor: backgroundColors,
                    hoverOffset: 5
                  }],
              },
              options: {
                  responsive: false,
                  legend: {
                    labels: {
                      fontColor: '#222', // Changez la couleur du texte ici
                    }
                  }
              },
            });
          }
        });

    } else {
      $(`#${id}`).removeClass("hide");
    }
  }

    function getSousZone(zoneId, level) {
      return new Promise(function(resolve, reject) {
        var params = {
            "zoneId": zoneId,
            "level" : level
        };

        ajaxPost(
            null,
            baseUrl + "/costum/cocity/souszone",
            params,
            function(respons) {
                resolve(respons);
            }
        );
      });
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function classDataForThematique(cityId, thematiques) {
      var dataParams = {
        "cityId" : cityId,
        "thematiques" : thematiques
      }
      ajaxPost(
        null,
        baseUrl + "/costum/cocity/classdatathematique",
        dataParams,
        function(responses) {
          var myDiv = document.getElementById("obsthema_city_content");
          $("#obsthema_city_content").html('');
          for (var key in responses) {
            if (responses.hasOwnProperty(key)) {
              var innerObject = responses[key];
              if(innerObject.somme > 0) {
                var html = `<div class="col-lg-2 col-sm-3 col-xs-6">
                            <div class="card-box bg-green">
                                <div class="inner">
                                    <h4>  ${(innerObject.somme).toLocaleString()}</h4>
                                    <p> ${capitalizeFirstLetter(key)} </p>
                                </div>
                                <div class="icon">
                                    <i class="fa ${innerObject.icon}" aria-hidden="true"></i>
                                </div>
                                <a href="#" class="card-box-footer"></a>
                            </div>
                          </div>`;
                $("#obsthema_city_content").append(html);
              }
            }
          }
          if(myDiv.innerHTML.trim() != "") { 
            $("#title_obsthema").removeClass("hide");
          } else {
            $("#title_obsthema").addClass("hide");
          }

        } 
      );

      setOptionsRegions();
    }

    var dropdownExist = [];
    function setOptionsRegions() {
      var dropdown = document.getElementById("regionDropdown");
      for (var countryId in allRegions) {
        if (allRegions.hasOwnProperty(countryId) && !dropdownExist.includes(countryId)) {
          dropdownExist.push(countryId);
          for (var regionId in allRegions[countryId]) {
            if (allRegions[countryId].hasOwnProperty(regionId)) {
              var region = allRegions[countryId][regionId];
              var name = region.name;

              var listItem = document.createElement("a");
              listItem.textContent = name;
              listItem.setAttribute("data-id", regionId);
              listItem.setAttribute("data-country", countryId);
              listItem.setAttribute("class", 'city_'+countryId+' region_'+regionId+' '+regionId);
              listItem.setAttribute("data-name", name);
              dropdown.appendChild(listItem);
            }
          }
        } else if(allIdSelected.includes(countryId)){
          $('.city_'+countryId).removeClass('hide');
        }
      }
    }

    function getRandomColor() {
      return '#' + Math.floor(Math.random() * 16777215).toString(16);
    }

    function setFilterActifs(selectedArray){
      const filtersContainer = $(".btn-select-filtre");
      var selectedIdConcat = allIdSelected.concat(allRegionIdSelected);
      if (Object.keys(selectedArray).length > 0) {
        const src = Object.entries(selectedArray).map(([key, value]) => {
          if(selectedIdConcat.includes(key))  return `<button style="margin-left: 5px" class="btn btn-success" data-field="${key}" data-check="${value.type}" data-country="${value.name}">
                        <i class="fa fa-times-circle delete_actif_filter" data-field="${key}" data-check="${value.type}" data-country="${value.name}"></i>
                        <span class="activeFilters">${value.name}</span>
                    </button>`;
        }).join("");
        const filterText = '&nbsp;&nbsp;&nbsp;<?php echo Yii::t('cms', "Active filter") ?> : &nbsp;&nbsp;';
        filtersContainer.html(filterText + src);
        $(".main-map").addClass("main-map-filter");
        $(".filter-btn-hide-map").addClass("hidenMap-filter");

        $(".delete_actif_filter").on("click", function() {
            const deletedFilter = $(this).attr("data-field");
            const deletedCountry = $(this).attr("data-country");
            const deletedType = $(this).attr("data-check");
           
            selectedIdConcat.remove(deletedFilter);
            $(`.delete_actif_filter[data-field="${deletedFilter}"]`).parent().remove();

            if(deletedType == 'country') {
              allIdSelected.remove(deletedFilter);
              $('.city_'+deletedFilter).addClass('hide'); // add class hide option region country
              getDataOneCity(allIdSelected);
            } else {
              allRegionIdSelected.remove(deletedFilter);
              setChartRegionSelected(allRegionIdSelected);
            }
            $(`#${deletedFilter}`).addClass("hide"); // add class hide pie chart country
            $(`.${deletedFilter}`).removeClass("hide"); // remove class hide option country

            if(selectedIdConcat.length == 0) {
              filtersContainer.html("");
              $(".main-map").removeClass("main-map-filter");
              $(".filter-btn-hide-map").removeClass("hidenMap-filter");
            };
        });
      } else {
          filtersContainer.html("");
      }
    }

    Array.prototype.remove = function(value) {
      var index = this.indexOf(value);
      if (index !== -1) {
          this.splice(index, 1);
      }
      return this;
    };

  var allRegionIdSelected = [];
  var dataRegionSelected = [];

  function setChartRegionSelected(regionId) {
    var params = {
      searchType: ["organizations", "poi", "projects", "classifieds", "citoyens", "events"],
      notSourceKey : true,
      filters: {},
      count: true,
      countType : ["organizations", "poi", "projects", "classifieds", "citoyens", "events"]
    }

    if(regionId.length > 0) {
      params.filters["address.level3"] = {$in: regionId};
      ajaxPost(
        null,
        baseUrl + "/" + moduleId + "/search/globalautocomplete",
        params,
        function(responses){
          var compte = responses.count;
          var html = '';
          for(var key in compte) {
            if(compte[key] > 0 && key != "spam" && key != "classifieds") html += `<div class="col-md-2 col-xs-6 datacitysection datacitysection_region">
                      <div class="card-counter ${colorClass[key]}">
                        <i class="fa ${icons[key]}"></i>
                        <span class="count-numbers">${compte[key]}</span>
                        <span class="count-name">${nameClass[key]}</span>
                      </div>
                    </div>`
          }
          $("#cumul-data-region").html(html);
          $("#data-regions-selected").removeClass('hide').hide().fadeIn();
        }
      )
      setBtnChartRegions(regionId);
    } else {
      $("#data-regions-selected").addClass('hide');
    }
  }
  var allDataRegionBarChart = [];

  function setBtnChartRegions(regionId) {
    var dataD3Chart = {};

    var promises = regionId.map(async function(id) {

      var latest = await getSousZone(id, '4');
      // allRegions[id]= latest;

      if(!allDataRegionBarChart[dataRegionSelected[id]]) {
        var paramsFilters = {
          searchType: ["organizations", "poi", "projects", "citoyens", "events"],
          notSourceKey: true,
          filters: {
              "address.level3": id,
          },
          count: true,
          countType: ["organizations", "poi", "projects", "citoyens", "events"],
          onlyCount: true
        };

        if(latest != null && latest != "") setPieChartRegion(latest, id, 'pieChart_region', 'title_pie_chart_region', dataRegionSelected);

        return new Promise(function(resolve, reject) {
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                paramsFilters,
                function(results) {
                    dataD3Chart[dataRegionSelected[id]] = results.count;
                    allDataRegionBarChart[dataRegionSelected[id]] = results.count;
                    resolve();
                }
            );
        });
      } else {

        if(latest != null && latest != "") setPieChartRegion(latest, id, 'pieChart_region', 'title_pie_chart_region', dataRegionSelected);

        return new Promise(function(resolve, reject) {
          dataD3Chart[dataRegionSelected[id]] = allDataRegionBarChart[dataRegionSelected[id]];
          resolve();
        })
      }
    });

    Promise.all(promises)
      .then(function() {
        setChartCity(dataD3Chart,'myChart-region');
      });
  }

  $(document).ready(function() {
    setOptions();

    if(!costum.editMode) $(".div_search_elements").css("position", "fixed");
    
    // Pays
    var dropdownContentPays = $(".dropdown-content-pays");
    $(".search-input-pays").on("click", function(event) {
        event.stopPropagation();
        dropdownContentPays.addClass("show");
    });
    $(document).on("click", function(event) {
        if (!$(event.target).closest(".dropdown").length) {
            dropdownContentPays.removeClass("show");
        }
    });
    $("#searchInputPays").on("input", function() {
      var filter = $(this).val().toUpperCase();
      $("#countryDropdown a").each(function() {
          var txtValue = $(this).text().toUpperCase();
          $(this).toggle(txtValue.indexOf(filter) > -1);
      });
    });
    $("#countryDropdown").on('click', 'a', function(){
      var selectedId = $(this).data("id");
      var selectedName = $(this).data("name");
      dataCitySelecter[selectedId] = selectedName;
      selectedFiltersActifs[selectedId] = {
        'name' : selectedName,
        'type' : 'country'
      };
      if(!allIdSelected.includes(selectedId)) allIdSelected.push(selectedId);
      setFilterActifs(selectedFiltersActifs);
      getDataOneCity(allIdSelected);
      dropdownContentPays.removeClass("show");
      $(this).addClass('hide');
      $(".search-input-pays").val("");
    })

    // Regions
    var dropdownContentRegions = $(".dropdown-content-regions");
    $(".search-input-regions").on("click", function(event) {
        event.stopPropagation();
        dropdownContentRegions.addClass("show");
    });
    $(document).on("click", function(event) {
        if (!$(event.target).closest(".dropdown").length) {
            dropdownContentRegions.removeClass("show");
        }
    });
    $("#searchInputRegions").on("input", function() {
      var filter = $(this).val().toUpperCase();
      $("#regionDropdown a").each(function() {
          var txtValue = $(this).text().toUpperCase();
          $(this).toggle(txtValue.indexOf(filter) > -1);
      });
    });

    $("#regionDropdown").on('click', 'a', function(){
      var selectedId = $(this).data('id');
      var selectedName = $(this).data('name');
      var selectedCountry = $(this).data('country');
      allRegionIdSelected.push(selectedId);
      dataRegionSelected[selectedId] = selectedName;
      selectedFiltersActifs[selectedId] = {
        'name' : selectedName,
        'type' : 'region'
      };;
      setFilterActifs(selectedFiltersActifs);
      setChartRegionSelected(allRegionIdSelected);
      dropdownContentRegions.removeClass('show');
      $(this).addClass('hide');
    });

    $("#showMap").on('click', function(){
      isMap = true;
      $(".list-affiche").addClass('hide').fadeOut();
      $(".map-affiche").removeClass('hide').hide().fadeIn();
      observaMap.clearMap();
      observaMap.addElts(dataMaps);
    });

    $("#hidenMap").on('click', function(){
      isMap = false;
      $(".map-affiche").addClass('hide').fadeOut();
      $(".list-affiche").removeClass('hide').hide().fadeIn();
    });
  });
</script>