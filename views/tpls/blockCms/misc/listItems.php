<?php  
$keyTpl = "listItems";
$paramsData=[
	/*"title"=>"",
	"description" => "",*/
	"bgDate"=>"#a552a7",
	"background"=>"#f5f5f5"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
};

?>

<style type="text/css">
	.event-list_<?=$kunik?> {
		list-style: none;
		margin: 20px;
		padding: 0px;
	}
	.event-list_<?=$kunik?> > li {
		background-color: #fff;
		box-shadow: 0px 0px 5px rgb(51, 51, 51);
		box-shadow: 0px 0px 5px rgba(51, 51, 51, 0.7);
		padding: 0px;
		margin: 0px 0px 20px;
	}
	.event-list_<?=$kunik?> > li > time {
		display: inline-block;
		width: 100%;
		color: #fff;
		background-color: <?= $paramsData["bgDate"]?>/*#c52c66*/;
		padding: 5px;
		text-align: center;
		text-transform: uppercase;
	}
	.event-list_<?=$kunik?> > li:nth-child(even) > time {
		background-color: <?= $paramsData["bgDate"]?>;
	}
	.event-list_<?=$kunik?> > li > time > span {
		display: none;
	}
	.event-list_<?=$kunik?> > li > time > .day {
		display: block;
		font-size: 56pt;
		font-weight: 100;
		line-height: 1;
	}
	.event-list_<?=$kunik?> > li time > .month {
		display: block;
		font-size: 24pt;
		font-weight: 900;
		line-height: 1;
	}
	.event-list_<?=$kunik?> > li > img {
		width: 100%;
	}
	.event-list_<?=$kunik?> > li > .info {
		padding-top: 5px;
		text-align: center;
	}
	.event-list_<?=$kunik?> > li > .info > .title {
		font-size: 17pt;
		font-weight: 700;
		margin: 0px;
	}
	.event-list_<?=$kunik?> > li > .info > .desc {
		font-size: 13pt;
		font-weight: 300;
		margin: 0px;
	}
	.event-list_<?=$kunik?> > li > .info > ul,
	.event-list_<?=$kunik?> > li > .social > ul {
		display: table;
		list-style: none;
		margin: 10px 0px 0px;
		padding: 0px;
		width: 100%;
		text-align: center;
	}
	.event-list_<?=$kunik?> > li > .social > ul {
		margin: 0px;
	}
	.event-list_<?=$kunik?> > li > .info > ul > li,
	.event-list_<?=$kunik?> > li > .social > ul > li {
		display: table-cell;
		cursor: pointer;
		color: #1e1e1e;
		font-size: 11pt;
		font-weight: 300;
        padding: 3px 0px;
	}
    .event-list_<?=$kunik?> > li > .info > ul > li > a {
		display: block;
		width: 100%;
		color: #1e1e1e;
		text-decoration: none;
	} 
    .event-list_<?=$kunik?> > li > .social > ul > li {    
        padding: 0px;
    }
    .event-list_<?=$kunik?> > li > .social > ul > li > a {
        padding: 3px 0px;
	} 
	.event-list_<?=$kunik?> > li > .info > ul > li:hover,
	.event-list_<?=$kunik?> > li > .social > ul > li:hover {
		color: #1e1e1e;
		background-color: #c8c8c8;
	}


	@media (min-width: 768px) {
		.event-list_<?=$kunik?> > li {
			position: relative;
			display: block;
			width: 100%;
			height: 120px;
			padding: 0px;
		}
		.event-list_<?=$kunik?> > li > time,
		.event-list_<?=$kunik?> > li > img  {
			display: inline-block;
		}
		.event-list_<?=$kunik?> > li > time,
		.event-list_<?=$kunik?> > li > img {
			width: 120px;
			float: left;
		}
		.event-list_<?=$kunik?> > li > .info {
			background-color: <?= $paramsData["background"]?>;
			overflow: hidden;
		}
		.event-list_<?=$kunik?> > li > time,
		.event-list_<?=$kunik?> > li > img {
			width: 120px;
			height: 120px;
			padding: 0px;
			margin: 0px;
		}
		.event-list_<?=$kunik?> > li > .info {
			position: relative;
			height: 120px;
			text-align: left;
			/*padding-right: 40px;*/
		}	
		.event-list_<?=$kunik?> > li > .info > .title, 
		.event-list_<?=$kunik?> > li > .info > .desc {
			padding: 0px 10px;
		}
		.event-list_<?=$kunik?> > li > .info > ul {
			position: absolute;
			left: 0px;
			bottom: 0px;
		}
		.event-list_<?=$kunik?> > li > .social {
			position: absolute;
			top: 0px;
			right: 0px;
			display: block;
			width: 40px;
		}
        .event-list_<?=$kunik?> > li > .social > ul {
            border-left: 1px solid #e6e6e6;
        }
		.event-list_<?=$kunik?> > li > .social > ul > li {			
			display: block;
            padding: 0px;
		}
		.event-list_<?=$kunik?> > li > .social > ul > li > a {
			display: block;
			width: 40px;
			padding: 10px 0px 9px;
		}
	}
</style>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
		<ul class="event-list_<?=$kunik?>">
			

		</ul>
	</div>
</div>


<div class="text-center">
	
	<button id="add-event" onclick="dyFObj.openForm('event')" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i> Add data</button>
</div>

<script type="text/javascript">
    
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    jQuery(document).ready(function() {

        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer votre section",
                "description" : "Personnaliser votre section1",
                "icon" : "fa-cog",
                "properties" : {
                    "bgDate":{
                        label : "Couleur du background date",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.bgDate
                    },
                    "background":{
                        label : "Couleur du background",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.background
                    }
                },
                save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) { 
                            $("#ajax-modal").modal('hide');
                            toastr.success("élement mis à jour");
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        } );
                    }
                }
            }

        };
        mylog.log("paramsData",sectionDyf);
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
        });

        /**************************** list event**************************/    
        $.ajax({
           url : baseUrl+"/costum/blockcms/geteventaction",
           type : 'POST', 
           data : {
            sourceKey : contextSlug,
           	collection:"events"
          },
           dataType : "json",
           success:function(data){
              mylog.log("blockcms_event",data);
              var html = "";
              $.each(data, function( index, value ) {

                html += 
                  	'<li>'+
						'<time datetime="2014-07-20">'+
							'<span class="day">4</span>'+
							'<span class="month">Jul</span>'+
							'<span class="year">2014</span>'+
							'<span class="time">ALL DAY</span>'+
						'</time>'+
						'<img alt="" src="'+value.profilMediumImageUrl+'"/>'+
						'<div class="info">'+
							'<h2 class="title">'+value.name+'</h2>'+
							'<p class="desc">'+value.shortDescription+'</p>';
							/*setTimeout(function(){
								$('.desc').after('<p>'+directory.getDateFormated(value)+'</p>');
							}, 2000);*/
							
				html += '</div>'+
					'</li>';

              });
              $(".event-list_<?=$kunik?>").html(html);
           }
        });
        /****************************end list event**********************/

    });


</script>
