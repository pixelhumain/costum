<?php  
$keyTpl = "socialNetwork";
$paramsData=[
	"iconColor"=>"#fff",
	"colorSocial" => "#D3D3D3",
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
};

?>
<style type="text/css">

	/* footer social icons */
	ul.social-network {
		list-style: none;
		display: inline;
		margin-left:0 !important;
		padding: 0;
	}
	ul.social-network li {
		display: inline;
		margin: 0 5px;
	}

	/* footer social icons */
	.social-network a.icoRss:hover {
		background-color: #F56505;
	}
	.social-network a.icoFacebook:hover {
		background-color:#3B5998;
	}
	.social-network a.icoTwitter:hover {
		background-color:#33ccff;
	}
	.social-network a.icoGoogle:hover {
		background-color:#BD3518;
	}
	.social-network a.icoVimeo:hover {
		background-color:#0590B8;
	}
	.social-network a.icoLinkedin:hover {
		background-color:#007bb7;
	}
	.social-network a.icoRss:hover i, .social-network a.icoFacebook:hover i, .social-network a.icoTwitter:hover i,
	.social-network a.icoGoogle:hover i, .social-network a.icoVimeo:hover i, .social-network a.icoLinkedin:hover i {
		color:#fff;
	}
	a.socialIcon:hover, .socialHoverClass {
		color:#44BCDD;
	}

	.social-circle li a {
		display:inline-block;
		position:relative;
		margin:0 auto 0 auto;
		-moz-border-radius:50%;
		-webkit-border-radius:50%;
		border-radius:50%;
		text-align:center;
		width: 50px;
		height: 50px;
		font-size:20px;
	}
	.social-circle li i {
		margin:0;
		line-height:50px;
		text-align: center;
	}

	.social-circle li a:hover i, .triggeredHover {
		-moz-transform: rotate(360deg);
		-webkit-transform: rotate(360deg);
		-ms--transform: rotate(360deg);
		transform: rotate(360deg);
		-webkit-transition: all 0.2s;
		-moz-transition: all 0.2s;
		-o-transition: all 0.2s;
		-ms-transition: all 0.2s;
		transition: all 0.2s;
	}
	.social-circle i {
		color: <?= $paramsData["iconColor"]?>;
		-webkit-transition: all 0.8s;
		-moz-transition: all 0.8s;
		-o-transition: all 0.8s;
		-ms-transition: all 0.8s;
		transition: all 0.8s;
	}
	.social-network li a {
	 	background-color: <?= $paramsData["colorSocial"]?>;   
	}
</style>

<div class="col-md-12 text-center margin-20">
    <ul class="social-network social-circle">
        <li class="network">
        	<a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a>
        </li>
        <li class="network">
        	<a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a>
        </li>
        <li class="network">
        	<a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a>
        </li>
    </ul>	

   <button id="add-social" onclick="dyFObj.openForm('project')" class="btn btn success">Add data</button>			
</div>


<script type="text/javascript">
 
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    jQuery(document).ready(function() {

        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer votre section",
                "description" : "Personnaliser votre section1",
                "icon" : "fa-cog",
                "properties" : {
                    "colorSocial" : {
                        label : "background color",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.colorSocial
                    },
                    "iconColor" : {
                        label : "Couleur de l'icone",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.iconColor
                    }
                },
                save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) { 
                            $("#ajax-modal").modal('hide');
                            toastr.success("élement mis à jour");
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        } );
                    }
                }
            }

        };
        mylog.log("paramsData",sectionDyf);
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
        });
    });


</script>