<?php 

	$keyTpl = "TLCategoryTags";
	
	$paramsData = [
		"titleCategoryTags" => "Voici les caractéristiques du Tiers Lieux",
		"subTitleCategoryTags" => "vous pouvez cliquer pour voir les projets ou événements associés",
		"thematicPreference" => [],
		"elementsType" => ["projects", "events"],
	];

	if(isset($blockCms)){
  		foreach($paramsData as $e => $v) {
    		if( isset($blockCms[$e]) ) {
      			$paramsData[$e] = $blockCms[$e];
    		}
  		}
	}
?>

<style type="text/css">
	#categoryTags<?= $kunik ?>{
text-align:center;
	} 
	.catag<?= $kunik ?>{
		width: 100%;
		height: 120px;
		border-radius: 10px;
		border:  4px solid;
		margin-bottom: 10px;
		display: block;
		cursor: pointer;
		text-transform: uppercase;
		font-size: 11pt;
		font-weight: bold;
		display: table;
	}

	.catag<?= $kunik ?>:hover, .result<?= $kunik ?>:hover{
		text-decoration: none;
	}

	.catag<?= $kunik ?> span {
	  	line-height: normal;
	  	display: table-cell;
    	vertical-align: middle;
    	float: none;
	}

	.result<?= $kunik ?>{
		width: 100%;
		border:  4px solid;
		margin-bottom: 10px;
		display: block;
		cursor: pointer;
		text-transform: uppercase;
		font-size: 11pt;
		font-weight: bold;
		display: table;
		padding-top: 40px !important;
	}

	.badge<?= $kunik ?>{
		position: absolute;
		top: 4px;
		left: 15px;
	}

</style>

<div class="container-fluid">
	<h3 class="sp-text" data-id="<?= $blockKey ?>" data-field="titleCategoryTags"><?= $paramsData["titleCategoryTags"] ?></h3>
	<div class="sp-text" data-id="<?= $blockKey ?>" data-field="subTitleCategoryTags"><?= $paramsData["subTitleCategoryTags"] ?></div>
	<div id="categoryTags<?= $kunik ?>" class="row padding-0"></div>
	<div id="results<?= $kunik ?>" class="row padding-0 margin-top-20"></div>
</div>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	jQuery(document).ready(function(){
		let activeFilterTags = [];

		let isConfigured = (sectionDyf.<?php echo $kunik ?>ParamsData.thematicPreference!=0);
		let categoriesListe = [];
		if(isConfigured){
			categoriesListe = sectionDyf.<?php echo $kunik ?>ParamsData.thematicPreference;
		}else if(typeof costum[costum.contextSlug].categoriesTL != "undefined"){
			categoriesListe = costum[costum.contextSlug].categoriesTL;
		}

		$.each(categoriesListe, function(index, categ){
			if(!isConfigured){
				sectionDyf.<?php echo $kunik ?>ParamsData.thematicPreference.push({color:"#FADE37", label:categ});
			}

			$("#categoryTags<?= $kunik ?>").append(`
				<div class="col-md-2 col-sm-4">
					<a class="catag<?= $kunik ?> text-center padding-10" data-filters="${categ.label||categ}" type="button" data-color="${categ.color||"#FADE37"}" style="border-color:${categ.color || "#FADE37"}">
						<span>${categ.rename||categ.label||categ}</span>
					</a>
				</div>
			`);
		})
		
		$(document).on("click", ".catag<?= $kunik ?>", function(){
            if(activeFilterTags.indexOf($(this).data("filters")) !== -1){
                $(this).css("background-color","transparent");
                $(this).css("color","black");
                activeFilterTags = activeFilterTags.filter((tag, index) => {
                	//alert(index+" "+tag) ;
                	return (tag!=$(this).data("filters"))
                });
            }else{
                $(this).css("background-color", $(this).data("color"));
                $(this).css("color", "white");
                activeFilterTags.push($(this).data("filters"))
            }
            getProjectsAndEventByTag(activeFilterTags, $(this).data("color"));
        });

        /*$(".catag<?= $kunik ?>").off().on("mouseover",function(){
            $(this).css("background-color", "#FADE37");
            $(this).css("color", "white");
        });

        $(".catag<?= $kunik ?>").off().on("mouseleave",function(){
            $(this).css("background-color", "transparent");
            $(this).css("color", "black");
        });*/

        function getProjectsAndEventByTag(tags, color){
        	params = {
                searchType : <?= json_encode($paramsData["elementsType"]) ?>,
                fields : ["name","tags", "type"],
                indexStep: 6
            };

	        if(tags.length!=0){
	            params["searchTags"] = tags;

	        ajaxPost(
	            null,
	            baseUrl+"/" + moduleId + "/search/globalautocomplete",
	            params,
	            function(data){
	            	$("#results<?= $kunik ?>").empty();
	                if(data.results){
	                	$.each(data.results, function(index, element){
	                		//alert(JSON.stringify(element));
	                		$("#results<?= $kunik ?>").append(`
								<div class="col-md-4">
									<a href='${"#page.type.events.id.62bac337569ba011bb721133"}' class="lbh-preview-element result<?= $kunik ?> text-center padding-10 padding-top-20 type="button" data-color="${color||"#FADE37"}" style="background-color:${color||"#FADE37"}">
										<span class="padding-5 text-white bg-dark badge<?= $kunik ?>">${trad[element.collection]}</span>
										<div>${element.name}</div>
									</a>
								</div>
							`)
	                	})
	                }
	            }
	        )
	    }else{
	    	$("#results<?= $kunik ?>").empty();
	    }
        }

        /*********dynform*******************/
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            "properties" : {
                "elementsType":{
                	"inputType" : "selectMultiple",
                    "label" : "Elément du map",
                    values : sectionDyf.<?php echo $kunik ?>ParamsData.elementsType,
                    "class" : "multi-select select2 form-control",
                    "options" : {
                        "events" : trad.events,
                        "projects" : trad.projects,
                        "poi" : trad.poi
                    }
                },
                "thematicPreference":{
                    "label" : "Préférence sur les thématique à afficher",
                    "inputType" : "lists",
                    "entries" : {
                        "color" : {
                            "type" : "colorpicker",
                            "label": "Code Couleur",
                            "class": "col-md-2",
                            "placeholder":"Couleur"
                        },
                        "label" : {
                            "type" : "text",
                            "label": "Libellé",
                            "class": "col-md-4",
                            "placeholder":"Libellé"
                        },
                        "rename" : {
                            "type" : "text",
                            "label": "Renomé",
                            "class": "col-md-4",
                            "placeholder":"Renomé"
                        }
                    }
                }
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                tplCtx.value[k] = $("#"+k).val();
                if (k == "thematicPreference"){
                    tplCtx.value[k] = [];
                    $.each(data.thematicPreference, function(index, va){
                        tplCtx.value[k].push(va);
                    })
                }
              });
              if(typeof tplCtx.value == "undefined"){
                toastr.error('value cannot be empty!');
              }else {
                  dataHelper.path2Value(tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }
            }
          }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
	});
</script>