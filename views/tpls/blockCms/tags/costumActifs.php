
<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<style type="text/css">
	.listCostum<?= $kunik?> {
		margin-bottom: 20px;
		margin-bottom: 20px;
	}
	.listCostum<?= $kunik?> .leaflet-popup-content-wrapper {
		border: 3px solid #005e6f;
	}
	.listCostum<?= $kunik?> .popup-address{
		margin-top: 7px;
		font-size: 16px;
	}
	.listCostum<?= $kunik?> .btn-more {
		border-color: #005e6f !important;
		color: #005e6f !important;
	}
	.listCostum<?= $kunik?> .btn-more:hover {
		background-color: #005e6f !important;
		color: white !important;
	}

	.listCostum<?= $kunik?> .leaflet-container a.leaflet-popup-close-button {
		color: #005e6f !important;
	}
	#thematic<?= $kunik ?>{
        background: transparent;
        padding: 0.3em;
    }

    #thematic<?= $kunik ?> .btn-white{
        color: black;
        background: gainsboro;
        font-weight: 900 !important;
    }
    #thematic<?= $kunik ?> .m-1{
        margin: 0.3em;
    }
    #thematic<?= $kunik ?> .btn-active {
    	color: white;
    	background-color: #052434;
    	border-color: #052434;
    	font-weight: 700;
    }
	.costumActif_<?= $kunik?> .menuRight {
		position: absolute;
	}

	.costumActif_<?= $kunik?> .leaflet-popup-content {
		width: 180px !important;
	}

	.costumActif_<?= $kunik?> .popup-count-element t {
		margin-right: 12px;
		margin-left: 3px;
	}

</style>
<?php if($costum["typeCocity"] == "epci" || $costum["typeCocity"] == "district"){ 
	$textTitle = $el['address']['addressCountry'] == 'FR' ? "Les Cocity actifs dans l'EPCI de ".$el['address']['level5Name'] : "Les Cocity actifs dans le district ".$el['address']['level4Name'];
	?>
	<div class=" costumActif_<?= $kunik?> text-center">
		<div class="">
			<div class="row">
				<h3 class="title-cocity-actif text-center"><?= $textTitle ?></h3>
				<div class="main_<?= $kunik?> col-md-12 col-sm-12 col-xs-12 vertical" >
					<div class="col-xs-12 bodySearchContainer  ">
						<!-- <p class="text-left" style="margin-left: 50px;font-size: 15px; margin-top: -25px; margin-bottom: -5px;">Filtrer par tags : </p> -->
						<div id="thematic<?= $kunik ?>"></div>
						<div id="listCostum" class="no-padding col-xs-12 listCostum<?= $kunik?>"  style="height: 500px; position: relative;" >

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var countsData = {};
		var elCostum = <?= json_encode($el) ?>;
		costum.level5AddressCity = elCostum.address.level5;
		function removeParams(sParam){
			var url = window.location.href.split('?')[0]+'?';
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;
			
			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');
				if (sParameterName[0] != sParam) {
					url = url + sParameterName[0] + '=' + sParameterName[1] + '&'
				}
			}
			return url.substring(0,url.length-1);
		}

		sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $blockCms ); ?>;
		var map<?= $kunik ?> = new CoMap({
			container : ".listCostum<?= $kunik?>",
			activePopUp : true,
			mapOpt:{
				menuRight : true,
				btnHide : false,
				doubleClick : true,
				zoom : 2,
			},
			mapCustom:{
				tile : 'maptiler',
				getPopup: function(data){
					var popup = "";
					var member = "links.memberOf."+data._id["$id"];
					var criterFilters = {};
					criterFilters["$or"] = {
						"source.keys" : data.slug,
						["links.memberOf."+data._id["$id"]] : true
					};
					var params = {
						"searchType" : ["organizations","citoyens", "projects", "poi", "events"],
						"notSourceKey": true,
						filters : criterFilters,
						"count" : true,
						"countType" : ["organizations","citoyens", "projects", "poi", "events"]
					};
					ajaxPost(
						null,
						baseUrl+"/" + moduleId + "/search/globalautocomplete",
						params,
						function(results){
							var countsData = results.count;
							var id = data._id ? data._id.$id:data.id;
							var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
							if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
								imgProfil = baseUrl + data.profilThumbImageUrl;
							else
								imgProfil = modules.map.assets + "/images/thumb/default_" + data.collection + ".png";
							//var imgProfil = map<?= $kunik ?>.mapCustom.getThumbProfil(data);

							var eltName = data.title ? data.title:data.name;
							
							popup += "<div class='padding-5' id='popup" + id + "'>";
							popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
							popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

							if(data.address){
								var addressStr="";
								if(data.address.streetAddress) 
									addressStr += data.address.streetAddress;
								if(data.address.postalCode)
									addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
								if(data.address.addressLocality)
									addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
								popup += "<div class='popup-address text-dark' style='font-size: 10pt; margin-left: 28%; margin-top: -5%;'>";
								popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
								popup += "</div>";
							}
							popup += "<div class='popup-count-element text-dark' style='margin-top: 10%; font-size: 12pt; cursor:pointer'>";
								countsData["organizations"] != 0 ? popup += "<i class='fa fa-group' title='Organisation'></i><t title='Organisation'>"+countsData["organizations"]+"</t>" : "";
								countsData["poi"] != 0 ? popup += "<i class='fa fa-rocket' title='POI'></i> <t title='POI'>"+countsData["poi"]+"</t>" : "";
								countsData["projects"] != 0 ? popup += "<i class='fa fa-lightbulb-o' title='Projet'></i> <t title='Projet'>"+countsData["projects"]+"</t>" : "";
								countsData["events"] != 0 ? popup += "<i class='fa fa-calendar' title='Evenement'></i> <t title='Evenement'>"+countsData["events"]+"</t>" : "";
							popup += "</div>";
							if(data.tags && data.tags.length > 0){
								popup += "<div style='margin-top : 5px;'>";
								var totalTags = 0;
								$.each(data.tags, function(index, value){
									totalTags++;
									// if (totalTags < 3) {
										popup += "<div class='popup-tags'>#" + value + " </div>";
									// }
								})
								popup += "</div>";
							}
							if(data.shortDescription && data.shortDescription != ""){
								popup += "<div class='popup-section'>";
								popup += "<div class='popup-subtitle'>Description</div>";
								popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
								popup += "</div>";
							}
							if((data.url && typeof data.url == "string") || data.email || data.telephone){
								popup += "<div id='pop-contacts' class='popup-section'>";
								popup += "<div class='popup-subtitle'>Contacts</div>";

								if(data.url && typeof data.url === "string"){
									popup += "<div class='popup-info-profil'>";
									popup += "<i class='fa fa fa-desktop fa_url'></i> ";
									popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
									popup += "</div>";
								}

								if(data.email){
									popup += "<div class='popup-info-profil'>";
									popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
									popup += "</div>";
								}

								if(data.telephone){
									popup += "<div class='popup-info-profil'>";
									popup += "<i class='fa fa-phone fa_phone'></i> ";
									var tel = ["fixe", "mobile"];
									var iT = 0;
									$.each(tel, function(keyT, valT){
										if(data.telephone[valT]){
											$.each(data.telephone[valT], function(keyN, valN){
												if(iT > 0)
													popup += ", ";
												popup += valN;
												iT++; 
											})
										}
									})
									popup += "</>";
								}

								popup += "</div>";
								popup += "</div>";
							}
							var url = baseUrl+'/costum/co/index/slug/'+data.slug;
							popup += "<div class='popup-section'>";
							popup += "<a href='" + url + "' target='_blank' class=' item_map_list popup-marker' id='popup" + id + "'>";
							popup += '<div class="btn btn-sm btn-more col-md-12">';
							popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
							popup += '</div></a>';
							popup += '</div>';
							popup += '</div>';
						},null,null, {async: false}
					);

					return popup;
				},
				markers: {
					getMarker : function(data){
						var imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/cocity/smarterritoire.png";	
						return imgM;
					}
				}
			},
			
			elts : []
		});
		var blocCostumObj<?= $kunik?> ={
			costumList:function(criteriaParams = null){
				var citiesArray = <?php echo json_encode($cities["cities"]); ?>;
				var params = {
					"searchType" : ["organizations", "poi", "projects", "citoyens", "classifieds", "events"],
					"notSourceKey": true,
					filters : {
						"costum.cocity" : {'$exists': true},
						"costum.typeCocity" : "ville",
						"address.localityId" : {$in: citiesArray}
					},
					"count" : true,
					"countType" : ["organizations", "citoyens", "projects", "poi", "events", "classifieds"]
				}
				if(criteriaParams!=null && criteriaParams!=""){
					params.filters["tags"] = criteriaParams.split(",");
				}
				ajaxPost(
					null,
					baseUrl+"/" + moduleId + "/search/globalautocomplete",
					params,
					function(data){
						map<?= $kunik ?>.clearMap();
						map<?= $kunik ?>.addElts(data.results);
					}
				);
			}
		}    
		var filteredTheme = "";
		jQuery(document).ready(function() {	
			if (getUrlParameter('map')=="scroll") {
				$('html, body').animate(
					{ scrollTop: $("#cocityactif").offset().top - $("#mainNav").height() },
					800,
					() => {
					},
				);
				window.history.pushState('',document.title,removeParams("map"));
			}

			blocCostumObj<?= $kunik?>.costumList();

			/********* Filter thematics *******************/
			var thematic = [];

			if(costum && costum.lists && costum.tagsFilters && costum.tagsFilters.theme){

				for (const [keyT, valueT] of Object.entries(costum.tagsFilters.theme)) {
					var asTags = [];
					let labelT = "";

					if(typeof valueT == "string"){
						asTags.push(valueT);
						labelT = valueT;
					}else{
						labelT = keyT;
						for (const [keyChild, valueChild] of Object.entries(valueT)){
							if(!thematic.includes(valueChild)){
								thematic.push(valueChild);
							}
							asTags.push(valueChild);
						}
					}
					$("#thematic<?= $kunik ?>").append('<a type="button" class="btn btn-white m-1 rounded<?= $kunik ?> thematic<?= $kunik ?>" data-filters="'+asTags.join(',')+'">'+labelT+'<!--span class="badge">0</span--></a>');
				}   
			}else{
				$("#thematic<?= $kunik ?>").remove();
			}
			$(".thematic<?= $kunik ?>").off().on("click",function(){
				if(filteredTheme.indexOf($(this).data("filters")) !== -1){
					filteredTheme = filteredTheme.replace($(this).data("filters"), "");
					$(this).removeClass("btn-active");
					$(this).addClass("btn-white");
				}else{
					$(this).removeClass("btn-white");
					$(this).addClass("btn-active");
					if(filteredTheme==""){
						filteredTheme = $(this).data("filters");
					}else{
						filteredTheme+=","+$(this).data("filters");
					}
					
				}

				if(filteredTheme.charAt(0)==","){
					filteredTheme = filteredTheme.substring(1);
				}

				blocCostumObj<?= $kunik?>.costumList(filteredTheme);
			});
		})	
	</script>
<?php } ?>