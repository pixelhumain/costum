
<?php 
$keyTpl = "teamwithdesc";
$content = [];
if(isset($blockCms["content"])) {
	$content = $blockCms["content"];
}
$paramsData = [
    "titleColor" => "#108d6f",
    "background_color" => "#eee"   
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

$title = ($blockCms["title"] ?? ["fr" => "NOTRE TEAM"]);
$title = is_array($title) ? $title : ["fr" => $title];
  
$paramsData["title"] = $title[$costum['langCostumActive']] ?? reset($title);

?>


<!-- ****************get image uploaded************** -->
<?php 
  $blockKey = (string)$blockCms["_id"];
 ?>
<style type="text/css">  
    #team<?php echo $kunik ?> {
        background: <?php echo $paramsData["background_color"]; ?> !important;
    }

    #team<?php echo $kunik ?> .btn-primary:hover,
    #team<?php echo $kunik ?> .btn-primary:focus {
        background-color: <?php echo $paramsData["titleColor"]; ?>;
        border-color: <?php echo $paramsData["titleColor"]; ?>;
        box-shadow: none;
        outline: none;
    }

    #team<?php echo $kunik ?> .btn-primary {
        color: #fff!important;
        background-color: <?php echo $paramsData["titleColor"]; ?>;
        border-color: <?php echo $paramsData["titleColor"]; ?>;
    }

    section#team<?php echo $kunik ?>  {
        padding: 25px 0;
    }

    section .section-title {
        text-align: center;
        color: <?php echo $paramsData["titleColor"]; ?>;
        margin-bottom: 50px;
        text-transform: uppercase;
    }

    #team<?php echo $kunik ?> .card {
        border: none;
        background: #ffffff;
        padding: 5px;
    }

    #team<?php echo $kunik ?> .image-flip:hover .backside,
    #team<?php echo $kunik ?> .image-flip.hover .backside {
        -webkit-transform: rotateY(0deg);
        -moz-transform: rotateY(0deg);
        -o-transform: rotateY(0deg);
        -ms-transform: rotateY(0deg);
        transform: rotateY(0deg);
        border-radius: .25rem;
    }

    #team<?php echo $kunik ?> .image-flip:hover .frontside,
    #team<?php echo $kunik ?> .image-flip.hover .frontside {
        -webkit-transform: rotateY(180deg);
        -moz-transform: rotateY(180deg);
        -o-transform: rotateY(180deg);
        transform: rotateY(180deg);
    }

    #team<?php echo $kunik ?> .mainflip {
        -webkit-transition: 1s;
        -webkit-transform-style: preserve-3d;
        -ms-transition: 1s;
        -moz-transition: 1s;
        -moz-transform: perspective(1000px);
        -moz-transform-style: preserve-3d;
        -ms-transform-style: preserve-3d;
        transition: 1s;
        transform-style: preserve-3d;
        position: relative;
    }

    #team<?php echo $kunik ?> .frontside {
        position: relative;
        -webkit-transform: rotateY(0deg);
        -ms-transform: rotateY(0deg);
        z-index: 2;
        margin-bottom: 30px;
    }

    #team<?php echo $kunik ?> .backside {
        position: absolute;
        top: 0;
        left: 0;
        background: white;
        -webkit-transform: rotateY(-180deg);
        -moz-transform: rotateY(-180deg);
        -o-transform: rotateY(-180deg);
        -ms-transform: rotateY(-180deg);
        transform: rotateY(-180deg);
        -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
    }

    #team<?php echo $kunik ?> .frontside,
    #team<?php echo $kunik ?> .backside {
        -webkit-backface-visibility: hidden;
        -moz-backface-visibility: hidden;
        -ms-backface-visibility: hidden;
        backface-visibility: hidden;
        -webkit-transition: 1s;
        -webkit-transform-style: preserve-3d;
        -moz-transition: 1s;
        -moz-transform-style: preserve-3d;
        -o-transition: 1s;
        -o-transform-style: preserve-3d;
        -ms-transition: 1s;
        -ms-transform-style: preserve-3d;
        transition: 1s;
        transform-style: preserve-3d;
    }

    #team<?php echo $kunik ?> .frontside .card,
    .backside .card {
        min-height: 312px;
        min-width: 280px;
    }

    #team<?php echo $kunik ?> .backside .card a {
        font-size: 18px;
        color: #fff !important;
    }

    #team<?php echo $kunik ?> .frontside .card .card-title,
    .backside .card .card-title {
        color: <?php echo $paramsData["titleColor"]; ?> !important;
        text-transform: none;
        font-weight: 500;
    }

    #team<?php echo $kunik ?> .frontside .card .card-body img {
        width: 160px;
        height: 160px;
        border-radius: 50%;
    }
    </style>
  


<!-- Team -->
<section id="team<?php echo $kunik ?>" class="pb-5">
    <div class="container">
        <h5 class="section-title title sp-text img-text-bloc title<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"></h5>
        <div class="row">
            <!-- Team member -->
         <?php 
        if (isset($content)) {
            foreach ($content as $key => $value) {?>   
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <?php  
                                    $initFiles = Document::getListDocumentsWhere(
                                        array(
                                          "id"=> $blockKey,
                                          "type"=>'cms',
                                          "subKey"=> (string)$key 
                                        ), "image"
                                    );
                                    $arrFile = [];
                                    //var_dump($initFiles);
                                    foreach ($initFiles as $k => $v) {
                                        $arrFile[] =$v['imagePath'];
                                    }
                                ?> 
                                <div class="card-body text-center">
                                    <p>
                                        <?php if (!empty($arrFile[0])){ ?>
                                            <img class=" img-fluid" src=" <?php echo $arrFile[0] ?>" alt="card image">
                                        <?php }else { ?>
                                            <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/Avatar.png"> 
                                        <?php } ?>
                                    </p>
                                    <h4 class="card-title"><?= $value["nameteam"] ?></h4>
                                    <p class="card-text"><?= $value["speciality"] ?></p>

                                    
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title"><?= $value["nameteam"] ?></h4>
                                    <p class="card-text"><?= $value["descriptionteam"] ?></p>
                                    <?php if(Authorisation::isInterfaceAdmin()){ ?>
                                        <a href="javascript:;" class="btn btn-primary btn-sm editElement<?= $blockCms['_id'] ?> editSectionBtns"
                                                data-key="<?= $key ?>" 
                                                data-img='<?php echo json_encode($initFiles) ?>' 
                                                data-nameteam="<?= $value["nameteam"] ?>"
                                                data-speciality="<?= $value["speciality"] ?>"
                                                data-descriptionteam="<?= $value["descriptionteam"] ?>"
                                            ><i class="fa fa-pencil"></i></a>

                                        <a  href="javascript:;" class="btn btn-sm btn-danger deletePlan<?= $blockKey ?> "
                                            data-key="<?= $key ?>" 
                                            data-id ="<?= $blockKey ?>"
                                            data-path="content.<?= $key ?>"
                                            data-collection = "cms">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./Team member -->
            <?php } 
            } ?>

        </div>
        <div class="text-center editSectionBtns">
            <div class="" style="width: 100%; display: inline-table; padding: 10px;">
                <?php if(Authorisation::isInterfaceAdmin()){?>
                    <div class="text-center addElement<?= $blockCms['_id'] ?>">
                        <button class="btn btn-primary">Ajouter</button>
                    </div>  
                <?php } ?>
            </div>
        </div>

    </div>
</section>
<script type="text/javascript">
    
    if(costum.editMode){
                cmsConstructor.sp_params["<?= $blockKey ?>"] = <?php echo json_encode( $blockCms ); ?>;
    };
    
    appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($title) ?>,"<?= $blockKey ?>");

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer la section",
                "description" : "Personnaliser votre section",
                "icon" : "fa-cog",
                "properties" : { 
                    background_color : {
                        label : "Couleur du fond",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.background_color
                    }
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                toastr.success("élement mis à jour");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            } );
                        }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        

        $(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
            var deleteObj ={};
            deleteObj.id = $(this).data("id");
            deleteObj.path = $(this).data("path");          
            deleteObj.collection = $(this).data("collection");
            deleteObj.value = null;
            bootbox.confirm("Etes-vous sûr de vouloir supprimer cet élément ?",
            function(result){
              if (!result) {
                return;
              }else {
                dataHelper.path2Value( deleteObj, function(params) {
                    mylog.log("deleteObj",params);
                    toastr.success("Element effacé");
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                });
              }
            }); 
        });
        
        $(".editElement<?= $blockCms['_id'] ?>").click(function() {  
            //var contentLength = Object.keys(<?php //echo json_encode($content); ?>).length;
            var key = $(this).data("key");
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>"
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(key);
            var obj = {
                nameteam :         $(this).data("nameteam"),
                descriptionteam:    $(this).data("descriptionteam"),
                img:            $(this).data("img"),
                speciality:     $(this).data("speciality")
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "Ajouter nouveau bloc CMS",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,key),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                      tplCtx.value = {};
                      $.each( activeForm.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                      });

                      if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                      else {
                          dataHelper.path2Value( tplCtx, function(params) { 
                               dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    // urlCtrl.loadByHash(location.hash);
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    }else{
                                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                        // urlCtrl.loadByHash(location.hash);
                                    }
                                });
                          } );
                      }

                    }
                }
                };          
                dyFObj.openForm( activeForm );
            });


        $(".addElement<?= $blockCms['_id'] ?>").click(function() { 
            var keys = Object.keys(<?php echo json_encode($content); ?>);
            var lastContentK = 0; 
            if (keys.length!=0) 
                lastContentK = parseInt((keys[(keys.length)-1]), 10);
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(lastContentK+1);
            var obj = {
                nameTeam :         $(this).data("nameTeam"),
                descriptionTeam:    $(this).data("descriptionTeam"),
                speciality:     $(this).data("speciality")
            };

            var activeForm = {
                "jsonSchema" : {
                    "title" : "Ajouter nouveau bloc CMS",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,lastContentK+1),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                      tplCtx.value = {};
                      $.each( activeForm.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                      });

                      if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                      else {
                          dataHelper.path2Value( tplCtx, function(params) { 
                               dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                    }else{
                                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                    }
                                });
                          } );
                      }

                    }
                }
                };          
                dyFObj.openForm( activeForm );
            });


        
        function getProperties(obj={},subKey){
            var props = {
                img : {
                    "inputType" : "uploader",
                    "label" : "image",
                    "docType" : "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "endPoint": "/subKey/"+subKey,
                    "domElement" : "image",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "label": "Image :",
                    "showUploadBtn": false,
                    initList : obj["img"]
                },
                nameteam : {
                    label : "Nom du personne",
                    "inputType" : "text",
                value : obj["nameteam"]
                },

                 speciality : {
                    label : "Spécialité",
                    "inputType" : "text",
                    value : obj["speciality"]
                },
                
                descriptionteam : {
                    label : "Description",
                    "inputType" : "textarea",
                    value :  obj["descriptionteam"]
                } 
                
            };
            return props;
        }
    });
</script>