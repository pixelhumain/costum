<?php  
$keyTpl = "socialNetwork2";
$paramsData=[
	"title"=>"",
	"textColor"=>"#fff",
	"backgroundSocial" => "#6d3795",
	"backgroundSocialhover" => "#bea041",
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
};

?>

<style type="text/css">
.mt-30{margin-top:30px}
.mt-40{margin-top:40px}
.boxProfil_<?=$kunik?> {
	text-align:center;
	color:<?= $paramsData["textColor"]?>;
	position:relative;
	border-radius: 5px;
	box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);
	height: 350px;
}
.boxProfil_<?=$kunik?> .box-content,.boxProfil_<?=$kunik?>:after {
	width:100%;
	position:absolute;
	left:0
}
.boxProfil_<?=$kunik?>:after{ 
	content:"";
	height:100%;
	background:linear-gradient(to bottom,rgba(0,0,0,0) 0,rgba(0,0,0,.09) 50%,rgba(0,0,0,.9) 100%);
	top:0;
	transition:all .5s ease 0s
}
.boxProfil_<?=$kunik?> .post,.boxProfil_<?=$kunik?> .title {
	transform:translateY(145px);
	transition:all .4s cubic-bezier(.13,.62,.81,.91) 0s
}
.boxProfil_<?=$kunik?>:hover:after {
	background:linear-gradient(to bottom,rgba(0,0,0,.01) 0,rgba(0,0,0,.09) 11%,rgba(0,0,0,.12) 13%,rgba(0,0,0,.19) 20%,rgba(0,0,0,.29) 28%,rgba(0,0,0,.29) 29%,rgba(0,0,0,.42) 38%,rgba(0,0,0,.46) 43%,rgba(0,0,0,.53) 47%,rgba(0,0,0,.75) 69%,rgba(0,0,0,.87) 84%,rgba(0,0,0,.98) 99%,rgba(0,0,0,.94) 100%)
}
.boxProfil_<?=$kunik?> img {
	width:100%;
	height:auto
}
.boxProfil_<?=$kunik?> .box-content {
	padding:20px;
	margin-bottom:20px;
	bottom:0;z-index:1
}
.boxProfil_<?=$kunik?>:hover .box-content {
    padding: 10px;
    margin-bottom: 5px;
}
.boxProfil_<?=$kunik?> .title {
	font-size:22px;
	font-weight:700;
	text-transform:uppercase;
	margin:0 0 10px
}
.boxProfil_<?=$kunik?> .post {
	display:block;
	padding:8px 0;
	font-size:15px
}
.boxProfil_<?=$kunik?> .social li a {
	border-radius:50%;
	font-size:20px;
	color:<?= $paramsData["textColor"]?>;
}
.boxProfil_<?=$kunik?>:hover .post,.boxProfil_<?=$kunik?>:hover .title {
	transform:translateY(0)
}
.boxProfil_<?=$kunik?> .social {
	list-style:none;padding:0 0 5px;
	margin:40px 0 25px;
	opacity:0;
	position:relative;
	transform:perspective(500px) rotateX(-90deg) rotateY(0) rotateZ(0);
	transition:all .6s cubic-bezier(0,0,.58,1) 0s
}
.boxProfil_<?=$kunik?>:hover .social {
	opacity:1;transform:perspective(500px) rotateX(0) rotateY(0) rotateZ(0);
	margin: 40px 0 0px;
}
.boxProfil_<?=$kunik?> .social:before {
	content:"";width:50px;
	height:2px;
	background:#fff;
	margin:0 auto;
	position:absolute;
	top:-23px;left:0;
	right:0
}
.boxProfil_<?=$kunik?> .social li {
	display:inline-block
}
.boxProfil_<?=$kunik?> .social li a{
	display:block;
	width:40px;
	height:40px;
	line-height:40px;
	background:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["backgroundSocial"]; ?>;
	margin-right:10px;
	margin-bottom: 10px;
	transition:all .3s ease 0s
}
.boxProfil_<?=$kunik?> .social li a img{
	margin: 10px;
    width: 20px;
}
.boxProfil_<?=$kunik?> .social li a:hover i, .boxProfil_<?=$kunik?> .social li a:hover img {
	-moz-transform: rotate(360deg);
	-webkit-transform: rotate(360deg);
	-ms--transform: rotate(360deg);
	transform: rotate(360deg);
	-webkit-transition: all 0.2s;
	-moz-transition: all 0.2s;
	-o-transition: all 0.2s;
	-ms-transition: all 0.2s;
	transition: all 0.2s;
}
.boxProfil_<?=$kunik?> .social li a i, .boxProfil_<?=$kunik?> .social li a img{
	-webkit-transition: all 0.8s;
		-moz-transition: all 0.8s;
		-o-transition: all 0.8s;
		-ms-transition: all 0.8s;
		transition: all 0.8s;
}
.boxProfil_<?=$kunik?> .social li a:hover { 
	background:<?= $paramsData["backgroundSocialhover"]?>;
}
.boxProfil_<?=$kunik?> .social li:last-child a {
	margin-right:0
}
.boxProfil_<?=$kunik?> {
	overflow:hidden;
	margin-bottom:30px;
	margin-bottom:30px;
}
@media only screen and (max-width:990px) {
	.boxProfil_<?=$kunik?> {
		margin-bottom:30px
	}
}

</style>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 col-lg-10">
    <h3 class="text-center sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h3>
    <div class="row row_profil mt-30">
 
    </div>
</div>



<script type="text/javascript">

    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    jQuery(document).ready(function() {

        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer votre section",
                "description" : "Personnaliser votre section1",
                "icon" : "fa-cog",
                "properties" : {
                    "backgroundSocial" : {
                        label : "Couleur du bouton reseau",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.backgroundSocial
                    },
                    "backgroundSocialhover" : {
                        label : "Couleur du bouton reseau hover",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.backgroundSocialhover
                    }
                },
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},                
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});
					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) { 
							dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}
				}
            }

        };
        mylog.log("paramsData",sectionDyf);
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
        });


        /**************************** list**************************/    
        $.ajax({
           url : baseUrl+"/costum/blockcms/getcollection",
           type : 'POST', 
           data : {
            sourceKey : contextSlug
            //type: "article"
          },
           dataType : "json",
           success:function(data){
              mylog.log("blockcms collection",data);
              var html = "";
              $.each(data, function( index, value ) {
                html += 
                  	'<div class="col-md-4 col-sm-6 col-xs-12">'+
			            '<div class="boxProfil_<?=$kunik?>">';
			            if (typeof(value.profilMediumImageUrl) != "undefined") {
			    html +=	'<img src="'+value.profilMediumImageUrl+'">';
			            }else{
			    html +=	'<img src="'+assetPath+'/images/blockCmsImg/Avatar.png">';
			            }
			                
			    html +=    '<div class="box-content">'+
			                    '<h3 class="title">'+value.name+'</h3>'+
			                    '<span class="post">'+value.shortDescription+'</span>';
			                  	if ( typeof value.socialNetwork != "undefined") {
			    html +=             '<ul class="social">';
			    					if (typeof value.socialNetwork.gitlab != "undefined") {
			    html += 				'<li><a href="'+value.socialNetwork.gitlab+'" class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Gitlab" target="_blank"><i class="fa fa-gitlab"></i></a></li>';
			    					}
			    					if (typeof value.socialNetwork.facebook != "undefined") {
			    html += 				'<li><a href="'+value.socialNetwork.facebook+'" class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>';
			    					}
			    					if (typeof value.socialNetwork.twitter != "undefined") {
			    html += 				'<li><a href="'+value.socialNetwork.twitter+'" class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>';
			    					}
				                    if (typeof value.socialNetwork.github != "undefined") {
			    html += 				'<li><a href="'+value.socialNetwork.github+'" class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Github" target="_blank"><i class="fa fa-github"></i></a></li>';
			    					}   
			    					if (typeof value.socialNetwork.diaspora != "undefined") {
			    html += 				'<li><a href="'+value.socialNetwork.diaspora+'" class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Diaspora" target="_blank"><img src="/assets/c4e4b8ae/images/diaspora_icon.png"></a></li>';
			    					}
			    					if (typeof value.socialNetwork.mastodon != "undefined") {
			    html += 				'<li><a href="'+value.socialNetwork.mastodon+'" class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Mastodon" target="_blank"><img src="/assets/c4e4b8ae/images/mastodon.png"></a></li>';
			    					}
			    					if (typeof value.socialNetwork.instagram != "undefined") {
			    html += 				'<li><a href="'+value.socialNetwork.instagram+'" class="tooltips" data-toggle="tooltip" data-placement="top" data-original-title="Instagram" target="_blank" ><i class="fa fa-instagram"></i></a></li>';
			    					} 
  
				html +=             '</ul>';            
				               
			                  	}
			    html +=               	
			                '</div>'+
			            '</div>'+
			        '</div>';

              });
              $(".row_profil").html(html);
           }
        });
        /****************************end list**********************/
    });


</script>
