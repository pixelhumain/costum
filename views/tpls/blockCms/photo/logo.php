<?php 
$keyTpl = "logo";
$paramsData = [
	"photo"=>""
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<?php 
$initFiles = Document::getListDocumentsWhere(
	array(
		"id"=> $blockKey,
		"type"=>'cms',
		"subKey"=>'block',
	), "image"
);

$arrayImg = [];
foreach ($initFiles as $key => $value) {
	$arrayImg[]= $value["imagePath"];
}
?>
<style type="text/css">
	
	.contenu_<?= $kunik ?> p {
		
		font-family: "Aquawax";
		font-size: 20px !important;
		padding: 18%;
		margin-bottom: -70px;
	}
	.parallax_<?=$kunik?> {
		background-attachment: fixed;
		background-position: center;
		background-repeat: no-repeat;
		background-size: cover;
		background-size: 100% 80%;
	}
	

	
	.logo_<?= $kunik?> img{
		width: 50%;
		height: auto;
	}
	@media (max-width: 414px) {
		.block-container-<?= $kunik?>{
			max-height: 100px !important;
			min-height: 100px !important;
		}
			.contenu_<?= $kunik ?> p {
			font-size: 13px !important;
			margin-bottom: 20px;
			padding: 63%;
		}
		.parallax_<?=$kunik?> {
			background-size: 100% 40%;
		}
	}
	@media screen and (min-width: 1500px){
		.logo_<?= $kunik?> img{
			width: 50%;
		    height: auto;
		    margin-top: -70px;
		    margin-bottom: -70px;
		}
	}
</style>
 <?php if (!empty($initFiles )) { 
	?>
	<div class=" logo_<?= $kunik?> text-center" >
		<img src="<?= $initFiles[0]["imageThumbPath"] ?>">
	<?php } else {?>
		<div class="logo_<?= $kunik?> text-center">
			<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/assoKosasa/kosasa-logo-rvb-couleur_orig.png">	
	<?php } ?>
	</div>
		
		
	<script type="text/javascript">
		sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
		jQuery(document).ready(function() {
			sectionDyf.<?php echo $kunik?>Params = {
				"jsonSchema" : {    
					"title" : "Configurer la section1",
					"description" : "Personnaliser votre section1",
					"icon" : "fa-cog",
					"properties" : {	
						"image" :{
							"inputType" : "uploader",
							"label" : "image",
							"docType": "image",
							"contentKey" : "slider",
							"itemLimit" : 1,
							"endPoint": "/subKey/block",
							"domElement" : "image",
							"filetypes": ["jpeg", "jpg", "gif", "png"],
							"label": "Image :",
							"showUploadBtn": false,
							initList : <?php echo json_encode($initFiles) ?>
			          }
					},
					beforeBuild : function(){
						uploadObj.set("cms","<?php echo $blockKey ?>");
					},
					save : function () {  
						tplCtx.value = {};

						$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
							tplCtx.value[k] = $("#"+k).val();
						});

						mylog.log("save tplCtx",tplCtx);

						if(typeof tplCtx.value == "undefined")
							toastr.error('value cannot be empty!');
						else {
			                  dataHelper.path2Value( tplCtx, function(params) {
			                    dyFObj.commonAfterSave(params,function(){
			                      toastr.success("Élément bien ajouté");
			                      $("#ajax-modal").modal('hide');
								  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
			                    //   urlCtrl.loadByHash(location.hash);
			                    });
			                  } );
						}
					}
				}
			};
			$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
				tplCtx.id = $(this).data("id");
				tplCtx.collection = $(this).data("collection");
				tplCtx.path = "allToRoot";
				dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			});

		});
	</script>