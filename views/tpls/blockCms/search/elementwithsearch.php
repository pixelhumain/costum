<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

form.example input[type=text] {
  padding: 10px;
  font-size: 17px;
  border-left: 1vw solid white;
  border-bottom: 1vw solid white;
  border-top: 1vw solid white;
  border-right: none;
  float: left;
  width: 80%;
  background: #c1c1c1;
  border-top-left-radius: 50px;
  border-bottom-left-radius: 50px;
}

form.example button {
  float: left;
  width: 20%;
  padding: 10px;
  background: #e84d4c;
  color: white;
  font-size: 17px;  
  border-right: 1vw solid white;
  border-bottom: 1vw solid white;
  border-top: 1vw solid white;
  border-left: none;
  cursor: pointer;  
  border-top-right-radius: 50px;
  border-bottom-right-radius: 50px;
}

form.example button:hover {
  background: #0b7dda;
}

form.example::after {
  content: "";
  clear: both;
  display: table;
}
.field-search {
	   box-shadow: 0px 0px 10px 0px #00000087;
	   border-radius: 50px;
}

.search-field ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: white;
  opacity: 1; /* Firefox */
}
 
 .search-field {
 	    margin-top: -30px;
 }
</style>
</head>
<body>
<div class="search-field">
<form class="example field-search" action="/action_page.php" style="margin:auto;max-width:50%">
  <input type="text" placeholder="Stage, formation..." name="search2">
  <button type="submit">Recherche</button>
</form>
</div>
