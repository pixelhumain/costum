<?php 
    $myCmsId  = isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
    $styleCss = (object) [$kunik => $blockCms["css"] ?? []];
?>
<style>
    .<?= $kunik ?> .container-data {
        margin-left: 10%;
        margin-right: 10%;
        margin-top: 2%;
        margin-bottom: 5%;
    }

    .<?= $kunik ?> .dataset-card {
        border: 1px solid #ddd;
        border-radius: 8px;
        padding: 24px;
        flex: 1 1 calc(33.33% - 20px);
        background-color: #fff;
        transition: box-shadow 0.3s ease;
        margin-bottom: 20px;
        box-sizing: border-box; 
    }

    .<?= $kunik ?> .dataset-card:hover {
        box-shadow: 0 6px 12px rgba(0, 0, 0, 0.2);
    }

    .<?= $kunik ?> .dataset-header h3 {
        font-size: 1.7rem; 
        color: #ff8800; 
        margin-bottom: 12px;
        /* cursor: pointer; */
    }

    .<?= $kunik ?> .dataset-header p {
        font-size: 1.5rem;
        color: #555;
        margin-bottom: 20px;
    }

    .<?= $kunik ?> .dataset-details p {
        font-size: 1.3rem;
        margin: 6px 0;
    }

    .dataset-tags {
        margin-top: 12px;
        margin-bottom: 20px;
    }

    .tag {
        display: inline-block;
        background-color: #0066cc;
        color: white;
        padding: 6px 12px;
        font-size: 1.2rem;
        border-radius: 4px;
        margin-right: 6px;
        transition: background-color 0.3s ease;
        cursor: pointer;
        margin-bottom: 2px;
        font-weight: 600;
    }
    .tag:hover {
        background-color: #0055aa; 
    }

    .tags-filters {
        background-color: #ffa008 !important;
    }

    .<?= $kunik ?> .section-title {
        font-size: 22px;          
        font-weight: bold;       
        color: #2C3E50;          
        text-align: center;       
        margin-bottom: 10px;      
        text-transform: uppercase;
        margin-top: 1%; 
        margin-left: 8%;
        margin-right: 8%;
        text-transform: none;
    }

    .<?= $kunik ?> .section-description {
        font-size: 16px;          
        color: #7F8C8D;           
        text-align: center;       
        margin-bottom: 20px;      
        line-height: 1.5;         
        font-style: italic;  
        margin-left: 5%;
        margin-right: 5%;    
    }

    .dataset-card .dataset-header p {
        max-height: 150px;
        overflow: auto;
        text-align: justify;
    }

    .dataset-card .dataset-header p::-webkit-scrollbar {
        width: 5px;
    }

    .dataset-card .dataset-header p::-webkit-scrollbar-thumb {
        background: #ccc;
        border-radius: 5px;
    }

    .dataset-card .dataset-header p::-webkit-scrollbar-thumb:hover {
        background: #aaa;
    }

    .dataset-card .dataset-header p::-webkit-scrollbar-track {
        background: #f4f4f4;
        border-radius: 5px;
    }

</style>
<div class="<?= $kunik ?>">
    <div class="container-title">
        <h3 class="section-title">Explorez les jeux de données ouverts disponibles pour votre région sur data.gouv.fr.</h3>
        <p class="section-description hidden-xs">Couvrant divers domaines tels que l'environnement, la santé, les transports et bien plus, pour chaque région du territoire.</p>
    </div>
    <div class="container-data">

    </div>
</div>
<script>
    
    var data_gouv = <?= json_encode($dataApi) ?>;
    if(typeof data_gouv.data != "undefined" && (data_gouv.data).length > 0) {
        var htmlData = `<h3>${(data_gouv.data).length} RESULTATS</h3>`;
        var elements = data_gouv.data;
        $.each(elements, function(index, value) {
            var htmlKey = "";
            if (typeof value.tags != "undefined" && (value.tags).length > 0) {
                (value.tags).forEach(tag => {
                    htmlKey += `<span class="tag tags-${tag}" onclick="filterTags(event)">#${tag}</span>`;
                });
            }
            var updat = "";
            if(typeof value.last_update != "undefined") updat = getTimeElapsedMessage(value.last_update);

            htmlData += `<div class="dataset-card">
                            <div class="dataset-header">
                                <h3 class="title-detail" data-id="${value.id}">${value.title}</h3>
                                <p>${value.description}</p>
                            </div>
                            <div class="dataset-details">
                                <p><strong>Licence:</strong> ${value.license}</p>
                                <p> ${updat}</p>
                            </div>
                            <div class="dataset-tags">
                                ${htmlKey}
                            </div>
                        </div>`;
        });
        $(".container-data").html(htmlData);
    };

    function getTimeElapsedMessage(dateString) {
        var date = new Date(dateString);
        var now = new Date();

        var diffMs = now - date;
        var diffSec = Math.floor(diffMs / 1000);
        var diffMin = Math.floor(diffSec / 60);
        var diffHours = Math.floor(diffMin / 60);
        var diffDays = Math.floor(diffHours / 24);

        if (diffDays > 15) {
            var options = { day: 'numeric', month: 'long', year: 'numeric' };
            return `Mis à jour le ${date.toLocaleDateString('fr-FR', options)}`;
        }

        let message = "Mise à jour il y a ";

        if (diffDays > 0) {
            message += `${diffDays} jour${diffDays > 1 ? 's' : ''}`;
        } else if (diffHours > 0) {
            message += `${diffHours} heure${diffHours > 1 ? 's' : ''}`;
        } else if (diffMin > 0) {
            message += `${diffMin} minute${diffMin > 1 ? 's' : ''}`;
        } else {
            message += "moins d'une minute";
        }

        return message;
    }

    var activeFilters = [];

    function filterTags(event) {
        var tagElement = event.target;
        var tagText = tagElement.textContent.slice(1);
        
        if (tagElement.classList.contains("tags-filters")) {
            $('.tags-'+tagText).removeClass("tags-filters");
            activeFilters = activeFilters.filter(tag => tag !== tagText);
        } else {
            $('.tags-'+tagText).addClass("tags-filters");
            activeFilters.push(tagText);
        }
        applyFilters();
    }

    function applyFilters() {
        var cards = document.querySelectorAll('.dataset-card');

        cards.forEach(card => {
            var tags = Array.from(card.querySelectorAll('.tag')).map(tag => tag.textContent.slice(1));

            if (activeFilters.length === 0 || tags.some(tag => activeFilters.includes(tag))) {
                card.style.display = 'block';
            } else {
                card.style.display = 'none';
            }
        });
    }
</script>