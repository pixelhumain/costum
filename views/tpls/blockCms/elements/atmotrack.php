<?php
$myCmsId  = isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
$styleCss = (object) [$kunik => $blockCms["css"] ?? []];
?>
<style>
    .container-map {
        width: 100%;
        height: 80vh;
    }

    .<?= $kunik ?> .section-title,
    .<?= $kunik ?> .section-sous-title {
        font-size: 22px;
        font-weight: bold;
        color: #2C3E50;
        margin-bottom: 10px;
        text-transform: uppercase;
        margin-top: 1%;
        text-transform: none;
    }

    .<?= $kunik ?> .section-title {
        text-align: center;
    }

    .<?= $kunik ?> .section-description {
        font-size: 16px;
        color: #7F8C8D;
        text-align: center;
        margin-bottom: 20px;
        line-height: 1.5;
        font-style: italic;
        margin-left: 5%;
        margin-right: 5%;
    }

    .tableau-container {
        margin-left: 10px;
    }

    .popup-content,
    .popup-content h3,
    .popup-content p,
    .popup-content li {
        font-size: 16px;
        text-transform: none;
    }

    .popup-content h3 {
        text-align: center;
        height: 40px;
        padding-top: 9px;
        background-color: #2e3951;
        border-radius: 10px 10px 0px 0px;
        color: white;
    }

    .btn-chart {
        width: 100%;
    }

    .input-group {
        display: flex;
        flex-direction: column;
        margin-bottom: 15px;
        height: 32px;
    }

    #toggle-filters {
        background: #31b4f1;
        color: black;
        border: none;
        padding: 5px 10px;
        border-radius: 5px;
        cursor: pointer;
        z-index: 2;
        height: 41px;
        width: 40px;
        margin-left: 5px;
    }

    .input-group input,
    .input-group select {
        width: 100%;
        padding: 10px 10px 5px;
        font-size: 16px;
        border: 1px solid #ccc;
        border-radius: 5px;
        outline: none;
        transition: border 0.3s ease;
    }

    .input-group label {
        position: absolute;
        left: 10px;
        top: 50%;
        transform: translateY(-50%);
        background: white;
        /* padding: 0 5px; */
        font-size: 14px;
        color: #777;
        transition: all 0.3s ease;
        pointer-events: none;
    }

    .input-group input:focus,
    .input-group select:focus,
    .input-group input:not(:placeholder-shown),
    .input-group select:not(:placeholder-shown) {
        border-color: #126595;
    }

    .input-group input:focus+label,
    .input-group select:focus+label,
    .input-group input:not(:placeholder-shown)+label,
    .input-group select:not(:placeholder-shown)+label {
        top: 0;
        font-size: 14px;
        color: #126595;
    }

    #active-filters,
    #circleMarker {
        height: 42px;
        margin-left: 5px;
    }

    #searchInput,
    #searchInput-day {
        width: 100%;
        max-width: 300px;
        height: 40px;
        padding: 10px;
        margin-bottom: 10px;
        margin-left: 5px;
        border: 1px solid #ccc;
        border-radius: 5px;
        display: block;
        /* margin: 0 auto 20px auto; */
        font-size: 16px;
    }

    .table-container {
        width: 100%;
        max-height: 70vh;
        overflow-x: auto;
        overflow-y: auto;
        border-radius: 5px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        background: white;
        position: relative;
    }

    .table-container::-webkit-scrollbar {
        width: 8px;
    }

    .table-container::-webkit-scrollbar-thumb {
        background: #ccc;
        border-radius: 5px;
    }

    .table-container::-webkit-scrollbar-thumb:hover {
        background: #aaa;
    }

    .table-container::-webkit-scrollbar-track {
        background: #f4f4f4;
        border-radius: 5px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        background: white;
    }

    thead {
        position: sticky;
        top: 0;
        z-index: 10;
        background-color: #3fa5f6;
    }

    th,
    td {
        padding: 12px;
        text-align: center;
        border-bottom: 1px solid #ddd;
    }

    th {
        background-color: #3fa5f6;
        color: white;
        position: sticky;
        cursor: pointer;
        top: 0;
        z-index: 20;
    }

    tr:nth-child(even) {
        background-color: #f9f9f9;
    }

    tr:hover {
        background-color: #f1f1f1;
    }

    /* Responsive */
    @media (max-width: 600px) {

        th,
        td {
            padding: 8px;
            font-size: 14px;
        }
    }

    .btn-change_visuel {
        text-align: right;
        padding: 5px;
    }

    .btn-change_visuel .btn-primary {
        background-color: #f5f5f5 !important;
        border-color: #041e29;
        color: #041e29 !important;
    }

    .btn-change_visuel .btn-primary:hover {
        background-color: #041e29 !important;
        border-color: #041e29;
        color: white !important;
    }

    .btn-change_visuel .active_views {
        background-color: #041e29 !important;
        border-color: #041e29;
        color: white !important;
    }

    .active_views {
        background-color: #041e29 !important;
        border-color: #041e29;
        color: white !important;
    }

    #barChart {
        max-height: 67vh;
    }

    #pm1Chart,
    #pm25Chart,
    #pm10Chart {
        max-height: 500px;
    }

    .filters-dyn {
        display: flex;
        gap: 5px;
    }

    .pagination {
        display: flex;
        justify-content: center;
        margin: 10px 0;
    }

    .pagination button {
        padding: 5px 10px;
        margin: 0 5px;
        border: 1px solid #ccc;
        background-color: #f8f8f8;
        border-radius: 5px;
        cursor: pointer;
    }

    .pagination button:disabled {
        background-color: #ddd;
        cursor: not-allowed;
    }

    .sort-icon {
        margin-left: 5px;
        cursor: pointer;
    }

    #pageInfo {
        padding: 5px;
    }


    /*  */
    .progressbar-container {
        display: flex;
        flex-direction: column;
        gap: 10px;
    }

    .progress-wrapper {
        display: flex;
        align-items: center;
        gap: 10px;
    }

    .progress-label {
        white-space: nowrap;
        font-weight: bold;
        width: 80px;
        text-align: right;
    }

    .progress-group {
        display: flex;
        flex-direction: column;
        flex: 1;
    }

    .progress-timeline {
        display: flex;
        justify-content: space-between;
        font-size: 12px;
        margin-bottom: 2px;
    }

    .progress-bar {
        height: 22px;
        width: 100%;
        border: 1px solid #ccc;
        display: flex;
        overflow: hidden;
        border-radius: 20px;
        background-color: white !important;
    }

    .all-bar-content {
        border: 1px solid #ccc;
        width: auto;
        background-color: #f9f9f9;
        border-radius: 5px;
        padding: 5px;
    }

    .legend-container {
        margin-top: 2px;
        margin-bottom: 5px;
        padding: 10px;
        border: 1px solid #ccc;
        width: fit-content;
        background-color: #f9f9f9;
        border-radius: 5px;
    }

    .legend-container h3 {
        margin: 0 0 10px 0;
        font-size: 16px;
        text-align: center;
    }

    .legend-item {
        display: flex;
        align-items: center;
        gap: 5px;
    }

    .color-box {
        width: 20px;
        height: 20px;
        border-radius: 4px;
        border: 1px solid #000;
        display: inline-block;
    }

    .legend-text {
        font-size: 14px;
        font-weight: bold;
        color: #333;
        white-space: nowrap;
    }

    #airQualityChart,
    #monthAirQualityChart,
    #airQualityChartday1,
    #airQualityChartday2,
    #combined-chart {
        max-height: 65vh;
    }

    #airQualityChart,
    #monthAirQualityChart,
    #combined-chart,
    #airQualityChartday2 {
        width: 100%;
        margin-left: 50px;
        margin-right: 50px;
    }

    #progress-container1 {
        width: 97%;
    }

    #airQualityChartday1 {
        margin-left: 10px;
        margin-right: 10px;
    }

    .threshold-section {
        width: 80%;
        /* max-width: 600px; */
        margin: auto;
        text-align: center;
        padding: 15px;
        border: 2px solid #ccc;
        border-radius: 8px;
        box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.1);
    }

    .threshold-title {
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 10px;
    }

    .threshold-table {
        width: 100%;
        border-collapse: collapse;
        margin-bottom: 15px;
    }

    .threshold-table th,
    .threshold-table td {
        padding: 8px;
        border: 1px solid #ddd;
        text-align: center;
    }

    .legend-container {
        display: flex;
        justify-content: center;
        gap: 15px;
        margin-top: 10px;
    }

    .legend-item {
        display: flex;
        align-items: center;
        gap: 5px;
    }

    .color-box {
        width: 15px;
        height: 15px;
        display: inline-block;
        border-radius: 3px;
    }

    /* .row {
        text-align: center;
        align-items: center;
        justify-content: center;
    } */

    .air-div-marker .marker-atmo-image {
        position: absolute;
        min-width: 20px !important;
        min-height: 20px !important;
        /* top: 2px;
        left: 2px; */
        text-align: center;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        object-fit: contain;
    }

    #calendar {
        padding: 20px 10px 10px;
        background-color: #161b22;
        border-radius: 5px;
        font-size: 12px;
        position: relative;
        overflow-x: auto;
    }

    .calendar-container {
        display: flex;
        padding-top: 20px;
        margin-bottom: 10px;
    }

    .months-header {
        display: flex;
        justify-content: flex-start;
        padding: 5px 10px;
        font-weight: bold;
        margin-left: 60px;
        font-size: 14px;
        color: #fff;
        position: relative;
    }

    .month-indicator {
        flex: 0 0 auto;
        text-align: center;
        position: absolute;
        top: 0;
        transform: translateX(-50%);
        white-space: nowrap;
    }

    .year-buttons {
        display: flex;
        align-items: flex-end;
        gap: 5px;
        margin-top: 10px;
    }

    .year-button {
        background: #222;
        color: white;
        border: none;
        padding: 8px 12px;
        margin: 4px 0;
        cursor: pointer;
        border-radius: 4px;
        transition: background 0.3s;
    }

    .year-button.active {
        background: #6a0dad;
        font-weight: bold;
    }

    .year-button:hover {
        background: #444;
    }

    .weekdays-column {
        display: flex;
        flex-direction: column;
        margin-right: 5px;
    }

    .weekday {
        height: 15px;
        width: 25px;
        color: #768390;
        font-size: 10px;
        text-align: right;
        padding-right: 5px;
        margin-bottom: 3px;
    }

    .week-column {
        display: flex;
        flex-direction: column;
        margin-right: 3px;
        position: relative;
        /* Pour positionner le mois */
    }

    .day {
        width: 15px;
        height: 15px;
        margin-bottom: 3px;
        border-radius: 2px;
        cursor: pointer;
    }

    .bonne {
        background-color: #238636 !important;
    }

    .moyenne {
        background-color: rgb(243, 206, 20) !important;
    }

    .mauvaise {
        background-color: #d97706 !important;
    }

    .dangereux {
        background-color: #e3342f !important;
    }

    .empty {
        background-color: #30363d !important;
    }

    .contribution-count {
        margin-bottom: 5px;
        color: #c9d1d9;
        font-size: 14px;
    }

    .legend {
        display: flex;
        align-items: center;
        justify-content: flex-end;
        gap: 5px;
        /* margin-top: 5px; */
        color: #768390;
        font-size: 12px;
    }

    .legend-color {
        width: 10px;
        height: 10px;
        border-radius: 2px;
    }

    .month-janv {
        left: 10px !important;
    }

    #circleMarker {
        position: absolute;
        top: 5px;
        z-index: 2;
        left: auto;
        right: 90px;
        background-color: #f5f5f5;
        border-color: #a0a4a5;
        color: #041e29;
        height: 40px;
        width: 40px;
        box-shadow: 2px 2px 0px 0px #ccc;
    }

    #mapMarker {
        position: absolute;
        top: 5px;
        z-index: 2;
        left: auto;
        right: 135px;
        font-size: 20px;
        background-color: #f5f5f5;
        border-color: #a0a4a5;
        color: #041e29;
        height: 40px;
        width: 40px;
        box-shadow: 2px 2px 0px 0px #ccc;
    }

    .marker-change {
        padding: 6px;
        font-size: 20px;
        background-color: #f5f5f5;
        border-color: #a0a4a5;
        color: #041e29;
        height: 40px;
        width: 40px;
        box-shadow: 2px 2px 0px 0px #ccc;
    }
    .chart-legende-container{
        background: white;
    }

    .popover {
        width: 230px;
        text-align: center;
    }

    #btn-lasteDay,
    #btn-afterDay {
        height: 40px;
    }

    .day-calendar-active {
        border: 2px solid white;
    }

    .row {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        align-items: stretch;
    }

    .card-box {
        position: relative;
        color: black;
        padding: 15px;
        margin: 20px 0;
        border-radius: 10px;
        text-align: center;
        background: white;
        box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.1);
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        height: 100%;
    }

    .card-box:hover {
        color: darkblue;
        text-decoration: none;
    }

    .card-box:hover .icon i {
        font-size: 100px;
        transition: 1s;
    }

    .card-box .inner {
        padding: 5px 10px;
    }

    .card-box h3 {
        font-size: 16px;
        font-weight: bold;
        margin-bottom: 8px;
        white-space: nowrap;
    }

    .card-box p {
        font-size: 20px;
        font-weight: bold;
    }

    .card-box .icon {
        position: absolute;
        bottom: 5px;
        right: 5px;
        font-size: 72px;
        color: rgba(0, 0, 0, 0.15);
    }

    .card-box .card-box-footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        padding: 3px 0;
        text-align: center;
        color: rgba(255, 255, 255, 0.8);
        background: rgba(0, 0, 0, 0.1);
    }

    .card-box:hover .card-box-footer {
        background: rgba(0, 0, 0, 0.3);
    }

    .section-title-public p {
        text-align: center;
        font-size: 30px;
        font-weight: bold;
        color: #2C3E50;
    }

    .sante-section,
    .day-cality-text {
        padding: 25px;
        border: 2px solid #8080804a;
        border-radius: 10px;
        box-shadow: 2px 2px 10px gray;
        font-family: 'Inter';
        margin-top: 20px;
    }

    .text-daily {
        text-align: justify;
        font-family: 'Inter' !important;
    }

    .seuil-section {
        display: flex;
        align-items: flex-end;
        justify-content: center;
    }

    .bar-container {
        display: flex;
        flex-direction: column;
        align-items: center;
        margin: 0 10px;
    }

    .bar {
        width: 40px;
        height: 300px;
        display: flex;
        flex-direction: column;
        position: relative;
    }

    .green {
        background-color: #4CAF50;
        height: 25%;
    }

    .yellow {
        background-color: #FFD700;
        height: 25%;
    }

    .orange {
        background-color: #FFA500;
        height: 25%;
    }

    .red {
        background-color: #FF4500;
        height: 25%;
    }

    .sportif-image,
    .vulnerable-image,
    .section-sportif,
    .section-vulnerable {
        padding: 2%;
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .sportif-image img,
    .vulnerable-image img {
        width: auto;
        max-height: 400px;
        border-radius: 20px;
        box-shadow: 2px 2px 10px gray;
    }
    .section-sportif .section-title,
    .section-vulnerable .section-title {
        font-size: 36px;
        text-align: center;
    }

    .section-sportif #text-sport,
    .section-vulnerable #text-sante {
        text-align: center;
        line-height: 2;
        padding: 2% 10%;
        font-family: 'Inter';
    }

    .level {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        color: black;
        font-size: 12px;
        font-weight: bold;
        position: relative;
    }

    .level span {
        position: absolute;
        top: 90%;
        transform: translateY(-50%);
    }

    .seuil-section .pm-label {
        font-weight: bold;
        margin-top: 5px;
    }

    .description {
        margin-left: 20px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        height: 300px;
    }
    @media (max-width: 992px) {

        .col-lg-5,
        .col-lg-7,
        .col-lg-8,
        .col-lg-4 {
            width: 85%;
            margin-bottom: 2%;
        }

        .quantit-container .col-lg-4 {
            width: 33% !important;
        }

        .sportif-image img,
        .vulnerable-image img {
            max-height: 250px;
        }
    }

    @media (max-width: 768px) {
        .section-title-public p {
            font-size: 24px;
        }

        .section-sportif #text-sport,
        .section-vulnerable #text-sante {
            padding: 5%;
        }

        .card-box {
            width: 90%;
        }
    }

    .loader-loader<?= $kunik ?> {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 42%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 99999 !important;
        padding-top: 180px !important;
    }

    .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> {
        width: auto;
        height: auto;
    }

    .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> .processingLoader {
        width: auto !important;
        height: auto !important;
        margin-top: 30% !important;
    }

    .backdrop-loader<?= $kunik ?> {
        position: fixed;
        opacity: .9;
        background-color: #ccc;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100vh;
        z-index: 999 !important;
    }

    #pieConatiner-chart_pm1, #pieConatiner-chart_pm25, #pieConatiner-chart_pm10, #airQualityPieChart {
        max-width: 450px;
        max-height: 450px;
    }

    #pieConatiner-chart {
        display: flex;
    }

    #alert-text p {
        font-size: 25px;
        font-family: 'Inter';
        text-align: center;
        margin-top: 3%;
    }
</style>

<div class="loader-loader<?= $kunik ?> hidden" id="loader-loader<?= $kunik ?>" role="dialog">
    <div class="content-loader<?= $kunik ?>" id="content-loader<?= $kunik ?>"></div>
</div>
<div class="backdrop-loader<?= $kunik ?> hidden" id="backdrop-loader<?= $kunik ?>"></div>

<div class="<?= $kunik ?>">
    <div class="container-title">
        <h3 class="section-title">Visualisation en direct de la qualité de l'air grâce aux capteurs Atmotrack.</h3>
        <p class="section-description hidden-xs">Les mesures affichées incluent les niveaux de particules fines PM1, PM2.5 et PM10, captées en temps réel à différents emplacements. 📡💨</p>
    </div>
    <div class="row">
        <div class="col-sm-8 filters-dyn">
            <button class="btn btn-primary" id="active-filters">Filtrer</button>
            <div class="input-group">
                <input type="datetime-local" id="debutTime" name="debutTime">
                <label for="debutTime">From</label>
            </div>

            <div class="input-group">
                <input type="datetime-local" id="finTime" name="finTime">
                <label for="finTime">To</label>
            </div>

            <!-- <div class="input-group">
                <input type="text" id="idSensore" name="idSensore">
                <label for="idSensore">ID Capteur</label>
            </div> -->
            <button class="btn btn-success change-day-data" data-key="last" id="btn-lasteDay"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
            <button class="btn btn-success change-day-data" data-key="after" id="btn-afterDay"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
        </div>
        <div class="col-sm-4">
            <div class="btn-change_visuel">
                <button class="btn btn-primary change-views active_views" data-div="map-container"><i class="fa fa-map-marker"></i> Carte</button>
                <button class="btn btn-primary change-views" data-div="public-container"><i class="fa fa-users"></i> Public</button>
                <button class="btn btn-primary change-views" data-div="analyse-container"><i class="fa fa-bar-chart"></i> Analyse</button>
                <button class="btn btn-primary change-views" data-div="tableau-container"><i class="fa fa-calendar"></i> Tableau</button>
            </div>
        </div>
    </div>
    <div class="container-data public-container hidden">
        <div class="col-lg-1"></div>
        <div class="public-section row col-lg-10">
            <div class="row col-lg-12">
                <div class="col-lg-12 section-title-public">
                    <p>Qualité de l'air à Fianarantsoa</p>
                </div>
                <div class="col-lg-12" id="alert-text">

                </div>
                <div class="col-lg-5 day-cality-text" style="margin-top: 5%;">
                    <p class="section-title">Qualité de l’air à Fianarantsoa le <t id="interpret-date">28 Février 2025</t>
                    </p>
                    <p class="text-daily" id="interpret-conclu"></p>
                    <br>
                    <p class="section-title" style="text-align: left !important; font-size: 18px;">Prévision de la qualité de l'air pour les prochains jours.</p>
                    <p id="interpret-predict"></p>
                    <p class="section-title" style="text-align: left !important; font-size: 18px;">Niveau moyen des particules PM pour la journée.</p>
                    <div class="row quantit-container">
                        <div class="col-lg-4 col-sm-6">
                            <div class="card-box">
                                <div class="inner">
                                    <h3>PM1</h3>
                                    <p id="pm1-value"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="card-box">
                                <div class="inner">
                                    <h3>PM2.5</h3>
                                    <p id="pm25-value"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="card-box">
                                <div class="inner">
                                    <h3>PM10</h3>
                                    <p id="pm10-value"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7" style="margin-top: 5%;" id="chart-tenday-las4t">
                    <p class="section-title">Moyenne journalière des concentrations des particules PM de 10 dérniere jours</p>
                    <canvas id="tenday-chart"></canvas>
                </div>
                <div class="row col-lg-12">
                    <div class="col-lg-8 sante-section" style="margin-top: 5%;">
                        <p class="section-title">Les effets des particules fines (PM1, PM2.5, PM10) sur la santé</p>
                        <p class="text-daily">L’air que nous respirons contient de nombreuses particules en suspension, appelées particules fines ou PM (Particulate Matter). Ces particules proviennent de sources variées : circulation routière, industries, chauffage au bois, pollens ou encore incendies.</p>
                        <p class="section-sous-title">Impacts sur la santé</p>
                        <ul>
                            <li>Irritation des yeux, du nez et la gorge</li>
                            <li>Difficultés respiratoires, toux et essouflement</li>
                            <li>Augmentation du risque de maladies cardovasculaires (infartus, AVC)</li>
                            <li>Maladie pulmonqires chroniques (BPCO, fibrose pulmonaire)</li>
                        </ul>
                        <p class="section-sous-title">Qui sont les plus vulnérables ?</p>
                        <ul>
                            <li> Enfants et nourrissons (leur système respiratoire est en développement) </li>
                            <li> Personnes âgées (plus à risque de maladies cardiovasculaires) </li>
                            <li> Personnes souffrant de maladies chroniques (asthme, BPCO, diabète) </li>
                            <li> Femmes enceintes (risques pour le développement du fœtus) </li>
                        </ul>
                    </div>
                    <div class="col-lg-4" style="padding: 4%; margin-top:5%;">
                        <div class="col-lg-12" style="padding: 5%;">
                            <p class="section-title">Les seuils (µg/m³)</p>
                        </div>
                        <div class="col-lg-12 seuil-section">
                            <div class="bar-container">
                                <div class="pm-label">PM1</div>
                                <div class="bar">
                                    <div class="level green"><span>8</span></div>
                                    <div class="level yellow"><span>17</span></div>
                                    <div class="level orange"><span>26</span></div>
                                    <div class="level red"><span></span></div>
                                </div>
                            </div>
                            <div class="bar-container">
                                <div class="pm-label">PM2.5</div>
                                <div class="bar">
                                    <div class="level green"><span>7.5</span></div>
                                    <div class="level yellow"><span>15</span></div>
                                    <div class="level orange"><span>25</span></div>
                                    <div class="level red"><span></span></div>
                                </div>
                            </div>
                            <div class="bar-container">
                                <div class="pm-label">PM10</div>
                                <div class="bar">
                                    <div class="level green"><span>22.5</span></div>
                                    <div class="level yellow"><span>45</span></div>
                                    <div class="level orange"><span>75</span></div>
                                    <div class="level red"><span></span></div>
                                </div>
                            </div>
                            <div class="description">
                                <div class="desc">Bonne</div>
                                <div class="desc">Modérée</div>
                                <div class="desc">Mauvaise</div>
                                <div class="desc">Dangereux</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12" style="margin-top: 5%;">
                        <p class="section-title" style="font-size: 33px; padding: 0% 20%; margin-bottom: 2%;">Évaluation globale de la variation des particules au cours d'une journée, en se basant sur les concentrations des particules enregistrées par les capteurs d'Atmotrack installés à Fianarantsoa.</p>
                        <div class="all-bar-content">
                            <div class="legend-container legend">
                                <div class="legend-item">
                                    <span class="color-box" style="background-color: green;"></span>
                                    <span class="legend-text">Faible</span>
                                </div>
                                <div class="legend-item">
                                    <span class="color-box" style="background-color: yellow;"></span>
                                    <span class="legend-text">Modéré</span>
                                </div>
                                <div class="legend-item">
                                    <span class="color-box" style="background-color: orange;"></span>
                                    <span class="legend-text">Élevé</span>
                                </div>
                                <div class="legend-item">
                                    <span class="color-box" style="background-color: red;"></span>
                                    <span class="legend-text">Très Élevé</span>
                                </div>
                            </div>
                            <div class="progressbar-container" id="progress-container"></div>
                        </div>
                    </div>
                    <div class="col-lg-12" style="text-align: center; margin-top: 5%;">
                        <h3 style="text-transform: none; font-size: 33px; padding: 0% 20%; color:#2C3E50;">Recommandation à l'intention des sportifs et des personnes qui sont exposées à la pollution de l'air.</h3>
                    </div>
                    <div class="section-sportif col-lg-7" style="margin-top: 5%;">
                        <p class="section-title"> Sport</p>
                        <p id="text-sport"></p>
                    </div>
                    <div class="sportif-image col-lg-5" style="margin-top: 5%;">
                        <img src="https://static.vecteezy.com/ti/vecteur-libre/p1/2612960-groupe-de-course-personnes-personnes-en-vetements-de-sport-jogging-personnes-athlete-personnes-sportives-gratuit-vectoriel.jpg" alt="">
                    </div>

                    <div class="vulnerable-image col-lg-5" style="margin-top: 5%;">
                        <img src="https://www.cdad-ca-rennes.fr/wp-content/uploads/2022/04/sans-titre-2.png" alt="">
                    </div>
                    <div class="section-vulnerable col-lg-7" style="margin-top: 5%;">
                        <p class="section-title">Santé </p>
                        <p id="text-sante"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1">

        </div>
    </div>
    <div class="row container-data map-container">
        <div class="col-lg-5 col-sm-12 col-xs-12">
            <!-- <div class="col-lg-8 col-sm-6 col-xs-12"> -->
            <div class="container-map" id="main-map">

            </div>
            <!-- </div> -->
        </div>
        <div class="col-lg-7 col-sm-12 col-xs-12">
            <div class="calendar-container">
                <div class="calendar" id="calendar"></div>
            </div>
            <p class="section-title" id="bilan-title">Évolution des particules (PM) aujourd’hui.</p>
            <canvas id="airQualityChartday1"></canvas>
            
            <!-- <canvas id="airQualityPieChart"></canvas> -->
        </div>
        <div class="row col-lg-12 hidden">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10 col-sm-12 col-xs-12">
                <div id="pieConatiner-chart">
                    
                </div>
            </div>
            <div class="col-lg-1">
            </div>
        </div>
        <div class="row col-lg-12">
            <!-- <div class="col-lg-8 col-sm-6 col-xs-12">
                <button class="btn btn-primary marker-change" style="margin-left:5px;" data-key="heat" id="circleMarker"><i class="fa fa-circle"></i></button>
                <button class="btn btn-primary marker-change" style="margin-left:5px;" data-key="map" id="mapMarker"><i class="fa fa-map-marker"></i></button>
                <div class="container-map" id="main-map">

                </div>
            </div> -->
        </div>
    </div>
    <div class="container-data tableau-container hidden" id="main-tableau">
        <p class="section-title">Liste de données.</p>
        <input class="text-search" type="text" id="searchInput" placeholder="Recherchez dans le tableau....">
        <div class="table-container">
            <table id="airQualityTable">
                <thead>
                    <tr>
                        <th data-column="id_sensor" data-order="asc">
                            ID Capteur <span class="sort-icon"></span>
                        </th>
                        <th data-column="pm1" data-order="asc">
                            PM1 (µg/m³) <span class="sort-icon"></span>
                        </th>
                        <th data-column="pm25" data-order="asc">
                            PM2.5 (µg/m³) <span class="sort-icon"></span>
                        </th>
                        <th data-column="pm10" data-order="asc">
                            PM10 (µg/m³) <span class="sort-icon"></span>
                        </th>
                        <th data-column="time" data-order="desc">
                            Date & Heure <span class="sort-icon"></span>
                        </th>
                        <th data-column="latitude" data-order="asc">
                            Latitude <span class="sort-icon"></span>
                        </th>
                        <th data-column="longitude" data-order="asc">
                            Longitude <span class="sort-icon"></span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="pagination">
            <button id="prevPage" disabled>⬅ Précédent</button>
            <span id="pageInfo"></span>
            <button id="nextPage">Suivant ➡</button>
        </div>
        <p class="section-title">Récapitulatif quotidien de la qualité de l'air.</p>
        <input class="text-search" type="text" id="searchInput-day" placeholder="Recherchez dans le tableau....">
        <div class="table-container">
            <table id="dailyReportTable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>PM1</th>
                        <th>PM2.5</th>
                        <th>PM10</th>
                        <th>Conclusion</th> <!-- Nouvelle colonne pour la conclusion -->
                    </tr>
                </thead>
                <tbody>
                    <!-- Les données seront insérées ici par JavaScript -->
                </tbody>
            </table>
        </div>
        <div class="pagination">
            <button id="prevPageAnalyse" disabled>⬅ Précédent</button>
            <span id="pageInfoAnalyse"></span>
            <button id="nextPageAnalyse">Suivant ➡</button>
        </div>
    </div>
    <div class="container-data analyse-container hidden" id="main-analyse">
        <!-- Section des seuils -->
        <div class="threshold-section">
            <div class="threshold-title">Seuils de concentration des particules (µg/m³)</div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="threshold-title">Seuils instantanés.</div>
                <table class="threshold-table">
                    <thead>
                        <tr>
                            <th>Particule</th>
                            <th>Faible</th>
                            <th>Modéré</th>
                            <th>Élevé </th>
                            <th>Très Élevé</th>
                        </tr>
                    </thead>
                    <tbody id="threshold-table-body"></tbody>
                </table>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="threshold-title">Seuils journalières</div>
                <table class="threshold-table">
                    <thead>
                        <tr>
                            <th>Particule</th>
                            <th>Faible</th>
                            <th>Modéré</th>
                            <th>Élevé </th>
                            <th>Très Élevé</th>
                        </tr>
                    </thead>
                    <tbody id="threshold-table-day-body"></tbody>
                </table>
            </div>

            <!-- Légende des couleurs -->
            <div class="legend-container">
                <div class="legend-item">
                    <span class="color-box" style="background-color: green;"></span>
                    <span class="legend-text">Faible</span>
                </div>
                <div class="legend-item">
                    <span class="color-box" style="background-color: yellow;"></span>
                    <span class="legend-text">Modéré</span>
                </div>
                <div class="legend-item">
                    <span class="color-box" style="background-color: orange;"></span>
                    <span class="legend-text">Élevé</span>
                </div>
                <div class="legend-item">
                    <span class="color-box" style="background-color: red;"></span>
                    <span class="legend-text">Très Élevé</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-xs-12">
                <p class="section-title" id="bilan-title2">Évolution des particules (PM) aujourd’hui.</p>
                <canvas id="airQualityChartday2"></canvas>
            </div>
            <div class="col-sm-12 col-lg-12 col-xs-12">
                <p class="section-title">Évaluation journalière</p>
                <div class="filters-dyn" style="justify-content:center;">
                    <div class="input-group">
                        <input type="date" id="debutjour" name="debutjour">
                        <label for="debutjour">From</label>
                    </div>
                    <div class="input-group">
                        <input type="date" id="finjour" name="finjour">
                        <label for="finjour">To</label>
                    </div>
                </div>
                <canvas id="airQualityChart"></canvas>
            </div>
            <div class="col-sm-12 col-lg-12 col-xs-12">
                <p class="section-title">Évaluation mensuel</p>
                <div class="filters-dyn" style="justify-content:center;">
                    <div class="input-group">
                        <input type="month" id="debutmois" name="debutmois">
                        <label for="debutmois">From</label>
                    </div>
                    <div class="input-group">
                        <input type="month" id="finmois" name="finmois">
                        <label for="finmois">To</label>
                    </div>
                </div>
                <canvas id="monthAirQualityChart"></canvas>
            </div>
            <p class="section-title">Évaluation globale de la variation des particules pendant la journée.</p>
            <!-- <div class="col-sm-6 col-lg-6 col-xs-12"> -->

            <div class="progressbar-container" id="progress-container1"></div>
            <canvas id="combined-chart"></canvas>
            <!-- </div>
            <div class="col-sm-6 col-lg-6 col-xs-12"> -->
            <!-- </div> -->
        </div>

        <p class="section-title" id="barchart-title">Évaluation de la quantité de particules détectées par capteur.</p>
        <canvas id="barChart"></canvas>

        <p class="section-title">Répartition des particules par capteur.</p>
        <div style="display: flex; justify-content: center; gap: 30px; width: 100%; max-width: 100%; flex-wrap: wrap; justify-content: space-between;">
            <canvas id="pm1Chart" style="flex: 1; max-width: 30%;"></canvas>
            <canvas id="pm25Chart" style="flex: 1; max-width: 30%;"></canvas>
            <canvas id="pm10Chart" style="flex: 1; max-width: 30%;"></canvas>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
    var rawData = "";
    var rawDataMap = {};
    var typeActivated = "map";
    var datelimitmin = "";
    var datelimitmax = "";
    costum.armotrackData = "";
    var monthlyData = "";
    var lastTimestamp = null;
    var dailyData = "";
    var seuilInstant = {
        pm1: [10, 20, 30],
        pm25: [12, 35.4, 55.4],
        pm10: [20, 50, 100]
    };

    var messageJour = {
        'bonne': "La qualité de l’air est bonne à Fianarantsoa. Les concentrations de particules fines PM sont nettement inférieures au seuil recommandé par l’Organisation Mondiale de la Santé (OMS) d'après les données relevées par les capteurs. Profitez pleinement de vos activités en extérieur. ",
        'moyenne': " La qualité de l’air est moyenne à Fianarantsoa. Les concentrations de particules fines PM sont proches du seuil recommandé par l’Organisation Mondiale de la Santé (OMS) d'après les données relevées par les capteurs. Les personnes sensibles peuvent ressentir une légère gêne lors d'efforts prolongés.",
        'mauvaise': "La qualité de l’air est mauvaise à Fianarantsoa. Les concentrations de particules fines PM dépassent le seuil recommandé par l’Organisation Mondiale de la Santé (OMS) d'après les données relevées par les capteurs. Il est conseillé de limiter les activités physiques en extérieur, surtout pour les personnes sensibles.",
        'dangereux': "La qualité de l’air est dangereuse à Fianarantsoa. Les concentrations de particules fines PM sont très largement supérieures au seuil recommandé par l’Organisation Mondiale de la Santé (OMS) d'après les données relevées par les capteurs. Il est fortement recommandé de rester à l'intérieur et d'éviter toute activité physique en extérieur."
    }

    var seuilJournal = {
        pm1: [8, 17, 26],
        pm25: [7.5, 15, 25],
        pm10: [22.5, 45, 75],
        aqi: [50, 150, 300]
    }

    var AQI_BREAKPOINTS = {
        pm25: [{
                min: 0.0,
                max: 12.0,
                aqiMin: 0,
                aqiMax: 50
            },
            {
                min: 12.1,
                max: 35.4,
                aqiMin: 51,
                aqiMax: 100
            },
            {
                min: 35.5,
                max: 55.4,
                aqiMin: 101,
                aqiMax: 150
            },
            {
                min: 55.5,
                max: 150.4,
                aqiMin: 151,
                aqiMax: 200
            },
            {
                min: 150.5,
                max: 250.4,
                aqiMin: 201,
                aqiMax: 300
            },
            {
                min: 250.5,
                max: 500.4,
                aqiMin: 301,
                aqiMax: 500
            }
        ],
        pm10: [{
                min: 0,
                max: 54,
                aqiMin: 0,
                aqiMax: 50
            },
            {
                min: 55,
                max: 154,
                aqiMin: 51,
                aqiMax: 100
            },
            {
                min: 155,
                max: 254,
                aqiMin: 101,
                aqiMax: 150
            },
            {
                min: 255,
                max: 354,
                aqiMin: 151,
                aqiMax: 200
            },
            {
                min: 355,
                max: 424,
                aqiMin: 201,
                aqiMax: 300
            },
            {
                min: 425,
                max: 604,
                aqiMin: 301,
                aqiMax: 500
            }
        ]
    };

    var noDataPlugin = {
        id: 'noDataMessage',
        beforeDraw: function(chart) {
            var ctx = chart.ctx;
            var width = chart.width;
            var height = chart.height;

            if (chart.data.datasets.every(dataset => dataset.data.length === 0)) {
                ctx.save();
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.font = '16px Arial';
                ctx.fillStyle = 'gray';
                ctx.fillText('Aucune donnée disponible', width / 2, height / 2);
                ctx.restore();
            }
        }
    };

    function getColor(value, type, isDaily = false) {
        var colors = ["green", "yellow", "orange", "red"];
        var seuils = isDaily ? seuilJournal : seuilInstant; 

        if (!seuils[type]) return "gray"; 

        var seuil = seuils[type];

        if (value <= seuil[0]) return "green"; 
        if (value <= seuil[1]) { 
            var ratio = (value - seuil[0]) / (seuil[1] - seuil[0]);
            return interpolateColor("green", "yellow", ratio);
        }
        if (value <= seuil[2]) { 
            var ratio = (value - seuil[1]) / (seuil[2] - seuil[1]);
            return interpolateColor("yellow", "orange", ratio);
        }

        return "red"; 
    }

    function interpolateColor(color1, color2, ratio) {
        function hexToRgb(hex) {
            var bigint = parseInt(hex.slice(1), 16);
            return [(bigint >> 16) & 255, (bigint >> 8) & 255, bigint & 255];
        }

        function rgbToHex(r, g, b) {
            return `#${((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)}`;
        }

        var colorMap = {
            "green": "#00FF00",
            "yellow": "#FFFF00",
            "orange": "#FFA500",
            "red": "#FF0000"
        };

        var rgb1 = hexToRgb(colorMap[color1]);
        var rgb2 = hexToRgb(colorMap[color2]);

        var r = Math.round(rgb1[0] + (rgb2[0] - rgb1[0]) * ratio);
        var g = Math.round(rgb1[1] + (rgb2[1] - rgb1[1]) * ratio);
        var b = Math.round(rgb1[2] + (rgb2[2] - rgb1[2]) * ratio);

        return rgbToHex(r, g, b);
    }

    function calculateAQI(concentration, breakpoints) {
        for (var i = 0; i < breakpoints.length; i++) {
            var {
                min,
                max,
                aqiMin,
                aqiMax
            } = breakpoints[i];
            if (concentration >= min && concentration <= max) {
                return Math.round(((aqiMax - aqiMin) / (max - min)) * (concentration - min) + aqiMin);
            }
        }
        return null;
    }

    function getGlobalAQI(pm10, pm25) {
        var aqiPm10 = calculateAQI(pm10, AQI_BREAKPOINTS.pm10);
        var aqiPm25 = calculateAQI(pm25, AQI_BREAKPOINTS.pm25);
        var overallAQI = Math.max(aqiPm10, aqiPm25);

        return overallAQI;
    }

    function setCalendarViews(data) {
        var calendar = $('#calendar');
        calendar.empty();

        var availableYears = [...new Set(data.map(d => new Date(d.date).getFullYear()))].sort((a, b) => b - a);
        var currentYear = new Date().getFullYear();
        var selectedYear;

        if (availableYears.includes(currentYear)) {
            selectedYear = currentYear;
        } else {
            selectedYear = availableYears[0];
        }

        var dataByDate = {};

        data.forEach(d => {
            dataByDate[d.date] = d;
        });

        function renderCalendar(year) {
            var calendar = $('#calendar');
            calendar.empty();
            var calendarContainer = $('<div class="calendar-container"></div>');
            var weekdaysColumn = $('<div class="weekdays-column"></div>');
            var weekdays = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
            weekdays.forEach(day => {
                weekdaysColumn.append(`<div class="weekday">${day}</div>`);
            });
            calendarContainer.append(weekdaysColumn);

            var startDate = new Date(year, 0, 1);
            var endDate = new Date(year, 11, 31);
            var firstDay = new Date(startDate);
            while (firstDay.getDay() !== 1) {
                firstDay.setDate(firstDay.getDate() - 1);
            }

            var totalWeeks = Math.ceil((endDate - firstDay) / (7 * 24 * 60 * 60 * 1000));
            var currentDate = new Date(firstDay);
            
            var contributionCount = 0;
            var monthsHeader = $('<div class="months-header"></div>');
            var monthPositions = {};

            var aujourdhui = new Date();
            var aujourformat = aujourdhui.toISOString().slice(0, 10);

            if(typeof dataByDate[aujourformat] == 'undefined') {
                var dataArray = Object.values(data);

                var latestEntry = dataArray.reduce((latest, current) => {
                    return new Date(current.date) > new Date(latest.date) ? current : latest;
                });

                aujourdhui = new Date(latestEntry.date);
            }

            var aujourdhuiformater = aujourdhui.toLocaleDateString('fr-FR', {
                day: 'numeric',
                month: 'long',
                year: 'numeric'
            });
            aujourformat = aujourdhui.toISOString().slice(0, 10);

            var alertText = setAlertQualiteAir(aujourdhui, data);
            $("#alert-text").html(`<p>${alertText}</p>`);

            for (var weekIndex = 0; weekIndex < totalWeeks; weekIndex++) {
                var weekColumn = $('<div class="week-column"></div>');
                for (var dayIndex = 0; dayIndex < 7; dayIndex++) {
                    var dateString = formateDateCam(currentDate);
                    var dayData = dataByDate[dateString];

                    var className = dayData ? (dayData.conclusion.includes('dangereux') ? 'dangereux' : dayData.conclusion.includes('mauvaise') ? 'mauvaise' : dayData.conclusion.includes('moyenne') ? 'moyenne' : 'bonne') : 'empty';
                    if (dayData) contributionCount++;
                    var datet = new Date(dateString);
                    var formattedDate = datet.toLocaleDateString("fr-FR", {
                        day: "2-digit",
                        month: "long",
                        year: "numeric"
                    });
                    var apopover = "";
                    var datavaleur = '';
                    if (typeof dayData != "undefined") {
                        apopover = `<a title="${formattedDate}" 
                                        href="javascript:;"
                                        data-toggle="popover" 
                                        data-trigger="focus"
                                        data-content="<b>PM1 : </b>${parseFloat(String(dayData.pm1).replace(",", ".")).toFixed(1)} µg/m³ <br/>
                                        <b>PM2.5 : </b> ${parseFloat(String(dayData.pm25).replace(",", ".")).toFixed(1)} µg/m³ <br/>
                                        <b>PM10 : </b> ${parseFloat(String (dayData.pm10).replace(",", ".")).toFixed(1)} µg/m³ <br/>
                                        ${dayData.conclusion}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>`;
                        datavaleur = `data-pm1='${parseFloat(String(dayData.pm1).replace(",", ".")).toFixed(1)}'
                                    data-pm25='${parseFloat(String(dayData.pm25).replace(",", ".")).toFixed(1)}'
                                    data-pm10='${parseFloat(String (dayData.pm10).replace(",", ".")).toFixed(1)}'`;

                        if (dateString == aujourformat) {
                            $("#interpret-date").html(`${aujourdhuiformater}`);
                            $("#pm1-value").html(parseFloat(String(dayData.pm1).replace(",", ".")).toFixed(1) + ' µg/m³');
                            $("#pm25-value").html(parseFloat(String(dayData.pm25).replace(",", ".")).toFixed(1) + ' µg/m³');
                            $("#pm10-value").html(parseFloat(String(dayData.pm10).replace(",", ".")).toFixed(1) + ' µg/m³');
                            $("#interpret-conclu").html(messageJour[className]);
                            $(".card-box").addClass(className);
                            className += ' day-calendar-active';
                        }
                    }
                    var dayCell = $(`<div class="day ${className}" 
                                        title="${dateString}(${className !== 'empty' ? className : 'Pas de données'})" 
                                        data-date="${dateString}" ${datavaleur} data-concl='${className}'>
                                        ${apopover}
                                        </div>`);
                    dayCell.on('click', function(event) {
                        $('.day').removeClass('day-calendar-active');
                        var dateValue = $(this).data('date');
                        updateViews(dateValue);
                        $(this).addClass('day-calendar-active');
                        event.stopPropagation();
                    });
                    dayCell.find('[data-toggle="popover"]').popover({
                        html: true,
                        template: '<div class="popover" role="tooltip">' +
                            '<div class="arrow"></div>' +
                            `<h3 class="popover-title ${className}"></h3>` +
                            '<div class="popover-content"></div>' +
                            '</div>'
                    });
                    if (dayData) dayCell.data('details', dayData);
                    weekColumn.append(dayCell);

                    var month = currentDate.toLocaleString('fr-FR', {
                        month: 'short'
                    });
                    if (!monthPositions[month]) {
                        monthPositions[month] = weekIndex;
                    }
                    currentDate.setDate(currentDate.getDate() + 1);
                }
                calendarContainer.append(weekColumn);
            }

            calendar.append(monthsHeader);
            calendar.append(calendarContainer);
            calendar.append(`<div class="contribution-count">Données de ${year}</div>`);

            var yearButtons = $('<div class="year-buttons"></div>');
            availableYears.forEach(y => {
                var yearButton = $(`<button class="year-button" data-year="${y}">${y}</button>`);
                if (y === selectedYear) yearButton.addClass('active');
                yearButton.on('click', function() {
                    selectedYear = parseInt($(this).data('year'));
                    renderCalendar(selectedYear);
                });
                yearButtons.append(yearButton);
            });
            calendar.append(yearButtons);

            var legend = $(`<div class="legend">
                <div class="legend-color bonne"></div> Bonne
                <div class="legend-color moyenne"></div> Moyenne
                <div class="legend-color mauvaise"></div> Mauvaise
                <div class="legend-color dangereux"></div> Dangereux
                <div class="legend-color empty"></div> Vide
            </div>`);
            calendar.append(legend);

            Object.keys(monthPositions).forEach(month => {
                var weekIndex = monthPositions[month];
                var monthIndicator = $(`<div class="month-indicator">${month}</div>`);
                monthsHeader.append(monthIndicator);
                monthIndicator.css('left', (weekIndex * 18) + 5 + 'px');
            });
        }

        function formateDateCam(date) {
            var year = date.getFullYear();
            var month = String(date.getMonth() + 1).padStart(2, '0');
            var day = String(date.getDate()).padStart(2, '0');
            return `${year}-${month}-${day}`;
        }

        renderCalendar(selectedYear);
        $('[data-toggle="popover"]').popover();
    }

    function updateViews(givenDate) {
        if (givenDate) {
            var day = $(`div[data-date="${givenDate}"]`);
            $("#pm1-value").html(day.data('pm1') + ' µg/m³');
            $("#pm25-value").html(day.data('pm25') + ' µg/m³');
            $("#pm10-value").html(day.data('pm10') + ' µg/m³');

            document.querySelectorAll('.card-box').forEach(divElement => {
                var classes = Array.from(divElement.classList);

                classes.forEach(className => {
                    if (className !== 'card-box') {
                        divElement.classList.remove(className);
                    }
                });
            });

            $(".card-box").addClass(day.data('concl'));
            $("#interpret-conclu").html(messageJour[day.data('concl')]);

            var startOfDay = new Date(givenDate);
            startOfDay.setHours(0, 0, 0, 0);

            var endOfDay = new Date(givenDate);
            endOfDay.setHours(23, 59, 59, 999);

            var startFormatted = startOfDay.toLocaleString('sv-SE').replace(" ", "T").slice(0, 16);
            var endFormatted = endOfDay.toLocaleString('sv-SE').replace(" ", "T").slice(0, 16);
            var date = new Date(givenDate);
            var formattedDate = date.toLocaleDateString("fr-FR", {
                day: "2-digit",
                month: "long",
                year: "numeric"
            });
            $("#debutTime").val(startFormatted);
            $("#finTime").val(endFormatted);

            $("#bilan-title").html(`Évolution des particules (PM) le ${formattedDate}`);
            $("#bilan-title2").html(`Évolution des particules (PM) le ${formattedDate}`);
            $("#interpret-date").html(`${formattedDate}`);
            // $("#circleMarker").attr('data-key', 'map');
            $("#interpret-predict").html(analyserQualiteAir(getLastNDaysData(rawData, 30, givenDate)));


            var todayData = getTodaySensorData(rawData, givenDate);
            var aggregatedData = aggregateDataByTime(todayData);
            var chartData = prepareChartData(aggregatedData);
            createChart(chartData, "airQualityChartday1");
            createChart(chartData, "airQualityChartday2");

            $("#barchart-title").html(`Évaluation de la quantité de particules détectées par capteur le ${formattedDate}`);
            generateBarChart(todayData);
            generatePieCharts(todayData);

            rawDataMap = filterAndAverageData(rawData, 'map', startFormatted, endFormatted);
            var newrawDataMap = filterAndAverageData(rawData, 'heat', startFormatted, endFormatted);
            mapAtmotrack.clearMap();
            mapAtmotrack.addElts(newrawDataMap);

        }
    }

    function analyserQualiteAir(data) {
        var joursAnalyse = 30;
        var aujourdHui = new Date().toISOString().split("T")[0];
        var derniersJours = data.slice(-joursAnalyse);
        var valeurAujourdhui = data.find(d => d.date === aujourdHui);

        var moyennePM1 = 0,
            moyennePM10 = 0,
            moyennePM25 = 0;

        derniersJours.forEach(d => {
            moyennePM1 += d.pm1;
            moyennePM10 += d.pm10;
            moyennePM25 += d.pm25;
        });

        moyennePM1 /= joursAnalyse;
        moyennePM10 /= joursAnalyse;
        moyennePM25 /= joursAnalyse;

        var premierJour = derniersJours[0];
        var dernierJour = derniersJours[derniersJours.length - 1];
        var tendancePM1 = dernierJour.pm1 - premierJour.pm1;
        var tendancePM10 = dernierJour.pm10 - premierJour.pm10;
        var tendancePM25 = dernierJour.pm25 - premierJour.pm25;

        var tendanceGenerale = "stable";
        if (tendancePM1 > 2 || tendancePM10 > 5 || tendancePM25 > 3) {
            tendanceGenerale = "augmentation";
        } else if (tendancePM1 < -2 || tendancePM10 < -5 || tendancePM25 < -3) {
            tendanceGenerale = "baisse";
        }

        var message = "";

        if (valeurAujourdhui) {
            if (valeurAujourdhui.pm10 > moyennePM10 * 1.5 || valeurAujourdhui.pm25 > moyennePM25 * 1.5) {
                message = "Une augmentation importante des polluants est observée aujourd'hui, ce qui pourrait impacter la qualité de l'air dans les jours à venir.";
            } else {
                if (tendanceGenerale === "augmentation") {
                    message = "Les conditions devraient se dégrader légèrement, avec une possible détérioration de la qualité de l'air.";
                } else if (tendanceGenerale === "baisse") {
                    message = "Les conditions devraient s'améliorer dans les prochains jours, avec une amélioration progressive de la qualité de l'air.";
                } else {
                    message = "Les conditions devraient rester similaires, avec une qualité de l'air stable.";
                }
            }
        } else {
            if (tendanceGenerale === "augmentation") {
                message = "Les conditions devraient se dégrader légèrement, avec une possible détérioration de la qualité de l'air.";
            } else if (tendanceGenerale === "baisse") {
                message = "Les conditions devraient s'améliorer dans les prochains jours, avec une amélioration progressive de la qualité de l'air.";
            } else {
                message = "Les conditions devraient rester similaires, avec une qualité de l'air stable.";
            }
        }

        return message;
    }

    function analyserIntervalle3Heures(donnees) {
        var meilleurIntervalle = null;
        var pireIntervalle = null;
        var niveauParticulesMinimal = Infinity;
        var niveauParticulesMaximal = -Infinity;

        var intervalles3Heures = [];
        for (var heureDebut = 4; heureDebut <= 15; heureDebut++) {
            for (var minuteDebutIndex = 0; minuteDebutIndex <= 3; minuteDebutIndex++) {
                var intervalle = [];
                for (var i = 0; i < 12; i++) {
                    var heure = heureDebut + Math.floor((minuteDebutIndex * 15 + i * 15) / 60);
                    var minute = (minuteDebutIndex * 15 + i * 15) % 60;
                    if (donnees[heure] && donnees[heure][minute]) {
                        intervalle.push({
                            heure: heure,
                            minute: minute,
                            data: donnees[heure][minute],
                        });
                    }
                }
                if (intervalle.length === 12) {
                    intervalles3Heures.push(intervalle);
                }
            }
        }

        intervalles3Heures.forEach((intervalle) => {
            var niveauParticulesTotal = 0;
            intervalle.forEach((creneau) => {
                niveauParticulesTotal +=
                    creneau.data.pm1 + creneau.data.pm25 + creneau.data.pm10;
            });
            var niveauParticulesMoyen = niveauParticulesTotal / (intervalle.length * 3);

            if (niveauParticulesMoyen < niveauParticulesMinimal) {
                niveauParticulesMinimal = niveauParticulesMoyen;
                meilleurIntervalle = `${intervalle[0].heure}h${intervalle[0].minute} - ${
        intervalle[11].heure
      }h${intervalle[11].minute}`;
            }

            if (niveauParticulesMoyen > niveauParticulesMaximal) {
                niveauParticulesMaximal = niveauParticulesMoyen;
                pireIntervalle = `${intervalle[0].heure}h${intervalle[0].minute} - ${
        intervalle[11].heure
      }h${intervalle[11].minute}`;
            }
        });

        return {
            meilleurIntervalle,
            pireIntervalle
        };
    }

    function genererTexteConseils(resultats) {
        var texteSportifs = "";
        var texteVulnerables = "";

        if (resultats.meilleurIntervalle) {
            texteSportifs += `Les meilleurs moments pour faire du sport sont généralement entre ${resultats.meilleurIntervalle}. Pendant ces périodes, les niveaux de particules fines (PM1, PM2.5 et PM10) sont les plus bas, ce qui signifie que la qualité de l'air est la plus propre.\n\n`;
            texteVulnerables += `Pour les personnes vulnérables (enfants, personnes âgées, personnes souffrant de maladies respiratoires ou cardiaques), les meilleurs moments pour sortir et profiter de l'extérieur sont également entre ${resultats.meilleurIntervalle}. Pendant ces heures, la qualité de l'air est la plus favorable, ce qui réduit les risques de problèmes de santé liés à la pollution.\n\n`;
        } else {
            texteSportifs += `Nous ne disposons pas d'informations suffisantes pour identifier les meilleurs moments pour faire du sport.\n\n`;
        }

        if (resultats.pireIntervalle) {
            texteSportifs += `<br>Il est recommandé d'éviter de faire du sport entre ${resultats.pireIntervalle}. Pendant ces périodes, les niveaux de particules fines sont les plus élevés, ce qui pourrait être nocif pour votre santé.\n`;
            texteVulnerables += `<br>Il est déconseillé de sortir entre ${resultats.pireIntervalle} en raison des niveaux élevés de particules fines.\n`;
        } else {
            texteSportifs += `Nous ne disposons pas d'informations suffisantes pour identifier les pires moments pour faire du sport.\n`;
        }

        return {
            texteSportifs,
            texteVulnerables
        };
    }

    function setAlertQualiteAir(aujourdhui, data) {

        var limiteDate = new Date(aujourdhui);
        limiteDate.setDate(limiteDate.getDate() - 10);

        var aujourdhuiStr = aujourdhui.toISOString().slice(0, 10);
        var limiteDateStr = limiteDate.toISOString().slice(0, 10);

        var derniersJours = Object.values(data).filter(entry => {
            var entryDateStr = entry.date; 
            return entryDateStr >= limiteDateStr && entryDateStr < aujourdhuiStr;
        });

        if (!Array.isArray(derniersJours) || derniersJours.length === 0) {
            return "Aucune donnée disponible pour analyser la qualité de l'air.";
        }

        let counts = { mauvaise: 0, dangereux: 0, moyenne: 0 };

        derniersJours.forEach(entry => {
            if (entry.conclusion.includes("mauvaise")) counts.mauvaise++;
            if (entry.conclusion.includes("dangereux")) counts.dangereux++;
            if (entry.conclusion.includes("moyenne")) counts.moyenne++;
        });

        let total = derniersJours.length;
        let seuilMajorite = Math.ceil(total / 2);

        if (counts.dangereux >= seuilMajorite) {
            return "🚨 ALERTE : La qualité de l'air est dangereuse depuis plusieurs jours. " +
                "Évitez toute activité extérieure prolongée et portez un masque si nécessaire.";
        } 
        
        if (counts.mauvaise + counts.dangereux >= seuilMajorite) {
            return "⚠️ ATTENTION : La qualité de l'air est préoccupante (mauvaise ou dangereuse). " +
                "Limitez vos sorties et privilégiez les espaces ventilés.";
        } 
        
        if (counts.mauvaise >= seuilMajorite) {
            return "🔶 La qualité de l'air est dégradée. " +
                "Les personnes sensibles doivent limiter leurs activités en extérieur.";
        }

        if (counts.moyenne >= seuilMajorite) {
            return "🔷 La qualité de l'air est généralement moyenne. " +
                "La situation est stable mais reste surveillée. Aucune précaution spécifique n'est nécessaire.";
        }

        return "✅ Bonne nouvelle : La qualité de l'air est globalement satisfaisante.";
    }
    // 

    async function fetchLimitedData(from = null, to = null, idSensor = null) {
        var apiKey = "x4bbgnq0jjhuot55i5cx1x3aosirjpkz";
        var baseUrl = "https://cdn.atmotrack.fr/api/v2.3/data";

        var params = new URLSearchParams({
            // limit: limit,
            sort: "desc",
            field: "pm1,pm25,pm10",
            interval: 15
        });

        var dateNow = new Date();
        var tenDaysAgo = new Date();
        tenDaysAgo.setDate(dateNow.getDate() - 180);
        tenDaysAgo.setHours(0, 0, 0, 0);

        if (idSensor != null && idSensor != "") {
            params.append('idSensors', idSensor);
        }

        if (from != null && from != "") {
            params.append("from", from.toISOString());
        } else {
            params.append("from", tenDaysAgo.toISOString());
        }

        if (to != null && to != "") {
            params.append("to", to.toISOString());
        } else {
            params.append("to", dateNow.toISOString());
        }

        var url = `${baseUrl}?${params.toString()}`;

        try {
            var response = await fetch(url, {
                method: "GET",
                headers: {
                    "Authorization": `Bearer ${apiKey}`,
                    "Content-Type": "application/json"
                }
            });

            if (!response.ok) {
                throw new Error(`Erreur: ${response.status}`);
            }

            var data = await response.json();

            return data;
        } catch (error) {
            toastr.error("Erreur lors de la récupération des données :", error);
        }
    }

    function formatTime(timestamp) {
        var date = new Date(timestamp);
        return date.toLocaleTimeString();
    }
    
    var d3MapAtmotrack = {
        container: "#main-map",
        activePopUp: true,
        showLegende: true,
        dynamicLegende: false,
        legende: [
            'pm1',
            'pm25',
            'pm10'
        ],
        groupBy: 'data',
        legendeVar: 'data',
        markerType: 'default',
        clusterType: 'pie',
        mapOpt: {
            zoomControl: false
        },
        mapCustom: {
            tile: "maptiler",
            markers: {
                getMarker: function(data) {
                    var imgM = "<?php echo Yii::app()->getModule('map')->assetsUrl ?>/images/markers/atmo-marker.png";
                    return imgM;
                },
            },
            transformDataCustom : function(allData, type, _context){
                for (const [key, legende] of Object.entries(_context.getOptions().legende)) {
                    if(typeof type[_context.getOptions().legendeVar] != "undefined" && typeof type[_context.getOptions().legendeVar][legende] != "undefined"){
                        allData[legende] = allData[legende] || 0;
                        allData[legende] += type[_context.getOptions().legendeVar][legende];  
                    }
                }
                mylog.log("transformDataCustom", allData, type);
                return allData;
            }
        },
        elts: {

        }
    };
    var mapAtmotrack = null;
    var mapD3Atmotrack = null;
    function initMapV2(){
        mapAtmotrack = new CoMap({
            container: "#main-map",
            activePopUp: true,
            activeCluster: false,
            mapOpt: {
                btnHide: false,
                doubleClick: true,
                scrollWheelZoom: true,
                zoom: 3,
            },
            mapCustom: {
                tile: "maptiler",
                markers: {
                    getMarker: function(data) {
                        var imgM = "<?php echo Yii::app()->getModule('map')->assetsUrl ?>/images/markers/atmo-marker.png";
                        return imgM;
                    },
                    icon: {
                        getIcon: function(_params) {
                            let pm10 = _params.elt.pm10;
                            let pm25 = _params.elt.pm25;
                            let valueaqi = getGlobalAQI(pm10, pm25);
                            let taille = valueaqi - 20;
                            return L.divIcon({
                                className: "air-div-marker",
                                iconAnchor: [23, 45],
                                iconSize: [45, 45],
                                shadowUrl: '',
                                labelAnchor: [-45, 0],
                                popupAnchor: [0, -45],
                                shadowSize: [68, 95],
                                shadowAnchor: [22, 94],
                                html: `<div class='marker-atmo-image' style='background-color:${getColor(_params.elt.pm25, 'pm25', true)}; width:${taille}px; height:${taille}px' ></div>`
                            });
                        }
                    },
                },
                addMarker: function(_context, params) {
                    if (params.elt && params.elt.geo && params.elt.geo.latitude && params.elt.geo.longitude) {
                        if (!params.opt)
                            params.opt = {}

                        if (typeof params.elt.type != "undefined" && params.elt.type == 'heat')
                            params.opt.icon = _context.getOptions().mapCustom.markers.icon.getIcon(params);
                        else
                            params.opt.icon = _context.getOptions().mapCustom.icon.getIcon(params);

                        var latLon = [params.elt.geo.latitude, params.elt.geo.longitude];
                        _context.setDistanceToMax(latLon);

                        var marker = L.marker(latLon, params.opt);
                        _context.getOptions().markerList[params.elt.id] = marker;

                        if (_context.getOptions().activePopUp) {
                            _context.addPopUp(marker, params.elt)
                        }

                        _context.getOptions().arrayBounds.push(latLon)

                        if (_context.getOptions().activeCluster) {
                            _context.getOptions().markersCluster.addLayer(marker)
                        } else {
                            marker.addTo(_context.getMap());
                            if (params.center) {
                                _context.getMap().panTo(latLon)
                            }
                        }

                        if (_context.getOptions().mapOpt.doubleClick) {
                            marker.on('click', function(e) {
                                // _context.openPopup()
                                coInterface.bindLBHLinks();
                            })
                            marker.on('dbclick', function(e) {
                                _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                                coInterface.bindLBHLinks();
                            })
                        } else {
                            marker.on('click', function(e) {
                                if (_context.getOptions().mapOpt.onclickMarker) {
                                    _context.getOptions().mapOpt.onclickMarker(params)
                                } else {
                                    _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                                    coInterface.bindLBHLinks();
                                }
                            });
                        }

                        if (_context.getOptions().mapOpt.mouseOver) {
                            marker.on('mouseover', function(e) {
                                // _context.openPopup()
                                coInterface.bindLBHLinks();
                            });
                            marker.on('mouseout', function(e) {
                                var thismarker = _context;
                                setTimeout(function() {
                                    thismarker.closePopup();
                                }, 2000);
                            });
                        }
                    }
                },
                getPopup: function(donne) {
                    var popup = "";
                    var {
                        data,
                        geo,
                        id_install,
                        id_sensor,
                        id_group,
                        is_fixed,
                        time
                    } = donne;
                    var date = new Date(time).toLocaleString();

                    var pollutantsHtml = "";
                    if (donne.data) {
                        for (var [key, value] of Object.entries(data)) {
                            pollutantsHtml += `<li><strong>${key.toUpperCase()}:</strong> ${value} &nbsp; µg/m³</li>`;
                        }
                    } else if (donne.type) {
                        pollutantsHtml += `<li><strong>PM1:</strong> ${parseFloat(String(donne.pm1).replace(",", ".")).toFixed(2)} &nbsp; µg/m³</li>`;
                        pollutantsHtml += `<li><strong>PM25:</strong> ${parseFloat(String(donne.pm25).replace(",", ".")).toFixed(2)} &nbsp; µg/m³</li>`;
                        pollutantsHtml += `<li><strong>PM10:</strong> ${parseFloat(String(donne.pm10).replace(",", ".")).toFixed(2)} &nbsp; µg/m³</li>`;
                    }

                    var capteurType = is_fixed ? "fixe" : "mobile"

                    popup += `<div class="popup-content">
                                <h3>Capteur ${id_sensor} (${id_install})</h3>
                                <p><strong>Group :</strong> ${id_group}</p>`

                    if (donne.time) popup += `<p><strong>Date :</strong> ${date}</p>`;

                    popup += `<p><strong>Type de Capteur :</strong> ${capteurType}</p>
                                <p><strong>Position :</strong> ${geo.latitude}, ${geo.longitude}</p>
                                <ul>${pollutantsHtml}</ul>
                            </div>`;
                    popup += `<button onclick="openPreviewsAir('${id_sensor}')" class="btn btn-primary btn-chart">${trad.knowmore}</button>`;

                    return popup;
                }
            }
        });
        
    }

    function addIcon(mapVar){
        var button = L.control({
            position: 'topright'
        });
        button.onAdd = function(map) {
            var div = L.DomUtil.create('div', 'info button btn-type');
            var str = `
                <button class="btn btn-primary marker-change ${typeActivated == "map" ? "active_views" : ''}" style="margin-left:5px;" data-key="map"><i class="fa fa-map-marker"></i></button>
                <button class="btn btn-primary marker-change ${typeActivated == "heat" ? "active_views" : ''}" style="margin-left:5px;" data-key="heat"><i class="fa fa-circle"></i></button>
                <button class="btn btn-primary marker-change ${typeActivated == "mapD3" ? "active_views" : ''}" style="margin-left:5px;" data-key="mapD3"><i class="fa fa-pie-chart" aria-hidden="true"></i></button>
            `;
            div.innerHTML = str;
            return div;
        }
        button.addTo(mapVar.getMap());
        initBtnClick();
    }
    function initMapD3(){
        mapD3Atmotrack = new MapD3(d3MapAtmotrack);
    }
    initMapV2();
    addIcon(mapAtmotrack);

    function openPreviewsAir(params) {
        urlCtrl.openPreview("view/url/costum.views.custom.cocity.previewair", {
            "id_sensor": params
        });
    }

    async function setDataMap(from = null, to = null, _id = null) {
        $('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
        $('#loader-loader<?= $kunik ?>').removeClass('hidden');
        coInterface.showLoader('#content-loader<?= $kunik ?>');

        mapAtmotrack.hideLoader();

        rawData = await fetchLimitedData(from, to, _id);
        costum.armotrackData = rawData;

        var dataGourper = groupByHourAndMinute(rawData);

        datelimitmin = new Date(rawData[rawData.length - 1].time);
        datelimitmax = new Date(rawData[0].time);

        if (!Array.isArray(rawData)) {
            toastr.error("Les données récupérées ne sont pas un tableau.");
            return {};
        }

        rawDataMap = {};
        rawDataMap = prepareDataMap(rawData);

        mylog.log("Francky : ", rawDataMap);

        mapAtmotrack.clearMap();
        mapAtmotrack.addElts(rawDataMap);
        $("#debutTime").val(datelimitmin.toISOString().slice(0, 16));
        $("#finTime").val(datelimitmax.toISOString().slice(0, 16));

        initDateInputs(rawData);
        initMonthInputs(rawData);

        // analyse global du jour
        createProgressBars(dataGourper, "progress-container");
        createProgressBars(dataGourper, "progress-container1")
        createChartDatasets(dataGourper);

        // Exemple d'utilisation (avec vos données)
        var resultats = analyserIntervalle3Heures(dataGourper);
        var texte = genererTexteConseils(resultats);

        $("#text-sport").html(texte.texteSportifs);
        $("#text-sante").html(texte.texteVulnerables);
        // création courbe du jour
        var todayData = getTodaySensorData(rawData);

        var aggregatedData = aggregateDataByTime(todayData);
        var chartData = prepareChartData(aggregatedData);
        createChart(chartData, "airQualityChartday1");
        createChart(chartData, "airQualityChartday2");
        // createParticlePieCharts(chartData, "pieConatiner-chart");

        // createPolarChart(chartData, "airQualityPieChart");

        // analyse
        updateChart(getLastNDaysData(rawData), "Bilan journalière", "airQualityChart");
        updateChart(getLastNDaysData(rawData, 10), "Bilan journalière", "tenday-chart");

        $("#interpret-predict").html(analyserQualiteAir(getLastNDaysData(rawData)));
        updateChart(getLastNMonthsData(rawData).map(d => ({
            ...d,
            date: d.month
        })), "Bilan mensuel", "monthAirQualityChart");

        generateBarChart(rawData);
        generatePieCharts(rawData);
        $("#barchart-title").html('Évaluation de la quantité de particules détectées par capteur.');

        // table
        generateTable();
        dailyData = processData(rawData, "daily");
        setCalendarViews(dailyData);
        fillTableWithDailyData();

        var $btn = $("#active-filters");
        var originalText = $btn.html();
        $btn.html('Filtrer');
        $btn.prop('disabled', false);

        $('#backdrop-loader<?= $kunik ?>').addClass('hidden');
        $('#loader-loader<?= $kunik ?>').addClass('hidden');
    }

    function prepareDataMap(datas) {
        var rawDataMaps = {};

        datas.forEach(item => {
            var sensorId = item.id_sensor;

            if (!rawDataMaps[sensorId] || new Date(item.time) > new Date(rawDataMaps[sensorId].time)) {
                rawDataMaps[sensorId] = {
                    ...item,
                    geo: {
                        "@type": "GeoCoordinates",
                        latitude: item.latitude,
                        longitude: item.longitude
                    }
                };
            }
        });

        return rawDataMaps
    }

    function prepareDataMapD3(datas) {
        var rawDataMaps = {};

        datas.forEach(item => {
            var sensorId = item.id_sensor;
            if(typeof item.data != "undefined" && Object.values(item.data).length > 0){
                Object.keys(item.data).forEach((dataItem) => {
                    if (!rawDataMaps[sensorId+dataItem] || new Date(item.time) > new Date(rawDataMaps[sensorId+dataItem].time)) {
                        rawDataMaps[sensorId+dataItem] = {
                            ...item,
                            geo: {
                                "@type": "GeoCoordinates",
                                latitude: item.latitude,
                                longitude: item.longitude
                            }
                            ,
                            data : {
                                [dataItem] : item.data[dataItem]
                            }
                        };
                    }
                })
            }
            
        });
        

        return rawDataMaps
    }

    function filterAndAverageData(rawData, type = 'heat', timeStart, timeEnd) {
        var startTime = timeStart ? timeStart : $("#debutTime").val();
        var endTime = timeEnd ? timeEnd : $("#finTime").val();
        var start = new Date(startTime).getTime();
        var end = new Date(endTime).getTime();
        var now = Date.now();

        var startCompar = startTime ? new Date(startTime) : null;
        var endCompar = endTime ? new Date(endTime) : null;
        var nowCompar = new Date();
        if (startCompar) {
            startCompar.setHours(0, 0, 0, 0);
        }
        if (endCompar) {
            endCompar.setHours(0, 0, 0, 0);
        }
        nowCompar.setHours(0, 0, 0, 0);

        if (startCompar && endCompar && startCompar > endCompar) {
            toastr.error("La date de début ne peut pas être supérieure à la date de fin.");
            return;
        }

        if (endCompar) {
            var tomorrow = new Date(nowCompar);
            tomorrow.setDate(tomorrow.getDate() + 1);

            if (endCompar >= tomorrow) {
                toastr.error("La date de fin ne peut pas être dans le futur.");
                return;
            }
        }

        if (startCompar) {
            if (startCompar > nowCompar) {
                toastr.error("La date de début ne peut pas être dans le futur.");
                return;
            }
        }

        var sensorData = {};

        rawData.forEach(entry => {
            var entryTime = entry.time;

            if (entryTime >= start && entryTime <= end) {
                var sensorId = entry.id_sensor;

                if (!sensorData[sensorId]) {
                    sensorData[sensorId] = {
                        count: 0,
                        pm1: 0,
                        pm25: 0,
                        pm10: 0,
                        geo: {
                            latitude: entry.latitude,
                            longitude: entry.longitude
                        },
                        type: type,
                        id: entry.id,
                        id_group: entry.id_group,
                        id_install: entry.id_install,
                        id_sensor: entry.id_sensor,
                        is_fixed: entry.is_fixed
                    };
                }

                sensorData[sensorId].pm1 += entry.data.pm1;
                sensorData[sensorId].pm25 += entry.data.pm25;
                sensorData[sensorId].pm10 += entry.data.pm10;
                sensorData[sensorId].count++;
            }
        });

        let averagedData = Object.keys(sensorData).map(sensorId => {
            let sensor = sensorData[sensorId];
            if(type == "mapD3"){
                return {
                    id_sensor: sensorId,
                    data: {
                        pm1: sensor.count ? sensor.pm1 / sensor.count : 0,
                        pm25: sensor.count ? sensor.pm25 / sensor.count : 0,
                        pm10: sensor.count ? sensor.pm10 / sensor.count : 0
                    },
                    geo: sensor.geo,
                    type: sensor.type,
                    id: sensor.id,
                    id_group: sensor.id_group,
                    id_install: sensor.id_install,
                    id_sensor: sensor.id_sensor,
                    is_fixed: sensor.is_fixed
                };
            }
            return {
                id_sensor: sensorId,
                pm1: sensor.count ? sensor.pm1 / sensor.count : 0,
                pm25: sensor.count ? sensor.pm25 / sensor.count : 0,
                pm10: sensor.count ? sensor.pm10 / sensor.count : 0,
                geo: sensor.geo,
                type: sensor.type,
                id: sensor.id,
                id_group: sensor.id_group,
                id_install: sensor.id_install,
                id_sensor: sensor.id_sensor,
                is_fixed: sensor.is_fixed
            };
        });

        return averagedData;
    }

    function initDateInputs(rawDatas) {
        var latestDate = new Date(rawDatas[0].time);
        var tenDaysAgo = new Date(latestDate);
        tenDaysAgo.setDate(latestDate.getDate() - 29);

        $("#debutjour").val(formatDate(tenDaysAgo));
        $("#finjour").val(formatDate(latestDate));
    }

    function initMonthInputs(rawDatas) {
        var latestDate = new Date(rawDatas[0].time);
        var firstOfMonth = new Date(latestDate);
        firstOfMonth.setMonth(latestDate.getMonth() - 3);

        $("#debutmois").val(formatDate(firstOfMonth, true));
        $("#finmois").val(formatDate(latestDate, true));
    }

    var batchSize = 500;
    var currentPage = 1;
    var totalPages = 1;

    function sortData(column, order) {
        rawData.sort((a, b) => {
            var aValue = a[column];
            var bValue = b[column];

            if (column === "time") {
                aValue = new Date(aValue);
                bValue = new Date(bValue);
            }

            if (order === "asc") {
                return aValue - bValue;
            } else {
                return bValue - aValue;
            }
        });

        generateTable().then(() => {
            document.body.style.cursor = "auto";
            document.getElementById("airQualityTable").style.cursor = "auto";
        });
    }

    function updateSortIcons(column, order) {
        $(".sort-icon").text("⬍");
        var icon = $(`th[data-column="${column}"] .sort-icon`);
        icon.text(order === "asc" ? "⬆" : "⬇");
    }

    async function generateTable() {
        return new Promise(resolve => {
            updatePagination(rawData, "all", batchSize, currentPage, "pageInfo", "prevPage", "nextPage");
            displayPageData();
            resolve();
        });
    }

    function displayPageData() {
        var tbody = $("#airQualityTable tbody");
        tbody.empty();

        var start = (currentPage - 1) * batchSize;
        var end = start + batchSize;
        var dataSlice = rawData.slice(start, end);

        dataSlice.forEach(item => {
            var row = $("<tr>");
            row.html(`
                <td>${item.id_sensor}</td>
                <td>${parseFloat(String(item.data.pm1).replace(",", ".")).toFixed(2)}</td>
                <td>${parseFloat(String(item.data.pm25).replace(",", ".")).toFixed(2)}</td>
                <td>${parseFloat(String(item.data.pm10).replace(",", ".")).toFixed(2)}</td>
                <td>${new Date(item.time).toLocaleString()}</td>
                <td>${item.latitude}</td>
                <td>${item.longitude}</td>
            `);
            tbody.append(row);
        });
    }

    function updatePagination(data, pagesTotal, sizeBatch, pageCurrent, pageInfoId, prevId, nextId) {
        var pt = 0;
        if (pagesTotal == "all") {
            totalPages = Math.ceil(data.length / sizeBatch);
            pt = totalPages;
        } else if (pagesTotal == "daily") {
            totalPagesAnalyse = Math.ceil(data.length / sizeBatch);
            pt = totalPagesAnalyse;
        }

        $(`#${pageInfoId}`).text(`Page ${pageCurrent} / ${pt}`);
        $(`#${prevId}`).prop("disabled", pageCurrent === 1);
        $(`#${nextId}`).prop("disabled", pageCurrent === pagesTotal);
    }

    $("#prevPage").click(() => {
        if (currentPage > 1) {
            currentPage--;
            displayPageData();
            updatePagination(rawData, "all", batchSize, currentPage, "pageInfo", "prevPage", "nextPage");
        }
    });

    $("#nextPage").click(() => {
        if (currentPage < totalPages) {
            currentPage++;
            displayPageData();
            updatePagination(rawData, "all", batchSize, currentPage, "pageInfo", "prevPage", "nextPage");
        }
    });

    $("#airQualityTable th").click(function() {
        var column = $(this).data("column");
        var order = $(this).data("order");

        order = order === "asc" ? "desc" : "asc";
        $(this).data("order", order);

        updateSortIcons(column, order);
        sortData(column, order);
    });

    var timeout;

    $("#searchInput").on("keyup", function() {
        clearTimeout(timeout);
        var searchValue = $(this).val().toLowerCase();

        timeout = setTimeout(() => {
            $("#airQualityTable tbody tr").each(function() {
                var text = $(this).text().toLowerCase();
                $(this).toggle(text.includes(searchValue));
            });
        }, 1200);
    });

    $("#searchInput-day").on("keyup", function() {
        clearTimeout(timeout);
        var searchValue = $(this).val().toLowerCase();

        timeout = setTimeout(() => {
            $("#dailyReportTable tbody tr").each(function() {
                var text = $(this).text().toLowerCase();
                $(this).toggle(text.includes(searchValue));
            });
        }, 1200);
    });

    async function generateBarChart(rawDatas) {
        var sensorData = {};

        rawDatas.forEach(item => {
            var id = item.id_sensor;
            if (!sensorData[id]) {
                sensorData[id] = {
                    pm1: [],
                    pm25: [],
                    pm10: []
                };
            }
            sensorData[id].pm1.push(item.data.pm1);
            sensorData[id].pm25.push(item.data.pm25);
            sensorData[id].pm10.push(item.data.pm10);
        });

        var labels = [];
        var avgPM1 = [],
            avgPM25 = [],
            avgPM10 = [];

        for (var id in sensorData) {
            labels.push(id);
            avgPM1.push(sensorData[id].pm1.reduce((a, b) => a + b, 0) / sensorData[id].pm1.length);
            avgPM25.push(sensorData[id].pm25.reduce((a, b) => a + b, 0) / sensorData[id].pm25.length);
            avgPM10.push(sensorData[id].pm10.reduce((a, b) => a + b, 0) / sensorData[id].pm10.length);
        }

        var ctx = document.getElementById('barChart').getContext('2d');

        if (Chart.getChart("barChart")) {
            Chart.getChart("barChart").destroy();
        }

        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                        label: 'PM1 (µg/m³)',
                        data: avgPM1,
                        backgroundColor: 'rgba(10, 72, 130, 0.74)'
                    },
                    {
                        label: 'PM2.5 (µg/m³)',
                        data: avgPM25,
                        backgroundColor: 'rgba(241, 171, 20, 0.87)'
                    },
                    {
                        label: 'PM10 (µg/m³)',
                        data: avgPM10,
                        backgroundColor: 'rgba(47, 167, 11, 0.92)'
                    }
                ]
            },
            options: {
                responsive: true,
                tooltip: {
                    enabled: true
                },
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: "ID Capteur"
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: "Moyenne (µg/m³)"
                        },
                        beginAtZero: true,
                        ticks: {
                            callback: function(value) {
                                return value + " µg/m³";
                            }
                        }
                    }
                }
            }
        });
    }

    async function generatePieCharts(rawDatas) {
        var sensorData = {};

        rawDatas.forEach(item => {
            var id = item.id_sensor;
            if (!sensorData[id]) {
                sensorData[id] = {
                    pm1: [],
                    pm25: [],
                    pm10: []
                };
            }
            sensorData[id].pm1.push(item.data.pm1);
            sensorData[id].pm25.push(item.data.pm25);
            sensorData[id].pm10.push(item.data.pm10);
        });

        var labels = [];
        var avgPM1 = [],
            avgPM25 = [],
            avgPM10 = [];

        for (var id in sensorData) {
            labels.push(id);
            avgPM1.push(sensorData[id].pm1.reduce((a, b) => a + b, 0) / sensorData[id].pm1.length);
            avgPM25.push(sensorData[id].pm25.reduce((a, b) => a + b, 0) / sensorData[id].pm25.length);
            avgPM10.push(sensorData[id].pm10.reduce((a, b) => a + b, 0) / sensorData[id].pm10.length);
        }

        var colors = labels.map(() => `hsl(${Math.random() * 360}, 70%, 60%)`);

        function createPieChart(canvasId, data, title) {
            var ctx = document.getElementById(canvasId).getContext('2d');
            var existingChart = Chart.getChart(canvasId);
            if (existingChart) {
                existingChart.destroy();
            }
            new Chart(ctx, {
                type: 'polarArea',
                data: {
                    labels: labels,
                    datasets: [{
                        data: data,
                        backgroundColor: colors
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        title: {
                            display: true,
                            text: title
                        }
                    }
                }
            });
        }

        createPieChart('pm1Chart', avgPM1, 'Répartition PM1 par capteur');
        createPieChart('pm25Chart', avgPM25, 'Répartition PM2.5 par capteur');
        createPieChart('pm10Chart', avgPM10, 'Répartition PM10 par capteur');
    }

    // $("#toggle-filters").click(function () {
    //     $(".filters-atmo_data").toggle();
    // });

    // function qui groupe les moyenne des données par heur avec des sous groupement par min (avec intervale de 15min)
    function groupByHourAndMinute(data) {
        var result = {};

        data.forEach(entry => {
            var date = new Date(entry.time);
            var hour = date.getHours();
            var minute = date.getMinutes();

            if (!result[hour]) {
                result[hour] = {};
            }

            var timeKey = minute;

            if (!result[hour][timeKey]) {
                result[hour][timeKey] = {
                    count: 0,
                    pm1: 0,
                    pm25: 0,
                    pm10: 0
                };
            }

            result[hour][timeKey].pm1 += entry.data.pm1;
            result[hour][timeKey].pm25 += entry.data.pm25;
            result[hour][timeKey].pm10 += entry.data.pm10;
            result[hour][timeKey].count += 1;
        });

        for (var hour in result) {
            for (var minute in result[hour]) {
                var dataPoint = result[hour][minute];
                dataPoint.pm1 /= dataPoint.count;
                dataPoint.pm25 /= dataPoint.count;
                dataPoint.pm10 /= dataPoint.count;
            }
        }

        return result;
    }

    function calculateVariation(groupedData) {
        var variations = {};

        for (var hour in groupedData) {
            var dataGroup = groupedData[hour];

            var timeKeys = Object.keys(dataGroup).sort((a, b) => a - b);

            var firstData = dataGroup[timeKeys[0]];
            var lastData = dataGroup[timeKeys[timeKeys.length - 1]];

            var variation = {
                pm1: ((lastData.pm1 - firstData.pm1) / firstData.pm1) * 100,
                pm25: ((lastData.pm25 - firstData.pm25) / firstData.pm25) * 100,
                pm10: ((lastData.pm10 - firstData.pm10) / firstData.pm10) * 100
            };

            variations[hour] = variation;
        }

        return variations;
    }

    function createProgressBars(groupedData, containerId) {
        var progressContainer = document.getElementById(containerId);
        progressContainer.innerHTML = "";

        var particles = ["pm1", "pm25", "pm10"];
        var totalIntervals = Object.keys(groupedData).length * 4;

        particles.forEach(particle => {
            var progressWrapper = document.createElement("div");
            progressWrapper.classList.add("progress-wrapper");

            var label = document.createElement("span");
            label.classList.add("progress-label");
            label.textContent = `${particle == 'pm25' ? 'PM2.5 ' : particle.toUpperCase() } :`;
            progressWrapper.appendChild(label);

            var progressGroup = document.createElement("div");
            progressGroup.classList.add("progress-group");

            var timeline = document.createElement("div");
            timeline.classList.add("progress-timeline");

            var progressBar = document.createElement("div");
            progressBar.classList.add("progress-bar");

            Object.keys(groupedData).forEach(hour => {
                [0, 15, 30, 45].forEach(interval => {
                    if (groupedData[hour] && groupedData[hour][interval] && groupedData[hour][interval][particle] !== undefined) {
                        var value = groupedData[hour][interval][particle];
                        var color = getColor(value, particle, false);

                        var width = 100 / totalIntervals;

                        var segment = document.createElement("div");
                        segment.style.width = `${width}%`;
                        segment.style.backgroundColor = color;
                        segment.style.height = "100%";
                        segment.style.display = "inline-block";
                        segment.style.cursor = 'pointer';

                        segment.addEventListener("click", function() {
                            var clickedHour = hour;
                            var clickedInterval = interval;
                        });

                        if (interval === 0) {
                            var hourLabel = document.createElement("span");
                            hourLabel.style.width = `${width * 4}%`;
                            hourLabel.style.textAlign = "center";
                            hourLabel.textContent = hour;
                            timeline.appendChild(hourLabel);
                        }

                        progressBar.appendChild(segment);
                    }
                });
            });

            progressGroup.appendChild(timeline);
            progressGroup.appendChild(progressBar);
            progressWrapper.appendChild(progressGroup);
            progressContainer.appendChild(progressWrapper);
        });
    }

    function formatDate(date, monthly = false) {
        var d = new Date(date);

        var year = d.getUTCFullYear();
        var month = ('0' + (d.getUTCMonth() + 1)).slice(-2);
        var day = ('0' + d.getUTCDate()).slice(-2);

        return monthly ? `${year}-${month}` : `${year}-${month}-${day}`;
    }

    // Billan journalière
    function processData(data, type = "daily") {
        var groupedData = {};

        data.forEach(entry => {
            var key = formatDate(entry.time, type === "monthly");

            if (!groupedData[key]) {
                groupedData[key] = {
                    pm1: 0,
                    pm25: 0,
                    pm10: 0,
                    count: 0
                };
            }

            groupedData[key].pm1 += entry.data.pm1;
            groupedData[key].pm25 += entry.data.pm25;
            groupedData[key].pm10 += entry.data.pm10;
            groupedData[key].count++;
        });

        return Object.keys(groupedData).map(key => ({
            [type === "monthly" ? "month" : "date"]: key,
            pm1: groupedData[key].pm1 / groupedData[key].count,
            pm25: groupedData[key].pm25 / groupedData[key].count,
            pm10: groupedData[key].pm10 / groupedData[key].count,
            conclusion: evaluateAirQuality(
                groupedData[key].pm1 / groupedData[key].count,
                groupedData[key].pm25 / groupedData[key].count,
                groupedData[key].pm10 / groupedData[key].count,
                true
            )
        })).sort((a, b) => new Date(a[type === "monthly" ? "month" : "date"]) - new Date(b[type === "monthly" ? "month" : "date"]));
    }

    function getLastNDaysData(data, days = 30, definiDate = "") {
        var latestDate = definiDate ? new Date(definiDate) : new Date(data[0].time);
        var cutoffDate = new Date(latestDate);
        cutoffDate.setDate(latestDate.getDate() - (days - 1));

        return processData(data, 'daily').filter(d => {
            var dataDate = new Date(d.date);
            return dataDate >= cutoffDate && dataDate <= latestDate;
        });
    }

    function filterDataByDate(startDate, endDate) {
        var start = new Date(startDate).toISOString().split("T")[0];
        var end = new Date(endDate).toISOString().split("T")[0];

        return processData(rawData, 'daily').filter(d => d.date >= start && d.date <= end);
    }

    function getLastNMonthsData(data, months = 6) {
        var latestDate = new Date(data[0].time);
        var cutoffDate = new Date(latestDate);
        cutoffDate.setMonth(latestDate.getMonth() - (months - 1));

        return processData(data, "monthly").filter(d => new Date(d.month) >= cutoffDate);
    }

    function filterMonthlyData(startMonth, endMonth) {
        return processData(rawData, 'monthly').filter(d => d.month >= startMonth && d.month <= endMonth);
    }

    function updateChart(data, label, canvasId) {
        var ctx = document.getElementById(canvasId).getContext('2d');

        if (Chart.getChart(canvasId)) {
            Chart.getChart(canvasId).destroy();
        }

        chartInstance = new Chart(ctx, {
            type: "line",
            data: {
                labels: data.map(d => d.date),
                datasets: [{
                        label: "PM1",
                        data: data.map(d => d.pm1),
                        borderColor: "blue",
                        fill: false,
                        pointBackgroundColor: data.map(d => getColor(d.pm1, "pm1", true)),
                        pointBorderColor: data.map(d => getColor(d.pm1, "pm1", true)),
                    },
                    {
                        label: "PM2.5",
                        data: data.map(d => d.pm25),
                        borderColor: "orange",
                        fill: false,
                        pointBackgroundColor: data.map(d => getColor(d.pm25, "pm25", true)),
                        pointBorderColor: data.map(d => getColor(d.pm25, "pm25", true))
                    },
                    {
                        label: "PM10",
                        data: data.map(d => d.pm10),
                        borderColor: "green",
                        fill: false,
                        pointBackgroundColor: data.map(d => getColor(d.pm10, "pm10", true)),
                        pointBorderColor: data.map(d => getColor(d.pm10, "pm10", true))
                    }
                ]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: "top"
                    },
                    tooltip: {
                        callbacks: {
                            title: function(tooltipItem) {
                                return tooltipItem[0].label;
                            },
                            label: function(tooltipItem) {
                                var index = tooltipItem.dataIndex;
                                var conclusion = data[index].conclusion;
                                return [
                                    `PM1: ${tooltipItem.raw.toFixed(2)} µg/m³`,
                                    `PM2.5: ${data[index].pm25.toFixed(2)} µg/m³`,
                                    `PM10: ${data[index].pm10.toFixed(2)} µg/m³`,
                                    `Conclusion: ${conclusion}`
                                ];
                            }
                        }
                    },
                    noDataMessage: true
                },
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: label
                        }
                    },
                    y: {
                        beginAtZero: true,
                        ticks: {
                            callback: function(value) {
                                return value + " µg/m³";
                            }
                        }
                    }
                }
            },
            plugins: [noDataPlugin]
        });
    }

    // filtre journal
    function handleDateChange() {
        var startDate = $("#debutjour").val();
        var endDate = $("#finjour").val();

        if (startDate && endDate) {
            var filteredData = filterDataByDate(startDate, endDate);
            updateChart(filteredData, "Bilan journalier", "airQualityChart");
        }
    }

    $("#debutjour, #finjour").on("change", handleDateChange);

    // filtre mensuel
    function handleMonthChange() {
        var startMonth = $("#debutmois").val();
        var endMonth = $("#finmois").val();

        if (startMonth && endMonth) {
            var filteredData = filterMonthlyData(startMonth, endMonth);
            updateChart(filteredData.map(d => ({
                ...d,
                date: d.month
            })), "Bilan mensuel", "monthAirQualityChart");
        }
    }

    $("#debutmois, #finmois").on("change", handleMonthChange);

    function evaluateAirQuality(pm1, pm25, pm10, isDaily = false) {
        var seuil = isDaily ? seuilJournal : seuilInstant;

        function getQualityLevel(value, thresholds) {
            if (value <= thresholds[0]) return 0;
            if (value <= thresholds[1]) return 1;
            if (value <= thresholds[2]) return 2;
            // if (value > thresholds[2]) return 3; 
            return 2;
        }

        var pm1Level = getQualityLevel(pm1, seuil.pm1);
        var pm25Level = getQualityLevel(pm25, seuil.pm25);
        var pm10Level = getQualityLevel(pm10, seuil.pm10);

        if (pm1Level == 2 && pm25Level == 2 && pm10Level == 2) return "Qualité de l'air dangereux";


        var levels = [pm1Level, pm25Level, pm10Level];
        var levelCounts = [0, 0, 0];
        levels.forEach(level => levelCounts[level]++);

        if (levelCounts[0] === 1 && levelCounts[1] === 1 && levelCounts[2] === 1) {
            return "Qualité de l'air moyenne";
        }

        var majorityLevel = levelCounts.indexOf(Math.max(...levelCounts));

        switch (majorityLevel) {
            case 0:
                return "Bonne qualité de l'air";
            case 1:
                return "Qualité de l'air moyenne";
            case 2:
                return "Qualité de l'air mauvaise";
            case 3:
                return "Qualité de l'air dangereux";
            default:
                return "Inconnu";
        }
    }

    var totalPagesAnalyse = 1;
    var batchSizeAnalyse = 30;
    var currentPageAnalyse = 1;

    function fillTableWithDailyData() {
        updatePagination(dailyData, "daily", batchSizeAnalyse, currentPageAnalyse, "pageInfoAnalyse", "prevPageAnalyse", "nextPageAnalyse");
        var tableBody = document.querySelector("#dailyReportTable tbody");
        tableBody.innerHTML = "";

        dailyData.reverse();

        var start = (currentPageAnalyse - 1) * batchSizeAnalyse;
        var end = start + batchSizeAnalyse;
        var dataSlice = dailyData.slice(start, end);

        dataSlice.forEach(entry => {
            var row = `<tr>
                <td>${entry.date}</td>
                <td>${entry.pm1.toFixed(2)}</td>
                <td>${entry.pm25.toFixed(2)}</td>
                <td>${entry.pm10.toFixed(2)}</td>
                <td>${entry.conclusion}</td> 
            </tr>`;
            tableBody.innerHTML += row;
        });
    }

    $("#prevPageAnalyse").click(() => {
        if (currentPageAnalyse > 1) {
            currentPageAnalyse--;
            fillTableWithDailyData();
            updatePagination(dailyData, "daily", batchSizeAnalyse, currentPageAnalyse, "pageInfoAnalyse", "prevPageAnalyse", "nextPageAnalyse");
        }
    });

    $("#nextPageAnalyse").click(() => {
        if (currentPageAnalyse < totalPages) {
            currentPageAnalyse++;
            fillTableWithDailyData();
            updatePagination(dailyData, "daily", batchSizeAnalyse, currentPageAnalyse, "pageInfoAnalyse", "prevPageAnalyse", "nextPageAnalyse");
        }
    });

    function createChartDatasets(groupedData) {
        var hours = Object.keys(groupedData);
        var hourlyLabels = [];

        for (var i = 0; i <= 23; i++) {
            hourlyLabels.push(`${i}h`);
        }

        function formatMinutesToTime(minutes) {
            var hours = Math.floor(minutes / 60);
            var mins = minutes % 60;
            return `${hours}h${mins.toString().padStart(2, '0')}`;
        }

        function createCombinedChart(canvasId) {
            var particles = ['pm1', 'pm25', 'pm10'];
            var colors = {
                pm1: 'blue',
                pm10: 'green',
                pm25: 'orange'
            };
            var datasets = [];

            particles.forEach(particle => {
                var particleData = [];

                hours.forEach(hour => {
                    Object.keys(groupedData[hour]).forEach(minute => {
                        var record = groupedData[hour][minute];
                        var value = record[particle] || 0;
                        var particleColor = getColor(value, particle, false);
                        particleData.push({
                            x: parseInt(hour) * 60 + parseInt(minute),
                            y: value,
                            backgroundColor: particleColor
                        });
                    });
                });

                datasets.push({
                    label: particle == 'pm25' ? 'pm2.5' : particle,
                    data: particleData,
                    borderColor: colors[particle],
                    fill: false,
                    borderWidth: 2,
                    pointBackgroundColor: particleData.map(point => point.backgroundColor),
                    pointBorderColor: particleData.map(point => point.backgroundColor),
                    // pointRadius: 0
                });
            });

            var ctx = document.getElementById(canvasId).getContext('2d');

            if (Chart.getChart(canvasId)) {
                Chart.getChart(canvasId).destroy();
            }

            new Chart(ctx, {
                type: 'line',
                data: {
                    datasets: datasets
                },
                options: {
                    elements: {
                        line: {
                            tension: 0.4
                        }
                    },
                    scales: {
                        x: {
                            type: 'linear',
                            position: 'bottom',
                            title: {
                                display: true,
                                text: 'Heure du jour'
                            },
                            ticks: {
                                stepSize: 60,
                                min: 0,
                                max: 1380,
                                callback: function(value) {
                                    return formatMinutesToTime(value);
                                }
                            }
                        },
                        y: {
                            beginAtZero: true,
                            title: {
                                display: true,
                                text: 'Concentration de Particules (µg/m³)'
                            },
                            ticks: {
                                callback: function(value) {
                                    return value + " µg/m³";
                                }
                            }
                        }
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                title: function(context) {
                                    return formatMinutesToTime(context[0].parsed.x);
                                },
                                label: function(context) {
                                    return `${context.dataset.label}: ${context.formattedValue} µg/m³`;
                                },
                                afterBody: function(tooltipItems) {
                                    if (tooltipItems.length === 0) return '';

                                    var timeValue = tooltipItems[0].parsed.x;

                                    var allDatasets = datasets;

                                    var pm1Value = 0;
                                    var pm25Value = 0;
                                    var pm10Value = 0;

                                    tooltipItems[0].dataset.data.forEach((dataPoint, index) => {
                                        var time = tooltipItems[0].chart.data.labels[index];
                                        if (new Date(time).getTime() === new Date(timeValue).getTime()) {
                                            if (tooltipItems[0].dataset.label.toLowerCase() === "pm1") {
                                                pm1Value = dataPoint;
                                            }
                                            if (tooltipItems[0].dataset.label.toLowerCase() === "pm25") {
                                                pm25Value = dataPoint;
                                            }
                                            if (tooltipItems[0].dataset.label.toLowerCase() === "pm10") {
                                                pm10Value = dataPoint;
                                            }
                                        }
                                    });

                                    var qualityConclusion = evaluateAirQuality(
                                        pm1Value,
                                        pm25Value,
                                        pm10Value,
                                        false
                                    );

                                    return '\nConclusion: ' + qualityConclusion;
                                }
                            }
                        },
                        noDataMessage: true
                    }
                },
                plugins: [noDataPlugin]
            });
        }

        createCombinedChart('combined-chart');
    }

    function getTodaySensorData(data, givenDate = null) {
        var dateToUse = givenDate ? new Date(givenDate) : new Date();
        dateToUse.setHours(0, 0, 0, 0);
        var startOfDay = dateToUse.getTime();
        dateToUse.setHours(23, 59, 59, 999);
        var endOfDay = dateToUse.getTime();

        var filteredData = data.filter(entry => entry.time >= startOfDay && entry.time <= endOfDay);

        if (filteredData.length === 0 && !givenDate) {
            if (data.length > 0) {
                var sortedData = [...data].sort((a, b) => b.time - a.time);
                var latestDate = new Date(sortedData[0].time);
                latestDate.setHours(0, 0, 0, 0);
                var latestStartOfDay = latestDate.getTime();
                latestDate.setHours(23, 59, 59, 999);
                var latestEndOfDay = latestDate.getTime();
                filteredData = data.filter(entry => entry.time >= latestStartOfDay && entry.time <= latestEndOfDay);
            }
        }

        return filteredData.sort((a, b) => a.time - b.time);
    }

    function aggregateDataByTime(data) {
        var groupedData = {};

        data.forEach(entry => {
            var timestamp = entry.time;
            if (!groupedData[timestamp]) {
                groupedData[timestamp] = {
                    pm1: [],
                    pm25: [],
                    pm10: []
                };
            }
            groupedData[timestamp].pm1.push(entry.data.pm1);
            groupedData[timestamp].pm25.push(entry.data.pm25);
            groupedData[timestamp].pm10.push(entry.data.pm10);
        });

        var aggregatedData = Object.entries(groupedData).map(([time, values]) => ({
            time: parseInt(time),
            pm1: values.pm1.reduce((sum, v) => sum + v, 0) / values.pm1.length,
            pm25: values.pm25.reduce((sum, v) => sum + v, 0) / values.pm25.length,
            pm10: values.pm10.reduce((sum, v) => sum + v, 0) / values.pm10.length
        }));

        // Extraction de la date à partir des données
        if (data.length > 0) {
            var firstTimestamp = data[0].time;
            var dataDate = new Date(firstTimestamp);
            dataDate.setHours(0, 0, 0, 0);

            var today = new Date();
            today.setHours(0, 0, 0, 0);

            // Mise à jour du titre
            if (dataDate.getTime() !== today.getTime()) {
                var formattedDate = dataDate.toLocaleDateString("fr-FR", {
                    day: "2-digit",
                    month: "long",
                    year: "numeric"
                });
                $("#bilan-title").html(`Évolution des particules (PM) le ${formattedDate}`);
                $("#bilan-title2").html(`Évolution des particules (PM) le ${formattedDate}`);
            }
        }

        return aggregatedData;
    }

    function prepareChartData(aggregatedData) {
        return {
            labels: aggregatedData.map(entry => new Date(entry.time).toLocaleTimeString("fr-FR", {
                hour: "2-digit",
                minute: "2-digit"
            })),
            pm1: aggregatedData.map(entry => entry.pm1),
            pm25: aggregatedData.map(entry => entry.pm25),
            pm10: aggregatedData.map(entry => entry.pm10)
        };
    }

    function createChart(chartData, containerId) {
        var ctx = document.getElementById(containerId).getContext("2d");

        if (Chart.getChart(containerId)) {
            Chart.getChart(containerId).destroy();
        }

        var datasets = [{
                label: "PM1",
                data: chartData.pm1,
                borderColor: "blue",
                borderWidth: 2,
                pointBackgroundColor: chartData.pm1.map(value => getColor(value, "pm1", false)),
                pointBorderColor: chartData.pm1.map(value => getColor(value, "pm1", false))
            },
            {
                label: "PM2.5",
                data: chartData.pm25,
                borderColor: "orange",
                borderWidth: 2,
                pointBackgroundColor: chartData.pm25.map(value => getColor(value, "pm25", false)),
                pointBorderColor: chartData.pm25.map(value => getColor(value, "pm25", false))
            },
            {
                label: "PM10",
                data: chartData.pm10,
                borderColor: "green",
                borderWidth: 2,
                pointBackgroundColor: chartData.pm10.map(value => getColor(value, "pm10", false)),
                pointBorderColor: chartData.pm10.map(value => getColor(value, "pm10", false))
            }
        ];

        new Chart(ctx, {
            type: "line",
            data: {
                labels: chartData.labels,
                datasets: datasets // Use the array of datasets
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: "top"
                    },
                    noDataMessage: true,
                    tooltip: {
                        callbacks: {
                            afterBody: function(context) {
                                var index = context[0].dataIndex;
                                var pm1 = chartData.pm1[index];
                                var pm25 = chartData.pm25[index];
                                var pm10 = chartData.pm10[index];

                                var qualityConclusion = evaluateAirQuality(pm1, pm25, pm10, false);

                                return '\nConclusion: ' + qualityConclusion;
                            }
                        }
                    }
                },
                scales: {
                    y: {
                        beginAtZero: true,
                        title: {
                            display: true,
                            text: "Concentration"
                        },
                        ticks: {
                            callback: function(value) {
                                return value + " µg/m³";
                            }
                        }
                    },
                    x: {
                        title: {
                            display: true,
                            text: "Heure"
                        }
                    }
                }
            },
            plugins: [noDataPlugin]
        });
    }

    function createParticlePieCharts(chartData, containerIdPrefix) {
        var particles = ["pm1", "pm25", "pm10"];
        var colors = ["blue", "orange", "green"];
        $(`#${containerIdPrefix}`).empty();

        particles.forEach((particle, index) => {
            var containerId = containerIdPrefix + "_" + particle;
            var canvas = document.createElement("canvas");
            canvas.id = containerId;
            document.getElementById(containerIdPrefix).appendChild(canvas);
            var ctx = canvas.getContext("2d");

            var data = chartData[particle]; // Récupère les données de la particule spécifique
            var labels = chartData.labels; // Récupère les étiquettes de temps

            new Chart(ctx, {
                type: "pie",
                data: {
                    labels: labels,
                    datasets: [{
                        data: data,
                        backgroundColor: data.map(value => getColor(value, particle, false)), // Génère une palette de couleurs pour chaque graphique
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: "top"
                        },
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    if (context.parsed !== null) {
                                        label += context.parsed.toFixed(2) + " µg/m³";
                                    }
                                    return label;
                                },
                                title: function() {
                                    return particle;
                                }
                            }
                        }
                    },
                    title: {
                        display: true,
                        text: particle.toUpperCase(), // Titre du graphique (PM1, PM2.5, PM10)
                    }
                }
            });
        });
    }

    function createPolarChart(chartData, containerId) {
        var canvas = document.getElementById(containerId);
        
        var ctx = canvas.getContext("2d");

        if (Chart.getChart(containerId)) {
            Chart.getChart(containerId).destroy();
        }

        function moyenne(values) {
            if (!Array.isArray(values) || values.length === 0) return 0;
            return values.reduce((a, b) => a + b, 0) / values.length;
        }

        var avgPm1 = moyenne(chartData.pm1);
        var avgPm25 = moyenne(chartData.pm25);
        var avgPm10 = moyenne(chartData.pm10);

        var data = [avgPm1, avgPm25, avgPm10];
        var labels =  ["pm1", "pm25", "pm10"];
        new Chart(ctx, {
            type: "polarArea",
            data: {
                labels: ["PM1", "PM2.5", "PM10"],
                datasets: [{
                    data: data,
                    backgroundColor: data.map((value, index) => getColor(value, labels[index], true))
                }]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: "top"
                    }
                }
            }
        });
    }

    function generateThresholdTable(seuils, tableId) {
        var tableBody = document.getElementById(tableId);
        tableBody.innerHTML = "";

        Object.entries(seuils).forEach(([particle, values]) => {
            var row = document.createElement("tr");

            var labParicle = particle == 'pm25' ? 'pm2.5' : particle;
            row.appendChild(createCell(labParicle.toUpperCase()));

            values.forEach(value => row.appendChild(createCell(value)));

            row.appendChild(createCell(`> ${values[values.length - 1]}`));

            tableBody.appendChild(row);
        });
    }

    function createCell(content) {
        var cell = document.createElement("td");
        cell.textContent = content;
        cell.style.color = "black";
        return cell;
    }

    function initBtnClick(){
        $(".marker-change").on('click', function() {
            var $btn = $(this);
            var key = $btn.attr('data-key');
            typeActivated = key;

            var moyenCapteur = filterAndAverageData(rawData, key);
            mylog.log("Francky :", moyenCapteur);
            if(key != "mapD3"){
                if(!mapAtmotrack){
                    initMapV2();
                    addIcon(mapAtmotrack);
                    mapD3Atmotrack = null;
                }else{
                    mapAtmotrack.clearMap();
                }
                mapAtmotrack.addElts(moyenCapteur);
            }else{
                if(!mapD3Atmotrack){
                    initMapD3();
                    mapAtmotrack = null;
                }
                mapd3Data = {};
                moyenCapteur.forEach((item) => {
                    var sensorId = item.id_sensor;
                    if(typeof item.data != "undefined" && Object.values(item.data).length > 0){
                        Object.keys(item.data).forEach((dataItem) => {
                            if (!mapd3Data[sensorId+dataItem] || new Date(item.time) > new Date(mapd3Data[sensorId+dataItem].time)) {
                                mapd3Data[sensorId+dataItem] = {
                                    ...item,
                                    data : {
                                        [dataItem] : item.data[dataItem]
                                    }
                                };
                            }
                        })
                    }
                })
                addIcon(mapD3Atmotrack);
                mapD3Atmotrack.addElts(mapd3Data);
            }
        });
    }

    $(document).ready(function() {

        setDataMap();
        generateThresholdTable(seuilInstant, "threshold-table-body");
        generateThresholdTable(seuilJournal, "threshold-table-day-body");

       

        $(document).click(function(e) {
            if (!$(e.target).hasClass('day')) {
                $('#popup').hide();
            }
        });

        $(".change-day-data").on('click', function() {
            var key = $(this).attr('data-key');
            var dateString = $("#finTime").val();

            if (!dateString) {
                toastr.error("Veuillez sélectionner une date.");
                return;
            }

            var date = new Date(dateString);
            var newDate;

            if (key === 'last') {
                newDate = new Date(date);
                newDate.setDate(date.getDate() - 1);
            } else if (key === 'after') {
                newDate = new Date(date);
                newDate.setDate(date.getDate() + 1);

                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                tomorrow.setHours(0, 0, 0, 0);

                if (newDate >= tomorrow) {
                    toastr.error("La date suivante ne peut pas être dans le futur.");
                    return;
                }
            } else {
                return;
            }
            var formattedDate = newDate.toISOString().slice(0, 10);
            $(".day").removeClass('day-calendar-active');
            $(`div[data-date="${formattedDate}"]`).addClass('day-calendar-active');
            updateViews(formattedDate);
        });

        $(document).on('keydown', function(e) {
            if (["INPUT", "TEXTAREA"].includes(document.activeElement.tagName) || document.activeElement.isContentEditable) {
                return;
            }
            if (e.key == "ArrowUp" || e.key == "ArrowRight") {
                e.preventDefault();
                $("#btn-afterDay").trigger('click');
            }
            if (e.key == "ArrowDown" || e.key == "ArrowLeft") {
                e.preventDefault();
                $("#btn-lasteDay").trigger('click');
            }

        })

        $(".change-views").on('click', function() {
            var divClass = $(this).attr('data-div');
            $(".container-data").addClass("hidden");
            $(`.${divClass}`).removeClass("hidden");

            $(".change-views").removeClass('active_views');

            if (divClass == "tableau-container") {
                $("#circleMarker").addClass('disabled');
                $("#toggle-filters").addClass("hidden");
            };
            if (divClass == "map-container") {
                $("#circleMarker").removeClass('disabled');
                mapAtmotrack.clearMap();
                mapAtmotrack.addElts(rawDataMap);
                $("#toggle-filters").removeClass("hidden");
            };
            if (divClass == "analyse-container") {
                $("#circleMarker").addClass('disabled');
                $("#toggle-filters").addClass("hidden");
            };

            $(this).addClass('active_views');
        });

        $("#active-filters").on('click', function() {
            var $btn = $(this);
            var originalText = $btn.html();
            $btn.html('<i class="fa fa-spinner fa-spin"></i>');
            $btn.prop('disabled', true);

            var from = $("#debutTime").val();
            from = from != "" ? new Date($("#debutTime").val()) : from;
            var to = $("#finTime").val();
            to = to != "" ? new Date($("#finTime").val()) : to;
            var _id = "";

            if (from != "" || to != "" || _id != "")
                setDataMap(from, to, _id);
            else
                toastr.warning("Il est nécessaire de remplir au moins l'un des filtres pour l'activer.")
        });

    });
</script>