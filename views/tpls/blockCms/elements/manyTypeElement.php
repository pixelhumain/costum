<?php 
$keyTpl ="manyTypeElement";
$paramsData = [ 
    "cardNbResult" => "3",
    "elType" => "poi",
    "content" => "<div style=\"text-align: center;\"><span style=\"background-color: transparent;\"><font color=\"#3b9ca3\" face=\"Please_write_me_a_song\" style=\"font-size: 35px;\"><u>Nos actualité</u></font></span></div>",
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
        }
    }
}
 
?>
<style>
    .element-<?= $kunik?>{
        padding: 0 10% 0 10% ;
    }
    .element-<?= $kunik?> #filters<?= $kunik?>{
        cursor: pointer;
        margin-bottom : 2%;
    }
    .element-<?= $kunik?> #filters<?= $kunik?> a{
        padding-right : 1%;
        padding: 5px;
        margin-right: 4px;
        border-radius: 5px;
    }
    .element-<?= $kunik?> #filters<?= $kunik?> a:hover{
        text-decoration: none;
        background: #3b9ca3;
        color :white !important;
    }
    .element-<?= $kunik?> #filters<?= $kunik?> a.active{
       
        background: #f0aa8d;
    }
    .element-<?= $kunik?> .swiper-container {
        width: 100%;
        height: 100%;
    }
    .element-<?= $kunik?> .content {
        background: #e7f6f6;
        margin-bottom: 3% ;
        font-size: 18px;
        border-left: 10px solid #51a2a2;
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
    .element-<?= $kunik?> .swiper-wrapper {
        flex-direction:column;
        flex-wrap:wrap;
    }
    .element-<?= $kunik?> .swiper-pagination,
    .element-<?= $kunik?> .swiper-button-next,
    .element-<?= $kunik?> .swiper-button-prev{
        display:none
    }
    .element-<?= $kunik?> .content-container{
        display: flex;
        flex-direction: row;
        width: 100%;
        height : auto;
    }
    .element-<?= $kunik?> .col-info{
        flex-basis: 70%;
        padding: 5px 5px 5px 15px;
        display: flex;
        flex-direction: column;
        justify-content: "";
    }
    .element-<?= $kunik?> .btnEdit<?= $kunik?>{
        align-items:  center;
    }
    
    .element-<?= $kunik?> .col-btn-detail{
        flex-basis: 30%;
        order : 3;
        background : #e7f6f6;
        display:  none;
        padding: 4% ;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        display: initial;
    }
    .element-<?= $kunik?> .col-btn-detail a{
        padding: 13px 30px 13px 30px;
        border-radius: 14px;
        color: white;
        background: #ea631f;
        margin-top: 5%;
        cursor: pointer;

    }
    .element-<?= $kunik?> .edit-element-<?= $kunik?>,.element-<?= $kunik?> .delete-element-<?= $kunik?>{
        display: none
    }
    .element-<?= $kunik?> .content:hover .edit-element-<?= $kunik?>,.element-<?= $kunik?> .content:hover .delete-element-<?= $kunik?>{
        display: initial;
    }
    @media (min-width: 1200px){
        .element-<?= $kunik?> .col-btn-detail{
            padding: 4% 4% 4% 8%;
        }
    }
    @media (max-width: 957px){
        .element-<?= $kunik?> .content-container{
            display: flex;
            flex-direction: column;
        }
        .element-<?= $kunik?> .col-info{
            order: 2;
        }
    }
    .element-<?= $kunik?> .see-more{
        padding: 13px 30px 13px 30px;
        border-radius: 14px;
        font-size: 20px;
        color: white;
        background: #ea631f;
        cursor: pointer;
    }
</style>
<div class="stdr-elem">
  <div class="title-bg-<?= $kunik ?> padding-top-10 padding-bottom-10 padding-left-10">
  <span class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content" ><?= $paramsData["content"]?></span>
  </div>
  
</div>

<div class="element-<?= $kunik?> col-md-12">
    <div id="filters<?= $kunik?>"></div>
    <div class="swiper-container">
        <div class="swiper-wrapper">

        </div>
        <div class="swiper-pagination"></div>
    </div>
</div>

<script>
    var elementType = {
        "poi" : "<?php echo Yii::t('common', 'News feed')?>",
        "event" : trad.events,
        "project" : trad.project,
        "organization" : trad.organization,
        "ressources" : trad.ressources,
        "classifieds" : trad.classifieds
    };
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    var htmlTypeElement = "";
    var type = "";
    $.each(elementType , function(k,val) { 
        htmlTypeElement += `<a class="findElement title-4 ${k}" data-type="${k}" data-val=${val}> #${val} </a>`;
    })
    $('#filters<?= $kunik?>').html(htmlTypeElement);
    $(".findElement").off().on("click",function() { 
        $(".element-<?= $kunik?> #filters<?= $kunik?> a").removeClass("active");
        $(".element-<?= $kunik?> #filters<?= $kunik?> ."+$(this).data("type")).addClass("active");
        sectionDyf.<?php echo $kunik ?>ParamsData.elType=$(this).data("type");
        elmObj<?= $kunik?>.fetchData(sectionDyf.<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
        
    })
    var elmObj<?= $kunik?> = {
        fetchData: function(paramsData, kunik) {
            setTimeout(function() {
                carouselObj.bindEvents(paramsData, kunik);
            }, 200)
            var html = "";
            var dataCount = 0;
            params = {
                searchType: [paramsData["elType"]]
            };
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                params,
                function(data) {
                    dataCount = Object.keys(data.results).length;
                    var i = 1;
                    var lblBtn = (exists(trad["add"+sectionDyf.<?php echo $kunik ?>ParamsData.elType]) ? trad["add"+sectionDyf.<?php echo $kunik ?>ParamsData.elType] : "Ajouter");
                    
                    var mesNoData = "";
                    console.log("dataaa",data);
                        <?php if(Authorisation::isInterfaceAdmin()){ ?>
                            if(sectionDyf.<?php echo $kunik ?>ParamsData.elType == "poi") {
                                lblBtn = "Ajouter une actualité";
                                mesNoData = "<?php echo Yii::t('cms', 'No news')?>";
                            }
                            if(sectionDyf.<?php echo $kunik ?>ParamsData.elType == "event") {
                                mesNoData = "<?php echo Yii::t('cms', 'No event')?>";
                            }
                            if(sectionDyf.<?php echo $kunik ?>ParamsData.elType == "project") {
                                mesNoData = "<?php echo Yii::t('cms', 'No project')?>"
                            }
                            if(sectionDyf.<?php echo $kunik ?>ParamsData.elType == "ressources") {
                                mesNoData = "<?php echo Yii::t('cms', 'No resources')?>"
                            }
                            if(sectionDyf.<?php echo $kunik ?>ParamsData.elType == "classifieds") {
                                mesNoData = "<?php echo Yii::t('cms', 'No classifieds')?>"
                            }
                            if(sectionDyf.<?php echo $kunik ?>ParamsData.elType == "organization") {
                                mesNoData = "<?php echo Yii::t('cms', 'No organizations')?>"
                            }
                            if(sectionDyf.<?php echo $kunik ?>ParamsData.elType == "classifieds" || sectionDyf.<?php echo $kunik ?>ParamsData.elType == "ressources" ) {
                               commonElObj.beforeBuild.properties = {
                                }
                                
                            }else {
                                commonElObj.beforeBuild.properties.shortDescription = {
                                    "inputType" : "textarea",
                                    "label" : "<?php echo Yii::t('common', 'shortDescription')?>",
                                    "rules" : {
                                        "maxlength" : 200
                                    }
                                }
                            }
                            html += '<div class="text-center btn-container">'+
                                `<a class="btn btn-primary btn-sm add " onclick="dyFObj.openForm('${sectionDyf.<?php echo $kunik ?>ParamsData.elType}',null,null,null,commonElObj)">${lblBtn}</a>`+
                            '</div>';                       
                        <?php }  ?>
                        if((data.results).length != 0 ) {
                            $.each(data.results, function(k, v) {
                                if (i <= <?= $paramsData["cardNbResult"]?>) {
                                    html += elmObj<?= $kunik?>.view(v, paramsData, kunik);
                                    i++;
                                }
                            });
                        }else {
                            html += `<div class="text-center"> <p>${mesNoData}</div>`
                        }
                    
                    
                    if ( dataCount > paramsData["cardNbResult"])
                        html +=
                        `<div class="text-center">
                              <a href="#search?types=${paramsData["elType"]}" class="btn btn-sm  lbh see-more">
                                 Afficher tout
                              </a>
                              
                          </div>`;
                    $('.element-' + kunik + ' .swiper-wrapper').html(html);
                    coInterface.bindLBHLinks();
                }
            )
        },
        view: function(v, paramsData, kunik) {
            var $this = this;
            var df = getDateFormatedCms(v);
            var html = "";
            html += `
                <div class="content">
              
                      <div class="content-container">
                          <div class="col-info">
                            <h4 class="">
                                <a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element event-place   title-6">
                                ${v.name}
                                </a>
                            </h4>`;

            if (v.type == "classifieds") {
                if (exists(v.price) && exists(v.devise))
                    html += `<h6 class="title-5">${v.price+' '+v.devise }</h6>`;
                if ($.inArray(v.collection, ['classifieds', 'ressources']) >= 0 && typeof v.category != 'undefined') {
                    html += `<h6 class="title-3">`;
                    html += tradCategory[v.section];
                    if (typeof v.category != 'undefined' && v.type != 'poi') html += ' > ' + tradCategory[v.category];
                    subtype = (typeof v.subtype != 'undefined' && typeof tradCategory[v.subtype] != 'undefined') ? ' > ' + tradCategory[v.subtype] : ' > ' + v.subtype;
                    html += subtype;
                    html += `</h6>`;
                }
            }


            html += `<p class="title-3">${v.shortDescription != null ? v.shortDescription : ""}</p>`;
            if (exists(df.startDay))
                html += `<h6 class="title-5"><i class="fa fa-calendar"></i> ${df.startLbl+' '+df.startDayNum+' '+df.startDay+' '+df.startMonth+' '+df.startYear+' <i class="fa fa-clock-o"></i> '+df.startTime}</h6>`;
            if (exists(df.endDay) )
                html += `<h6 class="title-5"> ${df.endLbl+' '+df.endDayNum+' '+df.endDay+' '+df.endMonth+' '+df.endYear+' <i class="fa fa-clock-o"></i> '+df.endTime}</h6>`;

            if (isInterfaceAdmin) {
                html += `<div class="btnEdit<?= $kunik?>">
                            <button class="btn btn-sm btn-success edit-element-<?= $kunik?> hiddenPreview" data-type="${v.collection}" data-id="${v._id.$id}">
                                <i class="fa fa-edit"></i>
                                </button>
                            <button class="btn btn-sm btn-danger delete-element-<?= $kunik?> hiddenPreview" data-type="${v.collection}" data-id="${v._id.$id}">
                                <i class="fa fa-trash"></i>
                                </button>
                        </div>`;
            }

            html += `</div>
                        <div class="col-btn-detail">
                            <a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element"> <?php echo Yii::t('cms', 'More information')?></a>
                        </div>
                      </div>
                  </div>`;
            return html;
        },
        bindEvents: function(paramsData, kunik) {
            $('.delete-element-' + kunik).on('click', function() {
                var idEvent = $(this).data('id');
                var typeEvent = $(this).data('type');
                bootbox.confirm(trad.areyousuretodelete, function(result) {
                    if (result) {
                        var url = baseUrl + "/" + moduleId + "/element/delete/id/" + idEvent + "/type/" + typeEvent;
                        var param = new Object;
                        ajaxPost(null, url, param, function(data) {
                            if (data.result) {
                                toastr.success(data.msg);
                                urlCtrl.loadByHash(location.hash);
                            } else {
                                toastr.error(data.msg);
                            }
                        })
                    }
                })
            });
            $('.edit-element-' + kunik).on('click', function() {
                dyFObj.editElement($(this).data('type'), $(this).data('id'), null, commonElObj);
            });
            coInterface.bindLBHLinks();
        },
    }
    var commonElObj = {
        "beforeBuild":{
            "properties" : {
                
            }
        },
        "onload" : {
            "actions" : {
                "src" : { 
                  "infocustom" : "Remplir le champ"
                },
                "presetValue": {
                    "type" :"article"
                }
                /*"hide" : {
                      "breadcrumbcustom" : 1,
                      "tagstags" :1,
                      "descriptiontextarea":1,
                      "locationlocation":1,
                      "formLocalityformLocality":1,
                      "parentfinder" : 1,
                      "removePropLineBtn" : 1
                }*/
            }
        }
    };    
    commonElObj.afterSave = function(data){
        dyFObj.commonAfterSave(data, function(){
          urlCtrl.loadByHash(location.hash);
        });
    }
    

    $(function(){
        $(".element-<?= $kunik?> #filters<?= $kunik?> .poi").addClass("active");
        elmObj<?= $kunik?>.fetchData(sectionDyf.<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
       
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
              "icon" : "fa-cog",
            "properties" : {
                "cardNbResult" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Number of results')?>",
                    rules : {
                        number:true,
                        max:100,
                        min:0,
                    },
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.cardNbResult
                },

            },
            beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }

            }
          }
        };
        sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties = Object.assign(
            sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,
        );

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            tplCtx.format = true;
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    })
</script>