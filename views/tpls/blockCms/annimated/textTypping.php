<?php
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
    $otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
    $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
    $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
?>

<style id="textTypping<?= $kunik ?>" type="text/css">

.other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
@media (min-width : 768px) and (max-width: 991px){
.other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
}

@media screen and (max-width: 767px) {
.other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
}

.textTypping<?= $blockKey ?> {
  text-align: center;
  margin: auto;
  color: white;
  font: 700 normal 2.5em 'tahoma';
  text-shadow: 5px 2px #222324, 2px 4px #222324, 3px 5px #222324;
}

</style>

<div class="textTypping<?= $blockKey ?> <?= $kunik ?> other-css-<?= $kunik ?> <?= $otherClass ?>"></div>

<script>

    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#textTypping<?= $kunik ?>").append(str);

    if (costum.editMode){
        cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?>;
        textTypping = {
            configTabs : {
                general : {
                    inputsConfig : [
                        {
                            type : "textarea",
                            options: {
                                name : "text",
                                label : tradCms.text,
                                translate:true
                            }
                        },
                        {
                            type: "number",
                            options: {
                                name: "speed",
                                label: tradCms.animationSpeed,
                                filterValue: cssHelpers.form.rules.checkLengthProperties
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig : [
                        "color",
                        "fontSize",
                        "fontStyle",
                        "fontFamily",
                        "textTransform",
                        "fontWeight",
                        "textAlign",
                        "textShadow",
                        "addCommonConfig"
                    ]
                },
                advanced : {
                    inputsConfig : [
                        "addCommonConfig",
                    ]
                },
                },
            onChange : function(path,valueToSet,name,payload,value){
                
            },
            afterSave: function(path,valueToSet,name,payload,value,id){
                if(name=="text") {
                    clearInterval(intervalId<?= $blockKey ?>);
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }

                if ( name == "speed" ) {
                    clearInterval(intervalId<?= $blockKey ?>);
                    speed<?= $blockKey ?> = value || speed<?= $blockKey ?>;
                    wordflick<?= $blockKey ?>();
                }

                costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "replaceBlock", 
                                data:{ id: id}
                })
            }
            
        }
        cmsConstructor.blocks.textTypping<?= $blockKey ?> = textTypping;
    }

    intervalId<?= $blockKey ?> = null;
    speed<?= $blockKey ?> = <?= json_encode($blockCms["speed"]) ?>;
    var wordflick<?= $blockKey ?> = function () {
        var text = <?= json_encode($blockCms["text"]) ?>;
        var costumLangActive = <?= json_encode($costum["langCostumActive"] ?? "fr") ?>;
        if (text.hasOwnProperty(costumLangActive)) {
            textToShow = text[costumLangActive];
            } else {
            textToShow = text[Object.keys(text)[0]];
        }
        var words = textToShow.split(',').map(item => item.trim().replace(/(^'|'$)/g, ''));
        var part = "",
            i = 0,
            offset = 0,
            len = words.length,
            forwards = true,
            skip_count = 0,
            skip_delay = 15;


        if (intervalId<?= $blockKey ?>) {
            clearInterval(intervalId<?= $blockKey ?>);
        }

        intervalId<?= $blockKey ?> = setInterval(function () {
            if (forwards) {
                if (offset >= words[i].length) {
                    ++skip_count;
                    if (skip_count == skip_delay) {
                        forwards = false;
                        skip_count = 0;
                    }
                }
            } else {
                if (offset === 0) {
                    forwards = true;
                    i++;
                    offset = 0;
                    if (i >= len) {
                        i = 0;
                    }
                }
            }
            part = words[i].substr(0, offset);
            if (skip_count === 0) {
                if (forwards) {
                    offset++;
                } else {
                    offset--;
                }
            }
            $('.textTypping<?= $blockKey ?>').html(part || '&nbsp;');
            if (costum.editMode) {
                cmsConstructor.sp_params["<?= $blockKey ?>"].language = <?= json_encode($costum["langCostumActive"]) ?>;
            }
            $('.textTypping<?= $blockKey ?>').attr("data-lang", <?= json_encode($costum["langCostumActive"]) ?>);
        }, speed<?= $blockKey ?>);
    };

    $(document).ready(function () {
        wordflick<?= $blockKey ?>();
    });

    

</script>