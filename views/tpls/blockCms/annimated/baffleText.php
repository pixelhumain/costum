<?php
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $typeUrl =  $blockCms["advanced"]["typeUrl"] ??  "internalLink";
    $link =  $blockCms["advanced"]["link"] ??  "";
    $targetBlank = $blockCms["advanced"]["targetBlank"] ?? false ;
    $downloadFile = $blockCms["advanced"]["downloadFile"] ?? false ;
    $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
    $otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
    $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
    $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
    
?>

<style id="baffle<?= $kunik ?>" type="text/css">

    .other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
    
    @media (min-width : 768px) and (max-width: 991px){
    .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
    }

    @media screen and (max-width: 767px) {
    .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
    }

    .container-baffle{
    width: 100%;
    position: absolute;
    }

    .text1<?= $blockKey ?>{
    text-align: center;
    color: black;
    font-size: 2em;
    font-weight: 600;
    }

    .text2<?= $blockKey ?>{
    text-align: center;
    color: black;
    font-size: 1em;
    font-weight: 600;
    }

</style>
<div class="container-baffle<?= $blockKey ?> <?= $kunik ?> other-css-<?= $kunik ?> <?= $otherClass ?>">
    <div class="text1 text1<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text1"></div>
    <div class="text2 text2<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text2"></div>
</div>
<script>
    if (costum.editMode){
        cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?>;
        editBaffle = {
            configTabs : {
                general : {
                    inputsConfig : [
                        {
                            type : "inputSimple",
                            options: {
                                name : "text1",
                                label : tradCms.first+" "+tradCms.text,
                                translate: true,
                            }
                        },
                        {
                            type : "inputSimple",
                            options: {
                                name : "text2",
                                label : tradCms.second+" "+tradCms.text,
                                translate: true,
                            }
                        },
                        "background", 
                        "width",
                        "height",
                    ]
                },
                style: {
                    inputsConfig : [
                        {
                            type: "section",
                            options: {
                                name: "text1",
                                    label: tradCms.style+" "+tradCms.first+" "+tradCms.text ,
                                    showInDefault: false,
                                    inputs: [
                                    "color",
                                    "fontSize",
                                    "fontStyle",
                                    "fontFamily",
                                    "textTransform",
                                    "fontWeight",
                                    "textAlign",
                                    "textShadow"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "text2",
                                    label: tradCms.style+" "+tradCms.second+" "+tradCms.text ,
                                    showInDefault: false,
                                    inputs: [
                                    "color",
                                    "fontSize",
                                    "fontStyle",
                                    "fontFamily",
                                    "textTransform",
                                    "fontWeight",
                                    "textAlign",
                                    "textShadow"
                                ]
                            }
                        },
                        "addCommonConfig"
                    ]
                },
                advanced : {
                    inputsConfig : [
                        "addCommonConfig",
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "typeUrl",
                                label : tradCms.link,
                                tabs: [
                                    {
                                        value:"internalLink",  
                                        label: tradCms.internalLink,
                                        inputs: [
                                            {
                                                type: "inputWithSelect",
                                                options: {
                                                    name: "link",
                                                    viewPreview : true,
                                                    configForSelect2: {
                                                        maximumSelectionSize: 1,
                                                        tags: Object.keys(costum.app)
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value:"externalLink"  , 
                                        label: tradCms.externalLink,   
                                        inputs: [
                                            {
                                                type : "inputSimple",
                                                options: {
                                                    name : "link",
                                                    viewPreview : true
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name: "targetBlank",
                                label: tradCms.openInNewTab,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name: "downloadFile",
                                label: tradCms.downloadFile,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        },
                    ]
                },
                },
            onChange : function(path,valueToSet,name,payload,value){
                if(name=="text1") {
                    mylog.log(".text1<?= $blockKey ?>");
                    $(".text1<?= $blockKey ?>").text(value["text"]);
                }
                if(name=="text2") {
                    $(".text2<?= $blockKey ?>").text(value["text"]);
                }
            },
            afterSave: function(path,valueToSet,name,payload,value,id){
                costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "refreshBlock", 
                                data:{ id: id}
                })
            }
            
        }
        cmsConstructor.blocks.baffleText<?= $blockKey ?> = editBaffle;
    } else {
        if ( "<?= $link ?>" !== "" ){
            $(".text1<?= $blockKey ?>").css("cursor", "pointer");
            $(".text1<?= $blockKey ?>").off("click").on("click", function(e){ 
                e.stopImmediatePropagation();
                onClickOpenLink("<?= $typeUrl ?>","<?= $link ?>","<?= $targetBlank ?>","<?= $downloadFile ?>");
            });
        }
    }

    cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? [ ]) ?>,'<?= $kunik ?>')

    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#baffle<?= $kunik ?>").append(str);

    appendTextLangBased(".text1<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["text1"]) ?>,"<?= $blockKey ?>");
    appendTextLangBased(".text2<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["text2"]) ?>,"<?= $blockKey ?>");

    var text = baffle('.text1<?= $blockKey ?>');
	text.set({
		characters : 'zpxzVpasdfh86136░█▒ ░░░█▓ >░░ ▓/▒█▓ █ █>█▒sayg▒ ░░░█▓ >yf',
		speed: 150
	});
	text.start();
	text.reveal(80000);

	var text1 = baffle('.text2<?= $blockKey ?>');
	text1.set({
		characters : 'qwertyuiopasdfgh8613611888',
		speed: 140
	});
	text1.start();
	text1.reveal(5500);


</script>