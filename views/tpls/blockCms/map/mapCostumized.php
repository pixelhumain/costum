<?php
$cssAnsScriptFilesModuleMap = array( 
    '/leaflet/leaflet.css',
    '/leaflet/leaflet.js',
    '/css/map.css',
    '/markercluster/MarkerCluster.css',
    '/markercluster/MarkerCluster.Default.css',
    '/markercluster/leaflet.markercluster.js',
    '/js/map.js',
);

HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );

$keyTpl = "mapCostumized";

$paramsData = [ 
    "title" => "Carte de la communautée",
    "sousTitle" =>"",
    "content"=>""
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>

<style type="text/css">
    #menuRightmapCommunity{
        position: absolute !important;
    }
   

    .title<?= $kunik?> {
        padding: 15px;
        text-transform: initial !important;
    }

    #mapCommunity<?= $kunik?>, .leaflet-pane, .leaflet-popup-pane, .leaflet-popup, .leaflet-popup-content-wrapper{
        text-align: left;
        border-radius: 2px;
        box-shadow: 0px 0px 2px 0px rgba(0,0,0,0.4);
    }

    #mapCommunity<?= $kunik?>, .leaflet-pane, .leaflet-popup-pane, .leaflet-popup, .leaflet-popup-content-wrapper, .leaflet-popup-content{
       /*width: 240px;*/
    }
</style>

<div class="communaute ">
    <h1 class="title<?= $kunik?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title" >
     <?= $paramsData["title"]; ?> 
 </h1>
 <h2 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="sousTitle">
     <?= $paramsData["sousTitle"]; ?> 
 </h2>
 <p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content"> 
    <?= $paramsData["content"]; ?> 
</p>
<div style="z-index: 1;height: 500px;" class="col-md-12 mapBackground no-padding" id="mapCommunity<?= $kunik?>"></div>

</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    jQuery(document).ready(function(){

    sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section de la carte de la communautée",
            "description" : "Personnaliser votre section e la carte de la communautée",
            "icon" : "fa-cog",
            "properties" : {
            },
            save : function () {  
                tplCtx.value = {};

                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        toastr.success("élement mis à jour"); 
                        $("#ajax-modal").modal('hide');

                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    } );
                }

            }
        }
    };

$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
    tplCtx.id = $(this).data("id");
    tplCtx.collection = $(this).data("collection");
    tplCtx.path = "allToRoot";

    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
});


mapCustom.popup= {
        default: function (data, mObj) {
            mylog.log("mapObj.mapCustom.popup.default", data);
            var id = (typeof data._id != "undefined") ? data._id.$id : data.id;
            var imgProfil = mapCustom.custom.getThumbProfil(data);
            //mylog.log("mObj imgProfil", imgProfil);
            var eltName = (typeof data.title != "undefined") ? data.title : data.name;
            var popup = "";
            popup += "<div id='popup" + id + "'>";
            popup += "<img src='" + imgProfil + "' height='60' width='60' class='' style='display: inline; vertical-align: middle; border-radius:10%;'>";
            popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

            if (typeof data.tags != "undefined" && data.tags != null && data.tags.length > 0) {
                popup += "<div style='margin-top : 5px;'>";
                var totalTags = 0;
                $.each(data.tags, function (index, value) {
                    totalTags++;
                    if (totalTags < 3) {
                        popup += "<div class='popup-tags'>#" + value + " </div>";
                    }
                });
                popup += "</div>";
            }
            if(typeof data.address != "undefined" && data.address != null){
                addressStr="";
                if(typeof data.address.streetAddress != "undefined")
                    addressStr+=data.address.streetAddress;
                if(typeof data.address.postalCode != "undefined")
                    addressStr+=((addressStr != "") ? ", " : "")+data.address.postalCode;
                if(typeof data.address.addressLocality != "undefined")
                    addressStr+=((addressStr != "") ? " " : "")+data.address.addressLocality;
                popup += "<div class='popup-address text-dark padding-5 bold'>";
                popup +=    "<i class='fa fa-map-marker'></i> Adresse";
                popup += "<div class='col-xs-12 separation' style='background-color: lightgray;min-width: 240px'></div><br>";
                popup += "<div style='padding:10px'>"+addressStr;
                popup += "</div>";
                popup += "</div>";
            }
            if (typeof data.shortDescription != "undefined" &&
                data.shortDescription != "" &&
                data.shortDescription != null) {
                popup += "<div class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Description</div>";
                    popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
                popup += "</div>";
            }

            if (    (typeof data.url != "undefined" && data.url != null && typeof data.url == "string") ||
                    (typeof data.email != "undefined" && data.email != null) ||
                    (typeof data.telephone != "undefined" && data.telephone != null) ) {
                popup += "<div id='pop-contacts' class='popup-section'>";
                popup += "<div class='popup-subtitle'>Contacts</div>";

                if (typeof data.url != "undefined" && data.url != null && typeof data.url == "string") {
                    popup += "<div class='popup-info-profil'>";
                    popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                    popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                    popup += "</div>";
                }

                if (typeof data.email != "undefined" && data.email != null) {
                    popup += "<div class='popup-info-profil'>";
                    popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                    popup += "</div>";
                }

                if (typeof data.telephone != "undefined" && data.telephone != null) {
                    popup += "<div class='popup-info-profil'>";
                    popup += "<i class='fa fa-phone fa_phone'></i> ";
                    var tel = ["fixe", "mobile"];
                    var iT = 0;
                    $.each(tel, function(keyT, valT){
                        if(typeof data.telephone[valT] != "undefined" && data.telephone[valT] != null){
                            $.each(data.telephone[valT], function(keyN, valN){
                                if(iT > 0)
                                    popup += ", ";
                                popup += valN;
                                iT++;
                            });
                        }
                    });
                    
                    popup += "</div>";
                }

                popup += "</div>";
                popup += "</div>";
            }
            var url = '#page.type.' + data.collection + '.id.' + id;

            // if (data.type.substr(0,11) == "poi.interop") {
            //  url = data.url;
            //  popup += "<a href='"+url+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
            // }else if (typeof TPL_IFRAME != "undefined" && TPL_IFRAME==true){
            //  url = "https://www.communecter.org/"+url;
            //  popup += "<a href='"+url+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
            // }else if (typeof networkJson != "undefined" && notNull(networkJson) && notNull(networkJson.dataSrc) && notNull(data.source)){
            //  popup += "<a href='"+data.source+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
            // }else{
            //  onclick = 'urlCtrl.loadByHash("'+url+'");';                 
            //  popup += "<a href='"+url+"' onclick='"+onclick+"' class='item_map_list popup-marker lbh' id='popup"+id+"'>";
            // }
            popup += "<div class='popup-section'>";
            if(mObj.activePreview)
                popup += "<a href='" + url + "' class='lbh-preview-element item_map_list popup-marker' id='popup" + id + "'>";
            else
                popup += "<a href='" + url + "' target='_blank' class='lbh item_map_list popup-marker' id='popup" + id + "'>";
            popup += '<div class="btn btn-sm btn-more col-md-12">';
            popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
            popup += '</div></a>';
            popup += '</div>';
            popup += '</div>';
            return popup;
        }
    }  

paramsmapCommunity<?= $kunik?> = {
    zoom : 3,
    container : "mapCommunity<?= $kunik?>",
    activePopUp : true,
    tile : "maptiler",
    menuRight : true,
    mapOpt:{
        latLon : ["-21.115141", "55.536384"],
    }
};

var mapCommunity<?= $kunik?>Home = {};

afficheCommunity();
});

  function afficheCommunity(){
    mylog.log("----------------- Affichage community");

    var params = {
        "id" : contextId,
        "type" : contextType
    };

    mapCommunity<?= $kunik?>Home = mapObj.init(paramsmapCommunity<?= $kunik?>);

    ajaxPost(
        null,
        baseUrl + "/costum/costumgenerique/getcommunity",
        params,
        function(data){
            mylog.log("success : elt ",data.elt);

            mapCommunity<?= $kunik?>Home.addElts(data.elt);
        }
    );
    mylog.log("mapCommunity<?= $kunik?>Home", mapCommunity<?= $kunik?>Home);
}


</script>