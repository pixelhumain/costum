<?php

?>

<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<style> 
        #map<?= $kunik ?>{
            width: 100%;
            height: 90vh; /* Ajustez la hauteur de la carte */
        }

        /* Style pour le champ de texte */
        .input-container {
            text-align: center;
            margin-top: 20px;
        }

        input[type="text"] {
            padding: 10px;
            font-size: 16px;
            width: 40%;
            max-width: 600px;
            margin: 0 auto;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        .input-container select {
            padding: 10px;
            font-size: 16px;
            max-width: 600px;
            margin: 0 auto;
            border: 1px solid #ccc;
            border-radius: 4px;
            width: 150px;
            display: inline-block;
        }

        .input-container button {
            padding: 10px 20px;
            font-size: 16px;
            background-color: #4CAF50;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            margin-top: 10px;
        }
        .loader-map {
            position: absolute;
            z-index: 999999;
            width: 100%;
            background: #000000ad;
        }
        .loader-map lottie-player {
            width: 50%;
            height: 90vh;
            display: block;
            margin: auto;
        }

        /* Nouveau style pour le container des résultats */
        .results-container {
            display: none;
            width: 80%;
            max-width: 600px;
            margin: 20px auto;
            border: 1px solid #ccc;
            border-radius: 4px;
            background-color: #f9f9f9;
            max-height: 200px;
            overflow-y: auto;
            padding: 10px;
        }

        .result-item {
            padding: 8px;
            border-bottom: 1px solid #ddd;
            cursor: pointer;
        }

        .result-item:hover {
            background-color: #e1e1e1;
        }

    .map-fliters-top {
        position: absolute;
        top: 10%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 11;
        width: 90%;
    }
</style>
<div class="map-fliters-top">
    <div class="input-container">
        <select class="" id="mois" name="mois">
            <option value="01" selected>Janvier</option>
            <option value="02">Février</option>
            <option value="03">Mars</option>
            <option value="04">Avril</option>
            <option value="05">Mai</option>
            <option value="06">Juin</option>
            <option value="07">Juillet</option>
            <option value="08">Août</option>
            <option value="09">Septembre</option>
            <option value="10">Octobre</option>
            <option value="11">Novembre</option>
            <option value="12">Décembre</option>
        </select>
        <select class="" id="annee" name="annee">
            <option value="2020">2020</option>
            <option value="2021">2021</option>
            <option value="2022">2022</option>
            <option value="2023" selected>2023</option>
        </select>
        <button id="validate" >Valider</button>
        <input type="text" id="addressInput" placeholder="Entrez une adresse" value="12 ruelle de la poste">
        <button id="searchButton" >Rechercher</button>
    </div>
</div>

<div class="loader-map hidden">
<lottie-player src="<?= Yii::app()->request->baseUrl . Yii::app()->getModule("co2")->getAssetsUrl() ?>/json/Hackaton/loader.json" background="transparent"  speed="1" loop autoplay></lottie-player>

</div>
    <!-- Conteneur de la carte -->
    <div id="map<?= $kunik ?>"></div>
<script>
    let polygonClicked = false;
    let dateDebut = "2023-01-01";
    let dateFin = "2023-01-31";
    let dataOfRR = [];
    function classifyRoof(coordinates, nature) {
        /**
         * Classifie le type de toiture basé sur les coordonnées et la nature du bâtiment.
         */
        const altitudes = [];
        
        // Parcourir toutes les coordonnées
        $.each(coordinates, function(_, ring) {
          $.each(ring, function(_, points) {
              $.each(points, function(_, point) {
                  if (point.length >= 3 && !isNaN(point[2])) {
                      altitudes.push(point[2]); // Ajouter seulement si l'altitude (z) existe et est valide
                  }
              });
            });
        });

        // Vérifier si des altitudes valides ont été extraites
        if (altitudes.length === 0) {
            console.error("Aucune altitude valide trouvée !");
            return "Inconnu";
        }

        const zMin = Math.min(...altitudes);
        const zMax = Math.max(...altitudes);
        const zVariation = zMax - zMin;

        // Classification
        if (zVariation <= 0) {
            return "Plat";
        } else if (zVariation > 0) {
            if (nature.includes("Industriel") || nature.includes("Agricole")) {
                return "En pente à surface lisse";
            } else {
                return "En pente à surface rugueuse";
            }
        } else {
            return "Inconnu";
        }
    }
    function calculateQuantity(surfcace, toiture){
        let data = {
            "Plat": 0.8,
            "En pente à surface lisse": 0.9,
            "En pente à surface rugueuse":0.8,
            "Inconnu": 0.4
        }
        let config = dataOfRR.reduce((acc, curr) => {
            return acc + (typeof curr.RR != "undefined" ? parseFloat(curr.RR) : (typeof curr.RR1 != "undefined" ? floatval(curr.RR1) : 0));
        }, 0);
        return surfcace * data[toiture] * config;
    }
    paramsMapCO = $.extend(true, {}, paramsMapCO, {
        mapCustom: {
            icon: {
                getIcon: function (params) {
                    var elt = params.elt;
                    var myCustomColour = '#F88518';
                    var markerHtmlStyles = `
                        background-color: ${myCustomColour};
                        width: 3.5rem;
                        height: 3.5rem;
                        display: block;
                        left: -1.5rem;
                        top: -1.5rem;
                        position: relative;
                        border-radius: 3rem 3rem 0;
                        transform: rotate(45deg);
                        border: 1px solid #FFFFFF`;

                    var myIcon = L.divIcon({
                        className: "my-custom-pin",
                        iconAnchor: [0, 24],
                        labelAnchor: [-6, 0],
                        popupAnchor: [0, -36],
                        html: `<span style="${markerHtmlStyles}" />`
                    });
                    return myIcon;
                }
            },
            getPopup: function(data){
                let toiture = classifyRoof(data.geometry.coordinates, data.properties.nature);
                let quantity = calculateQuantity(data.properties.surface, toiture);
                var popup = `
                    <div style="border-radius: 20px 0 10px 10px; background: white;padding: 10px;position: relative;">
                    <h5 style="text-transform: inherit;">Nature du batiment : ${data.properties.nature}</h5>
                    <div style="display: flex; align-items:center; margin-left: 5px">
                        <div style="margin-right: 5px; background: #37517e;height:100%; display:flex; align-items:center;justify-content:center; border-radius: 5px;color: white; padding: 5px;"><i class="fa fa-columns" aria-hidden="true"></i></div>
                        <div>
                            <h5 style="text-transform: capitalize;">Usage : ${data.properties.usage}</h5>
                        </div>
                    </div>
                    <div style="display: flex; align-items:center; margin-left: 5px">
                        <div style="margin-right: 5px; background: #37517e;height:100%; display:flex; align-items:center;justify-content:center; border-radius: 5px;color: white; padding: 5px;">
                            <i class="fa fa-window-maximize" aria-hidden="true"></i>
                        </div>
                        <div>
                            <h5 style="text-transform: inherit">Surface : ${data.properties.surface} m<sup>2</sup></h5>
                        </div>
                    </div>
                    <div style="display: flex; align-items:center; margin-left: 5px">
                        <div style="margin-right: 5px; background: #37517e;height:100%; display:flex; align-items:center;justify-content:center; border-radius: 5px;color: white; padding: 5px;">
                            <i class="fa fa-home" aria-hidden="true"></i>
                        </div>
                        <div>
                            <h5 style="text-transform: inherit;">Toiture : ${toiture}</h5>
                        </div>
                    </div>
                    <div style="display: flex; align-items:center; margin-left: 5px">
                        <div style="margin-right: 5px; background: #37517e;height:100%; display:flex; align-items:center;justify-content:center; border-radius: 5px;color: white; padding: 5px;">
                            <i class="fa fa-tint" aria-hidden="true"></i>
                        </div>
                        <div>
                            <h5 style="text-transform: inherit;">Max quantité d'eau récuperée pour le mois selectionné: ${quantity.toFixed(2)} L</h5>
                        </div>
                    </div>
                    <div style="position:absolute; left: -20px; bottom: 25%; width: 5px; height: 50px; background: #37517e;"></div>
                    <div style="position:absolute; right: -20px; bottom: 25%; width: 5px; height: 50px; background: #37517e;"></div>
                    <a href="https://qa.communecter.org/costum/co/index/slug/reunionEau#answer.index.id.new.form.6741bd895bb828609e52afa4.standalone.true" target="_blank" style="    width: 100%;
                        text-align: center;
                        background: #33537e;
                        display: block;
                        color: white;
                        padding: 10px;">
                        Commander une cuve
                    </a>
                    </div>`;

                return popup;
            },
            addElts : function(_context, data){
                var geoJson = [];
                $.each(data, function(k, v){
                    if(! _context.getOptions().data[k])
                            _context.getOptions().data[k] = v;
                    if(typeof v.geometry != "undefined"){
                        var data = $.extend({}, v);
                        var geoData = {
                            type: "Feature",
                            properties: {
                                name: v.name
                            },
                            geometry: v.geometry
                        };
                        delete data.name;
                        delete data.geometry; 
                        $.each(data, function(key,value) {
                            if(typeof geoData.properties[key] == 'undefined'){
                                geoData.properties[key] = value;
                            }
                        })
                        geoJson.push(geoData);
                    }
                    if(_context.getFiltres().result(v) && typeof v.geometry == "undefined" && typeof v.geo != "undefined"){
                        v.id = k;
                        _context.addMarker({ elt: v })
                        _context.getOptions().mapCustom.lists.createItemList(k, v, _context.getOptions().container)
                    }
                })
        
                    _context.getOptions().geoJSONlayer = L.geoJSON(geoJson, {
                    onEachFeature: function(feature, layer){
                        // _context.addPopUp(layer, feature);
                        layer.setStyle({
                            color: "#1a72d0",
                            opacity: 0.5
                        });
                        layer.on("click", function(event){
                            polygonClicked = true;
                            _context.addPopUp(layer, feature, true);
                        })
                        layer.on("mouseover", function(event){
                            this.setStyle({
                                "fillOpacity" : 1,
                                "strokeOpacity" : 1
                            });
                            let name = this.feature.properties.name;
                            // tooltip.innerHTML = name;
                            // tooltip.style.display = 'block';
                            // let x = event.containerPoint.x;
                            // let y = event.containerPoint.y;
                            // tooltip.style.left = `${x}px`;
                            // tooltip.style.top = `${y}px`;
                        });

                        layer.on("mouseout", function(){
                            this.setStyle({
                                "fillOpacity" : 0.2,
                                "strokeOpacity" : 0.2,
                            });
                            // tooltip.style.display = 'none';
                        });
                    }
                });

                _context.getMap().addLayer( _context.getOptions().geoJSONlayer);

                _context.hideLoader();
                // _context.fitBounds(data);
            }
        }
    });

    var map<?= $kunik ?>;


    function formLocalitycallDataGouv(requestPart, countryCode){
        var url = "//api-adresse.data.gouv.fr/search/?q=" + requestPart + ", La Possession 97419 , La Réunion";
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            async:false,
            crossDomain:true,
            complete: function () {},
            success: function (objDataGouv){
                if(objDataGouv.features.length > 1 ){
                    const resultsContainer = document.getElementById("resultsContainer");
                    resultsContainer.style.display = "block";
                    html = "";
                    objDataGouv.features.forEach(function(option , i){
                        html += `<li class="result-item" data-index="${i}">${option.properties.label}</li>`
                    });
                    resultsContainer.innerHTML = html;

                    $(".result-item").click(function(){
                        resultsContainer.innerHTML = "";
                        resultsContainer.style.display = "none";
                        var index = $(this).data("index");
                        var latlng = objDataGouv.features[index].geometry.coordinates;
                        var paramsMarker = {
                            elt : {
                                id : 0,
                                type : "2D",
                                geo : {
                                    latitude :  latlng[1],
                                    longitude : latlng[0],
                                }
                            },
                            opt : {
                                draggable: true
                            },
                            center : true
                        };
                        $.ajax({
                            url : baseUrl+"/co2/zone/findbatiment",
                            type: 'POST',
                            dataType: 'json',
                            data : {
                                latitude :  latlng[1],
                                longitude : latlng[0],
                            },
                            success : function(data){
                                let dataToAdd = data.batiment;
                                dataToAdd["marker"]={
                                    id : 0,
                                    type : "2D",
                                    geo : {
                                        latitude :  latlng[1],
                                        longitude : latlng[0],
                                    }
                                }
                                map<?= $kunik ?>.clearMap();
                                map<?= $kunik ?>.addElts(dataToAdd);
                            },
                            beforeSend : function () {
                                $(".loader-map").removeClass("hidden");
                            },
                            complete: function () {
                                $(".loader-map").addClass("hidden");
                            },
                        })
                    })
                } else {
                    var latlng = objDataGouv.features[0].geometry.coordinates;
                    var paramsMarker = {
                        elt : {
                            id : 0,
                            type : "2D",
                            geo : {
                                latitude :  latlng[1],
                                longitude : latlng[0],
                            }
                        },
                        opt : {
                            draggable: true
                        },
                        center : true
                    };
                    $.ajax({
                        url : baseUrl+"/co2/zone/findbatiment",
                        type: 'POST',
                        dataType: 'json',
                        data : {
                            latitude :  latlng[1],
                            longitude : latlng[0],
                        },
                        success : function(data){
                            let dataToAdd = data.batiment;
                            dataToAdd["marker"]={
                                id : 0,
                                type : "2D",
                                geo : {
                                    latitude :  latlng[1],
                                    longitude : latlng[0],
                                }
                            }
                            map<?= $kunik ?>.clearMap();
                            map<?= $kunik ?>.addElts(dataToAdd);
                        },
                        beforeSend : function () {
                            $(".loader-map").removeClass("hidden");
                        },
                        complete: function () {
                            $(".loader-map").addClass("hidden");
                        },
                    })
                }
            },
            error: function (thisError) {
                error(thisError);
            }
        });
    }

    $("#searchButton").on("click", function() {
        var address = $("#addressInput").val();
        var countryCode = "RE";
        formLocalitycallDataGouv(address , countryCode)
    })

    function fetchPrecipitations(){
        $.ajax({
            url : "https://qa.communecter.org/interop/meteofrance/commande/method/quotidienne-fichier",
            type: 'POST',
            dataType: 'json',
            data : {
                start : dateDebut,
                end : dateFin,
                station: "97408510"
            },
            success : function(data){
                if(data.success){
                    dataOfRR = data.content.body;
                }
            },

            beforeSend : function () {
                $(".loader-map").removeClass("hidden");
            },
            complete: function () {
                $(".loader-map").addClass("hidden");
            },
        })
    }

    $(function(){
        (new Promise(function(resolve, reject){
            map<?= $kunik ?> = new CoMap({
                container : "#map<?= $kunik ?>",
                activePopUp : false,
                activeCluster : false,
                mapOpt:{
                    btnHide : false,
                    doubleClick : false,
                    scrollWheelZoom: false,
                    zoom : 16,
                    center : [-20.927772603198584, 55.3348731994629]
                },
                mapCustom:paramsMapCO.mapCustom,
                elts : {}
            });
            map<?= $kunik ?>.getMap().on('moveend', () => {
                console.log("After click sur polygon", polygonClicked);
                if(polygonClicked){
                    // polygonClicked = false;
                    return;
                }else {
                    let bounds = map<?= $kunik ?>.getMap().getBounds();
                    const southwest = bounds.getSouthWest();
                    const northeast = bounds.getNorthEast();
                    getDatainBounds(southwest, northeast);
                }
            });
            map<?= $kunik ?>.getMap().on('click', (event) => {
                if(polygonClicked){
                    polygonClicked = false;
                }
            });
            resolve();
        })).then(function(){
            let bounds = map<?= $kunik ?>.getMap().getBounds();
            const southwest = bounds.getSouthWest();
            const northeast = bounds.getNorthEast();
            getDatainBounds(southwest, northeast);
            fetchPrecipitations();
        });
        
    });

    function getDatainBounds(southwest, northeast){
        $.ajax({
        url : baseUrl+"/co2/zone/findbatimentbounds",
        type: 'POST',
        dataType: 'json',
        data : {
            south :  {"lat": southwest.lat, "lng": southwest.lng},
            north : {"lat": northeast.lat, "lng": northeast.lng},
        },
        success : function(data){
            map<?= $kunik ?>.clearMap();
            map<?= $kunik ?>.addElts(data.batiments);
        },
        beforeSend : function () {
            $(".loader-map").removeClass("hidden");
        },
        complete: function () {
            $(".loader-map").addClass("hidden");
        },
    })
    }
    $("#validate").on("click", function() {
        dateDebut = $("#annee").val()+"-"+$("#mois").val()+"-01";
        dateFin =  $("#annee").val()+"-"+$("#mois").val()+"-31";
        fetchPrecipitations();
    })

</script>