<?php
$keyTpl		= "coeventmap";
$myCmsId	= isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
$styleCss	= (object) [$kunik => $blockCms["css"] ?? []];
?>
<style>
	.ce-map-title {
		font-size: 18px;
	}

	.ce-map-profile {
		display: inline-block;
		width: 50px;
		height: 50px;
		overflow: hidden;
		border-radius: 50%;
		vertical-align: middle;
		margin-right: 5px;
	}

	.ce-map-profile img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}
</style>
<style id="coeventmap">
</style>
<div class="coeventmap-container<?= $kunik ?> <?= $kunik ?>">
	<div id="thematic<?= $kunik ?>"></div>
	<div id="map<?= $kunik ?>" class="map" style="position: relative; overflow:show"></div>
</div>

<script>
	(function(X, $) {
		str = "";
		str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
		$("#coeventmap").append(str);
		var context;
		if (typeof costum === "object") {
			var match = location.hash.match(/\#[\w\-]+/);
			if (costum.contextType && costum.contextType === "events")
				context = {
					id: costum.contextId,
					type: "events"
				};
			else if (match && costum.app && costum.app[match[0]] && costum.app[match[0]].event)
				context = costum.app[match[0]].event;
			else
				context = {
					id: costum.contextId,
					type: costum.contextType
				};;
		}

		function get_session_data() {
			var session_data = sessionStorage.getItem('coevent-' + context.id + context.type);
			var filter = session_data ? JSON.parse(session_data) : {
				tags: [],
				regions: [],
				type: null
			};
			return (filter);
		}

		function request_get_subevents() {
			var defaultOptions = {
				fromToday: true,
				timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
			};
			var filter = get_session_data();
			filter = $.extend({}, filter, defaultOptions);
			var url = baseUrl + '/costum/coevent/get_events/request/subevents/id/' + context.id + "/type/" + context.type;
			return new Promise(function(resolve, reject) {
				ajaxPost(null, url, filter, resolve, resolve, 'json');
			});
		}

		function map_reload(map) {
			request_get_subevents().then(function(events) {
				var data = {};
				events.forEach(function(event) {
					if (typeof event.address === 'object' && typeof event.geo === 'object' && typeof event.geoPosition === 'object')
						data[event.id] = event;
				});
				map.clearMap();
				map.addElts(data);
			});
		}

		$(function() {
			var php = {
				legend_config: JSON.parse(JSON.stringify(<?= json_encode($blockCms["legende"]["config"]) ?>)),
				active_filter: JSON.parse(JSON.stringify(<?= json_encode($blockCms["activeFilter"]) ?>)),
				kunik: '<?= $kunik ?>',
				show_legend: JSON.parse(JSON.stringify(<?= json_encode($blockCms["legende"]["show"]) ?>))
			};
			var map;
			var custom_icon = {
				getPopup: function(data) {
					var l_filter_address = function(key) {
						return (typeof data.address[key] === 'string');
					};
					var l_map_address = function(key) {
						return (data.address[key]);
					};
					var date = {
						start: moment(data.start_date),
						end: moment(data.end_date)
					};
					var popup_option = {
						profile: '',
						banner_html: '',
						address: ['streetAddress', 'addressLocality'].filter(l_filter_address).map(l_map_address).join(' '),
						title: data.name,
						id: data.id
					};

					if (date.start.format('YYYY-MM-DD') === date.end.format('YYYY-MM-DD'))
						popup_option.date = dataHelper.printf('<i class="fa fa-calendar"></i> {{date}} <i class="fa fa-clock-o" aria-hidden="true"></i> {{start}} - {{end}}', {
							date: date.start.format('DD.MM.YYYY'),
							start: date.start.format('HH:mm'),
							end: date.end.format('HH:mm')
						});
					if (data.profile)
						popup_option.profile = '<div class="ce-map-profile"><img src="' + (baseUrl + data.profile) + '" /></div>';
					return (dataHelper.printf(
						'<div>' +
						'	{{banner_html}}' +
						'	<span><i class="fa fa-map"></i> {{address}}</span><br>' +
						'	<span>{{date}}</span><br>' +
						'	<div class="ce-map-title">{{profile}}{{title}}</div>' +
						'	<a href="#page.type.events.id.{{id}}" class="lbh-preview-element undefined item_map_list popup-marker" id="popup{{id}}"><div class="btn btn-sm btn-more col-md-12"><i class="fa fa-hand-pointer-o"></i>En savoir plus</div></a>' +
						'</div>', popup_option
					));
				},
				icon: {
					getIcon: function(data) {
						var option = {
							iconSize: [45, 55],
							iconAnchor: [25, 45],
							popupAnchor: [-3, -30],
							shadowUrl: '',
							shadowSize: [68, 95],
							shadowAnchor: [22, 94]
						};

						if (data.elt.marker)
							option.iconUrl = baseUrl + data.elt.marker;
						else
							option.iconUrl = modules.map.assets + '/images/markers/event-marker-default.png'
						return (L.icon(option));
					}
				}
			};
			var map_option = {
				container: '#map' + php.kunik,
				activePopUp: true,
				mapOpt: {
					btnHide: false,
					doubleClick: true,
					scrollWheelZoom: true,
					zoom: 2,
				},
				mapCustom: custom_icon,
				elts: {}
			};
			if (php.active_filter)
				map_option.menuRight = true;
			map = new CoMap(map_option);
			// events
			$('.coevent-program').on('coevent-filter', function() {
				map_reload(map);
			});
			map_reload(map);
		});
	})(window, jQuery);
</script>