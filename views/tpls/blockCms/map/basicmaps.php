<?php
    $keyTpl = "basicmaps";
    $myCmsId  = isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
    $styleCss = (object) [ $kunik => $blockCms["css"] ?? [] ];
?>
<style>
    .basicmaps-container<?= $kunik ?>{
        padding-top : 0px !important;
    }

    .basicmaps-container<?= $kunik ?> #menuRightmap<?php echo $kunik ?>{
        position: absolute !important;
    }
    .basicmaps-container<?= $kunik ?> a.title-4:focus,.basicmaps-container<?= $kunik ?> a.title-4:hover{
        font-weight: bold;
        text-decoration:none !important;
        border-bottom: 3px solid <?= @$blockCms["blockCmsColorTitle4"]; ?>;
    }
    .basicmaps-container<?= $kunik ?> a.title-4{
        padding-right: 0px;
        padding-left: 0px;
        margin: 0 15px;
    }
    .basicmaps-container<?= $kunik ?> .title-1{
        margin-top:-100px;
    }
    .menuRight .menuRight_header, .menuRight .panel_map, .menuRight .btn-panel {
        background-color: <?php echo @$blockCms['styleFilter']["headerColor"]; ?> !important;
        box-shadow: none !important;
    }

    .leaflet-popup-tip-container {
        bottom: -29px;
    }

    .m-1{
        margin: 0.3em;
    }

    .rounded<?= $kunik ?>{
        border-radius: 40px;
        text-transform: uppercase;
    }

    #thematic<?= $kunik ?>{
        background: #eee;
        /*padding: 0.3em;*/
    }

    #thematic<?= $kunik ?> .btn-white{
        color: black;
        background: white;
        font-weight: 900 !important;
    }

    #annuaireButton<?= $kunik ?> {
        border-width: 1px;
        text-align: center;
    }

    .buttons<?= $kunik ?>{
        display: flex;
        flex-direction: column;
        margin: 2%;
    }

    .buttons<?= $kunik ?> *{
        padding: 10px 15px;
        display: block;
        font-size: 20px;
    }

    .addButton<?= $kunik ?>{
        font-family: inherit !important;
        background-color: #76A5AF;
        color: white;
    }

    .legende-container<?= $kunik?>{
        position: absolute;
        left: auto;
        top: 50px;
        display: inline-block;
        z-index: 2;
        margin: 4px 0 2% 4%;
        border-radius: 10px;
        background: white;
        border:  2px solid #ddd;
   }

   .legende-container<?= $kunik?> ul {
        list-style-type: none;
        padding: 2px 3px 2px 8px;

    }
    #map<?= $kunik?> .leaflet-container{
        position: inherit !important;
        height: inherit !important;
        /*margin: 0 10px 0 10px;*/
    }
</style>
<style id="basicmaps<?= $kunik ?>">
</style>
<div class="basicmaps-container<?= $kunik ?> <?= $kunik ?>">
  <div id="thematic<?= $kunik ?>"></div> 
  <div id="map<?= $kunik ?>" class="map" style="position: relative; overflow:show"></div>
</div>

<script>
    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#basicmaps<?= $kunik ?>").append(str);
    var map<?= $kunik ?> = null;
    var legendeVar = <?= json_encode($blockCms["advanced"]["legendevar"]) ?>;
    $(function(){
        var customIcon={tile : "maptiler"};
        if(typeof paramsMapCO.mapCustom!="undefined"){
            paramsMapCO.mapCustom.mapLegends = <?= json_encode($blockCms["legende"]["config"]) ?>;
            customIcon = paramsMapCO.mapCustom;
        }else{
            paramsMapCO = $.extend(true, {}, paramsMapCO, {
                mapCustom: {
                    mapLegends: <?= json_encode($blockCms["legende"]["config"]) ?>,
                    icon: {
                        getIcon: function (params) {
                            var elt = params.elt;

                            var myCustomColour = '#F88518';

                            if (typeof elt[legendeVar] != "undefined") {
                                if(typeof elt[legendeVar] == "object"){
                                    $.each(elt[legendeVar], function (index, tag) {
                                        let sameKey = paramsMapCO.mapCustom.mapLegends.find(element => (element.label == tradCategory[tag] || element.label == tag));
                                        if (typeof sameKey != "undefined") {
                                            myCustomColour = sameKey.color;
                                        }
                                    });
                                }else{
                                    let sameKey = paramsMapCO.mapCustom.mapLegends.find(element => (element.label == tradCategory[elt[legendeVar]] || element.label == elt[legendeVar]));
                                    if (typeof sameKey != "undefined") {
                                        myCustomColour = sameKey.color;
                                    }
                                }
                            }

                            var markerHtmlStyles = `
                                background-color: ${myCustomColour};
                                width: 3.5rem;
                                height: 3.5rem;
                                display: block;
                                left: -1.5rem;
                                top: -1.5rem;
                                position: relative;
                                border-radius: 3rem 3rem 0;
                                transform: rotate(45deg);
                                border: 1px solid #FFFFFF`;

                            var myIcon = L.divIcon({
                                className: "my-custom-pin",
                                iconAnchor: [0, 24],
                                labelAnchor: [-6, 0],
                                popupAnchor: [0, -36],
                                html: `<span style="${markerHtmlStyles}" />`
                            });
                            return myIcon;
                        }
                    },
                    getClusterIcon: function (cluster) {
                        var childCount = cluster.getChildCount();
                        var c = ' marker-cluster-';
                        if (childCount < 100) {
                            c += 'small-'+costum.contextSlug;
                        } else if (childCount < 1000) {
                            c += 'medium-'+costum.contextSlug;
                        } else {
                            c += 'large-'+costum.contextSlug;
                        }
                        return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
                    }
                }
            });
        }
        map<?= $kunik ?> = new CoMap({
            container : "#map<?= $kunik ?>",
            activePopUp : true,
            mapOpt:{
                <?= ($blockCms["activeFilter"]!="" && $blockCms["activeFilter"]=="true")?'menuRight:true,':'' ?>
                btnHide : false,
                doubleClick : true,
                scrollWheelZoom: false,
                zoom : 2,
            },
            mapCustom:customIcon,
            elts : <?= json_encode($blockCms["elts"]) ?>
        });
        var button = L.control({
            position: 'topright'
        });
        button.onAdd = function(map) {
            var div = L.DomUtil.create('div', 'info button btn-type');
            var str = `<div class="buttons<?= $kunik ?>">
                    <a id="annuaireButton<?= $kunik ?>" href="#<?=$blockCms["btnAnnuaire"]["link"]?>" class="<?=($blockCms["btnAnnuaire"]["isLbh"]!=0)?'btn lbh':'lbh-anchor'?> styleAnnuaire" data-filters=""><?php echo $blockCms["btnAnnuaire"]["text"] ?></a>
                    <?php if($blockCms["btnAddText"]!=""){?>
                    <button class="btn margin-top-5 addButton<?= $kunik ?>" onclick="dyFObj.openForm('organization')"><?php echo $blockCms["btnAddText"] ?></button>
                    <?php } ?>
                </div>
            `;
            div.innerHTML = str;
            return div;
        }
        button.addTo(map<?= $kunik ?>.getMap());
        if(<?= json_encode($blockCms["legende"]["show"]) ?>){
            var legendeConfig = <?= json_encode($blockCms["legende"]["config"]) ?>;
            var legende = L.control({
                position: "bottomleft"
            })
            legende.onAdd = function(map){
                var div = L.DomUtil.create('div', 'mapLegendeContainer');
                var str = '<div style="max-height: 500px;overflow-y: auto;"><h4 class="no-margin">'+tradCms.legend+'</h4><svg id="map<?= $kunik ?>Legende"></svg></div>';

                str += '<button id="resetBtn<?= $kunik ?>" class="btn btn-sm hidden">Reinitialiser</button>';
                div.innerHTML = str;
                return div;
            }
            legende.addTo(map<?= $kunik ?>.getMap());
            var Svg = d3.select("#map<?= $kunik ?>Legende")
                // .attr("width", 200)
                .attr("height", legendeConfig.length * 30 + 20);
            Svg.selectAll(".d3map-legend").remove();

            Svg.selectAll("mydots")
                .data(legendeConfig)
                .join((enter) => {
                    let g = enter;
                    g = g.append("g").attr("height", "20px").attr("width", "100%").attr('class', 'd3map-legend').style("cursor", "pointer");
                    g.append("circle")
                        .attr("cx", 30)
                        .attr("cy", function(d,i){return 20 + i*30}) // 20 is where the first dot appears. 25 is the distance between dots
                        .attr("r", 7)
                        .style("fill", function(d){ return d.color})
                        .style("stroke", function(d){ return d.color})
                    g.append("text")
                        .attr("x", 50)
                        .attr("y", function(d,i){ return 20 + i*30}) // 20 is where the first dot appears. 25 is the distance between dots
                        .style("fill", function(d){ return d.color})
                        .text(function(d){ return d.label})
                        .attr("text-anchor", "left")
                        .style("font-size", "20px")
                        .style("alignment-baseline", "middle")
                        .on('click', function(d, i){
                            $("#resetBtn<?= $kunik ?>").removeClass("hidden");
                            i = (i.label == 'Autre' ? '' : i.label);
                            var data = Object.fromEntries(Object.entries(map<?= $kunik ?>.getOptions().data).
                            filter(([key, val]) => {
                                return i == '' ? (!val[legendeVar] || (val[legendeVar] && val[legendeVar].some(ai => legendeConfig.map(function(element){return element.label;}).includes(ai)) == false)) : (!val[legendeVar] ? false : (val[legendeVar].includes(i)));
                            }))
                            map<?= $kunik ?>.clearMap();
                            map<?= $kunik ?>.addElts(data);
                        })
                    return g;
                })
            d3.select("#resetBtn<?= $kunik ?>").on('click', function(){
                $(".mapLoading").show();
                $("#resetBtn<?= $kunik ?>").addClass("hidden");
                setTimeout(function(){
                    map<?= $kunik ?>.clearMap();
                    map<?= $kunik ?>.addElts(map<?= $kunik ?>.getOptions().elts);
                }, 100)
            })
        }
        addThematic();
        addLinkedData(map<?= $kunik ?>.getOptions().data);
    })

    function randomizeCurve() {
        return (Math.PI / 10) + (Math.random() * (Math.PI / 5));  // Randomize between a small and moderate angle
    }

    function calculateCurve(latlng1, latlng2){
        var offsetX = latlng2[1] - latlng1[1],
        offsetY = latlng2[0] - latlng1[0];
         // Calculer les décalages en X et Y
         var r = Math.sqrt(Math.pow(offsetX, 2) + Math.pow(offsetY, 2)),
            theta = Math.atan2(offsetY, offsetX);

        // Générer un angle aléatoire pour la courbure
        var thetaOffset = randomizeCurve();

        // Calculer la distance et l'angle pour le point de contrôle intermédiaire
        var r2 = (r / 2) / (Math.cos(thetaOffset)),
            theta2 = theta + thetaOffset;

        var midpointX = (r2 * Math.cos(theta2)) + latlng1[1],
            midpointY = (r2 * Math.sin(theta2)) + latlng1[0];

        var midpointLatLng = [midpointY, midpointX];
        return midpointLatLng;
    }

    function getCurve(latlngs) {
        var curvePath = [];

        for (var i = 0; i < latlngs.length - 1; i++) {
            var latlng1 = latlngs[i];
            var latlng2 = latlngs[i + 1];
            curvePath.push('M', latlng1);  // Démarre un nouveau segment à latlng1
            curvePath.push('Q', calculateCurve(latlng1, latlng2), latlng2);
        }
        mylog.log("curvePath", curvePath);
        return curvePath;
    }

    function addLinkedData(data, clicked = "all"){
        var linkedShow = <?= json_encode($blockCms['advanced']['linked']['linkshow']) ?>;
        if(linkedShow && typeof costum.lists["<?= $blockCms['advanced']['linked']['linkcible'] ?>"] != "undefined"){
            var linkedColor = <?= json_encode($blockCms['advanced']['linked']['linkconfig']) ?>;
            var legendes = [];
            var inList = (costum.lists["<?= $blockCms['advanced']['thematic']['themecible'] ?>"] || {});
            for (const [keyT, valueT] of Object.entries(inList)) {
                var asTags = [];
                let labelT = "";
                if(typeof valueT == "string"){
                    asTags.push(valueT);
                    labelT = valueT;
                }else{
                    labelT = keyT;
                    for (const [keyChild, valueChild] of Object.entries(valueT)){
                        asTags.push(valueChild);
                    }
                }
                var linkedData = Object.entries(data).
                filter(([key, val]) => {
                    return (typeof val[legendeVar] != "undefined" && val[legendeVar].some(ai => asTags.includes(ai)));
                });
                let polylineData = linkedData.map(([key, val]) => {
                        if(typeof val.geo != "undefined"){
                            return [parseFloat(val.geo.latitude), parseFloat(val.geo.longitude)];
                        }
                    }).filter((element) => element != undefined).sort((a, b) => {
                        if (a[0] === b[0]) {
                            return a[1] - b[1];  // Si les latitudes sont égales, trier par longitude
                        }
                        return a[0] - b[0];  // Sinon, trier par latitude
                    });
                var polyline = {
                    "id": labelT,
                    "coordinates": getCurve(polylineData)
                };
                if(polyline.coordinates.length > 0 && (clicked == "all" || clicked != labelT)){
                    legendes.push(labelT);
                    let colorData = linkedColor.filter(function(element){return element.label == labelT;});
                    var style = {
                        "color": typeof colorData[0] != "undefined" ? colorData[0].color : "#f88518",
                        "weight": 4,
                        "opacity": 1,
                        smoothFactor: 1
                    }

                    map<?= $kunik ?>.addCurveLine(polyline, style);
                }
            }
            if($(".maplinkedContainer").length > 0){
                $(".maplinkedContainer").remove();
            }
            var linked = L.control({
                    position: "bottomright"
                })
                linked.onAdd = function(map){
                    var div = L.DomUtil.create('div', 'maplinkedContainer');
                    var str = '<div style="max-height: 500px;overflow-y: auto; background-color: white"><h4 class="no-margin padding-10">'+tradCms.legend+' du ligne</h4><svg id="map<?= $kunik ?>linked"></svg></div>';

                    // str += '<button id="resetBtn<?= $kunik ?>" class="btn btn-sm hidden">Reinitialiser</button>';
                    div.innerHTML = str;
                    return div;
                }
                linked.addTo(map<?= $kunik ?>.getMap());
                var Svg = d3.select("#map<?= $kunik ?>linked")
                    // .attr("width", 200)
                    .attr("height", linkedColor.length * 30 + 20);
                Svg.selectAll(".d3map-legend").remove();
                linkedColor = linkedColor.filter(function(element){
                    return (legendes.includes(element.label));
                });
                Svg.selectAll("mydots")
                    .data(linkedColor)
                    .join((enter) => {
                        let g = enter;
                        g = g.append("g")
                            .attr("height", "20px")
                            .attr("width", "100%")
                            .attr('class', 'd3map-legend')
                            .style("cursor", "pointer")
                            .on('click', function(d, i){
                                let g = d3.select(this);
                                if(!map<?= $kunik ?>.polylineVisible(i.label)){
                                    g.select("line").style("stroke", i.color);
                                    g.select("text").style("fill", i.color);
                                }else{
                                    g.select("line").style("stroke", "darkgrey");
                                    g.select("text").style("fill", "darkgrey");
                                }
                                map<?= $kunik ?>.togglePolyline(i.label);
                            });
                        g.append("line")
                            .attr("x1", 20)  // Position horizontale du début de la ligne
                            .attr("x2", 40)  // Position horizontale de la fin de la ligne
                            .attr("y1", function(d,i){ return 20 + i*30 })  // Position verticale du début
                            .attr("y2", function(d,i){ return 20 + i*30 })  // Position verticale de la fin (identique pour créer une ligne horizontale)
                            .style("stroke-width", 4)  // Épaisseur de la ligne
                            .style("stroke", function(d){ return d.color })  // Couleur de la ligne
                            .style("stroke-linecap", "round"); 
                        g.append("text")
                            .attr("x", 50)
                            .attr("y", function(d,i){ return 20 + i*30}) // 20 is where the first dot appears. 25 is the distance between dots
                            .style("fill", function(d){ return d.color;})
                            .text(function(d){ return d.label})
                            .attr("text-anchor", "left")
                            .style("font-size", "20px")
                            .style("alignment-baseline", "middle")
                            
                        return g;
                    })
        }
    }

    function addThematic() {
        var thematicShow = <?= json_encode($blockCms['advanced']['thematic']['themeshow']) ?>;
        if(thematicShow && typeof costum.lists["<?= $blockCms['advanced']['thematic']['themecible'] ?>"] != "undefined"){
            var inList = (costum.lists["<?= $blockCms['advanced']['thematic']['themecible'] ?>"] || {});
            for (const [keyT, valueT] of Object.entries(inList)) {
                var asTags = [];
                let labelT = "";
                if(typeof valueT == "string"){
                    asTags.push(valueT);
                    labelT = valueT;
                }else{
                    labelT = keyT;
                    for (const [keyChild, valueChild] of Object.entries(valueT)){
                        // if(!thematic.includes(valueChild)){
                        //     thematic.push(valueChild);
                        // }
                        asTags.push(valueChild);
                    }
                }
                $("#thematic<?= $kunik ?>").append('<a type="button" class="btn btn-white m-1 rounded<?= $kunik ?> thematic<?= $kunik ?>" data-filters="'+asTags.join(',')+'">'+labelT+'<!--span class="badge">0</span--></a>');
            }
            bindClickThematic();
        }else{
            $("#thematic<?= $kunik ?>").remove();
        } 
    }

    
    function bindClickThematic() {
        $(".thematic<?= $kunik ?>").off().on('click', function(){
            var filters = $(this).data('filters').split(",");
            $(".thematic<?= $kunik ?>").not(this).removeClass("btn-primary").addClass("btn-white");
            var idPolyline = $(this).text();
            if($(this).hasClass("btn-primary")){
                $(this).removeClass("btn-primary");
                $(this).addClass("btn-white");
                $(".mapLoading").show();
                setTimeout(function(){
                    map<?= $kunik ?>.clearMap();
                    map<?= $kunik ?>.addElts(map<?= $kunik ?>.getOptions().elts);
                    addLinkedData(map<?= $kunik ?>.getOptions().elts);
                }, 100)
            }else{
                var data = Object.fromEntries(Object.entries(map<?= $kunik ?>.getOptions().data).
                filter(([key, val]) => {
                    return (typeof val[legendeVar] != "undefined" && val[legendeVar].some(ai => filters.includes(ai)));
                }));
                map<?= $kunik ?>.clearMap();
                map<?= $kunik ?>.addElts(data);
                addLinkedData(data, idPolyline);
                $(this).addClass("btn-primary");
                $(this).removeClass("btn-white");
            }
            
        })
    }
    <?php if($blockCms["btnAnnuaire"]["isLbh"]!="true" || !$blockCms["btnAnnuaire"]["isLbh"]){ ?>
        $("#annuaireButton<?= $kunik ?>").on("click", function() {
            var elementtoScrollToID = $(this).attr("href");
            $([document.documentElement, document.body]).animate({
                    scrollTop: $(elementtoScrollToID).offset().top
            }, 2000);
        });
    <?php } ?>
    if(costum.editMode){
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        var basicmapsInput = {
            configTabs: {
                general : {
                    inputsConfig : [
                        {
                            type: "select",
                            options: {
                                name : "activeFilter",
                                label: tradCms.filtr,
                                options: [
                                    {"value" : "true", label: "Map avec filtre"},
                                    {"value" : "false", label: "Map sans filtre"}
                                ]
                            }
                        },
                        {
                            type: "selectMultiple",
                            options: {
                                name: "elementsType",
                                label: tradCms.mapElement,
                                options: [
                                    {value : "organizations", label: trad.organizations},
                                    {value : "citoyens" , label: tradCategory.citizen},
                                    {value : "events" , label: trad.events},
                                    {value : "projects" , label: trad.projects},
                                    {value : "poi" , label: trad.poi}
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name : "btnAnnuaire",
                                label : `${tradCms.directoryBtn}`,
                                inputs: [
                                    {
                                        type: "inputSimple",
                                        options: {
                                            label: tradCms.text,
                                            name: "text"
                                        }
                                    },
                                    {
                                        type: "inputSimple",
                                        options: {
                                            label: tradCms.targetLink,
                                            name: "link"
                                        }
                                    },
                                    // {
                                    //     type: "groupButtons",
                                    //     options: {
                                    //         "name": "isLbh",
                                    //         "label" : "",
                                    //         options: [
                                    //             {
                                    //                 value : false,
                                    //                 label : trad.no
                                    //             },
                                    //             {
                                    //                 value : true,
                                    //                 label : trad.yes
                                    //             }
                                    //         ]
                                    //     }
                                    // }
                                ]
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                label: tradCms.textonbtnorganization,
                                name: "btnAddText"
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "legende",
                                label: tradCms.legend,
                                inputs: [
                                    {
                                        type: "groupButtons",
                                        options: {
                                            "name": "show",
                                            "label" : tradCms.showlegende,
                                            options: [
                                                {
                                                    value : false,
                                                    label : trad.no
                                                },
                                                {
                                                    value : true,
                                                    label : trad.yes
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        type: "select",
                                        options: {
                                            name: "cible",
                                            class: `legendecible-${cmsConstructor.kunik} hidden`,
                                            label: "Legende Cible",
                                            options: Object.keys((costum.lists||{})).map(function(element){
                                                return {label: element, value: element}
                                            })
                                        }
                                    },
                                    {
                                        type: "inputMultiple",
                                        options: {
                                            label: tradCms.preferencelegende,
                                            name: "config",
                                            class: `legendeconfig-${cmsConstructor.kunik} hidden`,
                                            inputs: [
                                                [
                                                    "color",
                                                    {
                                                        type: "inputSimple",
                                                        options: {
                                                            label: tradCms.label,
                                                            name: "label",
                                                        }
                                                    }
                                                ]
                                            ]
                                        }
                                    },
                                ]
                            }
                        },
                        {
                            type: "groupButtons",
                            options: {
                                "name": "activeContour",
                                "label" : tradCms.showzoneoutline,
                                options: [
                                    {
                                        value : false,
                                        label : trad.no
                                    },
                                    {
                                        value : true,
                                        label : trad.yes
                                    }
                                ]
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name : `map`,
                                label: trad.map,
                                inputs: [
                                    "height"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name : "styleAnnuaire",
                                label : tradCms.directoryBtn,
                                inputs: [
                                    "backgroundColor",
                                    "color",
                                    "borderRadius",
                                    "borderColor"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name : "styleFilter",
                                label : tradCms.filtr,
                                inputs: [
                                    {
                                        type: 'colorPicker',
                                        options: {
                                            name: "headerColor",
                                            label: tradCms.filterHeaderColor,
                                            defaultValue: cmsConstructor.sp_params['<?= $myCmsId ?>'].styleFilter.headerColor
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                "name": "mapLegendeContainer",
                                "label": tradCms.legend,
                                inputs: [
                                    "backgroundColor",
                                    "padding"
                                ]
                            }
                        }
                    ]
                },
                advanced: {
                    inputsConfig: [
                        {
                            type: "inputSimple",
                            options: {
                                name: "legendevar",
                                label: tradCms.legendVariable
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "thematic",
                                label: tradCms.thematic,
                                inputs: [
                                    {
                                        type: "groupButtons",
                                        options: {
                                            "name": "themeshow",
                                            "label" : tradCms.showThematic,
                                            options: [
                                                {
                                                    value : false,
                                                    label : trad.no
                                                },
                                                {
                                                    value : true,
                                                    label : trad.yes
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        type: "select",
                                        options: {
                                            name: "themecible",
                                            class: `theme-${cmsConstructor.kunik} hidden`,
                                            label: tradCms.thematicTarget,
                                            options: Object.keys((costum.lists||{})).map(function(element){
                                                return {label: element, value: element}
                                            })
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "linked",
                                label: "Lier les élements",
                                inputs: [
                                    {
                                        type: "groupButtons",
                                        options: {
                                            "name": "linkshow",
                                            "label" : "Afficher les liens",
                                            options: [
                                                {
                                                    value : false,
                                                    label : trad.no
                                                },
                                                {
                                                    value : true,
                                                    label : trad.yes
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        type: "select",
                                        options: {
                                            name: "linkcible",
                                            class: `linkcible-${cmsConstructor.kunik} hidden`,
                                            label: "Cible des liens",
                                            options: Object.keys((costum.lists||{})).map(function(element){
                                                return {label: element, value: element}
                                            })
                                        }
                                    },
                                    {
                                        type: "inputMultiple",
                                        options: {
                                            label: tradCms.preferencelegende,
                                            name: "linkconfig",
                                            class: `linkConfig-${cmsConstructor.kunik} hidden`,
                                            inputs: [
                                                [
                                                    "color",
                                                    {
                                                        type: "inputSimple",
                                                        options: {
                                                            label: tradCms.label,
                                                            name: "label",
                                                        }
                                                    }
                                                ]
                                            ]
                                        }
                                    },
                                ]
                            }
                        }
                    ]
                }
            },
            beforeLoad: function(){
                if(cmsConstructor.sp_params[cmsConstructor.spId].legende.show){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[4].options.inputs[2].options.class = `legendeconfig-${cmsConstructor.kunik}`
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[4].options.inputs[1].options.class = `legendecible-${cmsConstructor.kunik}`
                }else{
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[4].options.inputs[1].options.class = `legendecible-${cmsConstructor.kunik} hidden`
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[4].options.inputs[2].options.class = `legendeconfig-${cmsConstructor.kunik} hidden`
                }
                if(cmsConstructor.sp_params[cmsConstructor.spId].advanced.thematic.themeshow){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.advanced.inputsConfig[1].options.inputs[1].options.class = `themecible-${cmsConstructor.kunik}`
                }else{
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.advanced.inputsConfig[1].options.inputs[1].options.class = `themecible-${cmsConstructor.kunik} hidden`
                }
                if(cmsConstructor.sp_params[cmsConstructor.spId].advanced.linked.linkshow){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.advanced.inputsConfig[2].options.inputs[1].options.class = `linkcible-${cmsConstructor.kunik}`
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.advanced.inputsConfig[2].options.inputs[2].options.class = `linkconfig-${cmsConstructor.kunik}`
                }else{
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.advanced.inputsConfig[2].options.inputs[1].options.class = `linkcible-${cmsConstructor.kunik} hidden`
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.advanced.inputsConfig[2].options.inputs[2].options.class = `linkconfig-${cmsConstructor.kunik} hidden`
                }
            },
            onChange: function(path, valueToSet, name, payload, value){
                
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                if(name == "show"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[4].options.showInDefault = true;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "themeshow"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.advanced.inputsConfig[1].options.showInDefault = true;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "linkshow"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.advanced.inputsConfig[2].options.showInDefault = true;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "cible"){
                    if(typeof costum.lists[value] != "undefined"){
                        var legendeConfig = [];
                        for (const key in (costum.lists[value]||{})) {
                            if ((costum.lists[value]||{}).hasOwnProperty(key)) {
                                if(typeof costum.lists[value][key] == "object" && !Array.isArray(value)){
                                  legendeConfig.push({label: key, color: "#f88518"});  
                                }else{
                                  legendeConfig.push({label: costum.lists[value][key], color: "#f88518"});  
                                }
                            }
                        }
                        var data = {
                            id: cmsConstructor.spId,
                            collection: "cms",
                            path: "legende.config",
                            value: legendeConfig
                        };
                        dataHelper.path2Value( data, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[4].options.showInDefault = true;
                                cmsConstructor.sp_params[cmsConstructor.spId].legende.config = legendeConfig;
                                $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                            });
                        });
                    }
                }
                if(name == "linkcible"){
                    if(typeof costum.lists[value] != "undefined"){
                        var legendeConfig = [];
                        // var legendeConfig = Object.values((costum.lists[value]||{})).map(function(element){
                        //     return {label: element, color: "#f88518"}
                        // });

                        for (const key in (costum.lists[value]||{})) {
                            if ((costum.lists[value]||{}).hasOwnProperty(key)) {
                                if(typeof costum.lists[value][key] == "object" && !Array.isArray(value)){
                                  legendeConfig.push({label: key, color: "#f88518"});  
                                }else{
                                  legendeConfig.push({label: costum.lists[value][key], color: "#f88518"});  
                                }
                            }
                        }
                        var data = {
                            id: cmsConstructor.spId,
                            collection: "cms",
                            path: "advanced.linked.linkconfig",
                            value: legendeConfig
                        };
                        dataHelper.path2Value( data, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                cmsConstructor.blocks['<?= $kunik ?>'].configTabs.advanced.inputsConfig[2].options.showInDefault = true;
                                cmsConstructor.sp_params[cmsConstructor.spId].advanced.linked.linkconfig = legendeConfig;
                                $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                            });
                        });
                    }
                }
            }
        }
        cmsConstructor.blocks['<?= $kunik ?>'] = basicmapsInput;
    }
</script>