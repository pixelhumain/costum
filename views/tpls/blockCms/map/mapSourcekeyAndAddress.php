<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
	
    if( !(is_array($blockCms["title"]))){
        $title = ["fr" => $blockCms["title"]];
    } else {
        $title = $blockCms["title"];
    }
?>
<style type="text/css" id="<?= $kunik ?>mapSourcekeyAndAddress">
	.listCostum<?= $kunik?> {
		margin-bottom: 20px;
		margin-bottom: 20px;
	}
	.listCostum<?= $kunik?> .leaflet-popup-content-wrapper {
		border: 3px solid #005e6f;
	}
	.listCostum<?= $kunik?> .popup-address{
		margin-top: 7px;
		font-size: 16px;
	}
	.listCostum<?= $kunik?> .btn-more {
		border-color: #005e6f !important;
		color: #005e6f !important;
	}
	.listCostum<?= $kunik?> .btn-more:hover {
		background-color: #005e6f !important;
		color: white !important;
	}

	.listCostum<?= $kunik?> .leaflet-container a.leaflet-popup-close-button {
		color: #005e6f !important;
	}
	.rounded<?= $kunik ?>{
        border-radius: 40px;
        text-transform: uppercase;
    }

    #thematic<?= $kunik ?>{
        background: #eee;
        padding: 0.3em;
    }

    #thematic<?= $kunik ?> .btn-white{
        color: black;
        background: white;
        font-weight: 900 !important;
    }
    #thematic<?= $kunik ?> .m-1{
        margin: 0.3em;
    }
    #thematic<?= $kunik ?> .btn-active {
    	color: white;
    	background-color: #052434;
    	border-color: #052434;
    	font-weight: 700;
    }
    #annuaireButton<?= $kunik ?> {
    	position: absolute;
    	top: 20px;
    	right: 20px;
    	padding: 8px 10px;
    	z-index: 400;
    	font-size: 20px;
    	border-radius: 10px;
    	color: white;
    	text-decoration: none;
    	background-color: #052434;
    }
</style>
<div class=" costumActif_<?= $kunik?> text-center">
	<h1  class="sp-text img-text-bloc title<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"></h1>
	<div class="">
		<div class="row">
			<div class="main_<?= $kunik?> col-md-12 col-sm-12 col-xs-12 vertical" >
				<div id="thematic<?= $kunik ?>"></div>
				<div class="col-xs-12">
					<div id="nullAdresse<?= $kunik ?>"></div>
					<div id="listCostum" class="no-padding col-xs-12 listCostum<?= $kunik?>"  style="height: 70vh; position: relative;" >

					</div>
					<a id="annuaireButton<?= $kunik ?>" href="#search" class="btn  lbh" data-filters="">Voir dans l'annuaire</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var str = "";
	var elCostum = <?= json_encode($el) ?>;
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#<?= $kunik ?>mapSourcekeyAndAddress").append(str);
	if(costum.editMode)
	{
		cmsConstructor.sp_params["<?= $blockKey ?>"] = <?php echo json_encode( $blockCms ); ?>;
		var mapSourcekeyAndAddress = {
			configTabs : {
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
		}
		cmsConstructor.blocks.mapSourcekeyAndAddress<?= $blockKey ?> = mapSourcekeyAndAddress;
	};

    appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($title) ?>,"<?= $blockKey ?>");

	var contextData = <?php echo json_encode($el); ?>;
    var filteredTheme = "";
	var map<?= $kunik ?> = new CoMap({
		container : ".listCostum<?= $kunik?>",
		activePopUp : true,
		mapOpt:{
			menuRight : false,
			btnHide : false,
			doubleClick : true,
			zoom : 2,
        	scrollWheelZoom: true
		},
		mapCustom: {
			tile : "maptiler"
		},
		elts : []
	});
	var blocCostumObj<?= $kunik?> ={
		costumList:function(criteriaParams = null){
				var params = {
					searchType : ["organizations", "poi", "projects","classifieds","citoyens", "ressources"],
					notSourceKey:true,
					filters : {
						$or :{
							"source.keys" : ["<?=$el["slug"]?>"]
						}
					},
					count: true,
					countType : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative", "ressources", "poi", "projects","classifieds","citoyens"]
					// indexStep : 0
				};
				<?php if(isset($el["address"])) { ?>
						$("#nullAdresse<?= $kunik ?>").html("");
						var idAddress = {};
						var thematic = <?= (isset($el["thematic"]) && is_string($el["thematic"]) && strpos($el["thematic"], ',') === false) ? json_encode($el["thematic"]) : 'null' ?>;

						if (typeof costum.typeCocity !== "undefined") {
							switch (costum.typeCocity) {
								case "region":
									idAddress = { "address.level3": elCostum.address.level3 };
									break;
								case "ville":
									idAddress = { "address.localityId": elCostum.address.localityId };
									break;
								case "departement":
								case "district":
									idAddress = { "address.level4": elCostum.address.level4 };
									break;
								case "epci":
									idAddress = { "address.localityId": { $in: costum.citiesArray } };
									break;
							}
						} else if (elCostum.address.level3) {
							idAddress = { "address.level3": elCostum.address.level3 };
						}

						if (thematic) {
							var source = {};
							var tags = thematic.toLowerCase();
							var sourcekey = thematic === "Tiers lieux" ? "franceTierslieux" : thematic === "Pacte" ? "siteDuPactePourLaTransition" : null;

							if (sourcekey) {
								source["$or"] = [
									{ "source.keys": sourcekey },
									{ "reference.costum": sourcekey },
									{ "tags": tags }
								];
							} else {
								source = { "tags": tags };
							}

							var addressParams = [idAddress, source];
							idAddress = { "$and": addressParams };
						}

						var addressFilters = {
							"source.keys": "<?= $el['slug'] ?>",
							...idAddress,
							"links.memberOf.<?= (string)$el['_id'] ?>": { '$exists': true }
						};
						params.filters["$or"] = addressFilters;
					<?php } else { ?>
						var nullAdress = "<p style='font-size: 18px; padding: 5px'>Merci d'inscrire votre adresse dans les détails de votre page élément.</p>";
						$("#nullAdresse<?= $kunik ?>").html(nullAdress);
					<?php } ?>
				// < ?php if(isset($el["address"])){ ?>
				// 	$("#nullAdresse<?= $kunik ?>").html("");
				// 	var idAddress = {};
				// 	if (typeof costum.typeCocity !== "undefined") {
				// 		switch (costum.typeCocity) {
				// 			case "region":
				// 				idAddress = { "address.level3": elCostum.address.level3 };
				// 				break;
				// 			case "ville":
				// 				idAddress = { "address.localityId": elCostum.address.localityId };
				// 				break;
				// 			case "departement":
				// 			case "district":
				// 				idAddress = { "address.level4": elCostum.address.level4 };
				// 				break;
				// 			case "epci":
				// 				idAddress = { "address.localityId": { $in: costum.citiesArray } };
				// 				break;
				// 		}
				// 	} else if (elCostum.address.level3) {
				// 		idAddress = { "address.level3": elCostum.address.level3 };
				// 	}
				// 	var addressFilters = {
				// 		"source.keys": "<?= $el['slug'] ?>",
				// 		...idAddress,
				// 		"links.memberOf.<?= (string)$el['_id'] ?>": {'$exists': true}
				// 	};
				// 	params.filters["$or"] = addressFilters;
				// < ?php } else { ?> 
				// 	var nullAdress = "<p style='font-size: 18px; padding: 5px'>Merci d'inscrire votre adresse dans les détails de votre page.</p>"
				// 	$("#nullAdresse<?= $kunik?>").html(nullAdress);
				// < ?php } ?>

				if(criteriaParams!=null && criteriaParams!=""){
					params["searchTags"] = criteriaParams.split(",");
				}
				// < ?php if (isset($el["thematic"]) && is_string($el["thematic"]) && strpos($el["thematic"], ',') === false) {?>
				// 	params.filters['$or'].thematic = elCostum.thematic;
				// < ?php } ?>

				ajaxPost(
					null,
					baseUrl + "/" + moduleId + "/search/globalautocomplete",
					params,
					function(data){
						map<?= $kunik ?>.clearMap();
						map<?= $kunik ?>.addElts(data.results);

					}
				);
		}
	}    
	jQuery(document).ready(function() {	
		blocCostumObj<?= $kunik?>.costumList();
        var thematic = [];
        if(costum && costum.tagsFilters && costum.tagsFilters.theme){
            for (const [keyT, valueT] of Object.entries(costum.tagsFilters.theme)) {
				if(elCostum.tags && (elCostum.tags).includes(keyT)){
					var asTags = [];
					let labelT = "";

					if(typeof valueT == "string"){
						asTags.push(valueT);
						labelT = valueT;
					} else {
						labelT = keyT;
						for (const [keyChild, valueChild] of Object.entries(valueT)){
							if(!thematic.includes(valueChild)){
								thematic.push(valueChild);
							}
							asTags.push(valueChild);
						}
					}
                	$("#thematic<?= $kunik ?>").append('<a type="button" class="btn btn-white m-1 rounded<?= $kunik ?> thematic<?= $kunik ?>" data-filters="'+asTags.join(',')+'">'+labelT+'<!--span class="badge">0</span--></a>');
            
				}
			}   
        }else{
            $("#thematic<?= $kunik ?>").remove();
        }
        $(".thematic<?= $kunik ?>").off().on("click",function(){
            if(filteredTheme.indexOf($(this).data("filters")) !== -1){
                filteredTheme = filteredTheme.replace($(this).data("filters"), "");
                $(this).removeClass("btn-active");
                $(this).addClass("btn-white");
            }else{
                $(this).removeClass("btn-white");
                $(this).addClass("btn-active");
                if(filteredTheme==""){
                    filteredTheme = $(this).data("filters");
                }else{
                    filteredTheme+=","+$(this).data("filters");
                }
                $("#annuaireButton<?= $kunik ?>").attr("href", "#search?tags="+filteredTheme);
            }
            if(filteredTheme.charAt(0)==","){
                filteredTheme = filteredTheme.substring(1);
            }
            if(filteredTheme.length==0){
                $("#annuaireButton<?= $kunik ?>").attr("href", "#search");
            }
            blocCostumObj<?= $kunik?>.costumList(filteredTheme);
        });
	})	
</script>