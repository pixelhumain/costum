<?php
$keyTpl = "worldNavigateCocity";
$myCmsId  = isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
$styleCss = (object) [$kunik => $blockCms["css"] ?? []];
$baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
?>
<style>
    .<?= $kunik ?> .map-container {
        height: 83vh;
        width: auto;
    }

    .<?= $kunik ?> .tooltip {
        position: absolute;
        background-color: white;
        color: black;
        padding: 5px;
        border-radius: 5px;
        display: none;
        pointer-events: none;
        opacity: 1 !important;
        font-weight: bold;
        padding: 15px;
    }

    .<?= $kunik ?> .navigate-section {
        font-size: 21px;
    }

    .<?= $kunik ?> .navigate-section i {
        cursor: pointer;
    }

    .<?= $kunik ?> .navigate-section .right-position {
        margin-left: 84%;
    }

    .<?= $kunik ?> .navigate-section .position-right {
        margin-left: 94%;
    }

    .loader-loader<?= $kunik ?> {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 42%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 9999999999999999 !important;
        padding-top: 180px !important;
    }

    .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> {
        width: auto;
        height: auto;
    }

    .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> .processingLoader {
        width: auto !important;
        height: auto !important;
    }

    .backdrop-loader<?= $kunik ?> {
        position: fixed;
        opacity: .8;
        background-color: #000;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100vh;
        z-index: 999999999 !important;
    }

    .progress-container-world {
        width: 80%;
        background-color: #e0e0e0;
        border-radius: 25px;
        margin: 20px auto;
        overflow: hidden;
        position: relative;
        height: 20px;
        margin-top: 40%;
    }

    .progress-bar-world {
        height: 100%;
        width: 0%;
        background-color: #84d70f;
        border-radius: 25px 0 0 25px;
        transition: width 0.3s;
        position: absolute;
        top: 0;
        left: 0;
    }

    .progress-percentage-world {
        position: absolute;
        width: 100%;
        text-align: center;
        top: 50%;
        transform: translateY(-50%);
        font-size: 16px;
        color: #000;
    }

    .progress-message-world {
        text-align: center;
        font-size: 18px;
        margin-bottom: 10px;
        color: #ffffff;
    }

    .btn-more:hover {
        color: white !important;
        background-color: #092434;
    }

    .popup-thema {
        display: flex;
        flex-wrap: wrap;
        gap: 10px;
    }

    .popup-thema .fa {
        padding: 5px;
        margin: 3px;
        font-size: 16px;
        cursor: pointer;
        color: #013c55;
    }

    .popup-thema a {
        border: 1px solid #083557;
        padding: 2px 10px;
        border-radius: 5px;
        font-size: 15px;
        color: #083557;
        display: flex;
        align-items: center;
        justify-content: center;
        min-width: 150px;
        text-align: center;
        margin-bottom: 4px;
        max-width: 180px;
    }

    .thema-list {
        max-height: 240px;
        width: 100%;
        overflow: auto;
    }

    .thema-list::-webkit-scrollbar,
    .div-tags-popup::-webkit-scrollbar {
        width: 8px;
    }

    .thema-list::-webkit-scrollbar-thumb,
    .div-tags-popup::-webkit-scrollbar-thumb {
        background: #ccc;
        border-radius: 5px;
    }

    .thema-list::-webkit-scrollbar-thumb:hover,
    .div-tags-popup::-webkit-scrollbar-thumb:hover {
        background: #aaa;
    }

    .thema-list::-webkit-scrollbar-track,
    .div-tags-popup::-webkit-scrollbar-track {
        background: #f4f4f4;
        border-radius: 5px;
    }

    .popup-tags {
        color: #fff;
        font-weight: 500;
        text-decoration: none;
        display: inline-block;
        padding: 4px 6px;
        background: #083557;
        font-size: 13px;
        border-radius: 10px;
        margin: 0px 2px 3px 0;
        white-space: nowrap;
    }

    .div-tags-popup {
        margin-top: 5px;
        max-height: 150px;
        overflow: auto;
    }

    .<?= $kunik ?> .filters-container {
        display: flex;
        align-items: center;
        gap: 15px;
        /* max-width: 500px; */
        margin: 0 auto;
        position: absolute;
        z-index: 2;
        left: 50px;
        top: 97px;
    }

    .<?= $kunik ?> .dropdown-navigate,
    .<?= $kunik ?> .text-input {
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 5px;
        font-size: 16px;
        width: 100%;
    }

    .<?= $kunik ?> .dropdown-navigate {
        width: 200px;
    }

    .text-input {
        flex: 1;
    }

    .<?= $kunik ?> .btn-filters {
        background-color: #0691b9;
        height: 45px;
    }

    .<?= $kunik ?> .btn-filters:hover {
        background-color: #014d62;
    }

    .btn-filters .fa {
        font-size: 20px;
        color: white;
    }

    @media (max-width: 600px) {
        .filters-container {
            flex-direction: column;
            gap: 10px;
        }

        .dropdown-navigate {
            width: 100%;
        }
    }

    .<?= $kunik ?> .container-navigate {
        display: flex;
        width: 100%;
        height: 80vh;
        position: relative;
    }

    .<?= $kunik ?> .main-map {
        flex: 1;
        background-color: #f0f0f0; 
        transition: width 0.5s ease; 
    }

    .<?= $kunik ?> .info-shape {
        width: 0;
        height: 100%;
        background-color: rgba(255, 255, 255, 0.9);
        box-shadow: -4px 0 8px rgba(0, 0, 0, 0.1);
        overflow: hidden;
        transition: width 0.5s ease;
        position: absolute;
        z-index: 1;
        right: 0;
    }

    .<?= $kunik ?> .info-shape.active {
        width: 40%; 
    }

    .close-btn {
        position: absolute;
        top: 10px;
        right: 10px;
        background-color: #ff4d4d;
        color: white;
        border: none;
        border-radius: 50%;
        width: 30px;
        height: 30px;
        cursor: pointer;
        font-weight: bold;
        text-align: center;
        line-height: 30px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
    }

    .<?= $kunik ?> .info-shape-content {
        padding: 15px;
        font-family: Arial, sans-serif;
        overflow-y: auto;
        max-height: calc(100% - 40px);
    }


    .info-shape-content::-webkit-scrollbar {
        width: 5px;
    }

    .info-shape-content::-webkit-scrollbar-thumb {
        background: #ccc;
        border-radius: 5px;
    }

    .info-shape-content::-webkit-scrollbar-thumb:hover {
        background: #aaa;
    }

    .info-shape-content::-webkit-scrollbar-track {
        background: #f4f4f4;
        border-radius: 5px;
    }

    .<?= $kunik ?> .info-shape-content img {
        border-radius: 50%;
        height: 60px;
        width: 60px;
        object-fit: cover;
        position: absolute;
    }

    .<?= $kunik ?> .info-shape-content .title {
        font-size: 2.2rem;
        font-weight: bold;
        margin: 5px 100px;
    }

    .<?= $kunik ?> .info-shape-content .content-address {
        color: #555;
        font-size: 1.9rem;
        margin-top: 0px;
        margin-bottom: 20px;
        margin-left: 100px;
    }

    .<?= $kunik ?> .info-shape-content .content-subtitle {
        font-size: 1.5em;
        margin-top: 20px;
    }

    .<?= $kunik ?> .info-shape-content .content-tags {
        display: inline-block;
        background: #083557;
        padding: 5px 10px;
        border-radius: 20px;
        margin: 5px 5px 0 0;
        font-size: 1.4rem;
        color: white;
        text-decoration: none;
    }
    .<?= $kunik ?> .info-shape-content .thema-list a {
        background: #f0f0f0;
        /* padding: 10px 15px; */
        border-radius: 5px;
        margin: 5px 5px 0 0;
        font-size: 1.6rem;
        color: #333;
        text-decoration: none;
        border: 3px #083557 solid;
        color: #083557;
        cursor: pointer;
        width: 150px;
        text-align: center;
    }

    .<?= $kunik ?> .info-shape-content .thema-list a.one-thema_list {
        display: inline-flex;
        justify-content: center;
        align-items: center;
        height: 50px;
    }

    .<?= $kunik ?> .info-shape-content .thema-list a.on-annuaire_list {
        display: inline-block;
        height: 70px;
    }

    .<?= $kunik ?> .info-shape-content .count-annuaire {
        height: 30px;
        width: 100%;
        background-color: #083557;
        padding-top: 5px;
        color: white;
        font-weight: bold;
    }

    .<?= $kunik ?> .info-shape-content .name-annuaire {
        padding-top: 5px;
    }

    .<?= $kunik ?> .info-shape-content .content-tags:hover {
        background: #b7bac2;
        color: #083557;
    }
    .<?= $kunik ?> .info-shape-content .thema-list a:hover{
        background: #083557;
        color: white;
    }

    .<?= $kunik ?> .info-shape-content .content-shortDescription {
        font-size: 1.5rem;
        margin-top: 10px;
        line-height: 1.4;
    }

    .<?= $kunik ?> .info-shape-content .btn-more {
        margin-top: 20px;
        padding: 10px;
        background: #007bff;
        color: #fff;
        border-radius: 5px;
        text-align: center;
        cursor: pointer;
        font-size: 0.9rem;
    }

    .<?= $kunik ?> .info-shape-content .btn-more:hover {
        background: #0056b3;
    }

    .<?= $kunik ?> .main-container-navigate {
        margin-top: 2%;
    }

    .<?= $kunik ?> .cards-grid {
        display: grid;
        grid-template-columns: repeat(4, 1fr);
        gap: 16px;
        padding: 16px;
        justify-content: center;
    }

    .<?= $kunik ?> .card {
        background: #fff;
        border-radius: 8px;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        overflow: hidden;
        transition: transform 0.2s ease;
    }

    .<?= $kunik ?> .card:hover {
        transform: translateY(-5px);
    }

    .<?= $kunik ?> .card-img-wrapper {
        background: white;
        height: 200px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .<?= $kunik ?> .card-img {
        width: 100%;
        height: 100%;
        object-fit: contain; 
    }

    .<?= $kunik ?> .card-title {
        font-size: 18px;
        font-weight: bold;
        margin: 8px 20px;
        color: #333;
    }

    .<?= $kunik ?> .card-date,
    .<?= $kunik ?> .card-address {
        font-size: 14px;
        color: #666;
        margin: 4px 20px;
    }

    @media (max-width: 1200px) {
        .<?= $kunik ?> .cards-grid {
            grid-template-columns: repeat(3, 1fr);
        }
    }

    @media (max-width: 768px) {
        .<?= $kunik ?> .cards-grid {
            grid-template-columns: repeat(2, 1fr);
        }
    }

    @media (max-width: 480px) {
        .<?= $kunik ?> .cards-grid {
            grid-template-columns: 1fr;
        }
    }

    .main-container-navigate .btn-primary {
        color: white;
        background-color: #00b5c0;
        border-color: #00b5c0;
        font-weight: 700;
        margin-top: 14px;
        margin-right: 5px;
        font-size: 1.2em;
        /* width: 160px; */
    }

    .main-container-navigate .btn-primary a {
        text-decoration: none;
        color: white;
    }

    .main-container-navigate .btn-success {
        color: white;
        background-color: #00b5c0;
        border-color: #00b5c0;
        font-weight: 700;
        margin: 6px 20px;
        font-size: 1.2em;
    }

    .section-search_annuaire {
        display: flex;
        align-items: center;
        gap: 10px;
    }

    .section-search_annuaire input {
        flex: 1;
        padding: 8px;
        border: 1px solid #ccc;
        border-radius: 5px;
        margin-top: 14px;
    }

    .section-search_annuaire button {
        margin-left: auto;
    }

    .no_city .title, .no_city .content-address {
        margin: 0px !important;
    }

    .notif-null_data {
        text-align: center;
        margin-top: 15%;
    }
    .init-cocity_note {
        position: absolute;
        top: 10px;
        right: 45px;
        background-color: #083557 !important;
        color: white;
        border: none;
        /* border-radius: 50%; */
        width: 165px;
        height: 40px;
        cursor: pointer;
        font-weight: bold;
        text-align: center;
        line-height: 30px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
    }

    .thema-active {
        background: #083557 !important;
        color: white !important;
    }

    .leaflet-popup-content-wrapper {
        min-width: 250px;
    }

    .leaflet-popup-content-wrapper .leaflet-popup-content {
        width: 100%;
    }

    .voir_cocity_annuaire a {
        color: white !important;
        text-decoration: none;
    }
    
    #search-name, #search-address {
        padding: 8px !important;
    }

    .btn-links-thema {
        font-size: 16px;
        background-color: #00b5c0;
        margin-right: 18px;
        height: 44px;
        color: white;
    }

    .btn-links-thema a {
        color: white;
        font-weight: bold;
        text-decoration: none;
    }

    .btn-voir_filiere {
        text-align: right;
    }
</style>
<div class="loader-loader<?= $kunik ?> hidden" id="loader-loader<?= $kunik ?>" role="dialog">
    <div class="content-loader<?= $kunik ?>" id="content-loader<?= $kunik ?>"></div>
</div>
<div class="backdrop-loader<?= $kunik ?> hidden" id="backdrop-loader<?= $kunik ?>">
    <div class="progress-container-world hidden">
        <div class="progress-bar-world" id="progress-bar-world"></div>
        <div class="progress-percentage-world" id="progress-percentage-world">0%</div>
    </div>
    <div class="progress-message-world hidden">Cette opération peut prendre du temps. <br> Merci de patienter pendant la préparation des pages de(s) filière(s) et du COcity.</div>
</div>
<div class="main-container-navigate <?= $kunik ?>">
    <div class="row">
        <div class="col-md-7">
            <h3 class="title-zone titres" style="font-weight: normal; margin-left:1%;">Toutes les Régions</h3>
        </div>
        <div class="col-md-5 section-search_annuaire text-right">
            <?php if(isset($el) && isset($el['slug']) && $el['slug'] == 'cocityPrez') {?>
                <input class="cards-grid hidden" type="text" id="search-name" placeholder="Que cherchez vous ?">
                <input class="cards-grid hidden" type="text" id="search-address" placeholder="Où ?">
                <button class="btn btn-primary" id="change-views" data-key="annuaire"> <i class="fa fa-list"></i> Voir l'annuaire</button>
            <?php } ?> 
            <?php if(isset($cocityParent)) {?>
                <button class="btn btn-primary"><a href="<?= $baseUrl?>/costum/co/index/slug/<?= $cocityParent['slug']?>" target="_blank">Voir la COcity région</a></button>
            <?php }?>
        </div>
    </div>
    <div class="carte-navigate">
        <div class="filters-container">
            <select class="dropdown-navigate" id="dropdown-navigate">
                <option value="" selected disabled>Niveau de territoire</option>
            </select>
            <input type="text" class="text-input" placeholder="Saisir un territoire" id="textInput">
            <div class="check-filiere-page">
                
            </div>
            <!-- <button class="btn btn-filters" id="btn-filtre" title="activer la recherche"><i class="fa fa-search"></i></button> -->
        </div>
        <div class="container-navigate">
            <div class="map-container main-map" id="main-map">
            </div>
            <div class="info-shape" id="infoShape">
            </div>
        </div>
        <div id="tooltip" class="tooltip"></div>
    </div>
    <div class="cards-grid hidden" id="container-annuaire">

    </div>
</div>
<script>
    let elets = <?= json_encode($el) ?>;
    let zones = <?= json_encode($zones) ?>;
    let zonesgeo = <?= json_encode($zonesgeo) ?>;
    let sousCocity = <?= json_encode($sousCocity) ?>;
    let allInitZones = <?= json_encode($zones) ?>;
    let allInitZonesgeo = <?= json_encode($zonesgeo) ?>;
    let allInitSousCocity = <?= json_encode($sousCocity) ?>;
    let lesthematique = <?= json_encode($allSlugThematic) ?>;
    let allCocity = <?= json_encode($allCocitys) ?>;
    
    let titileNavigate = [];
    let _typeCreate = "";
    var titlnavInit = "";

    $("#change-views").on("click", function(){
        var key = $(this).attr('data-key');
        if(key == 'annuaire') {
            $(".title-zone").html('Toutes les COcity');
            $(this).attr('data-key', 'carte');
            $(this).html(`<i class="fa fa-map-marker"></i> &nbsp;&nbsp; Carte`)
            $(".carte-navigate").addClass('hidden');
            $(".cards-grid").removeClass('hidden');
        } else {
            $(".title-zone").html(titlnavInit);
            infoShape.classList.remove('active');
            mainMap.style.flex = '1';
            worldNavigateMap.clearMap();
            worldNavigateMap.addElts(allInitZones);
            worldNavigateMap.fitBounds();
            $(this).attr('data-key', 'annuaire');
            $(this).html(`<i class="fa fa-list"></i> &nbsp;&nbsp; Voir l'annuaire`)
            $(".cards-grid").addClass('hidden');
            $(".carte-navigate").removeClass('hidden');
        }  
    });

    if(typeof elets.slug != "undefined" && elets.slug == "cocityPrez") {
        function setAnnuaireData() {
            $(".check-filiere-page").empty();
            var container = document.getElementById("container-annuaire");

            $.each(allCocity, function(key, item){
                if(item.slug != "templateCocity") {
                    var card = document.createElement("div");
                    card.className = "card";

                    var img = new Image();
                    img.src = item.profilImageUrl;
                    var urlImgProfil = "";
                    img.onload = function() {
                        setCardContent(item.profilImageUrl);
                    };

                    img.onerror = function() { 
                        setCardContent(`${modules.costum.url}/images/thumbnail-default.jpg`); 
                    };
                    
                    let addressStr = "";
                    if (item.address) {
                        if (item.address.level5Name)
                            addressStr += ((addressStr != "") ? ", " : "") + item.address.level5Name;
                        if (item.address.level4Name)
                            addressStr += ((addressStr != "") ? ", " : "") + item.address.level4Name;
                        if (item.address.level3Name)
                            addressStr += ((addressStr != "") ? ", " : "") + item.address.level3Name;
                        if (item.address.level1Name)
                            addressStr += ((addressStr != "") ? ", " : "") + item.address.level1Name;;
                    }

                    function setCardContent(imageUrl) {
                        card.innerHTML = `
                            <div class="card-img-wrapper">
                                <img src="${imageUrl}" alt="${item.name}" class="card-img" />
                            </div>
                            <h3 class="card-title">${item.name}</h3>
                            <p class="card-address"><i class='fa fa-map-marker'></i> ${addressStr}</p>
                            <button id="${item._id['$id']}" data-key="${item._id['$id']}" class="btn btn-success btn-fiche_cocity">${trad.knowmore}</button>
                            <button class="btn btn-success voir_cocity_annuaire"><a href="${baseUrl + '/costum/co/index/slug/' + item.slug}" target='_blank'>Voir la COcity<a/></button>
                        `;
                        container.appendChild(card);

                        $(`#${item._id['$id']}`).on('click', function(){
                            var _id = $(this).attr('data-key');
                            urlCtrl.openPreview("view/url/costum.views.custom.cocity.previewannuaire", {"_id" : _id});
                        });
                    }
                }
            });

            var searchName = document.getElementById("search-name");
            var searchAddress = document.getElementById("search-address");
            var container = document.getElementById("container-annuaire");

            function filterCards() {
                var nameQuery = searchName.value.toLowerCase();
                var addressQuery = searchAddress.value.toLowerCase();
                
                document.querySelectorAll(".card").forEach(card => {
                    var title = card.querySelector(".card-title").textContent.toLowerCase();
                    var address = card.querySelector(".card-address").textContent.toLowerCase();
                    
                    if (title.includes(nameQuery) && address.includes(addressQuery)) {
                        card.style.display = "block";
                    } else {
                        card.style.display = "none";
                    }
                });
            }

            searchName.addEventListener("input", filterCards);
            searchAddress.addEventListener("input", filterCards);
        }
    }

    function tronquerChaine(chaine, longueurMax) {
        if (chaine.length > longueurMax) {
            return chaine.substring(0, longueurMax - 3) + '...';
        } else {
            return chaine;
        }
    }
    var slugTiersLieux = {
        "Nouvelle-Aquitaine": "laCooperativeTierslieux",
        "Hauts-de-France": "laCompagnieDesTierslieux",
        "Bourgogne-Franche-Comté": "tierslieuxBfc",
        "Auvergne-Rhône-Alpes": "relief",
        "Guyane": "reseauDesTierslieuxDeGuyane",
        "Provence-Alpes-Côte d'Azur": "sudTierslieux",
        "Occitanie": "laRosee",
        "Mayotte": "reseauDesTierslieuxDeMayotte",
        "Corse": "daLocu",
        "Martinique": "reseauRegionalDesTierslieuxMartinique",
        "Guadeloupe": "reseauRegionalDesTierslieuxGuadeloupe",
        "Île-de-France": "consortiumIledefranceTierslieux",
        "Centre-Val de Loire": "ambitionTiersLieuxValDeLoire",
        "Normandie": "reseauNormandDesTierslieux",
        "Bretagne": "bretagneTierslieux",
        "Grand Est": "reseauDesTierslieuxDuGrandEst",
        "Pays de la Loire": "capTierslieux",
        "Réunion": "LaReunionDesTiersLieux"
    };

    var mainMap = document.getElementById('main-map');
    var infoShape = document.getElementById('infoShape');
    var closeInfoShape = "";
    var datasCheck = [];
    function showInfoShape(params, exist) {
        $(".check-filiere-page").empty();
        infoShape.classList.add('active');
        mainMap.style.flex = '0.6';
        let content = `<button class="close-btn" id="closeInfoShape">X</button>`;
        if(exist == "trueCocity") {
            let data = sousCocity[params];
            datasCheck = data;
            let id = data._id ? data._id.$id : data.id;
            let imgProfil = (data.profilThumbImageUrl && data.profilThumbImageUrl !== "")
                ? baseUrl + data.profilThumbImageUrl
                : modules.map.assets + "/images/thumb/default_" + data.collection + ".png";
            let eltName = data.title ? data.title : data.name;
            eltName = tronquerChaine(eltName, 30);
            content += `<div class="info-shape-content">
                <img src="${imgProfil}" alt="Profil image">
                <div class="title">${eltName}</div>`;

            if (data.address) {
                let addressStr = [data.address.level5Name, data.address.level4Name, data.address.level3Name, data.address.level1Name]
                    .filter(Boolean)
                    .join(", ");
                content += `<div class="content-address"><i class="fa fa-map-marker"></i> ${addressStr}</div>`;
            }

            if (data.tags && data.tags.length > 0) {
                content += `<div class="div-tags-content">`;
                data.tags.forEach(tag => {
                    content += `<div class="content-tags">#${tag}</div>`;
                });
                content += `</div>`;
            }

            if (data.thematic && data.thematic.length > 0) {
                content += `<div class="content-thema">
                    <div class="content-subtitle">
                        Thématiques actives
                    </div>
                    <div class="thema-list">`;
                data.thematic.forEach(value => {
                    if (allThem[value]) {
                        var slug = generateThemaLink(data, value);
                        content += `<a class="one-thema_list" onclick="getElementsThema(this, '${value}', '${slug}')">
                            <i class="fa ${allThem[value].icon}" title="${value}"></i>&nbsp; &nbsp; ${value}
                        </a>`;
                    }
                });
                content += `</div></div>`;
            }

            if (data.shortDescription && data.shortDescription !== "") {
                content += `<div class="content-section">
                    <div class="content-subtitle">Description</div>
                    <div class="content-shortDescription">${data.shortDescription}</div>
                </div>`;
            }   

            document.getElementById('infoShape').innerHTML = content;

            closeInfoShape = document.getElementById('closeInfoShape');
            closeInfoShape.addEventListener('click', hideInfoShape);
        } else {
            content += `<div class="info-shape-content no_city">
                <div class="title">${params.name}</div>`;
            let addressStr = [params.level5Name, params.level4Name, params.level3Name, params.name, params.level1Name]
                .filter(Boolean)
                .join(", ");
            content += `<div class="content-address"><i class="fa fa-map-marker"></i> ${addressStr}</div>`;
            let addressFiltre = [];
            
            if (typeof params.level !== "undefined" && params.level.length > 0) {
                if (params.level.includes('3')) {
                    addressFiltre = { "address.level3": params._id['$id'] };
                } 
                if (params.level.includes('4')) {
                    addressFiltre = { "address.level4": params._id['$id'] };
                } 
                if (params.level.includes('5')) {
                    // addressFiltre = { "address.level5": params._id['$id'] };
                    addressFiltre = {
                        "address.localityId" : {
                            '$in' : params.cities
                        }
                    }
                }
            }

            if(typeof params.country != "undefined") addressFiltre = {"address.localityId" : params._id['$id']}

            let paramsFilters = {
                searchType : ['events', 'organizations', 'poi', 'projects', 'classifieds', 'citoyens', 'ressources'],
                notSourceKey : true,
                filters : {
                    ...addressFiltre
                },
                countType : ['events', 'organizations', 'poi', 'projects', 'classifieds', 'citoyens', 'ressources'],
                indexStep : 0,
                // onlyCount : true,
                count: true
            };

            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                paramsFilters,
                function(results) {
                    if(Object.keys(results.results).length > 0) {
                        content += `<div class="content-thema">
                                        <div class="content-subtitle">Les éléments existants</div>
                                        <div class="thema-list">`;
                        $.each(results.count, function(key, value){
                            if(value > 0) content += `<a class="on-annuaire_list annuaire_list" data-col="${key}">
                                <div class="count-annuaire">
                                    ${value}
                                </div>
                                <div class="name-annuaire">
                                    ${trad[key]}
                                </div>
                            </a>`
                        });
                        content += `</div></div>`

                        content += `<button class="btn btn-primary init-cocity_note" onclick="initCocity('${params._id["$id"]}','${(params.name).replaceAll("'", "\\'")}')">Créer votre COcity</button>`;

                        // content += `<div class="content-thema">
                        //                 <div class="content-subtitle">Thématiques</div>
                        //                 <div class="thema-list">`;
                        // var tagCounts = {};
                        // $.each(results.results, function(key, value){
                        //     if (value.tags) {
                        //         let tagsArray = [];

                        //         if (Array.isArray(value.tags)) {
                        //             tagsArray = value.tags;
                        //         } 

                        //         else if (typeof value.tags === "string") {
                        //             tagsArray = value.tags.split(",").map(tag => tag.trim());
                        //         } 

                        //         else if (typeof value.tags === "object") {
                        //             tagsArray = Object.values(value.tags);
                        //         }

                        //         tagsArray.forEach(tag => {
                        //             tagCounts[tag] = (tagCounts[tag] || 0) + 1;
                        //         });
                        //     }
                        // });

                        // let topTags = Object.entries(tagCounts)
                        //     .sort((a, b) => b[1] - a[1])
                        //     .slice(0, 10)
                        //     .map(entry => entry[0]);

                        // $.each(topTags, function(key, value){
                        //     content += `<a class="one-thema_list toptags_list" data-tags="${value}">${value}</a>`
                        // });
                        // content += `</div></div>`

                        // content += `</div>`;
                    
                        document.getElementById('infoShape').innerHTML = content;

                        $(".annuaire_list").on('click', function(){
                            document.querySelectorAll('.on-annuaire_list').forEach(el => el.classList.remove('thema-active'));
                            $(this).addClass('thema-active');

                            var coll = $(this).attr('data-col');
                            var dataColl = {};
                            $.each(results.results, function(key, value){
                                if (value.collection === coll) {
                                    dataColl[key] = value;
                                }
                            });
                            worldNavigateMap.getOptions().arrayBounds = [];
                            worldNavigateMap.getOptions().bounds = null;
                            worldNavigateMap.getMap().closePopup();
                            worldNavigateMap.getOptions().markersCluster.clearLayers();
                            if(Object.keys(worldNavigateMap.getOptions().markerList).length > 0){
                                $.each(worldNavigateMap.getOptions().markerList, function(){
                                    worldNavigateMap.getMap().removeLayer(this)
                                })
                            }
                            worldNavigateMap.getOptions().markerList = {};
                            worldNavigateMap.addElts(dataColl);
                            fitMarkerBounds();
                        });
                    } else {
                        content += `<div class="content-thema">
                            <p class="notif-null_data">Aucun élément n'est disponible pour la zone sélectionnée.</p>
                        </div>`;

                        document.getElementById('infoShape').innerHTML = content;
                    }

                    closeInfoShape = document.getElementById('closeInfoShape');
                    closeInfoShape.addEventListener('click', hideInfoShape);
                }
            )
        }
    }

    // async function getCountDataShape(params) {
    //     let addressFiltre = [];
            
    //     if (typeof params.level !== "undefined" && params.level.length > 0) {
            
    //         if (params.level.includes('3')) {
    //             addressFiltre = { "address.level3": params._id['$id'] };
    //         } 
    //         if (params.level.includes('4')) {
    //             addressFiltre = { "address.level4": params._id['$id'] };
    //         } 
    //         if (params.level.includes('5')) {
    //             addressFiltre = { "address.level5": params._id['$id'] };
    //         }
    //     }

    //     if(typeof params.country != "undefined") addressFiltre = {"address.localityId" : params._id['$id']}

    //     let paramsFilters = {
    //         searchType : ['events', 'organizations', 'poi', 'projects', 'classifieds', 'citoyens', 'ressources'],
    //         notSourceKey : true,
    //         filters : {
    //             ...addressFiltre
    //         },
    //         countType : ['events', 'organizations', 'poi', 'projects', 'classifieds', 'citoyens', 'ressources'],
    //         indexStep : 0,
    //         onlyCount : true,
    //         count: true
    //     };

    //     ajaxPost(
    //             null,
    //             baseUrl + "/" + moduleId + "/search/globalautocomplete",
    //             paramsFilters,
    //             function(result) {
    //                 return "red";
    //             })
    // }

    function generateThemaLink(data, value) {
        if (lesthematique[data.slug]?.[value]) {
            return (value === "tiers lieux" && data.costum.typeCocity === "region")
                ? slugTiersLieux[data.address.level3Name]
                : lesthematique[data.slug][value];
        } else {
            return (value === "tiers lieux" && data.costum.typeCocity === "region")
                ? slugTiersLieux[data.address.level3Name]
                : data.slug + value.normalize('NFD').replace(/[\u0300-\u036f]/g, '').charAt(0).toUpperCase() + value.slice(1);
        }
    }
    
    function hideInfoShape() {
        infoShape.classList.remove('active');
        mainMap.style.flex = '1';
    }

    function getElementsThema(element, thema, slug) {
        $('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
        $('#loader-loader<?= $kunik ?>').removeClass('hidden');
        coInterface.showLoader('#content-loader<?= $kunik ?>');
        var htmlBtn = `    <button class="btn btn-links-thema"><a href="${baseUrl + '/costum/co/index/slug/' +slug}" target="_blank" >Voir la page filière ${thema}</a></button>`;
        $(".check-filiere-page").empty();
        $(".check-filiere-page").html(htmlBtn);
        // Retirer la classe "thema-active" de tous les éléments
        document.querySelectorAll('.one-thema_list').forEach(el => el.classList.remove('thema-active'));

        // Ajouter la classe "thema-active" à l'élément cliqué
        element.classList.add('thema-active');

        var param = {
            'slug' : slug
        }

        ajaxPost(
            null,
            baseUrl+"/costum/cocity/getorgafiliere",
            param,
            function(data){
                var orgafiliere = typeof data.orgaFiliere != "undefined" ? data.orgaFiliere : "";
                var tagss = thema == "tiers lieux" ? "TiersLieux" : thema;
                var params = {
                    searchType: ['organizations','citoyens'],
                    countType: ['organizations','citoyens'],
                    notSourceKey: true,
                    filters: {
                        $or: {}
                    },
                    count: true,
                    searchTags: [tagss],
                    indexStep: 0
                };
                if(orgafiliere != null && typeof orgafiliere.slug != "undefined") {
                    // var params = {
                    //     searchType: ['organizations'],
                    //     notSourceKey: true,
                        params.filters = {
                            $or: {
                                'source.keys': orgafiliere.slug ,
                                [`links.memberOf.${orgafiliere._id['$id']}`]: { $exists: true }
                            }
                        }
                    //     indexStep: 0
                    // };
                }

                    params.filters['$or'].$and = setFilitersParams(thema);

                    ajaxPost(
                        null,
                        baseUrl + "/" + moduleId + "/search/globalautocomplete",
                        params,
                        function(results) {
                            worldNavigateMap.getMap().closePopup();
                            if(Object.keys(results.results).length > 0) {
                                worldNavigateMap.getOptions().arrayBounds = [];
                                worldNavigateMap.getOptions().bounds = null;
                                worldNavigateMap.getMap().closePopup();
                                worldNavigateMap.getOptions().markersCluster.clearLayers();
                                if(Object.keys(worldNavigateMap.getOptions().markerList).length > 0){
                                    $.each(worldNavigateMap.getOptions().markerList, function(){
                                        worldNavigateMap.getMap().removeLayer(this)
                                    })
                                }
                                worldNavigateMap.getOptions().markerList = {};
                                worldNavigateMap.addElts(results.results);
                                fitMarkerBounds();
                            } else {
                                toastr.info("Aucune élément");
                            }
                            $('#backdrop-loader<?= $kunik ?>').addClass('hidden');
                            $('#loader-loader<?= $kunik ?>').addClass('hidden');
                        }
                    )
                // } else {
                //     toastr.info("La page filière du thème choisi n'est pas encore disponible. Veuillez vous rendre dans la COcity du territoire si vous souhaitez activer cette page.")
                // }
            }
        );
    }

    function setFilitersParams(thema) {
        var dataAnd = [];
        var idAddress = {};

        if(typeof datasCheck.costum['typeCocity'] != "undefined" && typeof datasCheck.address != "undefined") {
            switch (datasCheck.costum['typeCocity']) {
                case "region":
                    idAddress = { "address.level3": datasCheck.address.level3 };
                    break;
                case "ville":
                    idAddress = { "address.localityId": datasCheck.address.localityId };
                    break;
                case "departement":
                case "district":
                case "epci":
                    idAddress = { "address.level4": datasCheck.address.level4 };
                    break;
            }
        } else {
            if (datasCheck.address.localityId) {
                idAddress = { "address.localityId": datasCheck.address.localityId };
            } else if (datasCheck.address.level3) {
                idAddress = { "address.level3": datasCheck.address.level3 };
            }
        }

        var dataAnd = [];

        if (thema === "tiers lieux" || thema === "pacte") {
            var sourceKey = thema === "tiers lieux" ? "franceTierslieux" : "siteDuPactePourLaTransition";
            var tags = thema === "tiers lieux" ? "TiersLieux" : thema.toLowerCase();

            var source = 
            {
                $or: [
                    // {
                    //     $or : [
                    //         { "source.keys": sourceKey },
                    //         { "reference.costum": sourceKey },
                    //     ],
                    //     "tags": tags 
                    // },
                    {
                        $or : [
                            { "source.keys": sourceKey },
                            { "reference.costum": sourceKey }
                        ]
                    }
                ]
            };
            dataAnd.push(source);
        } 
        // else {
        //     dataAnd.push({ tags: thema.toLowerCase() });
        // }

        if (Object.keys(idAddress).length > 0) {
            dataAnd.unshift(idAddress);
        }

        return dataAnd;
    }

    var worldNavigateMap = new CoMap({
        container: "#main-map",
        activePopUp: true,
        activeCluster : true,
        mapOpt: {
            btnHide: false,
            doubleClick: true,
            scrollWheelZoom: true,
            zoom: 3,
        },
        mapCustom: {
            tile: "maptiler",
            markers: {
                // getMarker : function(data){
                //     var imgM = "< ?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/cocity/smarterritoire.png";	
                //     return imgM;
                // }
            },
            getPopup: function(donne) {
                let popup = "";
                if(donne.geo && donne.collection) {
                    var eltName = tronquerChaine(donne.name, 30);
                    var imgProfil = modules.map.assets + "/images/thumb/default.png";
                    if (typeof donne.profilThumbImageUrl !== "undefined" && donne.profilThumbImageUrl != "")
                        imgProfil = baseUrl + donne.profilThumbImageUrl;
                    else
                        imgProfil = modules.map.assets + "/images/thumb/default_" + donne.collection + ".png";

                    popup += "<div class='padding-5' id='popup" + id + "'>";
                    popup += "<div class='leaflet-popup-tip-container'><div class='leaflet-popup-tip'></div></div>";
                    popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                    popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

                    if (donne.address) {
                        let addressStr = "";
                        if (donne.address.level5Name)
                            addressStr += ((addressStr != "") ? ", " : "") + donne.address.level5Name;
                        if (donne.address.level4Name)
                            addressStr += ((addressStr != "") ? ", " : "") + donne.address.level4Name;
                        if (donne.address.level3Name)
                            addressStr += ((addressStr != "") ? ", " : "") + donne.address.level3Name;
                        if (donne.address.level1Name)
                            addressStr += ((addressStr != "") ? ", " : "") + donne.address.level1Name;
                        popup += "<div class='popup-address text-dark' style='font-size: 10pt; margin-left: 13%; margin-top: -2%;'>";
                        popup += "<i class='fa fa-map-marker'></i> " + addressStr;
                        popup += "</div>";
                    }
                    if (donne.tags && donne.tags.length > 0) {
                        popup += "<div class='div-tags-popup'>";
                        let totalTags = 0;
                        $.each(donne.tags, function(index, value) {
                            totalTags++;
                            popup += "<div class='popup-tags'>#" + value + " </div>";
                        })
                        popup += "</div>";
                    }
                    if (donne.shortDescription && donne.shortDescription != "") {
                        popup += "<div class='popup-section'>";
                        popup += "<div class='popup-subtitle'>Description</div>";
                        popup += "<div class='popup-shortDescription'>" + donne.shortDescription + "</div>";
                        popup += "</div>";
                    }
                    popup += "</div>";
                } else {
                    var donness = "";
                    if(donne.geo && typeof donne.properties == "undefined") {
                        donness = donne;
                    } else if (typeof donne.properties != "undefined"){
                        donness = donne.properties;
                    }

                    let _id = donness._id['$id'];
                    let cocityId = donness.cocityId;
                    let onLevel = donness.level;
                    let levelNavigate = 0;
                    let htmlNavigate = `<div class="navigate-section">`;
                    let zone = "";
                    let idlast = "";
                    let idafter = "";
                    if (typeof donness.country != "undefined") {
                        zone = "La Ville";
                        levelNavigate = donness.country == 'MG' ? 5 : 6;
                        idlast = donness.country == 'MG' ? donness.level3 : donness.level4;
                        htmlNavigate += `<i class="fa fa-minus-circle navigate_carte"  onclick="navigetShapeMap('${_id}','last', ${levelNavigate},'${donness.country}', '${idlast}', '${idafter}')"></i>`;
                        infoTitle = {
                            "lastlabel": donness.country == 'FR' ? "Les EPCI du département de " + donness.level4Name : "Les Districts de la région " + donness.level3Name,
                        }
                    } else {
                        if (onLevel[onLevel.length - 1] == "5") {
                            zone = "L'EPCI ";
                            levelNavigate = 5;
                            idlast = donness.level3;
                            idafter = donness._id['$id'];
                            htmlNavigate += `<i class="fa fa-minus-circle navigate_carte"  onclick="navigetShapeMap('${_id}','last', ${levelNavigate},'${donness.countryCode}', '${idlast}', '${idafter}')"></i>
                                <i class="fa fa-plus-circle navigate_carte right-position" onclick="navigetShapeMap('${_id}','after', ${levelNavigate},'${donness.countryCode}', '${idlast}', '${idafter}')"></i>`;
                            infoTitle = {
                                "lastlabel": "Les départements de la région " + donness.level3Name,
                                "afterlabel": "Les communes faisant partie de l'EPCI " + donness.name
                            }
                        }

                        if (onLevel[onLevel.length - 1] == "4") {
                            if (donness.countryCode == 'MG') {
                                zone = "Le District";
                                infoTitle = {
                                    "lastlabel": "Les Régions de " + donness.level1Name,
                                    "afterlabel": "Les communes faisant partie du District d'(e) " + donness.name
                                }
                            } else {
                                zone = "Le Département";
                                infoTitle = {
                                    "lastlabel": "Les Régions de " + donness.level1Name,
                                    "afterlabel": "Les EPCI du département de " + donness.name
                                }
                            }
                            levelNavigate = 4;
                            idafter = donness._id['$id'];
                            if (typeof onLevel[onLevel.length - 2] != "undefined") {
                                htmlNavigate += `<i class="fa fa-plus-circle navigate_carte right-position" onclick="navigetShapeMap('${_id}','after', ${levelNavigate},'${donness.countryCode}', '${idlast}', '${idafter}')"></i>`;
                            } else {
                                htmlNavigate += `<i class="fa fa-minus-circle navigate_carte"  onclick="navigetShapeMap('${_id}','last', ${levelNavigate},'${donness.countryCode}', '${idlast}', '${idafter}')"></i>
                                    <i class="fa fa-plus-circle navigate_carte right-position" onclick="navigetShapeMap('${_id}','after', ${levelNavigate},'${donness.countryCode}', '${idlast}', '${idafter}')"></i>`;
                            }
                        }
                        if (onLevel[onLevel.length - 1] == "3") {
                            zone = "Le Région";
                            levelNavigate = 3;
                            idafter = donness._id['$id'];
                            htmlNavigate += `<i class="fa fa-plus-circle navigate_carte position-right" onclick="navigetShapeMap('${_id}','after', ${levelNavigate},'${donness.countryCode}', '${idlast}', '${idafter}')"></i>`;
                            infoTitle = {
                                "afterlabel": donness.countryCode == 'FR' ? "Les départements de la région " + donness.name : "Les Districts de la région " + donness.name
                            }
                        }
                    }

                    htmlNavigate += `</div>`;

                    titileNavigate[_id] = infoTitle;

                    if (typeof sousCocity[cocityId] != 'undefined') {
                        let data = sousCocity[cocityId];
                        let id = data._id ? data._id.$id : data.id;
                        let imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                        if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
                            imgProfil = baseUrl + data.profilThumbImageUrl;
                        else
                            imgProfil = modules.map.assets + "/images/thumb/default_" + data.collection + ".png";

                        let eltName = data.title ? data.title : data.name;
                        eltName = tronquerChaine(eltName, 30);

                        popup += "<div class='padding-5' id='popup" + id + "'>";
                        popup += "<div class='leaflet-popup-tip-container'><div class='leaflet-popup-tip'></div></div>";
                        popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                        popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

                        if (data.address) {
                            let addressStr = "";
                            if (data.address.level5Name)
                                addressStr += ((addressStr != "") ? ", " : "") + data.address.level5Name;
                            if (data.address.level4Name)
                                addressStr += ((addressStr != "") ? ", " : "") + data.address.level4Name;
                            if (data.address.level3Name)
                                addressStr += ((addressStr != "") ? ", " : "") + data.address.level3Name;
                            if (data.address.level1Name)
                                addressStr += ((addressStr != "") ? ", " : "") + data.address.level1Name;
                            popup += "<div class='popup-address text-dark' style='font-size: 10pt; margin-left: 13%; margin-top: -2%;'>";
                            popup += "<i class='fa fa-map-marker'></i> " + addressStr;
                            popup += "</div>";
                        }
                        if (data.shortDescription && data.shortDescription != "") {
                            popup += "<div class='popup-section'>";
                            popup += "<div class='popup-subtitle'>Description</div>";
                            popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
                            popup += "</div>";
                        }

                        let url = baseUrl + '/costum/co/index/slug/' + data.slug;
                        popup += "<div class='popup-section'>";
                        popup += "<a href='" + url + "' target='_blank' class=' item_map_list popup-marker' id='popup" + id + "'>";
                        popup += '<div class="btn btn-sm btn-more col-md-12">';
                        popup += '<i class="fa fa-hand-pointer-o"></i>&nbsp;&nbsp; Voir la COcity';
                        popup += '</div></a>';
                        if (levelNavigate != 0) popup += htmlNavigate;
                        popup += '</div>';
                        popup += '</div>';
                    } else {
                        popup += "<div class='padding-5' id='popup-new-cocity'>";
                        popup += "<div class='leaflet-popup-tip-container'><div class='leaflet-popup-tip'></div></div>";
                        popup += "<div class='popup-section'>";
                        popup += "<p>" + zone + " <b>" + donness.name + "</b> n'a pas de COCITY.</p>";
                        popup += `<a target='_blank' class='item_map_list popup-marker' onclick="initCocity('${donness._id["$id"]}','${(donness.name).replaceAll("'", "\\'")}')" id='btn-new-cocity${donness._id["$id"]}'>`;
                        popup += '<div class="btn btn-sm btn-more col-md-12">';
                        popup += 'Créer votre cocity';
                        popup += '</div></a>';
                        if (levelNavigate != 0) popup += htmlNavigate;
                        popup += '</div>';
                        popup += '</div>';
                    }
                }
                return popup;
            },
            addElts: function(_context, data) {
                let geoJson = [];
                let elements = document.querySelectorAll('#main-map .leaflet-interactive');
                let tooltip = document.getElementById('tooltip');
                $.each(data, function(k, v) {
                    if (!_context.getOptions().data[k])
                        _context.getOptions().data[k] = v;
                    if (typeof v.geoShape != "undefined") {
                        let data = $.extend({}, v);
                        let geoData = {
                            type: "Feature",
                            properties: {
                                name: v.name
                            },
                            geometry: v.geoShape
                        };
                        delete data.name;
                        delete data.getShape;
                        $.each(data, function(key, value) {
                            if (typeof geoData.properties[key] == 'undefined') {
                                geoData.properties[key] = value;
                            }
                        })
                        geoJson.push(geoData);
                    }
                    if(typeof v.geo != "undefined" && typeof v.geoShape == "undefined") {
                        v.id = k;
                        _context.addMarker({ elt: v })
                        _context.getOptions().mapCustom.lists.createItemList(k, v, _context.getOptions().container)
                    }
                })
                if( _context.getOptions().activeCluster)
                    _context.getMap().addLayer(_context.getOptions().markersCluster)

                _context.getOptions().geoJSONlayer = L.geoJSON(geoJson, {
                    onEachFeature: function(feature, layer) {
                        _context.addPopUp(layer, feature);
                        // var colour = getCountDataShape(feature.properties);
                        layer.setStyle({
                            color: typeof feature.properties.cocityId != 'undefined' ? "#1a72d0" : '#777777f0',
                            // color: colour,
                            opacity: 0.5
                        });
                        var onLevel = feature.properties.level;
                        var levelNavigate = 0;
                        var idlast = "";
                        var idafter = "";
                        var contryCd = "";
                        if (typeof feature.properties.country != "undefined") {
                            levelNavigate = feature.properties.country == 'MG' ? 5 : 6;
                            idlast = feature.properties.country == 'MG' ? feature.properties.level3 : feature.properties.level4;
                            contryCd = feature.properties.country;
                        } else {
                            idafter = feature.properties._id['$id'];
                            levelNavigate = parseInt(onLevel[onLevel.length - 1]);
                            contryCd = feature.properties.countryCode;
                            if (onLevel[onLevel.length - 1] == "5") {
                                idlast = feature.properties.level3;
                            }
                            if (onLevel[onLevel.length - 1] == "4") {
                                
                            }
                            if (onLevel[onLevel.length - 1] == "3") {
                                
                            }
                        }
                        layer.on("dblclick", function(event){
                            if(levelNavigate != "6") navigetShapeMap(feature.properties._id['$id'],'after', levelNavigate, contryCd, idlast, idafter);
                        });
                        layer.addEventListener("contextmenu", (event) => {
                            navigetShapeMap(feature.properties._id['$id'],'last', levelNavigate, contryCd, idlast, idafter);
                        });
                        layer.on("click", function(event){
                            worldNavigateMap.getOptions().markersCluster.clearLayers();
                            _context.getMap().fitBounds(layer.getBounds());
                            if(typeof feature.properties.cocityId != 'undefined' && feature.properties.cocityId != "" && typeof sousCocity[feature.properties.cocityId] != "undefined") {
                                showInfoShape(feature.properties.cocityId, "trueCocity");
                            } else {
                                showInfoShape(feature.properties, "falseCocity");
                            }
                        });
                        layer.on("mouseover", function(event) {
                            this.setStyle({
                                "fillOpacity": 1,
                                "strokeOpacity": 1
                            });
                            let name = this.feature.properties.name;
                            tooltip.innerHTML = name;
                            tooltip.style.display = 'block';
                            let x = event.containerPoint.x;
                            let y = event.containerPoint.y;
                            tooltip.style.left = `${x}px`;
                            tooltip.style.top = `${y}px`;
                        });

                        layer.on("mouseout", function() {
                            this.setStyle({
                                "fillOpacity": 0.2,
                                "strokeOpacity": 0.2,
                            });
                            tooltip.style.display = 'none';
                        });
                    }
                });
                _context.getMap().addLayer(_context.getOptions().geoJSONlayer);
                _context.hideLoader();
                // _context.fitBounds(data);
            }
        }
    });
    function fitMarkerBounds(){
        worldNavigateMap.getMap().fitBounds(worldNavigateMap.getOptions().arrayBounds)
        if(worldNavigateMap.getMap().getZoom() == 0){
            worldNavigateMap.getMap().setZoom(3)
        }
    }
    function navigetShapeMap(_id, section, level, country, id_last = "", id_after = "") {
        $(".check-filiere-page").empty();
        infoShape.classList.remove('active');
        mainMap.style.flex = '1';
        if (level == 4 && section == "last") {
            zones = allInitZones
            zonesgeo = allInitZonesgeo
            sousCocity = allInitSousCocity
            worldNavigateMap.clearMap();
            worldNavigateMap.addElts(zones);
            worldNavigateMap.fitBounds();
            $(".title-zone").html(titlnavInit);
            _typeCreate = "country";
        } else {
            let getparams = {};
            let titleNav = "";

            let levelInt = parseInt(level);

            if (section == 'last') {
                getparams = {
                    level: levelInt - 1,
                    countryCode: country,
                    collection: "zones",
                    getLevel: levelInt - 2,
                    idgetlevel: id_last
                };
                titleNav = titileNavigate[_id].lastlabel;

                switch (levelInt) {
                    case 4:
                        _typeCreate = "country";
                        break;
                    case 5:
                        _typeCreate = "region";
                        break;
                    case 6:
                        _typeCreate = "departement";
                        break;
                    default:
                        _typeCreate = undefined;
                }
            } else if (section == 'after') {
                if (level == "5" || (level == "4" && country == "MG")) {
                    let type = level == "5" ? 'EPCI' : 'District';
                    getparams = {
                        country: country,
                        collection: "cities",
                        idgetlevel: id_after,
                        typeZone: type
                    };
                } else {
                    getparams = {
                        level: levelInt + 1,
                        countryCode: country,
                        collection: "zones",
                        getLevel: levelInt,
                        idgetlevel: id_after
                    };
                }
                titleNav = titileNavigate[_id].afterlabel;

                switch (levelInt) {
                    case 3:
                        _typeCreate = "region";
                        break;
                    case 4:
                        _typeCreate = country === "FR" ? "departement" : "district";
                        break;
                    case 5:
                        _typeCreate = "epci";
                        break;
                    default:
                        _typeCreate = undefined;
                }
            }
            ajaxPost(
                null,
                baseUrl + "/costum/cocity/navigatshapemap",
                getparams,
                function(results) {
                    if (Object.keys(results.zones).length > 0) {
                        zones = results.zones;
                        zonesgeo = results.zones;
                        sousCocity = results.sousCocity;
                        $(".title-zone").html(titleNav);
                    } else {
                        zones = allInitZones;
                        zonesgeo = allInitZonesgeo;
                        sousCocity = allInitSousCocity;
                        $(".title-zone").html(titlnavInit);
                    }
                    worldNavigateMap.clearMap();
                    worldNavigateMap.addElts(zones);
                    worldNavigateMap.fitBounds();
                }
            );

        }
    };

    function initCocity(id_zone, name_zone) {
        let response = zonesgeo[id_zone];
        let initData = {};
        let globalAddress = {};
        let typeZone = "";
        let label = "";
        let collectionUpdateZone = "zones";
        if (_typeCreate == 'departement') {
            initData.address = {
                "localityId": {
                    $in: response.cities
                }
            };
            globalAddress = {
                "level5": id_zone,
                "level5Name": name_zone,
                "level4": response.level4,
                "level4Name": (response.level4Name).toUpperCase(),
                "level3": response.level3,
                "level3Name": response.level3Name,
                "level1": response.level1,
                "level1Name": response.level1Name,
                "addressCountry": response.countryCode
            }
            typeZone = "epci";
            label = "EPCI";
        } else if (_typeCreate == 'region') {
            initData.address = {
                "level4": id_zone,
                "level4Name": name_zone.toUpperCase()
            };
            globalAddress = {
                "level4": id_zone,
                "level4Name": name_zone.toUpperCase(),
                "level3": response.level3,
                "level3Name": response.level3Name,
                "level1": response.level1,
                "level1Name": response.level1Name,
                "addressCountry": response.countryCode
            }
            typeZone = response.countryCode == "MG" ? "district" : "departement";
            label = response.countryCode == "MG" ? "District" : "Département";
        } else if (_typeCreate == 'epci' || _typeCreate == 'district') {
            initData.address = {
                "localityId": id_zone,
                "level1Name": response.level1Name
            };
            globalAddress = {
                "localityId": id_zone,
                "addressLocality": name_zone,
                "level4": response.level4,
                "level4Name": (response.level4Name).toUpperCase(),
                "level3": response.level3,
                "level3Name": response.level3Name,
                "level1": response.level1,
                "level1Name": response.level1Name,
                "addressCountry": response.country
            }
            typeZone = "ville";
            label = "Ville";
            collectionUpdateZone = "cities";
        } else if (_typeCreate == "country") {
            initData.address = {
                "level3": id_zone,
                "level3Name": name_zone
            };
            globalAddress = {
                "level3": id_zone,
                "level3Name": name_zone,
                "level1": response.level1,
                "level1Name": response.level1Name,
                "addressCountry": response.countryCode
            }
            typeZone = "region";
            label = "Région";
        }

        let paramsCheck = {
            "thematic": allSugThematic,
            "address": initData.address,
            "typeCity": typeZone
        }
        ajaxPost(
            null,
            baseUrl + "/costum/cocity/checkdatafiliere",
            paramsCheck,
            function(results) {
                let dyfOrga = {
                    "beforeBuild": {
                        "properties": {
                            "name": {
                                "label": "Nom de votre " + label,
                                "inputType": "text",
                                "placeholder": "Nom de votre " + label,
                                "value": name_zone,
                                "order": 1
                            },
                            "thematic": {
                                "inputType": "tags",
                                "label": "Sélectionner les filières que vous voulez dés la création de la page",
                                "values": allSugThematic,
                                "value": results,
                                "order": 3
                            }
                        }
                    },
                    "onload": {
                        "actions": {
                            "setTitle": "Creer votre cocity",
                            "src": {
                                "infocustom": "<br/>Remplir le champ"
                            },
                            "hide": {
                                "formLocalityformLocality": 1,
                                "locationlocation": 1
                            }
                        }
                    }
                };
                dyfOrga.afterSave = function(orga) {
                    dyFObj.commonAfterSave(orga, function() {
                        $('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
                        $('#loader-loader<?= $kunik ?>').removeClass('hidden');
                        coInterface.showLoader('#content-loader<?= $kunik ?>');
                        let thematicCocity = [];
                        let allTags = [];
                        if (orga.map.thematic && notNull(orga.map.thematic)) thematicCocity = (orga.map.thematic).split(",");
                        if (orga.map.tags && notNull(orga.map.tags)) allTags = orga.map.tags;
                        if (thematicCocity != []) {
                            $.each(thematicCocity, function(k, value) {
                                let valeur = value.toLowerCase();

                                if (!allTags.includes(valeur)) allTags.push(valeur);

                                if (typeof allThem[valeur] != "undefined" && notNull(allThem[valeur]["tags"])) {
                                    let tagsToAdd = allThem[valeur]["tags"];
                                    let newTags = tagsToAdd.filter(tag => !allTags.includes(tag));
                                    Array.prototype.push.apply(allTags, newTags);
                                }
                            });
                        }
                        let data = {
                            collection: 'organizations',
                            id: orga.id,
                            path: 'allToRoot',
                            value: {
                                "costum.slug": "costumize",
                                "costum.cocity": true,
                                "thematic": thematicCocity,
                                "tags": allTags,
                                "costum.typeCocity": typeZone,
                                "address": globalAddress,
                                "geo": response.geo,
                                "geoPosition": response.geoPosition
                            },
                        }
                        let zoneUpdate = {
                            collection: collectionUpdateZone,
                            id: id_zone,
                            path: 'allToRoot',
                            value: {
                                "cocityId": orga.id
                            }
                        }

                        if (notNull(orga.map.thematic)) {
                            let thema = orga.map.thematic;
                            thematic = thema.split(",");
                            let filiers = {};
                            let coordonateTemp = {
                                "address": orga.map.address,
                                "geo": response.geo,
                                "geoPosition": response.geoPosition
                            };
                            startProcessingWorld(data, thematic, orga, coordonateTemp, globalAddress, zoneUpdate);
                        } else {
                            generateCocityWorld(orga, data, zoneUpdate);
                        }
                    });
                };
                initData.type = "GovernmentOrganization";
                initData.role = "admin";
                dyFObj.openForm('organization', null, Object.assign(initData, {}), null, dyfOrga);
            }
        )
    };

    async function startProcessingWorld(data, thematic, orga, coordonateTemp, standardAddress, zoneUpdate) {
        $(".progress-container-world").removeClass('hidden');
        $(".progress-message-world").removeClass('hidden');
        let progressBar = document.getElementById('progress-bar-world');
        let progressPercentage = document.getElementById('progress-percentage-world');
        let totalItems = thematic.length;
        let currentItem = 0;
        let filiers = {};

        for (let val of thematic) {
            let defaultTags = [];
            let valkey = val.toLowerCase();
            let valname = val.charAt(0).toUpperCase() + val.slice(1).toLowerCase();

            if (typeof allThem[valkey] != "undefined" && notNull(allThem[valkey]["tags"])) {
                defaultTags = allThem[valkey]["tags"];
                filiers[val] = allThem[valkey];
            } else {
                let oneThem = {
                    "default": {
                        "name": valname,
                        "icon": "fa-link",
                        "tags": [valname.toLowerCase()]
                    }
                };
                filiers[valkey] = oneThem["default"];
            }

            let defaultName = orga.map.name + " " + val;
            let paramscocity = {
                cocity: orga.id,
                ville: orga.map.name,
                typeZone: "region",
                thematic: valname,
                keys: orga.map.slug,
                address: standardAddress,
                idTemplatefiliere: "<?= $_idTemplatefiliere ?>"
            };
            let params = {
                name: defaultName,
                collection: "organizations",
                type: "Group",
                role: "admin",
                image: "",
                tags: defaultTags,
                ...coordonateTemp,
                addresses: undefined,
                shortDescription: "Filière " + val + " " + orga.map.name
            };

            currentItem++;
            let progress = (currentItem / totalItems) * 100;

            await createOrgaFiliere(params, paramscocity, progress, progressBar, progressPercentage);
        }

        if (Object.keys(filiers).length === thematic.length) {
            let tplCtx = {
                collection: "organizations",
                value: filiers,
                id: orga.id,
                path: "filiere",
            };
            dataHelper.path2Value(tplCtx, function(response) {
                generateCocityWorld(orga, data, zoneUpdate);
            });
        }
    }

    function generateCocityWorld(orga, data, zoneUpdate) {
        dataHelper.path2Value(zoneUpdate, function() {});
        <?php if ($_idTemplatecocity != "") { ?>
            let tpl_params = {
                "idTplSelected": "<?= $_idTemplatecocity ?>",
                "collection": "templates",
                "action": "",
                "contextId": orga.id,
                "contextSlug": orga.map.slug,
                "contextType": orga.map.collection
            };
            let loadpage = true;
            path2ValueCity(data, tpl_params, loadpage);
        <?php } ?>
    }

    function filterNavigate(level, name) {
        infoShape.classList.remove('active');
        mainMap.style.flex = '1';
        var collect = level == "6" ? "city" : "zones";
        var getparams = {
            level: level,
            countryCode: "",
            collection: collect,
            getLevel: "",
            idgetlevel: "",
            name: name
        };

        var levelInt = parseInt(level);

        switch (levelInt) {
            case 3:
                _typeCreate = "country";
                break;
            case 4:
                _typeCreate = "region";
                break;
            case 5:
                _typeCreate = "departement";
                break;
            case 6:
                _typeCreate = "epci";
                break;
            default:
                _typeCreate = undefined;
        }

        var titleNav = "Résultat de recherche";

        ajaxPost(
            null,
            baseUrl + "/costum/cocity/navigatshapemap",
            getparams,
            function(results) {
                if (Object.keys(results.zones).length > 0) {
                    zones = results.zones;
                    zonesgeo = results.zones;
                    sousCocity = results.sousCocity;
                    $(".title-zone").html(titleNav);
                } else {
                    zones = allInitZones;
                    zonesgeo = allInitZonesgeo;
                    sousCocity = allInitSousCocity;
                    toastr.warning("Aucun résultat n'est associé à ce que vous cherchez.");
                    $(".title-zone").html(titlnavInit);
                }
                worldNavigateMap.clearMap();
                worldNavigateMap.addElts(zones);
                worldNavigateMap.fitBounds();
            }
        );
    }

    jQuery(document).ready(function() {
        worldNavigateMap.clearMap();
        worldNavigateMap.addElts(zones);
        worldNavigateMap.fitBounds();

        if(Object.keys(allCocity).length > 0 && typeof elets.slug != "undefined" && elets.slug == "cocityPrez") setAnnuaireData();

        function executeFilter() {
            var level = $("#dropdown-navigate").val();
            var name = $("#textInput").val();
            
            if (level != null && name != "") {
                filterNavigate(level, name);
            } else {
                toastr.warning("Avant d'activer la recherche, veuillez sélectionner le niveau et saisir le nom du territoire.");
            }
        }

        // $("#btn-filtre").on("click", executeFilter);

        $("#textInput").on("keypress", function(e) {
            if (e.which === 13) {
                executeFilter();
            }
        });

        var dropdown = document.getElementById("dropdown-navigate");
        dropdown.innerHTML = '<option value="" selected disabled>Niveau de territoire</option>';
        var options = [];

        if (elets.slug !== "cocityPrez") {
            if (elets.costum && elets.costum.typeCocity && elets.address) {
                switch (elets.costum.typeCocity) {
                    case 'epci':
                        options = [
                            { value: "6", text: "Commune" }
                        ];
                        titlnavInit = "Les communes faisant partie de l'EPCI " + elets.address.level5Name;
                        _typeCreate = "epci";
                        break;

                    case 'district':
                        options = [
                            { value: "6", text: "Commune" }
                        ];
                        titlnavInit = "Les communes faisant partie du District d'(e) " + elets.address.level4Name;
                        _typeCreate = "district";
                        break;

                    case 'departement':
                        options = [
                            { value: "5", text: "EPCI" },
                            { value: "6", text: "Commune" }
                        ];
                        titlnavInit = "Les EPCI du département de " + elets.address.level4Name;
                        _typeCreate = "departement";
                        break;

                    case 'region':
                        if (elets.address.addressCountry === 'MG') {
                            options = [
                                { value: "4", text: "District" },
                                { value: "6", text: "Commune" }
                            ];
                            titlnavInit = "Les Districts de la région " + elets.address.level3Name;
                        } else {
                            options = [
                                { value: "4", text: "Département" },
                                { value: "5", text: "EPCI" },
                                { value: "6", text: "Commune" }
                            ];
                            titlnavInit = "Les départements de la région " + elets.address.level3Name;
                        }
                        _typeCreate = "region";
                        break;

                    default:
                        titlnavInit = "Toutes les zones";
                }
            } else {
                options = [
                    { value: "3", text: "Région" },
                    { value: "4", text: "Département / District" },
                    { value: "5", text: "EPCI" },
                    { value: "6", text: "Commune" }
                ];
                titlnavInit = "Toutes les zones";
                _typeCreate = "epci";
            }
        } else {
            options = [
                { value: "3", text: "Région" },
                { value: "4", text: "Département / District" },
                { value: "5", text: "EPCI" },
                { value: "6", text: "Commune" }
            ];
            titlnavInit = "Toutes les régions";
            _typeCreate = "country"
        }

        options.forEach(option => {
            var opt = document.createElement("option");
            opt.value = option.value;
            opt.textContent = option.text;
            dropdown.appendChild(opt);
        });

        $(".title-zone").html(titlnavInit);

        $("#dropdown-navigate").on("change", function() {
            var selectedValue = this.value;
            var minValue = options[0]?.value;

            if (selectedValue === minValue) {
                infoShape.classList.remove('active');
                mainMap.style.flex = '1';
                $("#textInput").val("");
                zones = allInitZones;
                zonesgeo = allInitZonesgeo;
                sousCocity = allInitSousCocity;
                $(".title-zone").html(titlnavInit);
                worldNavigateMap.clearMap();
                worldNavigateMap.addElts(zones);
                worldNavigateMap.fitBounds();
            }
        });
    });
</script>