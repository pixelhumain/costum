<?php
$cssAnsScriptFilesModuleMap = array(
    '/leaflet/leaflet.css',
    '/leaflet/leaflet.js',
    '/css/map.css',
    '/markercluster/MarkerCluster.css',
    '/markercluster/MarkerCluster.Default.css',
    '/markercluster/leaflet.markercluster.js',
    '/js/map.js',
);

HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule(Map::MODULE)->getAssetsUrl());

$keyTpl = "mapWithLegende";

$paramsData = [
    "color" => "black",
    "sizeTitle" => 24,
    "colorSousTitle" => "#DDD",
    "sizeSousTitle" => 18,

    "colorContent" => "#DDD",
    "sizeContent" => 14,
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>

<style type="text/css">
    .communaute<?= $kunik?> h1 {
        color:<?=$paramsData['color']?>;
        font-size: <?=$paramsData['sizeTitle']?>px;
    }
    .communaute<?= $kunik?> h2{
        color:<?=$paramsData['colorSousTitle']?>;
        font-size: <?=$paramsData['sizeSousTitle']?>px;
    }
    .communaute<?= $kunik?> p {
        color:<?=$paramsData['colorContent']?>;
        font-size: <?=$paramsData['sizeContent']?>px;
    }
    #mapWithLegende<?= $kunik?>{
        margin-top: 2%;
        position: relative;
        height: auto;
    }
    .legende-container<?= $kunik?>{
        position: absolute;
        left: 0;
        top: 100px;
        display: inline-block;
        z-index: 2;
        margin: 2%;
        border-radius: 10px;
        background: #e2e2e2;
   }
   .legende-container<?= $kunik?> .legend-body<?= $kunik?> {
        display: flex;
        text-align: left;
        padding: 10px;
    }
      
      .legende-container<?= $kunik?> .legend-body<?= $kunik?> {
        display: flex;
        text-align: left;
        padding: 10px;
    }
    .legende-container<?= $kunik?> ul {
        margin-left: -34px;
    }
    .legende-container<?= $kunik?> ul li{
        list-style: none;
        font-size: 15px;
        font-weight: bold;
    }
    .legende-container<?= $kunik?> ul  li i{
        padding: 4px;
        font-size: 20px;
    }
    .legende-container<?= $kunik?> p{
        font-size: 20px;
        font-weight: bold;
        text-align: center;
        color: #f5833c;
    }
    .legende-container<?= $kunik?> hr{
        margin-bottom: 0px;
        margin-top: 0px;
    }
    #mapCommunity<?= $kunik?>{
        z-index: 1;
        height: 500px;
    }
    @media (max-width: 414px) {
        .legende-container<?= $kunik?> {
            position: absolute;
            bottom: 7%;
            left: 5%;
            z-index: 2;
            border-radius: 10px;
            background: #e2e2e2;
        }
        .legende-container<?= $kunik?> ul li {
            list-style: none;
            font-size: 11px;
            font-weight: bold;
        }
        .legende-container<?= $kunik?> ul li i {
            padding: 4px;
            font-size: 13px;
        }
        .legende-container<?= $kunik?> p {
            font-size: 18px;
            font-weight: bold;
            text-align: center;
            color: #f5833c;
            margin-bottom: 0px;
        }
    }
    @media (max-width: 1500px) {
         .legende-container<?= $kunik?> p {
            font-size: 20px;
            font-weight: bold;
            text-align: center;
            color: #f5833c;
            margin-bottom: 0px;
        }
    }
</style>

<div class="communaute<?= $kunik?> ">
    <div id="mapWithLegende<?= $kunik?>">
        <div class="legende-container<?= $kunik?>">
            <div class="legend-header<?= $kunik?> active">
                <p>Legende <i class="fa fa-angle-down"></i></p>
            </div>
            <hr>
            <div class="legend-body<?= $kunik?>">
                <div id="legende<?= $kunik?>" >
                    <div class="legende-content<?= $kunik?>"></div>
                </div>

            </div>
        </div>
        <div class="col-md-12 mapBackground no-padding" id="mapCommunity<?= $kunik?>">
    </div>
    </div>
    
    
</div>

<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;

  jQuery(document).ready(function(){

    sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {
            "title" : "Configurer la section de la carte de la communautée",
            "description" : "Personnaliser votre section e la carte de la communautée",
            "icon" : "fa-cog",
            "properties" : {
            },
            save : function () {
                tplCtx.value = {};

                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                    tplCtx.value[k] = $("#"+k).val();
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        toastr.success("élement mis à jour");
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    } );
                }

            }
        }
    };

$(".edit<?php echo $kunik ?>Params").off().on("click",function() {
    tplCtx.id = $(this).data("id");
    tplCtx.collection = $(this).data("collection");
    tplCtx.path = "allToRoot";

    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
});

 paramsmapCommunity<?= $kunik?> = new CoMap({
    zoom : 3,
    container : "#mapCommunity<?= $kunik?>",
    activePopUp : true,
    tile : "maptiler",
    menuRight : true,
    mapOpt:{
        latLon : ["-21.115141", "55.536384"],
    },
    elts : [],
    mapCustom:{
            markers: {
                getMarker : function(data){
                    mylog.log("dataaaaa", data.category);
                    var imgM = modules.map.assets + '/images/markers/citizen-marker-default.png';
                    if (typeof data.category != "undefined"){
                        if ( data.category =="Artistes")
                            imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_artiste.png";
                        else if ( data.category == "Agriculteurs" || data.category == "Maraîchers")
                             imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_agri.png";
                        else if ( data.category == "Pêcheurs")
                            imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_peche.png";
                        else if(data.category == "Artisans")
                            imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_artiasnt.jpeg";
                        else if (data.category =="Bien-être")
                            imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_bienetre.png";
                        else if  (data.category =="Associations" ||data.category =="Tiers Lieux" )
                            imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_association.png";
                        return imgM;
                    }
                }
            }
        }
});
var mapCommunity<?= $kunik?>Home = {};


afficheCommunity();
});
    

    function afficheCommunity(){
        mylog.log("----------------- Affichage community");
        var params = {
            "id" : costum.contextId,
            "type" : costum.contextType,
            "sourceKey" :costum.contextSlug
        };

        ajaxPost(
            null,
            baseUrl + "/costum/assoKosasa/getcommunity",
            params,
            function(data){
                mylog.log("success : elt ",data.elt);

                paramsmapCommunity<?= $kunik?>.addElts(data.elt);

            }
        );
    }
    var WIND_COLORS =[
    {
        name: "Agriculteurs, Maraîchers",
        bgColor:"green",
        bgColorShade:"red",
        contentColor:"light"
    },
    {
        name: "Pêcheurs",
        bgColor:"blue",
        bgColorShade:"red",
        contentColor:"light"
    },

    {
        name: "Artisans",
        bgColor:"yellow",
        bgColorShade:"red",
        contentColor:"light"
    },
    {
        name: "Artistes",
        bgColor:"orange",
        bgColorShade:"red",
        contentColor:"light"
    },
    {
        name: "Bien-être",
        bgColor:"purple",
        bgColorShade:"red",
        contentColor:"light"
    },
    {
        name: "Associations, Tiers Lieux",
        bgColor:"red",
        bgColorShade:"red",
        contentColor:"light"
    },


    ];
    function kosasaLegende(){
        var legende = $('<ul></ul>');
        WIND_COLORS.reverse().map(color => {
            legende.append(`
                <li>
                <i class="fa fa-map-marker" style="color:${color.bgColor};"></i>
                ${ color.name}
                </li>
                `)
        })
        $('#legende<?= $kunik?> .legende-content<?= $kunik?>').html(legende);
    }
    $(function(){
        kosasaLegende();
        $('.legende-container<?= $kunik?> .legend-header<?= $kunik?>').click(function(){
            $(this).toggleClass('active')
            $('.legende-container<?= $kunik?> .legend-body<?= $kunik?>').toggle('speed')
        })
    });
</script>