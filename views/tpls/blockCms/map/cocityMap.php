<?php
    $keyTpl = "cocityMap";
    $myCmsId  = isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<style id="<?= $kunik ?>cocityMap">
    .<?= $kunik ?>-cocityMap .map-container {
        height: 550px;
        width: auto;
    }
        /* //css modal and loader */
	.loader-loader<?= $kunik ?> {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 42%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 9999999999999999 !important;
        padding-top: 180px !important;
    }
    .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> {
        width: auto;
        height: auto;
    }
    .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> .processingLoader {
        width : auto !important;
        height : auto !important;
    }
    .backdrop-loader<?= $kunik ?> {
        position: fixed;
        opacity: .8;
        background-color : #000;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height : 100vh;
        z-index: 999999999 !important;
    }
    .<?= $kunik ?> .title-zone {
        font-family: "Homestead-Display";
    }

    /* .< ?= $kunik ?> .file_arriane {
        position: absolute;
        margin-top: -45px;
        z-index: 11;
        margin-left: 10px;
        font-size: 17px;
        font-weight: bold;
        background-color: white;
        padding: 5px;
        border-radius: 5px;
    }
    .< ?= $kunik ?> .file_arriane a, .file_arriane i {
        color: #1572cf !important;
    } */

    .<?= $kunik ?> .navigate-section {
        font-size: 21px;
    }
    .<?= $kunik ?> .navigate-section i {
        cursor: pointer;
    }

    .<?= $kunik ?> .navigate-section .right-position {
        margin-left: 84%;
    }
    .<?= $kunik ?> .navigate-section .position-right {
        margin-left: 94%;
    }
    .<?= $kunik ?> .popup-section .btn-more:hover {
        color: white !important;
        background-color: #092434;
    }
    .<?= $kunik ?> .tooltip {
      position: absolute;
      background-color: white;
      color: black;
      padding: 5px;
      border-radius: 5px;
      display: none;
      pointer-events: none;
      opacity: 1 !important;
      font-weight: bold;
      padding: 15px;
    }

    .popup-thema .fa{
        padding: 5px;
        border: 1px solid;
        margin: 3px;
        border-radius: 50%;
        font-size: 16px;
        cursor: pointer;
        color: #013c55;
    }

</style>
<?php if(isset($el['address']) && $el["costum"]["typeCocity"] != "ville"){?>
    <div class="<?= $kunik ?>">
        <div class="loader-loader<?= $kunik ?> hidden" id="loader-loader<?= $kunik ?>" role="dialog">
            <div class="content-loader<?= $kunik ?>" id="content-loader<?= $kunik ?>"></div>
        </div>
        <div class="backdrop-loader<?= $kunik ?> hidden" id="backdrop-loader<?= $kunik ?>"></div>

        <h3 class="title-zone text-center titres" style="font-weight: normal;"></h3>
        <div class="<?= $kunik ?>-cocityMap">
            <div id="main-map" class="map-container">
            </div>
        </div>
        <div id="tooltip" class="tooltip"></div>
        <!-- <p class="file_arriane">
        </p> -->
    </div>
    <script type="text/javascript">

        if(costum.editMode)
        {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode( $blockCms ); ?>;
            var cocityMap = {
                configTabs : {
                    style : {
                        inputsConfig:[
                            {
                                type: "section",
                                options: {
                                    name: "titres",
                                    label: tradCms.title,
                                    inputs: [
                                        "fontSize",
                                        "color"
                                    ]
                                }
                            },
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    }
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            }
            cmsConstructor.blocks.cocityMap<?= $myCmsId ?> = cocityMap;
        }
        var str = "";
        var elCostum = <?= json_encode($el) ?>;
        var sousCocity = [];
        var zones = [];
        let lesthematique = <?= json_encode($allSlugThematic) ?>;
        var titileNavigate = [];
        var _typeCreate = "";
        var allInitZones = [];
        var allIniCocity = [];
        var initTitleZone = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#<?= $kunik ?>cocityMap").append(str);

        function tronquerChaine(chaine, longueurMax) {
            if (chaine.length > longueurMax) {
                return chaine.substring(0, longueurMax - 3) + '...';
            } else {
                return chaine;
            }
        }

        var <?= $kunik ?>cocityMap = new CoMap({
            container : "#main-map",
            activePopUp : true,
            mapOpt:{
                btnHide : false,
                doubleClick : true,
                scrollWheelZoom: false,
                zoom : 4,
            },
            mapCustom:{
                tile : "maptiler",
                getPopup: function(donne){
                    var _id = donne.properties._id['$id'];
                    var cocityId = donne.properties.cocityId;
                    var zone = "";
                    var popup = "";
                    var filters = {};
                    var levelNavigate = 0;
                    var idlast = "";
                    var idafter = "";
                    var htmlNavigate = `<div class="navigate-section">`;
                    var infoTitle = [];
                    var onLevel = donne.properties.level;
                    if(typeof donne.properties.country != "undefined") {
                        zone = "La Ville";
                        levelNavigate = elCostum.address.addressCountry == 'MG' ? 5 : 6;
                        idlast = elCostum.address.addressCountry == 'MG' ? donne.properties.level3 : donne.properties.level4;
                        infoTitle = {
                            "lastlabel" : elCostum.address.addressCountry == 'MG' ?  "Les Districts de la région "+donne.properties.level3Name : "Les EPCI du département de "+donne.properties.level4Name,
                        }
                        htmlNavigate += `<i class="fa fa-minus-circle navigate_carte"  onclick="navigetShapeMap('${_id}','last', ${levelNavigate}, '${idlast}', '${idafter}')"></i>`;
                    } else {
                        if(onLevel[onLevel.length - 1] == "5") {
                            zone = "L'EPCI ";
                            levelNavigate = 5;
                            idlast = donne.properties.level3;
                            idafter = donne.properties._id['$id'];
                            infoTitle = {
                                "lastlabel" : "Les départements de la région "+donne.properties.level3Name,
                                "afterlabel" : "Les communes faisant partie de l'EPCI "+donne.properties.name
                            }
                            htmlNavigate += `<i class="fa fa-minus-circle navigate_carte"  onclick="navigetShapeMap('${_id}','last', ${levelNavigate}, '${idlast}', '${idafter}')"></i>
                                <i class="fa fa-plus-circle navigate_carte right-position" onclick="navigetShapeMap('${_id}','after', ${levelNavigate}, '${idlast}', '${idafter}')"></i>`;
                        };
                        if(onLevel[onLevel.length - 1] == "4") {
                            levelNavigate = 4;
                            if(elCostum.address.addressCountry == 'MG') {
                                zone = "Le District",
                                infoTitle = {
                                    "lastlabel" : "Les Régions de "+donne.properties.level1Name,
                                    "afterlabel" : "Les communes faisant partie du District d'(e) "+donne.properties.name
                                }
                            } else {
                                zone = "Le Département";
                                infoTitle = {
                                    "lastlabel" : "Les Régions de "+donne.properties.level1Name,
                                    "afterlabel" : "Les EPCI du département de "+donne.properties.name
                                }
                            }
                            idafter = donne.properties._id['$id'];
                            htmlNavigate += `<i class="fa fa-minus-circle navigate_carte"  onclick="navigetShapeMap('${_id}','last', ${levelNavigate}, '${idlast}', '${idafter}')"></i>
                                <i class="fa fa-plus-circle navigate_carte right-position" onclick="navigetShapeMap('${_id}','after', ${levelNavigate}, '${idlast}', '${idafter}')"></i>`;
                        };
                        if(onLevel[onLevel.length - 1] == "3") {
                            levelNavigate = 3;
                            zone = "Le Région";
                            idafter = donne.properties._id['$id'];
                            infoTitle = {
                                "afterlabel" : elCostum.address.addressCountry == 'MG' ? "Les Districts de la région "+donne.properties.name : "Les départements de la région "+donne.properties.name
                            }
                            htmlNavigate += `<i class="fa fa-plus-circle navigate_carte position-right" onclick="navigetShapeMap('${_id}','after', ${levelNavigate}, '${idlast}', '${idafter}')"></i>`;
                        };
                    }

                    htmlNavigate += `</div>`;

                    titileNavigate[_id] = infoTitle;

                    if(typeof sousCocity[cocityId] != 'undefined') {
                        var data = sousCocity[cocityId];
                        var id = data._id ? data._id.$id:data.id;
                        var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                        if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
                            imgProfil = baseUrl + data.profilThumbImageUrl;
                        else
                            imgProfil = modules.map.assets + "/images/thumb/default_" + data.collection + ".png";

                        var eltName = data.title ? data.title : data.name;
                        eltName = tronquerChaine(eltName, 30);

                        popup += "<div class='padding-5' id='popup" + id + "'>";
                        popup += "<div class='leaflet-popup-tip-container'><div class='leaflet-popup-tip'></div></div>";
                        popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                        popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

                        if(data.address){
                            var addressStr="";
                            if(data.address.level5Name)
                                addressStr += ((addressStr != "")?", " : "") + data.address.level5Name;
                            if(data.address.level4Name)
                                addressStr += ((addressStr != "")?", " : "") + data.address.level4Name;
                            if(data.address.level3Name)
                                addressStr += ((addressStr != "")?", " : "") + data.address.level3Name;
                            if(data.address.level1Name)
                                addressStr += ((addressStr != "")?", " : "") + data.address.level1Name;
                            popup += "<div class='popup-address text-dark' style='font-size: 10pt; margin-left: 13%; margin-top: -2%;'>";
                            popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
                            popup += "</div>";
                        }
                        if(data.tags && data.tags.length > 0){
                            popup += "<div style='margin-top : 5px;'>";
                            var totalTags = 0;
                            $.each(data.tags, function(index, value){
                                totalTags++;
                                    popup += "<div class='popup-tags'>#" + value + " </div>";
                            })
                            popup += "</div>";
                        }
                        if(data.thematic && data.thematic.length > 0) {
                            popup += "<div class='popup-thema' style='margin-top : 5%;'>";
                            popup += "<div class='popup-subtitle'>Thématiques actives</div>";
                            $.each(data.thematic, function(index, value){
                                if(typeof allThem[value] != 'undefined') {
                                    let links = "";
                                    if(typeof lesthematique[data.slug] != "undefined" && typeof lesthematique[data.slug][value] != "undefined") {
                                        links = lesthematique[data.slug][value];
                                    } else {
                                        if(value == "tiers lieux") {
                                            links = data.slug + "TiersLieux";
                                        } else {
                                            let normalizedStr = value.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
                                            links = data.slug + normalizedStr.charAt(0).toUpperCase() + normalizedStr.slice(1);
                                        }
                                    }
                                    popup += `<a href='<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/${links}' target='_blank'><i class='fa ${allThem[value]['icon']}' title='${value}'></i></a>`;
                                }
                            })
                            popup += "</div>";
                        }
                        if(data.shortDescription && data.shortDescription != ""){
                            popup += "<div class='popup-section'>";
                            popup += "<div class='popup-subtitle'>Description</div>";
                            popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
                            popup += "</div>";
                        }

                        var url = baseUrl+'/costum/co/index/slug/'+data.slug;
                        popup += "<div class='popup-section'>";
                        popup += "<a href='" + url + "' target='_blank' class=' item_map_list popup-marker' id='popup" + id + "'>";
                        popup += '<div class="btn btn-sm btn-more col-md-12">';
                        popup += '<i class="fa fa-hand-pointer-o"></i>&nbsp;&nbsp; Voir la COcity';
                        popup += '</div></a>';
                        if(levelNavigate != 0) popup += htmlNavigate;
                        popup += '</div>';
                        popup += '</div>';
                    } else {
                        popup += "<div class='padding-5' id='popup-new-cocity'>";
                        popup += "<div class='leaflet-popup-tip-container'><div class='leaflet-popup-tip'></div></div>";
                        popup += "<div class='popup-section'>";
                        popup += "<p>"+ zone +" <b>"+donne.properties.name+"</b> n'a pas de COCITY.</p>";
                        popup += `<a target='_blank' class='item_map_list popup-marker' onclick="newCocity('${donne.properties._id["$id"]}','${(donne.properties.name).replaceAll("'", "\\'")}')" id='btn-new-cocity${donne.properties._id["$id"]}'>`;
                        popup += '<div class="btn btn-sm btn-more col-md-12">';
                        popup += 'Créer votre cocity';
                        popup += '</div></a>';
                        if(levelNavigate != 0) popup += htmlNavigate;
                        popup += '</div>';
                        popup += '</div>';
                    }
                    return popup;
                },
                addElts: function(_context, data){
                    var geoJson = [];
                    let elements = document.querySelectorAll('#main-map .leaflet-interactive');
                    let tooltip = document.getElementById('tooltip');
                    $.each(data, function(k, v){
                        if(! _context.getOptions().data[k])
                             _context.getOptions().data[k] = v;
                        if(typeof v.geoShape != "undefined"){
                            var data = $.extend({}, v);
                            var geoData = {
                                    type: "Feature",
                                    properties: {
                                        name: v.name
                                    },
                                    geometry: v.geoShape
                            };
                            delete data.name;
                            delete data.getShape;
                            $.each(data, function(key,value) {
                                if(typeof geoData.properties[key] == 'undefined'){
                                    geoData.properties[key] = value;
                                }
                            })
                            geoJson.push(geoData);
                        }
                        // if(typeof v.geo != "undefined") {
                        //     v.id = k;
                        //     _context.addMarker({ elt: v })
                        //     _context.getOptions().mapCustom.lists.createItemList(k, v, _context.getOptions().container)
                        // }
                    })
                    if( _context.getOptions().activeCluster)
                        _context.getMap().addLayer(_context.getOptions().markersCluster)

            
                     _context.getOptions().geoJSONlayer = L.geoJSON(geoJson, {
                        onEachFeature: function(feature, layer){
                            _context.addPopUp(layer, feature);
                            layer.setStyle({
                                color: typeof feature.properties.cocityId != 'undefined' ? "#1a72d0" : '#777777f0',
                                opacity: 0.5
                            });
                            var onLevel = feature.properties.level;
                            var levelNavigate = 0;
                            var idlast = "";
                            var idafter = "";
                            var contryCd = "";
                            layer.on("mouseover", function(event){
                                this.setStyle({
                                    "fillOpacity" : 1,
                                    "strokeOpacity" : 1
                                });
                                let name = this.feature.properties.name;
                                tooltip.innerHTML = name;
                                tooltip.style.display = 'block';
                                let x = event.containerPoint.x;
                                let y = event.containerPoint.y;
                                tooltip.style.left = `${x}px`;
                                tooltip.style.top = `${y}px`;
                            });

                            layer.on("mouseout", function(){
                                this.setStyle({
                                    "fillOpacity" : 0.2,
                                    "strokeOpacity" : 0.2,
                                });
                                tooltip.style.display = 'none';
                            });
                        }
                    });

                    _context.getMap().addLayer( _context.getOptions().geoJSONlayer);

                    _context.hideLoader();
                    _context.fitBounds(data);
                }
            },
            elts : {}
        });

        function newCocity(id_zone, name_zone){
            var response = zones[id_zone];
            var initData ={};
            var globalAddress = {};
            var typeZone = "";
            var label = "";
            var collectionUpdateZone = "zones";
            if(_typeCreate == 'departement') {
                initData.address = {
                    "localityId" : {$in : response.cities}
                };
                globalAddress = {
                    "level5" : id_zone,
                    "level5Name" : name_zone,
                    "level4" : response.level4,
                    "level4Name" : (response.level4Name).toUpperCase(),
                    "level3" : response.level3,
                    "level3Name" : response.level3Name,
                    "level1" : response.level1,
                    "level1Name" : response.level1Name,
                    "addressCountry" : response.countryCode
                }
                typeZone = "epci";
                label = "EPCI";
            } else if(_typeCreate == 'region') {
                initData.address = {
                    "level4" : id_zone,
                    "level4Name" : name_zone.toUpperCase()
                };
                globalAddress = {
                    "level4" : id_zone,
                    "level4Name" : name_zone.toUpperCase(),
                    "level3" : response.level3,
                    "level3Name" : response.level3Name,
                    "level1" : response.level1,
                    "level1Name" : response.level1Name,
                    "addressCountry" : response.countryCode
                }
                typeZone = response.countryCode == "MG" ? "district" : "departement";
                label = response.countryCode == "MG" ? "District" : "Département";
            } else if(_typeCreate == 'epci' || _typeCreate == 'district') {
                initData.address = {
                    "localityId" : id_zone,
                    "level1Name" : response.level1Name
                };
                globalAddress = {
                    "localityId" : id_zone,
                    "addressLocality" : name_zone,
                    "level4" : response.level4,
                    "level4Name" : (response.level4Name).toUpperCase(),
                    "level3" : response.level3,
                    "level3Name" : response.level3Name,
                    "level1" : response.level1,
                    "level1Name" : response.level1Name,
                    "addressCountry" : response.country
                }
                typeZone = "ville";
                label = "Ville";
                collectionUpdateZone = "cities";
            } else if(_typeCreate == "country") {
                initData.address = {
                    "level3" : id_zone,
                    "level3Name" : name_zone
                };
                globalAddress = {
                    "level3" : id_zone,
                    "level3Name" : name_zone,
                    "level1" : response.level1,
                    "level1Name" : response.level1Name,
                    "addressCountry" : response.countryCode
                }
                typeZone = "region";
                label = "Région";
            }

            var paramsCheck = {
                "thematic" : allSugThematic,
                "address" : initData.address,
                "typeCity" : typeZone
            }
            ajaxPost(
                null,
                baseUrl+"/costum/cocity/checkdatafiliere",
                paramsCheck,
                function(results) {
                    var dyfOrga={
                        "beforeBuild":{
                            "properties" : {
                                "name" : {
                                    "label" : "Nom de votre "+label, 
                                    "inputType" : "text",
                                    "placeholder" :"Nom de votre "+label,
                                    "value" : name_zone,
                                    "order" : 1
                                },
                                "thematic":{
                                    "inputType" : "tags",
                                    "label" : "Sélectionner les filières que vous voulez dés la création de la page",
                                    "values" : allSugThematic,
                                    "value" : results,
                                    "order" : 3
                                }
                            }
                        },
                        "onload" : {
                            "actions" : {
                                "setTitle" : "Creer votre cocity",
                                "src" : {
                                    "infocustom" : "<br/>Remplir le champ"
                                },
                            "hide" : {
                                "formLocalityformLocality" : 1,
                                "locationlocation" : 1
                                } 
                            }
                        }   
                    };
                    dyfOrga.afterSave = function(orga) {
                        dyFObj.commonAfterSave(orga, function(){
                            $('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
                            $('#loader-loader<?= $kunik ?>').removeClass('hidden');
                            coInterface.showLoader('#content-loader<?= $kunik ?>');
                            var thematicCocity = [];
                            var allTags = [];
                            if(orga.map.thematic && notNull(orga.map.thematic)) thematicCocity = (orga.map.thematic).split(",");
                            if(orga.map.tags && notNull(orga.map.tags)) allTags = orga.map.tags;
                            if(thematicCocity != []) {
                                $.each(thematicCocity, function(k, value){
                                    var valeur = value.toLowerCase();
                                    
                                    if(!allTags.includes(valeur)) allTags.push(valeur);

                                    if (typeof  allThem[valeur] != "undefined" && notNull(allThem[valeur]["tags"])) {
                                        var tagsToAdd = allThem[valeur]["tags"];
                                        var newTags = tagsToAdd.filter(tag => !allTags.includes(tag));
                                        Array.prototype.push.apply(allTags, newTags);
                                    } 
                                });
                            }
                            var data = {
                                collection : 'organizations',
                                id: orga.id,
                                path: 'allToRoot',
                                value : {
                                    "costum.slug" : "costumize",
                                    "costum.cocity" : true,
                                    "thematic" : thematicCocity,
                                    "tags" : allTags,
                                    "costum.typeCocity" : typeZone,
                                    "address" : globalAddress,
                                    "geo" : response.geo,
                                    "geoPosition" : response.geoPosition
                                },
                            }
                            var zoneUpdate = {
                                collection : collectionUpdateZone,
                                id:id_zone,
                                path: 'allToRoot',
                                value : {
                                    "cocityId" : orga.id
                                }
                            }

                            if (notNull(orga.map.thematic) ) {
                                var thema = orga.map.thematic;
                                thematic = thema.split(",");
                                var filiers = {};
                                var coordonateTemp = {
                                    "address" : orga.map.address,
                                    "geo" : response.geo,
                                    "geoPosition" : response.geoPosition
                                };
                                $.each(thematic , function(k,val) {
                                    var defaultTags = [];
                                    var valkey = val.toLowerCase();
                                    var valname = (val.toLowerCase()).charAt(0).toUpperCase() + (val.toLowerCase()).slice(1);
                                    
                                    if (typeof  allThem[valkey] != "undefined" && notNull(allThem[valkey]["tags"])) {
                                        defaultTags = allThem[valkey]["tags"]; 
                                        filiers[val] = allThem[valkey];
                                    } else {
                                        var oneThem = {
                                            "default" : {
                                                "name" : valname,
                                                "icon" : "fa-link",
                                                "tags" : [valname.toLowerCase()]
                                            }
                                        };
                                        filiers[valkey] = oneThem["default"];
                                    }
                                    var defaultName = orga.map.name +" "+ val;
                                    var paramscocity = {
                                        cocity : orga.id,
                                        ville : orga.map.name,
                                        typeZone : typeZone,
                                        thematic : valname,
                                        keys: orga.map.slug,
                                        address : globalAddress,
                                        idTemplatefiliere : "<?= $_idTemplatefiliere ?>"
                                    };
                                    var params = {
                                        name: defaultName,
                                        collection : "organizations",
                                        type: "Group",
                                        role: "admin",
                                        image: "",
                                        tags: defaultTags,
                                        ...coordonateTemp,
                                        addresses : undefined,
                                        shortDescription: "Filière "+val+" "+orga.map.name
                                    }
                                    createOrgaFiliere(params,paramscocity);  
                                });
                                if (filiers !== null && Object.keys(filiers).length == thematic.length){
                                    var tplCtx = {
                                        collection: "organizations",
                                        value: filiers,
                                        id: orga.id,
                                        path: "filiere",
                                    };
                                    dataHelper.path2Value(tplCtx, function (data){})
                                }
                            }

                            dataHelper.path2Value(zoneUpdate, function(){});
                            <?php if($_idTemplatecocity != "") { ?>
                                let tpl_params = {"idTplSelected" : "<?= $_idTemplatecocity ?>", "collection" : "templates", "action" : "", "contextId" : orga.id, "contextSlug" : orga.map.slug, "contextType" : orga.map.collection};
                                let loadpage = true;
                                path2ValueCity(data, tpl_params, loadpage)
                            <?php } ?>
                        });
                    }
                    initData.type = "GovernmentOrganization";
                    initData.role = "admin";
                    dyFObj.openForm('organization',null,Object.assign(initData, {}),null,dyfOrga);
                }
            )
        };
        function getDepartementRegion(){
            let initTypeCreate = "";
            <?php if(isset($initTypeCreate)) { ?>
                initTypeCreate = <?= json_encode($initTypeCreate) ?>;
            <?php } ?>
            _typeCreate = initTypeCreate != "" ? initTypeCreate : elCostum.costum.typeCocity;
            zones = <?= json_encode($blockCms['zones']) ?>;
            sousCocity = <?= json_encode($sousCocity) ?>;
            allIniCocity = <?= json_encode($sousCocity) ?>;
            allInitZones = <?= json_encode($blockCms['zones']) ?>

            if(_typeCreate == 'epci' || _typeCreate == 'district') {
                if(_typeCreate == 'epci') {
                    $(".title-zone").html("Les communes faisant partie de l'EPCI "+elCostum.address.level5Name);
                    initTitleZone = "Les communes faisant partie de l'EPCI "+elCostum.address.level5Name;
                } else {
                    $(".title-zone").html("Les communes faisant partie du District d'(e) "+elCostum.address.level4Name);
                    initTitleZone = "Les communes faisant partie du District d'(e) "+elCostum.address.level4Name;
                }
            } else if(_typeCreate == 'departement') {
                $(".title-zone").html("Les EPCI du département de "+elCostum.address.level4Name);
                initTitleZone = "Les EPCI du département de "+elCostum.address.level4Name;
            } else if(_typeCreate == 'region') {
                if(elCostum.address.addressCountry == 'MG') {
                    $(".title-zone").html("Les Districts de la région "+elCostum.address.level3Name);
                    initTitleZone = "Les Districts de la région "+elCostum.address.level3Name;
                } else {
                    $(".title-zone").html("Les département de la région "+elCostum.address.level3Name);
                    initTitleZone = "Les département de la région "+elCostum.address.level3Name;
                }
            }

            <?= $kunik ?>cocityMap.clearMap();
            <?= $kunik ?>cocityMap.addElts(zones);
        };

        function navigetShapeMap(_id, section, level, id_last = "", id_after = ""){
            let getparams = {};
            let titleNav = "";

            let levelInt = parseInt(level);

            if (section == 'last') {
                getparams = {
                    level: levelInt - 1,
                    countryCode: elCostum.address.addressCountry,
                    collection: "zones",
                    getLevel: levelInt - 2,
                    idgetlevel: id_last
                };
                titleNav = titileNavigate[_id].lastlabel;

                switch (levelInt) {
                    case 4:
                        _typeCreate = "country";
                        break;
                    case 5:
                        _typeCreate = "region";
                        break;
                    case 6:
                        _typeCreate = "departement";
                        break;
                    default:
                        _typeCreate = undefined;
                }
            } else if (section == 'after') {
                if (level == "5" || (level == "4" && elCostum.address.addressCountry == "MG")) {
                    let type = level == "5" ? 'EPCI' : 'District';
                    getparams = {
                        country: elCostum.address.addressCountry,
                        collection: "cities",
                        idgetlevel: id_after,
                        typeZone: type
                    };
                } else {
                    getparams = {
                        level: levelInt + 1,
                        countryCode: elCostum.address.addressCountry,
                        collection: "zones",
                        getLevel: levelInt,
                        idgetlevel: id_after
                    };
                }
                titleNav = titileNavigate[_id].afterlabel;

                switch (levelInt) {
                    case 3:
                        _typeCreate = "region";
                        break;
                    case 4:
                        _typeCreate = elCostum.address.addressCountry === "MG" ? "district" : "departement";
                        break;
                    case 5:
                        _typeCreate = "epci";
                        break;
                    default:
                        _typeCreate = undefined;
                }
            }
            ajaxPost(
                null,
                baseUrl+"/costum/cocity/navigatshapemap",
                getparams,
                function(results){
                    if(Object.keys(results.zones).length > 0) {
                        zones = results.zones
                        sousCocity = results.sousCocity;
                    } else {
                        zones = allInitZones;
                        sousCocity = allIniCocity;
                        titleNav = initTitleZone;
                    }
                    $(".title-zone").html(titleNav);
                    <?= $kunik ?>cocityMap.clearMap();
                    <?= $kunik ?>cocityMap.addElts(zones);
                }
            );
        }
        jQuery(document).ready(function(){
                getDepartementRegion();
        });
    </script>
<?php } else { ?> 
    <script>
        toastr.info("Il n'y a pas d'adresse associée à votre élément. Veuillez le renseigner dans l'information générale de votre organisation.");
    </script>
<?php } ?>