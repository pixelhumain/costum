<?php
    $keyTpl = "basicmaps";
    $myCmsId  = isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
    $styleCss = (object) [ $kunik => $blockCms["css"] ?? [] ];
?>

<style id="map<?= $kunik ?>"></style>

<div class="<?= $kunik ?>">
    <div id="mapElement<?= $kunik ?>" style="height: 500px;">
    </div>
</div>

<script>
    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#map<?= $kunik ?>").append(str);
    var appMap = null;
    $(function(){
        var customMap=(typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {tile : "maptiler"};
        var mapOptions = {
            container : "#mapElement<?= $kunik ?>",
            activePopUp : true,
            clusterType : 'pie',
            markerType : 'default',
            showLegende: false,
            dynamicLegende: false,
            activeCluster: false,
            groupBy: 'tags',
            legende: [],
            legendeLabel: '',
            legendeVar: 'tags',
            mapOpt:{
                zoomControl:false,
                onclickMarker: answers
            },
            mapCustom:customMap,
            elts : <?= json_encode($blockCms["dataZone"]) ?>
        };
        appMap = new MapD3(mapOptions);
    });
    function answers(params) {
        var id = typeof params.properties != "undefined" ? params.properties.id : params.elt.id;
        hashId = location.hash.split(".");
        setUrl=hashId[0].substring(0);
        history.replaceState({}, null, `${setUrl}.reseauId.${id}`);
        var arrayOfIds = [];
        $(".<?= $kunik ?>").parents("div[data-blocktype=section]").nextAll().each(function(){
            var idBlock = $(this).data('id');
            arrayOfIds.push(idBlock);
        });

        ajaxPost(
            null,
            baseUrl+"/co2/cms/refreshmultiblock",
            {
                idBlocks: arrayOfIds,
                extraParams: {
                    reseauId: id
                } 
            },
            function(data) {
                if(data.view){
                    Object.keys(data.view).forEach(element => {
                        $(`.cmsbuilder-block[data-id='${element}']`).html(data.view[element]);
                    });
                }
            },
            null,
            "json"
        )
        // cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
    }
    if(costum.editMode){
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        var mapVisualisation = {
            configTabs: {
                general : {
                    inputsConfig : [
                        {
                            type: "inputSimple",
                            options: {
                                name: "url",
                                label: "Lien de recuperation des données"
                            }
                        }
                    ]
                },
            },
            onChange: function(path, valueToSet, name, payload, value){
                
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
            }
        }
        cmsConstructor.blocks['<?= $kunik ?>'] = mapVisualisation;
    }
</script>