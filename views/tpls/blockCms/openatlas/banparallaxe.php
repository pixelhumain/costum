<?php ?>

<style>
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    .wrapper,
    .content {
        position: relative;
        width: 100%;
        z-index: 1;
    }

    .content {
        overflow: hidden;
    }

    .content .section {
        width: 100%;
        height: 100vh;
    }

    .content .section.hero {
        background-image: url('<?= Yii::app()->getModule(Costum::MODULE)->getAssetsUrl() ?>/images/OpenAtlas/banniere/welcome.png');
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .image-container {
        width: 100%;
        height: 100vh;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        z-index: 2;
        perspective: 500px;
        overflow: hidden;
    }

    .image-container img {
        width: 100%;
        height: 100%;
        object-fit: cover;
        object-position: center center;
    }
    
</style>

<div class="wrapper">
    <div class="content">
        <section class="section hero"></section>
    </div>
    <div class="image-container">
        <img src="https://assets-global.website-files.com/63ec206c5542613e2e5aa784/643312a6bc4ac122fc4e3afa_main%20home.webp" alt="image">
    </div>
</div>
<script>
    $(document).ready(function() {
        gsap.registerPlugin(ScrollTrigger);

        function initAnimations() {
            gsap
                .timeline({
                    scrollTrigger: {
                        trigger: ".wrapper",
                        start: "top top",
                        end: "+=150%",
                        pin: true,
                        scrub: true
                    }
                })
                .to(".image-container img", {
                    scale: 2,
                    z: 350,
                    transformOrigin: "center center",
                    ease: "power1.inOut"
                })
                .to(
                    ".section.hero", {
                        scale: 1.1,
                        transformOrigin: "center center",
                        ease: "power1.inOut"
                    },
                    "<"
                );
        }

        initAnimations();
    });
</script>