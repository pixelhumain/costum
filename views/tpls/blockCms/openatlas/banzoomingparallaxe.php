<style>
    .wrapper {
        position: relative;
        width: 100%;
        height: 100vh;
    }

    .image-container {
        width: 100%;
        height: 100vh;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        overflow: hidden;
    }

    .image-container img {
        width: 100%;
        height: 100%;
        object-fit: cover;
        object-position: center center;
    }

    .image-container img.image6 {
        object-fit: contain !important;
    }
</style>

<div class="wrapper">
    <div class="image-container">
        <img src="<?= Yii::app()->getModule(Costum::MODULE)->getAssetsUrl() ?>/images/OpenAtlas/banniere/1.png" class="image1">
    </div>
    <div class="image-container">
        <img src="<?= Yii::app()->getModule(Costum::MODULE)->getAssetsUrl() ?>/images/OpenAtlas/banniere/2.png" class="image2">
    </div>
    <div class="image-container">
        <img src="<?= Yii::app()->getModule(Costum::MODULE)->getAssetsUrl() ?>/images/OpenAtlas/banniere/3.png" class="image3">
    </div>
    <div class="image-container">
        <img src="<?= Yii::app()->getModule(Costum::MODULE)->getAssetsUrl() ?>/images/OpenAtlas/banniere/4.png" class="image4">
    </div>
</div>

<script>
    $(document).ready(function() {
        gsap.registerPlugin(ScrollTrigger);

        function initAnimations() {
            let wrapper = document.querySelector(".wrapper");
            let wrapperHeight = wrapper.offsetHeight;
            let windowHeight = window.innerHeight;
            let tl = gsap.timeline({
                scrollTrigger: {
                    trigger: ".wrapper",
                    start: "top top",
                    end: () => document.querySelector(".wrapper").offsetHeight + "px",
                    pin: true,
                    scrub: 5,
                }
            });

            tl.to(".image-container .image1", {
                scale: 2,
                transformOrigin: "center center",
                ease: "power1.inOut",
                duration: 1,
                opacity: 0
            });

            tl.fromTo(".image-container .image2", {
                opacity: 0
            }, {
                opacity: 1,
                scale: 1,
                ease: "power1.inOut",
                duration: 1
            });

            tl.to(".image-container .image2", {
                scale: 2,
                transformOrigin: "right center",
                ease: "power1.inOut",
                duration: 1,
                opacity: 0
            });

            tl.fromTo(".image-container .image3", {
                opacity: 0
            }, {
                scale: 1,
                opacity: 1,
                ease: "power1.inOut",
                duration: 1
            }, );

            tl.to(".image-container .image3", {
                scale: 2,
                transformOrigin: "center right",
                ease: "power1.inOut",
                duration: 1,
                opacity: 0
            });

            tl.fromTo(".image-container .image4", {
                opacity: 0
            }, {
                scale: 1,
                opacity: 1,
                ease: "power1.inOut",
                duration: 1
            }, );
        }

        initAnimations();
    })
</script>