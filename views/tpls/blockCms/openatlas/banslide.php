<?php
    $cssAndScriptFilesModule = array("/js/blockcms/gsap/gsap.min.js", "/js/blockcms/gsap/ScrollTrigger.min.js");
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());

    $container = "banner-block-".$kunik;
    $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
    $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
    $otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
    $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
    $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
    $costumLanguage = isset($costum['langCostumActive']) ? $costum['langCostumActive'] : 'fr';
?>

<style id="banslide-<?= $kunik ?>">
    :root {
        --green-color: #68ca91;
        --pink-color: #e54e5e;
        --black-color: #1b1b1b;
        --white-color: #fff;
    }
    
    /* Style du slider */
    .banner-block-<?= $kunik ?> .slider-container {
        position: relative;
        overflow: hidden;
        width: 100%;
        height: 100vh;
        background-size: cover;
        background-attachment: fixed;
        background-position: center center;
        background-repeat: no-repeat;
        background-blend-mode: overlay;
        transition: all .2s ease; 
    }

    .banner-block-<?= $kunik ?> .slider-container:before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%; 
        z-index: 1;
        background: linear-gradient(to bottom, rgba(27, 27, 27, 0.5), rgba(27, 27, 27, 0.8));
    }

    .banner-block-<?= $kunik ?> .slider-illustration {
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .banner-block-<?= $kunik ?> .slider-illustration .slider-text,
    .banner-block-<?= $kunik ?> .slider-illustration .hero-image {
        height: 100%;
        position: relative;
        display: flex;
        align-items: center;
        justify-content: center;
        flex: 0.5;
    }

    .banner-block-<?= $kunik ?> .slider-illustration .all-slide {
        position: relative;
        height: 100%;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .banner-block-<?= $kunik ?> .slider-illustration .slide {
        position: absolute;
        opacity: 0;
        padding-left: 150px;
        padding-right: 100px;
        text-align: center;
        z-index: 2;
    }

    .banner-block-<?= $kunik ?> .slider-illustration .slide-title strong {
        color: var(--pink-color);
    }

    .banner-block-<?= $kunik ?> .slider-illustration .slide-description {
        font-size: 1.5em;
        color: var(--white-color);
    }

    .banner-block-<?= $kunik ?> .slider-illustration .slide-title {
        font-size: 45px;
        font-weight: 900;
        margin-bottom: 10px;
        color: var(--white-color);
        text-transform: uppercase;
    }

    .banner-block-<?= $kunik ?> .slide-btn {
        padding: 20px 50px;
        background: var(--pink-color);
        border: none;
        color: var(--white-color);
        font-size: 1.0em;
        text-transform: uppercase;
        font-weight: 900;
        border-radius: 100px;
        margin-top: 40px;
        display: inline-block;
        text-decoration: none;
    }

    .banner-block-<?= $kunik ?> .slider-illustration .hero-image {
        z-index: 2;
        height: 100%;
        padding-right: 80px;
    }

    .banner-block-<?= $kunik ?> .imageHero {
        width: 100%;
    }

    .banner-block-<?= $kunik ?> .scroll-down-btn {
        position: absolute;
        bottom: 20px;
        left: 50%;
        transform: translateX(-50%);
        border: none;
        color: var(--white-color);
        font-size: 1.8em;
        cursor: pointer;
        z-index: 2;
        background: none;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        font-weight: 900;
        transition: background 0.3s;
    }

    .banner-block-<?= $kunik ?> .scroll-down-btn:hover {
        background: var(--pink-color);
    }

    .banner-block-<?= $kunik ?> .slider-nav {
        position: absolute;
        bottom: 50%;
        width: 105px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        z-index: 20;
        padding: 0 15px;
    }

    .banner-block-<?= $kunik ?> .prev-btn,
    .banner-block-<?= $kunik ?> .next-btn {
        background-color: var(--white-color);
        border: none;
        cursor: pointer;
        font-size: 1.0em;
        transition: background 0.3s;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        display: flex;
        align-items: center;
        justify-content: center;
        color: var(--black-color);
        margin: 10px;
    }

    .banner-block-<?= $kunik ?> .prev-btn:hover,
    .banner-block-<?= $kunik ?> .next-btn:hover {
        background: var(--pink-color);
        color: var(--white-color);
    }

    .banner-block-<?= $kunik ?> .dots-container-banner-content {
        position: absolute;
        bottom: 80px;
        right: 20px;
        transform: translateY(-50%);
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        gap: 10px;
        z-index: 999;
    }

    .banner-block-<?= $kunik ?> .dot-banner {
        width: 35px;
        height: 35px;
        background-color: var(--white-color);
        color: var(--black-color);
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        cursor: pointer;
        transition: background-color 0.3s;
        margin-bottom: 20px;
        font-size: 0.95em;
    }

    .banner-block-<?= $kunik ?> .dot-banner:last-child {
        margin-bottom: 0;
    }

    .banner-block-<?= $kunik ?> .dot-banner.active {
        background-color: var(--pink-color);
        color: var(--white-color);
    }

    .banner-block-<?= $kunik ?> .sep-dual {
        position: absolute !important;
        left: 50%;
        top: 100%;
        z-index: 2;
    }

    .banner-block-<?= $kunik ?> .sep-dual.sep-gray:after,
    .banner-block-<?= $kunik ?> .sep-dual.sep-gray:before {
        background: var(--white-color);
    }

    .banner-block-<?= $kunik ?> .sep-dual:after {
        -webkit-transform: rotate(7deg);
        transform: rotate(7deg) !important;
    }

    .banner-block-<?= $kunik ?> .sep-dual:before,
    .banner-block-<?= $kunik ?> .sep-dual:after {
        content: ' ';
        display: block;
        position: absolute;
        bottom: -250px;
        left: -1500px;
        z-index: 2;
        background: white;
        width: 3000px;
        height: 250px;
        -webkit-transform: rotate(-7deg);
        transform: rotate(-7deg);
    }

    @media screen and (max-width: 1000px) {
        .banner-block-<?= $kunik ?> .slider-illustration .hero-image {
            display: none;
        }

        .banner-block-<?= $kunik ?> .slider-illustration .slider-text {
            flex: 1;
            width: 100%;
            position: relative;
        }

        .banner-block-<?= $kunik ?> .slider-illustration .slide {
            padding: 150px;
        }
    }

    @media screen and (max-width: 750px) {
        .banner-block-<?= $kunik ?> .slider-nav {
            position: absolute;
            width: 100%;
            flex-direction: row;
            bottom: 10%;
        }

        .banner-block-<?= $kunik ?> .slider-illustration .slide {
            padding: 50px;
        }
    }

    @media screen and (max-width: 600px) {
        .banner-block-<?= $kunik ?> .slide-title {
            font-size: 50px;
        }
    }

    @media screen and (max-width: 450px) {
        .banner-block-<?= $kunik ?> .slide-title {
            font-size: 40px;
        }
    }

    /* input configuration */
    .input-section-slideData {
        border: 1px solid #ccc;
        border-radius: 5px;
    }

    .input-section-slideData .inputmultiple-label {
        background: #ccc;
        width: 100%;
        padding: 10px 5px;
        color: #181818;
    }

    .input-section-slideData .inputs-container {
        padding: 5px;
    }

    .input-section-slideData .input-item {
        position: relative;
    }

    .input-section-slideData .input-item:not(:last-child) {
        border-bottom: 1px solid #ccc;
        margin-bottom: 25px;
    }

    .input-simple-external-link,
    .input-select-internal-link {
        margin-top: 15px;
        margin-bottom: 30px !important;
    }

</style>
    
<!-- Conteneur du slider -->
<div class="banner-block-<?= $kunik ?> <?= $kunik ?>">
    <div class="slider-container back-container-slide1 slideContainer">
        <div class="slider-illustration">
            <div class="slider-text">
                <div class="all-slide">
                    <?php 
                        if (isset($blockCms["slideData"])) {
                            $count = 0;
                            foreach ($blockCms["slideData"] as $key => $slide) {
                                $hideTitle = (isset($slide["hideTitle"])) ? $slide["hideTitle"] : false;
                                $hideDescription = (isset($slide["hideDescription"])) ? $slide["hideDescription"] : false;
                                $hideBptn = isset($slide["hideBptn"]) ? $slide["hideBptn"] : false;
                                if (strpos($slide["slideBackground"], "images/OpenAtlas/banniere/") === 0) {
                                    $blockCms["slideData"][$key]["slideBackground"] = Yii::app()->getModule(Costum::MODULE)->getAssetsUrl() . "/" .$slide["slideBackground"];
                                }
                                $title = isset($slide["title"]) ? $slide["title"] : [ "fr" => "Lorem ipsum"];
                                $description = isset($slide["description"]) ? $slide["description"] : [ "fr" => "Lorem ipsum dolor" ];
                                if (isset($slide["button"])) {
                                    $btnText = isset($slide["button"]["text"]) ? $slide["button"]["text"] : [ "fr" => "Texte du boutton" ];
                                    $targetBlank = (isset($slide["button"]["targetBlank"]) && $slide["button"]["targetBlank"] === true) ? " target='_blank'" : ""; 
                                    $link = isset($slide["button"]["link"]) ? $slide["button"]["link"] : "#";
                                    $classBtn = "";
                                    if (strpos($link, '#') === 0) {
                                        $classBtn = " lbh";
                                    }
                                    if (strpos($link, '#') !== 0 && (strpos($link, 'http://') !== 0 && strpos($link, 'https://') !== 0)) {
                                        $link = "https://".$link;
                                    }
                                }

                                $titleToShow = "";
                                $titleLang = "fr";
                                if (array_key_exists($costumLanguage, $title)) {
                                    $titleToShow = $title[$costumLanguage];
                                    $titleLang = $costumLanguage;
                                } else {
                                    $titleToShow = reset($title);
                                }

                                $descriptionToShow = "";
                                $descriptionLang = "fr";
                                if (array_key_exists($costumLanguage, $description)) {
                                    $descriptionToShow = $description[$costumLanguage];
                                    $descriptionLang = $costumLanguage;
                                } else {
                                    $descriptionToShow = reset($description);
                                }

                                $btnTextToShow = "";
                                $btnTextLang = "fr";
                                if (array_key_exists($costumLanguage, $btnText)) {
                                    $btnTextToShow = $btnText[$costumLanguage];
                                    $btnTextLang = $costumLanguage;
                                } else {
                                    $btnTextToShow = reset($btnText);
                                }
                                $count++;
                            ?>
                                <div class="slide slide<?= $count ?>" data-speed="0.5" data-slide="<?= $count ?>">
                                    <?php if (!$hideTitle) { ?>
                                        <h2 class="slide-title sp-text" id="title-slide<?= $count ?>" data-id="<?= $blockKey ?>" data-field="slideData.<?= $key ?>.title" data-lang="<?= $titleLang ?>"><?= $titleToShow ?></h2>
                                    <?php } ?> 
                                    <?php if (!$hideDescription) { ?>
                                        <p class="slide-description sp-text" id="description-slide<?= $count ?>" data-id="<?= $blockKey ?>" data-field="slideData.<?= $key ?>.description" data-lang="<?= $descriptionLang ?>"><?= $descriptionToShow ?></p>
                                    <?php } ?> 
                                    <?php if (!$hideBptn) { ?>
                                        <a class="slide-btn sp-text<?= $classBtn ?> btnSlide" <?= (isset($costum["editMode"]) && $costum["editMode"] == "false") ? "href='".$link."'" : "" ?> id="button-slide<?= $count ?>" data-id="<?= $blockKey ?>" data-field="slideData.<?= $key ?>.button.text" data-lang="<?= $btnTextLang ?>"<?= $targetBlank ?>><?= $btnTextToShow ?></a>
                                    <?php } ?> 
                                </div>     
                    <?php
                            }
                        }
                    ?>
                </div>
            </div>
            <?php if (strpos($blockCms["imageHero"]["url"], "images/OpenAtlas/banniere/") === 0) {
                $blockCms["imageHero"]["url"] = Yii::app()->getModule(Costum::MODULE)->getAssetsUrl() . "/" .$blockCms["imageHero"]["url"];
            } ?>
            <div class="hero-image"><img class="imageHero" src="<?= $blockCms["imageHero"]["url"] ?>"></div>
        </div>

        <button class="scroll-down-btn">↓</button>

        <?php 
            if ($blockCms["slideData"] && count($blockCms["slideData"]) > 1) { ?>
            <div class="slider-nav">
                <button class="prev-btn btnNav" id="prevSlide"><i class="fa fa-chevron-left"></i></button>
                <button class="next-btn btnNav" id="nextSlide"><i class="fa fa-chevron-right"></i></button>
            </div>

            <div class="dots-container-banner-content" id="dots-container-banner"></div>
        <?php
            }
        ?>
        <div class="sep-dual sep-gray"></div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var styleCss="";
        styleCss += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#banslide-<?= $kunik ?>").append(styleCss);
        var slideData = <?= json_encode($blockCms["slideData"]) ?>;

        function arrayReplaceRecursive(obj1, obj2) {
            let newObj = $.extend(true, {}, obj1);
            $.each(obj2, function(key, value) {
                if ($.isPlainObject(value) && newObj.hasOwnProperty(key)) {
                    newObj[key] = arrayReplaceRecursive(newObj[key], value);
                } else if (!newObj.hasOwnProperty(key)) {
                    newObj[key] = value;
                }
            });
            
            return newObj;
        }

        function mergeDataChangeAndDefaultData(target, source) {
            const result = {};
            $.each(target, function(key, value) {
                if (typeof value["key"] != "undefined" && source.hasOwnProperty(value["key"])) {
                    result[key] = arrayReplaceRecursive(target[key], source[value["key"]]);
                } else {
                    result[key] = target[key];
                    const language = (typeof costum.language != "undefined") ? costum.language : "fr";
                    jsonHelper.setValueByPath(result[key], "title."+language, "LOREM <strong>IPSUM</strong>");
                    jsonHelper.setValueByPath(result[key], "description."+language, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s");
                    jsonHelper.setValueByPath(result[key], "button.text."+language, "Boutton");
                }
                result[key]["key"] = key;
            });
            return result;
        }

        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?>;
            var banslideInput = {
                configTabs : {
                    general : {
                        inputsConfig : [
                            {
                                type : "inputSimple",
                                options : {
                                    type : "checkbox",
                                    name : "autoplay",
                                    label : tradCms.autoplay,
                                    class : "switch"
                                }
                            },
                            {
                                type: "number",
                                options: {
                                    name: "interval",
                                    label: "<?= Yii::t("cms", "Delay between slides (in milliseconds)") ?>",
                                    filterValue: cssHelpers.form.rules.checkLengthProperties
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "imageHero",
                                    label: "<?= Yii::t("cms", "Main image") ?>",
                                    showInDefault: true,
                                    inputs: [
                                        {
                                            type: "inputFileImage",
                                            options: {
                                                name: "url",
                                                label: "<?= Yii::t("cms", "Choose image") ?>",
                                                collection: "cms"
                                            }
                                        },
                                        {
                                            type: "inputNumberRange",
                                            options: {
                                                name: "width",
                                                units: ["px", "%", "vw"],
                                                responsive: true,
                                                defaultZero: "auto",
                                                filterValue: cssHelpers.form.rules.checkLengthProperties,
                                                defaultValue: "<?= (isset($blockCms["css"]) && isset($blockCms["css"]["imageHero"]) && isset($blockCms["css"]["imageHero"]["width"])) ? $blockCms["css"]["imageHero"]["width"] : "" ?>"
                                            }
                                        },
                                        {
                                            type: "inputNumberRange",
                                            options: {
                                                name: "height",
                                                units: ["px", "%", "vh"],
                                                defaultZero: "max-content",
                                                responsive: true,
                                                filterValue: cssHelpers.form.rules.checkLengthProperties,
                                                defaultValue: "<?= (isset($blockCms["css"]) && isset($blockCms["css"]["imageHero"]) && isset($blockCms["css"]["imageHero"]["height"])) ? $blockCms["css"]["imageHero"]["height"] : "" ?>"
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                type: "inputMultiple",
                                options: {
                                    label: "<?= Yii::t("cms", "Slide contents") ?>",
                                    name: "slideData",
                                    class: "input-section-slideData",
                                    inputs: [
                                        {
                                            type: "inputSimple",
                                            options: {
                                                name: "hideTitle",
                                                label: "<?= Yii::t("cms", "Hide slide title") ?>",
                                                inputType: "checkbox",
                                                class: "switch"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                name: "hideDescription",
                                                label: "<?= Yii::t("cms", "Hide slide description") ?>",
                                                inputType: "checkbox",
                                                class: "switch"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                name: "hideBtn",
                                                label: "<?= Yii::t("cms", "Hide slide button") ?>",
                                                inputType: "checkbox",
                                                class: "switch"
                                            }
                                        },
                                        {
                                            type: "inputFileImage",
                                            options: {
                                                name: "slideBackground",
                                                label: "<?= Yii::t("cms", "Background image") ?>",
                                                collection: "cms"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                name: "key",
                                                class: "hidden"
                                            }
                                        },
                                        {
                                            type: "section",
                                            options: {
                                                name: "button",
                                                label: "<?= Yii::t("cms", "Button") ?>",
                                                showInDefault: true,
                                                inputs: [
                                                    {
                                                        type: "inputWithSelect",
                                                        options: {
                                                            name: "link",
                                                            label: "<?= Yii::t("cms", "Button link") ?>",
                                                            viewPreview : true,
                                                            class: "input-select-internal-link",
                                                            configForSelect2: {
                                                                maximumSelectionSize: 1,
                                                                tags: Object.keys(costum.app)
                                                            }
                                                        }
                                                    },
                                                    {
                                                        type: "inputSimple",
                                                        options: {
                                                            name: "targetBlank",
                                                            label: tradCms.openInNewTab,
                                                            inputType: "checkbox",
                                                            class: "switch"
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            }
                        ],
                        processValue: {
                            slideData: function(value) {
                                var valueChanged = {};
                                var valueToSave = {};
                                let count = 0;
                                $.each(value, function(key, value) {
                                    valueChanged["slide"+(key+1)] = value;
                                    count++;
                                });
                                if (value.length == count) {
                                    valueToSave = mergeDataChangeAndDefaultData(valueChanged, cmsConstructor.sp_params[cmsConstructor.spId]["slideData"]);
                                }
                                return valueToSave;
                            }
                        }
                    },
                    style: {
                        inputsConfig : [
                            "addCommonConfig",
                            {
                                type: "section",
                                options: {
                                    name: "btnSlide",
                                    label: "<?= Yii::t("cms", "Slide button") ?>",
                                    inputs: [
                                        "color",
                                        "backgroundColor",
                                        "margin",
                                        "padding",
                                        "borderRadius",
                                        "border",
                                        "boxShadow",
                                        "width",
                                        "height"
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "btnNav",
                                    label: "<?= Yii::t("cms", "Navigation button") ?>",
                                    inputs: [
                                        "color",
                                        "backgroundColor",
                                        "margin",
                                        "padding",
                                        "borderRadius",
                                        "border",
                                        "boxShadow",
                                        "width",
                                        "height"
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "dotBanner",
                                    label: "<?= Yii::t("cms", "Indicator button") ?>",
                                    inputs: [
                                        "color",
                                        "backgroundColor",
                                        "margin",
                                        "padding",
                                        "borderRadius",
                                        "border",
                                        "boxShadow",
                                        "width",
                                        "height"
                                    ]
                                }
                            }
                        ]
                    },
                    hover: {
                        inputsConfig : [
                            "addCommonConfig",
                            {
                                type: "section",
                                options: {
                                    name: "btnSlide",
                                    label: "<?= Yii::t("cms", "Slide button") ?>",
                                    inputs: [
                                        "color",
                                        "backgroundColor",
                                        "margin",
                                        "padding",
                                        "borderRadius",
                                        "border",
                                        "boxShadow",
                                        "width",
                                        "height"
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "btnNav",
                                    label: "<?= Yii::t("cms", "Navigation button") ?>",
                                    inputs: [
                                        "color",
                                        "backgroundColor",
                                        "margin",
                                        "padding",
                                        "borderRadius",
                                        "border",
                                        "boxShadow",
                                        "width",
                                        "height"
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "dotBanner",
                                    label: "<?= Yii::t("cms", "Indicator button") ?>",
                                    inputs: [
                                        "color",
                                        "backgroundColor",
                                        "margin",
                                        "padding",
                                        "borderRadius",
                                        "border",
                                        "boxShadow",
                                        "width",
                                        "height"
                                    ]
                                }
                            }
                        ]
                    },
                    advanced : {
                        inputsConfig : [
                            "addCommonConfig"
                        ]
                    },
                },
                onchange: function(path,valueToSet,name,payload,value,id) {
                    if (name == "url") {
                        $(".imageHero").attr("src", value);
                    }
                },
                afterSave: function(path,valueToSet,name,payload,value,id){
                    if (name == "slideData") {
                        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                    }
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "refreshBlock", 
                        data:{ id: id}
                    })
                }
            }
            cmsConstructor.blocks["<?= $kunik ?>"] =  banslideInput;
        }

        gsap.registerPlugin(ScrollTrigger);

        const slides = $('.banner-block-<?= $kunik ?> .slide');
        const dotsContainer = $('.banner-block-<?= $kunik ?> #dots-container-banner');
        let currentSlide = 0;
        const totalSlides = slides.length;

        function showSlide(index) {
            const prevSlide = slides.eq(currentSlide);
            const nextSlide = slides.eq(index);

            gsap.to(prevSlide, {
                opacity: 0,
                duration: 1,
                onComplete: () => prevSlide.css('z-index', 10)
            });

            nextSlide.css('z-index', 10);
            gsap.to(nextSlide, {
                opacity: 1,
                duration: 1
            });

            const title = nextSlide.find('.slide-title');
            gsap.fromTo(title, {
                y: -50,
                opacity: 0
            }, {
                y: 0,
                opacity: 1,
                duration: 1,
                ease: 'power3.out',
                delay: 0.2
            });

            const description = nextSlide.find('.slide-description');
            gsap.fromTo(description, {
                y: 50,
                opacity: 0
            }, {
                y: 0,
                opacity: 1,
                duration: 1,
                ease: 'power3.out',
                delay: 0.4
            });

            const button = nextSlide.find('.slide-btn');
            gsap.fromTo(button, {
                scale: 0.8,
                opacity: 0
            }, {
                scale: 1,
                opacity: 1,
                duration: 0.8,
                ease: 'power2.out',
                delay: 0.6,
                onComplete: () => {
                    if (costum.editMode) {
                        button.css("cursor", "text");
                    } else
                        button.css("cursor", "pointer");
                }
            });

            var slideDataKey = "slide"+ (index + 1);
            var slideData = <?= json_encode($blockCms["slideData"]) ?>;
            var backgroundImageSlide = "";
            if (notEmpty(slideData) && typeof slideData[slideDataKey] != "undefined" && typeof slideData[slideDataKey]["slideBackground"] != "undefined") {
                backgroundImageSlide = slideData[slideDataKey]["slideBackground"];
            }
            $(".banner-block-<?= $kunik ?> .slider-container").removeClass(function(index, className) {
                return (className.match(/\bback-container-slie\S*/g) || []).join(' ');
            }).addClass("back-container-slide"+nextSlide.data("slide")).css("background-image", "url("+backgroundImageSlide+")");

            updateDots(index);

            currentSlide = index;
        }

        function nextSlide() {
            const nextIndex = (currentSlide + 1) % totalSlides;
            showSlide(nextIndex);
        }

        function prevSlide() {
            const prevIndex = (currentSlide - 1 + totalSlides) % totalSlides;
            showSlide(prevIndex);
        }

        function updateDots(index) {
            const dots = $('.banner-block-<?= $kunik ?> .dot-banner');
            dots.each(function(i) {
                $(this).toggleClass('active', i === index);
            });
        }

        showSlide(0);

        if (!costum.editMode) {
            const autoplay = <?= (isset($blockCms["autoplay"])) ?  json_encode($blockCms["autoplay"]) : true ?>;
            if (autoplay && Object.keys(slideData).length > 1) {
                setInterval(nextSlide, <?= (isset($blockCms["interval"])) ?  json_encode($blockCms["interval"]) : 8000 ?>);
            }
        }
            


        $(".banner-block-<?= $kunik ?> .scroll-down-btn").on("click", function(e) {
            e.stopPropagation();
            $('html, body').animate({
                scrollTop: $(document).height()
            }, 800); 
        })


        function generateDots() {
            dotsContainer.empty();
            slides.each(function(i) {
                const dot = $('<div class="dot-banner dotBanner"></div>');
                dot.text((i + 1).toString().padStart(2, '0'));
                dot.on('click', () => showSlide(i));
                dotsContainer.append(dot);
            });
        }
        generateDots();

        $('.banner-block-<?= $kunik ?> #nextSlide').on('click', nextSlide);
        $('.banner-block-<?= $kunik ?> #prevSlide').on('click', prevSlide);
    });
</script>