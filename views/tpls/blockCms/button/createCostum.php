<?php 
    $keyTpl = "createCostum";
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss]; 
    $cssAndScriptFilesModule = array( 
        '/js/default/profilSocial.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
   //echo $this->renderPartial("co2.views.element.firstStepCostum", array("element"=>$element));
?>
<style id="css-<?= $kunik ?>">
    .<?= $kunik?> .createCostum{
        display: block;
        width: 300px;
        text-align: center;
        border-radius: 5px;
        height: 45px;
        line-height: 37px;
        text-decoration: none;
        border : 1px solid <?= $blockCms["css"]["bouton"]["borderColor"]; ?>;
        color: <?= $blockCms["css"]["bouton"]["color"]; ?>;
        font-size: 25px;
        background:<?= $blockCms["css"]["bouton"]["backgroundColor"]; ?>;
        transition:all .5s ease 0s
    }
</style>

<div class="<?= $kunik?>">
    <?php if(isset(Yii::app()->session['userId'])){?>
        <a href="javascript:;" class="createCostum"><?php echo $blockCms["label"] ?></a>
    <?php } ?>
</div>
<script>
    sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
   
    
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>blockCms;
        var createCostum = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type: "inputSimple",
                            options: {
                                name: "label",
                                label: "Label"
                            }
                        },
                    ]
                },
				style: {
					inputsConfig: [
						{
							type: "section",
							options: {
								name: "bouton",
								label: tradCms.button,
								inputs: [
									"backgroundColor",
									"color",
									"borderColor",
									"borderRadius"
								]
							}
						}
					]
				},
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            },
        }
        cmsConstructor.blocks.createCostum<?= $myCmsId ?> = createCostum;
    }
    jQuery(document).ready(function() {
        $(".createCostum").click(function(){
            costumObj.createCostum()
        })

    })
</script>