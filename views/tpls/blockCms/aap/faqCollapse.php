<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "faqCollapse";
?>

<style id="css-<?= $kunik ?>">
.faq-collapse-content.cs-<?= $kunik ?> {
    --dtl-costum-color1: <?= isset($costum["css"]["color"]["color1"]) ? $costum["css"]["color"]["color1"] : "#43c9b7" ?>;
    --dtl-costum-color2: <?= isset($costum["css"]["color"]["color2"]) ? $costum["css"]["color"]["color2"] : "#4623c9" ?>;
    --dtl-costum-color3: <?= isset($costum["css"]["color"]["color3"]) ? $costum["css"]["color"]["color3"] : "#ffc9cb" ?>;
}
.faq-collapse-content.cs-<?= $kunik ?> {
    display: flex;
    position: relative;
    width: 100%;
}

.faq-collapse-content.cs-<?= $kunik ?> .panel-default {
    border-radius: 15px;
}

.faq-collapse-content.cs-<?= $kunik ?> .panel-heading {
    display: flex;
    justify-content: space-between;
    align-items: center;
    background-color: transparent;
    padding: 1% 2%;
    border-top-left-radius: 15px;
    border-top-right-radius: 15px;
    transition: border 0.3s ease;
}
.faq-collapse-content.cs-<?= $kunik ?> .panel-body {
    padding: 0 2%;
}
.faq-collapse-content.cs-<?= $kunik ?> .panel-heading .panel-title {
    font-size: 20px;
    font-weight: 700;
    line-height: 1.5;
    width: 90%;
    text-transform: none;
    text-align: left;
}
.faq-collapse-content.cs-<?= $kunik ?> .panel-heading span {
    font-size: 45px;
    text-align: center;
    width: 10%;
    cursor: pointer;
}

.faq-collapse-content.cs-<?= $kunik ?> .panel-collapse.in {
    border: 5px solid var(--dtl-costum-color2);
    border-bottom-left-radius: 15px;
    border-bottom-right-radius: 15px;
    border-top: none !important;
}

.faq-collapse-content.cs-<?= $kunik ?> .panel-heading .chevron i {
    transition: all 0.6s ease;
}
.faq-collapse-content.cs-<?= $kunik ?> .panel-heading.active-collapse .chevron i {
    transform: rotate(180deg);
    color: #4623c9;
}
.faq-collapse-content.cs-<?= $kunik ?> .panel-collapse.collapse {
    transition: border 0.3s ease;
}
.faq-collapse-content.cs-<?= $kunik ?> .panel-body .collapse-desc {
    font-size: 20px;
    line-height: 1.5;
}

.faq-collapse-content.cs-<?= $kunik ?> .panel-body .know-content {
    margin : 20px 0 0 2px;
}
.faq-collapse-content.cs-<?= $kunik ?> .panel-body .know-content a {
    font-size: 22px;
    padding: 10px 30px;
    border: 8px solid #e50051;
    color: #fff;
    background-color: #FF286B;
    border-radius: 30px;
}
.faq-collapse-content.cs-<?= $kunik ?> .panel-body .know-content a:hover {
    text-decoration: none;
}
.active-collapse {
    border: 5px solid var(--dtl-costum-color2) !important;
    border-bottom: none !important;
}
@media (max-width : 765px) {
    .faq-collapse-content.cs-<?= $kunik ?> .panel-heading .panel-title {
        font-size: 14px;
    }
    .faq-collapse-content.cs-<?= $kunik ?> .panel-body .collapse-desc {
        font-size: 14px !important;
    }
}
</style>

<div class="faq-collapse-content cs-<?= $kunik ?> <?= $kunik ?> <?= $kunik ?>-css">
    <div class="panel-group col-xs-12 bs-px-0" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading" id="panelHeading<?= $blockKey ?>">
                <div class="panel-title sp-text title0<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title0" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $blockKey ?>">

                </div>
                <span class="hidden-xs chevron" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $blockKey ?>"><i class="fa fa-chevron-down"></i></span>
            </div>
            <div id="collapse<?= $blockKey ?>" class="panel-collapse collapse">
                <div class="panel-body">
                    <span class="collapse-desc sp-text text<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text">

                    </span>
                    <div class="know-content">
                        <!-- <a href="<?= $blockCms["link"] ?>" class="btn-know-more">En savoir plus</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        $(".faq-collapse-content.cs-<?= $kunik ?> .panel-heading span").css("cursor","text");
        $(".faq-collapse-content .panel-collapse").addClass("in")
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var faqCollapse = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : 'color',
                            options : {
                                label : 'Couleur du bordure',
                                name : 'borderColor',
                            }
                        },
                        {
							type: "inputWithSelect",
							options: {
								label: "Lien du bouton voir plus",
								name: "link",
								configForSelect2: {
									maximumSelectionSize: 1,
									tags: Object.keys(costum.app)
								}
							}
						},
                    ]
                },
                style: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                },
                advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.faqCollapse<?= $myCmsId ?> = faqCollapse;
    }
    appendTextLangBased(".text<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["text"]) ?>,"<?= $blockKey ?>");
    appendTextLangBased(".title0<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["title0"]) ?>,"<?= $blockKey ?>");

</script>
<script>
    $(document).ready(function(){
        $('#panelHeading<?= $blockKey ?> ').click(function(){
            var collapse = $('#collapse<?= $blockKey ?>')
            if(collapse.hasClass('in')) {
                $(this).removeClass('active-collapse')
            } else {
                $(this).addClass('active-collapse')
            }
        })
    })
</script>