<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "campCoFinancForm";
    $assets = [
        '/js/aap/campCoFinancForm.js',
    ];
    HtmlHelper::registerCssAndScriptsFiles($assets, Yii::app()->getModule('costum')->assetsUrl);
?>

<style id="css-<?= $kunik ?>">
    .co-financ-form-content {
        padding: 100px 80px;
        background-color: #4623C9;
        width: 100%;
        display: block;
        position: relative;
    }
    .co-financ-form-content .form-title {
        font-size: 50px;
        display: block;
        position: relative;
        color: #43C9B7;
        font-weight: 500;
        margin-bottom: 50px;
    }

    .co-financ-form-content .form-input-content .form-group {
        margin-bottom: 25px;
    }
    .co-financ-form-content .form-input-content label{
        color: #fff;
        font-size: 22px;
        font-weight: 200;
    }

    .co-financ-form-content .form-input-content input {
        border-color: #fff;
        background-color: #fff;
        color: #000;
        padding: 12px;
        font-size: 22px;
        height: 40px;
    }

    .co-financ-form-content .form-check-content {
        color: #fff;
        font-size: 22px;
        margin: 20px 0 0;
    }

    .co-financ-form-content .form-check-content div {
        margin-bottom: 30px;
    }

    .co-financ-form-content .form-check-content .check-radio {
        margin: 0 4px 0 10px;
        transform: scale(1.5);
        appearance: none;
        -webkit-appearance: none;
        border-radius: 3px;
        border: 1px solid #fff;
        position: relative;
        width: 15px;
        height: 15px;
        background-color: #fff;
    }

    .co-financ-form-content .form-check-content .check-radio:checked::after {
        content: '';
        background-color: #FF286B;
        font-size: 16px;
        position: absolute;
        top: 51%;
        left: 51%;
        transform: translate(-50%, -50%);
        font-weight: 600;
        width: 12px;
        height: 12px;
        border-radius: 50%;
    }

    .co-financ-form-content .form-check-content .montant-double {
        color: #FF286B;
        font-weight: 600;
    }
    .co-financ-form-content .form-check-content .sign-check {
        margin: 0 10px 0 0;
        transform: scale(1.5);
        appearance: none;
        -webkit-appearance: none;
        border-radius: 3px;
        border: 1px solid #fff;
        position: relative;
        width: 15px;
        height: 15px;
        background-color: #fff;
    }

    .co-financ-form-content .form-check-content .sign-check:checked::after {
        content: '';
        background-color: #FF286B;
        font-size: 16px;
        position: absolute;
        top: 51%;
        left: 51%;
        transform: translate(-50%, -50%);
        font-weight: 600;
        width: 12px;
        height: 12px;
        border-radius: 50%;
    }
    .form-input-content .coFormbody.coform-stepbody {
        margin-top: 0px !important;
    }
    .form-input-content .coforminput.custom-font label h4 {
        color: #fff !important;
        text-transform: none !important;
    }
    .form-input-content .coforminput.custom-font .sectionDescri p {
        font-size: 2.1rem !important;
        color: #fff !important;
    }
    .form-input-content .coforminput.custom-font .sectionDescri p strong {
        color: #FF286B;
    }
    .form-input-content .coforminput.custom-width {
        width: 150%;
    }
    .form-input-content .coforminput.move-left div.col-xs-12 {
        padding: 0 !important;
    }
    .form-input-content .coforminput.display-flex .form-check {
        display: flex !important;
    }
    .form-input-content .coforminput.display-flex .form-check label.form-check-label {
        margin-left: 0 !important;
    }
    .form-input-content .coforminput.display-flex .form-check label.form-check-label h4,
    .form-input-content .coforminput.display-flex .form-check label.form-check-label h4 {
        font-weight: normal !important;
        font-family: 'customFontfontfranceTierslieuxPublicSans-Regular.ttf';
        font-size: 22px;
    }
    .form-input-content .coforminput.display-flex .form-check label {
        margin-left: .8rem;
        top: .7rem;
    }
    .form-input-content .coforminput.display-flex .form-check label span,
    .form-input-content .coforminput .form-check label span {
        transform: scale(1.5);
        appearance: none;
        -webkit-appearance: none;
        border-radius: 1px;
        border: 1px solid #fff;
        position: absolute;
        display: block;
        width: 15px;
        height: 15px;
        background-color: #fff;
        top: .8rem;
        left: .8rem;
        transition: all .3s;
    }
    .form-input-content .coforminput.display-flex .form-check label span::after,
    .form-input-content .coforminput .form-check label span::after {
        content: 'X';
        transition: all .3s;
        font-size: 2rem;
        color: #575353;
        position: relative;
        top: -.9rem;
        left: -.25rem;
        padding: 1px;
        font-weight: 300;
        border: none;
        transform: scale(1.5, 1.1);
        background-color: transparent;
        font-family: system-ui !important;
    }
    .form-input-content .coforminput.move-check-left .form-check label span::after {
        left: -.35rem !important;
    }
    .form-input-content .coforminput.error-msg {
        border: none !important;
    }
    .form-input-content .coforminput.error-msg::after {
        content: '';
    }

</style>

<div class="co-financ-form-content">
    <div class="row">
        <div class="col-lg-7">
            <div class="form-title">
                <span><?= $blockCms["title"] ?></span>
            </div>
            <div class="form-input-content" id="formCampagne<?= $kunik ?>">
                <div class="form-group">
                    <label for="usr">Nom</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="usr">Prénom</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="usr">Mail</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="usr">Structure</label>
                    <input type="text" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl.'/images/aap/lesCommuns/epingle.png';?>" alt="">
        </div>
    </div>
    <!-- <div class="form-check-content">
        <div>
            <span>Je m’engage à contribuer à hauteur de</span>
            <span><input type="radio" class="check-radio" name="optradio"></span>
            <span>100 <i class="fa fa-eur"></i></span>
            <span><input type="radio" class="check-radio" name="optradio"></span>
            <span>300 <i class="fa fa-eur"></i></span>
            <span><input type="radio" class="check-radio" name="optradio"></span>
            <span>500 <i class="fa fa-eur"></i></span>
        </div>
        <div>
            <span>Vous obtiendrez alors </span> <span class="montant-double">600 <i class="fa fa-eur"></i></span> <span> que vous pourrez attribuer aux communs de votre choix !</span>
        </div>
        <div>
            <span><input type="checkbox" class="sign-check" /></span> <span> Je m’engage à participer à la phase de coﬁnancement qui se tiendra entre
            octobre et décembre 2024.</span>
        </div>
    </div> -->
</div>

<script type="text/javascript">
    (function(thisWindwow) {
        var php = {
            kunik: '<?= $kunik ?>',
            my_cms_id: '<?= $myCmsId ?>',
            block_cms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>)),
            style_css: JSON.parse(JSON.stringify(<?= json_encode($styleCss) ?>)),
            allFormsContext: JSON.parse(JSON.stringify(<?= json_encode($allFormsContext) ?>))
        }
        sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
        var str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#css-<?= $kunik ?>").append(str);
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
            var campCoFinancForm = {
                configTabs: {
                    general: {
                        inputsConfig: [
                            {
                                type : "inputSimple",
                                options : {
                                    name : "title",
                                    label : tradCms.label,
                                    collection : "cms"
                                }
                            },
                            {
                                type: "select",
                                options: {
                                    name: "form",
                                    label: 'Formulaire concerné',
                                    isSelect2: true,
                                    options: $.map(php.allFormsContext, function(val, key) {
                                        return {
                                            value: key,
                                            label: (val.name ? ucfirst(val.name.toLocaleLowerCase()) : "Pas de nom") + (val.type ? ` (${val.type})` : "")
                                        };
                                    })
                                }
                            },
                            // {
                            //     type: "select",
                            //     options: {
                            //         name: "form",
                            //         label: 'Etape à afficher',
                            //         isSelect2: true,
                            //         options: $.map(php.allFormsContext, function(val, key) {
                            //             return {
                            //                 value: key,
                            //                 label: (val.name ? ucfirst(val.name.toLocaleLowerCase()) : "Pas de nom") + (val.type ? ` (${val.type})` : "")
                            //             };
                            //         })
                            //     }
                            // },
                        ]
                    },
                    style: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    }
                },
                onchange: function(path, valueToSet, name, payload, value) {
                    if (path == "query" && name == "types" && valueToSet.indexOf("answers") < 0 && valueToSet.indexOf("projects") < 0) {

                    }
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            }
            cmsConstructor.blocks.campCoFinancForm<?= $myCmsId ?> = campCoFinancForm;
        }
        thisWindwow.campCoFinancForm.call(php);
    })(window);
</script>

