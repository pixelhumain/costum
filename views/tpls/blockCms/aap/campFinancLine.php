<?php 
    //Description des financement de la campagne
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "campFinancLine";
?>

<style>
    .financ-line-content {
        display: block;
        position: relative;
        padding: 20px 100px;
        width: 100%;
    }
    .financ-line-content .financ-line-title span {
        font-size: 40px;
        font-weight: 600;
        color: #43C9B7;
    }
    .financ-line-content .financ-line-desc {
        display: block;
        position: relative;
        width: 80%;
    }
    .financ-line-content .financ-line-desc .desc-title {
        position: relative;
        width: 100%;
        display: flex;
        align-items: center;
        font-weight: 600;
        font-size: 20px;
        padding: 25px 0 15px;
    }

    .financ-line-content .financ-line-desc .desc-motif {
        position: relative;
        width: 100%;
        display: flex;
        align-items: center;
        font-weight: 400;
        font-size: 20px;
        padding: 0 0 5px;
    }

    .financ-line-content .financ-line-desc .desc-line {
        flex-grow: 1;
        border-bottom: 2px dashed #000;
        margin: 0 10px;
    }
</style>

<div class="financ-line-content">
    <!--<div class="financ-line-title">
         <span><?php /*= $blockCms["title"] */?></span>
    </div>
    <div class="financ-line-desc">
        <div class="desc-title">
            <span>Payer l’hébergement de mon commun aﬁn qu’il soit accessible à tous</span>
            <span class="desc-line"></span>
            <span>2000 <i class="fa fa-eur"></i></span> 
        </div>
        <div class="desc-motif">
            <span>OVH</span>
            <span class="desc-line"></span>
            <span>1000 <i class="fa fa-eur"></i></span> 
        </div>
        <div class="desc-motif">
            <span>NOM DE DOMAINE</span>
            <span class="desc-line"></span>
            <span>1000 <i class="fa fa-eur"></i></span> 
        </div>
    </div>
    <div class="financ-line-desc">
        <div class="desc-title">
            <span>Dévelloper un nouvelle fonctionnalité sur mon commun</span>
            <span class="desc-line"></span>
            <span>2000 <i class="fa fa-eur"></i></span> 
        </div>
        <div class="desc-motif">
            <span>OVH</span>
            <span class="desc-line"></span>
            <span>1000 <i class="fa fa-eur"></i></span> 
        </div>
        <div class="desc-motif">
            <span>NOM DE DOMAINE</span>
            <span class="desc-line"></span>
            <span>1000 <i class="fa fa-eur"></i></span> 
        </div>
    </div>-->
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var campFinancLine = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputSimple",
                            options : {
                                name : "title",
                                label : tradCms.label,
                                collection : "cms"
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            
                        }
                    ]
                },
                advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.campFinancLine<?= $myCmsId ?> = campFinancLine;
    }
</script>

