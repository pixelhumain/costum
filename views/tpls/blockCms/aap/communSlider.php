<?php
//Description des financement de la campagne
$myCmsId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];
$keyTpl = "";
$files = [
	'/js/aap/aapv2.js'
];
HtmlHelper::registerCssAndScriptsFiles($files, Yii::app()->getModule('co2')->getAssetsUrl());
?>

<style id="css-<?= $kunik ?>">
	.commun-slider-content<?= $kunik ?> {
		--dtl-costum-color1: <?= isset($costum["css"]["color"]["color1"]) ? $costum["css"]["color"]["color1"] : "#43c9b7" ?>;
		--dtl-costum-color2: <?= isset($costum["css"]["color"]["color2"]) ? $costum["css"]["color"]["color2"] : "#4623c9" ?>;
		--dtl-costum-color3: <?= isset($costum["css"]["color"]["color3"]) ? $costum["css"]["color"]["color3"] : "#ffc9cb" ?>;
	}
	.commun-slider-content<?= $kunik ?> {
		display: block;
		position: relative;
		padding: 40px 10%;
		width: 100%;
		background-color: #D8F4F0;
	}

	.commun-slider-content<?= $kunik ?> .commun-slider-title .titleSlider {
		font-size: 40px;
		font-weight: 700;
		color: var(--dtl-costum-color1);
		margin-bottom: 20px;
	}

	.<?= $kunik ?> .swiper {
      width: 100%;
      padding: 20px 0;
	  position: relative;
	  overflow: hidden;
    }

    .<?= $kunik ?> .swiper-slide {
      display: flex;
      justify-content: center;
      align-items: center;
    }

	.commun-slider-content<?= $kunik ?> .swiper-wrapper {
		margin-bottom: 30px;
	}

	.commun-slider-content<?= $kunik ?> .swiper-pagination-bullet {
		width: 15px;
		height: 15px;
	}
	.aac-card<?= $kunik ?> {
		border: var(--dtl-costum-color1) 1px solid;
		border-radius: 10px;
		display: block;
		position: relative;
		padding: 0;
		overflow: hidden;
		width: 100%;
		flex-shrink: 0;
	}

	.aac-card<?= $kunik ?> .commun-card {
		width: 100%;
		background-color: #fff;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-image {
		display: block;
		position: relative;
		background-color: white;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-image img {
		width: 100%;
		height: 160px;
		border-radius: 10px;
		object-fit: contain;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body {
		padding: 25px;
		display: block;
		position: relative;
		background-color: #fff;
		min-height: 33rem;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-title {
		padding: 0;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-title>* {
		color: var(--dtl-costum-color1);
		font-size: 20px;
		font-weight: 500;
		display: -webkit-box;
		-webkit-line-clamp: 3;  /* Limite à 2 lignes */
		-webkit-box-orient: vertical;
		overflow: hidden;
		text-overflow: ellipsis;
		line-height: 1.5;
		margin-bottom: 5px;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-desc {
		color: #000;
		font-size: 14px;
		font-weight: 400;
		/* height: 100px; */
		width: 100%;
		display: -webkit-box;
		-webkit-line-clamp: 5;  /* Limite à 2 lignes */
		-webkit-box-orient: vertical;
		overflow: hidden;
		text-overflow: ellipsis;
		cursor: pointer;
	}
	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-desc a {
		text-decoration: none;
	} 
	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-desc a:hover {
		text-decoration: none;
	} 

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-tags .tags {
		padding: 2px 3px;
		border: 1px solid var(--dtl-costum-color3);
		border-radius: 8px;
		font-size: 14px;
		color: #fff;
		background-color: var(--dtl-costum-color3);
		margin: 0 2px 0 0;
		display: inline-block;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-collect {
		padding: 5px 0 15px;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-progress {
		padding: 15px 0 0;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-progress .progress .progress-bar {
		background-color: var(--dtl-costum-color1);
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-collect .collect-text {
		font-size: 18px;
		font-weight: 600;
		color: #494646;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-collect .collect-montant {
		font-size: 18px;
		font-weight: 600;
		color: grey;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-footer {
		display: flex;
		justify-content: space-between;
		align-items: center;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-footer .footer-text {
		font-size: 16px;
		font-weight: 400;
		color: var(--dtl-costum-color1);
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-footer .footer-text span {
		display: block;
	}

	.aac-card<?= $kunik ?> .commun-card .commun-card-body .commun-footer .footer-btn button {
		font-size: 20px;
		color: var(--dtl-costum-color1);
		border: 3px var(--dtl-costum-color1) solid;
		background-color: transparent;
		border-radius: 50%;
		padding: 5px 10px;
	}

	.commun-slider-content<?= $kunik ?> .btn-all-content {
		display: flex;
		position: relative;
		padding: 20px 9px;
		justify-content: center;
	}

	.commun-slider-content<?= $kunik ?> .btn-all-content .btn-all-commun {
		padding: 10px 28px;
		border-radius: 15px;
		border: 1px solid var(--dtl-costum-color1);
		background-color: var(--dtl-costum-color1);
		color: #fff;
		transition: all .4s;
		font-size: 22px;
		font-weight: 600;
	}
	.commun-slider-content<?= $kunik ?> .btn-all-content .btn-all-commun:hover {
		text-decoration: none;
	}
	@media (max-width : 550px) {
		.commun-slider-content<?= $kunik ?> .btn-all-content .btn-all-commun {
			font-size: 16px;
		}
	}
	@media (max-width: 767px) {
		.commun-slider-content<?= $kunik ?>  .commun-slider-title span {
			font-size: 22px !important;
		}
	}
	@media (max-width: 1400px) {
		.commun-slider-content<?= $kunik ?>  .dropdown-tags  {
			bottom: 34px !important;
		}
	}
	.commun-slider-content<?= $kunik ?>  .dropdown-tags {
		position: absolute;
		top: auto;
		bottom: 3px;
		min-width: 5px !important;
		right: auto;
		left: 8px;
		padding: 5px;
		box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
		z-index: 10;
		max-height: 250px;
		overflow-y: auto;
		overflow-x: hidden;
		transition: all 0.4s ease;
		display: none;
	}
	.commun-slider-content<?= $kunik ?>  .dropdown-tags.show{
		display: block !important;
	}
	.commun-slider-content<?= $kunik ?>  .dropdown-tags i{
		float: right;
		font-size: 15px;
		font-weight: 600;
		cursor: pointer;
		z-index: 10;
	}
	.commun-slider-content<?= $kunik ?>  .dropdown-tags .tags {
		display: block !important;
		margin: 3px 0;
		padding: 2px 3px;
		border: 1px solid var(--dtl-costum-color3);
		border-radius: 8px;
		font-size: 14px;
		color: #fff;
		background-color:var(--dtl-costum-color3)b;
		width: fit-content;
	}
	.commun-slider-content<?= $kunik ?>  .btn-more-tag {
		background-color: transparent;
		color: var(--dtl-costum-color3);
		border: 1pvar(--dtl-costum-color3) #ffc9cb;;
		width: 25px;
		height: 25px;
		text-align: center;
		border-radius: 50%;
		display: block;
	}
</style>

<?php if(isset($blockCms["typeSlide"]) && $blockCms["typeSlide"] == "randomCommun") { ?>
<div class="commun-slider-content<?= $kunik ?> <?= $kunik ?> <?= $kunik ?>-css">
	<div class="commun-slider-title">
		<!-- <span><?= @$blockCms["title"] ?></span> -->
		<div class="sp-text titleSlider titleSlider<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title10">

		</div>
	</div>
	<div class="swiper">
		<div class="swiper-wrapper">

		</div>

		<div class="swiper-pagination"></div>

		<!-- <div class="swiper-button-prev"></div>
		<div class="swiper-button-next"></div> -->

		<div class="swiper-scrollbar"></div>
	</div>
	<div class="btn-all-content">
		<a class="btn-all-commun" href="javascript:;">
			<!-- <?= @$blockCms["btnLabel"] ?> -->
			<div class="sp-text btnLabel btnLabel<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="btnLabel0">

			</div>
		</a>
	</div>
</div>

<?php } ?>

<?php if(isset($blockCms["typeSlide"]) && $blockCms["typeSlide"] == "randomAac") { ?>
  <style class="css-<?= $kunik ?>">
	@import url("https://fonts.googleapis.com/css2?family=Fira+Sans:wght@300;400;600&display=swap");
	:root {
		--primary-color: <?= isset($costum["css"]["color"]["color1"]) ? $costum["css"]["color"]["color1"] : "#37517e" ?>;
		--hover-color: #95abd0;
		--text-color: #fff;
	}
	.randomAac-<?= $kunik ?> {
		padding: 40px 10%;
		width: 100%;
	}
	.randomAac-<?= $kunik ?> .filters-content {
		flex-direction: column;
		padding: 20px;
	}

	.randomAac-<?= $kunik ?> .filters-content,
	.randomAac-<?= $kunik ?> .form-group-filters {
		width: 100%;
		position: relative;
		display: flex;
		align-items: flex-start;
	}

	.randomAac-<?= $kunik ?> .filters-content label {
		margin-bottom: 10px;
		text-transform: uppercase;
		font-weight: 600;
	}

	.randomAac-<?= $kunik ?> .form-group-filters input,
	.randomAac-<?= $kunik ?> .form-group-filters select {
		margin-right: 15px;
		border: 1px solid var(--primary-color) !important;
		height: 50px;
		margin-top: 5px;
		border-radius: 50px;
		box-shadow: none;
		width: inherit;
		font-weight: 500;
		font-size: 16px;
	}
	.randomAac-<?= $kunik ?> .metier-card-content {
		box-sizing: border-box;
		font-size: 100%;
		margin: 0;
	}

	.randomAac-<?= $kunik ?> .metier-card-content img {
		max-width: 100%;
		height: auto;
	}

	.randomAac-<?= $kunik ?> .metier-card-content {
		-webkit-text-size-adjust: 100%;
		font-variant-ligatures: none;
		text-rendering: optimizeLegibility;
		-moz-osx-font-smoothing: grayscale;
		-webkit-font-smoothing: antialiased;
		font-size: 100%;
		font-family: "Fira Sans", sans-serif;
		width: 100%;
	}

	.randomAac-<?= $kunik ?> .metier-card-content h1,
	.randomAac-<?= $kunik ?> .metier-card-content h2,
	.randomAac-<?= $kunik ?> .metier-card-content h3,
	.randomAac-<?= $kunik ?> .metier-card-content h4,
	.randomAac-<?= $kunik ?> .metier-card-content h5 {
		font-weight: 800;
		margin-top: 0;
		margin-bottom: 0;
	}


	.randomAac-<?= $kunik ?> .metier-data {
		margin-bottom: 20px;
	}
	.randomAac-<?= $kunik ?> .card-metier {
		width: 100%;
		height: 500px;
		position: relative;
		overflow: hidden;
		box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
		color: #1F1D42;
		border-radius: 20px;
	}
	
	.randomAac-<?= $kunik ?> .card-metier:hover .card-metier__content {
		background-color: var(--primary-color);
		bottom: 100%;
		transform: translateY(100%);
		padding: 50px 60px;
		transition: all 0.35s cubic-bezier(0.1, 0.72, 0.4, 0.97);
	}
	.randomAac-<?= $kunik ?> .card-metier:hover .card-metier__extra {
		transform: translateY(0);
		transition: transform 0.35s;
	}
	.randomAac-<?= $kunik ?> .card-metier:hover .card-metier__link {
		opacity: 1;
		transform: translate(-50%, 0);
		transition: all 0.3s 0.35s cubic-bezier(0.1, 0.72, 0.4, 0.97);
	}
	.randomAac-<?= $kunik ?> .card-metier:hover img {
		transform: scale(1);
		transition: 0.35s 0.1s transform cubic-bezier(0.1, 0.72, 0.4, 0.97);
	}
	.randomAac-<?= $kunik ?> .card-metier__content {
		width: 100%;
		text-align: center;
		background-color: var(--primary-color);
		padding: 0 40px 40px;
		position: absolute;
		bottom: 0;
		left: 0;
		transform: translateY(0);
		transition: all 0.35s 0.35s cubic-bezier(0.1, 0.72, 0.4, 0.97);
		will-change: bottom, background-color, transform, padding;
		z-index: 1;
	}
	.randomAac-<?= $kunik ?>  .card-metier__content::before, .randomAac-<?= $kunik ?>  .card-metier__content::after {
		content: "";
		width: 100%;
		height: 90px;
		background-color: inherit;
		position: absolute;
		left: 0;
		z-index: -1;
	}
	.randomAac-<?= $kunik ?>  .card-metier__content::before {
		top: -80px;
		-webkit-clip-path: ellipse(60% 80px at bottom center);
				clip-path: ellipse(60% 80px at bottom center);
	}
	.randomAac-<?= $kunik ?>  .card-metier__content::after {
		bottom: -80px;
		-webkit-clip-path: ellipse(60% 80px at top center);
				clip-path: ellipse(60% 80px at top center);
	}
	.randomAac-<?= $kunik ?>  .card-metier__title {
		font-size: 2rem !important;
		margin-bottom: 1em !important;
		text-transform: uppercase;
		font-weight: 600;
		text-align: center;
		color: var(--text-color) !important;
	}
	.randomAac-<?= $kunik ?>  .card-metier__title span {
		color: #2d7f0b;
	}
	.randomAac-<?= $kunik ?>  .card-metier__text {
		font-size: 15px !important;
		display: -webkit-box; 
		-webkit-line-clamp: 2; 
		-webkit-box-orient: vertical; 
		overflow: hidden;
		text-overflow: ellipsis;
	}
	.randomAac-<?= $kunik ?>  .card-metier__link {
		position: absolute;
		bottom: 1rem;
		left: 50%;
		transform: translate(-50%, 10%);
		display: flex;
		flex-wrap: wrap;
		align-items: center;
		text-decoration: none;
		color: #fff !important;
		font-weight: bolder;
		opacity: 0;
		padding: 10px;
		transition: all 0.35s;
		border-radius: 50px;
		cursor: pointer;
	}
	.randomAac-<?= $kunik ?>  .card-metier__link:hover svg {
		transform: translateX(4px);
	}
	.randomAac-<?= $kunik ?>  .card-metier__link svg {
		width: 18px;
		margin-left: 4px;
		transition: transform 0.3s;
	}
	.randomAac-<?= $kunik ?>  .card-metier__extra {
		height: 70%;
		position: absolute;
		display: flex;
		align-items: center;
		justify-content: center;
		flex-direction: column;
		width: 100%;
		font-size: 1.2rem;
		text-align: left;
		background-color: var(--text-color);
		padding: 50px;
		bottom: 0;
		z-index: 0;
		color: #dee8c2;
		transform: translateY(100%);
		will-change: transform;
		transition: transform 0.35s;
	}
	.randomAac-<?= $kunik ?>  .card-metier__extra span {
		color: #1F1D42;
		font-size: 13px;
		white-space: nowrap;      
		overflow: hidden;         
		text-overflow: ellipsis;
		width: 300px;             
		display: block;  

	}
	.randomAac-<?= $kunik ?>  .card-metier img {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		-o-object-fit: cover;
		object-fit: cover;
		-o-object-position: center;
		object-position: center;
		z-index: -1;
		transform: scale(1.2);
		transition: 0.35s 0.35s transform cubic-bezier(0.1, 0.72, 0.4, 0.97);
	}
	.randomAac-<?= $kunik ?>  .metier-code {
		position: absolute;
		top: 0;
		left: 0;
		background-color: var(--primary-color);
		color: #fff;
		padding: 10px 20px;
		font-size: 1.5rem;
		border-radius: 20px 0 20px 0;
	}
	.randomAac-<?= $kunik ?>  .metier-code span {
		color: #fff;
		font-weight: 600;
	}
	.randomAac-<?= $kunik ?>  .extra-footer {
		font-size: 1.1rem;
		color: var(--primary-color);
		position: absolute;
		bottom: 20px;
		left: 40px;
	}
</style>

<div class="randomAac-<?= $kunik ?> commun-slider-content<?= $kunik ?> <?= $kunik ?> <?= $kunik ?>-css">
	<div class="commun-slider-title">
		<span class="sp-text text<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text10">
			
		</span>
	</div>
	<div class="swiper">
		<div class="swiper-wrapper">

		</div>

		<div class="swiper-pagination"></div>


		<div class="swiper-scrollbar"></div>
	</div>
	<div class="btn-all-content">
		<a class="btn-all-commun" href="javascript:;">
			<div class="sp-text btnLabel btnLabel<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="btnLabel0">

			</div>
		</a>
	</div>

</div>

<?php } ?>

<script>
	var php = {
		kunik: '<?= $kunik ?>',
		my_cms_id: '<?= $myCmsId ?>',
		block_cms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>)),
		style_css: JSON.parse(JSON.stringify(<?= json_encode($styleCss) ?>)),
		aap: typeof aapObj === 'object' && aapObj ? aapObj : null,
		allFormsContext: JSON.parse(JSON.stringify(<?= json_encode($allFormsContext) ?>))
	}
	sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode($blockCms); ?>;
	var str = "";
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#css-<?= $kunik ?>").append(str);
	if(!costum.editMode && typeof sectionDyf.<?php echo $kunik ?>BlockCms['redirectPage'] != "undefined" && sectionDyf.<?php echo $kunik ?>BlockCms['redirectPage'] != "") {
		$('.commun-slider-content<?= $kunik ?> .btn-all-content .btn-all-commun').addClass("lbh")
		$('.commun-slider-content<?= $kunik ?> .btn-all-content .btn-all-commun').attr("href", sectionDyf.<?php echo $kunik ?>BlockCms['redirectPage'])
	}
	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
		var communSlider = {
			configTabs: {
				general: {
					inputsConfig: [
						{
							type: "inputSwitcher",
                            options: {
                                name: "typeSlide",
                                label: `Type de slide`,
                                class: "urlOptions ",
                                tabs: [
                                    {
                                        value: "randomCommun",
                                        label: "Les communs",
                                        inputs: [
                                            {
												type: "inputSimple",
												options: {
													name: "title",
													label: "Titre du block",
													collection: "cms"
												}
											},
											{
												type: "inputSimple",
												options: {
													name: "btnLabel",
													label: "Label du boutton",
													collection: "cms"
												}
											},
											{
												type: "inputWithSelect",
												options: {
													label: "Lien du bouton voir plus",
													name: "redirectPage",
													configForSelect2: {
														maximumSelectionSize: 1,
														tags: Object.keys(costum.app)
													}
												}
											},
											{
												type: "inputWithSelect",
												options: {
													label: "Page de détail",
													name: "communDetail",
													configForSelect2: {
														maximumSelectionSize: 1,
														tags: Object.keys(costum.app)
													}
												}
											},
											{
												type: "select",
												options: {
													name: "form",
													label: 'Formulaire concerné',
													isSelect2: true,
													options: $.map(php.allFormsContext, function(val, key) {
														return {
															value: key,
															label: (val.name ? ucfirst(val.name.toLocaleLowerCase()) : "Pas de nom") + (val.type ? ` (${val.type})` : "")
														};
													})
												}
											},
											{
												type: "section",
												options: {
													name: "filtersMoreOptions",
													label: "Options de filtres",
													class: "filtersMoreOptions",
													showInDefault: false,
													inputs: [
														{
															type : "select",
															options : {
																name : "applyFor",
																label : "À appliquer pour",
																options : [
																	{
																		value : "all",
																		label : "toutes les utilisateurs"
																	},
																	{
																		value : "forAdmin", 
																		label : "administrateur et super admin"
																	},
																	{
																		value : "forUser",
																		label : "simple utilisateur"
																	}
																]
															}
														},
														{
															type: "textarea",
															options: {
																label: "Filters option",
																name: "filtersOption"
															}
														}
													]
												}
											}
                                        ]
                                    },
                                    {
										value: "randomAac",
                                        label: "Les appel à communs",
                                        inputs: [
											{
												type: "inputSimple",
												options: {
													name: "title",
													label: "Titre du block",
													collection: "cms"
												}
											},
										]
									}
                                ]
                            }
						},
						
					]
				},
				style: {
					inputsConfig: [
						 "addCommonConfig",
						 "backgroundColor"
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
			},
			afterSave: function(path, valueToSet, name, payload, value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
			}
		}
		cmsConstructor.blocks.communSlider<?= $myCmsId ?> = communSlider;
	}
	appendTextLangBased(".btnLabel<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($blockCms["btnLabel0"]) ?>, "<?= $blockKey ?>");
	appendTextLangBased(".titleSlider<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($blockCms["title10"]) ?>, "<?= $blockKey ?>");
</script>
<script type="text/javascript">
	$(function() {
		var aap = new aapv2();
		var swiper_doms = $('.commun-slider-content<?= $kunik ?> .swiper-slide');
		const slideCount = 10;
		if (php && php.block_cms && php.block_cms.form)
			aap.form(php.block_cms.form);

		var filtersParams = {};
		if (php.block_cms && php.block_cms?.filtersMoreOptions?.filtersOption) {
			var parsedStr = null;
			// Remplacer les variables dynamiques formaté en ${var} par leur valeur
			parsedStr = php.block_cms.filtersMoreOptions.filtersOption.replace(/\$\{([^}]+)\}/g, (match, p1) => {
				try {
					// Évaluer l'expression à l'intérieur de ${}
					return eval(p1);
				} catch (error) {
					mylog.error("Erreur lors de l'évaluation de l'expression : ", p1, error);
					return p1;
				}
			})
			var parsedObj = null;
			try {
				parsedObj = JSON.parse(parsedStr)
			} catch (e) {
				mylog.error("the current value is not a valid json", parsedStr)
			}
			if (parsedObj) {
				const addParsedObjToFilters = (parsedObject) => {
					$.each(parsedObject, function (parsedObjKey, parsedObjVal) {
						filtersParams[parsedObjKey] = parsedObjVal;
					})
				}
				var applyFor = "all"
				if (php.block_cms.filtersMoreOptions.applyFor) {
					applyFor = php.block_cms.filtersMoreOptions.applyFor
				}
				switch (applyFor) {
					case "forAdmin":
						if (isSuperAdmin || isInterfaceAdmin) {
							addParsedObjToFilters(parsedObj)
						}
						break;
					case "forUser":
						if (!isSuperAdmin && !isInterfaceAdmin) {
							addParsedObjToFilters(parsedObj)
						}
						break;
					default:
						addParsedObjToFilters(parsedObj)
						break;
				}
				
			}
		}

		function do_init_swiper(params = {}) {
			const swiper = new Swiper(".swiper", {
				// Optional parameters
				direction: "horizontal",
				loop: typeof params.loop != "undefined" ? params.loop : true,
				slidesPerView: 1,
				spaceBetween: 30,
				centeredSlides: true,
				breakpoints: {
					// when window width is >= 320px
					525: {
						slidesPerView: php.block_cms.typeSlide == "randomAac" ? 1 : 3,
						spaceBetween: 10
					},
						// when window width is <= 640px
					850: {
						slidesPerView: php.block_cms.typeSlide == "randomAac" ? 1 : 3,
						spaceBetween: 10
					},

					1400: {
						slidesPerView: php.block_cms.typeSlide == "randomAac" ? 3 : 5,
						spaceBetween: 6
					},
						// when window width is <= 768px
					1500: {
						slidesPerView: php.block_cms.typeSlide == "randomAac" ? 3 : 5,
						spaceBetween: 20
					},
						// when window width is <= 1024px
					1600: {
						slidesPerView: php.block_cms.typeSlide == "randomAac" ? 3 : 5,
						spaceBetween: 20
					}
				},
				// If we need pagination
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},

				// Navigation arrows
				// navigation: {
				// 	nextEl: ".swiper-button-next",
				// 	prevEl: ".swiper-button-prev",
				// },

				// And if we need scrollbar
				// scrollbar: {
				//     el: ".swiper-scrollbar",
				// },
				autoplay: {
					delay: 15000,
					disableOnInteraction: false
				},

			});
		}

		function do_reset_dom(ref) {
			ref.find('.commun-title span')
				.empty()
				.html('<i class="fa fa-spinner fa-spin"></i> Chargement');
			ref.find('.commun-desc')
				.empty()
				.html('<i class="fa fa-spinner fa-spin"></i> Chargement');
			ref.find('.commun-tags')
				.empty()
				.html('<i class="fa fa-spinner fa-spin"></i> Chargement');
		}

		function do_load_answer_data(ref, ans) {
			var redirect;

			redirect = location.hash;
			if (typeof php.block_cms.communDetail === 'string')
				redirect = aap.url().hash(php.block_cms.communDetail).dot('communId', ans.id).toString();
			ref.find('.commun-card-image img')
				.attr('src', ans.image);
			ref.find('.commun-title')
				.empty()
				.html('<a class="btn-know-more lbh tooltips" data-toggle="tooltip" data-placement="top" title="'+ans.titre+'" href="' + redirect + '">' + ans.titre + '</a>')
			ref.find('.commun-desc')
				.empty()
				.html('<a href="javascript:;" class="tooltips" data-toggle="tooltip" title="'+ans.shortDesc+'">'+ans.shortDesc+'</a>');
			ref.find('.commun-tags')
				.empty()
				.append(ans.tagsbesoin.map(tag => '<span class="tags text-ellipsis">' + tag + '</span>'));
			const tags = ref.find('.commun-tags .tags');
			const dropdown_tags = ref.find('.dropdown-tags')
			if(ans.tagsbesoin.length < 4) {
				dropdown_tags.remove();
				return;
			}
			const close = $('<i>')
				.addClass('fa fa-times')
				.on('click', function() {
					dropdown_tags.removeClass('show')
				})
			dropdown_tags.append(close)
			if (tags.length > 3) {
				var showMoreBtn = $('<button>')
					.addClass('btn-more-tag')
				var icon = '<i class="fa fa-ellipsis-h"></i>'
				showMoreBtn.append(icon)
				// ref.find('.commun-tags').append(showMoreBtn)
				tags.slice(3).each(function() {
					const tag = $(this);
					tag.hide(); 
					dropdown_tags.append(tag.clone().show()); 
				});

				showMoreBtn.on('click', function() {
					if(dropdown_tags.hasClass('show')) {
						dropdown_tags.removeClass('show')
						return;
					}
					dropdown_tags.addClass('show') 
				});
			}
		}
		
		if(php.block_cms.typeSlide == "randomAac") {
			
			aap.xhr()
				.fetch_aac()
				.then(function(resp) {
						$.each(resp.results, function(index, aac) {
							var parent = "";
							$.each(aac.parent, function(i , v) {
								parent = v.name;
							})
							$(".randomAac-<?= $kunik ?> .swiper .swiper-wrapper").append(`
								<div class="metier-data swiper-slide">
									<div class="card-metier">
										<div class="metier-code">Parent : <span>${parent}</span></div>
										<div class="card-metier__content">
										<h3 class="card-metier__title">
											${aac.name}
										</h3>
										<p class="card-metier__text"> ${aac.description} </p>
										<a href="${"#les-communs?searchbyparent="+index}" class="card-metier__link lbh">
											<span>En savoir plus</span>
											<svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
											<path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
											</svg>        
										</a>
										</div>
										<div class="card-metier__extra">
											<div class="extra-footer">
												<h4>Communs déposer : ${typeof aac.stat != "undefined" && typeof aac.stat.communs != "undefined" ? aac.stat.communs : 0}</h4>
												<h4>Participants : ${typeof aac.stat != "undefined" && typeof aac.stat.participants != "undefined" ? aac.stat.participants : 0}</h4>
												<h4>Collecter : ${typeof aac.stat != "undefined" && typeof aac.stat.collectedFond != "undefined" ? aac.stat.collectedFond+" €" : 0+" €"}</h4>
												<h4>Objectif : ${typeof aac.stat != "undefined" && typeof aac.stat.solicitedFund != "undefined" ? aac.stat.solicitedFund+" €" : 0+" €"}</h4>
											</div>
										</div>
										<img src="${""}" alt="">
									</div>
								</div>
							`);
						});
						do_init_swiper({
							loop: true
						});
						
				});
		} else {
			aap.xhr()
				.get_random_answer(slideCount, {filters: filtersParams})
				.then(function(resp) {
					if (resp.success) {
						$.each(resp.content, function(i, answer) {
							var swiperSlideHtml = `
								<div class="swiper-slide">
									<div class="aac-card<?= $kunik ?>">
										<div class="commun-card">
											<div class="commun-card-image">
												<img src="${ modules?.co2?.url }/images/thumbnail-default.jpg'; ?>" />
											</div>
											<div class="commun-card-body">
												<div class="commun-title">
													<span>
														Nom du commun
													</span>
												</div>
												<div class="commun-desc">
													<span>
														Lorem ipsum dolor sit amet,
														conseteturLorem ipsum dolor sit amet,
														conseteturLorem ipsum dolor sit amet,
														consetetur dfsdfsdf <br /> fdsfdsf
													</span>
												</div>
												<div class="dropdown-menu dropdown-tags"></div>
												<div class="commun-tags cs-scroll scroll-small">
													<span class="tags text-ellipsis">#tags</span>
													<span class="tags text-ellipsis">#tags</span>
													<span class="tags text-ellipsis">#tags</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							`
							const currentSlide = $(swiperSlideHtml).appendTo(`.commun-slider-content<?= $kunik ?>.${ php.kunik } .swiper .swiper-wrapper`)
							do_reset_dom(currentSlide)
							do_load_answer_data(currentSlide, answer);
							if (i == resp.content.length - 1) {
								do_init_swiper({
									loop: resp.content.length < 6 ? false : true
								});
							}
						});
						for (var i = resp.content.length + 1; i < swiper_doms.length; i++) {
							swiper_doms.eq(i).hide();
						}
						coInterface.bindLBHLinks();
					} else if (php.block_cms?._id) {
						$(`.cmsbuilder-block[data-id="${ php.block_cms._id.$id }"]`).hide();
					}
				});
		}
	});
</script>