<?php 
    //Description des financement de la campagne
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "footer";
    $blockCms["imageContent"]["image1"] = $blockCms["imageContent"]["image1"] ?: Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl.'/images/aap/lesCommuns/ftl.png';
    $blockCms["imageContent"]["image2"] = $blockCms["imageContent"]["image2"] ?: Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl.'/images/aap/lesCommuns/tlAN.png';
    $blockCms["imageContent"]["image3"] = $blockCms["imageContent"]["image3"] ?: Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl.'/images/aap/lesCommuns/FCTL.png';
    $blockCms["imageContent"]["image4"] = $blockCms["imageContent"]["image4"] ?: Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl.'/images/aap/lesCommuns/frademe.png';
    $blockCms["imageContent"]["image5"] = $blockCms["imageContent"]["image5"] ?: Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl.'/images/aap/lesCommuns/FRANCT.png';
?>
<style id="css-<?= $kunik ?>">
    .footer-content {
        background-color: #D8F4F0;
        display: block;
        position: relative;
        width: 100%;
    }
    .footer-content .footer-partener-logo {
        padding: 5% 8% 5%;
        display: block;
        width: 100%;
    }
    .footer-content .footer-partener-logo .part-logo-content {
        padding: 60px 0;
        display: flex;
        justify-content: space-between;
        width: 100%;
        overflow-x: auto;
        border-top: <?= $blockCms["css"]["fondImage"]["partBorder"] ?>;
        border-bottom: <?= $blockCms["css"]["fondImage"]["partBorder"] ?>;
    }
    .footer-content .footer-partener-logo .ftl img {
        width: auto;
        height: 100px;
    }
    .footer-content .footer-partener-logo .tlan img {
        width: auto;
        height: 100px;
    }
    .footer-content .footer-partener-logo .fctl img {
        width: auto;
        height: 100px;
    }
    .footer-content .footer-partener-logo .frademe img {
        width: auto;
        height: 100px;
    }
    .footer-content .footer-partener-logo .franct img {
        width: auto;
        height: 100px;
    }
    .link-content {
        padding: 5% 8% 8%;
    }

    .footer-signin {
        cursor: pointer;
        transition: all .4s ease;
    }
    .footer-signin:hover {
        color: #4623C9;
    }
    .link-content .link-nav-item span, .link-content .link-mention span {
        font-size: 18px;
        font-weight: 500;
        display: block;
        margin-bottom: 10px;
    }
    .link-content .link-nav-item a {
        display: block;
        font-size: 18px;
        width: fit-content;
        line-height: 1.8;
        font-weight: 500;
    }
    .link-content .link-mention {
        text-align: center;
    }
    .link-content .link-social {
        text-align: right;
    }
    .link-content .link-social a {
        font-size: 30px;
        margin-left: 15px;
        color: #43C9B7;
        text-decoration: none !important;
    }
    .modal-header.set-sticky {
		position: sticky;
		z-index: 100;
		background-color: #fff;
	}
	#dialogContentBody .modal-header .modal-title {
		text-transform: none !important;
	}
    @media (min-width: 768px) {
		#dialogContentBody .section-nav-body {
			width: 11vw;
		}	
	}
    @media (max-width : 1200px) {
        .link-content .link-mention {
            text-align: right;
            margin-bottom: 20px;
        }
        .link-content .link-social {
            text-align: right;
        }
    }
    @media (max-width : 990px) {
        .link-content .link-mention {
            text-align: left;
            margin-top: 10px;
            margin-bottom: 20px;
        }
        .link-content .link-social {
            text-align: left;
            padding-left: 2px;
        }
    }
    @media (max-width : 767px) {
        .link-content {
            padding: 0;
        }
    }
</style>
<?php 
$imgUrl = Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg';
if(isset($initFiles[0])){
    $images = $initFiles[0];
    if(!empty($images["imagePath"]) && Document::urlExists($baseUrl.$images["imagePath"]))
        $imgUrl = $baseUrl.$images["imagePath"];
    elseif (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
        $imgUrl = $baseUrl.$images["imageMediumPath"];
}

?>

<div class="footer-content <?= $kunik ?> <?= $kunik ?>-css">
    <?php if (isset($blockCms["showBtn"]) && ($blockCms["showBtn"] == true || $blockCms["showBtn"] == "true")) { ?>
    <div class="footer-partener-logo hidden-xs fondImage">
        <div class="part-logo-content">
            <div class="part-logo ftl">
                <img src="<?= $blockCms["imageContent"]["image1"] ?>" alt="">
            </div>
            <div class="part-logo tlan">
                <img src="<?= $blockCms["imageContent"]["image2"] ?>" alt="">
            </div>
            <div class="part-logo fctl">
                <img src="<?= $blockCms["imageContent"]["image3"] ?>" alt="">
            </div>
            <div class="part-logo frademe">
                <img src="<?= $blockCms["imageContent"]["image4"] ?>" alt="">
            </div>
            <div class="part-logo franct">
                <img src="<?= $blockCms["imageContent"]["image5"] ?>" alt="">
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="link-content row fondLink">
        <div class="link-nav-item col-lg-4 col-xs-12 col-md-6 col-lg-4">
            <!-- <span class="footer-signin" data-toggle="modal" data-target="#modalLogin">Se connecter</span> -->
            <?php
                $btnLabel = [
                    $blockCms["linkLabelContent"]["linkLabel1"] ?? "Accueil",
                    $blockCms["linkLabelContent"]["linkLabel2"] ?? "Les communs",
                    $blockCms["linkLabelContent"]["linkLabel3"] ?? "Déposer un commun",
                    $blockCms["linkLabelContent"]["linkLabel4"] ?? "Contribuer aux communs",
                    $blockCms["linkLabelContent"]["linkLabel5"] ?? "Campagne de cofinancement",
                    $blockCms["linkLabelContent"]["linkLabel6"] ?? "Qui sommes-nous ?",
                    $blockCms["linkLabelContent"]["linkLabel7"] ?? "FAQ",
                ];
                foreach ($btnLabel as $index => $btnLabelVal) {
                    if (isset($blockCms["btnRedirection"]["button".$index]) && isset($blockCms["btnRedirection"]["typeUrl".$index])) {
                        if ($blockCms["btnRedirection"]["typeUrl".$index] == "internalLink") {
                            echo '<a href="'.$blockCms["btnRedirection"]["button".$index].'" class="lbh">'.$btnLabelVal.'</a>';
                        } else if ($blockCms["btnRedirection"]["typeUrl".$index] == "externalLink") {
                            echo '<a href="'.$blockCms["btnRedirection"]["button".$index].'" target="_blank">'.$btnLabelVal.'</a>';
                        } else if ($blockCms["btnRedirection"]["typeUrl".$index] == "insideModal") {
                            echo '<a href="javascript:;" data-link="'. $blockCms["btnRedirection"]["button".$index] .'" data-title="'. ($blockCms["btnRedirection"]["modalLabel".$index] ?? "" ).'" '. (isset($blockCms["btnRedirection"]["getFirstStepOnly".$index]) && filter_var($blockCms["btnRedirection"]["getFirstStepOnly".$index], FILTER_VALIDATE_BOOL) ? "data-getfirst-steponly='true'" : "") .' class="internalLinkToModal">'.$btnLabelVal.'</a>';
                        }  else {
                            echo '<a href="#" class="lbh">'.$btnLabelVal.'</a>';
                        }
                    } else {
                        echo '<a href="#" class="lbh">'.$btnLabelVal.'</a>';
                    }
                }
                
            ?>
        </div>
        <div class="link-mention col-xs-12 col-md-6 col-lg-4">
            <span>Mentions légales</span>
        </div>
        <div class="link-social col-xs-12 col-md-6 col-lg-4">
            <?= isset($blockCms["socialMediaLink"]["linkedin"]) && !empty(trim($blockCms["socialMediaLink"]["linkedin"])) ?  '<a href="'.$blockCms["socialMediaLink"]["linkedin"].'" class="" target="_blank"><i class="fa fa-linkedin"></i></a>' : '' ?>
            <?= isset($blockCms["socialMediaLink"]["twitter"]) && !empty(trim($blockCms["socialMediaLink"]["twitter"])) ?  '<a href="'.$blockCms["socialMediaLink"]["twitter"].'" class="" target="_blank"><i class="fa fa-twitter-square"></i></a>' : '' ?>
            <?= isset($blockCms["socialMediaLink"]["instagram"]) && !empty(trim($blockCms["socialMediaLink"]["instagram"])) ?  '<a href="'.$blockCms["socialMediaLink"]["instagram"].'" class="" target="_blank"><i class="fa fa-instagram"></i></a>' : '' ?>
            <?= isset($blockCms["socialMediaLink"]["facebook"]) && !empty(trim($blockCms["socialMediaLink"]["facebook"])) ?  '<a href="'.$blockCms["socialMediaLink"]["facebook"].'" class="" target="_blank"><i class="fa fa-facebook"></i></a>' : '' ?>
            <!-- <span><i class="fa fa-twitter-square"></i></span>
            <span><i class="fa fa-instagram"></i></span>
            <span><i class="fa fa-facebook"></i></span> -->
        </div>
    </div>
</div>


<script type="text/javascript">
    const php = {
        kunik: '<?= $kunik ?>',
        my_cms_id: '<?= $myCmsId ?>',
    }
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    console.log("sectionDyf.<?php echo $kunik ?>BlockCms", sectionDyf.<?php echo $kunik ?>BlockCms);
    var str = "";
    var initInterval = null;
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-"+php.kunik).append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params[php.my_cms_id] = <?= json_encode($blockCms); ?>;
        const allFormsContext = <?php echo json_encode( $allFormsContext ); ?>;
        var footerCommun = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type: "groupButtons",
                            options: {
                                label: "Afficher les logos",
                                name: "showLogo",
                                options: [
                                    {
										value: false,
										label: trad.no
									},
									{
										value: true,
										label: trad.yes
									}
								],
								defaultValue: cmsConstructor.sp_params["<?= $myCmsId ?>"].showLogo
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "linkLabelContent",
                                label: "Texte des liens",
                                showInDefault: false,
                                inputs: [
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "linkLabel1",
                                            label : "lien 1",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "linkLabel2",
                                            label : "lien 2",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "linkLabel3",
                                            label : "lien 3",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "linkLabel4",
                                            label : "lien 4",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "linkLabel5",
                                            label : "lien 5",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "linkLabel6",
                                            label : "lien 6",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "linkLabel7",
                                            label : "lien 7",
                                            collection : "cms"
                                        }
                                    }
                                    
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "btnRedirection",
                                label: "Page de redirection",
                                showInDefault: true,
                                inputs: [
                                ],
                                defaultValue: sectionDyf[php.kunik + "BlockCms"]?.btnRedirection ? sectionDyf[php.kunik + "BlockCms"]?.btnRedirection : {}
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "imageContent",
                                label: "Les images",
                                class: "filtersMoreOptions",
                                showInDefault: false,
                                inputs: [
                                    {
                                        type : "inputFileImage",
                                        options : {
                                            name : "image1",
                                            label : tradCms.image+" 1",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputFileImage",
                                        options : {
                                            name : "image2",
                                            label : tradCms.image+" 2",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputFileImage",
                                        options : {
                                            name : "image3",
                                            label : tradCms.image+" 3",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputFileImage",
                                        options : {
                                            name : "image4",
                                            label : tradCms.image+" 4",
                                            collection : "cms"
                                        }
                                    },
                                    {
                                        type : "inputFileImage",
                                        options : {
                                            name : "image5",
                                            label : tradCms.image+" 5",
                                            collection : "cms"
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "socialMediaLink",
                                label: "Réseaux sociaux",
                                showInDefault: false,
                                inputs: [
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "linkedin",
                                            label : `Linkedin`,
                                            defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.socialMediaLink?.linkedin ? sectionDyf?.[php.kunik + "BlockCms"]?.socialMediaLink?.linkedin : null
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "instagram",
                                            label : `Instagram`,
                                            defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.socialMediaLink?.instagram ? sectionDyf?.[php.kunik + "BlockCms"]?.socialMediaLink?.instagram : null
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "twitter",
                                            label : `Twitter`,
                                            defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.socialMediaLink?.twitter ? sectionDyf?.[php.kunik + "BlockCms"]?.socialMediaLink?.twitter : null
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options : {
                                            name : "facebook",
                                            label : `Facebook`,
                                            defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.socialMediaLink?.facebook ? sectionDyf?.[php.kunik + "BlockCms"]?.socialMediaLink?.facebook : null
                                        }
                                    }
                                ],
                                defaultValue: sectionDyf[php.kunik + "BlockCms"]?.socialMediaLink ? sectionDyf[php.kunik + "BlockCms"]?.socialMediaLink : {}
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "fondImage",
                                label: "Fond des images",
                                inputs: [
                                    "backgroundColor",
                                    {
                                        type: "border",
                                        options: {
                                            name: "partBorder",
                                            label: "Bordure",
                                        }
                                    }
                                ]
                            }
                        },
                        {
							type: "section",
							options: {
								name: "fondLink",
								label: "Fond des liens",
								inputs: [
									"backgroundColor",
								]
							}
						},
                        "addCommonConfig",
                    ]
                },
                advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
            },
            beforeLoad: function() {
                if (typeof initInterval == "undefined" || initInterval == null) {
                    var customPages = $.map( costum.app, function( val, key ) {
                        return {value: key,label: key}
                    })
                    var pageToInsideModal = [];
                    $.each(allFormsContext, (formKey, formVal) => {
                        const contextId = formVal.parent ? Object.keys(formVal.parent)[0] : "null";
                        customPages.push({
                            value : `#coformAap.context.${ costum.contextSlug }.formid.${ formKey }.step.aapStep1.answerId.new`,
                            label : `#coformAap (${ formVal.name })`
                        });
                        pageToInsideModal.push({
                            value : `/survey/answer/answer/id/new/form/${ formKey }`,
                            label : `Nouvelle réponse au formulaire <b>${ formVal.name }</b>`
                        });
                    });
                    if (footerCommun.configTabs.general.inputsConfig?.[2]?.options?.inputs && footerCommun.configTabs.general.inputsConfig?.[2]?.options?.inputs.length < 1) {
                        $(`[data-kunik="<?= $kunik ?>"] .link-nav-item a`).each((index, elem) => {
                            const currentElem = $(elem);
                            footerCommun.configTabs.general.inputsConfig[2].options.inputs.push({
                                type: "inputSwitcher",
                                options: {
                                    name: "typeUrl"+index,
                                    label: `Pour le boutton (${currentElem.text()})`,
                                    class: "urlOptions ",
                                    tabs: [
                                        {
                                            value: "internalLink",
                                            label: "Lien interne",
                                            inputs: [
                                                {
                                                    type: "select",
                                                    options: {
                                                        name: "button"+index,
                                                        isSelect2: true,
                                                        label: `Page`,
                                                        options: customPages,
                                                        defaultValue: sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["button"+index] ? sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["button"+index] : null
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            value: "externalLink",
                                            label: "Lien externe",
                                            inputs: [
                                                {
                                                    type: "inputSimple",
                                                    options: {
                                                        name: "button"+index,
                                                        viewPreview: true,
                                                        defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["button"+index] ? sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["button"+index] : null
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            value: "insideModal",
                                            label: "Lien interne (Modal)",
                                            inputs: [
                                                {
                                                    type : "select",
                                                    options : {
                                                        name : "button"+index,
                                                        isSelect2 : true,
                                                        label : `Page`,
                                                        options : pageToInsideModal,
                                                        defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["button"+index] ? sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["button"+index] : null
                                                    }
                                                },
                                                {
                                                    "type": "groupButtons",
                                                    "options": {
                                                        "name": "getFirstStepOnly"+index,
                                                        "label": "Récuperer l'etape un seulement",
                                                        "options": [
                                                            {
                                                                "label": "Oui",
                                                                "value": true,
                                                                "icon": ""
                                                            },
                                                            {
                                                                "label": "Non",
                                                                "value": false,
                                                                "icon": ""
                                                            }
                                                        ],
                                                        defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["getFirstStepOnly"+index] ? sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["getFirstStepOnly"+index] : false
                                                    }
                                                },
                                                {
                                                    type : "inputSimple",
                                                    options : {
                                                        name : "modalLabel"+index,
                                                        label : `Titre du modal`,
                                                        defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["modalLabel"+index] ? sectionDyf?.[php.kunik + "BlockCms"]?.btnRedirection?.["modalLabel"+index] : null
                                                    }
                                                }
                                            ]
                                        },
                                    ]
                                }
                            })
                        })
                    }
                    initInterval = "notNull";
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.footer<?= $myCmsId ?> = footerCommun;
    }
    (function() {
        if(typeof userConnected != "undefined" && userConnected != null) {
            // $(".footer-signin").data("toggle", "").data("target", "");
            // $(".footer-signin").text('Deconexion');
            // $(".footer-signin").off("click").on("click", function() {
            //     blackDashboard(userConnected._id.$id);
            // })
        }
        function blackDashboard(__id) {
			var url;
			var post;
			// conditionnement avec le type de costum aap
			if (costum && typeof costum.type === 'string' && costum.type === 'aap') {
				url = baseUrl + '/co2/aap/userdashboard';
				if (typeof costum.fUserDashboard === 'string')
					url += '/function/' + costum.fUserDashboard;
				post = {
					"user" : __id
				}
				$('.dashboard-aap').remove();
				var dashboardAap = $('<div>')
					.addClass('modal dashboard-aap').css({
						display: 'none',
						overflowY: 'scroll',
						backgroundColor: 'rgba(0,0,0,0.9)',
						zIndex: 100000,
						position: 'fixed',
						top: $("#mainNav").outerHeight(),
						left: $("#menuApp.menuLeft").width()
					});
				$(document.body).append(dashboardAap);
				dashboardAap.show();
				if (typeof costum.fUserDashboard === 'string')
					url += '/function/' + costum.fUserDashboard;
				ajaxPost(dashboardAap, url, post, null, null, 'html');
				$('.menu-name-profil')
					.tooltip({
						container: 'body'
					})
					.removeClass('lbh')
					.addClass('open-modal-user-aap')
					.find(".tooltips-menu-btn")
					.text("Tableau de bord");
			}
		}
        $("."+php.kunik+" .internalLinkToModal").off("click").on("click", function() {
            var link = $(this).data("link");
            const getFirstStepOnly = $(this).data("getfirst-steponly")
            getFirstStepOnly && /true/.test(getFirstStepOnly) ? link += "/step/aapStep1" : link = link.replace("/step/aapStep1", ""); 
            const modalTitle = $(this).data("title");
            const finderMethode = ["searchAndPopulateFinder", "addSelectedToForm", "bindSelectItems", "populateFinder"];
            const lastFinderVal = {};
            if (typeof finder != "undefined") {
                $.each(finderMethode, (index, methode) => {
                    if (typeof finder[methode] != "undefined") {
                        lastFinderVal[methode] = finder[methode];
                    }
                })
            }
            if (link) {
                dialogContent.open({
                    modalContentClass: "modal-custom-lg",
                    isRemoteContent: true,
                    backdropClick: true,
                    shownCallback: () => {
                        $("#dialogContent .modal-content .modal-body#dialogContentBody").next().hide()
                    },
                    hiddenCallback: () => {
                        $("#dialogContent .modal-content .modal-body#dialogContentBody").next().show()
                        $("#dialogContent #dialogContentBody").removeClass("col-xs-12").empty()
                        $("#dialogContent #currentModalHeader, #dialogContent #currentModalFooter").remove();
                        if (typeof finder != "undefined") {
                            $.each(lastFinderVal, (key, methode) => {
                                finder[key] = methode;
                            })
                        }
                        if ($(`[data-need-refreshblock]`).length > 0) {
                            $(`[data-need-refreshblock]`).each((index, elem) => {
                                const blockId = $(elem).attr("data-need-refreshblock")
                                if (typeof refreshBlock == "function") {
                                    refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
                                }
                            })
                        }
                    }
                });
                ajaxPost(null, baseUrl + link, null, function (data) {
                    const _htmlStr = `
                        <div class="modal-body col-xs-12 bs-xs-0">
                            ${data}
                        </div>
                    `
                    $("#dialogContent #dialogContentBody").addClass("col-xs-12").empty().html(_htmlStr)
                    $("#dialogContent .modal-dialog > .modal-content #dialogContentBody").before(`
                        <div class="modal-header set-sticky" id="currentModalHeader">
                            <h3 class="modal-title text-center">${ (modalTitle ? modalTitle : "Titre du modal") }</h3>
                        </div>
                    `)
                    $("#dialogContent > .modal-dialog > .modal-content").append(`
                        <div class="custom-modal-footer col-xs-12" id="currentModalFooter">
                            <button type="button" class="btn btn-default close-current-modal hide">Envoyer</button>
                        </div>
                    `)
                    var btnSendInterval = setInterval(() => {
                        if ($("#dialogContent .coformstep.standalonestep").length > 0) {
                            clearInterval(btnSendInterval)
                            $("#dialogContent .custom-modal-footer .close-current-modal").removeClass("hide")
                        }
                    }, 1000);
                    $("#dialogContent .close-current-modal").off("click").on("click", function() {
                        $("#dialogContent .close-modal[data-dismiss=modal]").trigger("click")
                    })
                    const stickyElem = $('.modal-header.set-sticky');
                    const modalCont = $('#dialogContent .modal-content');
                    var stickyMenu = $('#dialogContent .coform-nav .schu-sticky');

                    function checkStickyPos() {
                        const scrollTop = modalCont.scrollTop() + window.scrollY;
                        const modalOffsetTop = modalCont?.offset()?.top ? modalCont?.offset()?.top : 0;
                        const modalHeight = modalCont.outerHeight();
                        var stickyMenu = $('#dialogContent .coform-nav .schu-sticky, #dialogContent .coform-nav .schu-btn-sticky');

                        if (scrollTop >= modalOffsetTop) {
                            stickyElem.css({
                                position: 'fixed',
                                width: $('#dialogContent .modal-content').width() - 20
                            });
                            stickyMenu.css({
                                position: 'fixed',
                                "z-index": "100",
                                top: stickyElem.offset().top - window.scrollY + stickyElem.outerHeight() + 10 + "px" 
                            });
                            if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
                                stickyMenu.css({
                                    left: "3%",
                                    width: "90vw"
                                })
                                $("#dialogContent .coform-nav .schu-sticky").css({
                                    top: stickyElem.offset().top - window.scrollY + stickyElem.outerHeight() + $("#dialogContent .coform-nav .schu-btn-sticky").outerHeight() + 10 + "px"
                                })
                            }
                        } else {
                            stickyElem.css({
                                position: 'relative',
                                width: '100%'
                            });
                            stickyMenu.css({
                                position: 'relative',
                                top: "initial",
                                left: "inherit"
                            });
                            if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
                                stickyMenu.css({
                                    left: "inherit",
                                    width: "100%"
                                })
                            }
                        }
                        if (typeof syncSection != "undefined" && stickyMenu.length > 0) {
                            syncSection()
                        }
                    }
                    typeof checkStickyPosition != "undefined" ? modalCont[0].removeEventListener('scroll', checkStickyPosition) : "";
                    modalCont[0].addEventListener('scroll', checkStickyPos);
                    
                    checkStickyPos();
                }, null, null);
            }
        })
    })()
</script>