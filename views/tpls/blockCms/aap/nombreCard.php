<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "nombreCard";
?>


<style id="css-<?= $kunik ?>">
    
.nb<?= $blockKey ?>.nombreCard-content {
    padding: 15px 5%;
    display: block;
    position: relative;
}

.nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content {
    display: flex;
    justify-content: space-around;
    align-items: center;
    flex-wrap: wrap;
}

.nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item {
    border-radius: 10px;
    border-bottom: 8px solid <?= $blockCms["css"]["colorTheme"]["color"] ?>;
    padding: 10px;
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    width: 100%;
    margin: 10px;
}

@media (max-width : 992px) {
    .nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item {
        width: 100% !important;
    }
}
@media (max-width : 1200px) {
    .nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item {
        width: 100%;
    }
}

.nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item .nb-content {
    font-size: 60px;
    color: <?= $blockCms["css"]["colorTheme"]["color"] ?>;
    display: block;
    font-weight: 500;
}
.nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item .title-content {
    font-size: 24px;
    color: <?= $blockCms["css"]["colorTheme"]["color"] ?>;
    display: block;
    margin-top: -10px;
}

.nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item .text-content {
    font-size: 16px;
    color: #000;
    display: block;
    opacity: .7;
}


</style>
<div class="nb<?= $blockKey ?> nombreCard-content <?= $kunik ?> <?= $kunik ?>-css">
    <div class="nombreCard-item-content">
        <div class="nombreCard-item com-card">
            <span class="nb-content" id="nbContent<?= $kunik ?>"><?= $blockCms["nombre"] ?></span>
            <span class="title-content"><?= $blockCms["title"] ?></span>
            <span class="sp-text text-content text<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texta"></span>
        </div>
    </div>
</div>

<script type="text/javascript">

    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    const php = {
		kunik: '<?= $kunik ?>',
		my_cms_id: '<?= $myCmsId ?>',
		block_cms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>))
	}
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var allFormsContext = <?php echo json_encode( $allFormsContext ); ?>;
        var nombreCard = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputSimple",
                            options : {
                                name : "title",
                                label : "Titre",
                                collection : "cms"
                            }
                        },
                        {
							type: "section",
							options: {
								name: "dataConfig",
								label: "Configuration de données à afficher",
								showInDefault: true,
								inputs: [
									{
										type: "select",
										options: {
											name: "formId",
											label: "Formulaire concerné",
											isSelect2: true,
											options: $.map(allFormsContext, function (val, key) {
													return {
														value: key,
														label: (val.name ? ucfirst(val.name.toLocaleLowerCase()) : "Pas de nom") + (val.type ? ` (${val.type})` : "")
													};
												})
										}
									},
									{
										type: "select",
										options: {
											name: "dataType",
											label: "Type de données",
											isSelect2: true,
											options: [
												{
													value: "communs",
													label: "Nombre des communs"
												},
												{
													value: "contributors",
													label: "Nombre des contributeurs"
												},
                                                {
													value: "tierslieux",
													label: "Nombre des tiers-lieux"
												}
											]
										}
									},
									{
										type: "section",
										options: {
											name: "filtersMoreOptions",
											label: "Options de filtres",
											class: "filtersMoreOptions",
											showInDefault: false,
											inputs: [
												{
													type : "select",
													options : {
														name : "applyFor",
														label : "À appliquer pour",
														options : [
															{
																value : "all",
																label : "toutes les utilisateurs"
															},
															{
																value : "forAdmin", 
																label : "administrateur et super admin"
															},
															{
																value : "forUser",
																label : "simple utilisateur"
															}
														]
													}
												},
												{
													type: "textarea",
													options: {
														label: "Filters option",
														name: "filtersOption"
													}
												}
											]
										}
									}
								]
							}
						},
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "colorTheme",
                                label : tradCms.chooseThemeColor,
                                inputs : [
                                    "color"
                                ]
                            }
                        },
                        "addCommonConfig"
                    ]
                },
                advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.nombreCard<?= $myCmsId ?> = nombreCard;
    }
    appendTextLangBased(".text<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["texta"]) ?>,"<?= $blockKey ?>");
    (function () {
		if (php?.block_cms?.dataConfig?.dataType) {
			const dataType = php.block_cms.dataConfig.dataType;
			var partPost = {
				searchType: ["answers"],
				indexStep: "all",
				notSourceKey: true,
				field: ["answers.aapStep1.titre"],
				filters: {
					form: php.block_cms.dataConfig.formId,
					"answers.aapStep1.titre": {
						"$exists": true
					}
				}
			}
			if (php.block_cms.dataConfig?.filtersMoreOptions?.filtersOption) {
				/* Add options in defaults fiters */
				var parsedStr = null;
				// Remplacer les variables dynamiques formaté en ${var} par leur valeur
				parsedStr = php.block_cms.dataConfig.filtersMoreOptions.filtersOption.replace(/\$\{([^}]+)\}/g, (match, p1) => {
					try {
						// Évaluer l'expression à l'intérieur de ${}
						return eval(p1);
					} catch (error) {
						mylog.error("Erreur lors de l'évaluation de l'expression : ", p1, error);
						return p1;
					}
				})
				var parsedObj = null;
				try {
					parsedObj = JSON.parse(parsedStr)
				} catch (e) {
					mylog.error("the current value is not a valid json", parsedStr)
				}
				if (parsedObj) {
					const addParsedObjToFilters = (parsedObject) => {
						$.each(parsedObject, function (parsedObjKey, parsedObjVal) {
							partPost.filters[parsedObjKey] = parsedObjVal;
						})
					}
					var applyFor = "all"
					if (php.block_cms.dataConfig.filtersMoreOptions.applyFor) {
						applyFor = php.block_cms.dataConfig.filtersMoreOptions.applyFor
					}
					switch (applyFor) {
						case "forAdmin":
							if (isSuperAdmin || isInterfaceAdmin) {
								addParsedObjToFilters(parsedObj)
							}
							break;
						case "forUser":
							if (!isSuperAdmin && !isInterfaceAdmin) {
								addParsedObjToFilters(parsedObj)
							}
							break;
						default:
							addParsedObjToFilters(parsedObj)
							break;
					}
				}
			}
			var partUrl = baseUrl + `/co2/aap/directoryproposal/countonly/true/form/${ php.block_cms.dataConfig.formId }`
			switch (dataType) {
				case "contributors":
					if (php?.block_cms?.dataConfig?.formId) {
						partUrl = baseUrl + `/co2/aap/directoryelement/elttype/contributors/form/${ php.block_cms.dataConfig.formId }`
						partPost.fields = ["answers.aapStep1.titre", "links"];
						ajaxPost(null, partUrl, partPost, function(res) {
							if (res && res?.elements?.contributors) {
								const contribsCount = Object.keys(res.elements.contributors).length
								$("#nbContent"+ php.kunik ).text(contribsCount);
							}
						}, null, null)
					}
					break;
                case "tierslieux":
					if (php?.block_cms?.dataConfig?.formId) {
						partUrl = baseUrl + `/co2/aap/directoryelement/elttype/answerfinancers/form/${ php.block_cms.dataConfig.formId }`
						partPost.fields = ["answers.aapStep1.titre", "answers.aapStep1.depense", "links"];
						ajaxPost(null, partUrl, partPost, function(res) {
							if (res && res?.elements?.answerfinancers) {
								const eltsCount = Object.keys(res.elements.answerfinancers).length
								$("#nbContent"+ php.kunik ).text(eltsCount);
							}
						}, null, null)
					}
					break;
				default:
					if (php?.block_cms?.dataConfig?.formId) {
						ajaxPost(null, partUrl, partPost, function(res) {
							if (res && res?.count?.answers) {
								$("#nbContent"+ php.kunik ).text(res.count.answers);
							}
						}, null, null)
					}
					break;
			}
		}
	})();

</script>