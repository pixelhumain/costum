<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];
$keyTpl = "campCoFinancCount";
?>

<style>
	.co-financ-count-content {
		padding: 0px 15% 100px;
		text-align: center;
	}

	.co-financ-count-content .count-title {
		display: block;
		color: #000;
		font-size: 36px;
		font-weight: 800;
		padding-left: 20px;
	}

	.co-financ-count-content .count-rebours {
		display: flex;
		justify-content: center;
		text-align: center;
	}

	.co-financ-count-content .count-jours {
		padding-right: 15px;
	}

	.co-financ-count-content .count-jours,
	.co-financ-count-content .count-heures,
	.co-financ-count-content .count-minutes,
	.co-financ-count-content .count-second,
	.co-financ-count-content .count-point {
		color: #FF286B;
		font-size: 130px;
		font-weight: 600;
		display: block;
	}

	.co-financ-count-content .count-label {
		font-size: 20px;
		font-weight: 500;
		color: #000;
		display: block;
		margin-top: -25px;
	}
	.coutdown {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
	}
	.coutdown.count-days {
		margin: 0 50px;
	}
	@media (max-width : 992px) {
		.co-financ-count-content .count-title {
			font-size: 30px !important;
		}
		.co-financ-count-content .count-jours, .co-financ-count-content .count-heures, .co-financ-count-content .count-minutes, .co-financ-count-content .count-second, .co-financ-count-content .count-point {
			font-size: 100px;
		}
	}
	@media (max-width : 710px) {
		.co-financ-count-content .count-title {
			font-size: 22px !important;
		}
		.co-financ-count-content .count-jours, .co-financ-count-content .count-heures, .co-financ-count-content .count-minutes, .co-financ-count-content .count-second, .co-financ-count-content .count-point {
			font-size: 70px ;
		}
	}
	@media (max-width : 530px) {
		.co-financ-count-content .count-title {
			font-size: 16px !important;
		}
		.co-financ-count-content .count-jours, .co-financ-count-content .count-heures, .co-financ-count-content .count-minutes, .co-financ-count-content .count-second, .co-financ-count-content .count-point {
			font-size: 60px ;
		}
		.co-financ-count-content .count-label {
			font-size: 14px;
			margin-top: -15px;
		}
		.coutdown.count-days {
			margin: 0 10px;
		}
	}
</style>

<div class="co-financ-count-content">
	<span class="count-title"><?= $blockCms["title"] ?></span>
	<div class="count-rebours">
		<div class="coutdown count-days">
			<span class="count-jours">90</span><span class="count-label">Jours</span>
		</div>
		<div class="coutdown">
			<span class="count-heures">12</span><span class="count-label">Heures</span>
		</div>
		<div class="coutdown">
			<span class="count-point">:</span>
		</div>
		<div class="coutdown">
			<span class="count-minutes">34</span><span class="count-label">Minutes</span>
		</div>
		<div class="coutdown">
			<span class="count-point">:</span></span>
		</div>
		<div class="coutdown">
			<span class="count-second">54</span><span class="count-label">Secondes</span>
		</div>
	</div>
</div>

<script type="text/javascript">
	var php;
	var count_interval;
	$(document).ready(function() {
		php = {
			block_cms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>))
		};
		var end_date;
		if (typeof php.block_cms.endDate === 'string')
			end_date = moment(php.block_cms.endDate, 'DD/MM/YYYY HH:mm');
		else
			end_date = moment('2024-09-30');

		function updateCountdown() {
			const now = moment();
			const distance = end_date.diff(now);
			const duration = moment.duration(distance);
			const as_milliseconds = moment.utc(duration.asMilliseconds());

			const days = parseInt(duration.asDays());
			const hours = as_milliseconds.format('HH');
			const minutes = as_milliseconds.format('mm');
			const seconds = as_milliseconds.format('ss');

			$('.count-jours').text(days);
			$('.count-heures').text(hours);
			$('.count-minutes').text(minutes);
			$('.count-second').text(seconds);

			if (distance < 0) {
				clearTimeout(count_interval);
				$('.count-jours').text('0');
				$('.count-heures').text('00');
				$('.count-minutes').text('00');
				$('.count-second').text('00');
				// bootbox.alert({
				// 	message: 'LA CAMPAGNE DE COFINANCEMENT EST LANCER',
				// 	backdrop: true
				// })
				return ;
			}
			clearTimeout(count_interval);
			count_interval = setTimeout(updateCountdown, 1000);
		}
		updateCountdown();
	});
	sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode($blockCms); ?>;
	var str = "";
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#css-<?= $kunik ?>").append(str);
	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
		var campCoFinancCount = {
			configTabs: {
				general: {
					inputsConfig: [
						{
							type: "inputSimple",
							options: {
								name: "title",
								label: tradCms.label,
								collection: "cms"
							}
						},
						{
							type: "dateTimePicker",
							options: {
								name: "endDate",
								label: tradDynForm.endDate,
								collection: "cms"
							}
						},
					]
				},
				style: {
					inputsConfig: [{

					}]
				},
				advanced: {
                    inputsConfig: [
                    	"addCommonConfig"
                    ]
                },
			},
			afterSave: function(path, valueToSet, name, payload, value) {
				clearTimeout(count_interval);
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
			}
		}
		cmsConstructor.blocks.campCoFinancCount<?= $myCmsId ?> = campCoFinancCount;
	}
</script>