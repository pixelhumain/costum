<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "titleAndDesc";
?>

<style>
.deposer-commun-content {
    padding: 10%;
    display: flex;
    position: relative;
    width: 100%;
    flex-direction: <?= $blockCms["affichage"] == 'vertical' ? 'column' : 'row' ?>;
    justify-content: space-between;
    align-items: center;
    background-color: #43C9B7;
}
.deposer-commun-content .deposer-title {
    text-align: center;
}
.deposer-commun-content .deposer-title span {
    font-size: 50px;
    font-weight: 600;
    color: #fff;
}
.deposer-commun-content .deposer-question {
    /* margin: 70px 0 0; */
    text-align: center;
}
.deposer-commun-content .deposer-question span {
    font-size: 40px;
    font-weight: 500;
    color: #fff;
}
.deposer-commun-content .btn-content {
    display: flex;
    margin-left : <?= $blockCms["affichage"] == 'vertical' ? '-80px' : '0' ?>;
	white-space: nowrap;
}

.deposer-commun-content .btn-content a, .deposer-commun-content .btn-content button {
    padding: 8px 25px;
    border: 5px solid #fff;
    border-radius: 30px;
    font-size: 22px;
    color: #fff !important;
    margin: 30px 0 0 5px;
    text-decoration: none !important;
    background-color: transparent;
    transition: all .4s;
    font-weight: 600;
}
.deposer-commun-content .btn-content a:hover, .deposer-commun-content .btn-content button:hover {
    border: 5px solid #fff;
    color: #43C9B7 !important;
    background-color: #fff;
    text-decoration: none;
}
.deposer-commun-content .btn-question-content {
    display: flex;
    white-space: nowrap;
}

.deposer-commun-content .btn-question-content a, .deposer-commun-content .btn-question-content button {
    padding: 8px 25px;
    border: 5px solid #fff;
    margin: 30px 0 0 5px;
    border-radius: 30px;
    font-size: 22px;
    color: #fff !important;
    text-decoration: none !important;
    background-color: transparent;
    transition: all .4s;
    font-weight: 600;
}
.deposer-commun-content .btn-question-content a:hover, .deposer-commun-content .btn-question-content button:hover {
    border: 5px solid #fff;
    color: #43C9B7 !important;
    background-color: #fff;
    text-decoration: none;
}
    @media (min-width: 768px) {
		#dialogContentBody .section-nav-body {
			width: 11vw;
		}	
	}

@media (max-width : 550px) {
    .deposer-commun-content .deposer-title span {
        font-size: 30px !important;
    }
    .deposer-commun-content .btn-content a {
        font-size: 14px;
        font-weight: 800;
    }
    .deposer-commun-content .deposer-question span {
        font-size: 20px;
    }
    .deposer-commun-content .btn-question-content a {
        font-size: 14px;
        font-weight: 800;
    }
    .deposer-commun-content .deposer-question {
        margin-top: 30px !important;
    }
}
.post-content, .quest-content {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: relative;
}
.post-content {
    margin-bottom: <?= $blockCms["affichage"] == 'vertical' ? '50px' : '0' ?>;
}
</style>

<div class="deposer-commun-content <?= $kunik ?>">
    <div class="post-content">
        <div class="deposer-title">
            <span><?= $blockCms["title1"] ?></span>
        </div>
        <div class="btn-content">
            <img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl.'/images/aap/lesCommuns/fleche.png';?>" alt="">
            <?php
                
                if (isset($blockCms["button0"]) && isset($blockCms["typeUrl0"])) {
                    if ($blockCms["typeUrl0"] == "internalLink") {
                        echo '<a href="'.$blockCms["button0"].'" class="lbh">'.$blockCms["btnLabel1"].'</a>';
                    } else if ($blockCms["typeUrl0"] == "externalLink") {
                        echo '<a href="'.$blockCms["button0"].'" target="_blank">'.$blockCms["btnLabel1"].'</a>';
                    } else if ($blockCms["typeUrl0"] == "insideModal") {
                        echo '<a href="javascript:;" data-link="'. $blockCms["button0"] .'" data-title="'. ($blockCms["modalLabel0"] ?? "" ).'" '. (isset($blockCms["getFirstStepOnly0"]) && filter_var($blockCms["getFirstStepOnly0"], FILTER_VALIDATE_BOOL) ? "data-getfirst-steponly='true'" : "") .' class="internalLinkToModal">'.$blockCms["btnLabel1"].'</a>';
                    }  else {
                        echo '<a href="#" class="lbh">'.$blockCms["btnLabel1"].'</a>';
                    }
                } else {
                    echo '<a href="#" class="lbh">'.$blockCms["btnLabel1"].'</a>';
                }            
            ?>
        </div>
    </div>
    <div class="quest-content">
        <div class="deposer-question">
            <span><?= $blockCms["title2"] ?></span>
        </div>
        <div class="btn-question-content">
            <?php
                
                if (isset($blockCms["button1"]) && isset($blockCms["typeUrl1"])) {
                    if ($blockCms["typeUrl1"] == "internalLink") {
                        echo '<a href="'.$blockCms["button1"].'" class="lbh">'.($blockCms["btnLabel2"] ?? "").'</a>';
                    } else if ($blockCms["typeUrl1"] == "externalLink") {
                        echo '<a href="'.$blockCms["button1"].'" target="_blank">'.($blockCms["btnLabel2"] ?? "").'</a>';
                    } else if ($blockCms["typeUrl1"] == "insideModal") {
                        echo '<a href="javascript:;" data-link="'. $blockCms["button1"] .'" data-title="'. ($blockCms["modalLabel1"] ?? "" ).'" '. (isset($blockCms["getFirstStepOnly1"]) && filter_var($blockCms["getFirstStepOnly1"], FILTER_VALIDATE_BOOL) ? "data-getfirst-steponly='true'" : "") .' class="internalLinkToModal">'.($blockCms["btnLabel2"] ?? "").'</a>';
                    }  else {
                        echo '<a href="#" class="lbh">'.($blockCms["btnLabel2"] ?? "").'</a>';
                    }
                } else {
                    echo '<a href="#" class="lbh">'.($blockCms["btnLabel2"] ?? "").'</a>';
                }            
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    const php = {
        kunik: '<?= $kunik ?>',
        my_cms_id: '<?= $myCmsId ?>',
    }
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        const allFormsContext = <?php echo json_encode( $allFormsContext ); ?>;
        var customPages = $.map( costum.app, function( val, key ) {
            return {value: key,label: key}
        })
        $.each(allFormsContext, (formKey, formVal) => {
            const contextId = formVal.parent ? Object.keys(formVal.parent)[0] : "null";
            customPages.push({
                value : `#coformAap.context.${ costum.contextSlug }.formid.${ formKey }.step.aapStep1.answerId.new`,
                label : `#coformAap (${ formVal.name })`
            })
        });
        var pageToInModal = [];
        $.each(allFormsContext, (formKey, formVal) => {
            pageToInModal.push({
                value : `/survey/answer/answer/id/new/form/${ formKey }`,
                label : `Nouvelle réponse au formulaire <b>${ formVal.name }</b>`
            });
        });
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var deposerCommun = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "select",
                            options : {
                                name : "affichage",
                                label : "Type d'affichage",
                                options : [
                                    {
                                        value : "vertical",
                                        label : "Vertical"
                                    },
                                    {
                                        value : "horizontal", 
                                        label : "Horizontal"
                                    }
                                ]
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                name : "title1",
                                label : "Premier titre",
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                name : "title2",
                                label : "Deuxième titre",
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                name : "btnLabel1",
                                label : "label du bouton 1",
                                collection : "cms"
                            }
                        },
                        {
							type: "inputSwitcher",
                            options: {
                                name: "typeUrl0",
                                label: `Pour le boutton 1`,
                                class: "urlOptions ",
                                tabs: [
                                    {
                                        value: "internalLink",
                                        label: "Lien interne",
                                        inputs: [
                                            {
                                                type: "select",
                                                options: {
                                                    name: "button0",
                                                    isSelect2: true,
                                                    label: `Page`,
                                                    options: customPages,
                                                    defaultValue: sectionDyf?.[php.kunik + "BlockCms"]?.button0 ? sectionDyf?.[php.kunik + "BlockCms"]?.button0 : null
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value: "externalLink",
                                        label: "Lien externe",
                                        inputs: [
                                            {
                                                type: "inputSimple",
                                                options: {
                                                    name: "button0",
                                                    viewPreview: true,
                                                    defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.link0 ? sectionDyf?.[php.kunik + "BlockCms"]?.link0 : null
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value: "insideModal",
                                        label: "Lien interne (Modal)",
                                        inputs: [
                                            {
                                                type : "select",
                                                options : {
                                                    name : "button0",
                                                    isSelect2 : true,
                                                    label : `Page`,
                                                    options : pageToInModal
                                                }
                                            },
                                            {
                                                "type": "groupButtons",
                                                "options": {
                                                    "name": "getFirstStepOnly0",
                                                    "label": "Récuperer l'etape un seulement",
                                                    "options": [
                                                        {
                                                            "label": "Oui",
                                                            "value": true,
                                                            "icon": ""
                                                        },
                                                        {
                                                            "label": "Non",
                                                            "value": false,
                                                            "icon": ""
                                                        }
                                                    ],
                                                    defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.getFirstStepOnly0 ? sectionDyf?.[php.kunik + "BlockCms"]?.getFirstStepOnly0 : false
                                                }
                                            },
                                            {
                                                type : "inputSimple",
                                                options : {
                                                    name : "modalLabel0",
                                                    label : `Titre du modal`
                                                }
                                            }
                                        ]
                                    },
                                ]
                            }
						},
                        {
                            type : "inputSimple",
                            options : {
                                name : "btnLabel2",
                                label : "label du bouton 2",
                                collection : "cms"
                            }
                        },
                        {
							type: "inputSwitcher",
                            options: {
                                name: "typeUrl1",
                                label: `Pour le boutton 2`,
                                class: "urlOptions ",
                                tabs: [
                                    {
                                        value: "internalLink",
                                        label: "Lien interne",
                                        inputs: [
                                            {
                                                type: "select",
                                                options: {
                                                    name: "button1",
                                                    isSelect2: true,
                                                    label: `Page`,
                                                    options: customPages,
                                                    defaultValue: sectionDyf?.[php.kunik + "BlockCms"]?.button1 ? sectionDyf?.[php.kunik + "BlockCms"]?.button1 : null
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value: "externalLink",
                                        label: "Lien externe",
                                        inputs: [
                                            {
                                                type: "inputSimple",
                                                options: {
                                                    name: "button1",
                                                    viewPreview: true,
                                                    defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.button1 ? sectionDyf?.[php.kunik + "BlockCms"]?.button1 : null
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value: "insideModal",
                                        label: "Lien interne (Modal)",
                                        inputs: [
                                            {
                                                type : "select",
                                                options : {
                                                    name : "button1",
                                                    isSelect2 : true,
                                                    label : `Page`,
                                                    options : pageToInModal
                                                }
                                            },
                                            {
                                                "type": "groupButtons",
                                                "options": {
                                                    "name": "getFirstStepOnly1",
                                                    "label": "Récuperer l'etape un seulement",
                                                    "options": [
                                                        {
                                                            "label": "Oui",
                                                            "value": true,
                                                            "icon": ""
                                                        },
                                                        {
                                                            "label": "Non",
                                                            "value": false,
                                                            "icon": ""
                                                        }
                                                    ],
                                                    defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.getFirstStepOnly1 ? sectionDyf?.[php.kunik + "BlockCms"]?.getFirstStepOnly1 : false
                                                }
                                            },
                                            {
                                                type : "inputSimple",
                                                options : {
                                                    name : "modalLabel1",
                                                    label : `Titre du modal`
                                                }
                                            }
                                        ]
                                    },
                                ]
                            }
						},
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            
                        }
                    ]
                },
                advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.deposerCommun<?= $myCmsId ?> = deposerCommun;
    }
    (function() {
        $("."+php.kunik+" .internalLinkToModal").off("click").on("click", function() {
            var link = $(this).data("link");
            const modalTitle = $(this).data("title");
            const getFirstStepOnly = $(this).data("getfirst-steponly")
            getFirstStepOnly && /true/.test(getFirstStepOnly) ? link += "/step/aapStep1" : link = link.replace("/step/aapStep1", ""); 
            const finderMethode = ["searchAndPopulateFinder", "addSelectedToForm", "bindSelectItems", "populateFinder"];
            const lastFinderVal = {};
            if (typeof finder != "undefined") {
                $.each(finderMethode, (index, methode) => {
                    if (typeof finder[methode] != "undefined") {
                        lastFinderVal[methode] = finder[methode];
                    }
                })
            }
            if (link) {
                dialogContent.open({
                    modalContentClass: "modal-custom-lg",
                    isRemoteContent: true,
                    backdropClick: true,
                    shownCallback: () => {
                        $("#dialogContent .modal-content .modal-body#dialogContentBody").next().hide()
                    },
                    hiddenCallback: () => {
                        $("#dialogContent .modal-content .modal-body#dialogContentBody").next().show()
                        $("#dialogContent #dialogContentBody").removeClass("col-xs-12").empty()
                        $("#dialogContent #currentModalHeader, #dialogContent #currentModalFooter").remove();
                        if (typeof finder != "undefined") {
                            $.each(lastFinderVal, (key, methode) => {
                                finder[key] = methode;
                            })
                        }
                        if ($(`[data-need-refreshblock]`).length > 0) {
                            $(`[data-need-refreshblock]`).each((index, elem) => {
                                const blockId = $(elem).attr("data-need-refreshblock")
                                if (typeof refreshBlock == "function") {
                                    refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
                                }
                            })
                        }
                    }
                });
                ajaxPost(null, baseUrl + link, null, function (data) {
                    const _htmlStr = `
                        <div class="modal-body col-xs-12 bs-xs-0">
                            ${data}
                        </div>
                    `
                    $("#dialogContent #dialogContentBody").addClass("col-xs-12").empty().html(_htmlStr)
                    $("#dialogContent > .modal-dialog:first-child > .modal-content #dialogContentBody").before(`
                        <div class="modal-header set-sticky" id="currentModalHeader">
                            <h3 class="modal-title text-center">${ (modalTitle ? modalTitle : "Titre du modal") }</h3>
                        </div>
                    `)
                    $("#dialogContent > .modal-dialog:first-child > .modal-content").append(`
                        <div class="custom-modal-footer col-xs-12" id="currentModalFooter">
                            <button type="button" class="btn btn-default close-current-modal hide">Envoyer</button>
                        </div>
                    `)
                    var btnSendInterval = setInterval(() => {
                        if ($("#dialogContent .coformstep.standalonestep").length > 0) {
                            clearInterval(btnSendInterval)
                            $("#dialogContent .custom-modal-footer .close-current-modal").removeClass("hide")
                        }
                    }, 1000);
                    $("#dialogContent .close-current-modal").off("click").on("click", function() {
                        $("#dialogContent .close-modal[data-dismiss=modal]").trigger("click")
                    })
                    const stickyElem = $('.modal-header.set-sticky');
                    const modalCont = $('#dialogContent .modal-content');

                    function checkStickyPos() {
                        const scrollTop = modalCont.scrollTop() + window.scrollY;
                        const modalOffsetTop = modalCont?.offset()?.top ? modalCont?.offset()?.top : 0;
                        const modalHeight = modalCont.outerHeight();
                        var stickyMenu = $('#dialogContent .coform-nav .schu-sticky, #dialogContent .coform-nav .schu-btn-sticky');

                        if (scrollTop >= modalOffsetTop) {
                            stickyElem.css({
                                position: 'fixed',
                                width: $('#dialogContent .modal-content').width() - 20
                            });
                            stickyMenu.css({
                                position: 'fixed',
                                "z-index": "100",
                                top: stickyElem.offset().top - window.scrollY + stickyElem.outerHeight() + 10 + "px" 
                            });
                            if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
                                stickyMenu.css({
                                    left: "3%",
                                    width: "90vw"
                                })
                                $("#dialogContent .coform-nav .schu-sticky").css({
                                    top: stickyElem.offset().top - window.scrollY + stickyElem.outerHeight() + $("#dialogContent .coform-nav .schu-btn-sticky").outerHeight() + 10 + "px"
                                })
                            }
                        } else {
                            stickyElem.css({
                                position: 'relative',
                                width: '100%'
                            });
                            stickyMenu.css({
                                position: 'relative',
                                top: "initial",
                                left: "inherit"
                            });
                            if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
                                stickyMenu.css({
                                    left: "inherit",
                                    width: "100%"
                                })
                            }
                        }
                        if (typeof syncSection != "undefined" && stickyMenu.length > 0) {
                            syncSection()
                        }
                    }
                    typeof checkStickyPosition != "undefined" ? modalCont[0].removeEventListener('scroll', checkStickyPosition) : "";
                    modalCont[0].addEventListener('scroll', checkStickyPos);
                    
                    // checkStickyPos();
                }, null, null);
            }
        })
    })()
</script>