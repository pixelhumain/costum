<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];
$keyTpl = "partCommun";

$blockKey = (string)$blockCms["_id"];
$baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
$initFiles = Document::getListDocumentsWhere(
	array(
		"id" => $blockKey,
		"type" => 'cms',
		"subKey" => "notBackground"
	),
	"image"
);
//$arrayImg = [];
$imgUrl = Yii::app()->getRequest()->getBaseUrl(true) . Yii::app()->getModule("costum")->assetsUrl . '/images/aap/lesCommuns/cont1.png';

?>

<style id="css-<?= $kunik ?>">
	.part<?= $kunik ?> {
		--dtl-costum-color1: <?= isset($costum["css"]["color"]["color1"]) ? $costum["css"]["color"]["color1"] : "#43c9b7" ?>;
		--dtl-costum-color2: <?= isset($costum["css"]["color"]["color2"]) ? $costum["css"]["color"]["color2"] : "#4623c9" ?>;
		--dtl-costum-color3: <?= isset($costum["css"]["color"]["color3"]) ? $costum["css"]["color"]["color3"] : "#ffc9cb" ?>;
	}
	.part<?= $kunik ?> {
		display: flex;
		position: relative;
		padding: 40px 10%;
		width: 100%;
		background-color: #D8F4F0;
		align-items: center;
	}

	.part<?= $kunik ?> .text-content {
		display: block;
		position: relative;
		width: 55%;
	}

	.part<?= $kunik ?> .img-content {
		display: flex;
		position: relative;
		width: 55%;
		background-image: url('<?php echo $blockCms["image"] == '' ? $imgUrl : $blockCms["image"] ?> ');
		align-items: center;
		justify-content: center;
		background-position: center;
		background-repeat: no-repeat;
		height: 400px;
		flex-direction: column;
		background-size: contain;
	}
	.part<?= $kunik ?> .img-content .nb-part {
		font-size: 100px;
		color: var(--dtl-costum-color1);
		margin-top: -55px;
	}
	.part<?= $kunik ?> .img-content .txt-part {
		font-size: 18px;
		color: var(--dtl-costum-color1);
		display: block;
		margin-top: -30px;
	}
	.part<?= $kunik ?> .part-title {
		font-size: 40px;
		font-weight: 600;
		color: var(--dtl-costum-color1);
	}

	.part<?= $kunik ?> .partDesc {
		font-size: 20px;
		line-height: 1.5;
		font-weight: 500;
		margin-top: 20px;
	}

	.part<?= $kunik ?> .part-btn-content {
		margin-top: 40px;
		display: flex;
		justify-content: flex-end;
	}

	.part<?= $kunik ?> .part-btn-content .btn-part {
		padding: 10px 25px;
		border-radius: 30px;
		border: 1px solid var(--dtl-costum-color1);
		background-color: var(--dtl-costum-color1);
		color: #fff;
		transition: all .4s;
		font-size: 22px;
		font-weight: 500;
		margin-left: 10px;
		margin-right: 20px;
		display: inline-flex;
		align-items: center;
		text-decoration: none;
		line-height: inherit !important;
		overflow: visible !important;
	}
	.part<?= $kunik ?> .part-btn-content .btn-part:hover {
		border: 8px solid var(--dtl-costum-color1);
		background-color: transparent;
		color: var(--dtl-costum-color1);
		transition: all .4s;
		cursor: pointer;
	}

	.part<?= $kunik ?> .form-round {
		width: 300px;
		height: 300px;
		border-radius: 50%;
		/* background-color: #fff; */
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		margin-top: 30px;
		padding-left: 2%;
		padding-bottom: 3%;
	}
	@media screen and (max-width: 940px) {
		.part<?= $kunik ?> {
			display: flex;
			flex-direction: column-reverse;
		}
	}
	@media screen and (max-width: 768px) {
		.part<?= $kunik ?> >.text-content {
			width: 100%;
		}

		.part<?= $kunik ?> .part-btn-content {
			margin-top: 40px;
			display: flex;
			justify-content: center;
			flex-direction: row;
			align-content: center;
			align-items: center;
		}
	}
	@media (max-width : 550px) {
		.part<?= $kunik ?> .part-btn-content img {
			display: none;
		}
		.part<?= $kunik ?> .part-title span {
			font-size: 22px;
		}
		.part<?= $kunik ?> .partDesc {
			font-size: 14px;
		}
		.part<?= $kunik ?> .part-btn-content .btn-part {
			font-size: 16px;
		}
		.part<?= $kunik ?> .part-btn-content img {
			width: 50px;
		}
	}
	@media (max-width : 400px) {
		.part<?= $kunik ?> .part-btn-content .btn-part {
			padding: 8px 15px;
		}
		.part<?= $kunik ?> .img-content {
			width: 100% !important;
			height: auto !important;
		}
		.part<?= $kunik ?> .form-round { 
			padding-left: 0 !important;
		}
		.part<?= $kunik ?> .img-content .txt-part {
			font-size: 10px !important;
		}
	}
	@media (max-width : 360px) {
		.part<?= $kunik ?> .part-btn-content img {
			display: none;
		}
	}

	.part<?= $kunik ?> .img-content a:hover {
		text-decoration: none;
	}
</style>

<div class="part-commun-content part<?= $kunik ?> <?= $kunik ?> <?= $kunik ?>-css" >
	<div class="text-content">
		<div class="part-title">
			<div class="sp-text partTitle partTitle<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title0">

			</div>
			<!-- <span><?= @$blockCms["title"] ?></span> -->
		</div>
		<div class="sp-text partDesc partDesc<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="partDesc">

		</div>
		<div class="part-btn-content">
			<img src="<?= Yii::app()->getRequest()->getBaseUrl(true) . Yii::app()->getModule("costum")->assetsUrl . '/images/aap/lesCommuns/fleche2.png'; ?>" alt="">
			<a href="javascript:;" class="btn-part">
				<div class="sp-text btnLabel btnLabel<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="btnLabel0">

				</div>	
				<!-- <?= @$blockCms["btnLabel"] ?> -->
			</a>
		</div>
	</div>
	<div class="img-content">
		<a href="javascript:;" class="linkImage">
			<div class="form-round">
				<span class="nb-part" id="partNumber<?= $kunik ?>"><?= $blockCms["nombre"] ?></span>
				<!-- <span class="txt-part" id="partText<?= $kunik ?>"><?= @$blockCms["text"] ?></span> -->
				<div class="sp-text txt-part txt-part<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text0">

				</div>
			</div>
		</a>
	</div>
</div>
<script>
	sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode($blockCms); ?>;
	var str = "";
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#css-<?= $kunik ?>").append(str);
	const php = {
		kunik: '<?= $kunik ?>',
		my_cms_id: '<?= $myCmsId ?>',
		block_cms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>))
	}
	if(!costum.editMode && typeof sectionDyf.<?php echo $kunik ?>BlockCms['linkImage'] != "undefined" && sectionDyf.<?php echo $kunik ?>BlockCms['linkImage'] != "") {
		$('.part<?= $kunik ?> .img-content .linkImage').addClass("lbh")
		$('.part<?= $kunik ?> .img-content .linkImage').attr("href", sectionDyf.<?php echo $kunik ?>BlockCms['linkImage'])
	}
	if(!costum.editMode && typeof sectionDyf.<?php echo $kunik ?>BlockCms['link'] != "undefined" && sectionDyf.<?php echo $kunik ?>BlockCms['link'] != "") {
		$('.part<?= $kunik ?> .part-btn-content a').addClass("lbh")
		$('.part<?= $kunik ?> .part-btn-content a').attr("href", sectionDyf.<?php echo $kunik ?>BlockCms['link'])
	}
	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
		var allFormsContext = <?php echo json_encode( $allFormsContext ); ?>;
		var partCommun = {
			configTabs: {
				general: {
					inputsConfig: [{
							type: "inputSimple",
							options: {
								name: "title",
								label: "Titre",
								collection: "cms"
							}
						},
						{
							type: "inputSimple",
							options: {
								name: "text",
								label: "Text du nombre",
								collection: "cms"
							}
						},
						{
							type: "inputSimple",
							options: {
								name: "btnLabel",
								label: tradCms.label,
								collection: "cms"
							}
						},
						{
							type: "inputFileImage",
							options: {
								name: "image",
								label:"choisir l'image",
								collection: "cms"
							}
						},
						{
							type: "inputWithSelect",
							options: {
								label: "Lien de l'image",
								name: "linkImage",
								configForSelect2: {
									maximumSelectionSize: 1,
									tags: Object.keys(costum.app)
								}
							}
						},
						{

							type: "inputWithSelect",
							options: {
								label: "Lien du bouton",
								name: "link",
								configForSelect2: {
									maximumSelectionSize: 1,
									tags: Object.keys(costum.app)
								}
							}
						},
						{
							type: "section",
							options: {
								name: "dataConfig",
								label: "Configuration de données à afficher",
								showInDefault: true,
								inputs: [
									{
										type: "select",
										options: {
											name: "formId",
											label: "Formulaire concerné",
											isSelect2: true,
											options: $.map(allFormsContext, function (val, key) {
													return {
														value: key,
														label: (val.name ? ucfirst(val.name.toLocaleLowerCase()) : "Pas de nom") + (val.type ? ` (${val.type})` : "")
													};
												})
										}
									},
									{
										type: "select",
										options: {
											name: "dataType",
											label: "Type de données",
											isSelect2: true,
											options: [
												{
													value: "communs",
													label: "Nombre des communs déposés"
												},
												{
													value: "contributors",
													label: "Nombre des contributeurs"
												}
											]
										}
									},
									{
										type: "section",
										options: {
											name: "filtersMoreOptions",
											label: "Options de filtres",
											class: "filtersMoreOptions",
											showInDefault: false,
											inputs: [
												{
													type : "select",
													options : {
														name : "applyFor",
														label : "À appliquer pour",
														options : [
															{
																value : "all",
																label : "toutes les utilisateurs"
															},
															{
																value : "forAdmin", 
																label : "administrateur et super admin"
															},
															{
																value : "forUser",
																label : "simple utilisateur"
															}
														]
													}
												},
												{
													type: "textarea",
													options: {
														label: "Filters option",
														name: "filtersOption"
													}
												}
											]
										}
									}
								]
							}
						},
					]
				},
				style: {
					inputsConfig: [
						"addCommonConfig",
						"backgroundColor"
					]
				},
				advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
			},
			afterSave: function(path, valueToSet, name, payload, value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
			}
		}
		cmsConstructor.blocks.partCommun<?= $myCmsId ?> = partCommun;
	}
	appendTextLangBased(".partDesc<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($blockCms["partDesc"]) ?>, "<?= $blockKey ?>");
	appendTextLangBased(".partTitle<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($blockCms["title0"]) ?>, "<?= $blockKey ?>");
	appendTextLangBased(".txt-part<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($blockCms["text0"]) ?>, "<?= $blockKey ?>");
	appendTextLangBased(".btnLabel<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($blockCms["btnLabel0"]) ?>, "<?= $blockKey ?>");
	(function () {
		if (php?.block_cms?.dataConfig?.dataType) {
			const dataType = php.block_cms.dataConfig.dataType;
			var partPost = {
				searchType: ["answers"],
				indexStep: "all",
				notSourceKey: true,
				field: ["answers.aapStep1.titre"],
				filters: {
					form: php.block_cms.dataConfig.formId,
					"answers.aapStep1.titre": {
						"$exists": true
					}
				}
			}
			if (php.block_cms.dataConfig?.filtersMoreOptions?.filtersOption) {
				/* Add options in defaults fiters */
				var parsedStr = null;
				// Remplacer les variables dynamiques formaté en ${var} par leur valeur
				parsedStr = php.block_cms.dataConfig.filtersMoreOptions.filtersOption.replace(/\$\{([^}]+)\}/g, (match, p1) => {
					try {
						// Évaluer l'expression à l'intérieur de ${}
						return eval(p1);
					} catch (error) {
						mylog.error("Erreur lors de l'évaluation de l'expression : ", p1, error);
						return p1;
					}
				})
				var parsedObj = null;
				try {
					parsedObj = JSON.parse(parsedStr)
				} catch (e) {
					mylog.error("the current value is not a valid json", parsedStr)
				}
				if (parsedObj) {
					const addParsedObjToFilters = (parsedObject) => {
						$.each(parsedObject, function (parsedObjKey, parsedObjVal) {
							partPost.filters[parsedObjKey] = parsedObjVal;
						})
					}
					var applyFor = "all"
					if (php.block_cms.dataConfig.filtersMoreOptions.applyFor) {
						applyFor = php.block_cms.dataConfig.filtersMoreOptions.applyFor
					}
					switch (applyFor) {
						case "forAdmin":
							if (isSuperAdmin || isInterfaceAdmin) {
								addParsedObjToFilters(parsedObj)
							}
							break;
						case "forUser":
							if (!isSuperAdmin && !isInterfaceAdmin) {
								addParsedObjToFilters(parsedObj)
							}
							break;
						default:
							addParsedObjToFilters(parsedObj)
							break;
					}
				}
			}
			var partUrl = baseUrl + `/co2/aap/directoryproposal/countonly/true/form/${ php.block_cms.dataConfig.formId }`
			switch (dataType) {
				case "contributors":
					if (php?.block_cms?.dataConfig?.formId) {
						partUrl = baseUrl + `/co2/aap/directoryelement/elttype/contributors/form/${ php.block_cms.dataConfig.formId }`
						partPost.fields = ["answers.aapStep1.titre", "links"];
						ajaxPost(null, partUrl, partPost, function(res) {
							if (res && res?.elements?.contributors) {
								const contribsCount = Object.keys(res.elements.contributors).length
								$("#partNumber"+ php.kunik ).text(contribsCount);
								if (contribsCount > 1 && (/contributeur/i).test($("#partText"+ php.kunik).text())) {
									$("#partText"+ php.kunik).text("Contributeurs")
								} else {
									$("#partText"+ php.kunik).text("Contributeur")
								}
							}
						}, null, null)
					}
					break;
			
				default:
					if (php?.block_cms?.dataConfig?.formId) {
						ajaxPost(null, partUrl, partPost, function(res) {
							if (res && res?.count?.answers) {
								$("#partNumber"+ php.kunik ).text(res.count.answers);
								if (res.count.answers > 1 && (/commun d/i).test($("#partText"+ php.kunik).text())) {
									$("#partText"+ php.kunik).text("Communs déposés")
								}
							}
						}, null, null)
					}
					break;
			}
		}
	})();
</script>