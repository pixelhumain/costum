<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];
$keyTpl = "campDetail";
$files = [
	'/js/aap/aapv2.js'
];
HtmlHelper::registerCssAndScriptsFiles($files, Yii::app()->getModule('co2')->getAssetsUrl());
?>

<style id="css-<?= $kunik ?>">
	html, body {
		touch-action: cross-slide-y;
	}
	@font-face {
		font-family: 'lunchType';
		src: url("<?= Yii::app()->getRequest()->getBaseUrl(true) . Yii::app()->getModule("costum")->assetsUrl . '/font/coFinancement/Lunchtype22-Regular.ttf'; ?>")
	}

	.camp-detail-content {
		--default-button-color: #000;
		--active-button-color: #43C9B7;
		--dtl-costum-color1: <?= isset($costum["css"]["color"]["color1"]) ? $costum["css"]["color"]["color1"] : "#43c9b7" ?>;
		--dtl-costum-color2: <?= isset($costum["css"]["color"]["color2"]) ? $costum["css"]["color"]["color2"] : "#4623c9" ?>;
		--dtl-costum-color3: <?= isset($costum["css"]["color"]["color3"]) ? $costum["css"]["color"]["color3"] : "#ffc9cb" ?>;
	}

	.camp-detail-content {
		display: block;
		position: relative;
		padding: 4% 2% 2%;
		width: 100%;
	}

	.camp-detail-content .action-cont {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
	}

	.camp-detail-content .camp-detail-title .com-title {
		font-size: 43px;
		font-weight: 700;
		color: var(--dtl-costum-color1);
	}

	.campDesc,
	.porteurDesc {
		font-size: 20px;
		line-height: 1.5;
		transition: all .4s;
		box-sizing: border-box;
	}

	.camp-detail-content .tags-content {
		padding: 2px 3px;
		display: flex;
		flex-wrap: wrap;
	}

	.camp-detail-content .tags-content .tags {
		padding: 2px 4px;
		border: 1px solid var(--dtl-costum-color3);
		border-radius: 8px;
		font-size: 1.3rem;
		font-weight: 600;
		margin-right: 5px;
		color: #fff;
		background-color: var(--dtl-costum-color3);
		margin-bottom: 5px;
	}

	.camp-detail-content .camp-porteur {
		display: flex;
		align-items: center;
		padding: 15px 0 20px;
		gap: 5px;
		justify-content: center;
		width: 100%;
		text-align: center;
		flex-direction: column;
	}

	.camp-detail-content .camp-porteur .camp-port-img {
		width: 50px;
		height: 50px;
		overflow: hidden;
		border-radius: 50%;
		border: #d2d2d2 1px solid;
	}

	.camp-detail-content .camp-porteur .camp-port-img img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.camp-detail-content .camp-porteur span {
		font-size: 22px;
		font-weight: 600;
		color: #000;
	}

	/* 	.camp-detail-content .btn-change {
		padding: 8px 15px;
		border-radius: 10px;
		border: 1px solid #43C9B7;
		background-color: #43C9B7;
		text-decoration: none;
		color: #fff;
		float: right;
		transition: all .4s;
		font-size: 1.5rem;
		font-weight: 600;
	} */

	.camp-detail-content .side-action {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: end;
		text-align: center;
		margin-bottom: 5px;

	}

	.camp-detail-content .side-action .action-txt {
		text-align: center;
		width: 220px;
		display: block;
		position: relative;
	}


	.camp-detail-content .side-action .nb-action {
		font-size: 45px;
		font-weight: 700;
		color: var(--default-button-color);
		display: block;
	}

	.camp-detail-content .side-action .name-action {
		font-size: 22px;
		font-weight: 500;
		color: var(--default-button-color);
		margin-bottom: 5px;
		margin-top: -10px;
		display: block;
	}

	.camp-detail-content .btn-action-comm {
		padding: 10px 18px;
		border-radius: 10px;
		border: 1px solid var(--default-button-color);
		background-color: transparent;
		color: var(--default-button-color);
		transition: all .4s;
		font-size: 22px;
		font-weight: 600;
		width: 100%;
		text-decoration: none;
		position: relative;
	}

	.camp-detail-content .side-action .btn-action-comm.active {
		background-color: var(--active-button-color);
		color: #fff;
	}

	.camp-detail-content .financement-detail .finance-content {
		display: flex;
		align-items: center;
		justify-content: space-between;
		padding: 35px 5px 15px 7px;
	}

	.camp-detail-content .financement-detail .finance-content .title {
		font-size: 20px;
		font-weight: 800;
		color: #494646;
		display: block;
	}

	.camp-detail-content .financement-detail .finance-content .chiffre {
		font-size: 19px;
		font-weight: 700;
		color: grey;
		display: block;
	}

	.camp-detail-content .financement-detail .finance-content .financeur {
		font-size: 22px;
		font-weight: 900;
		color: #000;
		display: block;
	}

	.camp-detail-content .financement-detail .progress-content {
		padding: 0 7px 10px 9px;
	}

	.camp-detail-content .financement-detail .progress-content .progress {
		height: 40px;
		border-radius: 15px;
	}

	.camp-detail-content .financement-detail .progress-content .progress .progress-bar {
		background-color: #4623C9;
		display: flex;
		justify-content: center;
		align-items: center;
		font-size: 18px;
	}

	.camp-detail-content .btn-cofinance-content {
		display: block;
		position: relative;
		padding: 10px 9px;
	}

	.camp-detail-content .btn-cofinance-content .btn-cofinance {
		padding: 10px 18px;
		border-radius: 10px;
		border: 5px solid #4623C9;
		background-color: #4623C9;
		color: #fff;
		transition: all .4s;
		font-size: 22px;
		font-weight: 600;
		width: 220px;
		float: right;
	}

	.aac-tl-edit,
	.aac-tl-select,
	.aac-tl-comments,
	.aac-tl-delete,
	.aac-tl-canEdit-form {
		float: right;
	}

	.commun-project {
		float: left;
	}

	/* .aac-tl-comments.has-not-seen-comments  .edite-txt::after {
		content: "";
		position: relative;
		top: -1.5rem;
		right: -1rem;
		background-color: #6300cb;
		color: white;
		border-radius: 50%;
		padding: 0px 5px;
		font-size: 11px;
		font-weight: bold;
	} */

	.aac-tl-select {
		transition: all .3s;
	}

	.aac-tl-select.checked {
		font-weight: 600;
		background-color: var(--active-button-color);
		color: #fff;
	}

	.action-txt {
		position: relative;
		display: block;
	}

	.action-txt .list-contributor,
	.action-txt .list-love,
	.list-user {
		padding: 8px;
		max-height: 200px;
		overflow-y: auto;
		position: absolute;
		z-index: 10;
		display: none;
		background-color: #fff;
		box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
		border-radius: 10px;
		width: max-content;
		right: 0;
	}

	.action-txt .list-contributor .close-i,
	.action-txt .list-love .close-i,
	.action-txt .list-user .close-i {
		margin-bottom: 5px;
		display: flex;
		justify-content: end;
		width: 100%;
	}

	.action-txt .list-contributor .close-i i,
	.action-txt .list-love .close-i i,
	.action-txt .list-user .close-i i {
		cursor: pointer;
	}

	.action-txt .list-contributor .img-link,
	.action-txt .list-love .love-link,
	.action-txt .list-user .user-link {
		margin-top: 5px;
	}

	.action-txt .list-contributor.show,
	.action-txt .list-love.show,
	.action-txt .list-user.show {
		display: flex !important;
		flex-direction: column;
		justify-content: start;
		align-items: baseline;
	}

	.use-list {
		display: flex;
		flex-direction: column;
		justify-content: start;
		align-items: baseline;
	}

	.action-txt .list-contributor img,
	.action-txt .list-love img,
	.action-txt .list-user img {
		width: 25px;
		height: 25px;
		border-radius: 50%;
		cursor: pointer;
		margin-right: 2px;
	}

	#dropdown-contrib,
	#dropdown-love {
		position: absolute;
		top: 90px;
		min-width: 5px !important;
		right: 0;
		left: auto !important;
		padding: 4px;
	}

	#dropdown-contrib img,
	#dropdown-love img {
		width: 25px;
		height: 25px;
		margin-bottom: 3px;
		border-radius: 50%;
		display: block !important;
	}

	.btn-more-contrib,
	.btn-more-love {
		background-color: transparent;
		color: #000;
		border: 1px solid grey;
		width: 25px;
		height: 25px;
		text-align: center;
		border-radius: 50%;
	}

	.camp-detail-content .campDesc {
		min-height: 2px !important;
	}

	.camp-detail-content .camp-detail-title a {
		margin-top: 2.2%;
	}

	.modal-header.set-sticky {
		position: sticky;
		z-index: 100;
		background-color: #fff;
	}

	#dialogContentBody .modal-header .modal-title {
		text-transform: none !important;
	}

	.nativ-text-ellipsis {
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}

	.aac-parents {
		flex-direction: row;
		display: flex;
		flex-wrap: wrap;
		gap: 1em;
	}
	.card-collection {
		width: 100%;
		max-width: 25em;
		height: 80px;
		background: linear-gradient(#FEFEFF, var(--dtl-costum-color3));
		border-radius: 20px;
		display: flex;
		align-items: center;
		justify-content: left;
		transition: 0.5s ease-in-out;
		box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
		flex: 1 0 25em;
	}

	.card-collection:hover {
		cursor: pointer;
		transform: scale(1.05);
		text-decoration: none;
	}

	.card-collection:focus {
		text-decoration: none;
	}

	.card-collection-img {
		width: 50px;
		height: 50px;
		margin-left: 10px;
		border-radius: 50px;
		box-shadow: 0 1px 2px rgb(29 28 28 / 20%);
	}

	.card-collection-text-box {
		width: calc(100% - 90px);
		margin-left: 10px;
		color: #353535;
		/* font-family: "Poppins" sans-serif; */
	}

	.card-collection-text-content {
		display: flex;
		align-items: center;
		justify-content: space-between;
	}

	.card-collection-h1 {
		font-size: 16px;
		font-weight: bold;
	}

	.card-collection-p {
		font-size: 12px;
		font-weight: lighter;
	}

	.modal-this-small .modal-content .close-modal {
		display: none;
	}
	.modal-this-small .modal-content {
		padding: 0;
		min-height: 20vh;
	}
	.modal-this-small .modal-content #dialogContentBody {
		padding: 0;
	}

	.thisfinder-list-selection {
		height: 200px;
    	overflow-y: auto;
	}
	.thisfinder-item .checkbox-content {
		padding: 7px 10px 0px 0px;
	}
	.thisfinder-item .thumb-send-to {
		object-fit: none;
	}

	@media (min-width: 768px) {
		#dialogContentBody .section-nav-body {
			width: 11vw;
		}
		#dialogContent .modal-this-small {
			position: relative;
			width: 600px;
			padding-left: 0;
			padding-right: 0;
			margin-left: auto;
			margin-right: auto;
		}
	}

	@media (max-width : 550px) {
		.camp-detail-content .camp-detail-title .com-title {
			font-size: 22px !important;
		}

		.camp-detail-content .camp-detail-title .edite-txt {
			display: none;
		}

		.porteurDesc {
			padding: 0 !important;
			font-size: xx-small;
		}

		.porteurDesc p,
		.porteurDesc li {
			font-size: 14px !important;
		}

		.camp-detail-content .side-action .action-txt {
			display: none;
		}

		.contrib-pastil,
		.interest-pastil {
			display: block !important;
		}

		.camp-detail-content .btn-action-comm {
			font-size: 18px !important;
			width: 185px !important;
		}

		.btn-action-content {
			width: 185px !important;
		}

		.camp-detail-content .side-action {
			align-items: center !important;
			margin-bottom: 15px !important;
		}

		.titleAndDesc-content .text-title {
			font-size: 22px !important;
		}

		.titleAndDesc-content .textDesc {
			font-size: 14px !important;
		}

		.camp-detail-content .tags-content .tags {
			font-size: 14px;
			padding: 3px 6px;
		}
		.card-collection {
			width: 100%;
			max-width: 100%;
		}
	}

	@media (max-width: 767px) {
		.aac-parents {
			justify-content: center;
		}
	}

	.contrib-pastil,
	.interest-pastil {
		display: none;
		position: absolute;
		top: -10px;
		right: -10px;
		border-radius: 50%;
		background-color: #4623C9;
		font-size: 14px;
		color: #fff;
		padding: 2px 8px;
		z-index: 10;
	}

	.btn-action-content {
		display: block;
		position: relative;
		width: 220px;
	}

	.container-preview .graph-btn-icon {
		display: none !important;
	}

	.aac-tl-select .visible-xs {
		display: inline-block !important;
	}

	.camp-porteur .option-btn {
		display: flex;
		justify-content: space-around;
		align-items: center;
	}

	.camp-porteur .option-btn a:first-child {
		margin-right: 5px;
	}

	.desc-content {
		max-height: 300px;
		overflow: hidden;
		position: relative;
		transition: max-height 2s ease;
		font-family: "lunchType";
	}

	.desc-content li,
	.desc-content ul,
	.desc-content a,
	.desc-content p,
	.desc-content span,
	.desc-content h1,
	.desc-content h2,
	.desc-content h3,
	.desc-content h4 {
		font-family: "lunchType" !important;
	}

	#read-more-btn {
		margin-top: 10px;
		display: none;
		padding: 10px 15px;
		border-radius: 10px;
		background-color: #4623c9;
		color: #fff;
		border: #4623C9 1px solid;
		transition: all .4s;
		margin-left: 8px;
	}

	#read-more-btn:hover {
		background-color: transparent;
		color: #4623c9;
		border: #4623C9 1px solid;
	}

	.desc-content.expanded {
		max-height: 200vh !important;
	}

	.desc-content.collapsed {
		max-height: 300px;
		transition: max-height 2s ease;
	}

	.display-flex {
		display: flex;
	}

	.nb-action {
		cursor: pointer;
	}

	.aac-tl-comments.has-not-seen-comments[data-not-seen-comments] .edite-txt::after {
		content: attr(data-not-seen-comments);
		position: absolute;
		margin-top: -1.8rem;
		border-radius: 50%;
		background-color: #4623C9;
		font-size: 14px;
		color: #fff;
		padding: 3px 10px;
		z-index: 10;
	}

	.head-detail {
		display: flex;
		/* align-items: center; */
		margin-bottom: 20px;
		justify-content: space-between;
	}

	.ans-img-content {
		/* margin-right: 20px; */
		width: 15%;
	}

	.ans-img-content .img-ans {
		width: 100%;
		display: flex;
		justify-content: end;
	}

	.ans-img-content #communMyImg {
		border-radius: 30px;
		cursor: pointer;
	}

	/* The Modal (background) */
	.ans-img-content .img-modal {
		position: fixed;
		z-index: 1;
		padding-top: 100px;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		overflow: auto;
		background-color: black;
	}

	/* Modal Content */
	.ans-img-content .img-modal-content {
		position: relative;
		margin: auto;
		padding: 0;
		width: 90%;
		max-width: 1200px;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
	}

	/* The Close Button */
	.ans-img-content .img-modal .close {
		color: white;
		position: absolute;
		top: 170px;
		right: 25px;
		font-size: 80px;
		font-weight: 200;
		z-index: 10;
		opacity: 1 !important;
	}

	.ans-img-content .img-modal .close:hover,
	.ans-img-content .img-modal .close:focus {
		color: #999;
		text-decoration: none;
		cursor: pointer;
	}

	.ans-img-content .img-modal .mySlides {
		display: none;
		margin-top: 100px;
		width: 45%;
		position: relative;
	}

	.ans-img-content .img-modal .cursor {
		cursor: pointer;
	}

	/* Next & previous buttons */
	.ans-img-content .img-modal .prev,
	.ans-img-content .img-modal .next {
		cursor: pointer;
		position: absolute;
		top: 300px;
		width: auto;
		padding: 16px;
		margin-top: -50px;
		color: white;
		font-weight: bold;
		font-size: 20px;
		transition: 0.6s ease;
		border-radius: 0 3px 3px 0;
		user-select: none;
		-webkit-user-select: none;
		text-decoration: none;
	}

	.ans-img-content .img-preview-demo {
		display: flex;
		justify-content: center;
		align-items: center;
		max-width: 1200px;
		overflow-x: auto;
	}
	.ans-img-content .img-preview-demo .column {
		margin-right: 5px;
	}
	.ans-img-content .img-modal .prev {
		left: 0;
		border-radius: 3px 0 0 3px;
	}

	/* Position the "next button" to the right */
	.ans-img-content .img-modal .next {
		right: 0;
		border-radius: 3px 0 0 3px;
	}

	/* On hover, add a black background color with a little bit see-through */
	.ans-img-content .img-modal .prev:hover,
	.ans-img-content .img-modal .next:hover {
		text-decoration: none;
	}

	/* Number text (1/3 etc) */
	.ans-img-content .img-modal .numbertext {
		color: #f2f2f2;
		font-size: 16px;
		padding: 8px 12px;
		position: absolute;
		top: -30px;
		left: 0;
	}


	.ans-img-content .img-modal .caption-container {
		text-align: center;
		background-color: black;
		padding: 2px 16px;
		color: white;
	}

	.ans-img-content .img-modal .demo {
		opacity: 0.6;
	}

	.ans-img-content .img-modal .active,
	.ans-img-content .img-modal .demo:hover {
		opacity: 1;
	}

	.ans-img-content img.hover-shadow {
		transition: 0.3s;
	}

	.aac-parents-container .title-text {
		font-size: 2em;
		font-weight: 500;
		color: var(--default-button-color);
		margin-bottom: 5px;
		margin-top: -10px;
		display: block;
	}

	.ans-img-content .img-modal .hover-shadow:hover {
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
	}

	/* 100% Image Width on Smaller Screens */
	@media only screen and (max-width: 700px) {
		.img-modal-content {
			width: 100%;
		}
	}

	.commun-action {
		position: absolute;
		top: -40px;
		display: flex;
	}

	.camp-title-content {
		max-width: 85%;
	}
	.com-link {
		display: inline-block;
		font-size: 26px;
		margin-left: 10px;
	}

	.swipe-navigation {
		position: fixed;
		top: 50%;
		transform: translateX(-50%);
		background: white;
		width: 40px;
		height: 40px;
		border-radius: 50%;
		color: #ababab;
		border: 1px solid #ababab;
		display: flex;
		justify-content: center;
		align-items: center;
		transition: border .2s;
	}

	.swipe-navigation.active {
		border-width: 5px;
	}

	.swipe-navigation.swipe-prev {
		left: -40px;
	}

	.swipe-navigation.swipe-next {
		left: calc(100vw + 40px);
	}

	.camp-detail-title .commun-action .commun-project img {
		width: 34px;
		height: 34px;
		border-radius: 50%;
		border: 1px var(--dtl-costum-color1) solid;
	}

	@media (max-width : 500px) {
		.camp-detail-content {
			padding: 4% 8% 8%;
		}

		.camp-detail-content .action-cont {
			align-items: center !important;
		}

		.head-detail {
			display: flex;
			margin-bottom: 20px;
			justify-content: space-between;
			flex-direction: column-reverse;
			padding-top: 10px;
		}
		.ans-img-content {
			width: 100%;
			display: block;
		}
		.ans-img-content .img-ans {
			width: 100%;
			display: flex;
			justify-content: start;
		}
		#communMyImg {
			width: 100%;
		}
	}
	.projectListModal {
		top: 160px;
		z-index: 9999999999999;
	}
	.select2-drop.select2-drop-active {
		z-index: 99999999999999999;
	}
	.projectListModal .select2-container {
		padding: 0 !important;
	}
</style>

<div class="camp-detail-content <?= $kunik ?> <?= $kunik ?>-css" data-refreshblock-contrib="<?= $myCmsId ?>" data-refreshblock-projet="<?= $myCmsId ?>">
	<div class="row">
		<div class="col-lg-10 col-xs-12">
			<div class="head-detail">
				<div class="camp-title-content">
					<div class="camp-detail-title">
						<?php if (!isset($blockCms["disableEdition"]) || (isset($blockCms["disableEdition"]) && !filter_var($blockCms["disableEdition"], FILTER_VALIDATE_BOOLEAN))) { ?>
							<div class="commun-action">
								<a href="javascript:;" class="commun-project lbh-preview-element lbh bs-mr-2"><img class="tooltips" data-toggle="tooltip" title="" src="" alt=""></a>
								<a href="javascript:;" class="aac-tl-select btn btn-default bs-mr-2">
									<i class="fa fa-check-circle visible-xs"></i>
									<span class="edite-txt"><?= Yii::t("cms", "Select")?></span>
								</a>
								<a href="javascript:;" class="aac-tl-edit btn btn-default bs-mr-2">
									<i class="fa fa-edit"></i>
									<span class="edite-txt"><?= Yii::t("cms", "Edit")?></span>
								</a>
								<a href="javascript:;" class="aac-tl-canEdit-form btn btn-default bs-mr-2" data-toggle="tooltip" title="<?= Yii::t("cms", "Add contributors")?>">
									<i class="fa fa-users"></i>
								</a>
								<a href="javascript:;" class="aac-tl-project btn btn-default bs-mr-2" data-toggle="tooltip" title="<?= Yii::t("cms", "Associate with a project")?>">
									<i class="fa fa-lightbulb-o"></i>
								</a>
								<a href="javascript:;" class="aac-tl-delete btn btn-default" data-toggle="tooltip" title="<?= Yii::t("cms", "Delete the common")?>">
									<i class="fa fa-trash"></i>
									<!-- <span class="edite-txt">Editer</span> -->
								</a>
							</div>
						<?php } else { ?>
								<div class="commun-action">
									<a href="javascript:;" class="select-commun btn btn-default bs-mr-2">
										<i class="fa fa-check-circle visible-xs"></i>
										<span class="edite-txt"><?= Yii::t("cms", "Select")?></span>
									</a>
								</div>
						<?php } ?>
						<span class="com-title"><?= $blockCms["title"] ?></span>
						<a href="javascript:;" target="_blank" class="com-link"><i class="fa fa-external-link"></i></a>
					</div>
					<div class="campSwipe campDesc campDesc<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>">

					</div>
					<div class="campSwipe tags-content">
						<span class="campSwipe tags">#tags</span>
						<span class="campSwipe tags">#tags</span>
						<span class="campSwipe tags">#tags</span>
					</div>
					<div class="tags-content global-tags bs-pt-0">

					</div>
				</div>
				<div class="ans-img-content">
					<div class="img-ans">
						<img id="communMyImg" class="img-responsive" src="" alt="image preview" width="90%">
						<div id="imgModal" class="img-modal hide">
							<span class="close cursor">&times;</span>
							<div class="img-modal-content">

								<a class="prev">&#10094;</a>
								<a class="next">&#10095;</a>

								<div class="caption-container">
									<p id="captionImg"></p>
								</div>
								<div class="img-preview-demo">

								</div>

								<!-- <img class="img-modal-content" id="img01">

								<div id="captionImg"></div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="desc-content collapsed" id="desc-content">
				<div class="porteurDesc porteurDesc<?= $blockKey ?>" id="porteurDesc" data-id="<?= $blockKey ?>">

				</div>
			</div>
			<button id="read-more-btn"><?= Yii::t("cms", "View more")?>...</button>
			<?php if (isset($blockCms["disableEdition"]) && filter_var($blockCms["disableEdition"], FILTER_VALIDATE_BOOLEAN)) { ?>
				<div id="aac_parentsContainer" class="aac-parents-container bs-mt-3">

				</div>
			<?php } ?>
		</div>
		<div class="campSwipe col-lg-2 col-xs-12 action-cont">
			<div class="campSwipe camp-porteur">
				<!-- <div class="camp-port-img">
					<img src="<?= Yii::app()
									->getModule('co2')
									->getAssetsUrl() . '/images/thumb/default_persons.png' ?>" alt="Porteur">
				</div> -->
				<!-- <a class="name-link" href="javascript:;" data-id="">
				<span>Porteur du commun</span>
				</a> -->
				<div class="option-btn">
					<a class="btn btn-change btn-default btn-forum" data-target="aapStep1lzi6plod1j27tg99qa2">Forum</a>
					<a class="btn btn-change btn-default btn-discussion" data-target="aapStep1lzi6qmw1y98vifhvcjl">Chat</a>
				</div>
				<div class="optin-btn">
					<a href="javascript:;" class="aac-tl-comments btn btn-default bs-mr-2 display-flex">
						<i class="fa fa-comments bs-pt-2 bs-pr-2"></i>
						<p class="edite-txt bs-m-0"><?= Yii::t("cms", "Comments")?></p>
					</a>
				</div>
			</div>
			<div class="campSwipe side-action">
				<div class="campSwipe action-txt">
					<span class="nb-action aap-contributor-count">23</span>
					<!-- <div id="contrib<?= $blockKey ?>" class=""> -->
					<span class="list-contributor"><span class="close-i"><i class="fa fa-close"></i></span></span>
					<div id="dropdown-contrib" class="dropdown-menu"></div>
					<!-- </div> -->
					<span class="name-action"><?= Yii::t("cms", "Contributor")?>(s)</span>
				</div>
				<div class="btn-action-content">
					<span class="contrib-pastil">0</span>
					<button class="btn-action-comm aap-contributor-count">
						<?= Yii::t("cms", "I contribute")?>
					</button>
				</div>
			</div>
			<div class="campSwipe side-action">
				<div class="campSwipe action-txt">
					<span class="nb-action aap-love-count">45</span>
					<!-- <div id="interes<?= $blockKey ?>" class=""> -->
					<span class="list-love"><span class="close-i"><i class="fa fa-close"></i></span></span>
					<div id="dropdown-love" class="dropdown-menu"></div>
					<!-- </div> -->
					<span class="name-action"><?= Yii::t("cms", "Interested")?>(s)</span>
				</div>
				<div class="campSwipe btn-action-content">
					<span class="interest-pastil">0</span>
					<button class="btn-action-comm aap-love-count"><?= Yii::t("cms", "I'm interested")?></button>
				</div>
			</div>
			<div class="campSwipe side-action">
				<div class="campSwipe action-txt">
					<span class="nb-action aap-tl-count">0</span>
					<span class="list-user"><span class="close-i"><i class="fa fa-close"></i></span>
						<div class="use-list"></div>
					</span>
					<div id="dropdown-user" class="dropdown-menu"></div>
					<span class="name-action"><?= Yii::t("cms", "User")?>(s)</span>
				</div>
				<div class="campSwipe btn-action-content">
					<!-- <span class="contrib-pastil">5</span> -->
					<button class="btn-action-comm aap-tl-count"><?= Yii::t("cms", "I use")?></button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade projectListModal" id="projectListModal" role="dialog">
		<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Associer a un projet</h4>
			</div>
			<div class="modal-body">
				<div>
					<label for="projectList">Selectionner un project</label>
					<input type="text" class="form-control projectList" data-id="" data-path="projectList" value="" data-collection="" id="projectList" placeholder="">
				</div>
				<hr>
				<div class="generate-content">
					<button class="generateProject btn btn-success btn-sm btn-block">Generer le projet</button>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			</div>
		</div>
		</div>
	</div>
	<div class="campSwipe financement-detail">
		<!--<div class="finance-content">
			<div class="objectif">
				<span class="title">Objectif : </span>
				<span class="chiffre">1890 <i class="fa fa-eur"></i></span>
			</div>
			<div class="financeur">
				<span class="">3 </span>
				<span class=""> Coﬁnanceurs</span>
			</div>
			<div class="recolte">
				<span class="title">Récolté : </span>
				<span class="chiffre">1520 <i class="fa fa-eur"></i></span>
			</div>
		</div>-->
		<!--<div class="progress-content">
			<div class="progress">
				<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
					70%
				</div>
			</div>
		</div>
		<div class="btn-cofinance-content">
			<button class="btn-cofinance">Je cofinance</button>
		</div>-->
	</div>
	<div class="swipe-navigation swipe-prev">
		<i class="fa fa-arrow-left"></i>
	</div>
	<div class="swipe-navigation swipe-next">
		<i class="fa fa-arrow-right"></i>
	</div>
</div>
<script>
	$(document).ready(function() {
		var content = $('.porteurDesc<?= $blockKey ?>');
		var container = $('.desc-content');
		var readMoreBtn = $('#read-more-btn');
		readMoreBtn.click(function() {
			if (container.hasClass('collapsed')) {
				container.removeClass('collapsed').addClass('expanded');
				readMoreBtn.text('<?= Yii::t("cms", "View less")?>');
			} else {
				container.removeClass('expanded').addClass('collapsed');
				readMoreBtn.text('<?= Yii::t("cms", "View more")?>...');
			}
		});
	});
</script>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode($blockCms); ?>;
	var str = "";
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#css-<?= $kunik ?>").append(str);
	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
		var campDetail = {
			configTabs: {
				style: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
			},
			afterSave: function(path, valueToSet, name, payload, value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
			}
		}
		cmsConstructor.blocks.campDetail<?= $myCmsId ?> = campDetail;
	}
</script>
<script type="text/javascript">
	$(function() {
		var aap = new aapv2();
		aap.answer(aap.url().dot('communId'));
		var selectionData = {
			selectedAac: {}
		}
		var default_img = modules.co2.url + '/images/thumbnail-default.jpg'
		$(".nb-action.aap-contributor-count").off().click(function(e) {
			e.stopImmediatePropagation()
			if ($(this).text() == 0)
				return
			$('.action-txt .list-user').removeClass('show');
			$('.action-txt .list-love').removeClass("show")
			if ($('.action-txt .list-contributor').hasClass("show")) {
				$('.action-txt .list-contributor').removeClass("show")
			} else {
				$('.action-txt .list-contributor').addClass("show")
			}
		})

		$(".nb-action.aap-love-count").off().click(function(e) {
			e.stopImmediatePropagation()
			if ($(this).text() == 0)
				return
			$('.action-txt .list-user').removeClass('show');
			$('.action-txt .list-contributor').removeClass("show")
			if ($('.action-txt .list-love').hasClass("show")) {
				$('.action-txt .list-love').removeClass("show")
			} else {
				$('.action-txt .list-love').addClass("show")
			}
		})

		$(".nb-action.aap-tl-count").off().click(function(e) {
			e.stopImmediatePropagation()
			if ($(this).text() == 0)
				return
			$('.action-txt .list-love').removeClass("show")
			$('.action-txt .list-contributor').removeClass("show")
			if ($('.action-txt .list-user').hasClass("show")) {
				$('.action-txt .list-user').removeClass("show")
			} else {
				$('.action-txt .list-user').addClass("show")
			}
		})

		$(document).on('click', function(e) {
			if (!$(e.target).closest('.nb-action.aap-tl-count, .action-txt .list-user').length) {
				$('.action-txt .list-user').removeClass('show');
			}
			if (!$(e.target).closest('.nb-action.aap-love-count, .action-txt .list-love').length) {
				$('.action-txt .list-love').removeClass('show');
			}
			if (!$(e.target).closest('.nb-action.aap-contributor-count, .action-txt .list-contributor').length) {
				$('.action-txt .list-contributor').removeClass('show');
			}
		});

		$(".action-txt .list-love .close-i i, .action-txt .list-contributor .close-i i, .action-txt .list-user .close-i i").click(function() {
			var self = $(this)
			self.parent().parent().removeClass("show")
		})

		var slideIndex = 1;

		function showSlides(n) {
			var i;
			var slides = $("#imgModal .mySlides");
			var dots = $("imgModal .demo");
			var captionText = $("#captionImg");

			if (n > slides.length) { slideIndex = 1; }
			if (n < 1) { slideIndex = slides.length; }

			slides.hide();  
			dots.removeClass("active"); 

			slides.eq(slideIndex - 1).show(); 
			dots.eq(slideIndex - 1).addClass("active"); 
			captionText.text(dots.eq(slideIndex - 1).attr("alt"));  
		}

		function openModal() {
			$("#imgModal").removeClass("hide");
		}

		function closeModal() {
			$("#imgModal").addClass("hide");
		}

		function plusSlides(n) {
			showSlides(slideIndex += n);
		}

		function currentSlide(n) {
			showSlides(slideIndex = n);
		}

		function handle_use_click() {
			var self,
				form,
				form_option,
				finder_filter,
				text;

			self = $(this);
			if (self.prop('disabled'))
				return;
			text = self.text();
			self.prop('disabled', true);
			self.empty()
				.html('<i class="fa fa-spinner fa-spin"></i>');
			if (!userId) {
				toastr.error('Vous devez d\'abord vous connecter');
				Login.openLogin();
				return;
			}
			finder_filter = {
				$or: {
					'source.keys': 'franceTierslieux',
					'reference.costum': 'franceTierslieux',
					'mainTag': 'TiersLieux'
				}
			};
			finder_filter['links.members.' + userId + '.type'] = 'citoyens';
			finder_filter['links.members.' + userId + '.isAdmin'] = true;
			form_option = {
				type: 'bootbox'
			};
			aap.xhr()
				.get_tl_use(true)
				.then(function(resp) {
					var tls, value;

					tls = resp.tls;
					value = {};
					$.each(tls, function(_, tl) {
						value[tl.id] = {
							_id: {
								$id: tl.id
							},
							name: tl.name,
							type: tl.type
						};
					});
					form = {
						jsonSchema: {
							title: 'Un tiers-lieu utilise',
							properties: {
								tl: {
									label: 'Mes tiers-lieux',
									inputType: 'finder',
									filters: finder_filter,
									initType: ['organizations'],
									initBySearch: true,
									initMe: false,
									initContacts: false,
									initContext: false,
									noResult: { 
										label: 'Créez votre tiers lieux',
										action: function () {
											var customForm = {
												"beforeBuild" : {
													"properties" : {
																
													}    
												}
											};
											var extendedForm=customForm;
											if(typeof costum!="undefined" && costum!=null && costum.typeObj.organizationsTls !="undefined" && costum.typeObj.organizationsTls.dynFormCostum!="undefined") 
												extendedForm=$.extend(true,costum.typeObj.organizationsTls.dynFormCostum,customForm);
									
											dyFObj.openForm("organization",null,null, null, extendedForm);
											$(".bootbox").modal('hide');
										}
									},
									values: value
								}
							},
							save: function(data) {
								aap.xhr()
									.tl_use(data.tl)
									.then(function(resp) {
										do_reload_tl_use();
										dyFObj.closeForm();
									});
							}
						}
					};
					dyFObj.openForm(form, null, null, null, null, form_option);
					dyFObj.onclose = function() {
						self.prop('disabled', false)
							.text(text);
					}
				}).finally(function() {

				});
		}

		function add_form_contributor(answer) {

			var form,
				form_option,
				value

			if (!userId) {
				toastr.error('Vous devez d\'abord vous connecter');
				Login.openLogin();
				return;
			}
			// form_option = {
			// 	type: 'bootbox'
			// };

			value = {}
			if(typeof answer.canEditForm != 'undefined' && answer.canEditForm != null){
				$.each(answer.canEditForm, function(_id, canEdit) {
					value[_id] = {
						_id: {
							$id: _id
						},
						name: canEdit.name,
						profilThumbImageUrl: canEdit.profilThumbImageUrl ? canEdit.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png",
						type: canEdit.type
					};
				});
			}

			form = {
				jsonSchema: {
					title: "<?= Yii::t("cms", "Contributor to the edition")?>",
					properties: {
						canEdit: {
							label: '<?= Yii::t("cms", "The citizens")?>',
							inputType: 'finder',
							filters: {},
							initType: ['citoyens'],
							initBySearch: true,
							initMe: false,
							initContacts: false,
							initContext: false,
							values: value
						}
					},
					save: function(data) {
						mylog.log('datako', data);
						var canEditData = {};
						var canEditDataAns = {};
						$.each(data.canEdit, function(key, value) {
							canEditData[key] = {
								name : value.name,
								type : value.type,
								profilThumbImageUrl : finder.fullObject?.canEdit?.[key]?.profilThumbImageUrl ? finder.fullObject.canEdit[key].profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png" 
							};
							canEditDataAns[key] = {
								id : key,
								name : value.name,
								type : value.type,
								img : finder.fullObject?.canEdit?.[key]?.profilThumbImageUrl ? finder.fullObject.canEdit[key].profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png" 
							};
						})
						var params = {
							id: answer.id,
							collection: 'answers',
							path: 'links.canEdit',
							value: canEditData
						}

						dataHelper.path2Value(params, function(__response) {
							if (__response.result && __response.result === true) {
								mylog.log(__response.result, '_response')
								if (notEmpty(answer.answers_links)) {
									if (typeof answer.answers_links.canEdit != "undefined") {
										dataHelper.path2Value(
											{
												id: answer.id,
												collection: 'answers',
												path: answer.answers_links.canEdit,
												value: canEditDataAns
											}, 
											function() {}
										)
									}
								}
								if ($(`[data-refreshblock-contrib]`).length > 0) {
									$(`[data-refreshblock-contrib]`).each((index, elem) => {
										const blockId = $(elem).attr("data-refreshblock-contrib")
										if (typeof refreshBlock == "function") {
											refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
										}
									})
								}
								toastr.success("Publishing rights to contributor add!");
								dyFObj.closeForm();
							}
						});
					}
				}
			};
			dyFObj.openForm(form, null, null, null, null);
		}

		function remove_project(answer) {
			var today = new Date();
			var params = {
				id: answer.id,
				collection: 'answers',
				path: 'project',
				value: null
			}
			dataHelper.path2Value(params, function(__response) {
				if (__response.result && __response.result === true) {
					mylog.log(__response.result, '_response')
					if ($(`[data-refreshblock-projet]`).length > 0) {
						$(`[data-refreshblock-projet]`).each((index, elem) => {
							const blockId = $(elem).attr("data-refreshblock-projet")
							if (typeof refreshBlock == "function") {
								refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
							}
						})
					}
					toastr.success("Commun dissocié du projet");
				}
			});
		}
		function add_project(answer) {

			var form,
				form_option,
				value

			if (!userId) {
				toastr.error('Vous devez d\'abord vous connecter');
				Login.openLogin();
				return;
			}

			form = {
				jsonSchema: {
					title: "<?= Yii::t("cms", "Associate with a project")?>",
					properties: {
						projectList: {
							label: '<?= Yii::t("cms", "Projects")?>',
							inputType: 'finder',
							filters: {},
							initType: ['projects'],
							initBySearch: true,
							initMe: false,
							initContacts: false,
							initContext: false,
							multiple: false,
							rules: {
								required: true
							},
							noResult: { 
								label: "Generer le projet",
								action: function () {
									var today = new Date();
									var tplCtxCollection = "answers";
									var name = answer.titre ?? "Commun";
									var cntxtId = costum.contextId;
									var cntxtType = costum.contextType;
									var statusproject = answer.statusproject ?? [];
									
									var generateProject = function(){
										coInterface.showLoader("#finderSelectHtml");
										ajaxPost("", baseUrl+'/survey/form/generateproject/answerId/'+answer.id+'/parentId/'+cntxtId+'/parentType/'+cntxtType,
											null,
											function(data){
												tplCtx.id = answer.id;
												tplCtx.collection = tplCtxCollection;
												tplCtx.path = "project";
												tplCtx.value = {
													"id" : data["projet"],
													"startDate" : today.getDay()+'/'+today.getMonth()+"/"+today.getFullYear()
												};
												dataHelper.path2Value( tplCtx, function(params) {} );

												tplCtx.path = "step";
												tplCtx.value = "";

												dataHelper.path2Value( tplCtx, function(params) {
													tplCtx4 = {};
													tplCtx4.id = answer.id;
													tplCtx4.collection = tplCtxCollection;
													tplCtx4.value = statusproject;

													tplCtx4.value.push("projectstate");
													tplCtx4.path = "status";
													dataHelper.path2Value( tplCtx4, function(params){
														if ($(`[data-refreshblock-projet]`).length > 0) {
															$(`[data-refreshblock-projet]`).each((index, elem) => {
																const blockId = $(elem).attr("data-refreshblock-projet")
																if (typeof refreshBlock == "function") {
																	refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
																}
															})
														}
														$(".bootbox").modal('hide');
													});
												} );
											},
										"html");
									}
									generateProject();
								}
							}
						}
					},
					save: function(data) {
						mylog.log('datako', data);
						var today = new Date();
						var params = {
							id: answer.id,
							collection: 'answers',
							path: 'project',
							value: {}
						}
						var proId = Object.keys(data.projectList)[0];
						params.value = {
							"id": proId,
							"startDate": today.getDay()+'/'+today.getMonth()+"/"+today.getFullYear()
						}
						dataHelper.path2Value(params, function(__response) {
							if (__response.result && __response.result === true) {
								mylog.log(__response.result, '_response')
								if ($(`[data-refreshblock-projet]`).length > 0) {
									$(`[data-refreshblock-projet]`).each((index, elem) => {
										const blockId = $(elem).attr("data-refreshblock-projet")
										if (typeof refreshBlock == "function") {
											refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
										}
									})
								}
								toastr.success("Commun associé au projet");
								dyFObj.closeForm();
							}
						});
					}
				}
			};
			dyFObj.openForm(form, null, null, null, null,{
				type: "bootbox"
			});
		}

		function handle_interrest_click() {
			var self = $(this);
			if (self.prop('disabled'))
				return;
			self.prop('disabled', true);
			var text_dom = $('.action-txt .aap-love-count');
			var love = self.data('love');
			var loved = Math.abs(love - 1);
			var total = parseInt(text_dom.text());
			var text = self.text();

			self.empty().html('<i class="fa fa-spinner fa-spin"></i>');
			sessionStorage.removeItem('firelove');
			aap.xhr()
				.rate(love)
				.then(function(resp) {
					if (resp.result) {
						self.data('love', loved);
						self.text(text);
						if (love) {
							self.addClass(self.data('active-class'));
							var link = $(`<a href="javascript:;">`)
							link.addClass('lbh-preview-element img-link')
							link.attr('data-key', userId)
							var img = `<img data-id="${userId}" src="${typeof userConnected['profilImageUrl'] != 'undefined' && userConnected['profilImageUrl'] != '' ? userConnected['profilImageUrl'] : default_img}" 
							/> ${userConnected.name}`
							link.append(img)
							$('.action-txt .list-contributor').append(link)
							link.click(function() {
								blackDashboard(userId)
							})
							$('.action-txt .list-love').append(link)
						} else {
							self.removeClass(self.data('active-class'));
							$('.action-txt .list-love a[data-key=' + userId + ']').remove()
						}
						text_dom.text(loved ? total - 1 : total + 1);
						$('.interest-pastil').text(loved ? total - 1 : total + 1)
					}
				}).catch(function(err) {
					if (err.code === 103) {
						toastr.error('Vous devez vous connecter');
						sessionStorage.setItem('firelove', '1');
						$('#modalLogin').modal('show');
					}
					self.text(text);
				}).finally(function() {
					self.prop('disabled', false);
				});
		}

		function handle_contribution_click() {
			var self = $(this);
			if (self.prop('disabled'))
				return;
			self.prop('disabled', true);
			var text_dom = $('.action-txt .aap-contributor-count');
			var contribute = self.data('participate');
			var contributed = Math.abs(contribute - 1);
			var total = parseInt(text_dom.text());
			var text = self.text();

			self.empty().html('<i class="fa fa-spinner fa-spin"></i>');
			sessionStorage.removeItem('firecontribute');
			aap.xhr()
				.contribute(userId, contribute)
				.then(function(resp) {
					if (resp.success) {
						self.data('participate', contributed);
						self.text(text);
						if (contribute) {
							self.addClass(self.data('active-class'));
							var link = $(`<a href="javascript:;">`)
							link.addClass('lbh-preview-element img-link')
							link.attr('data-key', userId)
							var img = `<img data-id="${userId}" src="${typeof userConnected['profilImageUrl'] != 'undefined' && userConnected['profilImageUrl'] != '' ? userConnected['profilImageUrl'] : default_img}" 
							/> ${userConnected.name}`
							link.append(img)
							$('.action-txt .list-contributor').append(link)
							link.click(function() {
								blackDashboard(userId)
							})
						} else {
							self.removeClass(self.data('active-class'));
							$('.action-txt .list-contributor a[data-key=' + userId + ']').remove()
						}
						text_dom.text(contributed ? total - 1 : total + 1);
						$('.contrib-pastil').text(contributed ? total - 1 : total + 1)
					}
				}).catch(function(err) {
					if (err.code === 102) {
						toastr.error('Vous devez vous connecter');
						$('#modalLogin').modal('show');
					}
					self.text(text);
				}).finally(function() {
					self.prop('disabled', false);
				});
		}

		function handle_comments_click() {
			dialogContent.open({
				modalContentClass: "modal-custom-lg",
				isRemoteContent: true,
				backdropClick: true,
				shownCallback: () => {
					$("#dialogContent .modal-content .modal-body#dialogContentBody").next().hide()
				},
				hiddenCallback: () => {
					$("#dialogContent #dialogContentBody").empty();
					$("#dialogContent > .modal-content").removeClass("col-xs-12 bs-px-0");
					$("#dialogContent #dialogContentBody").removeClass("col-lg-12 bs-py-0")
					$("#dialogContent .modal-content .modal-body#dialogContentBody").next().show();
					if (
						discussionComments.alreadyNotifyDiscussionMember && typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" &&
						userId && typeof userConnected != "undefined" && userConnected.name
					) {
						discussionSocket.emitEvent(
							wsCO,
							"change_user_activity", {
								userId: userId,
								onUpdate: "userWriting",
								commentId: aap.answer(),
								userActivity: "stopWriting",
							}
						);
						discussionComments.alreadyNotifyDiscussionMember = false;
					}
					get_comments_data()
				}
			})
			aap.xhr()
				.get_discussion_space()
				.then(function(res) {
					$("#dialogContent #dialogContentBody").empty().html(res)
					$("#dialogContent > .modal-content").addClass("col-xs-12 bs-px-0");
					$("#dialogContent #dialogContentBody").addClass("col-lg-12 bs-py-0")
				})
				.catch(function(err) {
					if (err.code === 102) {
						toastr.error('Vous devez vous connecter');
						$('#modalLogin').modal('show');
					}
				}).finally(function() {});

		}

		function handleSelectCommunClick() {
			dialogContent.open({
				modalContentClass: "modal-this-small",
				isRemoteContent: true,
				backdropClick: true,
				shownCallback: () => {
					$("#dialogContent .modal-content .modal-body#dialogContentBody").next().hide()
				},
				hiddenCallback: () => {
					$("#dialogContent #dialogContentBody").empty();
					$("#dialogContent > .modal-content").removeClass("col-xs-12 bs-px-0");
					$("#dialogContent #dialogContentBody").removeClass("col-lg-12 bs-py-0")
					$("#dialogContent .modal-content .modal-body#dialogContentBody").next().show();
					do_reload_answer(aap.answer())
				}
			})
			urlCtrl.getAacList(
				{
					url: baseUrl + "/co2/aap/aac/method/aac_globalautocomplete",
					title: "Choisir un appel à commun",
					selectedList: selectionData.selectedAac,
					closeBtnEvent: () => {
						dialogContent.close()
					},
					acceptBtnEvent: (formData) => {
						ajaxPost(
							"", 
							baseUrl + "/co2/aap/commonaap/action/updateSelection", 
							{
								id: aap.answer(),
								selectionData: formData
							}, 
							function (data) {
								if (data.result) {
									selectionData.selectedAac = formData;
									toastr.success("La sélection a été mise à jour avec succès")
									dialogContent.close()
								}
							}, 
							function (error) {
								mylog.log("ajaxPost error", error)
							}, 
							"json"
						)
					}
				},
				(resHtml) => {
					$("#dialogContent #dialogContentBody").addClass("col-xs-12").empty().html(resHtml)
				}
			)
		}

		function get_comments_data() {
			aap.xhr()
				.get_discussion_space(aap.answer(), null, "json")
				.then(function(res) {
					if (typeof discussionSocket != "undefined" && typeof res.params != "undefined" && res.params.answer && res.params.answer._id) {
						if (typeof discussionComments != "undefined" && (!notEmpty(discussionComments.params) || (discussionComments.params?.answer && discussionComments.params?.answer?._id?.$id != res.params.answer._id))) {
							discussionComments.params = res.params
						}
						discussionSocket.emitEvent(wsCO, "join_discussion_room", {
							userId: userId,
							userName: currentUser.name,
							disableStatus: true
						})
						var connectedFromHereInterval = setInterval(() => {
							if ($(".aac-tl-comments").length < 1) {
								clearInterval(connectedFromHereInterval)
								discussionSocket.emitEvent(wsCO, "user_leave_discussion", {
									userId: userId,
									userName: currentUser.name
								})
							}
						}, 1000);
					}
					var newCommentsCount = discussionComments.params && discussionComments.params.toHighlightComments && discussionComments.params.toHighlightComments.allComments ? discussionComments.params.toHighlightComments.allComments.length : 0;
					if (discussionComments.params && discussionComments.params.toHighlightComments) {
						const exludeKeys = ["allComments"];
						$.each(discussionComments.params.toHighlightComments, function(rubricKey, rubricVal) {
							if (exludeKeys.indexOf(rubricKey) == -1 && discussionComments.params.toHighlightComments[rubricKey]) {
								newCommentsCount += discussionComments.params.toHighlightComments[rubricKey].length
							}
						})
					}
					var hasNotSeenComment = false;
					if (res && typeof res?.params?.allDiverNotSeenComments != "undefined" && res.params.allDiverNotSeenComments > 0) {
						$(".aac-tl-comments").addClass("has-not-seen-comments").attr("data-not-seen-comments", newCommentsCount)
						$(".aac-tl-comments .edite-txt").attr("data-not-seen-comments", newCommentsCount)
						hasNotSeenComment = true;
					} else {
						$(".aac-tl-comments").removeClass("has-not-seen-comments").removeAttr("data-not-seen-comments")
						$(".aac-tl-comments .edite-txt").removeAttr("data-not-seen-comments")
					}
					if (res && typeof res?.params?.orderedQuestion != "undefined" && notEmpty(res?.params?.orderedQuestion)) {
						var iteration = 0
						$.each(res.params.orderedQuestion, (stepIndex, stepVal) => {
							if (stepVal.inputs) {
								$.each(stepVal.inputs, (inputIndex, inputVal) => {
									if (typeof inputVal?.notSeenComments != "undefined" && inputVal.notSeenComments > 0) {
										hasNotSeenComment = true;
									}
								})
							}
							if (iteration == Object.keys(res.params.orderedQuestion).length - 1 && hasNotSeenComment) {
								$(".aac-tl-comments").addClass("has-not-seen-comments").attr("data-not-seen-comments", newCommentsCount)
								$(".aac-tl-comments .edite-txt").attr("data-not-seen-comments", newCommentsCount)
							} else {
								$(".aac-tl-comments").removeClass("has-not-seen-comments").removeAttr("data-not-seen-comments")
								$(".aac-tl-comments .edite-txt").removeAttr("data-not-seen-comments")
							}
							iteration++;
						})
					}
				})
				.catch(function(err) {
					if (err.code === 102) {
						toastr.error('Vous devez vous connecter');
						$('#modalLogin').modal('show');
					}
				}).finally(function() {});
		}

		function do_reset_dom() {
			var loading_str = '<i class="fa fa-spinner fa-spin"></i> Chargement';

			$('.camp-detail-title .com-title').empty().html(loading_str);
			$('.aac-tl-edit, .aac-tl-select, .aac-tl-delete , .aac-tl-canEdit-form').hide();
			// $('.aac-tl-comments').addClass("hide");
			$('.camp-porteur span').empty().html(loading_str);
			$('.btn-change').hide();
			$('.action-txt .aap-contributor-count').text('0');
			$('.action-txt .aap-love-count').text('0');
			$('.btn-action-comm.aap-contributor-count')
				.data('participate', 0)
				.data('answer', 0)
				.data('text', [])
				.prop('disabled', true)
				.off('click', handle_contribution_click);
			$('.btn-action-comm.aap-tl-count')
				.prop('disabled', true);
			$('.btn-action-comm.aap-love-count')
				.data('love', 0)
				.data('answer', 0)
				.data('text', [])
				.prop('disabled', true)
				.off('click', handle_interrest_click);
			$('.tags-content, .tags-content.global-tags').empty();
			$('.porteurDesc').empty();
			$('.campDesc').empty();
			$('.financement-detail .progress-bar')
				.attr('aria-valuenow', 0)
				.css('width', '0%')
				.text('0%');
			$('.objectif .chiffre').empty().html('0 <i class="fa fa-eur"></i>');
			$('.recolte .chiffre').empty().html('0 <i class="fa fa-eur"></i>');
			$('.financeur>span:first').text('0');
			$('.btn-cofinance').attr('data-id', 0);
		}

		function getPartDetail(id, type = 'citoyens') {
			var data = '';
			ajaxPost('', baseUrl + "/co2/element/get/type/" + type + "/id/" + id,
				null,
				function(result) {
					data = result.map;
				}, null, null, {
					async: false
				});
			return data;
		}

		function blackDashboard(__id) {
			var url;
			var post;
			// conditionnement avec le type de costum aap
			if (costum && typeof costum.type === 'string' && costum.type === 'aap') {
				url = baseUrl + '/co2/aap/userdashboard';
				if (typeof costum.fUserDashboard === 'string')
					url += '/function/' + costum.fUserDashboard;
				post = {
					"user": __id
				}
				$('.dashboard-aap').remove();
				var dashboardAap = $('<div>')
					.addClass('modal dashboard-aap').css({
						display: 'none',
						overflowY: 'scroll',
						backgroundColor: 'rgba(0,0,0,0.9)',
						zIndex: 100000,
						position: 'fixed',
						top: $("#mainNav").outerHeight(),
						left: $("#menuApp.menuLeft").width()
					});
				$(document.body).append(dashboardAap);
				dashboardAap.show();
				if (typeof costum.fUserDashboard === 'string')
					url += '/function/' + costum.fUserDashboard;
				ajaxPost(dashboardAap, url, post, null, null, 'html');
				$('.menu-name-profil')
					.tooltip({
						container: 'body'
					})
					.removeClass('lbh')
					.addClass('open-modal-user-aap')
					.find(".tooltips-menu-btn")
					.text("Tableau de bord");
			}
		}

		function load_context(){
			if (costum && costum.contextId && costum.contextType) {
				aap.xhr()
					.get_element(costum.contextId, costum.contextType)
					.then(function(res) {
						if(res.oceco == null) {
							$('.aac-tl-project').remove();
						}
					})
			}
		}

		function do_load_answer_data(answer) {
			mylog.log(answer, 'answerr')
			var is_contributor = userId && answer.contributors[userId];
			var is_interrested = userId && answer.votes[userId] && answer.votes[userId].status === 'love';
			var funding = {
				total: 0,
				funded: 0,
				cofinancers: []
			};
			var progress = 0;
			// var edit_link = aap.url('#editCoformAap').dot({
			// 	context: costum.slug,
			// 	formid: answer.infos.form,
			// 	answer.id: answer.id,
			// 	step: 'aapStep1',
			// 	communId: null
			// }).toString();
			var edit_link = "/survey/answer/answer/id/" + answer.id + "/form/" + answer.infos.form
			var exchange_list = ['aapStep1lzi6plod1j27tg99qa2', 'aapStep1lzi6qmw1y98vifhvcjl'];

			funding = answer.depenses.reduce(function(prev, current) {
				var funding = {
					price: 0,
					cofinancers: []
				};
				funding = current.financer.reduce(function(a, b) {
					var cofinancers = a.cofinancers.slice();
					if (b.id && !a.cofinancers.includes(b.id))
						cofinancers.push(b.id);
					return ({
						price: a.price + b.amount,
						cofinancers: cofinancers
					});
				}, funding);
				var cofinancers = prev.cofinancers.slice();
				$.each(funding.cofinancers, function(_, financer) {
					if (!cofinancers.includes(financer))
						cofinancers.push(financer);
				});
				return ({
					total: prev.total + current.price,
					funded: prev.funded + funding.price,
					cofinancers: cofinancers
				})
			}, funding);
			progress = !funding.total ? 0 : parseInt((funding.funded / funding.total) * 100);
			mylog.log(answer, 'answer.title')
			load_context();
			$('.camp-detail-title .com-title').text(answer.titre)
			if(answer.project != null) {
				$('.camp-detail-title .commun-action .commun-project').attr('href', '#page.type.projects.id.'+answer.project._id.$id)
				$('.camp-detail-title .commun-action .commun-project img').attr('src', typeof answer.project.profilImageUrl != 'undefined'? answer.project.profilImageUrl : default_img)
				$('.camp-detail-title .commun-action .commun-project img').attr('alt', answer.project.name)
				$('.camp-detail-title .commun-action .commun-project img').attr('title', answer.project.name+' : Projet relier a ce commun')
				$(".aac-tl-project").css({"border-color" : "red", "color" : "red"});
				$(".aac-tl-project").attr("title", "désassocier le projet");
			} else {
				$('.camp-detail-title .commun-action .commun-project').remove()
			}
			$(".projectList").empty().select2({
				minimumInputLength: 1,
				//tags: true,
				multiple: false,
				"tokenSeparators": [','],
				createSearchChoice: function(term, data) {

				},
				ajax: {
					url: baseUrl + "/" + moduleId + "/search/globalautocomplete",
					dataType: 'json',
					type: "POST",
					quietMillis: 50,
					data: function(term) {
						return {
							name: term,
							searchType: ["projects"],
							filters: {
								'$or': {
									"name": {
										'$exists': true
									},
								}
							}
						};
					},
					results: function(data) {
						return {
							results: $.map(Object.values(data.results), function(item) {
								return {
									text: item.name,
									id: item._id.$id,
									img: item.profilImageUrl == "" ? modules.co2.url + '/images/thumbnail-default.jpg' : item.profilImageUrl,
									slug: item.slug
								}
							})
						};
					}
				}
			}).on('change', function(e) {
				if (exists(e.added)) {
					mylog.log('ADDED', e, e.added)

					var today = new Date();
					var params = {
						id: answer.id,
						collection: 'answers',
						path: 'project',
						value: {}
					}
					params.value = {
						"id": e.added.id,
						"startDate": today.getDay()+'/'+today.getMonth()+"/"+today.getFullYear()
					}
					dataHelper.path2Value(params, function(__response) {
						if (__response.result && __response.result === true) {
							mylog.log(__response.result, '_response')
							if ($(`[data-refreshblock-projet]`).length > 0) {
								$(`[data-refreshblock-projet]`).each((index, elem) => {
									const blockId = $(elem).attr("data-refreshblock-projet")
									if (typeof refreshBlock == "function") {
										refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
									}
								})
							}
							toastr.success("Commun associé au projet "+e.added.text);
							$("#projectListModal").modal("hide")
						}
					});
				}
			});
			$(".generateProject").click(function(){
				var today = new Date();
				var tplCtxCollection = "answers";
				var name = answer.titre ?? "Commun";
				var cntxtId = costum.contextId;
				var cntxtType = costum.contextType;
				var statusproject = answer.statusproject ?? [];
				
				var generateProject = function(){
					coInterface.showLoader("#projectListModal");
					ajaxPost("", baseUrl+'/survey/form/generateproject/answerId/'+answer.id+'/parentId/'+cntxtId+'/parentType/'+cntxtType,
						null,
						function(data){
							tplCtx.id = answer.id;
							tplCtx.collection = tplCtxCollection;
							tplCtx.path = "project";
							tplCtx.value = {
								"id" : data["projet"],
								"startDate" : today.getDay()+'/'+today.getMonth()+"/"+today.getFullYear()
							};
							dataHelper.path2Value( tplCtx, function(params) {} );

							tplCtx.path = "step";
							tplCtx.value = "";

							dataHelper.path2Value( tplCtx, function(params) {
								tplCtx4 = {};
								tplCtx4.id = answer.id;
								tplCtx4.collection = tplCtxCollection;
								tplCtx4.value = statusproject;

								tplCtx4.value.push("projectstate");
								tplCtx4.path = "status";
								dataHelper.path2Value( tplCtx4, function(params){
									if ($(`[data-refreshblock-projet]`).length > 0) {
										$(`[data-refreshblock-projet]`).each((index, elem) => {
											const blockId = $(elem).attr("data-refreshblock-projet")
											if (typeof refreshBlock == "function") {
												refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
											}
										})
									}
									$("#projectListModal").modal("hide")
								});
							} );
						},
					"html");
				}
				generateProject();
			})
			if ($("#aac_parentsContainer").length > 0 && answer?.aacParents) {
				$("#aac_parentsContainer").empty();
				const aacParentNb = Object.keys(answer.aacParents).length;
				var aacParentsHtml = `
					${ 
						aacParentNb > 0 ? `
							<span class="no-text-transform title-text">Les appels à communs</span>
							<div class="aac-parents col-xs-12 bs-px-0 bs-mt-3">
						` 
						: "" 
					}
				`;
				$.each(answer.aacParents, function(aacId, aacVal) {
					selectionData.selectedAac[aacVal?.parentData?.id ? aacVal.parentData.id : aacId] = {
						name: aacVal?.parentData?.name ? aacVal.parentData.name :aacVal.name,
						type: aacVal?.parentData?.type ? aacVal.parentData.type : "organization",
						value: "selected"
					}
					aacParentsHtml += `
						<a href="${ baseUrl }/costum/co/index/slug/${ aacVal.parentData.slug }/#detail-un-commun.communId.${ answer.id }" target="_blank" class="card-collection" data-key="${aacId}">
							<img class="card-collection-img" data-id="${aacId}" src="${typeof aacVal['profilImageUrl'] != 'undefined' && aacVal['profilImageUrl'] != '' ? aacVal['profilImageUrl'] : default_img}" 
								data-toggle="tooltip" title="${aacVal.name}" 
							/>
							<div class="card-collection-text-box">
								<div class="card-collection-text-content">
									<p class="card-collection-h1 nativ-text-ellipsis">${aacVal.name}</p>
								</div>
								<p class="card-collection-p">${ aacVal?.parentData?.name ? aacVal.parentData.name : "" }</p>
								<div></div>
							</div>
						</a>
					`
				});
				aacParentNb > 0 ? aacParentsHtml += "</div>" : "";
				$("#aac_parentsContainer").html(aacParentsHtml);
			}
			$('#communMyImg').attr('alt', answer.titre);
			$('#communMyImg').attr('src', baseUrl + (typeof answer.image != "undefined" && answer.image != '' ? answer.image : default_img))
			$('#communMyImg').data('slide', 1)

			$('.camp-porteur span').text(answer.author ? answer.author.name : '(Sans porteur)');
			if(typeof(answer.context_link) != 'undefined' && answer.context_link != null && answer.context_link != "") {
				var txtLink = answer.context_link;
				txtLink.startsWith('http') ? $('.camp-detail-title .com-link').attr('href',txtLink) : $('.camp-detail-title .com-link').attr('href','http://'+txtLink)
			} else {
				$('.camp-detail-title .com-link').remove()
			}

			var modalImg = $('#imgModal')
			if(typeof answer.images != 'Undefined' && answer.images.length > 0) {
				showSlides(slideIndex);
				modalImg.find('.img-preview-demo').empty()
				for(var i = 0; i < answer.images.length; i++) {
					var imgItem = $('<div>')
						.addClass('mySlides')
						.data('slide', answer.images.length - i)
						.html(`<div class="numbertext">${i + 1} / ${answer.images.length}</div><img src="${answer.images[i]}" style="width:100%">`)
					var demoItem = $('<div>')
						.addClass('column')
						.data('slide', answer.images.length - i)
						.html(`<img src="${answer.images[i]}" class="demo cursor" style="height:100px; width:100%;object-fit: contain;" alt="${answer.title}">`)
					modalImg.find('.img-modal-content').prepend(imgItem)
					modalImg.find('.img-preview-demo').append(demoItem)
					demoItem.click(function() {
						var slideNum = $(this).data("slide");
						currentSlide(slideNum);
					});
				}

				modalImg.find(".close").click(function() {
					closeModal();
				});

				modalImg.find(".prev").click(function() {
					plusSlides(1);
				});

				modalImg.find(".next").click(function() {
					plusSlides(-1);
				});

			}

			var communMyImg = $('#communMyImg')
			var captionText = $('#captionImg')
			$('#communMyImg').click(function() {
				var slideNum = $(this).data("slide");
				mylog.log('mandea dada a')
				openModal();
				currentSlide(slideNum);
			})

			var spanClose = $(".close-img");

			spanClose.click(function() {
				modalImg.addClass("hide")
			})
			$.map(answer.contributors, function(val, key) {
				var data_contrib = getPartDetail(key, val.type)
				var dom_contrib_img = `<a class="img-link" href="javascript:;" data-key="${key}">
				<img data-id="${key}" src="${typeof data_contrib['profilImageUrl'] != 'undefined' && data_contrib['profilImageUrl'] != '' ? data_contrib['profilImageUrl'] : default_img}" 
				data-toggle="tooltip" title="${data_contrib.name}" /> ${data_contrib.name}</a>`
				$('.action-txt .list-contributor').append(dom_contrib_img)
			})
			$('.img-link').on('click', function() {
				var __id = $(this).data('key')
				console.log(__id, 'idea')
				blackDashboard(__id)
			})
			// const images_contrib = $('.action-txt .list-contributor').find('a');
			// const dropdown_contrib = $('#dropdown-contrib')
			// if (images_contrib.length > 7) {
			// 	var showMoreBtn = $('<button>')
			// 		.addClass('btn-more-contrib')
			// 	var icon = '<i class="fa fa-plus"></i>'
			// 	showMoreBtn.append(icon)
			// 	$('.action-txt .list-contributor').append(showMoreBtn)
			// 	images_contrib.slice(7).each(function() {
			// 		const img = $(this);
			// 		img.hide();
			// 		dropdown_contrib.append(img.clone().show());
			// 	});

			// 	showMoreBtn.on('click', function() {
			// 		dropdown_contrib.toggle();
			// 	});
			// }

			var dataLove = Object.keys(answer.votes).filter(id => answer.votes[id].status === 'love')
			dataLove.map(function(element) {
				var data_love = getPartDetail(element)
				var dom_love_img = `<a class="love-link" href="javascript:;" data-key="${element}">
				<img data-id="${element}" src="${typeof data_love['profilImageUrl'] != 'undefined' && data_love['profilImageUrl'] != '' ? data_love['profilImageUrl'] : default_img}" 
				/> ${data_love.name}</a>`
				$('.action-txt .list-love').append(dom_love_img)
			})
			$('.love-link').on('click', function() {
				var __id = $(this).data('key')
				blackDashboard(__id)
			})
			// const images_love = $('.action-txt .list-love').find('a');
			// const dropdown_love = $('#dropdown-love')
			// if (images_love.length > 7) {
			// 	var showMore = $('<button>')
			// 		.addClass('btn-more-love')
			// 	var icone = '<i class="fa fa-ellipsis-h"></i>'
			// 	showMore.append(icone)
			// 	$('.action-txt .list-love').append(showMore)
			// 	images_love.slice(7).each(function() {
			// 		const imgs = $(this);
			// 		imgs.hide();
			// 		dropdown_love.append(imgs.clone().show());
			// 	});

			// 	showMore.on('click', function() {
			// 		dropdown_love.toggle();
			// 	});
			// }

			if (answer.author) {
				// $('.camp-porteur .camp-port-img .img-link, .camp-porteur .name-link').attr('href', '#page.type.citoyens.id.'+answer.author.id)
				$('.camp-porteur .camp-port-img img').attr('src', answer.author.image_medium);
			}
			$('.action-txt .aap-contributor-count').text(Object.keys(answer.contributors).length);
			$('.contrib-pastil').text(Object.keys(answer.contributors).length);
			mylog.log('love', Object.keys(answer.votes).filter(id => answer.votes[id].status === 'love'))
			$('.action-txt .aap-love-count').text(Object.keys(answer.votes).filter(id => answer.votes[id].status === 'love').length);
			$('.interest-pastil').text(Object.keys(answer.votes).filter(id => answer.votes[id].status === 'love').length);
			$('.btn-action-comm.aap-contributor-count')
				.data('participate', is_contributor ? 0 : 1)
				.data('answer', answer.id)
				.data('active-class', 'active')
				.prop('disabled', false)
				.off('click', handle_contribution_click)
				.on('click', handle_contribution_click);
			if (is_contributor)
				$('.btn-action-comm.aap-contributor-count').addClass('active');
			$('.btn-action-comm.aap-tl-count')
				.prop('disabled', false);
			$('.btn-action-comm.aap-love-count')
				.data('love', is_interrested ? 0 : 1)
				.data('answer', answer.id)
				.data('active-class', 'active')
				.prop('disabled', false)
				.off('click', handle_interrest_click)
				.on('click', handle_interrest_click);
			if (is_interrested)
				$('.btn-action-comm.aap-love-count').addClass('active');
			// commun tags
			const besoinsTags = "aapStep1m03ot9qymmfgashp7l";
			if (answer.steps && answer.steps.aapStep1 && answer.steps.aapStep1[besoinsTags] && answer.steps.aapStep1[besoinsTags].length > 0) {
				$('.tags-content').empty().append(answer.steps.aapStep1[besoinsTags].map(item => '<span class="tags">' + item + '</span>'));
			} else {
				$('.tags-content').remove()
			}
			if (typeof answer.tags != 'undefined' && answer.tags.length > 0) {
				$('.tags-content.global-tags').empty().append(answer.tags.map(item => '<span class="bs-mr-2">#' + item + '</span>'));
			} else {
				$('.tags-content.global-tags').remove()
			}
			$('.porteurDesc').empty().html(dataHelper.markdownToHtml(answer.desc_full));
			$('.campDesc').empty().html(answer.desc_summary);
			$('.financement-detail .progress-bar')
				.attr('aria-valuenow', progress)
				.css('width', progress + '%')
				.text(progress + '%');
			$('.objectif .chiffre')
				.empty()
				.html(funding.total + ' <i class="fa fa-eur"></i>');
			$('.recolte .chiffre')
				.empty()
				.html(funding.funded + ' <i class="fa fa-eur"></i>');
			$('.financeur>span:first').text(funding.cofinancers.length);
			$('.btn-cofinance').attr('data-id', answer.id);
			$('.btn-cofinance').off('click', function() {
				var thisbtn = $(this);
				var sampleTextarea = "";
				if (window.location.href.includes('/costum/'))
					sampleTextarea = baseUrl + "/costum/co/index/slug/" + costum.slug + "#answer.answer.id." + thisbtn.attr('data-id') + ".form." + aapObj.form["_id"]["$id"] + ".step.aapStep3.input.financer.view.custom";
				else
					sampleTextarea = baseUrl + "/#answer.answer.id." + thisbtn.attr('data-id') + ".form." + aapObj.form["_id"]["$id"] + ".step.aapStep3.input.financer.view.custom";
				window.open(sampleTextarea, "_blank");
			});
			// exchange button
			$.each(exchange_list, function(_, exchange) {
				var selector;

				selector = $('.btn-change[data-target=' + exchange + ']')
				if (answer.steps.aapStep1 && answer.steps.aapStep1[exchange])
					selector.show()
					.attr('target', '_blank')
					.attr('href', answer.steps.aapStep1[exchange]);
				else
					selector.hide();
			});
			if (userId) {
				$(".aac-tl-comments").removeClass("hide").show()
			}
			// selection button
			const selectValue = answer?.steps?.aapStep2?.choose?.[costum?.contextId];
			if (isSuperAdmin || isInterfaceAdmin) {
				if (selectValue && selectValue.value == "selected") {
					$('.aac-tl-select')
						.show()
						.addClass("checked cursor-pointer")
						.find(".edite-txt")
						.text("<?= Yii::t("cms", "Selected")?>");
					$('.aac-tl-select').find("i").show();
				} else {
					$('.aac-tl-select')
						.show()
						.addClass("cursor-pointer")
						.removeClass("checked disabled")
						.find(".edite-txt")
						.text("<?= Yii::t("cms", "Select")?>");
					$('.aac-tl-select').find("i").hide()
				}
			} else {
				if (selectValue && selectValue.value == "selected" && answer.editable) {
					$('.aac-tl-select')
						.show()
						.addClass("checked disabled")
						.removeClass("cursor-pointer")
						.find(".edite-txt")
						.text("Séléctionné");
					$('.aac-tl-select').find("i").show();
				} else {
					$('.aac-tl-select').hide();
				}
			}
			// edition button
			if (answer.editable)
				$('.aac-tl-edit, .aac-tl-delete, .aac-tl-canEdit-form').show()
				.attr('data-link', edit_link);
			else
				$('.aac-tl-edit, .aac-tl-delete, .aac-tl-canEdit-form').remove();
			if (userId) {
				$(".aac-tl-comments").removeClass("hide")
			}
			// event edit button
			$(".aac-tl-edit").off("click").on("click", function() {
				const link = $(this).data("link");
				const finderMethode = ["searchAndPopulateFinder", "addSelectedToForm", "bindSelectItems", "populateFinder"];
				const lastFinderVal = {};
				if (typeof finder != "undefined") {
					$.each(finderMethode, (index, methode) => {
						if (typeof finder[methode] != "undefined") {
							lastFinderVal[methode] = finder[methode];
						}
					})
				}
				if (link) {
					dialogContent.open({
						modalContentClass: "modal-custom-lg",
						isRemoteContent: true,
						backdropClick: true,
						shownCallback: () => {
							$("#dialogContent .modal-content .modal-body#dialogContentBody").next().hide()
						},
						hiddenCallback: () => {
							$("#dialogContent #dialogContentBody").removeClass("col-xs-12").empty();
							$("#dialogContent #currentModalHeader, #dialogContent #currentModalFooter").remove();
							$("#dialogContent .modal-content .modal-body#dialogContentBody").next().show()

							do_reload_answer(answer.id)
							if ($(`[data-need-refreshblock]`).length > 0) {
								$(`[data-need-refreshblock]`).each((index, elem) => {
									const blockId = $(elem).attr("data-need-refreshblock")
									if (typeof refreshBlock == "function") {
										refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
									}
								})
							}
							if (typeof finder != "undefined") {
								$.each(lastFinderVal, (key, methode) => {
									finder[key] = methode;
								})
							}
						}
					})
					ajaxPost(null, baseUrl + link, null, function(data) {
						const _htmlStr = `
							<div class="modal-body col-xs-12">
								${data}
							</div>
						`
						$("#dialogContent #dialogContentBody").addClass("col-xs-12").empty().html(_htmlStr)
						$("#dialogContent > .modal-dialog:first-child > .modal-content #dialogContentBody").before(`
							<div class="modal-header set-sticky" id="currentModalHeader">
								<h3 class="modal-title text-center">${ trad["Edit the common"] }</h3>
							</div>
						`)
						$("#dialogContent > .modal-dialog:first-child > .modal-content").append(`
							<div class="custom-modal-footer col-xs-12" id="currentModalFooter">
								<button type="button" class="btn btn-default close-current-modal hide">${ trad.save }</button>
							</div>
						`)
						$("#dialogContent .close-current-modal").off("click").on("click", function() {
							$("#dialogContent .close-modal[data-dismiss=modal]").trigger("click")
						})
						var btnSendInterval = setInterval(() => {
							if ($("#dialogContent .coformstep.standalonestep").length > 0) {
								clearInterval(btnSendInterval)
								$("#dialogContent .custom-modal-footer .close-current-modal").removeClass("hide")
							}
						}, 1000);
						const stickyElement = $('.modal-header.set-sticky');
						const modalContent = $('#dialogContent .modal-content');
						var stickyMenu = $('#dialogContent .coform-nav .schu-sticky');

						function checkStickyPosition() {
							const scrollTop = modalContent.scrollTop() + window.scrollY;
							const modalOffsetTop = modalContent?.offset()?.top ? modalContent?.offset()?.top : 0;
							const modalHeight = modalContent.outerHeight();
							var stickyMenu = $('#dialogContent .coform-nav .schu-sticky, #dialogContent .coform-nav .schu-btn-sticky');


							if (scrollTop >= modalOffsetTop) {
								stickyElement.css({
									position: 'fixed',
									width: $('#dialogContent .modal-content').width() - 20
								});
								stickyMenu.css({
									position: 'fixed',
									"z-index": "100",
									top: stickyElement.offset().top - window.scrollY + stickyElement.outerHeight() + 10 + "px"
								});
								if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
									stickyMenu.css({
										left: "3%",
										width: "90vw"
									})
									$("#dialogContent .coform-nav .schu-sticky").css({
										top: stickyElement.offset().top - window.scrollY + stickyElement.outerHeight() + $("#dialogContent .coform-nav .schu-btn-sticky").outerHeight() + 10 + "px"
									})
								}
							} else {
								stickyElement.css({
									position: 'relative',
									width: '100%'
								});
								stickyMenu.css({
									position: 'relative',
									top: "initial",
									left: "inherit"
								});
								if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
									stickyMenu.css({
										left: "inherit",
										width: "100%"
									})
								}
							}
							if (typeof syncSection != "undefined" && stickyMenu.length > 0) {
								syncSection()
							}
						}

						typeof checkStickyPos != "undefined" ? modalContent[0].removeEventListener('click', checkStickyPos) : "";
						modalContent[0].addEventListener('scroll', checkStickyPosition);

						// checkStickyPosition();
					}, null, null);
				}
			})
			$(".aac-tl-select.cursor-pointer").off("click").on("click", function() {
				const thisBtn = $(this);
				if ((isSuperAdmin || isInterfaceAdmin) && costum?.contextId) {
					aap.xhr()
						.update_select_status({
								value: !thisBtn.hasClass("checked") ? "selected" : "notselected",
								type: costum.contextType,
								name: costum.title,
							},
							"answers.aapStep2.choose." + costum.contextId
						).then(function(res) {
							if (res) {
								toastr.success(
									"Le commun a été " + (thisBtn.hasClass("checked") ? "séléctionné" : "déséléctionné"),
									'', {
										closeButton: true,
										preventDuplicates: true
									}
								)
							}
						})
				}
				if (thisBtn.hasClass("checked")) {
					thisBtn
						.removeClass("checked")
						.find(".edite-txt")
						.text("Séléctionner");
					thisBtn.find("i").hide();
				} else {
					thisBtn
						.addClass("checked")
						.find(".edite-txt")
						.text("Séléctionné");
					thisBtn.find("i").show();
				}
			})
			//event delete button
			$(".aac-tl-delete").off("click").on("click", function() {
				$.confirm({
					title: trad["delete"],
					type: 'red',
					content: "<span class='bold'>" + trad["Are you sure you want to delete the common"] + " \"" + answer.titre + "\"?</span><br><span class='text-red bold'>" + trad.actionirreversible + "</span>",
					buttons: {
						no: {
							text: trad.no,
							btnClass: 'btn btn-default'
						},
						yes: {
							text: trad.yes,
							btnClass: 'btn btn-danger',
							action: function() {
								getAjax("", baseUrl + "/survey/co/delete/id/" + answer.id, function(data) {
									if (data.result) {
										toastr.success(trad["processing delete ok"] + " !");
										urlCtrl.loadByHash("#welcome")
									}
								}, "html");
							}
						}
					}
				});
			})
			$('.aac-tl-canEdit-form').click(function(){
				mylog.log('can edit form')
				add_form_contributor(answer)
			})
			$('.aac-tl-project').click(function() {
				if(answer.project != null) {
					$.confirm({
						title: "Désassocier le projet",
						type: 'red',
						content: "<span class='bold'>Vous êtes sur de votre decision?</span><br><span class='text-red bold'>" + trad.actionirreversible + "</span>",
						buttons: {
							no: {
								text: trad.no,
								btnClass: 'btn btn-default'
							},
							yes: {
								text: trad.yes,
								btnClass: 'btn btn-danger',
								action: function() {
									remove_project(answer)
								}
							}
						}
					});
				} else {		
					$("#projectListModal").modal('show')
					// add_project(answer);
				}
			})

			
			$(".aac-tl-comments").off("click").on("click", handle_comments_click)
			$('.name-link').on('click', function() {
				blackDashboard(answer.author.id)
			})
			$(".select-commun").off("click", handleSelectCommunClick).on("click", handleSelectCommunClick);
			// Execute last action blocked by non connected
			if (sessionStorage.getItem('firelove') && !is_interrested)
				$('.btn-action-comm.aap-love-count').click();
			else
				sessionStorage.removeItem('firelove');
			if (sessionStorage.getItem('firecontribute') && !is_contributor)
				$('.btn-action-comm.aap-contributor-count').click();
			else
				sessionStorage.removeItem('firecontribute');
			var readMoreBtn = $('#read-more-btn');
			if ($('#porteurDesc').outerHeight() > $('#desc-content').height()) {
				readMoreBtn.show();
			}
			coInterface.bindLBHLinks();
		}

		function do_reload_answer(answer) {
			do_reset_dom();
			$('.action-txt .list-contributor').empty();
			$('.action-txt .list-love').empty();
			aap.answer(answer)
			aap.xhr()
				.get_answer_data()
				.then(function(resp) {
					if (resp.success) {
						do_load_answer_data(resp.content);
					}
				});
		}

		function do_reload_tl_use() {
			aap.xhr()
				.get_tl_use()
				.then(function(resp) {
					var tls, btn_dom;

					tls = resp.tls;
					btn_dom = $('.btn-action-comm.aap-tl-count');
					btn_dom.removeClass('active');
					$('.nb-action.aap-tl-count').text(tls.length);
					var use_list_dom = $('.action-txt .list-user .use-list')
					use_list_dom.empty()
					tls.map(function(element) {
						var dom_use_img = `<a class="lbh-preview-element lbh contact${element.id}" href="#page.type.${element.type}.id.${element.id}">
						<img data-id="${element.id}" src="${typeof element['image'] != 'undefined' && element['image'] != '' ? element['image'] : default_img}" 
						/> ${element.name}</a>`
						use_list_dom.append(dom_use_img)
					})
					// $('.user-link').on('click', function(){
					// 	var __id = $(this).data('key')
					// 	mylog.log('id', __id)
					// 	blackDashboard(__id) 
					// })
					if (resp.is_user)
						btn_dom.addClass('active');
				});
		}

		do_reset_dom();
		aap.xhr()
			.get_answer_data()
			.then(function(resp) {
				if (resp.success) {
					do_load_answer_data(resp.content);
					aap.xhr()
						.update_seen()
						.then();
					do_reload_tl_use();
				}
			});
		get_comments_data();
		var criteria = {};
		criteria["answers.aapStep2.choose." + costum.contextId + ".value"] = "selected";
		aap.xhr()
		.get_prev_answer_id(criteria)
		.then(function(id) {
			$(".swipe-navigation.swipe-prev").data("id",id);
		});
		aap.xhr()
		.get_next_answer_id(criteria)
		.then(function(id) {
			$(".swipe-navigation.swipe-next").data("id",id);
		});
		$('button.aap-tl-count').off('click', handle_use_click)
			.on('click', handle_use_click);
		// (function($) {
		// 	var x_start,
		// 		x_end,
		// 		target_prop,
		// 		max_drop;
		//
		// 	max_drop = 120;
		// 	$(document).off("touchstart")
		// 		.off("touchend")
		// 		.off("touchmove")
		// 		.on("touchstart", function(e) {
		// 			var touch;
		//
		// 			touch = e.originalEvent.touches[0];
		// 			x_start = touch.clientX;
		// 		})
		// 		.on("touchmove", function(e) {
		// 			var touch,
		// 				diff,
		// 				room,
		// 				navigation_dom;
		//
		// 			touch = e.originalEvent.touches[0];
		// 			x_end = touch.clientX;
		// 			diff = Math.abs(x_end - x_start);
		// 			$(".swipe-navigation").removeClass("active");
		// 			if (x_end < x_start) {
		// 				room = $(window).outerWidth(true) - Math.min(diff, max_drop) + 40;
		// 				navigation_dom = $(".swipe-navigation.swipe-next");
		// 			} else if (x_end > x_start) {
		// 				room = Math.min(diff, max_drop) - 40;
		// 				navigation_dom = $(".swipe-navigation.swipe-prev");
		// 			}
		// 			if (navigation_dom && navigation_dom.data("id")) {
		// 				navigation_dom.css("left", room);
		// 				if (diff >= max_drop)
		// 					navigation_dom.addClass("active");
		// 			}
		// 		})
		// 		.on("touchend", function() {
		// 			var diff,
		// 				target;
		//
		// 			diff = Math.abs(x_end - x_start);
		// 			if (diff >= max_drop) {
		// 				target = $(".swipe-navigation.active");
		// 				if (target.data("id"))
		// 					urlCtrl.loadByHash(aap.url().dot("communId", target.data("id")).toString());
		// 			}
		// 			$(".swipe-navigation.swipe-next").animate({
		// 				left: $(window).outerWidth(true) + 40
		// 			}, 200, function() {
		// 				$(this).css("left", "")
		// 					.removeClass("active");
		// 			});
		// 			$(".swipe-navigation.swipe-prev").animate({
		// 				left: -40
		// 			}, 200, function() {
		// 				$(this).css("left", "")
		// 					.removeClass("active");
		// 			});
		// 		});
		// })($);
	});
</script>