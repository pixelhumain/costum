<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "btnAac";
?>

<style id="css-<?= $kunik ?>">
.btnAac<?= $kunik ?> {
    --default-button-color: #000;
    --active-button-color: #43C9B7;
    --dtl-costum-color1: <?= isset($costum["css"]["color"]["color1"]) ? $costum["css"]["color"]["color1"] : "#43c9b7" ?>;
    --dtl-costum-color2: <?= isset($costum["css"]["color"]["color2"]) ? $costum["css"]["color"]["color2"] : "#4623c9" ?>;
    --dtl-costum-color3: <?= isset($costum["css"]["color"]["color3"]) ? $costum["css"]["color"]["color3"] : "#ffc9cb" ?>;
}
.btnAac<?= $kunik ?> {
    padding: 8px 25px;
    border: 8px solid var(--dtl-costum-color1);
    border-radius: 30px;
    font-size: 22px;
    color: #fff;
    margin: 30px 0 0 5px;
    text-decoration: none !important;
    display: inline-block;
    text-align: center;
    background-color: var(--dtl-costum-color1);
    transition: all .4s;
}
.btnAac<?= $kunik ?>:hover{
    border: 8px solid var(--dtl-costum-color1);
    color: var(--dtl-costum-color1);
    background-color: #fff;
    text-decoration: none;
}
.sp-elt-<?= $myCmsId ?> {
    width: auto !important;
}

    @media (min-width: 768px) {
		#dialogContentBody .section-nav-body {
			width: 11vw;
		}	
	}

	#oceco-tools-coevent-modal {
		z-index: 999999;
	}

	.otc-select2-item {
		display: flex;
		gap: 10px;
	}

	.otc-select2-image {
		width: 40px;
		height: 40px;
		min-width: 40px;
		overflow: hidden;
		min-height: 40px;
		border-radius: 50%;
	}

	.otc-select2-image img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.otc-select2-info {
		font-size: 16px;
	}

	.otc-select2-type {
		font-size: 14px;
	}

	.otc-select2-info>* {
		display: block;
	}

	.otc-select2-name {
		font-weight: bold;
	}

	.otc-select2-search {
		text-decoration: underline;
		font-style: italic;
	}
</style>
<div class="modal fade" id="oceco-tools-coevent-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Coevent</h4>
			</div>
			<div class="modal-body">
				<form action="#!" id="oceco-tools-coevent-form">
					<input type="hidden" id="otc-context-type">
					<div class="form-group">
						<label for="otc-context">Contexte</label>
						<input type="text" hidden id="otc-context">
					</div>
					<div class="form-group">
						<label for="otc-parent-event">Evénement parent</label>
						<select id="otc-parent-event">
							<option value="" selected></option>
						</select>
					</div>
					<div class="form-group" id="create-event-group">
						<label for="oct-create-event">Vous avez besoin de créer un événement ?</label><br>
						<button id="oct-create-event" class="btn btn-primary">Créer un événement</button><br>
						<label class="error"></label>
					</div>
					<div class="form-group" id="otc-page-name-group">
						<label for="otc-page-name">Nom de la page</label>
						<input type="text" class="form-control" id="otc-page-name">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				<button type="button" class="btn btn-primary" id="oct-save"><span class="fa fa-plus"></span> Créer</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
        <?php
            
            if (isset($blockCms["typeUrl"]) && $blockCms["typeUrl"] == "generateAap") {
                echo '<a href="javascript:;" class="btnAac'.$kunik.' '.(isset($blockCms["generateType"]) && $blockCms["generateType"] == "generateAac" ? "generateAac" : "generateCoEvent").' '.$kunik.' '.$kunik.'-css">
                    <div class="sp-text btnLabel btnLabel'.$blockKey.'" id="sp-'.$blockKey.'" data-id="'.$blockKey.'" data-field="btnLabel0">

                    </div></a>
                </a>';
            } else {
                if (isset($blockCms["button"]) && isset($blockCms["typeUrl"])) {
                    if ($blockCms["typeUrl"] == "internalLink") {
                        echo '<a href="javascript:;" class="btnAac'.$kunik.' '.$kunik.' '.$kunik.'-css">
                            <div class="sp-text btnLabel btnLabel'.$blockKey.'" id="sp-'.$blockKey.'" data-id="'.$blockKey.'" data-field="btnLabel0">

                            </div></a>';
                    } else if ($blockCms["typeUrl"] == "externalLink") {
                        echo '<a href="javascript:;" class="btnAac'.$kunik.' '.$kunik.' '.$kunik.'-css">
                                <div class="sp-text btnLabel btnLabel'.$blockKey.'" id="sp-'.$blockKey.'" data-id="'.$blockKey.'" data-field="btnLabel0">

                                </div></a>';
                    } else if ($blockCms["typeUrl"] == "insideModal") {
                        $btnRedirectUrl = isset($blockCms["modalBtnRedirection"]) ? ('data-btnredirectionurl="' . $blockCms["modalBtnRedirection"] . '"') : "";
                        echo '<a href="javascript:;" data-link="'. $blockCms["button"] .'" data-title="'. ( isset($blockCms["modalLabel"]) ? Yii::t("common", $blockCms["modalLabel"]) : "" ).'" '. (isset($blockCms["getFirstStepOnly"]) && filter_var($blockCms["getFirstStepOnly"], FILTER_VALIDATE_BOOL) ? "data-getfirst-steponly='true'" : "") .' '. $btnRedirectUrl .' class="internalLinkToModal btnAac'.$kunik.' '.$kunik.' '.$kunik.'-css">			
                                <div class="sp-text btnLabel btnLabel'.$blockKey.'" id="sp-'.$blockKey.'" data-id="'.$blockKey.'" data-field="btnLabel0">

                                </div></a>';
                    }  else {
                        echo '<a href="javascript:;" class="btnAac'.$kunik.' '.$kunik.' '.$kunik.'-css">
                                <div class="sp-text btnLabel btnLabel'.$blockKey.'" id="sp-'.$blockKey.'" data-id="'.$blockKey.'" data-field="btnLabel0">

                                </div></a>';
                    }
                } else {
                    echo '<a href="javascript:;" class="btnAac'.$kunik.' '.$kunik.' '.$kunik.'-css">
                            <div class="sp-text btnLabel btnLabel'.$blockKey.'" id="sp-'.$blockKey.'" data-id="'.$blockKey.'" data-field="btnLabel0">

                            </div></a>';
                }  
            }          
        ?>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    const php = {
        kunik: '<?= $kunik ?>',
        my_cms_id: '<?= $myCmsId ?>',
    }
    $("#css-<?= $kunik ?>").append(str);
    if(!costum.editMode && typeof sectionDyf.<?php echo $kunik ?>BlockCms['button'] != "undefined" && typeof sectionDyf.<?php echo $kunik ?>BlockCms['typeUrl'] != "undefined"  && sectionDyf.<?php echo $kunik ?>BlockCms['typeUrl'] == "internalLink") {
		$('.btnAac<?= $kunik ?>').addClass("lbh")
		$('.btnAac<?= $kunik ?>').attr("href", sectionDyf.<?php echo $kunik ?>BlockCms['button'])
	}
    if(!costum.editMode && typeof sectionDyf.<?php echo $kunik ?>BlockCms['button'] != "undefined" && typeof sectionDyf.<?php echo $kunik ?>BlockCms['typeUrl'] != "undefined"  && sectionDyf.<?php echo $kunik ?>BlockCms['typeUrl'] == "externalLink") {
		$('.btnAac<?= $kunik ?>').attr("href", sectionDyf.<?php echo $kunik ?>BlockCms['button'])
        $('.btnAac<?= $kunik ?>').attr("target", "_blank")
	}
    if (costum.editMode) {
        const allFormsContext = <?php echo json_encode( $allFormsContext ); ?>;
        var customPages = $.map( costum.app, function( val, key ) {
            return {value: key,label: key}
        })
        $.each(allFormsContext, (formKey, formVal) => {
            const contextId = formVal.parent ? Object.keys(formVal.parent)[0] : "null";
            customPages.push({
                value : `#coformAap.context.${ costum.contextSlug }.formid.${ formKey }.step.aapStep1.answerId.new`,
                label : `#coformAap (${ formVal.name })`
            })
        });
        var pageToInModal = [];
        $.each(allFormsContext, (formKey, formVal) => {
            pageToInModal.push({
                value : formVal.type && formVal.type == "aap" ? `/survey/answer/answer/id/new/form/${ formKey }` : `/survey/answer/index/id/new/form/${ formKey }`,
                label : `Nouvelle réponse au formulaire <b>${ formVal.name }</b>`
            });
        });
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var btnAac = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputSimple",
                            options : {
                                name : "btnLabel",
                                label : "label du bouton",
                                collection : "cms"
                            }
                        },
                        {
							type: "inputSwitcher",
                            options: {
                                name: "typeUrl",
                                label: `Pour le boutton`,
                                class: "urlOptions ",
                                tabs: [
                                    {
                                        value: "internalLink",
                                        label: "Lien interne",
                                        inputs: [
                                            {
                                                type: "select",
                                                options: {
                                                    name: "button",
                                                    isSelect2: true,
                                                    label: `Page`,
                                                    options: customPages,
                                                    defaultValue: sectionDyf?.[php.kunik + "BlockCms"]?.button ? sectionDyf?.[php.kunik + "BlockCms"]?.button : null
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value: "externalLink",
                                        label: "Lien externe",
                                        inputs: [
                                            {
                                                type: "inputSimple",
                                                options: {
                                                    name: "button",
                                                    viewPreview: true,
                                                    defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.externalLink ? sectionDyf?.[php.kunik + "BlockCms"]?.externalLink : null
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value: "insideModal",
                                        label: "Lien interne (Modal)",
                                        inputs: [
                                            {
                                                type : "select",
                                                options : {
                                                    name : "button",
                                                    isSelect2 : true,
                                                    label : `Page`,
                                                    options : pageToInModal
                                                }
                                            },
                                            {
                                                "type": "groupButtons",
                                                "options": {
                                                    "name": "getFirstStepOnly",
                                                    "label": "Récuperer l'etape un seulement",
                                                    "options": [
                                                        {
                                                            "label": "Oui",
                                                            "value": true,
                                                            "icon": ""
                                                        },
                                                        {
                                                            "label": "Non",
                                                            "value": false,
                                                            "icon": ""
                                                        }
                                                    ],
                                                    defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.getFirstStepOnly ? sectionDyf?.[php.kunik + "BlockCms"]?.getFirstStepOnly : false
                                                }
                                            },
                                            {
                                                type : "inputSimple",
                                                options : {
                                                    name : "modalLabel",
                                                    label : `Titre du modal`
                                                }
                                            },
                                            {
                                                type: "select",
                                                options: {
                                                    name : "modalBtnRedirection",
                                                    label : `Lien de redirection`,
                                                    isSelect2: true,
                                                    options: customPages,
                                                    defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.modalBtnRedirection ? sectionDyf?.[php.kunik + "BlockCms"]?.modalBtnRedirection : null
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value: "generateAap",
                                        label: "Générer un app",
                                        inputs: [
                                            {
                                                "type": "select",
                                                "options": {
                                                    "name": "generateType",
                                                    "label": "choisir l'element à generer",
                                                    "options": [
                                                        {
                                                            "label": "Aac",
                                                            "value": "generateAac"
                                                        },
                                                        {
                                                            "label": "co-event",
                                                            "value": "generateCoEvent"
                                                        }
                                                    ],
                                                    defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.generateType
                                                }
                                            }
                                        ]
                                    },
                                ]
                            }
						}
                    ]
                },
                style: {
                    inputsConfig: [
                        "addCommonConfig",
                        "width",
                        "color",
                        "backgroundColor"
                    ]
                },
                advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                },
                hover: {
                        inputsConfig: [
                            "addCommonConfig",
                            "color",
                            "backgroundColor"
                        ]
                },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.btnAac<?= $myCmsId ?> = btnAac;
    }
	appendTextLangBased(".btnLabel<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($blockCms["btnLabel0"]) ?>, "<?= $blockKey ?>");

    (function() {
        if(!costum.editMode) {
            $("."+php.kunik+".generateAac").off("click").on("click", function() {
                ocecotoolsObj_aac.createAac(ocecotoolsObj_aac)
            })
            $("."+php.kunik+".generateCoEvent").off("click").on("click", function() {
                coevent_form()
            })
            $("."+php.kunik+".internalLinkToModal").off("click").on("click", function() {
                var link = $(this).data("link");
                const getFirstStepOnly = $(this).data("getfirst-steponly")
                getFirstStepOnly && /true/.test(getFirstStepOnly) ? link += "/step/aapStep1" : link = link.replace("/step/aapStep1", ""); 
                const modalTitle = $(this).data("title");
                const finderMethode = ["searchAndPopulateFinder", "addSelectedToForm", "bindSelectItems", "populateFinder"];
                const btnRedirectionUrl = $(this).attr("data-btnredirectionurl") ? $(this).attr("data-btnredirectionurl") : "";
                const lastFinderVal = {};
                if (typeof finder != "undefined") {
                    $.each(finderMethode, (index, methode) => {
                        if (typeof finder[methode] != "undefined") {
                            lastFinderVal[methode] = finder[methode];
                        }
                    })
                }
                if (link) {
                    dialogContent.open({
                        modalContentClass: "modal-custom-lg",
                        isRemoteContent: true,
                        backdropClick: true,
                        shownCallback: () => {
                            $("#dialogContent .modal-content .modal-body#dialogContentBody").next().hide()
                        },
                        hiddenCallback: () => {
                            $("#dialogContent .modal-content .modal-body#dialogContentBody").next().show()
                            $("#dialogContent #dialogContentBody").removeClass("col-xs-12").empty()
                            $("#dialogContent #currentModalHeader, #dialogContent #currentModalFooter").remove();
                            if (typeof finder != "undefined") {
                                $.each(lastFinderVal, (key, methode) => {
                                    finder[key] = methode;
                                })
                            }
                            if ($(`[data-need-refreshblock]`).length > 0) {
                                $(`[data-need-refreshblock]`).each((index, elem) => {
                                    const blockId = $(elem).attr("data-need-refreshblock")
                                    if (typeof refreshBlock == "function") {
                                        refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
                                    }
                                })
                            }
                        }
                    });
                    ajaxPost(null, baseUrl + link, null, function (data) {
                        const _htmlStr = `
                            <div class="modal-body col-xs-12 bs-xs-0">
                                ${data}
                            </div>
                        `
                        $("#dialogContent #dialogContentBody").addClass("col-xs-12").empty().html(_htmlStr)
                        $("#dialogContent > .modal-dialog:first-child > .modal-content #dialogContentBody").before(`
                            <div class="modal-header set-sticky" id="currentModalHeader">
                                <h3 class="modal-title text-center">${ (modalTitle ? modalTitle : "Titre du modal") }</h3>
                            </div>
                        `)
                        $("#dialogContent > .modal-dialog:first-child > .modal-content").append(`
                            <div class="custom-modal-footer col-xs-12" id="currentModalFooter">
                                <button type="button" class="btn btn-default close-current-modal hide">${ trad.Add }</button>
                            </div>
                        `)
                        var btnSendInterval = setInterval(() => {
                            if ($("#dialogContent .coformstep.standalonestep").length > 0) {
                                clearInterval(btnSendInterval)
                                $("#dialogContent .custom-modal-footer .close-current-modal").removeClass("hide")
                            }
                        }, 1000);
                        $("#dialogContent .close-current-modal").off("click").on("click", function() {
                            $("#dialogContent .close-modal[data-dismiss=modal]").trigger("click")
                            if (btnRedirectionUrl && btnRedirectionUrl.indexOf("#") == 0 && btnRedirectionUrl != location.hash) {
                                urlCtrl.loadByHash(btnRedirectionUrl)
                            }
                        })
                        const stickyElem = $('.modal-header.set-sticky');
                        const modalCont = $('#dialogContent .modal-content');

                        function checkStickyPos() {
                            const scrollTop = modalCont.scrollTop() + window.scrollY;
                            const modalOffsetTop = modalCont?.offset()?.top ? modalCont?.offset()?.top : 0;
                            const modalHeight = modalCont.outerHeight();
                            var stickyMenu = $('#dialogContent .coform-nav .schu-sticky, #dialogContent .coform-nav .schu-btn-sticky');

                            if (scrollTop >= modalOffsetTop) {
                                stickyElem.css({
                                    position: 'fixed',
                                    width: $('#dialogContent .modal-content').width() - 20
                                });
                                stickyMenu.css({
                                    position: 'fixed',
                                    "z-index": "100",
                                    top: stickyElem.offset().top - window.scrollY + stickyElem.outerHeight() + 10 + "px" 
                                });
                                if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
                                    stickyMenu.css({
                                        left: "3%",
                                        width: "90vw"
                                    })
                                    $("#dialogContent .coform-nav .schu-sticky").css({
                                        top: stickyElem.offset().top - window.scrollY + stickyElem.outerHeight() + $("#dialogContent .coform-nav .schu-btn-sticky").outerHeight() + 10 + "px"
                                    })
                                }
                            } else {
                                stickyElem.css({
                                    position: 'relative',
                                    width: '100%'
                                });
                                stickyMenu.css({
                                    position: 'relative',
                                    top: "initial",
                                    left: "inherit"
                                });
                                if ($("#dialogContent .coform-nav .schu-btn-sticky .btn").is(":visible")) {
                                    stickyMenu.css({
                                        left: "inherit",
                                        width: "100%"
                                    })
                                }
                            }
                            if (typeof syncSection != "undefined" && stickyMenu.length > 0) {
                                syncSection()
                            }
                        }
                        typeof checkStickyPosition != "undefined" ? modalCont[0].removeEventListener('scroll', checkStickyPosition) : "";
                        modalCont[0].addEventListener('scroll', checkStickyPos);
                        
                        // checkStickyPos();
                    }, null, null);
                }
            })
        }
        "use strict";

        function create_costum_page(args) {
            return new Promise(function(resolve, reject) {
                if (typeof args.hash !== "string" ||
                    typeof args.event !== "string" ||
                    typeof args.id !== "string" ||
                    typeof args.collection !== "string")
                    reject();
                var path2value = {
                    id: args.id,
                    collection: args.collection,
                    path: "costum.app.#" + args.hash,
                    value: {
                        hash: "#app.view",
                        coevent: true,
                        event: args.event,
                        urlExtra: "/page/" + args.hash,
                        name: {}
                    },
                    setType: [{
                        path: "coevent",
                        type: "boolean"
                    }]
                };
                if (typeof args.name === "string")
                    path2value.value.name[mainLanguage] = args.name;
                dataHelper.path2Value(path2value, resolve);
            });
        }

        function get_event_from_context(args) {
            var url = baseUrl + "/costum/coevent/get_events/request/element_event";
            var elementEvents = {};
            return new Promise(function(resolve, reject) {
                ajaxPost(null, url, args, resolve, reject, "json")
            });
        }

        function coevent_form() {
            var context_input_dom = $("#otc-context");
            var event_select_dom = $("#otc-parent-event");
            var page_name_group_dom = $("#otc-page-name-group");
            var context_type_input_dom = $("#otc-context-type");
            var create_event_button_dom = $("#oct-create-event");
            var creae_event_group_dom = $("#create-event-group");
            var create_btn_dom = $("#oct-save");

            function enable_spin(state) {
                create_btn_dom.prop("disabled", state);
                if (state)
                    create_btn_dom.find(".fa")
                    .removeClass("fa-plus")
                    .addClass("fa-spinner")
                    .addClass("fa-spin");
                else
                    create_btn_dom.find(".fa")
                    .addClass("fa-plus")
                    .removeClass("fa-spinner")
                    .removeClass("fa-spin");
            }

            event_select_dom.build_event_select = function(events) {
                var new_event_id = "new_event";
                var self = this;
                var _events = [];
                if (typeof events === "object" && Array.isArray(events))
                    _events = events.slice();
                _events.unshift({
                    id: new_event_id,
                    name: "Créer un événement"
                });
                if (self.data("select2"))
                    self.select2("destroy");
                while (self.children().length > 0)
                    self.children().last().remove();
                self.append("<option value=\"\" selected></option>");
                $.each(_events, function(i, event) {
                    self.append(
                        $("<option>")
                        .attr("value", event.id)
                        .text(event.name)
                        .data("storage", event)
                    );
                });
                self.select2({
                    width: "100%",
                    formatResult: function(item) {
                        if (item.id === new_event_id) {
                            return ("<div style=\"text-align: center;\"><button id=\"oct-create-event\" class=\"btn btn-primary\">Créer un événement</button></div>");
                        }
                        var regex = new RegExp(self.data('select2').search.val(), 'ig');
                        var copy = $.extend(true, {}, $(item.element[0]).data("storage"));
                        copy.text = copy.name.replace(regex, function(match) {
                            return ("<span class=\"otc-select2-search\">" + match + "</span>");
                        });
                        var start_date = copy.startDate.format("LL");
                        var start_hour = copy.startDate.format("HH:mm");
                        var end_date = copy.endDate.format("LL");
                        var end_hour = copy.endDate.format("HH:mm");
                        if (end_hour === "00:00") {
                            var clone = copy.endDate.clone().subtract(1, "minutes");
                            end_date = clone.format("YYYY-MM-DD");
                            end_hour = clone.format("HH:mm");
                        }
                        if (start_date !== end_date) {
                            copy.interval = start_date;
                            if (start_hour !== "00:00")
                                copy.interval += " (" + start_hour + ")";
                            copy.interval += " - " + end_date + " (" + end_hour + ")";
                        } else
                            copy.interval = start_date + " (" + start_hour + " - " + end_hour + ")";
                        return (dataHelper.printf(
                            "<div class=\"otc-select2-item\">" +
                            "	<div class=\"otc-select2-info\">" +
                            "		<span class=\"otc-select2-name\">{{text}}</span>" +
                            "		<span class=\"otc-select2-type\">{{interval}}</span>" +
                            "	</div>" +
                            "</div>",
                            copy
                        ));
                    }
                }).off("click").on("click", function() {
                    if (self.val() === new_event_id) {
                        self.val("").trigger("change");
                        create_event_button_dom.trigger("click");
                        return (true);
                    }
                });
            };
            var modal_dom = $("#oceco-tools-coevent-modal");
            var search_length = 5;
            var elements = ["organizations", "projects"];
            if (context_input_dom.data("select2"))
                context_input_dom.select2("destroy");
            context_input_dom.select2({
                width: "100%",
                minimumInputLength: 1,
                ajax: {
                    url: baseUrl + "/co2/element/select2list",
                    dataType: "json",
                    type: "post",
                    cache: true,
                    data: function(term, page) {
                        return ({
                            search: term,
                            page: page,
                            elements: elements,
                            length: search_length,
                            user: userId
                        });
                    },
                    results: function(data) {
                        var results = data.results.map(function(result) {
                            return ({
                                id: result.id,
                                text: result.name,
                                image: result.image,
                                type: result.type,
                                slug: result.slug
                            });
                        });
                        return ({
                            results: results,
                            more: data.more
                        });
                    }
                },
                formatResult: function(item) {
                    var regex = new RegExp(context_input_dom.data('select2').search.val(), 'ig');
                    var copy = $.extend({}, item);
                    copy.match = copy.text.replace(regex, function(match) {
                        return ("<span class=\"otc-select2-search\">" + match + "</span>");
                    });
                    return (dataHelper.printf(
                        "<div class=\"otc-select2-item\">" +
                        "	<div class=\"otc-select2-image\">" +
                        "		<img src=\"{{image}}\" alt=\"{{text}}\">" +
                        "	</div>" +
                        "	<div class=\"otc-select2-info\">" +
                        "		<span class=\"otc-select2-name\">{{match}}</span>" +
                        "		<span class=\"otc-select2-type\">{{type}}</span>" +
                        "	</div>" +
                        "</div>",
                        copy
                    ));
                }
            }).off("change").on("change", function(e) {
                var self = $(this);
                var data = self.select2("data");
                create_event_button_dom.data(data).nextAll(".error").hide();
                context_type_input_dom.val(data.type);
                get_event_from_context(data).then(function(events) {
                    var mapped = Object.keys(events).map(function(key) {
                        return ({
                            id: key,
                            name: events[key].name,
                            startDate: moment.unix(events[key].startDate.sec),
                            endDate: moment.unix(events[key].endDate.sec),
                        })
                    });
                    event_select_dom.build_event_select(mapped);
                });
                dataHelper.element_has_costum(data).then(function(exists) {
                    if (exists)
                        page_name_group_dom.show();
                    else
                        page_name_group_dom.hide().find(".form-control").val("");
                });
            });
            // initialize when modal opens
            event_select_dom.build_event_select();
            page_name_group_dom.hide().find(".form-control").val("");
            modal_dom.modal("show");
            context_type_input_dom.val("");
            creae_event_group_dom.hide();
            enable_spin(false);
            // initialize when modal opens
            modal_dom.off("hidden.bs.modal").on("hidden.bs.modal", function() {
                context_input_dom.select2("destroy").val("");
                event_select_dom.select2("destroy");
            });

            $("#oceco-tools-coevent-form").off("submit").on("submit", function(e) {
                var form_valid;
                var has_costum;

                e.preventDefault();
                has_costum = page_name_group_dom.is(":visible");
                form_valid = context_input_dom.val() !== "";
                form_valid = form_valid && event_select_dom.val() !== "";
                form_valid = form_valid && (!has_costum || page_name_group_dom.find(".form-control").val() !== "");

                if (form_valid) {
                    enable_spin(true);
                    var context_data = {
                        id: context_input_dom.val(),
                        collection: context_type_input_dom.val(),
                        firstStepper: false
                    };
                    if (!has_costum) {
                        coInterface.create_costum(context_data).then(function(response) { 
                            if (response.result) {
                                create_costum_page($.extend({}, context_data, {
                                    hash: "welcome",
                                    event: event_select_dom.val(),
                                    name: "welcome"
                                })).then(function(response) {
                                    if (typeof response === "object" && response.result) {
                                        var context_data = context_input_dom.select2("data");
                                        location.href = baseUrl + "/costum/co/index/slug/" + context_data.slug + "/edit/true#welcome?text=coevent";
                                    }
                                });
                            }
                        });
                    } else {
                        var page_link = page_name_group_dom.find(".form-control").val()
                            .toLowerCase()
                            .replace(/\s/g, "-")
                            .normalize("NFD")
                            .replace(/[\u0300-\u036f]/g, "");
                        var name = {};
                        create_costum_page($.extend({}, context_data, {
                            hash: page_link,
                            event: event_select_dom.val(),
                            name: page_name_group_dom.find(".form-control").val()
                        })).then(function(response) {
                            if (typeof response === "object" && response.result) {
                                var context_data = context_input_dom.select2("data");
                                location.href = baseUrl + "/costum/co/index/slug/" + context_data.slug + "/edit/true#" + page_link + "?text=coevent";
                            }
                        });
                    }
                }
            });
            $("#oct-save").off("click").on("click", function() {
                var self = $(this);
                if (self.prop("disabled"))
                    return (true);
                $("#oceco-tools-coevent-form").trigger("submit");
            });
            if (typeof contextId !== "undefined" && typeof contextType !== "undefined" && typeof contextName !== "undefined")
                $.ajax({
                    url: baseUrl + "/co2/element/select2list",
                    dataType: "json",
                    type: "post",
                    data: {
                        id: contextId,
                        type: contextType
                    },
                    success: function(response) {
                        if (response.results.length) {
                            context_input_dom.select2("data", {
                                id: response.results[0].id,
                                text: response.results[0].name,
                                image: response.results[0].image,
                                type: response.results[0].type,
                                slug: response.results[0].slug
                            });
                            context_input_dom.val(contextId).trigger("change");
                        }
                    }
                });
            create_event_button_dom.off("click").on("click", function() {
                var self = $(this);
                var data = $.extend({}, {
                    id: null,
                    type: null
                }, self.data());
                if (notEmpty(data.id) && notEmpty(data.type)) {
                    var default_value = {
                        organizer: {}
                    };
                    default_value.organizer[data.id] = {
                        name: data.text,
                        type: data.type,
                        profilThumbImageUrl: data.image
                    };
                    default_value.organizer[data.id]._id = {
                        $id: data.id
                    };
                    var build = {
                        afterSave: function(data) {
                            dyFObj.onclose = function() {
                                event_select_dom.build_event_select([{
                                    id: data.id,
                                    name: data.map.name,
                                    startDate: moment.unix(data.map.startDate.sec),
                                    endDate: moment.unix(data.map.endDate.sec),
                                }]);
                                event_select_dom.val(data.id).trigger("change");
                                modal_dom.css("z-index", "");
                            }
                            dyFObj.closeForm();
                        }
                    }
                    dyFObj.openForm("event", null, default_value, null, build);
                    dyFObj.onclose = function() {
                        modal_dom.css("z-index", "");
                    }
                    modal_dom.css("z-index", "99999");
                } else
                    self.nextAll(".error").show().text("Vous devez sélectionner un contexte");
            });
        }
    })()
</script>