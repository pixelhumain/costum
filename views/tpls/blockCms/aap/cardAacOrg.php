<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "nombreCard";
?>

<style id="css-<?= $kunik ?>">
    
.nb<?= $blockKey ?>.nombreCard-content {
    padding: 15px 5%;
    display: block;
    position: relative;
}

.nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    width: 100%;
}

.nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item {
    border-radius: 10px;
    border-bottom: 8px solid <?= $blockCms["css"]["colorTheme"]["color"] ?>;
    padding: 10px;
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    width: 20%;
    margin: 10px;
    position: relative;
}

@media (max-width : 992px) {
    .nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item {
        width: 100% !important;
    }
}
@media (max-width : 1200px) {
    .nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item {
        width: 100%;
    }
}

.nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item .nb-content {
    font-size: 60px;
    color: <?= $blockCms["css"]["colorTheme"]["color"] ?>;
    display: block;
    font-weight: 500;
}

.nb<?= $blockKey ?>.nombreCard-content .nombreCard-item-content .nombreCard-item .text-content {
    font-size: 24px;
    color: <?= $blockCms["css"]["colorTheme"]["color"] ?>;
    display: block;
    opacity: .7;
}

.com-card-icon {
    position: absolute;
    top: -50px;
    right: 20px;
    font-size: 70px;
}

</style>

<div class="nb<?= $blockKey ?> nombreCard-content <?= $kunik ?> <?= $kunik ?>-css">
    <div class="nombreCard-item-content">
        <div class="nombreCard-item com-card">
            <span class="com-card-icon"><i class="fa fa-empire"></i></span>
            <span class="nb-content" id="nbAac<?= $kunik ?>"><?= $blockCms["nombre"] ?></span>
            <span class="sp-text text-content text0<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texta0"></span>
        </div>
        <div class="nombreCard-item com-card">
            <span class="com-card-icon"><i class="fa fa-wpforms"></i></span>
            <span class="nb-content" id="nbCom<?= $kunik ?>"><?= $blockCms["nombre"] ?></span>
            <span class="sp-text text-content text1<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texta1"></span>
        </div>
        <div class="nombreCard-item com-card">
            <span class="com-card-icon"><i class="fa fa-pie-chart"></i></span>
            <span class="nb-content" id="nbCollect<?= $kunik ?>"><?= $blockCms["nombre"] ?></span>
            <span class="sp-text text-content text2<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texta2"></span>
        </div>
        <div class="nombreCard-item com-card">
            <span class="com-card-icon"><i class="fa fa-euro"></i></span>
            <span class="nb-content" id="nbObject<?= $kunik ?>"><?= $blockCms["nombre"] ?></span>
            <span class="sp-text text-content text3<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texta3"></span>
        </div>
    </div>
</div>

<script type="text/javascript">

    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    const php = {
		kunik: '<?= $kunik ?>',
		my_cms_id: '<?= $myCmsId ?>',
		block_cms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>))
	}
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var nombreCard = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputSimple",
                            options : {
                                name : "title",
                                label : "Titre",
                                collection : "cms"
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "colorTheme",
                                label : tradCms.chooseThemeColor,
                                inputs : [
                                    "color"
                                ]
                            }
                        },
                        "addCommonConfig"
                    ]
                },
                advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.cardAacOrg<?= $myCmsId ?> = nombreCard;
    }
    appendTextLangBased(".text0<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["texta0"]) ?>,"<?= $blockKey ?>");
    appendTextLangBased(".text1<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["texta1"]) ?>,"<?= $blockKey ?>");
    appendTextLangBased(".text2<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["texta2"]) ?>,"<?= $blockKey ?>");
    appendTextLangBased(".text3<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["texta3"]) ?>,"<?= $blockKey ?>");
    (function(){
        var post = {
            "searchType" : ["forms"],
            "indexMin" : 0,
            "initType" : "", 
            "count" : true,
            "countType" : ["forms"],
            "indexStep" : 30,
            "filters": {
                "aapType": ["aac"]
            }
        };
        var aacNombre = 0;
        var communsNombre = 0;
        var collectNombre = 0;
        var objectNombre = 0;
        var url = baseUrl + '/co2/aap/aac/method/aac_directory';
        ajaxPost(null, url, post, function (data) {
            console.log('data.count.forms',data.count.forms)
            aacNombre = data.count.forms
            $("#nbAac<?= $kunik ?>").text(aacNombre)
            $.each(data.results, function(index, value) {
                if(typeof value.stat != "undefined" && typeof value.stat.communs != "undefined") {
                    communsNombre += value.stat.communs;
                }
                if(typeof value.stat != "undefined" && typeof value.stat.collectedFond != "undefined") {
                    collectNombre += value.stat.collectedFond;
                }
                if(typeof value.stat != "undefined" && typeof value.stat.solicitedFund != "undefined") {
                    objectNombre += value.stat.solicitedFund;
                }
            })
            $("#nbCom<?= $kunik ?>").text(communsNombre)
            $("#nbCollect<?= $kunik ?>").text(collectNombre)
            $("#nbObject<?= $kunik ?>").text(objectNombre)
        });
    })();
</script>
