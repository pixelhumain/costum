<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "titleAndDesc";
?>


<style>
    
.titleAndDesc-content {
    padding: 15px 10%;
}
.titleAndDesc-content .text-title {
    font-size: 38px;
    display: block;
    position: relative;
    color: #43C9B7;
    font-weight: 500;
}
.titleAndDesc-content .textDesc {
    color: #000000;
    font-size: 20px;
    padding: 0 0 0 2px;
    line-height: 1.5;
    display: block;
}

</style>
<div class="titleAndDesc-content <?= $blockKey ?>">
    <div class="text-title">
        <span><?= $blockCms["title"] ?></span>
    </div>
    <span class="sp-text textDesc text1<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text1">
        
    </span>
    <span class="sp-text textDesc text2<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text2">
        
    </span>
</div>

<script type="text/javascript">

    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var titleAndDesc = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputSimple",
                            options : {
                                name : "title",
                                label : tradCms.label,
                                collection : "cms"
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            
                        }
                    ]
                },
                advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.titleAndDesc<?= $myCmsId ?> = titleAndDesc;
    }
    appendTextLangBased(".text1<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["text1"]) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".text2<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["text2"]) ?>,"<?= $blockKey ?>");
</script>