<?php
//Description des financement de la campagne
$myCmsId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];
$keyTpl = "";

$blockKey = (string)$blockCms["_id"];
$baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
$initFiles = Document::getListDocumentsWhere(
    array(
        "id" => $blockKey,
        "type" => 'cms',
        "subKey" => "notBackground"
    ),
    "image"
);
//$arrayImg = [];
$imgUrl = Yii::app()->controller->module->assetsUrl . '/images/thumbnail-default.jpg';
if (isset($initFiles[0]))
{
    $images = $initFiles[0];
    if (!empty($images["imagePath"]) && Document::urlExists($baseUrl . $images["imagePath"]))
        $imgUrl = $baseUrl . $images["imagePath"];
    elseif (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl . $images["imageMediumPath"]))
        $imgUrl = $baseUrl . $images["imageMediumPath"];
}
if ($imgUrl != Yii::app()->controller->module->assetsUrl . '/images/thumbnail-default.jpg' && $blockCms["image"] == "")
{
    $blockCms["image"] = $imgUrl;
}
$assets = [
    '/css/aap/campFinancDesc.css',
    '/js/aap/campFinancDesc.js',
];
HtmlHelper::registerCssAndScriptsFiles($assets, Yii::app()->getModule('costum')->assetsUrl);
?>
<style id="css-<?= $kunik ?>"></style>
<style>
    .campDesc-content {
		--default-button-color: #000;
		--active-button-color: #43C9B7;
		--dtl-costum-color1: <?= isset($costum["css"]["color"]["color1"]) ? $costum["css"]["color"]["color1"] : "#43c9b7" ?>;
		--dtl-costum-color2: <?= isset($costum["css"]["color"]["color2"]) ? $costum["css"]["color"]["color2"] : "#4623c9" ?>;
		--dtl-costum-color3: <?= isset($costum["css"]["color"]["color3"]) ? $costum["css"]["color"]["color3"] : "#ffc9cb" ?>;
	}
    @media (max-width : 550px) {
        .campDesc-content .campDesc-header span {
            font-size: 30px !important;
        }
        .campDesc-content .campDesc-body .desc-content .desc-title span {
            font-size: 22px !important;
        }
        .campDesc-content .campDesc-body .desc-content .finance-content .title, .campDesc-content .campDesc-body .desc-content .finance-content .chiffre {
            font-size: 15px !important;
        }
        .campDesc-content .campDesc-body .desc-content .action-content .btn-know-more {
            font-size: 14px !important;
        }
    }
</style>
<div class="campDesc-content <?= $kunik ?> <?= $kunik ?>-css" data-need-refreshblock="<?= $myCmsId ?>">
    <div class="campDesc-header">
        <span class="sp-text title0 title<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title0">
        
        </span>
    </div>
    <div class="campDesc-body">
        <div class="content">
            <div class="img-content">
                <img class="" src="<?php echo $blockCms["image"] == '' ? $imgUrl : $blockCms["image"] ?> " data-src="<?php echo $blockCms["image"] == '' ? $imgUrl : $blockCms["image"] ?> " alt="">
            </div>
            <div class="desc-content">
                <div class="desc-title">
                    <span><?= $blockCms["subTitle"] ?></span>
                </div>
                <div class="sp-text communDesc<?= $blockKey ?>"  id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="communDesc">
                    
                </div>
                <div class="commun-tags tags-content">
                    <!-- <span class="tags">tags</span>
                    <span class="tags">tags</span>
                    <span class="tags">tags</span> -->
                </div>
                <div class="progress-content">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                            70%
                        </div>
                    </div>
                </div>
                <div class="finance-content">
                    <div class="recolte">
                        <span class="title"><?= Yii::t("cms", "Collected")?> : </span>
                        <span class="chiffre">1520 <i class="fa fa-eur"></i></span>
                    </div>
                    <div class="objectif">
                        <span class="title"><?= Yii::t("cms", "Objective")?> : </span>
                        <span class="chiffre">1890 <i class="fa fa-eur"></i></span>
                    </div>
                </div>
                <div class="action-content">
                    <!-- <a href="<?= $blockCms["btnLink"] ?>" class="btn-know-more"><?= $blockCms["btnName"] ?> <i class="fa fa-long-arrow-right"> </i></a> -->
                    <a href="<?= !empty($blockCms["redirectPage"]) ? $blockCms["redirectPage"] . ".communId." . "" : "" ?>" id="actionBtn<?= $kunik ?>" class="btn-know-more lbh"><?php echo Yii::t("cms", "Go to")?> <i class="fa fa-long-arrow-right"> </i></a>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    (function() {
        var php = {
            kunik: '<?= $kunik ?>',
            my_cms_id: '<?= $myCmsId ?>',
            block_cms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>)),
            style_css: JSON.parse(JSON.stringify(<?= json_encode($styleCss) ?>)),
            aap: typeof aapObj === 'object' && aapObj ? aapObj : null,
            allFormsContext: JSON.parse(JSON.stringify(<?= json_encode($allFormsContext) ?>))
        }
        sectionDyf[php.kunik + 'BlockCms'] = php.block_cms;
        $('#css-' + php.kunik).append(cssHelpers.render.generalCssStyle(php.style_css).toString());
        if (costum.editMode) {
            cmsConstructor.sp_params[php.my_cms_id] = php.block_cms;
            cmsConstructor.blocks['campFinancDesc' + php.my_cms_id] = {
                configTabs: {
                    general: {
                        inputsConfig: [
                            {
                                type: "inputWithSelect",
                                options: {
                                    label: "Page de redirection",
                                    name: "redirectPage",
                                    configForSelect2: {
                                        maximumSelectionSize: 1,
                                        tags: Object.keys(costum.app)
                                    }
                                }
                            },
                            // {
                            //     type: "inputSimple",
                            //     options: {
                            //         name: "btnLink",
                            //         label: "LIen du boutton",
                            //         collection: "cms"
                            //     }
                            // },
                            {
                                type: "select",
                                options: {
                                    name: "form",
                                    label: 'Formulaire concerné',
                                    isSelect2: true,
                                    options: $.map(php.allFormsContext, function(val, key) {
                                        return {
                                            value: key,
                                            label: (val.name ? ucfirst(val.name.toLocaleLowerCase()) : "Pas de nom") + (val.type ? ` (${val.type})` : "")
                                        };
                                    })
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "filtersMoreOptions",
                                    label: "Options de filtres",
                                    class: "filtersMoreOptions",
                                    showInDefault: false,
                                    inputs: [
                                        {
                                            type : "select",
                                            options : {
                                                name : "applyFor",
                                                label : "À appliquer pour",
                                                options : [
                                                    {
                                                        value : "all",
                                                        label : "toutes les utilisateurs"
                                                    },
                                                    {
                                                        value : "forAdmin", 
                                                        label : "administrateur et super admin"
                                                    },
                                                    {
                                                        value : "forUser",
                                                        label : "simple utilisateur"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            type: "textarea",
                                            options: {
                                                label: "Filters option",
                                                name: "filtersOption"
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    style: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
                },
                afterSave: function(path, valueToSet, name, payload, value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                }
            }
        }
        appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["title0"]) ?>,"<?= $blockKey ?>");
        appendTextLangBased(".communDesc<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["communDesc"]) ?>,"<?= $blockKey ?>");
        campFinancDesc.call(php);
    })();
</script>