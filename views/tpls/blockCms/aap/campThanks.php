<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "campThanks";
?>

<style id="css-<?= $kunik ?>">
.thanks-content {
	/* background-color : #D8F4F0; */
	display : flex;
	justify-content: center;
	align-items: center;
	position: relative;
	/* height: 700px; */
}
.thanks-img-content {
	width: 50%;
}
.thanks-img-content img {
	width: 90%;
}
.thnk-title {
	font-size : 70px;
	color : #43C9B7;
	display: block;
}
.thnk-desc {
	font-size : 20px;
	color : #000;
	opacity: .8;
	display : block;
	margin-left: 4px;
}
.thanks-text-content {
	display : flex;
	/* justify-content: center;
	align-items: center; */
	flex-direction: column;
	width: 50%;
}

.top-form {
    display: block;
    position: absolute;
    top: 0;
    left: -35px;
    width: 150px;
    height: 300px;
    border-top-right-radius: 150px;
    border-bottom-right-radius: 150px;
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
	background-color: #43C9B7;
}

.bottom-form {
	display: block;
    position: absolute;
    bottom: 0;
    right: -35px;
    width: 150px;
    height: 300px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 150px;
    border-top-left-radius: 150px;
	background-color: #4623c9;
}
.mini-form {
    display: block;
    position: absolute;
    width: 40px;
    height: 40px;
    bottom: 20px;
    left: 40px;
    background-color: #D8F4F0;
    border-radius: 50%;
}
.nano-form {
    display: block;
    position: absolute;
    width: 20px;
    height: 20px;
    top: 20px;
    right: 100px;
    background-color:  #4623c9;
    border-radius: 50%;
}

.btn-back-home {
	padding: 10px 20px;
	font-size: 20px;
	color: #fff;
	background-color: #43C9B7;
	text-decoration: none;
	border: #43C9B7 2px solid;
	border-radius: 10px;
	margin : 40px 0 0 8px;
	transition: all .4s;
}
.btn-see-contrib {
	padding: 10px 20px;
	font-size: 20px;
	color: #4623c9;
	background-color: transparent;
	text-decoration: none;
	border: #4623c9 2px solid;
	border-radius: 10px;
	margin : 40px 0 0 8px;
	transition: all .4s;
}
.btn-back-home:hover { 
	text-decoration: none;
	border-radius: 20px;
}
.btn-see-contrib:hover { 
	text-decoration: none;
	border-radius: 20px;
}
.btn-back-content {
	margin-top: 50px;
}
.btn-back-content img {
	margin-top: -30px;
}
</style>

<div class="thanks-content <?= $kunik ?> <?= $kunik ?>-css">
	<!-- <div class="top-form"></div>
	<div class="bottom-form"></div>
	<div class="mini-form"></div>
	<div class="nano-form"></div> -->
    <div class="thanks-img-content">
        <img src="<?= Yii::app()->getRequest()->getBaseUrl(true) . Yii::app()->getModule("costum")->assetsUrl . '/images/aap/lesCommuns/aprecie.png'; ?>" alt="">
    </div>
    <div class="thanks-text-content">
		<span class="thnk-title"><?= $blockCms["title"] ?></span>
		<span class="thnk-desc"><?= $blockCms["desc"] ?></span>
		<div class="btn-back-content">
			<img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl.'/images/aap/lesCommuns/fleche2.png';?>" alt="">
			<a class="btn-back-home" href="javascript:;">Revenir à l'accueil</a>
			<a class="btn-see-contrib" href="javascript:;">Voir les contributions <i class="fa fa-long-arrow-right"></i></a>
		</div>
    </div>
</div>

<script>
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode($blockCms); ?>;
	var str = "";
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#css-<?= $kunik ?>").append(str);
	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
		var campThanks = {
			configTabs: {
				general: {
					inputsConfig: [
						{
							type: "inputSimple",
							options: {
								name: "title",
								label: "Titre",
								collection: "cms"
							}
						},
						{
							type: "inputSimple",
							options: {
								name: "desc",
								label: "Description",
								collection: "cms"
							}
						}
					]
				},
				style: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
			},
			afterSave: function(path, valueToSet, name, payload, value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
			}
		}
		cmsConstructor.blocks.campThanks<?= $myCmsId ?> = campThanks;
	}

</script>