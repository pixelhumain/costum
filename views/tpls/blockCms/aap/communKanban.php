<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "communKanban";
?>

<style id="css-<?= $kunik ?>">

</style>

<div class="commun-kanban-content kan-<?= $kunik ?> <?= $kunik ?> <?= $kunik ?>-css">
    <div data-kunik="<?= $kunik ?>" class="communKanban-<?= $kunik ?>" data-name="communKanban" data-id="<?= $myCmsId ?>">
        <div id="communKanbanAac<?= $kunik ?>">
        </div>
    </div>
</div>


<script type="text/javascript">
    const allFormsContext = <?php echo json_encode( $allFormsContext ); ?>;
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        $(".faq-collapse-content .panel-collapse").addClass("in")
        formulaireCible = [];
        $.each(allFormsContext, (formKey, formVal) => {
            formulaireCible.push({
                value : formKey,
                label : `Réponse du formulaire <b>${ formVal.name }</b>`
            });
        });
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var communKanban = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : 'select',
                            options : {
                                name : "formCible",
                                isSelect2 : true,
                                label : `Formulaire cible`,
                                options : formulaireCible
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "dataOptions",
                                label: "Type de données",
                                isSelect2: true,
                                options: [
                                    {
                                        value: "tags",
                                        label: "Filtrer par tags"
                                    },
                                    {
                                        value: "aapStep1m03ot9qymmfgashp7l",
                                        label: "Filtrer par besoin"
                                    }
                                ]
                            }
                        },
                    ]
                },
                style: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                },
                advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.communKanban<?= $myCmsId ?> = communKanban;
    }
    (function(){
        var dataCommun<?= $kunik ?>;
        var params<?= $kunik ?> = {     
            searchType : ["answers"],
            notSourceKey : true,
            fields : ["answers.aapStep1.titre", "answers.aapStep1.tags", "profilImageUrl",  "answers.aapStep1.aapStep1m03ot9qymmfgashp7l", "allVoteCount", "vote", "links"],
            filters : {
                "answers.aapStep1" : {
                    "$exists" : true
                },
                "answers.aapStep1.titre" : {
                    "$exists" : true
                },
                "form" : sectionDyf.<?php echo $kunik ?>BlockCms.formCible
            },
            indexStep : 0    
        };
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params<?= $kunik ?>,
            function(data){
                dataCommun<?= $kunik ?> = data.results;
            },null,null, {async:false}
        )
        loadCommunKanban(dataCommun<?= $kunik ?>)
        function loadCommunKanban(answerData) {
            var kanbanHeader = []
            var kanbanData = []
            var kanbanBesoin = []
            var kanbanDataType = sectionDyf.<?php echo $kunik ?>BlockCms.dataOptions;
            $.map(answerData, function(valeur, keys) {
                if(typeof valeur.answers != 'undefined' && typeof valeur.answers.aapStep1 != 'undefined' && typeof valeur.answers.aapStep1[kanbanDataType] != 'undefined' && Array.isArray(valeur.answers.aapStep1[kanbanDataType])) {
                    for(let i = 0 ; i < valeur.answers.aapStep1[kanbanDataType].length ; i++) {
                        kanbanBesoin.push(valeur.answers.aapStep1[kanbanDataType][i])
                        let kanbanCard = {
                            _id : keys+i,
                            id : keys,
                            title : "<b>"+valeur.answers.aapStep1.titre+"</b>",
                            header : sanitazeString(valeur.answers.aapStep1[kanbanDataType][i]),
                            canEditHeader: false,
                            canAddCard: false,
                            html : true,
                            name : valeur.answers.aapStep1.titre,
                            collection : valeur.collection,
                            canMoveCard: false,
                            actions: [
                                {
                                    'icon': 'fa fa-group',
                                    'badge' : typeof valeur.links != 'undefined' && typeof valeur.links.contributors != 'undefined' ? Object.keys(valeur.links.contributors).length : 0,
                                    'bstooltip' : {
                                        'text' : 'Contributeur(s)', 
                                        'position' : 'left'
                                    }, 
                                    'action': ''
                                },
                                {
                                    'icon': 'fa fa-heartbeat',
                                    'badge' : typeof valeur.allVoteCount != 'undefined' && typeof valeur.allVoteCount.love != 'undefined' ? valeur.allVoteCount.love : 0,
                                    'bstooltip' : {
                                        'text' : 'Interessé(s)', 
                                        'position' : 'left'
                                    }, 
                                    'action': ''
                                }
                            ]
                        }
                        kanbanData.push(kanbanCard)
                    }           
                } else {
                    let kanbanCard = {
                        _id : keys+'other',
                        id : keys,
                        title : "<b>"+valeur.answers.aapStep1.titre+"</b>",
                        header : "other",
                        canEditHeader: false,
                        canAddCard: false,
                        html : true,
                        name : valeur.answers.aapStep1.titre,
                        collection : valeur.collection,
                        canMoveCard: false,
                        actions: [
                            {
                                'icon': 'fa fa-group',
                                'badge' : typeof valeur.links != 'undefined' && typeof valeur.links.contributors != 'undefined' ? Object.keys(valeur.links.contributors).length : 0,
                                'bstooltip' : {
                                    'text' : 'Contributeur(s)', 
                                    'position' : 'left'
                                }, 
                                'action': ''
                            },
                            {
                                'icon': 'fa fa-heartbeat',
                                'badge' : typeof valeur.allVoteCount != 'undefined' && typeof valeur.allVoteCount.love != 'undefined' ? valeur.allVoteCount.love : 0,
                                'bstooltip' : {
                                    'text' : 'Interessé(s)', 
                                    'position' : 'left'
                                }, 
                                'action': ''
                            }
                        ]
                    }
                    kanbanData.push(kanbanCard)
                }
            })

            kanbanHeader = [...new Set(kanbanBesoin)];
            const objectArray = kanbanHeader.map(item => ({
                id: sanitazeString(item),
                label: item,
                editable: false,
            }));
            objectArray.splice(0, 1, {
                id: 'other',
                label: kanbanDataType != 'tags' ? 'Pas de besoin' : 'Pas de tags',
                editable: false,
            });
            console.log('kanbanHeader', kanbanHeader);
            console.log('kanbanData', kanbanData);
            console.log('objectArray', objectArray);
            let kanbanDom = $('#communKanbanAac<?= $kunik?>')
                .kanban({
                    headers : objectArray,
                    data : kanbanData,
                    editable: false,
                    language : mainLanguage,
                    canAddCard: false,
                    editable : false,
                    canAddColumn: false,
                    canEditCard: false,
                    canEditHeader: false,
                    canMoveCard: false,
                    canMoveColumn: false,
                    copyWhenDragFrom: [],
                    endpoint : `${baseUrl}/plugins/kanban/`,
                    defaultColumnMenus: [''],
                    onRenderDone(){
                        $('#kanban-wrapper-other .kanban-list-content').css({
                            'background-color' : 'gray'
                        })
                    }
                });
        }
        function sanitazeString(str) {
            str = str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
            str = str.replace(/\s+/g, '-');
            str = str.replace(/#/g, '');
            str = str.toLowerCase();
            return str;
        }
    })();
    
</script>