<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];
$keyTpl = "campCoFinancBanner";
?>

<style>
	.co-financ-banner-content {
		padding: 0;
		display: block;
		position: relative;
		width: 100%;
		background-color: #4623C9;
	}

	.co-financ-banner-content .banner-content {
		display: flex;
		align-items: center;
		position: relative;
	}

	.co-financ-banner-content .text-content {
		width: 50%;
		display: block;
		position: relative;
	}

	.co-financ-banner-content .img-content {
		width: 50%;
		display: block;
		position: relative;
	}
/* .co-financ-banner-content .banner-content .img-content .banner-img {
    width: 100%;
    display: flex;
    position: relative;
    align-items: center;
} */
.co-financ-banner-content .banner-text {
    padding: 5% 0 5% 10%;
}
	.co-financ-banner-content .campTitle {
		font-size: 5rem;
		display: block;
		position: relative;
		color: #43C9B7;
		font-weight: 600;
	}

	.co-financ-banner-content .campDesc {
		color: #ffff;
		font-size: 22px;
		padding-left: 2px;
	}

	.co-financ-banner-content .banner-img {
		padding: 10px;
		width: 100%;
		height: 700px;
	}

	.co-financ-banner-content .banner-img img {
		position: absolute;
		display: block;
		right: 0;
		width: 85%;    
		bottom: 20%;
	}

	.co-financ-banner-content .elipse-white {
		align-items: center;
		justify-content: center;
		height: 310px;
		display: flex;
		width: 310px;
		text-align: center;
		background-color: #ffff;
		color: #4623C9;
		font-size: 160px;
		font-weight: 900;
		border-radius: 50%;
		position: absolute;
		left: 26%;
		top: 25%;
		z-index: 5;
	}

	.co-financ-banner-content .elipse-pink {
		position: absolute;
		display: block;
		font-size: 100px;
		font-weight: 900;
		right: 26%;
		top: 68%;
		height: 100px;
		color: #ffff;
		z-index: 5;
	}

	.co-financ-banner-content .btn-cofinanc-content {
		margin-top: 40px;
		display: flex;
		justify-content: flex-end;
	}

	.co-financ-banner-content .btn-cofinanc-content .btn-cofinanc {
		padding: 20px;
		border-radius: 30px;
		border: 8px solid #43C9B7;
		background-color: transparent;
		color: #43C9B7;
		transition: all .4s;
		font-size: 20px;
		font-weight: 800;
		margin-left: 10px;
		margin-right: 20px;
	}

    .co-financ-banner-content .btn-cofinanc-content .btn-cofinanc:hover {
		border: 8px solid #43C9B7;
		background-color: #43C9B7;
		color: #fff;
		transition: all .4s;
		text-decoration: none;
	}

	@media screen and (max-width: 768px) {
		.co-financ-banner-content .img-content {
			display: none;
		}

		.co-financ-banner-content .text-content {
			width: 100%;
		}

		.co-financ-banner-content .banner-text {
			padding: 30px;
		}

		.co-financ-banner-content .btn-cofinanc-content {
			justify-content: center;
			flex-direction: row;
			align-content: center;
			align-items: center;
		}
	}
	@media (max-width : 550px) {
		.co-financ-banner-content .campTitle {
			font-size: 30px !important;
		}
		.co-financ-banner-content .campDesc {
			font-size: 16px !important;
		}
		.co-financ-banner-content .campDesc span {
			font-size: 16px !important;
		}
		.co-financ-banner-content .btn-cofinanc-content img {
			display: none;
		}
		.co-financ-banner-content .btn-cofinanc-content .btn-cofinanc {
			font-size: 16px !important;
			padding: 10px;
		}
	}
</style>

<div class="co-financ-banner-content">
	<div class="banner-content">
		<div class="text-content">
			<div class="banner-text">
				<span class="campTitle">
					<?= $blockCms["title"] ?>
				</span>
				<div class="sp-text campDesc campDesc<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="campDesc">

				</div>
				<?php if (isset($blockCms["showBtn"]) && ($blockCms["showBtn"] == true || $blockCms["showBtn"] == "true"))
				{ ?>
					<div class="btn-cofinanc-content">
						<img src="<?= Yii::app()->getRequest()->getBaseUrl(true) . Yii::app()->getModule("costum")->assetsUrl . '/images/aap/lesCommuns/fleche2.png'; ?>" alt="">
						<a href="<?= $blockCms["link"] ?>" class="btn-cofinanc">Participez à la campagne</a>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="img-content">
			<div class="banner-img">
				<img src="<?= Yii::app()->getRequest()->getBaseUrl(true) . Yii::app()->getModule("costum")->assetsUrl . '/images/aap/lesCommuns/chiffreFtl.png'; ?>" alt="">
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode($blockCms); ?>;
	var str = "";
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#css-<?= $kunik ?>").append(str);
	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
		var campCoFinancBanner = {
			configTabs: {
				general: {
					inputsConfig: [{
							type: "inputSimple",
							options: {
								name: "title",
								label: tradCms.label,
								collection: "cms"
							}
						},
						{

							type: "inputWithSelect",
							options: {
								label: "Lien du bouton",
								name: "link",
								configForSelect2: {
									maximumSelectionSize: 1,
									tags: Object.keys(costum.app)
								}
							}
						},
						{
							type: "groupButtons",
							options: {
								name: "showBtn",
								label: "Afficher le boutton",
								options: [{
										value: false,
										label: trad.no
									},
									{
										value: true,
										label: trad.yes
									}
								],
								defaultValue: cmsConstructor.sp_params["<?= $myCmsId ?>"].showBtn
							}
						}
					]
				},
				style: {
					inputsConfig: [{

					}]
				},
				advanced: {
                    inputsConfig: [
                    "addCommonConfig"
                    ]
                },
			},
			afterSave: function(path, valueToSet, name, payload, value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
			}
		}
		cmsConstructor.blocks.campCoFinancBanner<?= $myCmsId ?> = campCoFinancBanner;
	}
	appendTextLangBased(".campDesc<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($blockCms["campDesc"]) ?>, "<?= $blockKey ?>");
</script>