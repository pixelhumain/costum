<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "bannerTxtAndImg";

    $blockKey = (string)$blockCms["_id"];
    $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
    $initFiles = Document::getListDocumentsWhere(
      array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>"notBackground"
      ), "image"
    );
  //$arrayImg = [];
    $imgUrl = Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg';
    if(isset($initFiles[0])){
        $images = $initFiles[0];
        if(!empty($images["imagePath"]) && Document::urlExists($baseUrl.$images["imagePath"]))
            $imgUrl = $baseUrl.$images["imagePath"];
        elseif (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
            $imgUrl = $baseUrl.$images["imageMediumPath"];
    }
    if ($imgUrl != Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg' && $blockCms["image"] == "") {
        $blockCms["image"] = $imgUrl;
    }

?>

<style id="css-<?= $kunik ?>">
.banner-txt-img-content {
    padding: 0 10%;
    display: flex;
    position: relative;
    width: 100%;
}

.banner-txt-img-content .txt-content {
    width: 60%;
    padding: 10% 0;
}

.banner-txt-img-content .img-content {
    width: 40%;
}

.banner-txt-img-content .txt-content .txt-title {
    font-size: 45px;
    display: block;
    position: relative;
    color: #43C9B7;
    font-weight: 700;
}
.banner-txt-img-content .txt-content .txt-desc {
    font-size: 20px;
    line-height: 1.5;
}
.banner-txt-img-content .img-content img {
    width: 100%;
}
</style>

<div class="banner-txt-img-content <?= $kunik ?> <?= $kunik ?>-css">
    <div class="txt-content">
        <div class="txt-title">
            <?= $blockCms["title"] ?>
        </div>
        <div class="sp-text txt-desc text<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text">
        </div>
    </div>
    <div class="img-content">
        <img class="" src="<?php echo $blockCms["image"] == '' ? $imgUrl : $blockCms["image"]?> " data-src="<?php echo $blockCms["image"] == '' ? $imgUrl : $blockCms["image"]?> " alt="">
    </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var bannerTxtAndImg = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputSimple",
                            options : {
                                name : "title",
                                label : tradCms.label,
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputFileImage",
                            options : {
                                name : "image",
                                label : tradCms.image,
                                collection : "cms"
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }, 
                advanced: {
                    inputsConfig: [
                    "addCommonConfig"
                    ]
                },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.bannerTxtAndImg<?= $myCmsId ?> = bannerTxtAndImg;
    }
    appendTextLangBased(".text<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["text"]) ?>,"<?= $blockKey ?>");
</script>