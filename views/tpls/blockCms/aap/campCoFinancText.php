<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];
$keyTpl = "campCoFinancText";
$files = [
	'/js/aap/aapv2.js'
];
HtmlHelper::registerCssAndScriptsFiles($files, Yii::app()->getModule('co2')->getAssetsUrl());
?>
<style id="css-<?= $kunik ?>">
	@font-face {
		font-family: 'lunchType';
		src: url("<?= Yii::app()->getRequest()->getBaseUrl(true) . Yii::app()->getModule("costum")->assetsUrl . '/font/coFinancement/Lunchtype22-Regular.ttf'; ?>")
	}

	.co-financ-text-content .text-title {
		font-size: 36px;
		display: block;
		position: relative;
		color: #43C9B7;
		font-weight: 500;
		margin-bottom: 15px;
	}

	.co-financ-text-content .textDesc {
		color: #000000;
		font-size: 22px;
		padding: 0 0 0 2px;
		font-family: 'lunchType';
		margin-bottom: 20px;
	}
	.textDesc li, .textDesc ul, .textDesc a,.textDesc p, .textDesc span, .textDesc h1,.textDesc h2,.textDesc h3, .textDesc h4{
		font-family: "lunchType" !important;
	}
	.second-title {
		margin-top: 40px;
	}

	@media (max-width: 767px) {
		.co-financ-text-content .text-title {
			font-size: 22px !important;
		}

		.co-financ-text-content .textDesc {
			font-size: 14px !important;
		}

		.co-financ-text-content .textDesc p {
			font-size: inherit !important;
		}
	}
</style>
<div class="co-financ-text-content <?= $kunik ?>-css <?= $kunik ?>" data-block="<?= $blockKey ?>" data-need-refreshblock="<?= $myCmsId ?>">

</div>

<script type="text/javascript">
	var php = {
		kunik: '<?= $kunik ?>',
		my_cms_id: '<?= $myCmsId ?>',
		blockKey: '<?= $blockKey ?>',
		block_cms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>)),
		style_css: JSON.parse(JSON.stringify(<?= json_encode($styleCss) ?>)),
		aap: typeof aapObj === 'object' && aapObj ? aapObj : null,
		allFormsContext: JSON.parse(JSON.stringify(<?= json_encode($allFormsContext) ?>)),
		inputs: JSON.parse(JSON.stringify(<?= json_encode($inputs) ?>))
	}
	sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode($blockCms); ?>;
	var str = "";
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#css-<?= $kunik ?>").append(str);
	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
		var campCoFinancText = {
			configTabs: {
				general: {
					inputsConfig: [{
							type: "inputSimple",
							options: {
								name: "title1",
								label: tradCms.label,
								collection: "cms"
							}
						},
						{
							type: "inputSimple",
							options: {
								name: "title2",
								label: tradCms.label,
								collection: "cms"
							}
						},
						{
							type: "select",
							options: {
								name: "form",
								label: 'Formulaire concerné',
								isSelect2: true,
								options: $.map(php.allFormsContext, function(val, key) {
									return ({
										value: key,
										label: (val.name ? ucfirst(val.name.toLocaleLowerCase()) : "Pas de nom") + (val.type ? ` (${val.type})` : "")
									});
								})
							}
						},
						{
							type: "selectMultiple",
							options: {
								name: "inputs",
								label: 'Questionnaires',
								isSelect2: true,
								options: $.map(php.inputs, function(val, key) {
									var label;

									label = '';
									if (val.label)
										label = val.label;
									return ({
										value: key,
										label: label
									});
								})
							}
						}
					]
				},
				style: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
			},
			afterSave: function(path, valueToSet, name, payload, value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
			}
		}
		cmsConstructor.blocks.campCoFinancText<?= $myCmsId ?> = campCoFinancText;
	}
	// appendTextLangBased(".text1<?= $blockKey ?>", <?= json_encode($costum["language"]) ?>, <?= json_encode($blockCms["text1"]) ?>, "<?= $blockKey ?>");
	// appendTextLangBased(".text2<?= $blockKey ?>", <?= json_encode($costum["language"]) ?>, <?= json_encode($blockCms["text2"]) ?>, "<?= $blockKey ?>");
	// appendTextLangBased(".text3<?= $blockKey ?>", <?= json_encode($costum["language"]) ?>, <?= json_encode($blockCms["text3"]) ?>, "<?= $blockKey ?>");
	// appendTextLangBased(".text4<?= $blockKey ?>", <?= json_encode($costum["language"]) ?>, <?= json_encode($blockCms["text4"]) ?>, "<?= $blockKey ?>");
</script>
<script>
	(function($, X) {
		function dom_load_titles(args) {
			return (dataHelper.printf('' +
				'<div class="text-title">' +
				'	<span>{{title}}</span>' +
				'</div>' +
				'<div class=" textDesc text1{{block}}" id="sp-{{block}}" data-id="{{block}}">{{content}}</div>', args))
		}
		$(function() {
			var aap = new aapv2();
			aap.answer(aap.url().dot('communId'))
				.xhr()
				.get_answer_data()
				.then(function(db_answer) {
					var arg = {
						title: '',
						content: '',
						block: php.blockKey
					};
					var container_dom = $('.co-financ-text-content[data-block="' + php.blockKey + '"]');
					var answer;
					var inputs = {};

					if (php.block_cms.inputs)
						inputs = php.block_cms.inputs;
					if (db_answer.success) {
						answer = db_answer.content;
						container_dom.empty().html('')
						$.each(inputs, function(key, input) {
							var as1;

							as1 = {
								title: php.inputs[input].label
							};
							if (answer.steps.aapStep1)
								as1.content = dataHelper.markdownToHtml(answer.steps.aapStep1[input]);
							if (as1.content)
								container_dom.append(dom_load_titles($.extend({}, arg, as1)));
						});
					}
				});
		});
	})(jQuery, window)
</script>