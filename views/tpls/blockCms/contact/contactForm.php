<?php
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];

    $defaultTexts = ["agreeMsgAgree","agreeMsgInfo"];

    foreach ($defaultTexts as $key) {
        if( !(is_array($blockCms[$key]))){
            ${$key} = ["fr" => $blockCms[$key]];
        } else {
            ${$key} = $blockCms[$key];
        }
    };

?>

<?php 
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'block',
    ), "image"
  );

 ?>

<style id="contactform<?= $kunik ?>">
    .<?= $kunik ?>contactform .invalid-feedback {
        display: none;
        width: 100%;
        margin-top: 0.25rem;
        font-size: 80%;
        color: #dc3545;
    }

    .<?= $kunik ?>contactform .contact-email{
        text-transform : none !important;
    }

    .<?= $kunik ?>contactform .btn-edit-delete{
        display: none;
    }
    .<?= $kunik ?>contactform:hover .btn-edit-delete{
        display: block;
        position: absolute;
        top:0%;
        left: 50%;
        transform: translate(-50%,0%);
    }
    .<?= $kunik ?>contactform .p-img img{
        width: 100%;
        border:2px solid #bca87d;
    }
    .<?= $kunik ?>contactform .p-img{
        text-align: center
    }
    .<?= $kunik ?>contactform .btn-contact-me{
        background-color:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["btnBgColor"]; ?>;
        color:<?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $blockCms["btnLabelColor"]; ?>;
        padding: 14px 36px;
        border-radius: <?php echo $blockCms["btnRadius"] ?>!important;
        border:2px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $blockCms["btnBorderColor"]; ?>;
        font-size: large !important;
    }
    .<?= $kunik ?>contactform .btn-contact-me:hover{
        background-color:<?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $blockCms["btnLabelColor"]; ?>;
        color:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["btnBgColor"]; ?>;
        border: 2px solid <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["btnBgColor"]; ?>;
    }
    .<?= $kunik ?>contactform .container-img{
        padding: 0 80px;
    }
    .<?= $kunik ?>contactform .container-img{
        background-color:white;
        <?php if (!empty($initImage)) { ?>
        background-image:url(<?php echo $initImage[0]["imageThumbPath"] ?>) !important;
        <?php  }else{ ?>
        background-image:url(<?php  echo isset($defaultImg)??$defaultImg ; ?>) !important;
        <?php  } ?>
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        min-height: 413px;
        margin: 0 77px 0 58px;
    }
    @media (max-width:767px ){
        .<?= $kunik ?> #formContact,
        .<?= $kunik ?> .form-group,
        .<?= $kunik ?> .contain-input,
        .<?= $kunik ?> .title-contact
        {
            padding-left : 0;
            padding-right : 0;
        }

        .subTitle<?php echo $kunik ?> small{
            font-size : 11px !important;

        }
        .subTitle<?php echo $kunik ?>{ 
            margin-bottom: 10px !important;
        }

    }
    @media (max-width:768px ){
        .<?= $kunik ?>contactform .container-img{
            margin:0 15px 50px 15px !important;
        }
    }
    @media (max-width:1000px ){
        .<?= $kunik ?>contactform .container-img{
            margin:0 15px 0px 15px;
        }


    }
    .<?= $kunik ?>contactform #messageAfterSend i {
        color: <?php echo $blockCms["css"]["label"]["color"] ?>;
        font-size: 70px;
        width: 100%;
        display: block;
    }
    .<?= $kunik ?>contactform #messageAfterSend p {
        font-size: 25px;
        width: 100%;
        margin-top: 22px;
        text-align: center;
        color: <?php echo $blockCms["css"]["label"]["color"] ?>;
    }
    .<?= $kunik ?>contactform #messageAfterSend{
        display: none;
        padding-top: 10%;
        justify-content: center;
        width: 100%;
    }
    .<?= $kunik ?>contactform #messageAfterSend .mess {
        width: 50%;
        box-shadow: 1px 1px 5px 1px <?php echo $blockCms["css"]["champ"]["border"] ?>;
        padding: 3%;
    }
    @media (max-width:768px ){
        .<?= $kunik ?>contactform #messageAfterSend p {
            font-size: 18px;
        }
    }
    .subTitle<?php echo $kunik ?>{
        text-transform: none !important;
        margin-bottom: 50px;
        font-size: <?= $blockCms["css"]["subTitleBlockTextSize"]["fontSize"] ?>px;
    }

    .<?php echo $kunik ?>.modal{
        margin-top: 91px;
    }
    .<?php echo $kunik ?>.modal .modal-body .fa{
        color :#bca87d;
    }
    .<?= $kunik ?>contactform .contain-input {
        text-align: <?php echo $blockCms["labelAlign"] ?>;
    }
    /* .< ?= $kunik ?>contactform .sendbtn:hover {
        color: < ?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["css"]["sendBouton"]["backgroundColor"]; ?>;
        background-color: < ?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $blockCms["css"]["sendBouton"]["color"]; ?>;
        border: 2px solid < ?php echo $blockCms["css"]["sendBouton"]["borderColor"] ?>;
    } */
    .<?= $kunik ?>contactform .contain-sendbtn {
        text-align: <?php echo $blockCms["sendBtnAlign"] ?>;
    }
    .<?= $kunik ?>contactform  .checkbox-msg,
    .<?= $kunik ?>contactform  .checkbox-content label input[type="checkbox"]{
        color: <?php echo $blockCms["css"]["label"]["color"] ?>;
    }
    .<?= $kunik ?>contactform  .form-check{
        display : inline-flex;
    }
    .<?= $kunik ?>contactform .div-btn-openFormContact {
        text-align: <?= $blockCms["btnPosition"] ?>;
    }
</style>

<div class="<?= $kunik ?>contactform col-md-12 <?= $kunik ?>">
    <h2 class="title-1"><?php echo $blockCms["titleBlock"] ?></h2>
    <?php if($blockCms["showForm"] == "show") {?>
        <div id="formContact" class="col-xs-12 col-lg-10 col-lg-offset-1">
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left padding-top-60 form-group">
                <div class="col-xs-12 title-contact">
                    <h3 class="subTitle<?php echo $kunik ?>">
                        <?php if ($blockCms["subTitleBlock"] !== "") { ?>
                            <i class="fa fa-envelope color-label"></i>
                            <small class="color-label label"><?php echo $blockCms["subTitleBlock"] ?> </small>
                            <span id="contact-name" class="color-label label" style="text-transform: none!important;"></span><br>
                        <?php } ?>
                    </h3>
                    <!-- <div>
                        <span class="">
                        <span class="color-label label" ><?php //echo Yii::t('cms', 'This message will be sent to')?></span>
                        <b><span class="contact-email color-label label"></span></b>
                        </span>
                    </div> -->
                    <hr><br>
                </div>
                <div class="col-xs-12 no-padding">
                    <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                        <label for="email" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t("common","Your mail"); ?>*</label>
                        <input type="email" class="form-control contact-input champ" placeholder="exemple@mail.com" id="emailSender" required />
                        <div class="invalid-feedback emailSender"><?php echo Yii::t('cms', 'Invalid email')?>.</div><br>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                        <label for="objet" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Phone')?>*</label>
                        <input class="form-control contact-input champ" placeholder="<?php echo Yii::t('cms', 'Phone')?>" id="phoneSender" required />
                        <div class="invalid-feedback phoneSender"><?php echo Yii::t('cms', 'Invalid phone')?>.</div><br>
                    </div>
                </div>
                
                <div class="col-xs-12 no-padding">
                    <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                        <label for="senderName" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Name')?>*</label>
                        <input class="form-control contact-input champ" placeholder="<?php echo Yii::t('cms', 'Name')?>" id="senderName" required />
                        <div class="invalid-feedback senderName"><?php echo Yii::t('cms', 'Required field')?>.</div><br>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                        <label for="senderFirstName" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'First name')?>*</label>
                        <input class="form-control contact-input champ" placeholder="<?php echo Yii::t('cms', 'First name')?>" id="senderFirstName" required />
                        <div class="invalid-feedback senderFirstName"><?php echo Yii::t('cms', 'Required field')?>.</div><br>
                    </div>
                </div>
                

                <div class="col-md-12 col-sm-12 col-xs-12 contain-input">
                    <label for="objet" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Subject of your message')?>*</label>
                    <input class="form-control contact-input champ" placeholder="<?php echo Yii::t('cms', 'what is it about?')?>" id="subject" required/>
                    <div class="invalid-feedback subject"><?php echo Yii::t('cms', 'Required object')?>.</div><br>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left form-group">
                <div class="col-md-12 contain-input">
                    <label for="message" class="label-contact label">
                        <i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Your message')?>*
                    </label>
                    <textarea placeholder="Votre message..." class="form-control contact-input champ txt-mail" id="message" style="min-height: 200px;" required ></textarea>
                    <div class="invalid-feedback message">
                        <?php echo Yii::t('cms', 'Your message is empty')?>.
                    </div>
                </div>
            </div>
            <?php if($blockCms["showCharte"]){?>
			    <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left form-group showCharte">
				    <div class="checkbox-content pull-left no-padding">
                        <div class="form-check">
                            <input type="checkbox" class="agreeCo " id="agreeCo" name="agreeCo"></input>
                            <label for="agreeCo" class="">              
                                <div class="sp-text img-text-bloc agreeMsg checkbox-msg letter-white pull-left agreeMsgAgree<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="agreeMsgAgree"></div>
                            </label>
                        </div>
                        <div class="invalid-feedback agreeCo"><?php echo Yii::t('login', 'You must validate the CGU to sign up.')?></div>            
                        <br>
                        <div class="form-check">
                            <input type="checkbox" class="info" id="info" name="info"></input>
                        </div>
                            <label for="info" class="margin-top-10">
                                <div class="sp-text img-text-bloc agreeMsg checkbox-msg letter-white pull-left agreeMsgInfo<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="agreeMsgInfo"></div>
                            </label>
                        <div class="invalid-feedback info"><?php echo Yii::t('login', 'You must validate the CGU to sign up.')?></div><br>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left form-group">
                <div class="col-md-12 margin-top-15 contain-sendbtn">
                    <button type="submit" class="btn sendbtn sendBouton" id="btn-send-mail">
                        <i class="fa fa-send"></i> <?php echo $blockCms["sendBtnLabel"] ?>
                    </button>
                </div>
            </div>
        </div>
        <div id="messageAfterSend" class="  messageAfterSend text-center  footer-arianelila" > 
            <div class="mess footer-arianelila">
                <i class="fa fa-check-circle"> </i>
                <p><?php echo Yii::t('cms', 'Email sent to')?> <?= isset($costum["contactMail"])?$costum["contactMail"]:(isset($costum["admin"]["email"]) ? $costum["admin"]["email"] : "") ;?> !</p>
            </div>
        </div>
    <?php } else { ?>
        <div class="col-sm-12 col-md-12 div-btn-openFormContact padding-top-20">
            <a href="javascript:;" class="tooltips btn openFormContact openModBouton btn btn-contact-me" data-toggle="modal" data-target="#myModal-contact-us"
                data-id-receiver="" 
                data-email=""
                data-name="">
                <?php echo $blockCms["btnLabel"] ?>
            </a>     
        </div>
        <div class="portfolio-modal modal fade" id="formContact" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content padding-top-15">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                            
                        </div>
                    </div>
                </div>
                <div class="sp-cms-container bg-white">
                    <div id="form-group-contact">
                        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left padding-top-60 form-group">
                            <h3>
                                <i class="fa fa-send letter-blue"></i> 
                                <small class="letter-blue">
                                    <?php echo Yii::t('cms', 'Send an e-mail to')?> : 
                                </small>
                                <span id="contact-name" style="text-transform: none!important;"><?= isset($costum["contactMail"])?$costum["contactMail"]:(isset($costum["admin"]["email"]) ? $costum["admin"]["email"] : "") ;?></span>
                                <br>
                                <small class="">
                                    <small class="">
                                        <?php echo Yii::t('cms', 'This message will be sent to')?>
                                    </small>
                                    <b><span class="contact-email"></span></b>
                                </small>
                            </h3>
                            <hr><br>
                            <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                                <label for="email" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t("common","Your mail"); ?>*</label>
                                <input type="email" class="form-control contact-input champ" placeholder="exemple@mail.com" id="emailSender" required />
                                <div class="invalid-feedback emailSender"><?php echo Yii::t('cms', 'Invalid email')?>.</div><br>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                                <label for="objet" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Phone')?>*</label>
                                <input class="form-control contact-input champ" placeholder="<?php echo Yii::t('cms', 'Phone')?>" id="phoneSender" required />
                                <div class="invalid-feedback phoneSender"><?php echo Yii::t('cms', 'Invalid phone')?>.</div><br>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                                <label for="senderName" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Name')?>*</label>
                                <input class="form-control contact-input champ" placeholder="<?php echo Yii::t('cms', 'Name')?>" id="senderName" required />
                                <div class="invalid-feedback senderName"><?php echo Yii::t('cms', 'Required field')?>.</div><br>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                                <label for="senderFirstName" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'First name')?>*</label>
                                <input class="form-control contact-input champ" placeholder="<?php echo Yii::t('cms', 'First name')?>" id="senderFirstName" required>
                                <div class="invalid-feedback senderFirstName"><?php echo Yii::t('cms', 'Required field')?>.</div><br>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 contain-input">
                                <label for="objet" class="label-contact label"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Subject of your message')?>*</label>
                                <input class="form-control contact-input champ" placeholder="<?php echo Yii::t('cms', 'what is it about?')?>" id="subject">
                                <div class="invalid-feedback subject"><?php echo Yii::t('cms', 'Required object')?>.</div><br>

                            </div>
                        </div>
                        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left form-group">
                            <div class="col-md-12 contain-input">
                                <label for="message" class="label-contact label">
                                    <i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Your message')?>
                                </label>
                                <textarea placeholder="<?php echo Yii::t('cms', 'Your message')?>..." class="form-control contact-input champ txt-mail" id="message" style="min-height: 200px;"></textarea>
                                <div class="invalid-feedback message">
                                    <?php echo Yii::t('cms', 'Your message is empty')?>.
                                </div>
                                <br>
                            </div>
                            <div class="margin-top-15 contain-sendbtn">
                                <button type="submit" class="btn sendbtn sendBouton" id="btn-send-mail">
                                    <i class="fa fa-send"></i> <?php echo $blockCms["sendBtnLabel"] ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $blockCms ); ?>;

    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#contactform<?= $kunik ?>").append(str);

    var alignelabel = {
		"center" : tradCms.center,
		"left" : tradCms.left,
		"right" : tradCms.right
	};

    var contactAdmin = costum.contactMail || ((typeof costum.admin != "undefined" && typeof costum.admin.email != "undefined")?costum.admin.email:"");

    if(costum.editMode){
        cmsConstructor.sp_params["<?= $blockKey ?>"] = sectionDyf.<?php echo $kunik?>ParamsData;
        var contactForm = {
            configTabs : {
                general : {
                    inputsConfig : [
                        {
                            type: "inputSimple",
                            options: {
                                name: "subTitleBlock",
                                label: "<?php echo Yii::t('cms', 'Subtitle of the block')?>"
                            }
                        },
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "showForm",
                                label : "<?php echo Yii::t('cms', 'Type of display form')?>",
                                tabs: [
                                    {
                                        value:"show",  
                                        label: "<?php echo Yii::t('cms', 'Show direct')?>",
                                        inputs : []
                                    },
                                    {
                                        value:"modal"  , 
                                        label: "<?php echo Yii::t('cms', 'Modal')?>",   
                                        inputs :[ 
                                            {
                                                type: "colorPicker",
                                                options: {
                                                    name: "btnLabelColor",
                                                    label: "<?php echo Yii::t('cms', 'Color of the label')?>",
                                                    defaultValue: sectionDyf.<?php echo $kunik?>ParamsData.btnLabelColor
                                                }
                                            },
                                            {
                                                type: "colorPicker",
                                                options: {
                                                    name: "btnBgColor",
                                                    label: "<?php echo Yii::t('cms', 'Background color')?>",
                                                    defaultValue: sectionDyf.<?php echo $kunik?>ParamsData.btnBgColor
                                                }
                                            },
                                            {
                                                type: "colorPicker",
                                                options: {
                                                    name: "btnBorderColor",
                                                    label: "<?php echo Yii::t('cms', 'Border color')?>",
                                                    defaultValue: sectionDyf.<?php echo $kunik?>ParamsData.btnBorderColor
                                                }
                                            },
                                            {
                                                type: "number",
                                                options: {
                                                    name: "btnRadius",
                                                    units : ["px", "%"],
                                                    label: "<?php echo Yii::t('cms', 'Border radius')?>",
                                                    filterValue: cssHelpers.form.rules.checkLengthProperties, 
                                                    defaultValue: sectionDyf.<?php echo $kunik?>ParamsData.btnRadius
                                                }
                                            },
                                            {
                                                type: "inputSimple",
                                                options: {
                                                    name: "btnLabel",
                                                    label: tradCms.buttonlabelformodaldisplaytype,
                                                    defaultValue: sectionDyf.<?php echo $kunik?>ParamsData.btnLabel
                                                }
                                            },
                                            {
                                                type: "select",
                                                options: {
                                                name: "btnPosition",
                                                label: tradCms.buttonAlignment,
                                                options: $.map( alignelabel, function( key, val ) {
                                                    return {value: val, label: key}
                                                })
                                                }
                                            },
                                        ]
                                    }
                                ]
                            }
                        },
                        {
                            type: "select",
                            options: {
                              name: "labelAlign",
                              label: tradCms.fieldlabelalignment,
                              options: $.map( alignelabel, function( key, val ) {
                                return {value: val, label: key}
                              })
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name: "sendBtnLabel",
                                label: tradCms.labelleforSendbutton
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "sendBtnAlign",
                                label: tradCms.alignmentoftheSendbutton,
                                options: $.map( alignelabel, function( key, val ) {
                                  return {value: val, label: key}
                                })
                            }
                        },   
                        {
                            type: "inputSimple",
                            options: {
                                name: "showCharte",
                                label: tradCms.privacyPolicy,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        },  
                        {
                            type: "inputSimple",
                            options: {
                                name: "mailFeedBack",
                                label: tradCms.sendMailFeedback,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "subTitleBlockTextSize",
                                label: "<?php echo Yii::t('cms', 'Subtitle of the block')?>",
                                inputs: [
                                  "fontSize"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "label",
                                label: "<b>"+tradCms.label+"</b>",
                                inputs: [
                                  "fontSize",
                                  "color"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "champ",
                                label: "<b>"+tradCms.textfield+"<b>",
                                inputs: [
                                  "fontSize",
                                  "height",
                                  "borderType",
                                  "backgroundColor",
                                  "color",
                                  "border",
                                  "borderRadius"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "sendBouton",
                                label: "<b>"+tradCms.sendbutton+"</b>",
                                inputs: [
                                  "fontSize",
                                  "backgroundColor",
                                  "color",
                                  "borderColor",
                                  "borderRadius"
                                ]
                            }
                        }
                    ]
                },
                hover : { 
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "sendBouton",
                                label: "<b>"+tradCms.sendbuttononhover+"</b>",
                                inputs: [
                                  "backgroundColor",
                                  "color",
                                  "borderColor"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "openModBouton",
                                label: "<b>"+tradCms.contactmebuttononhover+"</b>",
                                inputs: [
                                  "backgroundColor",
                                  "color",
                                  "borderColor"
                                ]
                            }
                        }
                    ]
                },
                advanced : {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }

        cmsConstructor.blocks.contactForm<?= $blockKey ?> = contactForm;
    }

    appendTextLangBased(".agreeMsgAgree<?= $blockKey ?>",<?= json_encode($costum["language"]) ?>,<?= json_encode($agreeMsgAgree) ?>,"<?= $blockKey ?>");
    appendTextLangBased(".agreeMsgInfo<?= $blockKey ?>",<?= json_encode($costum["language"]) ?>,<?= json_encode($agreeMsgInfo) ?>,"<?= $blockKey ?>");
    if(typeof contactAdmin != "undefined" || contactAdmin != ""){
        $("#contact-name").html(contactAdmin);
    }
    //valid mail
    function validateEmail<?= $kunik ?>(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validatePhoneNumber<?= $kunik ?>(phoneNumber) {
        const re = /^(?=.{8,17}$)\+?[0-9]\d{0,2}([-. \s]?\d{2,4}){3,5}$/;;
        return re.test(phoneNumber);
    }
    //snd feedback email
    function sendMailFeedback<?= $kunik ?>(emailSender,senderFirstName,senderName){
    var paramsFeedBack = {
            tpl : "feedBackContactForm",
            tplMail : emailSender,
            replyTo: emailSender,
            //fromMail: contactAdmin, 
            tplObject:"feedBack",
            names:senderFirstName +" "+ senderName,
            subject : "feedBack", 
            names:senderFirstName +" "+ senderName,
            emailSender : contactAdmin,
            message : "Votre demande est pris en compte",
            sign : "L'équipe de la Technopole de La Réunion",
            logo:"",
        };
        ajaxPost(
            null,
            baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
            paramsFeedBack,
            function(data){ 
                if(data.result == true){
                    $.each(["emailSender","senderName","senderFirstName","subject","message","phoneSender"],(k,v)=>{$("#"+v).val("")})
                }else{
                    toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                }
            },
            function(xhr, status, error){
                toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
            }
        ); 
    }
   function validateContectForm<?= $kunik ?>(input=null){
        var acceptFields = true;
        var fields = ["emailSender","senderName","senderFirstName","subject","message","phoneSender","agreeCo","info"];
        
        if(input!=null){
            fields = input;
            if(typeof input == "string"){
                fields = [input];
            }
        }
        $.each(fields, function(k,v){
            let fieldValue = $("#"+v).val();
            let isRequired = $("#"+v).is(":required");
            let isValid = true;
            if(isRequired && fieldValue == ""){
                acceptFields=false
                isValid=false;
            }else if(isRequired && fieldValue != ""){
                if(v=="emailSender" && validateEmail<?= $kunik ?>(fieldValue)==false){
                    acceptFields=false;
                    isValid=false;
                }else if(v=="phoneSender" && validatePhoneNumber<?= $kunik ?>(fieldValue)==false) {
                    acceptFields=false;
                    isValid=false;
                }else if(v=="agreeCo" && sectionDyf.<?php echo $kunik ?>ParamsData.showCharte && !$("#"+v).is(":checked")){
                    acceptFields=false;
                    isValid=false;
                }
            }
            if(isValid==false){
                $("."+v).show();
            }else{
                $("."+v).hide();
            }
        })
        return acceptFields;
   }
    //send email
    function sendEmail<?= $kunik ?>(){
        var acceptFields = validateContectForm<?= $kunik ?>();
        var seconds = Math.floor(new Date().getTime() / 1000);
        var allowedTime = localStorage.getItem("canSend");
        if(acceptFields){
            localStorage.removeItem("canSend");
            var emailSender = $("#emailSender").val();
            var phoneSender = $("#phoneSender").val();
            var subject = $("#subject").val();
            var senderName = $("#senderName").val();
            var senderFirstName = $("#senderFirstName").val();
            var message = $("#message").val()+"<br /><br /> <?php echo Yii::t('cms', 'Phone')?> : "+phoneSender;
            var emailFrom = $(".contact-email").html();

            var params = {
                tpl : "contactForm",
                tplMail : contactAdmin,//emailFrom,
                replyTo: emailSender,
                //fromMail: contactAdmin, 
                tplObject:subject,
                subject :subject, 
                names:senderName,
                emailSender:emailSender,
                message : message,
                sign : senderFirstName +" "+ senderName+'<br>Téléphone: <a href="tel:'+phoneSender+'">'+phoneSender+'</a>',
                logo:"",
            };

            ajaxPost(
                null,
                baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
                params,
                function(data){ 
                    if(data.result == true){
                    localStorage.setItem("canSend", (seconds+300));
                    toastr.success("<?php echo Yii::t('cms', 'Your message has been sent')?>");
                        $("#formContact").hide();
                        $(".messageAfterSend").css("display" , "flex");
                        if(typeof sectionDyf.<?php echo $kunik ?>ParamsData.mailFeedBack != "undefined" && sectionDyf.<?php echo $kunik ?>ParamsData.mailFeedBack){
                            sendMailFeedback<?= $kunik ?>(emailSender,senderFirstName,senderName);
                        }
                        if(typeof sectionDyf.<?php echo $kunik ?>ParamsData.mailFeedBack != "undefined" && !sectionDyf.<?php echo $kunik ?>ParamsData.mailFeedBack){
                            $.each(["emailSender","senderName","senderFirstName","subject","message","phoneSender"],(k,v)=>{$("#"+v).val("")})
                        }  
                        localStorage.setItem("canSend", (seconds+300));
                    }else{
                        toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                        bootbox.alert("<p class='text-center text-red'><?php echo Yii::t('cms', 'Email not sent to')?> "+emailFrom+" !</p>");
                    }

                },
                function(xhr, status, error){
                    toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                }
            );  
        }
    }
    if (notNull(costum) && exists(costum.admin) && exists(contactAdmin) && contactAdmin != ""){
      $("#formContact .contact-email").html(contactAdmin);
    }
    //btn send email
    $("#btn-send-mail").click(function(){
        sendEmail<?= $kunik ?>();
    });

    $(".contact-input").on("focusout", function(){
        console.log("keyup", $(this).val());
        validateContectForm<?= $kunik ?>($(this).attr("id"));
    });

    $('#emailSender').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
    $('#name').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
    $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
    $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
    
    $(".<?= $kunik ?>contactform .openFormContact").click(function(){
        if (notNull(costum) && exists(costum.admin) && exists(contactAdmin) && contactAdmin != ""){
            $("#formContact").modal("show");
        }else{
            bootbox.alert("<?php echo Yii::t('cms', 'Please try again later')?>");
        }
    })
</script>