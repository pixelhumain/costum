<?php 
  $keyTpl ="e_contact";
  $loremIpsum = "lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
  $paramsData = [ 
    "btnBgColor" => "#008037",
    "btnColor" => "#ffffff",
    "btnLabel" => "ME CONTACTER",
    "btnReadMore" => false,
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }

  $defaultTexts = [
    "title" => "NOUS CONTACTER",
    "subTitle" => "lorem Ipsum is simply dummy text of the printing and typesetting industry",
    "description" => $loremIpsum
  ];
  
  foreach ($defaultTexts as $key => $defaultValue) {
    ${$key} = ($blockCms[$key] ?? ["fr" => $defaultValue]);
    ${$key} = is_array(${$key}) ? ${$key} : ["fr" => ${$key}];
    
    $paramsData[$key] = ${$key}[$costum['langCostumActive']] ?? reset(${$key});
  };


  $cssAnsScriptFiles = array(
    '/assets/vendor/jquery_realperson_captcha/jquery.realperson.css',
    '/assets/vendor/jquery_realperson_captcha/jquery.plugin.js',
    '/assets/vendor/jquery_realperson_captcha/jquery.realperson.min.js'
  //  '/assets/css/referencement.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->theme->baseUrl);

    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
 ?>
 <!-- ****************get image uploaded************** --> 
<?php 
  $defaultImgPath = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $LatestInitImageInText = [];
    $initImageInText = [];
    $LatestInitImageLogo = [];
    $initImageLogo = [];
    $LatestInitImageLogoFont = [];
    $initImageLogoFont = [];

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
        ),"image"
    );
    //var_dump($initImage);
    foreach ($initImage as $key => $value) {
      if ($value["subKey"] == "imageInText") {
        $initImageInText[] = $value; 
        $LatestInitImageInText[]= $value["imageThumbPath"];
      }
      if ($value["subKey"] == "Logo") {
        $initImageLogo[] = $value; 
         $LatestInitImageLogo[]= $value["imageThumbPath"];
      }
      if ($value["subKey"] == "logoFont") {
        $initImageLogoFont[] = $value; 
         $LatestInitImageLogoFont[]= $value["imageMediumPath"];
      }
    }
 ?>
<!-- ****************end get image uploaded************** -->
<style>
    .invalid-feedback {
    display: none;
    width: 100%;
    margin-top: 0.25rem;
    font-size: 80%;
    color: #dc3545;
  }
  .sub-container-right<?=$kunik ?>{
      <?php if(!empty($LatestInitImageLogoFont)){ ?>
        background-image:url('<?=  $LatestInitImageLogoFont[0] ?>') !important;
      <?php }else{ ?>
        background-image:url('<?=  $defaultImgPath ?>/bg_header.png') !important;
      <?php }?>
      background-size: cover;
      background-position: center;
      position: absolute;
      top: 0;
      width: 577px;
      height: 577px;
      right: 0px;
  }
  .sub-container-right<?= $kunik ?> img{
    position: absolute;
    top: 50%;
    left:61%;
    transform: translate(-50%,-50%);
    height: 50%;
  }
  .sub-container-left<?= $kunik ?> img{
    height: 237px;
    text-align: center;
  }

  .css-<?= $kunik ?>{
    width: 100%;
  }

  .css-<?= $kunik ?> .btn-contactez-nous{
      color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnColor"]; ?> !important;
      background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?> !important;
      font-weight: bold;
      margin: 0px 10px 38px 29px;
      padding: 14px 36px;
      border-radius: 12px;
      font-size: large;
  }
  .css-<?= $kunik ?> .btn-contactez-nous:hover{
      background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?> ;
    letter-spacing: 1px;
    -webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
    transition: all 0.4s ease 0s;
  }
    .css-<?= $kunik ?> .btn-contactez-nous{
      margin:0px;
    }
    .css-<?= $kunik ?> .btn-contactez-nous,.content<?php echo $kunik ?> .btn-savoir-plus{
        margin-top: 45px;
    }
    .css-<?= $kunik ?> .img-container{
      padding: 0px !important;
    }
    .sub-container-left<?=$kunik ?> .img-xs{
      display: none;
    }
    .sub-container-left<?=$kunik ?>{
      text-align: center;
    }
    @media (max-width: 992px){
      .css-<?= $kunik ?> .img-container{
        display: none;
      }
      .sub-container-left<?=$kunik ?> .img-xs{
        display: block;
        float: right;
      }
    }
    @media (max-width: 360px){
      .sub-container-left<?=$kunik ?> .img-xs{
        display: initial;
        float: none;
      }
      .sub-container-left<?=$kunik ?> .p-img{
        text-align: center;
      }
        .more<?php echo $kunik ?> .btn-contactez-nous,.content<?php echo $kunik ?> .btn-savoir-plus{
          margin-top: 45px;
          width: 100%
        }
    }
</style>
<div class="css-<?= $kunik ?>">
  <h1 class="title sp-text img-text-bloc title<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"></h1>
  <h2 class="subtitle sp-text img-text-bloc subtitle<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subTitle"></h2>
  <div class="col-md-6 col-xs-12 sub-container-left<?=$kunik ?>">
    <p class="p-img">
      <?php if(!empty($LatestInitImageInText[0])){ ?>
        <img class="" src="<?=  $LatestInitImageInText[0] ?>">
      <?php }else{?>
        <img class="" src="<?=  $defaultImgPath ?>/people_tree-02.png">
      <?php }?>


      <?php if(!empty($LatestInitImageLogo[0])){ ?>
        <img class="pull-right img-xs" src="<?=  $LatestInitImageLogo[0] ?>">
      <?php }else{ ?>
        <img class="img-xs lzy_img" src="<?= @$defaultImg ?>" data-src="<?= $defaultImgPath ?>/logo-02.png">
      <?php }?>
    </p>
    <div class="description more<?php echo $kunik ?> sp-text img-text-bloc description<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description" style="position: relative;z-index: 1;line-height: 1.6;"></div>
    <p class="">
     <a href="javascript:;" class="btn tooltips openFormContact btn-contactez-nous" data-toggle="modal" data-target="#myModal-contact-us" data-id-receiver="" data-email="" data-name="">
      <?php echo $paramsData["btnLabel"] ?>
    </a>
  </p>
</div>
<div class="col-md-6 col-xs-12 img-container">
  <div style="" class="sub-container-right<?= $kunik ?>">
    <?php if(!empty($LatestInitImageLogo[0])){ ?>
      <img class=" pull-right" src="<?=  $LatestInitImageLogo[0] ?>">
    <?php }else{?>
      <img class=" pull-right" src="<?=  $defaultImgPath ?>/logo-02.png">
    <?php }?>
  </div>
</div>

<div class="portfolio-modal modal fade" id="formContact" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content padding-top-15">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl">
        </div>
      </div>
    </div>

    <div class="sp-cms-container bg-white">

      <div id="form-group-contact">
        <div class="col-md-10 text-left padding-top-60 form-group">
          <h3>
            <i class="fa fa-send letter-blue"></i> 
            <small class="letter-blue">
              <?php echo Yii::t('cms', 'Send an e-mail to')?>: </small>
              <span id="contact-name" style="text-transform: none!important;"></span>
              <?php echo @$element["organizer"]["name"]; ?><br>
              <small class="">
                <small class=""><?php echo Yii::t('cms', 'This message will be sent to')?>
              </small>
              <b><span class="contact-email"></span></b>
            </small>
          </h3>
          <hr><br>
          <div class="col-md-6">
            <label for="email"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Your e-mail address')?>*</label>
            <input type="email" class="form-control" placeholder="<?php echo Yii::t('cms', 'Your e-mail address')?>: exemple@mail.com" id="emailSender">
            <div class="invalid-feedback emailSender"><?php echo Yii::t('cms', 'Invalid email')?>.</div><br>
          </div>

          <div class="col-md-6">
            <label for="senderName"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Name / First name')?></label>
            <input class="form-control" placeholder="<?php echo Yii::t('cms', 'What is your name?')?>" id="senderName">
            <div class="invalid-feedback senderName"><?php echo Yii::t('cms', 'Required field')?>.</div><br>
          </div>

          <div class="col-md-12">
            <label for="objet"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Subject of your message')?></label>
            <input class="form-control" placeholder="<?php echo Yii::t('cms', 'what is it about?')?>" id="subject">
            <div class="invalid-feedback subject"><?php echo Yii::t('cms', 'Required object')?>.</div><br>

          </div>
        </div>
        <div class="col-md-12 text-left form-group">
          <div class="col-md-12">
            <label for="message"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Your message')?></label>
            <textarea placeholder="<?php echo Yii::t('cms', 'Your message')?>..." class="form-control txt-mail"
              id="message" style="min-height: 200px;"></textarea>
              <div class="invalid-feedback message"><?php echo Yii::t('cms', 'Your message is empty')?>.</div><br>
              <div class="col-md-12 margin-top-15 pull-left">
                <button type="submit" class="btn btn-success pull-left" id="btn-send-mail">
                  <i class="fa fa-send"></i> <?php echo Yii::t('cms', 'Send the message')?>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  if (costum.editMode){
      cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>
  } 

	appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($title) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".subtitle<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($subTitle) ?>,"<?= $blockKey ?>");
  appendTextLangBased(".description<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($description) ?>,"<?= $blockKey ?>");

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
              "icon" : "fa-cog",
            
            "properties" : {
                
                "title" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Title')?>",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                "subTitle" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Subtitle')?>",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.subTitle
                },
                "description" :{
                      "inputType" : "textarea",
                      "markdown" : true,
                      "label" : "<?php echo Yii::t('cms', 'Description')?>",
                      values: sectionDyf.<?php echo $kunik ?>ParamsData.description
                },
                "btnLabel" : {
                    "inputType" : "Texte du boutton",
                    "label" : "<?php echo Yii::t('cms', 'Button label')?>",
                  
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabel
                },
                "btnColor" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Button color')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnColor
                },
                "btnBgColor" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Button background color')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                },
                "imageInText" :{
                  "inputType" : "uploader",
                  "label" : "<?php echo Yii::t('cms', 'Image on text')?>",
                  "docType": "image",
                  "contentKey" : "slider",
                  "itemLimit" : 1,
                  "endPoint": "/subKey/imageInText",
                  "domElement" : "imageInText",
                  "filetypes": ["jpeg", "jpg", "gif", "png"],
                  "showUploadBtn": false,
                  initList : <?php echo json_encode($initImageInText) ?>
                },
                "Logo" :{
                    "inputType" : "uploader",
                    "label" : "<?php echo Yii::t('cms', 'Logo')?>",
                    "docType": "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "endPoint": "/subKey/Logo",
                    "domElement" : "Logo",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "showUploadBtn": false,
                    initList : <?php echo json_encode($initImageLogo) ?>
                },
                "logoFont" :{
                    "inputType" : "uploader",
                    "label" : "<?php echo Yii::t('cms', 'Image background of the logo')?>",
                    "docType": "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "endPoint": "/subKey/logoFont",
                    "domElement" : "logoFont",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "showUploadBtn": false,
                    initList : <?php echo json_encode($initImageLogoFont) ?>
                },
                "btnReadMore":{  
                  "inputType" : "checkboxSimple",
                  "label" : "<?php echo Yii::t('cms', 'See more')?>",
                  "params" : checkboxSimpleParams,
                  "checked" : <?= json_encode($paramsData["btnReadMore"]) ?> 
                }         
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

<?php // ***************************send mail************************************* ?>
  $("#btn-send-mail").click(function(){
    sendEmail();
  });

  $('#emailSender').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#name').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});

  $(".openFormContact").click(function(){
    mylog.log("openFormContact");
    if (exists(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
      $("#formContact .contact-email").html(costum.admin.email);
      $("#formContact").modal("show");
    }else{
      bootbox.alert("<?php echo Yii::t('cms', 'Please try again later')?>");
    }
  })

  //readMoreLess(".more<?php echo $kunik ?>",5,"");
  <?php if($paramsData["btnReadMore"]==true){ ?>
			setTimeout(() => {
				$(".more<?php echo $kunik ?>").myOwnLineShowMoreLess({
				showLessLine: 10,
				showLessText:'<?php echo Yii::t("common", "Read less")?>',
				showMoreText:'<?php echo Yii::t("common", "Read more")?>',
				//lessAtInitial:false,
				//showLessAfterMore:false
				});
			}, 900);
		<?php } ?>
});
function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function sendEmail(){
  var acceptFields = true;
  $.each(["emailSender","senderName","subject","message"],(k,v)=>{
      if($("#"+v).val() == ""){
        $("."+v).show();
        acceptFields=false
      }
      if(validateEmail($("#emailSender").val())==false){
        acceptFields=false;
        $(".emailSender").show();
      }

  });
  var seconds = Math.floor(new Date().getTime() / 1000);
  var allowedTime = localStorage.getItem("canSend");
  if(acceptFields){
    if(seconds < allowedTime){
      return bootbox.alert("<p class='text-center text-dark'><?php echo Yii::t('cms', 'Send back after')?> "+(Math.floor((allowedTime-seconds)/60)==0 ? allowedTime-seconds +" <?php echo Yii::t('cms', 'second(s)')?>" : Math.floor((allowedTime-seconds)/60)+" <?php echo Yii::t('cms', 'minute(s)')?>")+ "</p>");
    }
    localStorage.removeItem("canSend");
        var emailSender = $("#emailSender").val();
        var subject = $("#subject").val();
        var senderName = $("#senderName").val();
        var message = $("#message").val();
        var emailFrom = $(".contact-email").html();

        var params = {
          tpl : "contactForm",
          tplMail : emailFrom,
          fromMail: emailSender, 
          emailSender:emailSender,
          tplObject:subject,
          subject :subject, 
          names:senderName,
          message : message,
          logo:"",
        };

        ajaxPost(
          null,
          baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
          params,
              function(data){ 
                if(data.result == true){
                  localStorage.setItem("canSend", (seconds+300));
                  toastr.success("<?php echo Yii::t('cms', 'Your message has been sent')?>");
                  bootbox.alert("<p class='text-center text-green-k'><?php echo Yii::t('cms', 'Email sent to')?> "+emailFrom+" !</p>");
                  $.each(["emailSender","senderName","subject","message"],(k,v)=>{$("#"+v).val("")})
                }else{
                  toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                  bootbox.alert("<p class='text-center text-red'><?php echo Yii::t('cms', 'Email not sent to')?> "+emailFrom+" !</p>");
                }   
              },
              function(xhr, status, error){
                  toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
              }
        );  
  }
}
</script>
