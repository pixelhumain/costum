<?php 
  $keyTpl ="concerts";
  $paramsData = [ 
    "title" => "CONCERTS",
    "dateBg" => "#f0ad16",
    "concertBg" => "rgb(0,0,0,.5)",
    "alternateCardDate" => false,
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
?>

<style>
	.toolsMenu a{
		display: none;
	}
	.container-<?= $kunik ?>{
		position: relative;
		width: 59%;
		height: 621px;
		/*background-color: grey;*/
		margin: 0 auto;
		padding: 5px 68px;
		overflow-y: auto;
		background: <?= $paramsData["concertBg"] ?>;
	}
	.container-<?= $kunik ?> .date-<?= $kunik ?>{
		position: relative;
		height: 145px;
		width: 100%;
		display: flex;
	    flex-direction: column;
	    justify-content: center;
	}
	.container-<?= $kunik ?> .date-<?= $kunik ?> h3,.info-<?= $kunik ?> h3{
		margin:0 !important;
	}
	.container-<?= $kunik ?> .info-<?= $kunik ?>{
		position: relative;
		height: 145px;
		width: 100%;
		padding-top: 10px;
	}
	.container-<?= $kunik ?> .name-<?= $kunik ?>{
		display: block;position: relative;height: 70%;border:0px solid red
	}
	.container-<?= $kunik ?> .hour-<?= $kunik ?>{
		display: flex;
	    position: relative;
	    height: 30%;
	    border: 0px solid red;
	    flex-direction: column;
	    justify-content: flex-end;
	}
	.container-<?= $kunik ?> .title-2 {
		font-size: 39px;
	}
	.container-<?= $kunik ?> .title-3 {
		font-size: 40px;
		font-weight: initial;
	}
	.container-<?= $kunik ?> .title-5,.container-<?= $kunik ?> .title-6 {
		font-weight: initial;
	}
	.container-<?= $kunik ?> .title-4,.container-<?= $kunik ?> .title-5 {
		overflow: hidden;
	   text-overflow: ellipsis;
	   display: -webkit-box;
	   -webkit-line-clamp: 2; /* number of lines to show */
	   -webkit-box-orient: vertical;
	}
	.container-<?= $kunik ?> .left-<?= $kunik ?>{
		background:<?= $paramsData["dateBg"] ?>;
	}
	.container-<?= $kunik ?> .btn-action-<?=$kunik ?>{
		position: absolute;
		top: 0;
		left:15px;
		z-index: 99;
	}
	@media (max-width: 1200px ){
		.container-<?= $kunik ?>{
			padding: 5px 0;
		}
	}
	@media (max-width: 400px){
		.container-<?= $kunik ?>{
			width: 100% !important;
		}
	}
	@media (max-width: 450px){
		.container-<?= $kunik ?> .right-<?= $kunik ?>{
			padding-left: 5px !important;
			padding-right: 5px !important;
		}
		.container-<?= $kunik ?> .btn-action-<?=$kunik ?>{
			position: absolute;
			top: 0;
			left:0;
			z-index: 99;
		}
	}
	@media (max-width: 630px){
		.container-<?= $kunik ?>{
			width: 80%;
		}
	}
	@media (max-width: 991px){
		.container-<?= $kunik ?> .date-<?= $kunik ?>{
			position: relative;
			height: 95px;
		}
		.container-<?= $kunik ?> .right-<?= $kunik ?>,.left-<?= $kunik ?>{
			border:1px solid <?= $paramsData["dateBg"] ?>;
		}
	}
	/* width */
	.container-<?= $kunik ?>::-webkit-scrollbar {
	  width: 3px;
	}

	/* Track */
	.container-<?= $kunik ?>::-webkit-scrollbar-track {
	  box-shadow: inset 0 0 5px grey; 
	  border-radius: 10px;
	}
	 
	/* Handle */
	.container-<?= $kunik ?>::-webkit-scrollbar-thumb {
	  background: <?= $paramsData["dateBg"] ?>; 
	  border-radius: 10px;
	}

	/* Handle on hover */
	.container-<?= $kunik ?>::-webkit-scrollbar-thumb:hover {
	  background: <?= $paramsData["dateBg"] ?>; 
	}
</style>
<br>
 <h3 class=" title-1 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"] ?></h3>
<?php if(Authorisation::isInterfaceAdmin()){ ?>
	<div style="text-align: center;width: 100%;">
		<button class="btn btn-success add-event<?=$kunik ?> hiddenPreview">Ajouter un évènement</button>
	</div>
<?php } ?>
<div class="container-<?= $kunik ?>">
	<h2 class="text-center"><i class="fa fa-spin fa-spinner fa-2x"></i></h2>
<!-- 	<div class="col-xs-12" style="margin-top: 45px;">
		<div class="btn-action-<?=$kunik ?>">
			<button class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></button>
			<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
		</div>
		<div class="col-md-5 bg-green">
			<div class="date-<?= $kunik ?>">
				<h3 class="title-2">22</h3>
				<h3 class="title-3">Avr 2021</h3>
			</div>
		</div>
		<div class="col-md-7">
			<div class="info-<?= $kunik ?>">
				<div class="name-<?= $kunik ?>">
					<h3 class="title-4">CONCERT EN LIGNE</h3>
					<h3 class="title-5">SUR LA PLATEFORME REMPAR</h3>
				</div>
				<div class="hour-<?= $kunik ?>">
					<h3 class="title-6"><i class="fa fa-clock-o"></i> 20h</h3>
				</div>
			</div>
		</div>		
	</div> -->
</div>

<script>
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
$(function(){
  sectionDyf.<?php echo $kunik ?>Params = {
    "jsonSchema" : {
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
      "icon" : "fa-cog",
      
      "properties" : {
          title:{
          	inputType:"text",
          	label:"<?php echo Yii::t('cms', 'Title')?>",
          	value: "<?= $paramsData["title"] ?>"
          },
          dateBg:{ 
          	inputType:"colorpicker",
          	label: "<?php echo Yii::t('cms', 'Date background color')?>",
          },
          concertBg:{
          	inputType:"colorpicker",
          	label: "<?php echo Yii::t('cms', 'Panel background color')?>",
          },
          alternateCardDate:{  
			"inputType" : "checkboxSimple",
			"label" : "<?php echo Yii::t('cms', 'Date of the card Alternate')?>",
			"params" : checkboxSimpleParams,
			"checked" : <?= json_encode($paramsData["alternateCardDate"]) ?> 
          }           
      },
      beforeBuild : function(){
          uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
      },
      save : function (data) {  
        tplCtx.value = {};
        tplCtx.format = true;
        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
          tplCtx.value[k] = $("#"+k).val();
          if (k == "parent")
            tplCtx.value[k] = formData.parent;
        });
        console.log("save tplCtx",tplCtx);

        if(typeof tplCtx.value == "undefined")
          toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
					toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
					$("#ajax-modal").modal('hide');

					var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
					var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
					var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
					cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }

      }
    }
  };


  $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
    tplCtx.id = $(this).data("id");
    tplCtx.collection = $(this).data("collection");
    tplCtx.path = "allToRoot";
    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"btn",2,6,null,null,"Bouton","blue","");
  });

  $('.add-event<?=$kunik ?>').click(function(){
        var dyfEvent={
          "beforeBuild":{
            "properties" : {

            }
          },
          "onload" : {
            "actions" : {
              	"setTitle" : "Ajouter un évènement",
              	"src" : { 
                	"infocustom" : "Remplir le champ"
              	},
              	"presetValue" : {
                    "type" : "link"
                },
	            "hide" : {
	              	/*"breadcrumbcustom" : 1,
                    "tagstags" :1,
                    "descriptiontextarea":1,
                    "locationlocation":1,
                    "formLocalityformLocality":1,
                    "parentfinder" : 1,
                    "removePropLineBtn" : 1*/
	            }
        	}
           }

          }
        dyfEvent.afterSave = function(data){
          dyFObj.commonAfterSave(data, function(){
           	urlCtrl.loadByHash(location.hash);

          });
        }
        dyFObj.openForm('event',null, null,null,dyfEvent);
	})

    getAjax('',baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+costum.contextType+'/id/'+costum.contextId+'/dataName/events/sort/1',
      function(data){ 
          mylog.log("andrana events",data);
          var html = "";
          var i= 1;
          $.each(data,function(k,v){
          	var dayObj = getDateFormated<?php echo $kunik ?>(v);
          	html += `<div class="col-xs-12" style="margin-top: 45px;">
          			<?php if(Authorisation::isInterfaceAdmin()){ ?>
	          			<div class="btn-action-<?=$kunik ?> hiddenPreview">
							<button class="btn btn-sm btn-primary edit-event-<?= $kunik ?>" data-id="${v.id}"><i class="fa fa-edit"></i></button>
							<button class="btn btn-sm btn-danger delete-event-<?= $kunik ?>" data-id="${v.id}"><i class="fa fa-trash"></i></button>
						</div>
					<?php } ?>`;
			if(sectionDyf.<?php echo $kunik ?>ParamsData.alternateCardDate == true && i%2 == 0){
				html +=	`<div class="col-md-7 right-<?= $kunik ?>">
						<div class="info-<?= $kunik ?>">
							<div class="name-<?= $kunik ?>">

								<h3>
									<a class="title-4 lbh-preview-element" href="#page.type.${v.collection}.id.${v.id}">${v.name}</a>
								</h3>
								<h3 class="title-5">${(exists(v.address.streetAddress) ? v.address.streetAddress : "")}</h3>
							</div>
							<div class="hour-<?= $kunik ?>">`;
							if(exists(dayObj.startDay) && exists(dayObj.endDay) && !exists(v.openingHours))
				html +=			`<h3 class="title-6">${dayObj.startLbl+' '+dayObj.startDay+' '+dayObj.startMonth+' '+dayObj.startYear+' '+dayObj.startTime}</h3>
						<h3 class="title-6">${dayObj.endLbl+' '+dayObj.endDay+' '+dayObj.endMonth+' '+dayObj.endYear+' '+dayObj.endTime}</h3>`;

							else if(!exists(v.openingHours))
				html +=			`<h3 class="title-6"><i class="fa fa-clock-o"></i>&nbsp;${dayObj.startTime}</h3>`;
				html +=		`</div>
						</div>
					</div>

					<div class="col-md-5 left-<?= $kunik ?>">
						<div class="date-<?= $kunik ?>">`;
						if(exists(v.openingHours)){
				html += dayObj.initOpeningHours;
						}else{
				html +=		`<h3 class="title-2">${dayObj.startDay}</h3>
							<h3 class="title-3">${dayObj.startMonth+' '+dayObj.startYear}</h3>`;
						}
				html +=	`</div>
					</div>`;

			}else{

				html +=	`<div class="col-md-5 left-<?= $kunik ?>">
							<div class="date-<?= $kunik ?>">`;
							if(exists(v.openingHours)){
					html += dayObj.initOpeningHours;
							}else{
					html +=		`<h3 class="title-2">${dayObj.startDay}</h3>
								<h3 class="title-3">${dayObj.startMonth+' '+dayObj.startYear}</h3>`;
							}
					html +=	`</div>
						</div>

				<div class="col-md-7 right-<?= $kunik ?>">
						<div class="info-<?= $kunik ?>">
							<div class="name-<?= $kunik ?>">

								<h3>
									<a class="title-4 lbh-preview-element" href="#page.type.${v.collection}.id.${v.id}">${v.name}</a>
								</h3>
								<h3 class="title-5">${(exists(v.address.streetAddress) ? v.address.streetAddress : "")}</h3>
							</div>
							<div class="hour-<?= $kunik ?>">`;
							if(exists(dayObj.startDay) && exists(dayObj.endDay) && !exists(v.openingHours))
				html +=			`<h3 class="title-6">${dayObj.startLbl+' '+dayObj.startDay+' '+dayObj.startMonth+' '+dayObj.startYear+' '+dayObj.startTime}</h3>
						<h3 class="title-6">${dayObj.endLbl+' '+dayObj.endDay+' '+dayObj.endMonth+' '+dayObj.endYear+' '+dayObj.endTime}</h3>`;

							else if(!exists(v.openingHours))
				html +=			`<h3 class="title-6"><i class="fa fa-clock-o"></i>&nbsp;${dayObj.startTime}</h3>`;
				html +=		`</div>
						</div>
					</div>`;				
			}

			html += `</div>`;
			i++;
          });
          $('.container-<?= $kunik ?>').html(html);
          coInterface.bindLBHLinks();
          $('.edit-event-<?= $kunik ?>').on('click',function(){
          	dyFObj.editElement('event',$(this).data('id'));
          })
          $('.delete-event-<?= $kunik ?>').on('click',function(){
                  var idEvent = $(this).data('id');
                  bootbox.confirm(trad.areyousuretodelete, function(result){ 
                      if(result){
                        var url = baseUrl+"/"+moduleId+"/element/delete/id/"+idEvent+"/type/events";
                        var param = new Object;
                        ajaxPost(null,url,param,function(data){ 
                                if(data.result){
                                  toastr.success(data.msg);
                                  urlCtrl.loadByHash(location.hash);
                                }else{
                                  toastr.error(data.msg); 
                              }
                        })
                      }
                  })          	
          })
      });
});


function getDateFormated<?php echo $kunik ?>(params, onlyStr, allInfos){
	var dateObj = {};
	if(exists(params.recurrency != 'undefined') && params.recurrency){
		dateObj.initOpeningHours = initOpeningHours<?php echo $kunik  ?>(params, allInfos);
	  return dateObj;
	}else{
	  params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
	  params.startDay = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
	  params.startMonth = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
	  params.startYear = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
	  params.startDayNum = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('d') : '';
	  params.startTime = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
	    
	  params.endDateDB = notEmpty(params.endDate) ? params.endDate: null;
	  params.endDay = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
	  params.endMonth = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
	  params.endYear = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
	  params.endDayNum = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).format('d') : '';
	  params.endTime = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
	  params.startDayNum = directory.getWeekDayName(params.startDayNum);
	  params.endDayNum = directory.getWeekDayName(params.endDayNum);

	  params.startMonth = directory.getMonthName(params.startMonth);
	  params.endMonth = directory.getMonthName(params.endMonth);
	  params.color='orange';
	    

	  var startLbl = (params.endDay != params.startDay) ? trad['fromdate'] : '';
	  var endTime = ( params.endDay == params.startDay && params.endTime != params.startTime) ? ' - ' + params.endTime : '';
	  mylog.log('params.allDay', !notEmpty(params.allDay), params.allDay);
	   
	    
	  var str = '';
	  var dStart = params.startDay + params.startMonth + params.startYear;
	  var dEnd = params.endDay + params.endMonth + params.endYear;
	  mylog.log('DATEE', dStart, dEnd);

	  if(params.startDate != null){
	    if(notNull(onlyStr)){
	      if(params.endDate != null && dStart != dEnd)
	    	dateObj.fromdate = trad.fromdate;
	      dateObj.startDay = params.startDay;

	      if(params.endDate == null || dStart == dEnd || (params.startMonth != params.endMonth || params.startYear != params.endYear))
	    		dateObj.startMonth = params.startMonth;
	      if(params.endDate == null || dStart == dEnd || params.startYear != params.endYear)
	     		dateObj.startYear = params.startYear;

	      if(params.endDate != null && dStart != dEnd){
	      	dateObj.endLbl = trad['todate'];
	      	dateObj.endDay = params.endDay;
	      	dateObj.endMonth = params.endMonth;
	      	dateObj.endYear = params.endYear;
	      }
	      	dateObj.startTime = params.startTime;
	      	dateObj.endTime = endTime;            
	    }else{
	    	dateObj.startLbl = startLbl;
	    	dateObj.startDayNum = params.startDayNum;
	    	dateObj.startDay = params.startDay;
	    	dateObj.startMonth = params.startMonth;
	    	dateObj.startYear = params.startYear;
	      if(params.collection == "events")
	      	dateObj.startTime = params.startTime;
	    }
	  }    
	    
	  if(params.endDate != null && dStart != dEnd && !notNull(onlyStr)){
	      	dateObj.endLbl = trad['todate'];
	      	dateObj.endDayNum = params.endDayNum;
	      	dateObj.endDay = params.endDay;
	      	dateObj.endMonth = params.endMonth;
	      	dateObj.endYear = params.endYear;
	        if(params.collection == "events")
	        	dateObj.endTime = params.endTime;
	  }   
	  return dateObj
	}
}

function initOpeningHours<?php echo $kunik  ?> (params, allInfos) {
    mylog.log('initOpeningHours', params, allInfos);
    var html = '<h5 class="text-center">';
    html += (notNull(allInfos)) ? '<span class=\' ttr-desc-4 bold uppercase" style="padding-left: 15px; padding-bottom: 10px;"\'><i class="fa fa-calendar"></i>&nbsp;'+trad.eachWeeks+'</span>' : '<span class=\' title-2 uppercase bold no-padding\'>'+trad.each+' </span>' ;
    mylog.log('initOpeningHours contextData.openingHours', params.openingHours);
    if(notNull(params.openingHours) ){
      var count=0;

      var openHour = '';
      var closesHour = '';
      $.each(params.openingHours, function(i,data){
        mylog.log('initOpeningHours data', data, data.allDay, notNull(data));
        mylog.log('initOpeningHours notNull data', notNull(data), typeof data, data.length);
        if( (typeof data == 'object' && notNull(data) ) || (typeof data == 'string' && data.length > 0) ) {
          var day = '' ;
          var dayNum=(i==6) ? 0 : (i+1);
          if(notNull(allInfos)){
            mylog.log('initOpeningHours data.hours', data.hours);
            day = '<li class="tl-item">';
            day += '<div class="item-title">'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</div>';
            $.each(data.hours, function(i,hours){
              mylog.log('initOpeningHours hours', hours);
              day += '<div class="item-detail">'+hours.opens+' : '+hours.closes+'</div>';
            });
            day += '</li>';
            if( moment().format('dd') == data.dayOfWeek )
              html += day;
            else
              html += day;
          }else{
            if(count > 0) html+=', ';

          
            var color = '';
          
            if( typeof agenda != 'undefined' && agenda != null && 
            (moment(agenda.getStartMoment(agenda.dayCount)).isoWeekday() - 1 ) == i &&
            typeof data.hours[i] != 'undefined' && 
            typeof data.hours[i].opens != 'undefined' && 
            typeof data.hours[i].closes != 'undefined' ) {

              openHour = data.hours[i].opens;
              closesHour = data.hours[i].closes;
            }
  

            html+= '<b style=\'font-variant: small-caps;font-size:x-large !important;\' class=\'title-3'+color+'\'>'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</b>';
          
          }
          count++;
        }
        
      });

      html +=  '&nbsp<small class=" margin-top-5"><b><i class="fa fa-clock-o"></i> '+openHour+'-'+closesHour+'</b></small>';
    } else 
      html = '<i>'+trad.notSpecified+'</i>'; 

    return html+'</h5>';
}
</script>