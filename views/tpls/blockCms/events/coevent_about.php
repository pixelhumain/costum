<?php

/**
 * @deprecated
 */

$costum = !empty($this->costum) ? $this->costum : $this->context->config['costum'];
if (!empty($costum) && !empty($costum["css"]) && !empty($costum["css"]["color"])) {
    $mainKey = array_keys($costum["css"]["color"])[0];
    $mainColor = $costum["css"]["color"][$mainKey];
}

$paramsData = [ 'primaryColor' => (!empty($mainColor)) ? $mainColor : '#EE302C' ];
$defaultTexts = [
    'mainTitle'    => 'Qui sommes-nous?',
    'aboutTitle'   => 'titre',
    'aboutContent' => 'Contenu',
];

foreach ($defaultTexts as $key => $defaultValue) {
    ${$key} = ($blockCms[$key] ?? ["fr" => $defaultValue]);
    ${$key} = is_array(${$key}) ? ${$key} : ["fr" => ${$key}];

    $paramsData[$key] = ${$key}[$costum['langCostumActive']] ?? reset(${$key});
};

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style>
    a.event-type-filter {
        display: block;
        cursor: pointer;
        text-decoration: none;
        color: white;
        padding: 0 2rem 0 0;
    }

    a.event-type-filter.active {
        color: white;
        text-decoration: none;
        padding: 0 0 0 2rem;
    }

    a.event-type-filter:hover {
        color: white;
        text-decoration: underline;
    }

    a.event-type-filter.active:hover {
        color: white;
        text-decoration: none;
    }

    @media only screen and (max-width: 425px) {
        .about-image {
            display: none;
        }
    }
</style>
<div id="quisommesnous<?= $blockKey ?>" data-anchor-target="quisommesnous" style="margin-bottom: 3%;" class="row infos">
    <div class="col-md-12">
        <h1 class="dinalternate text-center sp-text spTitleBlock mainTitle<?= $blockKey ?>" style="color: black;" data-id="<?= $blockKey ?>" data-field="mainTitle"></h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6" style="position: relative; min-height: 296px;">
                <img src="<?= $costum['assetsUrl'] . '/images/coevent/points_block_membre.png' ?>" alt="block membre" style="width: 100px; border-radius: 0;">
                <img src="<?= $costum['assetsUrl'] . '/images/coevent/img4.jpg' ?>" alt="block img4" class="about-image" style="width: 250px; margin-left: 75px; border-radius: 0;">
                <img src="<?= $costum['assetsUrl'] . '/images/coevent/img3.png' ?>" alt="block img3" class="about-image" style="width: 320px; position: absolute; top: 170px; left: 150px; border-radius: 0;">
                <div id="about-link-container<?= $blockKey ?>" style="position: absolute; top: 0; left: 90px; padding: 2rem 5rem 2rem 2rem; border-radius: 20px; background: <?= $paramsData['primaryColor'] ?>; color: white; font-size: 1.5rem; text-align: left; width: auto">
                    <span style="display: block; margin-bottom: 10px;">EVENEMENTS</span>
                </div>
            </div>
            <div class="col-md-6" style="text-align: left;">
                <h2 class="sp-text aboutTitle<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="aboutTitle"></h2>
                <div class="sp-text aboutContent<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="aboutContent"></div>
            </div>
        </div>
    </div>
</div>
<script>
    if (costum.editMode) {
            cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?>
    } else 

    appendTextLangBased(".mainTitle<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($mainTitle) ?>, "<?= $blockKey ?>");
    appendTextLangBased(".aboutTitle<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($aboutTitle) ?>, "<?= $blockKey ?>");
    appendTextLangBased(".aboutContent<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($aboutContent) ?>, "<?= $blockKey ?>");
    

    (function($) {
        function get_events() {
            var back_events = baseUrl + '/costum/agenda/events/request/all/reorder/1';
            return new Promise(function(__resolve, __reject) {
                ajaxPost(null, back_events, {
                    costumType: costum.contextType,
                    costumId: costum.contextId
                }, function(__data) {
                    __resolve(__data)
                }, function(__error) {
                    __reject(__error)
                }, 'json');
            });
        }

        function about_link_refresh() {
            var element_type_filter_elements = $('a.event-type-filter');
            element_type_filter_elements.on('click', function(__e) {
                __e.preventDefault();
                var type = this.getAttribute('href');
                var is_active = $(this).hasClass('active');
                var self = $(this);

                element_type_filter_elements.removeClass('active');
                if (!is_active) {
                    self.addClass('active');
                    sessionStorage.setItem('coevent-filter' + costum.contextId, type);
                } else {
                    self.removeClass('active');
                    sessionStorage.setItem('coevent-filter' + costum.contextId, '');
                }
                $('.active_filter_container:first').trigger('active-filter');
            });
        }

        $(function() {
            get_events().then(function(__events) {
                var html = '';
                var about_link_container = $('#about-link-container<?= $blockKey ?>');
                for (var i = 0; i < __events['groups']['length']; i++) {
                    var group = __events['groups'][i];
                    html += '<a class="event-type-filter" href="' + group.value + '">' + tradCategory[group.value] + '</a>';
                }
                html = about_link_container.html() + html;
                about_link_container.html(html);
                about_link_refresh();
            })
        });
    })(jQuery)
</script>