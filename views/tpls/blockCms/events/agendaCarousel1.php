<?php 
  $keyTpl ="agendaCarousel1";
  $paramsData = [ 
    "title" => "AGENDA",
    "subTitle" => "Ce que je propose",
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>
<?php
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>
<style>

  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
    z-index: 9999;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:50%;
    left: 50%;
    transform: translate(-50%,-50%);
  }
  .title<?php echo $kunik ?>{
    text-transform: none !important;
  }
  .subTitle<?php echo $kunik ?>{
    text-transform: none !important;
    margin-bottom: 50px;
  }
  .container<?php echo $kunik ?> .owl-item img{
    vertical-align: middle;
    height: 100%;
    border: 2px solid grey;
  }
  .container<?php echo $kunik ?> .owl-item .item{
    height: 327px;
  }
  .container<?php echo $kunik ?> .owl-item {
    transition: all .2s ease-in-out;
    height: 500px !important
  }
  .container<?php echo $kunik ?> .owl-item:hover {
    transform: scale(1.1);
  }
 .container<?php echo $kunik ?> .owl-stage-outer {
    padding: 35px 0px 0px 0px;
  }
  .container<?php echo $kunik ?> .event-place{
      color:#ffffff;
      text-transform: none !important;
  }
.container<?php echo $kunik ?> .event-name{
    color: #ffffff;
    text-transform: none !important;
}
.container<?php echo $kunik ?> .letter-orange{
  color:#000000 !important;
  text-transform: capitalize !important;
  font-weight: normal !important;
}
.container<?php echo $kunik ?> .info{
  padding: 0px 46px !important;
}

  .container<?php echo $kunik ?> .swiper-container {
    width: 100%;
    height: 300px;
    margin-left: auto;
    margin-right: auto;
  }

  .container<?php echo $kunik ?> .swiper-slide {
    background-size: cover;
    background-position: center;
  }
  .container<?php echo $kunik ?> .swiper-slide:hover .fa-eye{
    display: inline-block;
  }
  .container<?php echo $kunik ?> .swiper-slide .fa-eye{
    display: none;
    padding: 0 5px 0 5px;
    background: rgb(0,0,0,0.8);
    color: white;
    border-radius: 0 0 16px 0;
    font-size: 24px;
  }

  .container<?php echo $kunik ?> .info-events{
    background-color: rgb(8 8 8 / 60%);
    position: absolute;
    width:100%;
    height: auto;
    bottom: 0;
    left: 50%;
    transform: translate(-50%, 0%);
  }
  .container<?php echo $kunik ?> .info-events .fa-map-marker{
    color: #fff;
  }
  .container<?php echo $kunik ?> span,.container<?php echo $kunik ?> b{
    color: #ffffff !important;
  }
  .container<?php echo $kunik ?> span{
    padding-left:10px;
  }
  @media (max-width: 450px){
    .container<?php echo $kunik ?> .info-events{
      width: 100%;
    }
  }
  .container<?php echo $kunik ?> .description{
    color: white;
      padding:0 5px 0 5px;
     overflow: hidden;
     text-overflow: ellipsis;
     display: -webkit-box;
     -webkit-line-clamp: 2; /* number of lines to show */
     -webkit-box-orient: vertical;
     font-size: 13px;
     margin-top: 20px;
  }
  .container<?php echo $kunik ?> h3 small,.container<?php echo $kunik ?> .letter-orange{
    color: white !important;
  }
  .container<?php echo $kunik ?> h3{
        padding:0 10px 0 10px !important;
  }
  .container<?php echo $kunik ?> .add-event-caroussel{
    visibility: hidden
  }
  .container<?php echo $kunik ?>:hover .add-event-caroussel{
    visibility: visible;
  }
</style>
<div class="container<?php echo $kunik ?> col-md-12">
	<h2 class=" title title<?php echo $kunik ?> Oswald sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
    	<?php echo $paramsData["title"] ?>
  	</h2>
  	<h3 class="subtitle subTitle<?php echo $kunik ?> ReenieBeanie sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subTitle">
    	<?php echo $paramsData["subTitle"] ?>
	</h3>
  <?php if(Authorisation::isInterfaceAdmin()){ ?>
    <p class="text-center">
        <button class="btn btn-primary add-event-caroussel"><?php echo Yii::t('cms', 'Add an event')?></button>
    </p>
  <?php } ?>
  <div class="swiper-container">
    <div class="swiper-wrapper"></div>
     <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your agenda.')?>",
            "description" : "<?php echo Yii::t('cms', 'Personalize your agenda')?>",
            "icon" : "fa-cog",
            
            "properties" : {
                        
            },
              beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              	else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');

                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              	}

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        //add-event*****************
        $(".container<?php echo $kunik ?> .add-event-caroussel").on('click',function(){
          var eventsObj ={
            afterSave : function(data){
              dyFObj.commonAfterSave(data, function(){
                urlCtrl.loadByHash(location.hash);
              });
            }
          };
          dyFObj.openForm('event',null,null,null,eventsObj)
        })
	});

</script>

<script>
$(function ($) {
var html = "";
  getAjax('',baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+costum.contextType+
            '/id/'+costum.contextId+'/dataName/events',
          function(data){ 
              mylog.log("andrana events",data);
              $.each(data,function(k,v){
                html += 
                      `<div class="swiper-slide" style="background-image:url(${v.profilImageUrl})">
                       <a href="${v.profilImageUrl}" data-lightbox="all"><i class="fa fa-eye fa-2x"></i></a>
                          <div class="info-events">
                              <h4 class="text-center">
                                <a href="#page.type.${v.collection}.id.${v.id}" class="lbh event-place text-center Oswald other">
                                ${v.name}
                                </a>
                              </h4>
                              <h5 class="text-center">
                              <i class="fa fa-map-marker"></i>
                                <a href="#page.type.${v.collection}.id.${v.id}" class="lbh event-place text-center Oswald other">
                                ${((typeof v.address.level4Name !="undefined") ? v.address.level4Name : "Addresse non renséigné")}
                                ${((typeof v.address.streetAddress !="undefined") ? v.address.streetAddress : "Addresse non renséigné")}
                                </a>
                              </h5>
                              ${directory.getDateFormated(v)}
                              <p class="description">${v.shortDescription != null ? v.shortDescription : ""}</p>
                          </div>
                      </div>`;
                
              });
              $('.container<?php echo $kunik ?> .swiper-wrapper').html(html);
              var swiper = new Swiper(".container<?php echo $kunik ?> .swiper-container", {
                slidesPerView: 1,
                spaceBetween: 0,
                // init: false,
                pagination: {
                  el: '.swiper-pagination',
                  clickable: true,
                },
                navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
                keyboard: {
                  enabled: true,
                },
               /* autoplay: {
                  delay: 2500,
                  disableOnInteraction: false,
                },*/
                breakpoints: {
                  640: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                  },
                  768: {
                    slidesPerView: 2,
                    spaceBetween: 40,
                  },
                  1024: {
                    slidesPerView: 3,
                    spaceBetween: 50,
                  },
                }
              });
              coInterface.bindLBHLinks();
          }
  ,"");
});
</script>
