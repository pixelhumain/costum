<?php
 

 $myCmsId        = $blockCms["_id"]->{'$id'};
 $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
 $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
 $otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
 $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
 $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";

$base_url = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
$todayPhp = date('Y-m-d');
HtmlHelper::registerCssAndScriptsFiles([
	"/js/coevent/carousel.js",
	"/js/coevent/day_filter.js",
	"/css/coevent/day_filter.css",
], Yii::app()->getModule('costum')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles([
	"/plugins/knightlab_timeline3/css/timeline.css",
	"/plugins/knightlab_timeline3/js/timeline.js",
], Yii::app()->baseUrl);
?>

<style id="coevent_program<?= $kunik ?>" ></style>

<style>
	#program-container<?= $myCmsId ?> {
		text-align: center;
		padding-top: 10px;
	}

	#journee-caroussel<?= $myCmsId ?> {
		position: relative;
	}

	.rolesLabel.attendeesCounter:hover+.attendeeThumb {
		display: inline-block !important;
	}

	.attendeeThumb:hover {
		display: inline-block !important;
	}

	.rolesLabel.attendeesCounter:hover {
		cursor: pointer !important;
	}

	.listsItemsPod h4 {
		display: none;
	}

	.slide {
		list-style: none;
		flex: 0 0 auto;
		width: 100%;
	}

	button.program-view:nth-child(2) {
		margin-left: 2em;
		margin-right: 2em;

	}

	button.program-view {
		border: none;
		background-color: unset;
		font-size: 18px;
	}

	.event-detail li a {
		color: white !important;
		font-weight: bold;
		font-size: 18px
	}

	.slider {
		width: 100%;
		overflow: hidden;
		margin: 0 auto;
		position: relative;
	}

	.slides {
		display: flex;
		flex-direction: row;
		height: 100%;
		width: 100%;
		margin: 0;
		padding: 0;
	}

	#items<?= "$myCmsId " ?> {
		display: flex;
		align-items: center;
		justify-content: center;
	}

	#items<?= "$myCmsId " ?>.item {
		cursor: pointer;
		display: flex;
		flex-direction: column;
		align-items: center;
	}

	#items<?= "$myCmsId " ?>.item .date {
		background-color: white;
		color: #01305E;
		padding: 1.2rem 1.5rem .8rem;
		border-radius: 10px;
		display: block;
		font-weight: bold;
		font-size: 1.9rem;
		width: fit-content;
		width: -moz-fit-content;
	}

	#items<?= "$myCmsId " ?>.item.active .date {
		color: white;
	}

	#items<?= "$myCmsId " ?>.item .jour,
	.month {
		text-align: center;
		font-weight: bold;
		display: block;
		margin: auto 10px;
		color: #01305E;
	}

	#journee-navigation<?= "$myCmsId " ?>.journee-control {
		position: absolute;
		top: 5px;
		padding: 1.9rem 1rem;
		border-radius: 10px;
		border: transparent 1px solid;
		background-color: transparent;
	}

	#journee-navigation<?= "$myCmsId " ?>.journee-control:hover {
		background-color: white;
		cursor: pointer;
	}

	.journee-control.right {
		right: 40px;
	}

	.journee-control.left {
		left: 40px;
	}

	#event-content<?= $myCmsId ?>.empty {
		margin: 0 auto;
	}

	#event-content<?= $myCmsId ?>:not(.empty) {
		padding: 2rem;
		margin-left: 5rem !important;
	}

	#event-content<?= "$myCmsId " ?>.date {
		font-size: 2rem;
		font-weight: bold;
		display: block;
	}

	#event-content<?= "$myCmsId " ?>ul {
		margin: none;
	}

	#event-content<?= "$myCmsId " ?>.event-detail {
		position: relative;
		font-weight: bold;
		margin-right: 40px;
	}

	#event-content<?= "$myCmsId " ?>.event-detail li {
		list-style: none;
		margin-top: 2rem;
	}

	#event-content<?= "$myCmsId " ?>.event-detail-detail li {
		margin-top: 0;
		line-height: 1;
	}

	#event-content<?= "$myCmsId " ?>.event-detail-detail li::before {
		color: black;
	}

	#event-content<?= "$myCmsId " ?>.event-hour {
		font-weight: bold;
		position: absolute;
		left: -8rem;
		top: -0.3rem;
	}

	#active_filter_container<?= $myCmsId ?> {
		color: white;
		position: fixed;
		left: 50px;
		padding: 10px;
		border-radius: 0 0 10px 10px;
		box-shadow: 0 0 10px rgba(0, 0, 0, .3);
		z-index: 9;
		cursor: pointer;
	}

	.coevent-button<?= $myCmsId ?> {
		border: none;
		border-radius: 20px;
		padding: 1rem 2rem;
		color: white;
		margin-bottom: 10px;
	}

	.coevent-button<?= "$myCmsId " ?>:hover {
		text-decoration: none;
	}

	.coevent-button<?= "$myCmsId " ?>.inverted {
		background-color: white;
	}

	@media only screen and (max-width: 425px) {
		.journee-control.left {
			left: 0;
		}

		.journee-control.right {
			right: 0;
		}

		.ce-map-create-event {
			position: absolute;
			top: 25px !important;
			left: 25% !important;
			z-index: 11;
			transform: translateX(-50%);
		}		

		.filter-btn-hide-map {
			top: 25px !important;
			border-radius: 5px;
			width: auto;
		}

		#ce-prog-map-container<?= "$myCmsId " ?>	.leaflet-top .leaflet-control{
			margin-top: 75px !important;
		}

	}

	.program-organizer-image {
		display: inline-block;
		width: 40px;
		height: 40px;
		border-radius: 50%;
		box-shadow: 0 0 10px rgba(0, 0, 0, .2);
		overflow: hidden;
	}

	.program-organizer-image img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.event-detail li {
		position: relative;
	}

	.line-indicator {
		position: absolute;
		top: 15px;
		left: -65px;
		width: 60px;
		height: 5px;
		display: block;
		border-radius: 50px;
	}

	.point-holder {
		background-color: blue;
		display: block;
		height: 10px;
		width: 10px;
		border-radius: 50%;
		position: absolute;
		/* top: px; */
		left: -66px;
	}

	@media only screen and (max-width: 992px) {

		#program-container<?= $myCmsId ?> {
			text-align: center;
			max-width: 100%;
		}
	}

	.rolesLabel {
		font-size: 17px;
		font-weight: 500;
	}

	.actorNames {
		color: #01305E;
		font-size: 16px;
		font-style: italic;
		display: inline-block;
		margin-right: 5px;
	}

	.actorNames .img-container {
		width: 50px;
		height: 45px;
		overflow: hidden;
		display: inline-block;
		vertical-align: middle;
		margin-right: -30px;
	}

	.actorNames .img-container img {
		width: 100%;
		height: 100%;
		object-fit: cover;
		border-radius: 50%;
    	border: 2px solid white;
	}

	.btn-participate {
		color: white;
		margin-left: 30px;
		border: none;
		padding: 8px 12px;
		border-radius: 4px;
		cursor: pointer;
		font-size: 14px;
	}

	.btn-edit-coevent{
		color: black;
		margin-left: 5px;
		border: none;
		padding: 8px 12px;
		border-radius: 4px;
		cursor: pointer;
		font-size: 14px;
	}
	.btn-delete-coevent{
		color: white;
		margin-left: 5px;
		border: none;
		padding: 8px 12px;
		border-radius: 4px;
		cursor: pointer;
		font-size: 14px;
	}

	.btn-details-inscri{
		float: right;
	}

	.btn-participate:hover {
		color: #01305E;
		background-color: white;
	}

	.descr-event {
		font-size: 16px;
	}

	.filters-activate {
		border: 2px solid #fff;
		margin-right: 5px;
		margin-bottom: 5px;
		padding: 5px 10px;
		border-radius: 5px;
		color: #4623C9 !important;
		text-decoration: none;
		display: inline-block;
		background-color: white;
	}

	.filters-activate i {
		margin-right: 5px;
	}

	.program-content {
		text-align: left;
		width: 100%;
	}

	.coevent-program-viewer {
		display: none;
	}

	.coevent-program-viewer.show {
		display: block;
	}

	.df-container {
		--color-normal: #01305E;
		--color-invert: #f0f0f0;
	}

	.ce-prog-map-container {
		display: none;
		position: fixed;
		width: 100vw;
		z-index: 11;
	}

	.ce-prog-map-container .map-content {
		width: 100%;
		height: 100%;
	}

	.filter-btn-hide-map {
		top: 40px;
		border-radius: 5px;
		width: auto;
	}

	.ce-map-create-event {
		position: absolute;
		top: 40px;
		left: 50%;
		z-index: 11;
		transform: translateX(-50%);
	}

	.ce-map-title {
		font-size: 18px;
	}

	.ce-map-profile {
		display: inline-block;
		width: 50px;
		height: 50px;
		overflow: hidden;
		border-radius: 50%;
		vertical-align: middle;
		margin-right: 5px;
	}

	.ce-map-profile img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.ce-map-banner {
		width: 100%;
		height: auto;
		margin-bottom: 5px;
		overflow: hidden;
		border-radius: 6px;
	}

	.ce-map-banner img {
		width: 100%;
	}

	.no-events {
		text-align: center;
	}

	.modal-header {
		--main1: #82c17f;
	}

		/* Modal Custom Styles */
	.event-container {
		display: flex;
		gap: 20px;
	}

	.event-image {	
		color: #ffffff;
	}

	.event-image img {
		object-fit: cover;
		border-radius: 10px;
	}

	.event-details {
		flex: 1;
		display: flex;
		flex-direction: column;
		color: #ffffff;
	}

	h2 {
		font-size: 24px;
		margin-bottom: 15px;
	}

	.event-time {
		display: flex;
		align-items: center;
		gap: 10px;
		margin-bottom: 15px;
	}

	label {
		font-size: 14px;
		margin-right: 5px;
	}

	input[type="datetime-local"] {
		background-color: #242B4A;
		color: #ffffff;
		border: 1px solid #555;
		border-radius: 5px;
		padding: 5px;
	}

	.timezone {
		margin-left: auto;
		font-size: 12px;
	}

	.event-location button,
	.event-description button {
		background-color: #2D2F48;
		color: #ffffff;
		border: none;
		border-radius: 5px;
		padding: 10px;
		margin-bottom: 10px;
		cursor: pointer;
	}

	.event-options {
		display: flex;
		gap: 15px;
		margin-bottom: 15px;
	}

	.option {
		flex: 1;
		display: flex;
		align-items: center;
		justify-content: space-between;
	}

	.option input[type="text"],
	.option input[type="checkbox"] {
		background-color: #242B4A;
		color: #ffffff;
		border: 1px solid #555;
		border-radius: 5px;
		padding: 5px;
	}

	.create-event-button {
		background-color: #3949AB;
		color: #ffffff;
		border: none;
		border-radius: 5px;
		padding: 10px;
		cursor: pointer;
		align-self: flex-start;
	}



	/* modal createevent */
	.modal-boostrap-create-event {
		background-color: #423e3ebe;
		margin-top: 30px;
	}

 
	.modal-boostrap-create-event .modal-body,.modal-footer{
		background-color: #131a58;
	}
	

	.modal-boostrap-create-event .modal-header {
		display: flex;
		align-items: center;
		justify-content: space-around;
		padding-bottom: 0;
		background-color: #e5e5e5;
		position: relative;
		padding-bottom: 15px;
	}

	.modal-boostrap-create-event .modal-header button.close {
		position: absolute;
		right: 15px;
		top: 18px;
		opacity: .75;
	}

	.modal-boostrap-create-event .modal-header::before,
	.modal-boostrap-create-event .modal-header::after {
		content: none;
	}

	.modal-boostrap-create-event .modal-header .tab-item {
		margin: 2px 5px 0px 5px;
		padding-bottom: 10px;
		cursor: pointer;
		text-transform: none;
		text-align: center;
		font-size: 17px;
		text-decoration: none;
	}

	.modal-boostrap-create-event .modal-body .link-to-doc-content {
		display: flex;
		align-items: center;
		justify-content: flex-end;
	}

	.modal-boostrap-create-event .modal-body a.link-to-doc {
		background-color: #1890FF;
		padding: 8px 15px;
		font-weight: bold;
		color: white;
		border-top-right-radius: 10px;
		border-top-left-radius: 10px;
		font-size: 1.38rem;
	}

	.modal-boostrap-create-event .modal-body a.link-to-doc i {
		margin-right: 5px;
	}

	.modal-boostrap-create-event .modal-header a:hover,
	.modal-boostrap-create-event .modal-header a.active {
		border-bottom: 1px solid #f3aa31;
	}

	.modal-boostrap-create-event .btn-submit {
		display: flex;
		align-items: flex-start;
		justify-content: space-between;
	}

	.modal-boostrap-create-event .btn-submit p {
		font-size: 13px;
		text-align: justify;
		font-weight: 400;
		flex: 0.70;
	}

	.modal-boostrap-create-event .btn-submit p a,
	.modal-boostrap-create-event .btn-submit p b {
		font-weight: bold;
	}

	.modal-boostrap-create-event .btn-submit p a {
		color: #1890FF;
	}

	.modal-boostrap-create-event #tabQuestion h5 {
		text-align: center;
		font-size: 14px;
		text-transform: none;
		font-weight: 500;
	}

	.modal-boostrap-create-event #tabQuestion h5 a {
		font-weight: bold;
	}

	.modal-boostrap-create-event .btn-submit button {
		min-width: 100px;
	}

	.modal-boostrap-create-event .form-group textarea {
		height: 150px;
	}

	.modal-boostrap-create-event .form-group .check-value {
		display: flex;
		align-items: center;
		justify-content: flex-start;
	}

	.modal-boostrap-create-event .form-group .check-value .form-check:not(:first-child) {
		margin-left: 10px;
	}


	.inputContainerCoEvent {
		background-color: #2A2D6F;
		color : white;
		padding: 10px 20px;
		border-radius: 8px;
		display: flex;
		flex-direction: column;
	}

	.inputContainerCoEvent  textarea{
		background-color: #2A2D6F ;
		color : white ;
	}

	.inputContainerCoEvent  .select2-container-multi .select2-choices .select2-search-field input  {
		background-color: #2A2D6F !important ;
		color : white ;
	}

	.inputContainerCoEvent  .select2-container-multi .select2-choices{
		background-color: #2A2D6F !important ;
	}


	.inputContainerCoEvent  input {
		background-color: #2A2D6F ;
		color : white ;
	}

	.inputContainerCoEvent  select {
		background-color: #2A2D6F !important;
		color : white !important;
	}

	.inputContainerCoEvent  textarea{
		height: 80px ;
	}

	.inputContainerCoEvent .select2-container,.select2-choices {
		background-color: #2A2D6F ;
		color : white;
		z-index: 999999;
	}

	.event-card {
            background-color: #2c2c2e;
            border-radius: 8px;
            padding: 20px;
			color: white ;
            justify-content: space-between;
            align-items: center;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.3);
    }

	.warningStartDate,.warningEndDate {
		color: red !important;
	}

	.coevent-program{
		font-family: Arial, sans-serif;
	}

	.modal-personnalise {
		width: 60%; 
		height: 80vh; 
	}

	.co-into-content {
		display: flex;
		justify-content: space-between;
	}

	.co-timeline-avatar {
		width: 150px;
    	height: auto;
    	border-radius: 15px;
	}

	.co-timeline-block {
		display: flex; 
		margin-bottom: 5px;
	}

	.co-btn-manage-event {
		border-radius: 5px;
		padding: 10px 20px;
		background: #242427;
		border: none;
	}

	.co-btn-manage-event:hover {
		background: #141414;
	}

	.co-content-slide {
		padding: 10px;
	}

	.co-transform-deg {
		transform: rotate(90deg)
	}


	[data-target="slide-content"] {
		height: 0;
		overflow: hidden;
		visibility: hidden;
	}

	.slide-down {
		animation: slide-down 0.2s linear both;
	}

	.slide-up {
		animation: slide-up 0.2s linear both;
	}

	.co-parent-img a img {
		border-radius: 50% !important;
	}

	.co-remaining {
		background: white;
		height: min-content;
		color: black;
		margin-top: 20px;
		padding: 2px 3px;
		border-radius: 10px;
		border: 1px solid black;
		margin-left: 6px;
	}

	@keyframes slide-down {
		0% {
			visibility: hidden;
			height: 0;
		}

		95% {
			visibility: visible;
			height: slide-content-height;
		}

		100% {
			visibility: visible;
			height: auto;
		}
	}

	@keyframes slide-up {
		from {
			visibility: visible;
			height: slide-content-height;
		}

		to {
			visibility: hidden;
			height: 0;
		}
	}

	.co-calendar .fc-past {
		background: none !important;
	}

	.co-calendar .fc-state-default {
		color: #fff ! important;
		opacity: 1 !important;
	}

	.co-calendar .fc-state-default:hover {
		opacity: 0.8 !important;
	}

	.co-calendar .fc-state-default.fc-state-active {
		background: #fff !important;
		color: #000 ! important;
	}

	#timeline-embed<?= $myCmsId ?> {
		height: 80vh !important;
	}

	#timeline-embed<?= $myCmsId ?> .tl-slider-container-mask {
		/* background-color: #1c1c1e !important; */
	}
	
	#timeline-embed<?= $myCmsId ?> .tl-timenav {
		/* background-color: #1c1c1e; */
		border-top: none !important;
	}
	
	#timeline-embed<?= $myCmsId ?> .tl-timeaxis {
		background: black;
	}
	
	#timeline-embed<?= $myCmsId ?> .tl-timemarker-timespan {
		background: transparent !important;
	}
	
	#timeline-embed<?= $myCmsId ?> .tl-menubar-button {
	}

	#timeline-embed<?= $myCmsId ?> .tl-attribution a {
		color: #000 !important;
	}

	#event_calendar_container<?= $myCmsId ?> .fc-day-header {
	}

	#event_calendar_container<?= $myCmsId ?> .fc-day-header span {
		text-transform: uppercase;
	}

	#event_calendar_container<?= $myCmsId ?> .fc-today.fc-state-highlight {
		/* background: #444 !important; */
	}

	#timeline-embed<?= $myCmsId ?> button.tl-menubar-button span {
		/* color: #000 !important; */
	}

	#timeline-embed<?= $myCmsId ?> .tl-attribution {
		/* color: #000 !important; */
	}
	
	#timeline-embed<?= $myCmsId ?>  .tl-timemarker.tl-timemarker-active .tl-timemarker-content-container .tl-timemarker-content .tl-timemarker-text h2.tl-headline {

	}
	
	#timeline-embed<?= $myCmsId ?> .tl-timeaxis-background	,
	#timeline-embed<?= $myCmsId ?> .tl-timemarker-content-container {
		/* background: #000 !important;
		color: #fff ! important;
		border: 1px solid #c7d301; */
	}

	@media only screen and (max-width: 768px) {
		.modal-dialog.modal-personnalise.modal-dialog-centered.co-resposnive {
			width: auto !important;
		}

		.event-detail.co-timeline-parent {
			padding-left: 2px;
			margin-right: 5px;
		}

		.co-into-content {
			display: block;
		}
	}


</style>
<div class="<?= $kunik ?> other-css-<?= $kunik ?> <?= $otherClass ?>">
	<div id="active_filter_container<?= $myCmsId ?>" class="active_filter_container" style="display: none;">
		<?= Yii::t("cms","Click here to deactivate the filter") ?> : <div id="active_filter_text<?= $myCmsId ?>" class="active_filter" data-type=""></div>
		<div id="tooltipeventfilter<?= $myCmsId ?>" class="tooltip"><?= Yii::t("cms","Click here to deactivate the filter") ?></div>
	</div>
	<div class="ce-prog-map-container" id="ce-prog-map-container<?= $myCmsId ?>">
		<div class="map-content"></div>
		<button class="coevent-button<?= $myCmsId ?> btn-participate ce-map-create-event" type="button">
			<?= Yii::t("cms","Submit an event") ?> <i class="fa fa-calendar"></i>
		</button>
		<button type="button" class="filter-btn-hide-map"> <?= Yii::t("cms","Programm") ?> </button>
	</div>
	<div class="container coevent-program">
		<div id="leprogramme<?= $myCmsId ?>" data-anchor-target="leprogramme" class="row">
			<div class="col-sm-12 program-container" id="program-container<?= $myCmsId ?>">
				<!-- <h1 class="dinalternate" style="color: black;">
					Le programme
				</h1> -->
				<div class="btn-group journee_views" style="margin-bottom: 1em;">
					<button type="button" class="program-view" data-target="#event_calendar_container<?= $myCmsId ?>"><?= Yii::t("cms","Calendar") ?></button>
					<button type="button" class="program-view" data-target="#journee-caroussel<?= $myCmsId ?>"><?= Yii::t("cms","Filter by day") ?></button>
					<button type="button" class="program-view" data-target="#chronology_container<?= $myCmsId ?>"><?= Yii::t("cms","Timeline") ?></button>
					<button type="button" class="program-view no-view" data-target="#map-view<?= $myCmsId ?>"> <i class="fa fa-map-marker"></i> <?= Yii::t('common', 'Map') ?></button>
				</div>
				<div id="journee-caroussel<?= $myCmsId ?>" class="coevent-program-viewer">
					<div class="df-container" data-id="<?= $myCmsId ?>">
						<div class="df-prev df-navigation">
							<i class="fa fa-chevron-left"></i>
						</div>
						<div class="df-dates"></div>
						<div class="df-next df-navigation">
							<i class="fa fa-chevron-right"></i>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="program-content empty" id="event-content<?= $myCmsId ?>"></div>
					</div>
				</div>
				<!-- vue calendrier des events -->
				<div id="event_calendar_container<?= $myCmsId ?>" class="coevent-program-viewer">
					<div id="event_calendar<?= $myCmsId ?>" class="co-calendar"></div>
				</div>
				<!-- vue calendrier des events -->
				<div id="chronology_container<?= $myCmsId ?>" class="coevent-program-viewer">
					<div class="btn-group">
						<button type="button" class="btn btn-default event-filter" data-state="false"><?= Yii::t('common', 'Show past events') ?></button>
						<button type="button" class="btn btn-default event-filter" data-state="true"><?= Yii::t('common', 'Hide past events') ?></button>
					</div>
					<div id='timeline-embed<?= $myCmsId ?>' style="width: 100%; height: 55vh; margin-top: 50px;"></div>
				</div>
				<br>
				<div class="text-center">
					<button class="coevent-button<?= $myCmsId ?> btn-participate" type="button" id="event-proposition<?= $myCmsId ?>">
						<?= Yii::t('cms', 'Submit an event') ?> <i class="fa fa-calendar"></i>
					</button>
					<button class="coevent-button<?= $myCmsId ?> btn-participate" type="button" id="event-download<?= $myCmsId ?>">
						<?= Yii::t('cms', 'Download program') ?> <i class="fa fa-download"></i>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-boostrap-create-event" id="openModalCreateEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 999999;overflow-y: auto;">
	<div class="modal-dialog modal-personnalise modal-dialog-centered co-resposnive" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?= Yii::t('cms', 'Create an event') ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body col-xs-12">
				<div class="row">
					<div class="event-container  col-xs-12 col-md-4 col-lg-4">
						<div class="event-image">
							<div id="createvent-image-input"></div>
						</div>
					</div>
					<div class="col-xs-12 col-md-8 col-lg-8 event-details"></div>
				</div>
			</div>
			<div class="modal-footer col-xs-12">
				<button class="create-event-button" data-id="<?= $myCmsId ?>" ><?= Yii::t('cms', 'Save') ?></button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	var nearestDate = "";
	var coevent_parent;
	var parent_start, parent_end;
	var db_event = [];
	var actors = [];
	var dt_participate = [];
	var isFollow = {};
	var expectedAttendeesOne;
	var labelBtnUnfolow;
	var actionConnectCLick;

	str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#coevent_program<?= $kunik ?>").append(str);

	if(costum.editMode){
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        var coeventProgram = {
            configTabs: {
                general : {
                    inputsConfig : [
						{
                            type: "colorPicker",
                            options: {
                                name : "menuColor",
                                label: tradCms.optionsColor,
                            }
						},
                        {
                            type: "colorPicker",
                            options: {
                                name : "primaryColor",
                                label: tradCms.buttonsColor,
                            }
						}
                    ]
                },
				style : {
					inputsConfig : [
						"color",
						"backgroundColor",
						"addCommonConfig",
						{
                            type: "section",
                            options: {
								name: "program-view",
								label: tradCms.filterBtn,
								showInDefault: false,
								inputs: [
									"color",
									"backgroundColor",
                                    "border",
                                    "boxShadow",
                                    "margin",
                                    "padding",
                                    "borderRadius"
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
                                name: "btn-participate",
								label: tradCms.btnBottom ,
								showInDefault: false,
								inputs: [
									"color",
									"backgroundColor",
                                    "border",
                                    "boxShadow",
                                    "margin",
                                    "padding",
                                    "borderRadius"
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
                                name: "program-content",
								label: tradCms.line,
								showInDefault: false,
								inputs: [
                                    "borderLeft",
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
                                name: "point-holder",
								label: tradCms.pointOfInterest,
								showInDefault: false,
								inputs: [
                                    "backgroundColor",
									"width",
									"height",
									"border-radius",
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
								name: "tl-timeline",
								label: tradCms.timeline,
								showInDefault: false,
								inputs: [
									{
										type: "section",
										options: {
											name: "tl-slider-container-mask",
											label: tradCms.content,
											showInDefault: false,
											inputs: [
												"backgroundColor"
											]
										}										
									},
									{
										type: "section",
										options: {
											name: "tl-timenav",
											label: tradCms.headline,
											showInDefault: false,
											inputs: [
												"backgroundColor"
											]
										}										
									},
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
                                name: "event-card",
								label: "Card" ,
								showInDefault: false,
								inputs: [
									"color",
									"backgroundColor",
                                    "border",
                                    "boxShadow",
                                    "margin",
                                    "padding",
                                    "borderRadius"
                                ]
                            }
                        },

                    ]
				},
				hover : {
					inputsConfig : [
						"color",
						"backgroundColor",
						"addCommonConfig",
						{
                            type: "section",
                            options: {
								name: "program-view",
								label: tradCms.filterBtn,
								showInDefault: false,
								inputs: [
									"color",
									"backgroundColor",
                                    "border",
                                    "boxShadow",
                                    "margin",
                                    "padding",
                                    "borderRadius"
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
                                name: "btn-participate",
								label: tradCms.btnBottom ,
								showInDefault: false,
								inputs: [
									"color",
									"backgroundColor",
                                    "border",
                                    "boxShadow",
                                    "margin",
                                    "padding",
                                    "borderRadius"
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
                                name: "program-content",
								label: tradCms.line,
								showInDefault: false,
								inputs: [
                                    "borderLeft",
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
                                name: "point-holder",
								label: tradCms.pointOfInterest,
								showInDefault: false,
								inputs: [
                                    "backgroundColor",
									"width",
									"height",
									"border-radius",
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
								name: "tl-timeline",
								label: tradCms.timeline,
								showInDefault: false,
								inputs: [
									{
										type: "section",
										options: {
											name: "tl-slider-container-mask",
											label: tradCms.content,
											showInDefault: false,
											inputs: [
												"backgroundColor"
											]
										}										
									},
									{
										type: "section",
										options: {
											name: "tl-timenav",
											label: tradCms.headline,
											showInDefault: false,
											inputs: [
												"backgroundColor"
											]
										}										
									},
                                ]
                            }
                        },
						{
                            type: "section",
                            options: {
                                name: "event-card",
								label: "Card" ,
								showInDefault: false,
								inputs: [
									"color",
									"backgroundColor",
                                    "border",
                                    "boxShadow",
                                    "margin",
                                    "padding",
                                    "borderRadius"
                                ]
                            }
                        },

                    ]
				}
            },
            beforeLoad: function(){
    
            },
            onChange: function(path, valueToSet, name, payload, value){
                
            },
            afterSave: function(path, valueToSet, name, payload, value) {
				if ( name === "menuColor" || name === "primaryColor"){
					cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
				}
            }
        }
        cmsConstructor.blocks['<?= $kunik ?>'] = coeventProgram;
    }

	if (typeof costum === "object") {
		var match = location.hash.match(/\#[\w\-]+/);
		if (costum.contextType && costum.contextType === "events")
			coevent_parent = {
				id: costum.contextId,
				type: "events"
			};
		else if (match && costum.app && costum.app[match[0]] && costum.app[match[0]].event)
			coevent_parent = {
				id: costum.app[match[0]].event,
				type: "events"
			};
		else
			coevent_parent = {
				id: costum.contextId,
				type: costum.contextType
			};
	}

	var blockKey = '<?= $myCmsId ?>';

	function template_date_filter(args) {
		var container_dom = $('<div>');
		container_dom.addClass('df-date')
			.attr('data-target', args.date)
			.append(
				$('<div>').addClass('df-month')
				.html(args.month),
				$('<div>').addClass('df-order')
				.html(args.order),
				$('<div>').addClass('df-day')
				.html(args.day)
			);
		return (container_dom);
	}

	function request_join_event(args) {
		var url = baseUrl + '/costum/coevent/join_event';
		return new Promise(function(resolve, reject) {
			ajaxPost(null, url, args, resolve, reject);
		});
	}

	if (typeof window.event_participation !== 'function') {
		window.event_participation = function(dom) {
			var that = dom;
			thisElement = $(that);
			var isExpAttFull = (thisElement.data("expected-attendees") > 0 && Object.keys(thisElement.data("attendees")).length == eventData.expectedAttendees);
			if (!isExpAttFull) {
				if (userId) {
					subscribeToEvent(thisElement);
				} else {
					$("#modalLogin").on('show.bs.modal', function(e) {
						if ($("#infoBL").length == 0) {
							$("#modalLogin .modal-content .container div.row").append(`<div class="col-xs-12" id="infoBL">
                                Vous devez être connecté(e) pour vous inscrire à l’événement. Vous n’avez pas encore de compte ? 
                                <a href="javascript:;" class="bold" data-toggle="modal" data-target="#modalRegister"> Créez-en un </a>
                            </div>`);
						}
					});
					$("#modalLogin").on('hide.bs.modal', function(e) {
						$("#infoBL").remove();
					});
					toastr.info("Vous devez être connecté(e) pour vous inscrire à l’événement");
					Login.openLogin();
				}
			} else {
				if (userId)
					subscribeToEvent(thisElement);
			}
		}
	}

	if(typeof window.event_toggle !== 'function'){
		window.event_toggle = function(event){
			var content = $(event).parents(".event-card").find('.co-content-slide');
			var icon = $(event).parents(".event-card").find('.fa-arrow-right');
			icon.toggleClass('co-transform-deg')

			if (content.hasClass('slide-down')) {
				content.removeClass('slide-down').addClass('slide-up');
			} else {
				content.removeClass('slide-up').addClass('slide-down');
			}
		}
	}

	if(typeof window.open_preview_event !== 'function'){
		window.open_preview_event = function(event){
			var myId = event.getAttribute('data-drawer');
			ajaxPost(
                null,
                baseUrl + "/co2/event/requestattendees",
                {
                    id: db_event[myId].id,
                },
                function(data) {
                    if(data.result){
                        db_event[myId]["links"]["attendees"] = data.attendees ?? [];
                        urlCtrl.openPreview("/view/url/costum.views.custom.coevent.preview", 
                            {
                                "params" : 
                                    {   
                                        "expectedAttendeesOne": expectedAttendeesOne,
                                        "parentId" : "<?= $myCmsId ?>" ,
                                        "event": db_event[myId],
                                        "actors": actors[myId],
                                        "isFollow": typeof userId !== "undefined" && typeof db_event[myId]?.links?.attendees?.[userId] !== "undefined" ? true : false,
                                        "parentStart" : parent_start.format('YYYY-MM-DD HH:mm'), 
										"parentEnd" : parent_end.format('YYYY-MM-DD HH:mm')
                                    }
                            }
                        );
                    } else {
                        toastr.error(tradCms.somethingWrong);
                    }
                },
                null,
                "json",
                {
                    async: false
                }
            )
		}
	}

	if(typeof window.open_preview_organizer !== 'function'){
		window.open_preview_organizer = function(event){
			var idOrganizer = $(event).data('id');
			var typeOrganizer = $(event).data('type');
			urlCtrl.openPreview("/view/url/costum.views.custom.coevent.previeworga", 
									{
										"params" : 
										{
											"id" : idOrganizer, 
											"type": typeOrganizer
										}
									}
								);
		}
	}

	if (typeof window.event_delete !== 'function') {
		window.event_delete = function(dom) {
			var that = dom;
        	var self = $(that);
			$.confirm({
				title : trad.delete,
				content : 'Procéder à la suppression de l\'événement',
				buttons : {
					confirm : {
						text : 'Confirmer',
						action() {
							var url = baseUrl + '/' + moduleId + '/element/delete/id/' + self.data('event') + '/type/events';
							var request_params = {reasons : 'none'};
							ajaxPost(null, url, request_params, function (__data) {
								if (__data.result) {
									toastr.success(__data.msg);
									refreshBlock(idBlock, ".cmsbuilder-block[data-id='" + idBlock + "']")
								} else {
									toastr.error(__data.msg);
								}
							});
						}
					},
					cancel : {
						text : 'Annuler'
					}
				}
			})
		}
	}


	function updateFilterEventPeriodChronology() {
		var state = sessionStorage.getItem(dataHelper.printf('{{block}}-showAllEvents', {
			block: blockKey
		}));
		state = state ? state : 'false';
		$(dataHelper.printf('#chronology_container{{blockKey}} .event-filter', {
			blockKey: blockKey
		})).each(function() {
			var self = $(this);
			if (self.data('state').toString() === state.toString()) {
				self.css('display', 'none');
			} else {
				self.css('display', '');
			}
		});
	}

	function subscribeToEvent(eventSource, theUserId = null, on_success) {
		var labelLink = "";
		var parentId = eventSource.data("id");
		var parentType = eventSource.data("type");
		var childId = (theUserId) ? theUserId : userId;
		var childType = "citoyens";
		var name = eventSource.data("name");
		var id = eventSource.data("id");
		//traduction du type pour le floopDrawer
		eventSource.html("<i class='fa fa-spin fa-circle-o-notch text-azure'></i>");
		var connectType = (parentType == "events") ? "connect" : "follow";

		if (eventSource.data("ownerlink") == "follow") {
			callback = function() {
				labelLink = (parentType == "events") ? "DÉJÀ INSCRIT(E)" : trad.alreadyFollow;
				if (eventSource.hasClass("btn-add-to-directory")) {
					labelLink = "";
				}
				$(`[data-id=${id}]`).each(function() {
					$(this).html("<small><i class='fa fa-unlink'></i> " + labelLink.toUpperCase() + "</small>");
					$(this).data("ownerlink", "unfollow");
					$(this).data("original-title", labelLink);
					var strCounter = $(this).siblings(".rolesLabel.attendeesCounter").find("span").html();
					intCounter = parseInt(strCounter);
					newCounter = intCounter + 1;
					$(this).siblings(".rolesLabel.attendeesCounter").find("span").html(newCounter.toString());
				});
				if (typeof on_success === 'function')
					on_success();
			}
			request_join_event({
				user: childId,
				event: id
			}).then();
			// return ;
			if (parentType == "events")
				links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
			else
				links.follow(parentType, parentId, childId, childType, callback);
		} else {
			//eventSource.data("ownerlink")=="unfollow"
			connectType = (parentType == "events") ? "attendees" : "followers";
			callback = function() {
				labelLink = (parentType == "events") ? "S'inscrire" : "Déjà inscrite";
				if (eventSource.hasClass("btn-add-to-directory")) {
					labelLink = "";
				}
				$(`[data-id=${id}]`).each(function() {
					$(this).html("<small><i class='fa fa-chain'></i> " + labelLink.toUpperCase() + "</small>");
					$(this).data("ownerlink", "follow");
					$(this).data("original-title", labelLink);
					$(this).removeClass("text-white");
					var strCounter = $(this).siblings(".rolesLabel.attendeesCounter").find("span").html();
					intCounter = parseInt(strCounter);
					newCounter = intCounter - 1;
					$(this).siblings(".rolesLabel.attendeesCounter").find("span").html(newCounter.toString());
				});
				if (typeof on_success === 'function'){
					on_success();}
			};
			links.disconnectAjax(parentType, parentId, childId, childType, connectType, null, callback);
		}
	}

	function get_session_data() {
		var session_data = sessionStorage.getItem('coevent-' + coevent_parent.id + coevent_parent.type);
		var filter = session_data ? JSON.parse(session_data) : {
			tags: [],
			regions: [],
			type: null
		};
		return (filter);
	}

	function set_session_data(data) {
		sessionStorage.setItem('coevent-' + coevent_parent.id + coevent_parent.type, JSON.stringify(data));
	}

	function html_active_filter(args) {
		var container = $('<div>')
			.addClass('filters-activate')
			.data({
				key: args.key,
				value: args.value,
				type: args.type
			});
		container.append($('<i>').addClass('fa fa-times-circle'));
		container.append($('<span>').addClass('activeFilters').text(args.value));
		container.on('click', function() {
			var self = $(this);
			var filter = get_session_data();
			switch (self.data('type')) {
				case 'type':
					filter.type = null;
					break;
				case 'tag':
					var index_of_value = filter.tags.indexOf(self.data('key'));
					filter.tags.splice(index_of_value, 1);
					break;
				case 'region':
					var index_of_value = filter.regions.findIndex(function(region) {
						return (region.id === self.data('key'));
					});
					filter.regions.splice(index_of_value, 1);
					break;
			}
			set_session_data(filter);
			// format new url
			filter.regions = filter.regions.map(region => region.id);
			var hash = location.hash.match(/\#[\w\-]+/)[0];
			hash += '?'
			var key;
			for (key in filter) {
				if (filter[key] === null)
					continue;
				if (typeof filter[key] === 'string')
					hash += key + '=' + filter[key] + '&';
				else
					$.each(filter[key], function(i, param) {
						hash += key + '[]=' + param + '&';
					});
			}
			hash = hash.replace(/.$/, '');
			hash = location.origin + location.pathname + hash;
			history.pushState({}, '', hash);
			$('.coevent-program').trigger('coevent-filter');
		});
		return (container);
	}

	function load_filter_view() {
		var main_nav_dom = $('#mainNav');
		var filter_container_dom = $('.active_filter_container:first');
		var active_filter_dom = filter_container_dom.find('.active_filter');
		$('.event-types .event-type').removeClass('active filter-active');
		filter_container_dom.css({
			top: main_nav_dom.outerHeight() + 'px'
		});
		var filter = get_session_data();
		active_filter_dom.empty();
		if (filter.type) {
			active_filter_dom.append(html_active_filter({
				key: filter.type,
				value: coTranslate(filter.type),
				type: 'type'
			}));
			$('.event-types .event-type[data-target="' + filter.type + '"]').addClass('active filter-active');
		}
		$.each(filter.tags, function(i, tag) {
			active_filter_dom.append(html_active_filter({
				key: tag,
				value: tag,
				type: 'tag'
			}));
		});
		$.each(filter.regions, function(i, region) {
			active_filter_dom.append(html_active_filter({
				key: region.id,
				value: region.name,
				type: 'region'
			}));
		});
		if (!filter.type && !filter.tags.length && !filter.regions.length)
			filter_container_dom.hide();
		else
			filter_container_dom.show();
	}

	(function($, W) {
		var _journee_carousel = null;
		var loadTimeout;
		var php = {
			blockKey: '<?= $myCmsId ?>'
		};

		function reload_program_event() {
			$('#program-container<?= $myCmsId ?>').off('click').on('click', '#journee-carousel<?= $myCmsId ?> .item', function(__e) {
				__e.preventDefault();
				load_programs($(this).data('target'));
			});
		}

		function get_event_context() {
			return new Promise(function(resolve) {
				ajaxPost(null, baseUrl + '/costum/coevent/get_events/request/event/id/' + coevent_parent.id + '/type/' + coevent_parent.type, null, function(event) {
					resolve(event);
					parent_start = moment(event.start_date);
					parent_end = moment(event.end_date);
				}, null, 'json')
			});
		}

		function request_get_subevents(options) {
			var defaultOptions = {
				fromToday: false,
				timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
			};
			var filter = get_session_data();
			filter = $.extend({}, filter, defaultOptions, options);
			var url = baseUrl + '/costum/coevent/get_events/request/subevents/id/' + coevent_parent.id + '/type/' + coevent_parent.type;
			return new Promise(function(resolve, reject) {
				ajaxPost(null, url, filter, resolve, resolve, 'json');
			});
		}

		function request_get_subevent_dates(option) {
			var url = baseUrl + '/costum/coevent/get_events/request/dates/id/' + coevent_parent.id + '/type/' + coevent_parent.type;
			var filter = get_session_data();
			filter = $.extend({}, filter, option);
			return new Promise(function(resolve, reject) {
				ajaxPost(null, url, filter, resolve, reject, 'json')
			});
		}

		function load_timeline(__option) {
			var go;
			if (typeof __option.goto !== 'undefined') {
				go = __option.goto;
				delete __option.goto;
			}
			return new Promise(function(resolve, reject) {
				var timeline_knightlab = null;
				var container_dom = $('#timeline-embed<?= $myCmsId ?>');
				coInterface.showLoader(container_dom, trad.currentlyloading);
				var tl_interval = setInterval(function() {
					if (typeof TL !== 'undefined') {
						clearInterval(tl_interval);
						var showAllEvents = sessionStorage.getItem(dataHelper.printf('{{block}}-showAllEvents', {
							block: blockKey
						}));
						var args = {
							fromToday: showAllEvents !== null ? JSON.parse(showAllEvents) : false
						};
						request_get_subevents(args)
							.then(function(db_subevent) {
								var start_at_slide = -1;
								if (db_subevent.length === 0) {
									$('#timeline-embed<?= $myCmsId ?>').empty().html('<p class="no-events">' + coTranslate('No events to show') + '</p>')
								} else {
									var todayPhp = moment('<?= $todayPhp ?>');
									var filterNearestDate = db_subevent.reduce(function(date, reduce) {
										var itsDate = moment(reduce.start_date);
										if (todayPhp.isSameOrBefore(itsDate)) {
											if (date === null)
												return (itsDate);
											if (itsDate.isBefore(date))
												return (itsDate);
										}
										return (date);
									}, null);
									var data = {
										events: db_subevent.map(function(__event, i) {
											var moment_start = moment(__event['start_date']);
											var moment_end = moment(__event['end_date']);
											if (typeof go !== 'undefined' && moment_start.isSame(go, 'day') && start_at_slide < 0) {
												start_at_slide = i;
											} else if (filterNearestDate !== null) {
												if (moment_start.isSame(filterNearestDate)) {
													start_at_slide = i;
												}
											}
											var map = {
												start_date: {
													year: moment_start.year(),
													month: moment_start.month() + 1,
													day: moment_start.date(),
													hour: moment_start.hour(),
													minute: moment_start.minute()
												},
												end_date: {
													year: moment_end.year(),
													month: moment_end.month() + 1,
													day: moment_end.date(),
													hour: moment_end.hour(),
													minute: moment_end.minute()
												},
												text: {
													headline: __event['name'],
													text: `<blockquote>${__event['short_description'].replace(/\n/g, "<br>")}</blockquote> ${dataHelper.markdownToHtml(__event['full_description'])}`
												},
												media: {
													url: __event['profile'],
													thumbnail: __event['profile']
												},
												background: {}
											};
											if (__event['banner'].length) {
												map['background']['url'] = __event['banner'];
											} else {
											}

											return map;
										})
									};
									var options = {
										script_path: `${baseUrl}/plugins/knightlab_timeline3/js/`,
										language: 'fr',
										font: 'bevan-pontanosans',
										theme: 'contrast',
										scale_factor: 0.5,
										timenav_position: 'top'
									}

									if (start_at_slide >= 0)
										options.start_at_slide = start_at_slide;
									container_dom.empty().html('');
									timeline_knightlab = new TL.Timeline('timeline-embed<?= $myCmsId ?>', data, options);
								}
								resolve();
							})
							.catch(reject);
					}
				});
			});
		}

		function load_fullcalendar(__option = {}) {
			var go;
			if (typeof __option.goto !== 'undefined') {
				go = __option.goto;
				delete __option.goto;
			}
			var calendar_dom = $('#event_calendar<?= $myCmsId ?>');
			coInterface.showLoader(calendar_dom, trad.currentlyloading);
			return new Promise(function(resolve) {
				get_event_context().then(function(__parent_event) {
					var start_moment_parent = moment(__parent_event['start_date']);
					var end_moment_parent = moment(__parent_event['end_date']);
					var default_view = '';
					var selectedView = sessionStorage.getItem(dataHelper.printf('{{block}}-calendarview', {
						block: '<?= $myCmsId ?>'
					}));
					switch (true) {
						case typeof selectedView === 'string':
							default_view = selectedView;
							break;
						case start_moment_parent.format('YYYY-MM-DD') == end_moment_parent.format('YYYY-MM-DD'):
							default_view = 'agendaDay';
							break;
						case end_moment_parent.diff(start_moment_parent, 'days') <= 6 && start_moment_parent.day() == 1:
							default_view = 'agendaWeek';
							break;
						default:
							default_view = 'month';
							break
					}
					var request_arg = $.extend({}, __option);
					request_get_subevents(request_arg).then(function(__events) {
						var gotoCalendarDate;
						var todayPhp = moment('<?= $todayPhp ?>');
						var events = __events.map(function(__event) {
							var eventDate = moment(__event['start_date'])
							if (todayPhp.isAfter(eventDate, 'day'))
								gotoCalendarDate = eventDate.clone();
							return {
								id: __event['id'],
								title: __event['name'],
								start: eventDate.format('YYYY-MM-DD HH:mm'),
								end: moment(__event['end_date']).format('YYYY-MM-DD HH:mm'),
								className: ['lbh-preview-element'],
								url: `#page.type.events.id.${__event['id']}`,
							}
						});
						calendar_dom.empty().html('');
						calendar_dom.fullCalendar('destroy');
						calendar_dom.fullCalendar({
							lang: 'fr',
							timeFormat: 'H(:mm)',
							defaultView: default_view,
							selectable: true,
							header: {
								left: 'prev,next today',
								center: 'title',
								right: 'month,agendaWeek,agendaDay'
							},
							height: 600,
							events: events,
							viewRender(view) {
								sessionStorage.setItem(dataHelper.printf('{{block}}-calendarview', {
									block: '<?= $myCmsId ?>'
								}), view.name);
							},
							eventMouseover: function() {
								coInterface.bindLBHLinks();
							},
							select(start, end) {
								coevent_create_event({
									start: start,
									end: end,
									afterLoad: "afterLoad"
								});
							}
						});
						if (typeof go !== 'undefined') {
							calendar_dom.fullCalendar('gotoDate', go);
						} else if (gotoCalendarDate) {
							if (gotoCalendarDate.isBefore(start_moment_parent, 'day'))
								calendar_dom.fullCalendar('gotoDate', start_moment_parent.format('YYYY-MM-DD HH:mm'));
							else
								calendar_dom.fullCalendar('gotoDate', gotoCalendarDate.format('YYYY-MM-DD HH:mm'));
						}
						resolve();
					});
				});
			});
		}

		function reset__journee_carousel() {
			return carousel({
				container: $('#journee-carousel<?= $myCmsId ?>'),
				autoplay: false,
				navigation: {
					indicator: false,
					button: {
						show: false,
						next: '<span class="fa fa-chevron-right"></span>',
						prev: '<span class="fa fa-chevron-left"></span>'
					}
				},
				delay: 2000
			});
		}

		function coevent_create_event(args) {
			var default_args = {
				start: null,
				end: null,
				afterLoad: null
			};

			var preferences = {
				isOpenData : true,
				isOpenEditional : true
			}

			var newEvent = {};
			

			args = $.extend({}, default_args, args);
			get_event_context().then(function(__event) {
				var moment_start, moment_end;
				parent_start = moment(__event.start_date);
				parent_end = moment(__event.end_date);
				if (typeof args.start === 'undefined' || args.start === null)
					moment_start = moment();
				else
					moment_start = args.start;
				if (typeof args.end === 'undefined' || args.end === null)
					moment_end = moment_start.clone().add(1, 'hours');
				else if (args.end.format('HH:mm') === '00:00')
					moment_end = args.end.subtract(1, 'minutes');
				else
					moment_end = args.end;
				// create parent limit
				var parent_start_date_dom = $("#startDateParent"),
					parent_end_date_dom = $("#endDateParent");
				if (!parent_start_date_dom.length) {
					parent_start_date_dom = $('<input>')
						.attr('id', 'startDateParent')
						.val(parent_start.format('YYYY-MM-DD HH:mm'))
						.prop('hidden', true);
					$(document.body).append(parent_start_date_dom);
				}
				if (!parent_end_date_dom.length) {
					parent_end_date_dom = $('<input>')
						.attr('id', 'endDateParent')
						.val(parent_end.format('YYYY-MM-DD HH:mm'))
						.prop('hidden', true);
					$(document.body).append(parent_end_date_dom);
				}
				parent_start_date_dom.val(parent_start.format('YYYY-MM-DD HH:mm'));
				parent_end_date_dom.val(parent_end.format('YYYY-MM-DD HH:mm'));
				var eventDynformDefaultParams = {
					onload: {
						actions: {
							hide: {
								parentfinder: 1,
								publiccheckboxSimple: 1,
								recurrencycheckbox: 1,
								organizerNametext: 1
							}
						}
					},
					beforeBuild: {
						properties: {
							type: {
								options: eventTypes
							}
						}
					},
					afterBuild: function() {
						$(".finder-organizer").find("a").html("Rechercher et sélectionner ma structure");
					},
					afterSave: function(data) {
						var callbackParams = {
							parent: {},
							links: {
								attendees: {}
							}
						};
						callbackParams.parent[coevent_parent.id] = {
							type: coevent_parent.type
						};
						callbackParams.links.attendees[userId] = {
							type: 'citoyens'
						};

						var params = {
							id: data['id'],
							collection: data['map']['collection'],
							path: 'allToRoot',
							value: callbackParams
						};
						link_parent = {
							id: coevent_parent.id,
							collection: coevent_parent.type,
							path: 'links.subEvents.' + data.id,
							value: {
								type: "events"
							}
						};
						dataHelper.path2Value(link_parent, function() {});
						links.connectAjax(coevent_parent.type, coevent_parent.id, data.id, "events", 'subEvents', null, null);
						dyFObj.commonAfterSave(null, function() {
							dataHelper.path2Value(params, function() {
								$('.coevent-program').trigger('coevent-filter');
								$('.theme-selector').each(function() {
									var self = $(this);
									self.trigger('block-update');
								});
							});
						});
					}
				};
				var eventDynformParams = typeof window.defaultCoevent === 'object' ? window.defaultCoevent : eventDynformDefaultParams;
				eventDynformParams = $.extend({}, eventDynformDefaultParams, eventDynformParams);
				var presentValuesEvent = {
					startDate: moment_start.format('DD/MM/YYYY HH:mm'),
					endDate: moment_end.format('DD/MM/YYYY HH:mm')
				};


				if ($('#event-content<?= $myCmsId ?> .event-hour').length) {
					// var selected_date = moment($('#event-content<?= $myCmsId ?> .dinalternate.date').text(), 'DD.MM.YYYY');
					// selected_date.hour(parseInt($('.event-hour:last').data('last-hour')));
					var last_schedule = $('#event-content<?= $myCmsId ?> .event-hour:last').data('last-schedule');
					// presentValuesEvent.startDate = selected_date.format('DD/MM/YYYY HH:mm');
					presentValuesEvent.startDate = last_schedule;
					presentValuesEvent.endDate = moment(last_schedule, 'DD/MM/YYYY HH:mm').add(1, 'hours').format('DD/MM/YYYY HH:mm');
				}
				presentValuesEvent.parent = {};
				presentValuesEvent.parent[__event.id] = {
					_id: {
						$id: __event.id
					},
					name: __event.name,
					type: 'events',
					profilThumbImageUrl: co2AssetPath + '/images/thumb/default_events.png'
				};
				// dyFObj.openForm('event', args.afterLoad, presentValuesEvent, null, eventDynformParams);
				dyFObj.setMongoId('events', function() {
					const newEventToAdd = {
						id : dyFObj.currentElement.id,
						public : true,
						scope:null,
						recurrency: false,
						costumSlug: costum.slug,
						costumEditMode: costum.editMode,
						preferences: preferences,
						key: "event",
						collection: "events",
						timeZone : moment.tz.guess()
					};

					Object.assign(newEvent, newEventToAdd);

					new CoInput({
						container: "#createvent-image-input",
						inputs: [
							{
								type: "inputFileImage",
								options: {
									name: "image",
									label: "Image",
									collection: "documents",
									defaultValue: "",
									canSelectMultipleImage : false,
									payload: {
										path: "logo",
									},
									icon: "chevron-down",
									class: "logoUploader inputContainerCoEvent",
									contentKey: "profil",
									uploadOnly: true,
									uploaderInitialized : true,
									endPoint: "",
									domElement: "profil",
									// defaultValue: defaultData["profile"] ?? ""
								},
							},
							{
								type : "dateTimePicker",
								options: {
									name : "startDate",
									label: tradCms.startDate +" *",
									defaultValue : "",
									icon : "chevron-down",
									class: "inputContainerCoEvent newStartDate",
								}
							},
							{
								type : "dateTimePicker",
								options: {
									name : "endDate",
									label: tradCms.endDate +" *",
									defaultValue : "",
									icon : "chevron-down",
									class: "inputContainerCoEvent newEndDate",
								}
							},
							{
								type : "tags",
								options : {
									name : "tags",
									label : tradDynForm.keyWords,
									options : [""],
									defaultValue : "",
									class: "inputContainerCoEvent",
									icon : "chevron-down"
								}
							},
							{
								type : "inputSimple",
								options: {
									name : "email",
									label : "E-mail",
									defaultValue : "",
									icon : "chevron-down",
									class: "inputContainerCoEvent",
								}
							},
							{
								type : "inputSimple",
								options: {
									name : "url",
									label : "URL",
									defaultValue : "",
									icon : "chevron-down",
									class: "inputContainerCoEvent",
								}
							},
							
						],
						onchange: function(name, value, payload) {
							if ( name === "image") {
								jsonHelper.setValueByPath(newEvent, "profilImageUrl", value);
							} else if ( name === "startDate"){
								var value = moment(value, "DD/MM/YYYY HH:mm");
								var dateDebutParent = moment(__event.start_date);
								var dateDebutConvertie = value.tz(newEvent.timeZone).format("YYYY-MM-DDTHH:mm:ssZ");
								if (moment(dateDebutConvertie).isSameOrBefore(dateDebutParent)){
									$(".warningStartDate").remove();
									$(".newStartDate").append(`<span class="warningStartDate" ><i class='fa fa-warning'></i> ${tradCms.mustBeAfter+" "+dateDebutParent.format("DD/MM/YYYY HH:mm")}.</span>`);
								} else {
									$(".warningStartDate").remove();
									jsonHelper.setValueByPath(newEvent, name,dateDebutConvertie);
								}
							} else if ( name === "endDate"){
								var value = moment(value, "DD/MM/YYYY HH:mm");
								var dateFinParent = moment(__event.end_date);
								var dateEndConvertie = value.tz(newEvent.timeZone).format("YYYY-MM-DDTHH:mm:ssZ");
								if ( moment(dateEndConvertie).isSameOrBefore(newEvent.startDate) || moment(dateEndConvertie).isAfter(dateFinParent)){
									$(".warningEndDate").remove();
									$(".newEndDate").append(`<span class="warningEndDate"><i class='fa fa-warning'></i> ${ tradCms.mustBeBetween+" "+moment(newEvent.startDate).format("DD/MM/YYYY HH:mm")+" "+tradCms.and+" "+ dateFinParent.format("DD/MM/YYYY HH:mm")}.</span>`);
								} else {
									$(".warningEndDate").remove();
									jsonHelper.setValueByPath(newEvent, name , dateEndConvertie );
								}
							} else if ( name === "tags"){
								jsonHelper.setValueByPath(newEvent, name,value.split(','));
							} else {
								jsonHelper.setValueByPath(newEvent, name,value);
							}
						},
						onblur : function(name , value, payload){
						
						}
					});

					new CoInput({
							container: ".event-details",
							inputs: [
								{
									type : "inputSimple",
									options: {
										name : "name",
										label : tradCms.eventName +" *",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "select",
									options: {
										name : "type",
										label : tradDynForm.eventTypes +" *",
										options :  Object.keys(eventTypes).map(function(value) {
											return {
												label : typeof tradCategory[eventTypes[value]] == "String" ? tradCategory[eventTypes[value]] : eventTypes[value] ,
												value : value
											}
										}),
										icon : "chevron-down",
										class: "inputContainerCoEvent"
									}
								},
								{
									type : "finders",
									options: {
										name : "organizer",
										label: tradDynForm.whoorganizedevent,
										initType: ["projects", "organizations"],
										multiple: true,
										initMe: false,
										icon : "chevron-down",
										openSearch: true,
										initBySearch: true,
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "formLocality",
									options: {
										name : "formLocality",
										label: tradCms.place,
										icon : "chevron-down",
										class: "inputContainerCoEvent",

									}
								},
								{
									type : "textarea",
									options: {
										name : "shortDescription",
										label : tradCms.shortDescription,
										defaultValue :"",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									},
								},
							],
							onchange: function(name, value, payload) {
								if ( name === "name" ){
									jsonHelper.setValueByPath(newEvent, name, value);
								} 
							},
							onblur: function(name, value, payload) {
								if ( name === "formLocality"){
										var address = [];
										var geo = [];
										var geoPosition = [];
										var addresses = [];
										value.forEach((location, index) => {
											if (location.center) {
												address = location.address;
												geo = location.geo;
												geoPosition = location.geoPosition;
											} else {
												addresses.push(location);
											}
										});
										jsonHelper.setValueByPath(newEvent, "address", address);
										jsonHelper.setValueByPath(newEvent, "geo", geo);
										jsonHelper.setValueByPath(newEvent, "geoPosition", geoPosition);
										jsonHelper.setValueByPath(newEvent, "addresses", addresses);
								} else {
									jsonHelper.setValueByPath(newEvent, name, value);
								} 
							}
					});
					$("#openModalCreateEvent").modal("show");
           		});

				
				return false;
			});

			$(".create-event-button").off("click").on("click", function(){
				var idBlock =  $(this).data("id");
				delete newEvent.images
				jsonHelper.setValueByPath(newEvent, "parent", { [coevent_parent.id]: { "type": coevent_parent.type } });

				ajaxPost(
					null,
					baseUrl+"/co2/element/save",
					newEvent, 
					function (response) {  
						if (response.result){
							var events = {
								id: costum.contextId,
								collection: "events",
								path: "links.subEvents."+newEvent.id,
								value : {
									type : "events"
								},
								costumSlug: costum.slug,
								costumEditMode: costum.editMode                                                           
                            }
							dataHelper.path2Value(events, function (params) {
								if (params.result){
									var attende = {
										id : newEvent.id,
										collection : "events",
										path : "links.attendees."+userId,
										value : {
											type : "citoyens"
										},
										costumSlug: costum.slug,
										costumEditMode: costum.editMode   
									}
									dataHelper.path2Value(attende, function (params) {
										$("#openModalCreateEvent").modal("hide");
										toastr.success(tradCms.editionSucces);
										refreshBlock(idBlock, ".cmsbuilder-block[data-id='" + idBlock + "']")
									});
								}
							});
						} else {
							toastr.error(response.msg);
						}                 
					}
				);
			});
		}

		if (typeof window.ce_prog_download !== 'function')
			window.ce_prog_download = function(filename) {
				var nearest_moment = moment(nearestDate, "DD.MM.YYYY");
				var url = baseUrl + '/costum/coevent/getpdfaction?event=' + coevent_parent.id;
				url += '&costum_slug=' + costum.slug;
				url += '&costum_page=' + location.hash.match(/\#[\w\-]+/)[0].substr(1);
				if (nearest_moment.isValid())
					url += '&startDate=' + nearest_moment.toISOString();
				var post = {
					url: location.protocol + '//' + location.host + location.pathname + location.hash
				};
				post = {
					method: 'post',
					header: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify(post)
				};
				fetch(url, post).then(function(resp) {
					resp.blob().then(function(blob) {
						var url = URL.createObjectURL(blob);
						var a_dom = document.createElement('a');
						a_dom.href = url;
						a_dom.download = filename;
						a_dom.addEventListener('click', function() {
							setTimeout(function() {
								a_dom.remove();
								URL.revokeObjectURL(url);
							}, 200);
						});
						$(document.body).append(a_dom);
						a_dom.click();
					});
				});
				return false;
			}

		function get_actors(__types = ['animator', 'organizer', 'links.attendees', 'creator', 'links.creator', 'links.organizer', 'organizerName']) {
			var url = baseUrl + '/costum/coevent/get_events/request/actors/id/' + coevent_parent.id + '/type/' + coevent_parent.type;
			var parameter = {
				types: __types
			}
			return new Promise(function(__resolve, reject) {
				ajaxPost(null, url, parameter, __resolve, reject)
			});
		}

		function load_programs(__date = '', __element = null, options = {}) {
			var container = $('#event-content<?= $myCmsId ?>');
			container.addClass('empty');
			coInterface.showLoader(container, trad.currentlyloading);
			return new Promise(function(__resolve) {
				// 'links.organizer',
				if (!__date) {
					container.empty().html('<p class="no-events">Aucun événement à afficher</p>');
					__resolve(null);
					return;
				}
				var criteria = ['organizer', 'organizerName', 'animator'];
				get_actors(criteria).then(function(communities) {
					var subevent_criteria = $.extend({}, options);
					subevent_criteria.date = __date || 'begining';
					request_get_subevents(subevent_criteria).then(function(db_subevents) {
						var html = '';
						var last_hour = '';
						var last_schedule = '';

						function has_role(community, subevent) {
							return (typeof community === 'object' &&
								typeof community.roles === 'object' &&
								typeof community.roles[subevent.id] === 'object' &&
								typeof community.roles[subevent.id].role === 'string');
						}

						function is_animator(community, subevent) {
							return (community.roles[subevent.id].role === 'animator');
						}
						
						db_event = db_subevents;

						$.each(db_subevents, function(i, db_subevent) {
							var organizers = communities.filter(community => has_role(community, db_subevent) && !is_animator(community, db_subevent)).map(community => {

								db_subevents[i]['organizer'][community['id']]["image"] = community['image'];
								return {
									id: community['id'],
									type: community['type'],
									name: community['name'],
									image: community['image']
								}
							});

							actors.push(organizers);

							var animators = communities.filter(community => has_role(community, db_subevent) && is_animator(community, db_subevent)).map(community => {
								return {
									name: community['name'],
									image: community['image']
								}
							});

							var moment_start = moment(db_subevent['start_date']);
							var moment_end = moment(db_subevent['end_date']);

							if (i === 0)
								html += `<span class="dinalternate date">${moment_start.format('DD.MM.YYYY')}</span>`;
							if (moment_start.format('HH:mm') !== last_hour) {
								last_hour = moment_start.format('HH:mm');
								last_schedule = (moment_start.format('mm') == "00") ? moment_start.format('HH') + "h" : moment_start.format('HH') + "h" + moment_start.format('mm');
								if (i !== 0)
									html += '</ul>';
								html += `<ul class="event-detail co-timeline-parent"><span class="event-hour" data-last-schedule="${moment_end.format('DD/MM/YYYY HH:mm')}">${last_schedule}</span>`;
							}
							var actors_with_id = [];
							if (userConnected)
								actors_with_id = db_subevent['communities'].filter(community => community === userConnected['_id']['$id']);

							var isFollowed = false;
							if (userId != "" && typeof db_subevent.links.attendees != 'undefined' && typeof db_subevent.links.attendees[userId] != "undefined")
								isFollowed = true;

							isFollow[db_subevent['id']] = isFollowed;

							var isShared = false;
							var tip = "S'inscrire";
							var actionConnect = 'follow';
							var icon = 'user-plus';
							var urlVisio = (typeof db_subevent.url != 'undefined') ? db_subevent.url : "";
							var tipVisio = "Rejoindre la visio-conférence";
							var labelVisio = "Rejoindre";
							var attendeeObj = {};
							if (typeof db_subevent.links.attendees != 'undefined') {
								$.each(db_subevent.links.attendees, function(idAtt, valAtt) {
									attendeeObj[idAtt] = {
										type: valAtt
									}
								});
							}

							var attendees = (typeof db_subevent.links.attendees != 'undefined') ? JSON.stringify(db_subevent.links.attendees) : {};
							var nbAttendees = (typeof db_subevent.links.attendees != 'undefined') ? Object.keys(db_subevent.links.attendees).length : 0;
							var expectedAttendees = (typeof db_subevent.expectedAttendees != "undefined" && parseInt(db_subevent.expectedAttendees) != NaN) ? parseInt(db_subevent.expectedAttendees) : 0;
							expectedAttendeesOne = expectedAttendees;
							var classBtn = '';
							if (isFollowed) {
								actionConnect = 'unfollow';
								icon = 'unlink';
								classBtn = 'bg-white-comment text-dark';
								tip = "Je ne veux plus être inscrit.e";
							}

							actionConnectCLick = actionConnect;

							var btnLabel = (actionConnect == "follow") ? "Inscription en un click" : "Déjà inscrite";
							labelBtnUnfolow = btnLabel;
							// var btnLabel = (actionConnect == "follow") ? "S'inscrire" : "Déjà inscrite";
							var btnVisio = (urlVisio !== ``) ? `<button class="btn btn-default btn-sm tooltips btn-participate" onclick="window.open('${urlVisio}')" data-toggle="tooltip" data-placement="left" data-original-title="${tipVisio}"><i class="fa fa-link"></i>
                                        ${labelVisio}
                                    </button><br>` : ``;

							var thumbAttendeesStr = '';
							if (nbAttendees > 0) {
								ajaxPost('#thumbAttendee' + db_subevent['id'], baseUrl + '/' + moduleId + '/default/render?url=/./pod/listItems', {
										"title": "",
										"links": attendeeObj,
										"connectType": "attendees",
										"number": 12,
										"titleClass": "col-xs-12 title text-gray",
										"heightWidth": 50,
										"containerClass": "text-center no-padding margin-top-10 margin-bottom-10",
										"contextId": coevent_parent.id,
										"contextType": coevent_parent.type,
										"eventClass": "lbh-preview-element"
									},
									function(data) {
										thumbAttendeesStr = data;
									}, null, null, {
										async: false
									});
							}
							dt_participate = thumbAttendeesStr;
							var address_str;
							if (db_subevent.address) {
								var keys = ['streetAddress', 'addressLocality'];
								address_str = '';
								$.each(keys, function(i, key) {
									if (typeof db_subevent.address[key] === 'string')
										address_str += db_subevent.address[key] + ' ';
								})
							}
							var parser = new DOMParser();
							var htmlDoc = parser.parseFromString(thumbAttendeesStr, 'text/html');
							var attendeesLinks = Array.from(htmlDoc.querySelectorAll('a'));
							html += `<li class="event-${db_subevent.id}">
                                    <span class="point-holder"></span>
									<div class="event-card">
										<div class="co-into-content">
											<div style="display: block; padding: 10px;">
												<div style="text-transform: uppercase; margin-bottom: 5px;">
													<button onclick="open_preview_event(this)" data-drawer="${i}" style="border: none; padding-left: 0; background: transparent;"><i class="fa fa-eye" aria-hidden="true" style="margin-right: 5px;"></i> ${db_subevent['name']}</button>
												</div>	
												<div class="co-timeline-block" style="margin-top: 30px;">
													<div style="width: 20px; padding-left: 2px;">
														<i class="fa ${address_str ? 'fa-map-marker' : 'fa-exclamation-triangle'}" aria-hidden="true"></i> 
													</div> 
													<div style="font-size: 15px;">${address_str ? ` ${address_str}` : tradCms.missingLocation }</div>
												</div>
												<div class="co-timeline-block" style="align-items: baseline;margin-top: 10px;">
													<div style="width: 20px; font-size: 15px;">
														<i class="fa fa-users" aria-hidden="true"></i> 
													</div>
													<div style="font-size: 15px;">
														${nbAttendees > 0 
														    ? (nbAttendees <= 3
																? `<div>${nbAttendees = 1 ? nbAttendees +" "+tradCms.withParticip : nbAttendees+" "+tradCms.withParticip+"s"}  </div><div id="thumbAttendee${db_subevent.id}" style="display: inline-flex; border: 2px solid #333; padding: 2px 15px; border-radius: 20px; margin-top: 5px;" class="co-parent-img">${thumbAttendeesStr}</div>` 
																: `<div>3 participant(s)</div> 
																	<div id="thumbAttendee${db_subevent.id}" style="display: inline-flex; border: 2px solid #333; padding: 2px 15px; border-radius: 20px; margin-top: 5px;"  class="co-parent-img">` 
																	+ attendeesLinks.slice(0, 3).map(a => a.outerHTML).join('') 
																	+ `</div> + ${nbAttendees - 3} autre(s)`)
															: tradCms.noParticip }
													</div>
												</div>
											</div>
											
											<div>
												${dataHelper.printf('<div class="rolesLabel" style="display: grid;">{{organizer_str}}</div>', {
													organizer_str: organizers.length === 0 
														? ''
														: dataHelper.printf(
															'Organisateur{{s}} <div style="display: flex">{{organizer_join}}{{extra_organizers}}</div>', 
															{ 
																s: organizers.length > 1 ? 's' : '',
																organizer_join: organizers.slice(0, 2).map(function (organizer) {
																	return dataHelper.printf(
																		'<div class="actorNames" style="display: flex; justify-content: center;">{{define_img}}</div>', 
																		{
																			define_img: dataHelper.printf(
																				`<div class="img-container" title="{{title}}">
																					<button onclick="open_preview_organizer(this)" data-id="${organizer.id}" data-type="${organizer.type}" style="border: none; padding-left: 0; background: transparent; width: 100%; height: 100%;">
																						<img src="{{src}}" class="co-timeline-avatar" />
																					</button>
																				</div>`, 
																				{
																					src: baseUrl + organizer.image,
																					title: organizer.name.replace(/"/g, "'"),
																					type: organizer.type,
																					id: organizer.id
																				}
																			)
																		}
																	);
																}).join(''),
																extra_organizers: organizers.length > 2
																	? dataHelper.printf('<div class="extra-organizers co-remaining">+{{remaining}}</div>', { 
																		remaining: organizers.length - 2
																	})
																	: ''

															}
														)
												})}
											</div>


										</div>
									<div style="display:none;">
										<button class="co-btn-manage-event" onclick="event_toggle(this)">Gérer l'évènement <i class="fa fa-arrow-right" style="margin-left: 5px;" aria-hidden="true"></i></button>
									</div>
									<div class="co-content-slide" data-target="slide-content">
										<div class="co-my-event-descr" style="margin-bottom: 10px;display: none;">
											<small class="">${typeof db_subevent['short_description'] !== 'undefined' ? db_subevent['short_description'].replace(/\n/g, "<br>") : ''}</small>
										</div>
										<button class="btn btn-danger btn-sm tooltips  btn-delete-coevent btn-details-inscri" data-event="${db_subevent['id']}" onclick="event_delete(this)"><i class="fa fa-trash"></i></button>
										<button class="btn btn-default btn-sm tooltips  btn-edit-coevent btn-details-inscri" data-event="${db_subevent['id']}"><i class="fa fa-pencil"></i></button>
										<button class="btn btn-default btn-sm tooltips btn-participate btn-details-inscri ${classBtn}" data-program="${db_subevent['id']}" onclick="event_participation(this)"
										data-toggle="tooltip" data-placement="left" data-original-title="${tip}" data-expected-attendees="${expectedAttendees}" data-attendees='${attendees}'
										data-ownerlink="${actionConnect}" data-id="${db_subevent['id']}" data-type="events" data-name="${db_subevent['name']}" data
										data-isFollowed="${isFollowed}"><i class="fa fa-${icon}"></i> ${btnLabel}</button>
										${btnVisio}
										<p class="rolesLabel">${animators.length == 0 ? `` : `Animé par : ${animators.map(function (map) {
											return `<span class="actorNames"> ${map['name']}</span></p>`;
										}).join('')}`}
									</div>
                                </li></div>`;
						});

						var items = $('#journee-caroussel<?= $myCmsId ?> #items<?= $myCmsId ?> .item');
						items.removeClass('active');

						if (items.length && db_subevents.length) {
							date = moment(db_subevents[0]['start_date']).format('DD.MM.YYYY');
							if (__element) {
								__element.classList.add('active');
							} else {
								items.each(function() {
									if ($(this).data('target') === date) {
										$(this).addClass('active');
									}
								});
							}
						}
						html += '';
						container.removeClass('empty').html(html);
						coInterface.bindLBHLinks();
						$('.img-container').tooltip();
						__resolve();
					});
				});
			})
		}

		function load_journees(__option = {}) {
			return new Promise(function(__resolve) {
				request_get_subevent_dates().then(function(dates) {
					var df_date_container_dom = $('.df-container[data-id="' + php.blockKey + '"] .df-dates');
					df_date_container_dom.empty().html('');

					function buildHtml(dates) {
						var change = 0;
						var last_group = '';
						dates.forEach(function(__date) {
							if (last_group !== __date['date_group']) {
								change++;
								last_group = __date['date_group'];
								df_date_container_dom.append(template_date_filter({
									date: last_group,
									month: __date['year'] + '<br>' + trad[__date['month'].toLowerCase()].toUpperCase(),
									order: __date['date'],
									day: trad[__date['day'].toLowerCase()].toUpperCase()
								}));
							}
						});
					}
					if (dates.fromNow && dates.fromNow.length)
						buildHtml(dates.fromNow);
					else if (dates.before && dates.before.length)
						buildHtml(dates.before);
					(function() {
						var last_width = $(window).width();

						function is_hidden(date_dom) {
							var parent_dom = date_dom.parent();
							var parent_width = parent_dom.outerWidth(true);
							var offset = (date_dom.offset().left - parent_dom.offset().left) + date_dom.outerWidth(true);
							offset = parseInt(offset);
							return (offset > parent_width);
						}

						function do_update_navigation() {
							$('.df-container').each(function() {
								var container_dom = $(this);
								var date_list_dom = container_dom.find('.df-date');
								var navigation_dom = container_dom.find('.df-navigation');
								navigation_dom.hide();
								date_list_dom.each(function() {
									if (is_hidden($(this))) {
										navigation_dom.css('display', '');
										return (false);
									}
								})
							});
						}
						setInterval(function() {
							if (last_width !== $(window).width()) {
								last_width = $(window).width();
								do_update_navigation();
							}
						});
						do_update_navigation();
					})();
					if (dates.fromNow.length > 0)
						__resolve(moment(dates.fromNow[0].date_group, 'DD.MM.YYYY'));
					else if (dates.before.length)
						__resolve(moment(dates.before[dates.before.length - 1].date_group, 'DD.MM.YYYY'));
					else
						__resolve(null);
				});
			});
		}

		function change_views(element) {
			var jq_element_dom = $(element);
			$('#program-container<?= $myCmsId ?> .journee_views button').removeClass('active');
			$('.coevent-program-viewer').removeClass('show');
			jq_element_dom.addClass('active');
			$(jq_element_dom.data('target')).addClass('show');
			switch ($('#program-container' + php.blockKey + ' .program-view').index(jq_element_dom)) {
				case 0:
					load_journees().then(function(nearest) {
						var params = {};
						if (nearest)
							params.goto = nearest;
						load_fullcalendar(params).then(function() {
							coInterface.bindLBHLinks();
						});
					});
					break;
				case 1:
					load_journees().then(function(nearest) {
						if (nearest)
							$('.df-container[data-id="' + php.blockKey + '"] .df-date[data-target="' + nearest.format('DD.MM.YYYY') + '"]').trigger('click');
						else
							load_programs().then();
						coInterface.bindLBHLinks();
					});
					break;
				case 2:
					load_journees().then(function(nearest) {
						params = {};
						if (nearest)
							params.goto = nearest;
						load_timeline(params).then(function() {
							coInterface.bindLBHLinks();
						})
					});
					break;
			}
		}

		function load_views() {
			var params, self, target_date, this_moment, journees_dom, nearest_incoming_event, today, format_string;
			return new Promise(function(__resolve) {
				load_journees().then(function(nearest) {
					params = {};
					if (nearest) {
						$('.df-container[data-id="' + php.blockKey + '"] .df-date[data-target="' + nearest.format('DD.MM.YYYY') + '"]').trigger('click');
						params.goto = nearest;
					} else
						load_programs().then();
					load_timeline(params).then(function() {
						load_fullcalendar(params).then(function() {
							coInterface.bindLBHLinks();
							__resolve();
						});
					});
				});
			});
		}

		function toggleEventFilter() {
			var filter = get_session_data();
			var self = $(this);

			// enable typology and event filters on view
			$('a.event-type-filter').each(function() {
				var self = $(this);
				self.removeClass('active');
				if (filter.type === self.attr('href'))
					self.addClass('active');
			});
			$('.event-types .event-type').each(function() {
				var self = $(this);
				self.removeClass('filter-active');
				if (filter.type === self.attr('data-target'))
					self.addClass('filter-active');
			});

			if (filter.type !== null && filter.type.length > 0) {
				self.css({
					'display': '',
					top: `${mainNavDom.outerHeight()}px`
				});
			} else {
				self.fadeOut(800);
				if (programContainerDom.length)
					$('.coevent-program').trigger('coevent-filter');
			}
		}


		$(function() {
			var ce_map;

			function do_open_map(map) {
				return new Promise(function(resolve, reject) {
					var map_option;
					if (costum.editMode)
						reject('edit mode');
					else
						$('#ce-prog-map-container' + blockKey).css({
							height: ($(window).height() - $('#mainNav').outerHeight(true)) + 'px',
							width: $('#all-block-container').outerWidth(true),
							top: $('#mainNav').outerHeight(true) + 'px'
						}).show(200, function() {
							if (typeof map === 'undefined') {
								map_option = {
									container: '#ce-prog-map-container' + blockKey + ' .map-content',
									activePopUp: true,
									mapOpt: {
										btnHide: false,
										doubleClick: true,
										scrollWheelZoom: false,
										zoom: 2,
									},
									mapCustom: {
										getPopup: function(data) {
											var l_filter_address = function(key) {
												return (typeof data.address[key] === 'string');
											};
											var l_map_address = function(key) {
												return (data.address[key]);
											};
											var l_html_tag = function(tag) {
												return ('<span class="label label-default"><i class="fa fa-hashtag"></i> ' + tag + '</span>');
											};
											var date = {
												start: moment(data.start_date),
												end: moment(data.end_date)
											};
											var popup_option = {
												profile: '',
												banner_html: '',
												tags: '',
												address: ['streetAddress', 'addressLocality'].filter(l_filter_address).map(l_map_address).join(' '),
												title: data.name,
												id: data.id
											};

											if (date.start.format('YYYY-MM-DD') === date.end.format('YYYY-MM-DD'))
												popup_option.date = dataHelper.printf('<i class="fa fa-calendar"></i> {{date}} <i class="fa fa-clock-o" aria-hidden="true"></i> {{start}} - {{end}}', {
													date: date.start.format('DD.MM.YYYY'),
													start: date.start.format('HH:mm'),
													end: date.end.format('HH:mm')
												});
											else
												popup_option.date = dataHelper.printf('<i class="fa fa-calendar"></i> {{start_date}} <i class="fa fa-clock-o" aria-hidden="true"></i> {{start_hour}} <br><i class="fa fa-calendar"></i> {{end_date}} <i class="fa fa-clock-o" aria-hidden="true"></i> {{end_hour}}', {
													start_date: date.start.format('DD.MM.YYYY'),
													end_date: date.end.format('DD.MM.YYYY'),
													start_hour: date.start.format('HH:mm'),
													end_hour: date.end.format('HH:mm')
												});
											if (data.banner)
												popup_option.banner_html = '<div class="ce-map-banner"><img src="' + (baseUrl + data.banner) + '" /></div>';
											if (data.profile)
												popup_option.profile = '<div class="ce-map-profile"><img src="' + (baseUrl + data.profile) + '" /></div>';
											if (data.tags.length > 0)
												popup_option.tags = dataHelper.printf('<div style="margin-bottom: 5px">{{tags}}</div>', {
													tags: data.tags.map(l_html_tag).join(' ')
												})
											return (dataHelper.printf(
												'<div>' +
												'	{{banner_html}}' +
												'	<span><i class="fa fa-map-marker"></i> {{address}}</span><br>' +
												'	<span>{{date}}</span><br>' +
												'	{{tags}}' +
												'	<div class="ce-map-title">{{profile}}{{title}}</div>' +
												'	<a href="#page.type.events.id.{{id}}" class="lbh-preview-element undefined item_map_list popup-marker" id="popup{{id}}"><div class="btn btn-sm btn-more col-md-12"><i class="fa fa-hand-pointer-o"></i>En savoir plus</div></a>' +
												'</div>', popup_option
											));
										},
										icon: {
											getIcon: function(data) {
												var option = {
													iconSize: [45, 55],
													iconAnchor: [25, 45],
													popupAnchor: [-3, -30],
													shadowUrl: '',
													shadowSize: [68, 95],
													shadowAnchor: [22, 94]
												};

												if (data.elt.marker)
													option.iconUrl = baseUrl + data.elt.marker;
												else
													option.iconUrl = modules.map.assets + '/images/markers/event-marker-default.png'
												return (L.icon(option));
											}
										}
									},
									elts: {}
								}
								map = new CoMap(map_option);
							}
							resolve(map);
						});
				})
			}
			$('#program-container<?= $myCmsId ?>').on('coeventfilter', function() {
				load_views();
			});
			$('#program-container<?= $myCmsId ?> .program-view:not(.no-view)').each(function(_, element) {
				$(this).on('click', function() {
					change_views(this);
				})
			});
			$('.program-view[data-target="#map-view' + blockKey + '"]').on('click', function() {
				do_open_map(ce_map).then(function(map) {
					ce_map = map;
					request_get_subevents({
						fromToday: true
					}).then(function(db_subevent_list) {
						var data = {};
						db_subevent_list.forEach(function(event) {
							if (typeof event.address === 'object' && typeof event.geo === 'object' && typeof event.geoPosition === 'object')
								data[event.id] = event;
						});
						map.clearMap();
						map.addElts(data);
					})
				});
			});
			$('.filter-btn-hide-map').on('click', function() {
				$('#ce-prog-map-container' + blockKey).hide(200);
			});
			$('.df-container .df-dates').on('click', '.df-date', function() {
				var self = $(this);
				if (self.hasClass('active'))
					return;
				self.parent().find('.df-date').removeClass('active');
				self.addClass('active');
				load_programs(self.data('target'));
			});
			$('#journee-navigation<?= $myCmsId ?> .journee-control.right').on('click', function() {
				if (_journee_carousel) {
					_journee_carousel.gotoNext(function() {
						reload_program_event();
						var active = $('#event-content<?= $myCmsId ?> .dinalternate.date').text();
						$('[data-target="' + active + '"]').addClass('active');
					});
				}
			});
			$('#journee-navigation<?= $myCmsId ?> .journee-control.left').on('click', function() {
				if (_journee_carousel) {
					_journee_carousel.gotoPrevious(function() {
						reload_program_event();
						var active = $('#event-content<?= $myCmsId ?> .dinalternate.date').text();
						$('[data-target="' + active + '"]').addClass('active');
					});
				}
			});
			$('#event-proposition<?= $myCmsId ?>, .ce-map-create-event').on('click', function(__e) {
				__e.preventDefault();
				coevent_create_event({
					afterLoad: "afterLoad"
				});
			});
			$('#event-download<?= $myCmsId ?>').on('click', function(__e) {
				__e.preventDefault();
				get_event_context().then(function(event) {
					window.ce_prog_download(event.name + '.pdf');
				});
			});
			$('.active_filter_container').on('active-filter', toggleEventFilter).on('disable-filter', toggleEventFilter);
			$('.coevent-program').on('coevent-filter', function() {
				load_filter_view()
				load_views();
			});
			$(dataHelper.printf('#chronology_container{{blockKey}}', {
				blockKey: blockKey
			})).on('click', '.event-filter', function() {
				var self = $(this);
				var state = self.data('state');
				sessionStorage.setItem(dataHelper.printf('{{block}}-showAllEvents', {
					block: blockKey
				}), state);
				updateFilterEventPeriodChronology();
				load_timeline(get_session_data());
			});
			updateFilterEventPeriodChronology();
			var page_config = typeof costum.app['#' + page] === 'object' && costum.app['#' + page] ? costum.app['#' + page] : {};
			page_config = typeof page_config.coeventConfig === 'object' && page_config.coeventConfig ? page_config.coeventConfig : null;
			if (notNull(page_config) && typeof page_config.regionFilter != "undefined" && page_config.regionFilter) {
				var filter = get_session_data();
				filter.regions = page_config.regionFilter;
				set_session_data(filter);
				load_filter_view();
			}
			get_event_context().then(function(event) {
				load_filter_view();
				var start_moment_parent = moment(event['start_date']);
				var end_moment_parent = moment(event['end_date']);
				if (end_moment_parent.diff(start_moment_parent, 'days') > 6)
					change_views($('#program-container' + php.blockKey + ' .program-view').eq(1));
				else
					change_views($('#program-container' + php.blockKey + ' .program-view').eq(0));
			});
			if (page_config && page_config.showMapFirst)
				do_open_map(ce_map).then(function(map) {
					ce_map = map;
					request_get_subevents({
						fromToday: true
					}).then(function(db_subevent_list) {
						var data = {};
						db_subevent_list.forEach(function(event) {
							if (typeof event.address === 'object' && typeof event.geo === 'object' && typeof event.geoPosition === 'object')
								data[event.id] = event;
						});
						map.clearMap();
						map.addElts(data);
					})
				});
		});
	if (pageMenu == "#programme") {
		$(".filter-btn-hide-map").click()
	}
	})(jQuery, window);
</script>