<?php

$costum = !empty($this->costum) ? $this->costum : $this->context->config['costum'];
$primaryColor = '#EE302C';
if (!empty($costum) && !empty($costum["css"]) && !empty($costum["css"]["color"])) {
	$mainKey = array_keys($costum["css"]["color"])[0];
	$primaryColor = $costum["css"]["color"][$mainKey] ?? $primaryColor;
}
$blockCms["primaryColor"] = $blockCms["primaryColor"] ?? $primaryColor;
$blockCms["menuColor"] = $blockCms["menuColor"] ?? '#01305E';
$todayPhp = date('Y-m-d');
HtmlHelper::registerCssAndScriptsFiles([
	"/js/coevent/carousel.js",
	"/js/coevent/day_filter.js",
	"/css/coevent/day_filter.css",
], Yii::app()->getModule('costum')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles([
	"/plugins/knightlab_timeline3/css/timeline.css",
	"/plugins/knightlab_timeline3/js/timeline.js",
], Yii::app()->baseUrl);
?>
<style>
	#program-container<?= $blockKey ?> {
		text-align: center;
	}

	#journee-caroussel<?= $blockKey ?> {
		position: relative;
	}

	.rolesLabel.attendeesCounter:hover+.attendeeThumb {
		display: inline-block !important;
	}

	.attendeeThumb:hover {
		display: inline-block !important;
	}

	.rolesLabel.attendeesCounter:hover {
		cursor: pointer !important;
	}

	.listsItemsPod h4 {
		display: none;
	}

	.slide {
		list-style: none;
		flex: 0 0 auto;
		width: 100%;
	}

	button.program-view:nth-child(2) {
		margin-left: 2em;
		margin-right: 2em;

	}

	button.program-view {
		border: none;
		background-color: unset;
		font-size: 18px;
		color: <?= $blockCms['menuColor'] ?>;
	}

	button.program-view.active,
	button.program-view:hover {
		border-bottom: solid 1px<?= $blockCms['primaryColor'] ?>;
	}

	/* span.event-hour:after {
        content: "\2022";
        font-size: 2.2em;
        position: absolute;
        top: -0.4em;
        left: 1.6em;
    }

    .event-detail li::before {
        content: "";
        background: #52A8AC;
        font-size: 1.5em;
        width: 1.8em;
        margin-left: -1em;
        height: 4px;
        position: absolute;
        left: 0.4em;
        top: 0.5em;
    } */

	.event-detail li a {
		color: #01305E !important;
		font-weight: bold;
		font-size: 18px
	}

	.slider {
		width: 100%;
		overflow: hidden;
		margin: 0 auto;
		position: relative;
	}

	.slides {
		display: flex;
		flex-direction: row;
		height: 100%;
		width: 100%;
		margin: 0;
		padding: 0;
	}

	#items<?= "$blockKey " ?> {
		display: flex;
		align-items: center;
		justify-content: center;
	}

	#items<?= "$blockKey " ?>.item {
		cursor: pointer;
		display: flex;
		flex-direction: column;
		align-items: center;
	}

	#items<?= "$blockKey " ?>.item .date {
		border: <?= $blockCms['primaryColor'] ?> 1px solid;
		background-color: white;
		color: #01305E;
		padding: 1.2rem 1.5rem .8rem;
		border-radius: 10px;
		display: block;
		font-weight: bold;
		font-size: 1.9rem;
		width: fit-content;
		width: -moz-fit-content;
	}

	#items<?= "$blockKey " ?>.item.active .date {
		background: <?= $blockCms['primaryColor'] ?>;
		color: white;
	}

	#items<?= "$blockKey " ?>.item .jour,
	.month {
		text-align: center;
		font-weight: bold;
		display: block;
		margin: auto 10px;
		color: #01305E;
	}

	#journee-navigation<?= "$blockKey " ?>.journee-control {
		position: absolute;
		top: 5px;
		padding: 1.9rem 1rem;
		border-radius: 10px;
		border: transparent 1px solid;
		background-color: transparent;
		color: <?= $blockCms['primaryColor'] ?>;
	}

	#journee-navigation<?= "$blockKey " ?>.journee-control:hover {
		border-color: <?= $blockCms['primaryColor'] ?>;
		background-color: white;
		cursor: pointer;
	}

	.journee-control.right {
		right: 40px;
	}

	.journee-control.left {
		left: 40px;
	}

	#event-content<?= $blockKey ?>.empty {
		margin: 0 auto;
	}

	#event-content<?= $blockKey ?>:not(.empty) {
		border-left: <?= $blockCms['primaryColor'] ?> solid 5px;
		padding: 2rem;
		margin-left: 5rem !important;
	}

	#event-content<?= "$blockKey " ?>.date {
		font-size: 2rem;
		font-weight: bold;
		display: block;
	}

	#event-content<?= "$blockKey " ?>ul {
		margin: none;
	}

	#event-content<?= "$blockKey " ?>.event-detail {
		position: relative;
		font-weight: bold;
	}

	#event-content<?= "$blockKey " ?>.event-detail li {
		list-style: none;
		margin-top: 2rem;
	}

	/*#event-content*/
	<?php //= "$blockKey " 
	?>
	/*.event-detail li::before {*/
	/*    content: "\2022";*/
	/*    color: */
	<?php //= $blockCms['primaryColor'] 
	?>
	/*;*/
	/*    font-weight: bold;*/
	/*    font-size: 1.5em;*/
	/*    display: inline-block;*/
	/*    width: .8em;*/
	/*    margin-left: -1em;*/
	/*}*/

	#event-content<?= "$blockKey " ?>.event-detail-detail li {
		margin-top: 0;
		line-height: 1;
	}

	#event-content<?= "$blockKey " ?>.event-detail-detail li::before {
		color: black;
	}

	#event-content<?= "$blockKey " ?>.event-hour {
		font-weight: bold;
		position: absolute;
		left: -8rem;
		top: .7rem;
		color: <?= $blockCms['primaryColor'] ?>;
	}

	#active_filter_container<?= $blockKey ?> {
		background-color: <?= $blockCms['primaryColor'] ?>;
		color: white;
		position: fixed;
		left: 50px;
		padding: 10px;
		border-radius: 0 0 10px 10px;
		box-shadow: 0 0 10px rgba(0, 0, 0, .3);
		z-index: 9;
		border-left: 1px solid<?= $blockCms['primaryColor'] ?>;
		border-bottom: 1px solid<?= $blockCms['primaryColor'] ?>;
		border-right: 1px solid<?= $blockCms['primaryColor'] ?>;
		cursor: pointer;
	}

	.coevent-program-viewer .fc-time,
	.coevent-program-viewer .fc-title {
		color: white !important;
	}

	.coevent-button<?= $blockKey ?> {
		border: none;
		border-radius: 20px;
		padding: 1rem 2rem;
		background-color: <?= $blockCms['primaryColor'] ?>;
		color: white;
		margin-bottom: 10px;
	}

	.coevent-button<?= "$blockKey " ?>:hover {
		text-decoration: none;
	}

	.coevent-button<?= "$blockKey " ?>.inverted {
		color: <?= $blockCms['primaryColor'] ?>;
		background-color: white;
	}

	@media only screen and (max-width: 425px) {
		.journee-control.left {
			left: 0;
		}

		.journee-control.right {
			right: 0;
		}

		.ce-map-create-event {
			position: absolute;
			top: 25px !important;
			left: 25% !important;
			z-index: 11;
			transform: translateX(-50%);
		}

		.filter-btn-hide-map {
			top: 25px !important;
			border-radius: 5px;
			width: auto;
		}

		#ce-prog-map-container<?= "$blockKey " ?>.leaflet-top .leaflet-control {
			margin-top: 75px !important;
		}

	}

	.program-organizer-image {
		display: inline-block;
		width: 40px;
		height: 40px;
		border-radius: 50%;
		box-shadow: 0 0 10px rgba(0, 0, 0, .2);
		overflow: hidden;
	}

	.program-organizer-image img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.event-detail li {
		position: relative;
	}

	.line-indicator {
		position: absolute;
		top: 15px;
		left: -65px;
		width: 60px;
		height: 5px;
		display: block;
		background-color: <?= $blockCms['primaryColor'] ?>;
		border-radius: 50px;
	}

	.point-holder {
		display: block;
		height: 15px;
		width: 15px;
		border-radius: 50%;
		position: absolute;
		top: -5px;
		left: -5px;
		background: <?= $blockCms['primaryColor'] ?>;
	}

	@media only screen and (max-width: 992px) {

		#program-container<?= $blockKey ?> {
			text-align: center;
			max-width: 100%;
		}
	}

	.rolesLabel {
		color: #01305E;
		font-size: 17px;
		font-weight: 500;
	}

	.actorNames {
		color: #01305E;
		font-size: 16px;
		font-style: italic;
		display: inline-block;
		margin-right: 5px;
	}

	.actorNames .img-container {
		width: 50px;
		height: 50px;
		overflow: hidden;
		border-radius: 50%;
		display: inline-block;
		vertical-align: middle;
		margin-right: 2px;
	}

	.actorNames .img-container img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.btn-participate {
		background-color: <?= $blockCms['primaryColor'] ?>;
		color: white;
		border: solid 1px <?= $blockCms['primaryColor'] ?>;
		border-radius: 50px;
		padding: 0.5rem 2rem;
		margin-left: 30px;
		font-size: 18px;
	}

	.btn-participate:hover {
		color: #01305E;
		border: solid 1px<?= $blockCms['primaryColor'] ?>;
		;
		background-color: white;
	}

	.descr-event {
		color: #01305E;
		font-size: 16px;
	}

	.filters-activate {
		border: 2px solid #fff;
		margin-right: 5px;
		margin-bottom: 5px;
		padding: 5px 10px;
		border-radius: 5px;
		color: #4623C9 !important;
		text-decoration: none;
		display: inline-block;
		background-color: white;
	}

	.filters-activate i {
		margin-right: 5px;
	}

	.program-content {
		text-align: left;
		width: 100%;
	}

	.coevent-program-viewer {
		display: none;
	}

	.coevent-program-viewer.show {
		display: block;
	}

	.df-container {
		--primary-color: <?= $blockCms['primaryColor'] ?>;
		--color-normal: #01305E;
		--color-invert: #f0f0f0;
	}

	.ce-prog-map-container {
		display: none;
		position: fixed;
		width: 100vw;
		z-index: 11;
	}

	.ce-prog-map-container .map-content {
		width: 100%;
		height: 100%;
	}

	.filter-btn-hide-map {
		top: 40px;
		border-radius: 5px;
		width: auto;
	}

	.ce-map-create-event {
		position: absolute;
		top: 40px;
		left: 50%;
		z-index: 11;
		transform: translateX(-50%);
	}

	.ce-map-title {
		font-size: 18px;
	}

	.ce-map-profile {
		display: inline-block;
		width: 50px;
		height: 50px;
		overflow: hidden;
		border-radius: 50%;
		vertical-align: middle;
		margin-right: 5px;
	}

	.ce-map-profile img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.ce-map-banner {
		width: 100%;
		height: auto;
		margin-bottom: 5px;
		overflow: hidden;
		border-radius: 6px;
	}

	.ce-map-banner img {
		width: 100%;
	}

	.no-events {
		text-align: center;
	}

	.modal-header {
		--main1: #82c17f;
	}
</style>
<div id="active_filter_container<?= $blockKey ?>" class="active_filter_container" style="display: none;">
	Filtres actifs : <div id="active_filter_text<?= $blockKey ?>" class="active_filter" data-type=""></div>
	<div id="tooltipeventfilter<?= $blockKey ?>" class="tooltip">Cliquez ici pour désactiver le filtre</div>
</div>
<div class="ce-prog-map-container" id="ce-prog-map-container<?= $blockKey ?>">
	<div class="map-content"></div>
	<button class="coevent-button<?= $blockKey ?> btn-participate ce-map-create-event" type="button">
		Proposer un évènement <i class="fa fa-calendar"></i>
	</button>
	<button type="button" class="filter-btn-hide-map">Programme</button>
</div>
<div class="container coevent-program">
	<div id="leprogramme<?= $blockKey ?>" data-anchor-target="leprogramme" class="row">
		<div class="col-sm-12 program-container" id="program-container<?= $blockKey ?>">
			<!-- <h1 class="dinalternate" style="color: black;">
                Le programme
            </h1> -->
			<!-- <div style="border: solid <?= $blockCms['primaryColor'] ?> 4px;border-radius: 11px;width: 17%; margin-bottom: 15px;"></div> -->
			<div class="btn-group journee_views" style="margin-bottom: 1em;">
				<button type="button" class="program-view" data-target="#event_calendar_container<?= $blockKey ?>">Calendrier</button>
				<button type="button" class="program-view" data-target="#journee-caroussel<?= $blockKey ?>">Filtre par journée</button>
				<button type="button" class="program-view" data-target="#chronology_container<?= $blockKey ?>">Chronologie</button>
				<button type="button" class="program-view no-view" data-target="#map-view<?= $blockKey ?>"> <i class="fa fa-map-marker"></i> <?= Yii::t('common', 'Map') ?></button>
			</div>
			<div id="journee-caroussel<?= $blockKey ?>" class="coevent-program-viewer">
				<div class="df-container" data-id="<?= $blockKey ?>">
					<div class="df-prev df-navigation">
						<i class="fa fa-chevron-left"></i>
					</div>
					<div class="df-dates"></div>
					<div class="df-next df-navigation">
						<i class="fa fa-chevron-right"></i>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="program-content empty" id="event-content<?= $blockKey ?>"></div>
				</div>
			</div>
			<!-- vue calendrier des events -->
			<div id="event_calendar_container<?= $blockKey ?>" class="coevent-program-viewer">
				<div id="event_calendar<?= $blockKey ?>"></div>
			</div>
			<!-- vue calendrier des events -->
			<div id="chronology_container<?= $blockKey ?>" class="coevent-program-viewer">
				<div class="btn-group">
					<button type="button" class="btn btn-default event-filter" data-state="false"><?= Yii::t('common', 'Show past events') ?></button>
					<button type="button" class="btn btn-default event-filter" data-state="true"><?= Yii::t('common', 'Hide past events') ?></button>
				</div>
				<div id='timeline-embed<?= $blockKey ?>' style="width: 100%; height: 55vh; margin-top: 50px;"></div>
			</div>
			<br>
			<div class="text-center">
				<button class="coevent-button<?= $blockKey ?> btn-participate" type="button" id="event-proposition<?= $blockKey ?>">
					Proposer un évènement <i class="fa fa-calendar"></i>
				</button>
				<button class="coevent-button<?= $blockKey ?> btn-participate" type="button" id="event-download<?= $blockKey ?>">
					Télécharger le programme <i class="fa fa-download"></i>
				</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	(function($, W) {
		var _journee_carousel = null;
		var loadTimeout;
		var php = {
			blockKey: '<?= $blockKey ?>'
		};
		var nearestDate = "";
		var model = {
			EVENT: 'events'
		};

		if (costum.editMode) {
			cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?>;
			var coeventProgram = {
				configTabs: {
					general: {
						inputsConfig: [{
								type: "colorPicker",
								options: {
									name: "menuColor",
									label: tradCms.optionsColor,
								}
							},
							{
								type: "colorPicker",
								options: {
									name: "primaryColor",
									label: tradCms.buttonsColor,
								}
							}
						]
					},
				},
				beforeLoad: function() {

				},
				onChange: function(path, valueToSet, name, payload, value) {

				},
				afterSave: function(path, valueToSet, name, payload, value) {
					cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
				}
			}
			cmsConstructor.blocks['<?= $kunik ?>'] = coeventProgram;
		}

		if (typeof costum === "object") {
			var match = location.hash.match(/\#[\w\-]+/);
			if (costum.contextType && costum.contextType === "events")
				W.coevent_parent = {
					id: costum.contextId,
					type: "events"
				};
			else if (match && costum.app && costum.app[match[0]] && costum.app[match[0]].event)
				W.coevent_parent = {
					id: costum.app[match[0]].event,
					type: "events"
				};
			else
				W.coevent_parent = {
					id: costum.contextId,
					type: costum.contextType
				};
		}

		var blockKey = '<?= $blockKey ?>';

		function template_date_filter(args) {
			var container_dom = $('<div>');
			container_dom.addClass('df-date')
				.attr('data-target', args.date)
				.append(
					$('<div>').addClass('df-month')
					.html(args.month),
					$('<div>').addClass('df-order')
					.html(args.order),
					$('<div>').addClass('df-day')
					.html(args.day)
				);
			return (container_dom);
		}

		function request_join_event(args) {
			var url = baseUrl + '/costum/coevent/join_event';
			return new Promise(function(resolve, reject) {
				ajaxPost(null, url, args, resolve, reject);
			});
		}

		if (typeof window.event_participation !== 'function') {
			window.event_participation = function(dom) {
				var that = dom;
				// var eventData = JSON.parse(JSON.stringify()).find(function(find) {
				//     return find['id'] == $(that).data("id")
				// });
				thisElement = $(that);
				var isExpAttFull = (thisElement.data("expected-attendees") > 0 && Object.keys(thisElement.data("attendees")).length == eventData.expectedAttendees);
				if (!isExpAttFull) {
					if (userId) {
						subscribeToEvent(thisElement);
					} else {
						$("#modalLogin").on('show.bs.modal', function(e) {
							if ($("#infoBL").length == 0) {
								$("#modalLogin .modal-content .container div.row").append(`<div class="col-xs-12" id="infoBL">
                                Vous devez être connecté(e) pour vous inscrire à l’événement. Vous n’avez pas encore de compte ? 
                                <a href="javascript:;" class="bold" data-toggle="modal" data-target="#modalRegister"> Créez-en un </a>
                            </div>`);
							}
						});
						$("#modalLogin").on('hide.bs.modal', function(e) {
							$("#infoBL").remove();
						});
						toastr.info("Vous devez être connecté(e) pour vous inscrire à l’événement");
						Login.openLogin();
					}
				} else {
					if (userId)
						subscribeToEvent(thisElement);
				}

				// if (typeof eventData != "undefined" && isExpAttFull && thisElement.data("ownerlink") == "follow") {
				//     bootbox.dialog({
				//         message: `<div class="alert-white text-center"><br>
				//                     <strong>Désolé ! Il n'y a plus de place. Le nombre maximal de participants a été atteint</strong>
				//                     <br><br>
				//                     <button class="btn btn-default bootbox-close-button">OK</button>
				//                 </div>`
				//     });
				// }
			}
		}


		function updateFilterEventPeriodChronology() {
			var state = sessionStorage.getItem(dataHelper.printf('{{block}}-showAllEvents', {
				block: blockKey
			}));
			state = state ? state : 'false';
			$(dataHelper.printf('#chronology_container{{blockKey}} .event-filter', {
				blockKey: blockKey
			})).each(function() {
				var self = $(this);
				if (self.data('state').toString() === state.toString()) {
					self.css('display', 'none');
				} else {
					self.css('display', '');
				}
			});
		}

		function subscribeToEvent(eventSource, theUserId = null, on_success) {
			var labelLink = "";
			var parentId = eventSource.data("id");
			var parentType = eventSource.data("type");
			var childId = (theUserId) ? theUserId : userId;
			var childType = "citoyens";
			var name = eventSource.data("name");
			var id = eventSource.data("id");
			//traduction du type pour le floopDrawer
			eventSource.html("<i class='fa fa-spin fa-circle-o-notch text-azure'></i>");
			var connectType = (parentType == "events") ? "connect" : "follow";

			if (eventSource.data("ownerlink") == "follow") {
				callback = function() {
					labelLink = (parentType == "events") ? "DÉJÀ INSCRIT(E)" : trad.alreadyFollow;
					if (eventSource.hasClass("btn-add-to-directory")) {
						labelLink = "";
					}
					$(`[data-id=${id}]`).each(function() {
						$(this).html("<small><i class='fa fa-unlink'></i> " + labelLink.toUpperCase() + "</small>");
						$(this).data("ownerlink", "unfollow");
						$(this).data("original-title", labelLink);
						var strCounter = $(this).siblings(".rolesLabel.attendeesCounter").find("span").html();
						intCounter = parseInt(strCounter);
						newCounter = intCounter + 1;
						$(this).siblings(".rolesLabel.attendeesCounter").find("span").html(newCounter.toString());
					});
					if (typeof on_success === 'function')
						on_success();
				}
				request_join_event({
					user: childId,
					event: id
				}).then();
				// return ;
				if (parentType == "events")
					links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
				else
					links.follow(parentType, parentId, childId, childType, callback);
			} else {
				//eventSource.data("ownerlink")=="unfollow"
				connectType = (parentType == "events") ? "attendees" : "followers";
				callback = function() {
					labelLink = (parentType == "events") ? "S'inscrire" : "Déjà inscrit.e";
					if (eventSource.hasClass("btn-add-to-directory")) {
						labelLink = "";
					}
					$(`[data-id=${id}]`).each(function() {
						$(this).html("<small><i class='fa fa-chain'></i> " + labelLink.toUpperCase() + "</small>");
						$(this).data("ownerlink", "follow");
						$(this).data("original-title", labelLink);
						$(this).removeClass("text-white");
						var strCounter = $(this).siblings(".rolesLabel.attendeesCounter").find("span").html();
						intCounter = parseInt(strCounter);
						newCounter = intCounter - 1;
						$(this).siblings(".rolesLabel.attendeesCounter").find("span").html(newCounter.toString());
					});
					if (typeof on_success === 'function')
						on_success();
				};
				links.disconnectAjax(parentType, parentId, childId, childType, connectType, null, callback);
			}
		}

		function get_session_data() {
			var session_data = sessionStorage.getItem('coevent-' + W.coevent_parent.id + W.coevent_parent.type);
			var filter = session_data ? JSON.parse(session_data) : {
				tags: [],
				regions: [],
				type: null
			};
			return (filter);
		}

		function set_session_data(data) {
			sessionStorage.setItem('coevent-' + W.coevent_parent.id + W.coevent_parent.type, JSON.stringify(data));
		}

		function html_active_filter(args) {
			var container = $('<div>')
				.addClass('filters-activate')
				.data({
					key: args.key,
					value: args.value,
					type: args.type
				});
			container.append($('<i>').addClass('fa fa-times-circle'));
			container.append($('<span>').addClass('activeFilters').text(args.type === "tag" ? decodeURI(args.value) : args.value));
			container.on('click', function() {
				var self = $(this);
				var filter = get_session_data();
				switch (self.data('type')) {
					case 'type':
						filter.type = null;
						break;
					case 'tag':
						var index_of_value = filter.tags.indexOf(self.data('key'));
						filter.tags.splice(index_of_value, 1);
						break;
					case 'region':
						var index_of_value = filter.regions.findIndex(function(region) {
							return (region.id === self.data('key'));
						});
						filter.regions.splice(index_of_value, 1);
						break;
				}
				set_session_data(filter);
				// format new url
				filter.regions = filter.regions.map(region => region.id);
				var hash = location.hash.match(/\#[\w\-]+/);
				if (!hash)
					hash = '';
				else
					hash = hash[0];
				hash += '?'
				var key;
				for (key in filter) {
					if (filter[key] === null)
						continue;
					if (typeof filter[key] === 'string')
						hash += key + '=' + filter[key] + '&';
					else
						$.each(filter[key], function(i, param) {
							hash += key + '[]=' + param + '&';
						});
				}
				hash = hash.replace(/.$/, '');
				hash = location.origin + location.pathname + hash;
				history.pushState({}, '', hash);
				$('.coevent-program').trigger('coevent-filter');
			});
			return (container);
		}

		function load_filter_view() {
			var main_nav_dom = $('#mainNav');
			var filter_container_dom = $('.active_filter_container:first');
			var active_filter_dom = filter_container_dom.find('.active_filter');
			$('.event-types .event-type').removeClass('active filter-active');
			filter_container_dom.css({
				top: main_nav_dom.outerHeight() + 'px'
			});
			var filter = get_session_data();
			mylog.log("coevent_program filter", filter);
			active_filter_dom.empty();
			if (filter.type) {
				active_filter_dom.append(html_active_filter({
					key: filter.type,
					value: coTranslate(filter.type),
					type: 'type'
				}));
				$('.event-types .event-type[data-target="' + filter.type + '"]').addClass('active filter-active');
			}
			$.each(filter.tags, function(i, tag) {
				active_filter_dom.append(html_active_filter({
					key: tag,
					value: tag,
					type: 'tag'
				}));
			});
			$.each(filter.regions, function(i, region) {
				active_filter_dom.append(html_active_filter({
					key: region.id,
					value: region.name,
					type: 'region'
				}));
			});
			if (!filter.type && !filter.tags.length && !filter.regions.length)
				filter_container_dom.hide();
			else
				filter_container_dom.show();
		}

		function reload_program_event() {
			$('#program-container<?= $blockKey ?>').off('click').on('click', '#journee-carousel<?= $blockKey ?> .item', function(__e) {
				__e.preventDefault();
				load_programs($(this).data('target'));
			});
		}

		function xhr_event_context() {
			return new Promise(function(resolve) {
				ajaxPost(null, baseUrl + '/costum/coevent/get_events/request/event/id/' + W.coevent_parent.id + '/type/' + W.coevent_parent.type, null, resolve, null, 'json')
			});
		}

		function xhr_subevents(options) {
			var defaultOptions = {
				fromToday: false,
				timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
			};
			var filter = get_session_data();
			filter = $.extend({}, filter, defaultOptions, options);
			var url = baseUrl + '/costum/coevent/get_events/request/subevents/id/' + W.coevent_parent.id + '/type/' + W.coevent_parent.type;
			return new Promise(function(resolve, reject) {
				ajaxPost(null, url, filter, resolve, resolve, 'json');
			});
		}

		function request_get_subevent_dates(option) {
			var url = baseUrl + '/costum/coevent/get_events/request/dates/id/' + W.coevent_parent.id + '/type/' + W.coevent_parent.type;
			var filter = get_session_data();
			filter = $.extend({}, filter, option);
			return new Promise(function(resolve, reject) {
				ajaxPost(null, url, filter, resolve, reject, 'json')
			});
		}

		function load_timeline(__option) {
			var go;
			if (typeof __option.goto !== 'undefined') {
				go = __option.goto;
				delete __option.goto;
			}
			return new Promise(function(resolve, reject) {
				var timeline_knightlab = null;
				var container_dom = $('#timeline-embed<?= $blockKey ?>');
				coInterface.showLoader(container_dom, trad.currentlyloading);
				var tl_interval = setInterval(function() {
					if (typeof TL !== 'undefined') {
						clearInterval(tl_interval);
						var showAllEvents = sessionStorage.getItem(dataHelper.printf('{{block}}-showAllEvents', {
							block: blockKey
						}));
						var args = {
							fromToday: showAllEvents !== null ? JSON.parse(showAllEvents) : false
						};
						xhr_subevents(args)
							.then(function(db_subevent) {
								var start_at_slide = -1;
								if (db_subevent.length === 0) {
									$('#timeline-embed<?= $blockKey ?>').empty().html('<p class="no-events">' + coTranslate('No events to show') + '</p>')
								} else {
									var todayPhp = moment('<?= $todayPhp ?>');
									var filterNearestDate = db_subevent.reduce(function(date, reduce) {
										var itsDate = moment(reduce.start_date);
										if (todayPhp.isSameOrBefore(itsDate)) {
											if (date === null)
												return (itsDate);
											if (itsDate.isBefore(date))
												return (itsDate);
										}
										return (date);
									}, null);
									var data = {
										events: db_subevent.map(function(__event, i) {
											var moment_start = moment(__event['start_date']);
											var moment_end = moment(__event['end_date']);
											if (typeof go !== 'undefined' && moment_start.isSame(go, 'day') && start_at_slide < 0) {
												start_at_slide = i;
											} else if (filterNearestDate !== null) {
												if (moment_start.isSame(filterNearestDate)) {
													start_at_slide = i;
												}
											}
											var map = {
												start_date: {
													year: moment_start.year(),
													month: moment_start.month() + 1,
													day: moment_start.date(),
													hour: moment_start.hour(),
													minute: moment_start.minute()
												},
												end_date: {
													year: moment_end.year(),
													month: moment_end.month() + 1,
													day: moment_end.date(),
													hour: moment_end.hour(),
													minute: moment_end.minute()
												},
												text: {
													headline: __event['name'],
													text: `<blockquote>${__event['short_description'].replace(/\n/g, "<br>")}</blockquote> ${dataHelper.markdownToHtml(__event['full_description'])}`
												},
												media: {
													url: __event['profile'],
													thumbnail: __event['profile']
												},
												background: {}
											};
											if (__event['banner'].length) {
												map['background']['url'] = __event['banner'];
											} else {
												map['background']['color'] = `<?= $blockCms['primaryColor'] ?>`;
											}

											return map;
										})
									};
									var options = {
										script_path: `${baseUrl}/plugins/knightlab_timeline3/js/`,
										language: 'fr',
										font: 'bevan-pontanosans',
										theme: 'contrast',
										scale_factor: 0.5,
										timenav_position: 'top'
									}

									if (start_at_slide >= 0)
										options.start_at_slide = start_at_slide;
									container_dom.empty().html('');
									timeline_knightlab = new TL.Timeline('timeline-embed<?= $blockKey ?>', data, options);
								}
								resolve();
							})
							.catch(reject);
					}
				});
			});
		}

		function load_fullcalendar(__option = {}) {
			var go;
			if (typeof __option.goto !== 'undefined') {
				go = __option.goto;
				delete __option.goto;
			}
			var calendar_dom = $('#event_calendar<?= $blockKey ?>');
			coInterface.showLoader(calendar_dom, trad.currentlyloading);
			return new Promise(function(resolve) {
				xhr_event_context().then(function(parent_data) {
					mylog.log("Coevent program", parent_data);
					var start_moment_parent = moment(parent_data['start_date']);
					var end_moment_parent = moment(parent_data['end_date']);
					var default_view = '';
					var selectedView = sessionStorage.getItem(dataHelper.printf('{{block}}-calendarview', {
						block: '<?= $blockKey ?>'
					}));
					switch (true) {
						case typeof selectedView === 'string':
							default_view = selectedView;
							break;
						case start_moment_parent.format('YYYY-MM-DD') == end_moment_parent.format('YYYY-MM-DD'):
							default_view = 'agendaDay';
							break;
						case end_moment_parent.diff(start_moment_parent, 'days') <= 6 && start_moment_parent.day() == 1:
							default_view = 'agendaWeek';
							break;
						default:
							default_view = 'month';
							break
					}
					calendar_dom.empty().html('');
					calendar_dom.fullCalendar('destroy');
					calendar_dom.fullCalendar({
						contentHeight: 'auto',
						lang: 'fr',
						timeFormat: 'H(:mm)',
						defaultView: default_view,
						selectable: true,
						header: {
							left: 'prev,next today',
							center: 'title',
							right: 'month,agendaWeek,agendaDay'
						},
						height: 600,
						events: [],
						viewRender: view => {
							mylog.log("Rinelfi render", view);
							sessionStorage.setItem(dataHelper.printf('{{block}}-calendarview', {
								block: '<?= $blockKey ?>'
							}), view.name);
							var request_arg = $.extend({
								date: {
									between : {
										start : view.start.format(),
										end : view.end.format(),
									}
								}
							}, __option);
							xhr_subevents(request_arg).then(function(__events){
								var gotoCalendarDate;
								var todayPhp = moment('<?= $todayPhp ?>');
								var events = __events.map(function(__event){
									var eventDate = moment(__event['start_date'])
									if (todayPhp.isAfter(eventDate, 'day'))
										gotoCalendarDate = eventDate.clone();
									return {
										id : __event['id'],
										title : __event['name'],
										start : eventDate.format('YYYY-MM-DD HH:mm'),
										end : moment(__event['end_date']).format('YYYY-MM-DD HH:mm'),
										className : ['lbh-preview-element'],
										url : `#page.type.events.id.${__event['id']}`,
										backgroundColor : `<?= $blockCms['primaryColor'] ?> !important`
									}
								});
								calendar_dom.fullCalendar('removeEvents');
								calendar_dom.fullCalendar('addEventSource', events);
							});
						},
						eventMouseover: () => {
							coInterface.bindLBHLinks();
						},
						select: (start, end) => {
							coevent_create_event({
								start: start,
								end: end,
								afterLoad: "afterLoad"
							});
						}
					});
					if (typeof go !== 'undefined') {
						calendar_dom.fullCalendar('gotoDate', go);
					}
					// else if (gotoCalendarDate) {
					// 	if (gotoCalendarDate.isBefore(start_moment_parent, 'day'))
					// 		calendar_dom.fullCalendar('gotoDate', start_moment_parent.format('YYYY-MM-DD HH:mm'));
					// 	else
					// 		calendar_dom.fullCalendar('gotoDate', gotoCalendarDate.format('YYYY-MM-DD HH:mm'));
					// }
					resolve();
				});
			});
		}

		function reset__journee_carousel() {
			return carousel({
				container: $('#journee-carousel<?= $blockKey ?>'),
				autoplay: false,
				navigation: {
					indicator: false,
					button: {
						show: false,
						next: '<span class="fa fa-chevron-right"></span>',
						prev: '<span class="fa fa-chevron-left"></span>'
					}
				},
				delay: 2000
			});
		}

		function coevent_create_event(args) {
			// dyFObj.unloggedMode = true;
			if (typeof defaultCoevent === 'string') {
				dyFObj.openForm('event', 'afterLoad');
				return ;
			}
			var default_args = {
				start: null,
				end: null,
				afterLoad: null
			};
			args = $.extend({}, default_args, args);
			xhr_event_context().then(function(parent_data) {
				mylog.log("Coevent program", parent_data);
				var moment_start, moment_end;
				var parent_start, parent_end;
				parent_start = moment(parent_data.start_date);
				parent_end = moment(parent_data.end_date);
				if (typeof args.start === 'undefined' || args.start === null)
					moment_start = moment();
				else
					moment_start = args.start;
				if (typeof args.end === 'undefined' || args.end === null)
					moment_end = moment_start.clone().add(1, 'hours');
				else if (args.end.format('HH:mm') === '00:00')
					moment_end = args.end.subtract(1, 'minutes');
				else
					moment_end = args.end;
				// create parent limit
				var parent_start_date_dom = $("#startDateParent"),
					parent_end_date_dom = $("#endDateParent");
				if (!parent_start_date_dom.length) {
					parent_start_date_dom = $('<input>')
						.attr('id', 'startDateParent')
						.val(parent_start.format('YYYY-MM-DD HH:mm'))
						.prop('hidden', true);
					$(document.body).append(parent_start_date_dom);
				}
				if (!parent_end_date_dom.length) {
					parent_end_date_dom = $('<input>')
						.attr('id', 'endDateParent')
						.val(parent_end.format('YYYY-MM-DD HH:mm'))
						.prop('hidden', true);
					$(document.body).append(parent_end_date_dom);
				}
				parent_start_date_dom.val(parent_start.format('YYYY-MM-DD HH:mm'));
				parent_end_date_dom.val(parent_end.format('YYYY-MM-DD HH:mm'));
				var eventDynformDefaultParams = {
					onload: {
						actions: {
							hide: {
								parentfinder: 1,
								publiccheckboxSimple: 1,
								recurrencycheckbox: 1,
								organizerNametext: 1
							}
						}
					},
					beforeBuild: {
						properties: {
							type: {
								options: eventTypes
							}
						}
					},
					afterBuild: function() {
						$(".finder-organizer").find("a").html("Rechercher et sélectionner ma structure");
					},
					afterSave: function(data) {
						var callbackParams = {
							parent: {},
							links: {
								attendees: {}
							}
						};
						callbackParams.parent[W.coevent_parent.id] = {
							type: W.coevent_parent.type
						};
						callbackParams.links.attendees[userId] = {
							type: 'citoyens'
						};

						var params = {
							id: data['id'],
							collection: data['map']['collection'],
							path: 'allToRoot',
							value: callbackParams
						};
						link_parent = {
							id: W.coevent_parent.id,
							collection: W.coevent_parent.type,
							path: 'links.' + (W.coevent_parent.type === model.EVENT ? 'subEvents' : 'events') + '.' + data.id,
							value: {
								type: "events"
							}
						};
						dataHelper.path2Value(link_parent, function() {});
						links.connectAjax(W.coevent_parent.type, W.coevent_parent.id, data.id, "events", 'subEvents', null, null);
						dyFObj.commonAfterSave(null, function() {
							dataHelper.path2Value(params, function() {
								$('.coevent-program').trigger('coevent-filter');
								$('.theme-selector').each(function() {
									var self = $(this);
									self.trigger('block-update');
								});
							});
						});
					}
				};
				var eventDynformParams = typeof window.defaultCoevent === 'object' ? window.defaultCoevent : eventDynformDefaultParams;
				eventDynformParams = $.extend({}, eventDynformDefaultParams, eventDynformParams);
				var presentValuesEvent = {
					startDate: moment_start.format('DD/MM/YYYY HH:mm'),
					endDate: moment_end.format('DD/MM/YYYY HH:mm')
				};

				if ($('#event-content<?= $blockKey ?> .event-hour').length) {
					// var selected_date = moment($('#event-content<?= $blockKey ?> .dinalternate.date').text(), 'DD.MM.YYYY');
					// selected_date.hour(parseInt($('.event-hour:last').data('last-hour')));
					var last_schedule = $('#event-content<?= $blockKey ?> .event-hour:last').data('last-schedule');
					// presentValuesEvent.startDate = selected_date.format('DD/MM/YYYY HH:mm');
					presentValuesEvent.startDate = last_schedule;
					presentValuesEvent.endDate = moment(last_schedule, 'DD/MM/YYYY HH:mm').add(1, 'hours').format('DD/MM/YYYY HH:mm');
				}
				presentValuesEvent.parent = {};
				presentValuesEvent.parent[parent_data.id] = {
					_id: {
						$id: parent_data.id
					},
					name: parent_data.name,
					type: W.coevent_parent.type,
					profilThumbImageUrl: co2AssetPath + '/images/thumb/default_events.png'
				};
				dyFObj.openForm('event', args.afterLoad, presentValuesEvent, null, eventDynformParams);
				return false;
			});
		}

		if (typeof window.ce_prog_download !== 'function')
			window.ce_prog_download = function(filename) {
				var nearest_moment = moment(nearestDate, "DD.MM.YYYY");
				var url = baseUrl + '/costum/coevent/getpdfaction?id=' + W.coevent_parent.id;
				url += '&type=' + W.coevent_parent.type;
				url += '&costum_slug=' + costum.slug;
				url += '&costum_page=' + location.hash.match(/\#[\w\-]+/)[0].substr(1);
				const filter = get_session_data();
				if (nearest_moment.isValid())
					url += '&startDate=' + nearest_moment.toISOString();
				var post = {
					url: location.protocol + '//' + location.host + location.pathname + location.hash,
				};
				post = $.extend({}, post, filter);
				post = {
					method: 'post',
					header: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify(post)
				};
				fetch(url, post).then(function(resp) {
					resp.blob().then(function(blob) {
						var url = URL.createObjectURL(blob);
						var a_dom = document.createElement('a');
						a_dom.href = url;
						a_dom.download = filename;
						a_dom.addEventListener('click', function() {
							setTimeout(function() {
								a_dom.remove();
								URL.revokeObjectURL(url);
							}, 200);
						});
						$(document.body).append(a_dom);
						a_dom.click();
					});
				});
				return false;
			}

		function get_actors(__types = ['animator', 'organizer', 'links.attendees', 'creator', 'links.creator', 'links.organizer', 'organizerName']) {
			var url = baseUrl + '/costum/coevent/get_events/request/actors/id/' + W.coevent_parent.id + '/type/' + W.coevent_parent.type;
			var parameter = {
				types: __types
			}
			return new Promise(function(__resolve, reject) {
				ajaxPost(null, url, parameter, __resolve, reject)
			});
		}

		function load_programs(__date = '', __element = null, options = {}) {
			var container = $('#event-content<?= $blockKey ?>');
			container.addClass('empty');
			coInterface.showLoader(container, trad.currentlyloading);
			return new Promise(function(__resolve) {
				// 'links.organizer',
				if (!__date) {
					container.empty().html('<p class="no-events">Aucun événement à afficher</p>');
					__resolve(null);
					return;
				}
				var criteria = ['organizer', 'organizerName', 'animator'];
				get_actors(criteria).then(function(communities) {
					var subevent_criteria = $.extend({}, options);
					subevent_criteria.date = __date || 'begining';
					xhr_subevents(subevent_criteria).then(function(db_subevents) {
						var html = '';
						var last_hour = '';
						var last_schedule = '';

						function has_role(community, subevent) {
							return (typeof community === 'object' &&
								typeof community.roles === 'object' &&
								typeof community.roles[subevent.id] === 'object' &&
								typeof community.roles[subevent.id].role === 'string');
						}

						function is_animator(community, subevent) {
							return (community.roles[subevent.id].role === 'animator');
						}
						$.each(db_subevents, function(i, db_subevent) {
							var organizers = communities.filter(community => has_role(community, db_subevent) && !is_animator(community, db_subevent)).map(community => {
								return {
									id: community['id'],
									type: community['type'],
									name: community['name'],
									image: community['image']
								}
							});
							var animators = communities.filter(community => has_role(community, db_subevent) && is_animator(community, db_subevent)).map(community => {
								return {
									name: community['name'],
									image: community['image']
								}
							});

							var moment_start = moment(db_subevent['start_date']);
							var moment_end = moment(db_subevent['end_date']);

							if (i === 0)
								html += `<span class="dinalternate date">${moment_start.format('DD.MM.YYYY')}</span>`;
							if (moment_start.format('HH:mm') !== last_hour) {
								last_hour = moment_start.format('HH:mm');
								last_schedule = (moment_start.format('mm') == "00") ? moment_start.format('HH') + "h" : moment_start.format('HH') + "h" + moment_start.format('mm');
								if (i !== 0)
									html += '</ul>';
								html += `<ul class="event-detail"><span class="event-hour" data-last-schedule="${moment_end.format('DD/MM/YYYY HH:mm')}">${last_schedule}</span>`;
							}
							var actors_with_id = [];
							if (userConnected)
								actors_with_id = db_subevent['communities'].filter(community => community === userConnected['_id']['$id']);

							var isFollowed = false;
							if (userId != "" && typeof db_subevent.links.attendees != 'undefined' && typeof db_subevent.links.attendees[userId] != "undefined")
								isFollowed = true;
							var isShared = false;
							var tip = "S'inscrire";
							var actionConnect = 'follow';
							var icon = 'user-plus';
							var urlVisio = (typeof db_subevent.url != 'undefined') ? db_subevent.url : "";
							var tipVisio = "Rejoindre la visio-conférence";
							var labelVisio = "Rejoindre";
							var attendeeObj = {};
							if (typeof db_subevent.links.attendees != 'undefined') {
								$.each(db_subevent.links.attendees, function(idAtt, valAtt) {
									attendeeObj[idAtt] = {
										type: valAtt
									}
								});
							}

							var attendees = (typeof db_subevent.links.attendees != 'undefined') ? JSON.stringify(db_subevent.links.attendees) : {};
							var nbAttendees = (typeof db_subevent.links.attendees != 'undefined') ? Object.keys(db_subevent.links.attendees).length : 0;
							var expectedAttendees = (typeof db_subevent.expectedAttendees != "undefined" && parseInt(db_subevent.expectedAttendees) != NaN) ? parseInt(db_subevent.expectedAttendees) : 0;
							var classBtn = '';
							if (isFollowed) {
								actionConnect = 'unfollow';
								icon = 'unlink';
								classBtn = 'bg-white-comment text-dark';
								tip = "Je ne veux plus être inscrit.e";
							}

							var btnLabel = (actionConnect == "follow") ? "S'inscrire" : "Déjà inscrit.e";
							var btnVisio = (urlVisio !== ``) ? `<button class="btn btn-default btn-sm tooltips btn-participate" onclick="window.open('${urlVisio}')" data-toggle="tooltip" data-placement="left" data-original-title="${tipVisio}"><i class="fa fa-link"></i>
                                        ${labelVisio}
                                    </button><br>` : ``;

							var thumbAttendeesStr = '';
							if (nbAttendees > 0) {
								ajaxPost('#thumbAttendee' + db_subevent['id'], baseUrl + '/' + moduleId + '/default/render?url=/./pod/listItems', {
										"title": "",
										"links": attendeeObj,
										"connectType": "attendees",
										"number": 12,
										"titleClass": "col-xs-12 title text-gray",
										"heightWidth": 50,
										"containerClass": "text-center no-padding margin-top-10 margin-bottom-10",
										"contextId": W.coevent_parent.id,
										"contextType": W.coevent_parent.type,
										"eventClass": "lbh-preview-element"
									},
									function(data) {
										thumbAttendeesStr = data;
									}, null, null, {
										async: false
									});
							}
							var address_str;
							if (db_subevent.address) {
								var keys = ['streetAddress', 'addressLocality'];
								address_str = '';
								$.each(keys, function(i, key) {
									if (typeof db_subevent.address[key] === 'string')
										address_str += db_subevent.address[key] + ' ';
								})
							}
							html += `<li>
                                    <div class="line-indicator"><span class="point-holder"></span></div>
                                    <a href="#page.type.events.id.${db_subevent['id']}" class="lbh-preview-element" style="color: <?= $blockCms['primaryColor'] ?>">${db_subevent['name']}</a>
                                    <button class="btn btn-default btn-sm tooltips btn-participate ${classBtn}" data-program="${db_subevent['id']}" onclick="event_participation(this)"
                                    data-toggle="tooltip" data-placement="left" data-original-title="${tip}" data-expected-attendees="${expectedAttendees}" data-attendees='${attendees}'
                                    data-ownerlink="${actionConnect}" data-id="${db_subevent['id']}" data-type="events" data-name="${db_subevent['name']}" data
                                    data-isFollowed="${isFollowed}"><i class="fa fa-${icon}"></i> ${btnLabel}</button>
                                    ${btnVisio}
                                    <p class="rolesLabel">${animators.length == 0 ? `` : `Animé par : ${animators.map(function (map) {
                                return `<span class="actorNames"> ${map['name']}</span></p>`;
                            }).join('')}`}
						${dataHelper.printf('<div class="rolesLabel">{{organizer_str}}</div>', {
							organizer_str: organizers.length === 0 ? '' : dataHelper.printf('Organizateur{{s}} : {{organizer_join}}', {
								s: organizers.length > 1 ? 's' : '',
								organizer_join: organizers.map(function (organizer) {
                                return dataHelper.printf('<div class="actorNames">{{define_img}}</div>', {
									define_img: dataHelper.printf('<div class="img-container" title="{{title}}"><a href="#page.type.{{type}}.id.{{id}}" class="lbh-preview-element"><img src="{{src}}" /></a></div>', {
										src: baseUrl + organizer.image,
										title: organizer.name.replace(/"/g, "'"),
										type: organizer.type,
										id: organizer.id
									})
								});
                            }).join('')
							})
						})}
                                    <div style="display:flex;">
                                        <p class="rolesLabel attendeesCounter">Nombre d'inscrits : <span>${nbAttendees}</span>
                                        <div id="thumbAttendee${db_subevent['id']}" class="attendeeThumb" style="display:none;margin-top:-20px;padding-left: 20px;">${thumbAttendeesStr}</div>
                                    </div>
                                    <p class="descr-event">
                                    ${address_str ? `Lieu : ${address_str}<br>` : ''}
                                    ${typeof db_subevent['short_description'] !== 'undefined' ? db_subevent['short_description'].replace(/\n/g, "<br>") : ''}<br>
                                    </p>
                                </li>`;
						});

						var items = $('#journee-caroussel<?= $blockKey ?> #items<?= $blockKey ?> .item');
						items.removeClass('active');

						if (items.length && db_subevents.length) {
							date = moment(db_subevents[0]['start_date']).format('DD.MM.YYYY');
							if (__element) {
								__element.classList.add('active');
							} else {
								items.each(function() {
									if ($(this).data('target') === date) {
										$(this).addClass('active');
									}
								});
							}
						}
						html += '';
						container.removeClass('empty').html(html);
						coInterface.bindLBHLinks();
						$('.img-container').tooltip();
						__resolve();
					});
				});
			})
		}

		function load_journees(__option = {}) {
			return new Promise(function(__resolve) {
				request_get_subevent_dates().then(function(dates) {
					var df_date_container_dom = $('.df-container[data-id="' + php.blockKey + '"] .df-dates');
					df_date_container_dom.empty().html('');

					function buildHtml(dates) {
						var change = 0;
						var last_group = '';
						dates.forEach(function(__date) {
							if (last_group !== __date['date_group']) {
								change++;
								last_group = __date['date_group'];
								df_date_container_dom.append(template_date_filter({
									date: last_group,
									month: __date['year'] + '<br>' + trad[__date['month'].toLowerCase()].toUpperCase(),
									order: __date['date'],
									day: trad[__date['day'].toLowerCase()].toUpperCase()
								}));
							}
						});
					}
					if (dates.fromNow && dates.fromNow.length)
						buildHtml(dates.fromNow);
					else if (dates.before && dates.before.length)
						buildHtml(dates.before);
					(function() {
						var last_width = $(window).width();

						function is_hidden(date_dom) {
							var parent_dom = date_dom.parent();
							var parent_width = parent_dom.outerWidth(true);
							var offset = (date_dom.offset().left - parent_dom.offset().left) + date_dom.outerWidth(true);
							offset = parseInt(offset);
							return (offset > parent_width);
						}

						function do_update_navigation() {
							$('.df-container').each(function() {
								var container_dom = $(this);
								var date_list_dom = container_dom.find('.df-date');
								var navigation_dom = container_dom.find('.df-navigation');
								navigation_dom.hide();
								date_list_dom.each(function() {
									if (is_hidden($(this))) {
										navigation_dom.css('display', '');
										return (false);
									}
								})
							});
						}
						setInterval(function() {
							if (last_width !== $(window).width()) {
								last_width = $(window).width();
								do_update_navigation();
							}
						});
						do_update_navigation();
					})();
					if (dates.fromNow.length > 0)
						__resolve(moment(dates.fromNow[0].date_group, 'DD.MM.YYYY'));
					else if (dates.before.length)
						__resolve(moment(dates.before[dates.before.length - 1].date_group, 'DD.MM.YYYY'));
					else
						__resolve(null);
				});
			});
		}

		function change_views(element) {
			var jq_element_dom = $(element);
			$('#program-container<?= $blockKey ?> .journee_views button').removeClass('active');
			$('.coevent-program-viewer').removeClass('show');
			jq_element_dom.addClass('active');
			$(jq_element_dom.data('target')).addClass('show');
			switch ($('#program-container' + php.blockKey + ' .program-view').index(jq_element_dom)) {
				case 0:
					load_journees().then(function(nearest) {
						var params = {};
						if (nearest)
							params.goto = nearest;
						load_fullcalendar(params).then(function() {
							coInterface.bindLBHLinks();
						});
					});
					break;
				case 1:
					load_journees().then(function(nearest) {
						if (nearest)
							$('.df-container[data-id="' + php.blockKey + '"] .df-date[data-target="' + nearest.format('DD.MM.YYYY') + '"]').trigger('click');
						else
							load_programs().then();
						coInterface.bindLBHLinks();
					});
					break;
				case 2:
					load_journees().then(function(nearest) {
						params = {};
						if (nearest)
							params.goto = nearest;
						load_timeline(params).then(function() {
							coInterface.bindLBHLinks();
						})
					});
					break;
			}
		}

		function load_views() {
			var params, self, target_date, this_moment, journees_dom, nearest_incoming_event, today, format_string;
			return new Promise(function(__resolve) {
				load_journees().then(function(nearest) {
					params = {};
					if (nearest) {
						$('.df-container[data-id="' + php.blockKey + '"] .df-date[data-target="' + nearest.format('DD.MM.YYYY') + '"]').trigger('click');
						params.goto = nearest;
					} else
						load_programs().then();
					load_timeline(params).then(function() {
						load_fullcalendar(params).then(function() {
							coInterface.bindLBHLinks();
							__resolve();
						});
					});
				});
			});
		}

		function toggleEventFilter() {
			var filter = get_session_data();
			var self = $(this);

			// enable typology and event filters on view
			$('a.event-type-filter').each(function() {
				var self = $(this);
				self.removeClass('active');
				if (filter.type === self.attr('href'))
					self.addClass('active');
			});
			$('.event-types .event-type').each(function() {
				var self = $(this);
				self.removeClass('filter-active');
				if (filter.type === self.attr('data-target'))
					self.addClass('filter-active');
			});

			if (filter.type !== null && filter.type.length > 0) {
				self.css({
					'display': '',
					top: `${mainNavDom.outerHeight()}px`
				});
			} else {
				self.fadeOut(800);
				if (programContainerDom.length)
					$('.coevent-program').trigger('coevent-filter');
			}
		}


		$(function() {
			var ce_map;

			function do_open_map(map) {
				return new Promise(function(resolve, reject) {
					var map_option;
					if (costum.editMode)
						reject('edit mode');
					else
						$('#ce-prog-map-container' + blockKey).css({
							height: ($(window).height() - $('#mainNav').outerHeight(true)) + 'px',
							width: $('#all-block-container').outerWidth(true),
							top: $('#mainNav').outerHeight(true) + 'px'
						}).show(200, function() {
							if (typeof map === 'undefined') {
								map_option = {
									container: '#ce-prog-map-container' + blockKey + ' .map-content',
									activePopUp: true,
									mapOpt: {
										btnHide: false,
										doubleClick: true,
										scrollWheelZoom: false,
										zoom: 2,
									},
									mapCustom: {
										getPopup: function(data) {
											var l_filter_address = function(key) {
												return (typeof data.address[key] === 'string');
											};
											var l_map_address = function(key) {
												return (data.address[key]);
											};
											var l_html_tag = function(tag) {
												return ('<span class="label label-default"><i class="fa fa-hashtag"></i> ' + tag + '</span>');
											};
											var date = {
												start: moment(data.start_date),
												end: moment(data.end_date)
											};
											var popup_option = {
												profile: '',
												banner_html: '',
												tags: '',
												address: ['streetAddress', 'addressLocality'].filter(l_filter_address).map(l_map_address).join(' '),
												title: data.name,
												id: data.id
											};

											if (date.start.format('YYYY-MM-DD') === date.end.format('YYYY-MM-DD'))
												popup_option.date = dataHelper.printf('<i class="fa fa-calendar"></i> {{date}} <i class="fa fa-clock-o" aria-hidden="true"></i> {{start}} - {{end}}', {
													date: date.start.format('DD.MM.YYYY'),
													start: date.start.format('HH:mm'),
													end: date.end.format('HH:mm')
												});
											else
												popup_option.date = dataHelper.printf('<i class="fa fa-calendar"></i> {{start_date}} <i class="fa fa-clock-o" aria-hidden="true"></i> {{start_hour}} <br><i class="fa fa-calendar"></i> {{end_date}} <i class="fa fa-clock-o" aria-hidden="true"></i> {{end_hour}}', {
													start_date: date.start.format('DD.MM.YYYY'),
													end_date: date.end.format('DD.MM.YYYY'),
													start_hour: date.start.format('HH:mm'),
													end_hour: date.end.format('HH:mm')
												});
											if (data.banner)
												popup_option.banner_html = '<div class="ce-map-banner"><img src="' + (baseUrl + data.banner) + '" /></div>';
											if (data.profile)
												popup_option.profile = '<div class="ce-map-profile"><img src="' + (baseUrl + data.profile) + '" /></div>';
											if (data.tags.length > 0)
												popup_option.tags = dataHelper.printf('<div style="margin-bottom: 5px">{{tags}}</div>', {
													tags: data.tags.map(l_html_tag).join(' ')
												})
											return (dataHelper.printf(
												'<div>' +
												'	{{banner_html}}' +
												'	<span><i class="fa fa-map-marker"></i> {{address}}</span><br>' +
												'	<span>{{date}}</span><br>' +
												'	{{tags}}' +
												'	<div class="ce-map-title">{{profile}}{{title}}</div>' +
												'	<a href="#page.type.events.id.{{id}}" class="lbh-preview-element undefined item_map_list popup-marker" id="popup{{id}}"><div class="btn btn-sm btn-more col-md-12"><i class="fa fa-hand-pointer-o"></i>En savoir plus</div></a>' +
												'</div>', popup_option
											));
										},
										icon: {
											getIcon: function(data) {
												var option = {
													iconSize: [45, 55],
													iconAnchor: [25, 45],
													popupAnchor: [-3, -30],
													shadowUrl: '',
													shadowSize: [68, 95],
													shadowAnchor: [22, 94]
												};

												if (data.elt.marker)
													option.iconUrl = baseUrl + data.elt.marker;
												else
													option.iconUrl = modules.map.assets + '/images/markers/event-marker-default.png'
												return (L.icon(option));
											}
										}
									},
									elts: {}
								}
								map = new CoMap(map_option);
							}
							resolve(map);
						});
				})
			}
			$('#program-container<?= $blockKey ?>').on('coeventfilter', function() {
				load_views();
			});
			$('#program-container<?= $blockKey ?> .program-view:not(.no-view)').each(function(_, element) {
				$(this).on('click', function() {
					change_views(this);
				})
			});
			$('.program-view[data-target="#map-view' + blockKey + '"]').on('click', function() {
				do_open_map(ce_map).then(function(map) {
					ce_map = map;
					xhr_subevents({
						fromToday: true
					}).then(function(db_subevent_list) {
						var data = {};
						db_subevent_list.forEach(function(event) {
							if (typeof event.address === 'object' && typeof event.geo === 'object' && typeof event.geoPosition === 'object')
								data[event.id] = event;
						});
						map.clearMap();
						map.addElts(data);
					})
				});
			});
			$('.filter-btn-hide-map').on('click', function() {
				$('#ce-prog-map-container' + blockKey).hide(200);
			});
			$('.df-container .df-dates').on('click', '.df-date', function() {
				var self = $(this);
				if (self.hasClass('active'))
					return;
				self.parent().find('.df-date').removeClass('active');
				self.addClass('active');
				load_programs(self.data('target'));
			});
			$('#journee-navigation<?= $blockKey ?> .journee-control.right').on('click', function() {
				if (_journee_carousel) {
					_journee_carousel.gotoNext(function() {
						reload_program_event();
						var active = $('#event-content<?= $blockKey ?> .dinalternate.date').text();
						$('[data-target="' + active + '"]').addClass('active');
					});
				}
			});
			$('#journee-navigation<?= $blockKey ?> .journee-control.left').on('click', function() {
				if (_journee_carousel) {
					_journee_carousel.gotoPrevious(function() {
						reload_program_event();
						var active = $('#event-content<?= $blockKey ?> .dinalternate.date').text();
						$('[data-target="' + active + '"]').addClass('active');
					});
				}
			});
			$('#event-proposition<?= $blockKey ?>, .ce-map-create-event').on('click', function(__e) {
				__e.preventDefault();
				coevent_create_event({
					afterLoad: "afterLoad"
				});
			});
			$('#event-download<?= $blockKey ?>').on('click', function(__e) {
				__e.preventDefault();
				xhr_event_context().then(function(parent_data) {
					mylog.log("Coevent program", parent_data);
					window.ce_prog_download(parent_data.name + '.pdf');
				});
			});
			$('.active_filter_container').on('active-filter', toggleEventFilter).on('disable-filter', toggleEventFilter);
			$('.coevent-program').on('coevent-filter', function() {
				load_filter_view()
				load_views();
			});
			$(dataHelper.printf('#chronology_container{{blockKey}}', {
				blockKey: blockKey
			})).on('click', '.event-filter', function() {
				var self = $(this);
				var state = self.data('state');
				sessionStorage.setItem(dataHelper.printf('{{block}}-showAllEvents', {
					block: blockKey
				}), state);
				updateFilterEventPeriodChronology();
				load_timeline(get_session_data());
			});
			updateFilterEventPeriodChronology();
			var page_config = typeof costum.app['#' + page] === 'object' && costum.app['#' + page] ? costum.app['#' + page] : {};
			page_config = typeof page_config.coeventConfig === 'object' && page_config.coeventConfig ? page_config.coeventConfig : null;
			if (notNull(page_config) && typeof page_config.regionFilter != "undefined" && page_config.regionFilter) {
				var filter = get_session_data();
				filter.regions = page_config.regionFilter;
				set_session_data(filter);
				load_filter_view();
			}
			xhr_event_context().then(function(parent_data) {
				mylog.log("Coevent program", parent_data);
				load_filter_view();
				var start_moment_parent = moment(parent_data['start_date']);
				var end_moment_parent = moment(parent_data['end_date']);
				if (end_moment_parent.diff(start_moment_parent, 'days') > 6)
					change_views($('#program-container' + php.blockKey + ' .program-view').eq(1));
				else
					change_views($('#program-container' + php.blockKey + ' .program-view').eq(0));
			});
			if (page_config && page_config.showMapFirst)
				do_open_map(ce_map).then(function(map) {
					ce_map = map;
					xhr_subevents({
						fromToday: true
					}).then(function(db_subevent_list) {
						var data = {};
						db_subevent_list.forEach(function(event) {
							if (typeof event.address === 'object' && typeof event.geo === 'object' && typeof event.geoPosition === 'object')
								data[event.id] = event;
						});
						map.clearMap();
						map.addElts(data);
					})
				});
		});
		if (pageMenu == "#programme") {
			$(".filter-btn-hide-map").click()
		}
	})(jQuery, window);
</script>