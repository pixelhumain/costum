<?php
/**
 * TPLS QUI PERMET D'AFFICHER UN CAROUSEL SUR LES ÉVÈNEMENTS EN COURS DU JOUR + 30
 * BASÉ SUR LE MODELE FILIÈRE NUMÉRIQUE
 * POSSIBILITÉ DE PARAMATRÉ LES LIMITATIONS, DE BASE ELLE SERA À 3
 * @params : 
 *  $limit : limitation concernant le nombre d'affichage d'évènement
 *  $background : définir la couleur du fond
 *  $color : définir la couleur du texte
 */
$keyTpl = "eventcarousel";

$paramsData = [ 
    "title"         => "Évenement en carousel",
    "icon"          =>  "",
    "color"         => "#000000",
    "background"    => "#FFFFFF",
    "txtcolor"      => "#000000",
    "limit"         => 3
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style>
    .event_<?= $kunik?> .resume-infoc{
        text-align: left;
        z-index: 10;
        position: absolute;
        padding-top: 5%;
        padding-left: 3%;
        background-color:
        rgba(0,0,0,0.7);
        width: 40%;
        padding-bottom: 100%;
    }
    .event_<?= $kunik?> #arrow-caroussel{
        margin-top: -5%;
        font-size: 3vw;
        color:white;
        position: absolute;
        width: 40%;
        text-align: center;
        z-index: 100;
        margin-left: 9%;
    }
    .event_<?= $kunik?> #arrow-caroussel a{
        color:white;
    }
    .event_<?= $kunik?> .carousel-inner{
        height : 350px;
    }
    .event_<?= $kunik?> .h2-day{
        font-size: 3vw;
    }
    @media (max-width:768px){
        .event_<?= $kunik?> .carousel-inner{
            height : 200px;
        }
        .event_<?= $kunik?> #arrow-caroussel{
            font-size: 3vw;
            color:white;
        }
        .event_<?= $kunik?> a span {
            font-size: 20px !important;
        }
        .event_<?= $kunik?>  span {
            font-size: 18px !important;
        }
        .event_<?= $kunik?> h1{
            font-size: 20px;
        }
    }
    .event_<?= $kunik?> h1{
        color:<?= $paramsData["color"] ?>;
        background-color: <?= $paramsData["background"] ?>;
    }
    .event_<?= $kunik?> a span {
        font-size: 40px;
        color : <?= $paramsData["txtcolor"] ?> ;
    }
    .event_<?= $kunik?>  span {
        font-size: 25px;
        color : <?= $paramsData["txtcolor"] ?>
    }
    .event_<?= $kunik?> .btn-edit-delete{
        display: none;
        top: 50%;
        left: 50%;
        z-index: 1000;
    }
    .event_<?= $kunik?>:hover  .btn-edit-delete {
         display: block;
        -webkit-transition: all 0.9s ease-in-out 9s;
        -moz-transition: all 0.9s ease-in-out 9s;
        transition: all 0.9s ease-in-out 0.9s;
        position: absolute;
        top:50%;
        left: 50%;
        transform: translate(-50%,-50%);  
    }
</style>
<div class="event_<?= $kunik?>">
    <h1 class="text-center title">
        <i class="fa <?= $paramsData['icon'] ?>"></i> 
        <?= $paramsData["title"] ?> 
    </h1>
    <div class="text-center btn-edit-delete">
        <?php 
           
            if(Authorisation::isInterfaceAdmin()){ ?>
                <button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('event')">
                    <?php echo Yii::t('cms', 'Add an event')?>
                </button>            
        <?php } ?>
    </div>

    <div id="carousel"  class="carousel slide" data-ride="carousel">
        <?php
        
        // A crée dans un modèle a part ce code
        date_default_timezone_set('UTC');

        //On récupère sur ce mois-ci les évènements
        $date_array = getdate ();
        $numdays = $date_array["wday"];
        $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
        $startDate = strtotime(date("Y-m-d H:i"));

        // Préparation de la requête
        $where = array("source.key"     => $costum["contextSlug"]);
        // "startDate"      => array('$gte' => new MongoDate($startDate)));

        if (!empty($filters)) {
            $where["thematique"] = $filters;
        }

        $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where, $paramsData["limit"]);
        if($allEvent){
            ?>
            <div class="carousel-inner">
               <?php 
               end($allEvent);
               $lk = key($allEvent);
               foreach($allEvent as $k => $v){
                $bannerImg = (isset($v["profilMediumImageUrl"])) ? "/ph".$v["profilMediumImageUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/templateCostum/no-banner.jpg";
                $date = date(DateTime::ISO8601, $v["startDate"]->sec);
                ?>
                <div class="<?php if($k === $lk) echo "item active"; else echo "item"; ?>">
                   <div class="resume-infoc col-xs-3 col-sm-3">
                    <a class="lbh-preview-element" href="#page.type.<?= $v["collection"]?>.id.<?= (String) $v["_id"] ?>">
                        <span> 
                            <?= $v["name"]; ?> 
                        </span>
                    </a> 
                    <br>
                    <span> Le  <?= date("d/m/Y" , strtotime($date)); ?> </span>
                </div>


                <img src="<?= $bannerImg; ?>"  alt="<?= $v["name"]; ?>" style="width:100%">
                <!-- ARROW -->
            </div>
        <?php } ?>
    </div>
    <?php if(count($allEvent) > 1) { ?>
        <div id="arrow-caroussel">
            <a href="#carousel" data-slide="next"> <rect class="cls-3" x="73.74" width="78.36" height="54.9"/>
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="#carousel" data-slide="prev"> <rect class="cls-3" x="73.74" width="78.36" height="54.9"/>
                <i class="fa fa-arrow-right"></i>
            </a>
        <div id="caroussel" class="carousel-control col-xs-6 col-sm-6 col-md-6">
     </div>
 </div> 
<?php }
}
?>
</div>
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    title : {
                        label : "<?php echo Yii::t('cms', 'Title')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                    },
                    icon : { 
                        label : "<?php echo Yii::t('cms', 'Icon')?>",
                        inputType : "select",
                        options : {
                            "fa-newspaper-o"    : "Newspapper",
                            "fa-calendar " : "Calendar",
                            "fa-lightbulb-o "  :"Lightbulb"
                        },
                        values : sectionDyf.<?php echo $kunik ?>ParamsData.icon
                    },
                    color : {
                        label : "<?php echo Yii::t('cms', 'Title color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
                    },
                    background : {
                        label : "<?php echo Yii::t('cms', 'Background color of the title')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
                    },
                    txtcolor : {
                        label : "<?php echo Yii::t('cms', 'Text color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.txtcolor
                    },
                    limit : {
                        label : "<?php echo Yii::t('cms', 'Number of results')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.limit
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      mylog.log("activeForm save tplCtx",tplCtx);
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        //   urlCtrl.loadByHash(location.hash);
                        });
                      } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

    });
</script>