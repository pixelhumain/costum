<?php 
$paramsData = [ 
  "title" => "AGENDA",
  "title2" => "1",
  "elType" => "events",
  "elPoiType" => "",
  "elDesign" => "1",
  "imgShow" => true,
  "imgWidth" => "352px",
  "imgHeight"  => "352px",
  "imgRound" => "0%",
  "imgBg" => "#f0ad16",
  "infoBg" => "#f0ad16",
  "imgBorder" => "",
  "imgBorderColor" => "",
  "infoDistanceFromImage" => "-14px",
  "infoMarginLeft" => "0px",
  "infoMarginRight" => "0px",
  "infoDateFormat" => "1",
  "infoOrder" => true,
  "infoAlign" => "center",
  "infoCenter" => true,
  "infoBorderRadius" => "0%",
  "infoHeight" => "auto",
  "cardBg" => "transparent",
  "cardBoxShadow" => false,
  "cardAutoPlay" => true,
  "cardNumberPhone" => 1,
  "cardNumberTablet" => 2,
  "cardNumberDesktop" => 3,
  "cardSpaceBetween" => 10,
  "cardNbResult" => 6,
  "cardBtnShowMore" => true,
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) 
            $paramsData[$e] = $blockCms[$e];
    }
} 
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
$addType = $paramsData["elType"];
if($paramsData["elType"] == "events" || $paramsData["elType"] == "organizations" /*substr($addType, -1) == "s"*/)
    $addType = substr($addType, 0, -1);
?>
<style>
  .block-container-<?= $kunik ?> .btn-container{display: none;}
  .block-container-<?= $kunik ?>:hover .btn-container{
    display: block;
    position: absolute;
    top: 10px;
    left: 50%;
    transform: translateX(-50%);
    z-index: 9;
  }

</style>
<p class="text-center btn-container">
  <button class="btn btn-primary btn-sm add hiddenPreview" onclick="dyFObj.openForm('<?= $addType ?>',null,null,null,elObj<?= $kunik ?>)"></button>
</p>

<script>
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    var addType = "<?= $addType ?>";
    var contextData = <?php echo json_encode($el); ?>;
    contextData.id = costum.contextId;

    var elObj<?= $kunik ?> = {
      afterSave : function(data){
       dyFObj.commonAfterSave(data, function(){
          urlCtrl.loadByHash(location.hash);
        });
      }
    };

  $(function(){
   $(".block-container-<?= $kunik ?> button.add").text(exists(trad.add<?= $paramsData["elType"] ?>) ? trad.add<?= $paramsData["elType"] ?> : trad.add<?= $addType ?> );

        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
              "icon" : "fa-cog",
            "properties" : {
                "title" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Title')?>",
                },
                "title2" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Sous titre')?>",
                },
                "elType" : {
                    "inputType" : "select",
                      "label" : "<?php echo Yii::t('cms', 'Type of element')?>",
                      "options" : elTypeOptions,
                },
                "elPoiType" : {
                      "inputType" : "select",
                      "label" : "<?php echo Yii::t('cms', 'Type of')?> "+trad.poi,
                      "options" : poiOptions,
                },
                "elDesign" : {
                  "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Display mode')?>",
                    "options" : {
                      "1" : "<?php echo Yii::t('cms', 'Configurable multi-card carousel')?>",
                      "2" : "<?php echo Yii::t('cms', 'One card horizontal carousel')?>",
                      "3" : "<?php echo Yii::t('cms', 'No configurable multi-card carousel')?>",
                      "4" : "<?php echo Yii::t('cms', 'Carousel multi-card of communecter')?>",
                      "5" : "<?php echo Yii::t('cms', 'Non Carousel multi-carte de communecter')?>",
                      "6" : "<?php echo Yii::t('cms', 'No Carousel to a horizontal card')?>",
                    }       
                },
                "imgShow" : {
                    "label": "<?php echo Yii::t('cms', 'Show image')?>",
                    "inputType" : "checkboxSimple",
                    "params" : checkboxSimpleParams,
                    "checked" : <?= json_encode($paramsData["imgShow"]) ?>
                },
                "imgWidth" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Image width (in px)')?>",
                    "placeholder" : "359px"
                },
                "imgHeight" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Height of the image')?>",
                    "placeholder" : "359px"
                },
                "imgRound" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Round image (% or px)')?>",
                    "placeholder" : "50%"
                },
                "imgBg" :{
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Image background')?>",
                  },
                "imgBorder" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Border thickness')?> (px)",
                    "placeholder" : "5px"
                },
                "imgBorderColor" :{
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Border color')?>",
                  },
                "cardBoxShadow" : {
                  "label": "<?php echo Yii::t('cms', 'Ombre sur la cart')?>",
                    "inputType" : "checkboxSimple",
                    "params" : checkboxSimpleParams,
                    "checked" : <?= json_encode($paramsData["cardBoxShadow"]) ?>
                },
                <?php if($paramsData["elDesign"] != 3){ ?>
                "cardAutoPlay" : { 
                  "label": "<?php echo Yii::t('cms', 'Automatic slide')?>",
                    "inputType" : "checkboxSimple",
                    "params" : checkboxSimpleParams,
                    "checked" : <?= json_encode($paramsData["cardAutoPlay"]) ?>
                },
                <?php } ?>
                "cardBg" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Card background')?>",
                },
                "cardNumberPhone" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Number of the card on the phone')?>",
                    rules : {
                      number:true,
                      max:2,
                      min:0,
                    },
                },
                "cardNumberTablet" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Number of the card on tablet')?>",
                    rules : {
                      number:true,
                      max:3,
                      min:0,
                    },
                },
                "cardNumberDesktop" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Number of the card on desktop')?>",
                    rules : {
                      number:true,
                      max:10,
                      min:0,
                    },
                },
                "cardNbResult" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Number of results')?>",
                    rules : {
                      number:true,
                      max:100,
                      min:0,
                    },
                },
                "cardBtnShowMore" : { 
                  "label": "<?php echo Yii::t('cms', 'See more button')?>",
                    "inputType" : "checkboxSimple",
                    "params" : checkboxSimpleParams,
                    "checked" : <?= json_encode($paramsData["cardBtnShowMore"]) ?>
                },
                <?php if($paramsData["elDesign"] == 3){ ?>
                "cardSpaceBetween" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Space between the cards')?>",
                    rules : {
                      number:true,
                      max:100,
                      min:0,
                    },
                },           
                <?php } ?>
                "infoBg" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background information')?>",
                },
                <?php if($paramsData["elDesign"] == 1 || $paramsData["elDesign"] == 3){ ?>
                "infoDistanceFromImage" : {
                  "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Distance between info and image (ex: -150px)')?>",
                    "placeholder" : "-144px"
                },
                "infoMarginLeft" : {
                  "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Margin left')?>",
                    "placeholder" : "0px"
                },
                "infoMarginRight" : {
                  "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Margin right')?>",
                    "placeholder" : "0px"
                },
                "infoBorderRadius" : { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Border radius')?>",
                    "placeholder" : "0px"
                },
                <?php } ?>
                "infoHeight" : {
                  "label": "<?php echo Yii::t('cms', 'Height of the textual information')?>",
                    "inputType" : "text",
                    "placeholder" :"default : auto"
                },
                <?php if($paramsData["elDesign"] == 2){ ?>
                "infoAlign" : {
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Alignment')?>",
                    "options" : {
                      "flex-start" : "<?php echo Yii::t('cms', 'Left')?>",
                      "center" : "<?php echo Yii::t('cms', 'Center')?>",
                      "flex-end" : "<?php echo Yii::t('cms', 'Right')?>"
                    }
                },
                "infoOrder" : {
                    "inputType" : "checkboxSimple",
                    "label" : "<?php echo Yii::t('cms', 'Image on the left')?>",
                    "params" : checkboxSimpleParams,
                    "checked" : <?= json_encode($paramsData["infoOrder"]) ?>
                },
                "infoCenter" : {
                    "inputType" : "checkboxSimple",
                    "label" : "<?php echo Yii::t('cms', 'Center vertically')?>",
                    "params" : checkboxSimpleParams,
                    "checked" : <?= json_encode($paramsData["infoCenter"]) ?>           
                },
                <?php } ?>
                <?php if($paramsData["elType"] == "events"){ ?>
                "infoDateFormat" : {
                  "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Date format')?>",
                    "options" : {
                      "1" : "<?php echo Yii::t('cms', 'Format')?> 1",
                      "2" : "<?php echo Yii::t('cms', 'Format')?> 2"
                    }       
                }
                <?php } ?>

            },
            beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            tplCtx.format = true;
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);

            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"title",6,6,null,null,"<?php echo Yii::t('cms', 'Title')?>","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"el",6,6,null,null,"<?php echo Yii::t('cms', 'Options')?>","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"img",4,6,null,null,"<?php echo Yii::t('cms', 'Image')?>","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"info",2,6,null,null,"<?php echo Yii::t('common', 'Information')?>","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"card",4,6,null,null,"<?php echo Yii::t('cms', 'Card')?>","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"cardNumber",4,6,null,null,"<?php echo Yii::t('cms', 'Number')?>","#000","");

            $('#elDesign').change(function(){
              var elDesign = $(this).val();
              tplCtx.value = {};
              tplCtx.value["elDesign"] = elDesign;
              dataHelper.path2Value(tplCtx, function(params) {
                toastr.success("Chargement . . .");
                  //urlCtrl.loadByHash(location.hash);
                  location.reload();
              });           
            });

            if($('#elType').val()=="poi")
                $('.elPoiTypeselect').fadeIn();
              else
                $('.elPoiTypeselect').hide();                


            $('#elType').change(function(){
              if($(this).val()=="poi")
                $('.elPoiTypeselect').fadeIn();
              else
                $('.elPoiTypeselect').fadeOut();
            })
        });

    <?php if($paramsData["elDesign"]==1 || $paramsData["elDesign"]==3 || $paramsData["elDesign"]==4 || $paramsData["elDesign"]== 5 ){ ?> 
      carouselObj.multiCart.init(sectionDyf.<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
    <?php } ?>
    <?php if($paramsData["elDesign"]==2 || $paramsData["elDesign"]==6){ ?> 
     carouselObj.cardBlock.init(sectionDyf.<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
    <?php } ?>    
  })
</script>