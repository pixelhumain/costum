  <?php 
      $keyTpl ="agendaSlide";
      $blockKey = (string)$blockCms["_id"];
      $paramsData = [
          "backgroundColor"=>"#97bf1e",
      ];

    if (isset($blockCms)) {
      foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
          $paramsData[$e] = $blockCms[$e];
        }
      }
    }
      if($costum["contextType"] && $costum["contextId"]){
        $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
    }
  ?>
  <style>
      .agenda{
          /* background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/filiere/fond/fond.png); */
          color : white; 
          margin-top :3%;
          background : <?php echo $paramsData["backgroundColor"] ?>;
      }
      .agenda-h1{
          margin-left :12%;
          left: 4%;
          margin-top: 2%;
      }
      .agenda-carousel{
          margin-top : -3%;
          background: white;
          width: 85%;
  	}
  	
  	
  </style>

  <!-- Agenda --> 
  <div class="content-<?= $kunik?>">
  	<div class="agenda row">
  		<div class="agenda-h1 col-xs-6 col-sm-6">
  			<h1>
  				<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/filiere/svg/agenda.svg" class="img-responsive" style="width:5%; margin-right: 2%;"> Agenda <br>
  			</h1>
  			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/filiere/svg/trait-blanc.svg" class="img-responsive" style="margin-top: -3%; padding-right: 64%;">
  			<p style="margin-top: -1.5%;">Évènement à venir</p>
  		</div>
  		<div class="event col-xs-12 col-sm-12">
  			<div id="event-affiche"></div>
  		</div>
  		<div class="col-xs-12 col-sm-12" style="text-align : center;">
  			<a href="javascript:;" data-hash="#agenda" class="lbh-menu-app " style="text-decoration : none; font-size:2rem;">
  				<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/filiere/svg/plus.svg" class="img-responsive plus-m" style="margin: 1%;width:5%;">
  				<br><?php echo Yii::t('cms', 'See more')?> <br>	<?php echo Yii::t('cms', 'news')?>
  			</a>
  		</div>
  	</div>

  	
  	<!-- Carousel --> 
  	<div class="agenda-carousel">
  		<div class="col-xs-12 col-sm-12 col-md-12 no-padding carousel-border" style="margin-top : -7.5%;">
  			<div id="docCarousel" class="carousel slide" data-ride="carousel">
  				<div class="carousel-inner itemctr"></div>
  				<div id="arrow-caroussel">
  					<!-- Début svg --> 
  					<svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.1 54.9" style="width:35%;">
  						<defs><style>.cls-3{fill:none;}.cls-2{fill:#fff;}</style></defs>
  						<title>fleche</title>
  						<a href="#docCarousel" data-slide="next"> <rect class="cls-3" x="73.74" width="78.36" height="54.9"/><polygon class="cls-2" points="87.57 43.75 119.64 43.75 119.64 54.9 147.08 27.45 119.64 0 119.64 11.14 87.57 11.14 87.57 43.75"/></a>
  						<a href="#docCarousel" data-slide="prev"><rect class="cls-3" x="131.87" y="340.95" width="78.36" height="54.9" transform="translate(210.23 395.84) rotate(180)"/><polygon class="cls-2" points="64.53 11.14 32.46 11.14 32.46 0 5.01 27.45 32.46 54.9 32.46 43.75 64.53 43.75 64.53 11.14"/></a>
  					</svg>
  					<!-- Fin svg --> 
  					<div id="caroussel" class="carousel-control col-xs-6 col-sm-6 col-md-6"></div>
  				</div>
  				<!-- <a class="left carousel-control" href="#docCarousel" data-slide="prev">
  				<span class="glyphicon glyphicon-chevron-left"></span>
  				<span class="sr-only">Previous</span>
  				</a>
  				<a class="right carousel-control" href="#docCarousel" data-slide="next">
  				<span class="glyphicon glyphicon-chevron-right"></span>
  				<span class="sr-only">Next</span>
  				</a> -->
  			</div>
  		</div>
  	</div>
  </div>

  <script type="text/javascript">
      sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
      jQuery(document).ready(function() {
        afficheEvent();
          sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your agenda.')?>",
                "description" : "<?php echo Yii::t('cms', 'Personalize your agenda')?>",
                "icon" : "fa-cog",
              
              "properties" : {
                  "backgroundColor" : {
                      "inputType" : "colorpicker",
                      "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                      value :  sectionDyf.<?php echo $kunik ?>ParamsData.backgroundColor
                  }
              },
              beforeBuild : function(){
                  uploadObj.set("cms","<?php echo $blockKey ?>");
              },
              save : function (data) {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                  if (k == "parent")
                    tplCtx.value[k] = formData.parent;

                  if(k == "items")
                    tplCtx.value[k] = data.items;
                    mylog.log("andrana",data.items)
                });
                console.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                      dyFObj.commonAfterSave(params,function(){
                        toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                      });
                    } );
                }

              }
            }
          };

          $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          });
      });

      function afficheEvent(){
      ajaxPost(
        null,
        baseUrl+"/costum//geteventaction",
        {},
        function(data){ 
          var str = "";
          var ctr = "";
          var itr = "";
          var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
          var url_event = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>" + "/images/<?php echo $el['slug'] ?>/default_event.jpg";
          var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";
          
          var i = 1;
          if(data.result == true){
            $(data.element).each(function(key,value){
              
              var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;
              
              
              var imgMedium = (value.imgMedium != "none") ?  baseUrl+value.imgMedium : url_event;
              //var imgMedium = (value.imgMedium != "none") ?  baseUrl+value.imgMedium : url;
              var img = (value.img != "none") ? baseUrl+value.img : url;
              
              
              str += "<div class='col-md-4 card-mobile'>";
              str += "<div class='card'>";
              str += "<button class='lbh-preview-element' href='#@"+value.slug+"' data-hash='#page.type.events.id."+value.id+"' style='text-decoration : none; color:white;'>";
              str += "<div class='col-xs-12 col-sm-12 col-md-12 card-c'>";
              str += "<div class='col-xs-6 col-sm-6 col-md-6 card-info'>"+startDate+"<br>"+value.type;
              str += "</div></div>";
              str += "<img src='"+imgMedium+"' class='img-responsive img-event'>";
              str += "</button></div></div>";
              
              if(i == 1) ctr += "<div class='item active'>";
              else ctr += "<div class='item'>";
              
              ctr += "<div class='resume col-xs-3 col-sm-3 col-md-3'>";
              ctr += "<h2 class='h2-day'>"+startDate+"</h2><br>";
              ctr += "<h2>"+value.name+"</h2><br>";
              ctr += "<p>"+value.resume+"<p>";
              ctr += "<button class='btn btn-info lbh-preview-element' href='#@"+value.slug+"' data-hash='#page.type.events.id."+value.id+"'><i class='fa fa-info-circle'></i> <?php echo Yii::t('cms', 'More information')?> </button>";
              ctr += "</div>";
              
              ctr += "<img src='"+img+"' class='ctr-img img-responsive'>";
              ctr += "</div>";
              
              i++;
            });
            
            $(".itemctr").html(ctr);
            // $("#tclass").html(ctr);
            // $(".resume").html(resume);
            
            
            
          }
          else{
            str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' ><?php echo Yii::t('cms', 'Element')?>Aucun évènement n'est prévu</b></div>";
            $(".agenda-carousel").hide();
          }
          
          if(i != 2) $("#arrow-caroussel").show();
          else $("#arrow-caroussel").hide();
          
          $("#event-affiche").html(str);
        }
      ); 
    }
  </script>
  