<?php

use Liliumdev\ICalendar\ZCiCal;
use PixelHumain\PixelHumain\modules\co2\controllers\actions\agenda\CalendarAction;

$keyTpl = 'generic_agenda';
$kunik = $kunik ?? sha1(date('c'));
$blockKey = $blockKey ?? '';
$connectedUser = Yii::app()->session['userId'] ?? '';
$this->costum = !empty($this->costum) ? $this->costum : $this->context->config['costum'];
$context = PHDB::findOneById($this->costum['contextType'], $this->costum['contextId'], ['links', 'agendas']);

$readonly_content = $readonly_content ?? false;
$is_admin = Authorisation::isInterfaceAdmin();
$organizationMembers = [];
if (!empty($context['links']['members'])) {
	foreach ($context['links']['members'] as $id => $member) {
		if (!empty($member['type']) && $member['type'] === Organization::COLLECTION) $organizationMembers[] = $id;
	}
}
$view_table = [
	'calendar'            => 'Calendrier',
	'vertical_timeline'   => 'Timeline vertical',
	'horizontal_timeline' => 'Timeline horizontal',
	'gantt'               => 'Gantt',
];

$show_el = [
	'participateBtn'     => 'Bouton pour participer',
	'allParticipantList' => 'Bouton qui affiche la liste des participants',
];

$cms_data = [
	'content_type'           => $content_type ?? 'events',
	'allow_external_sources' => false,
	'width'                  => true,
	'available_views'        => [
		'calendar',
		'vertical_timeline',
		'horizontal_timeline',
		'gantt',
	],
	'show'                   => [],
];

if (isset($blockCms)) {
	foreach ($cms_data as $e => $v) {
		if (isset($blockCms[$e])) {
			$cms_data[$e] = $blockCms[$e];
		}
	}
}

$openagendas = [];
$icalendars = [];

if ($cms_data['allow_external_sources'] && $cms_data['content_type'] === 'events') {
	// traitement de l'openagenda
	$urls = $context['agendas']['openagenda']['urls'] ?? [];
	$api_key = $context['agendas']['openagenda']['api_key'] ?? '';
	if (!empty($api_key)) {
		$response = CacheHelper::get("openagenda$api_key");
		if (!$response) {
			$request = curl_init("https://api.openagenda.com/v2/me/agendas?key=$api_key");
			// Tester l'url distant
			curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
			$response = json_decode(curl_exec($request), true);
			curl_close($request);
			CacheHelper::set("openagenda$api_key", $response);
		}

		// Url valide
		if (!empty($response['success'])) {
			if (!empty($urls)) {
				$slugs = [];
				foreach ($urls as $url) {
					$exploded = explode('/', $url);
					$domain_index = array_search('openagenda.com', $exploded);
					$slug = $domain_index === false ? $exploded[0] : $exploded[$domain_index + 1];
					$domain_index = array_search(Yii::app()->language, $exploded);
					$domain_index = $domain_index === false ? array_search('openagenda.com', $exploded) : $domain_index;
					$slug = $domain_index === false ? $exploded[0] : explode('?', $exploded[$domain_index + 1])[0];

					$public_agenda = CacheHelper::get("openagenda$api_key$slug");
					if (!$public_agenda) $slugs[] = $slug;
					else $response['items'][] = $public_agenda;
				}
				$slugs = array_unique($slugs);
				if (count($slugs) > 0) {
					$url = 'https://api.openagenda.com/v2/agendas?key=' . $api_key . '&slug[]=' . implode('&slug[]=', $slugs);
					$public = curl_init($url);
					curl_setopt($public, CURLOPT_RETURNTRANSFER, true);
					$public_agendas = json_decode(curl_exec($public), true);
					curl_close($public);
					$response['items'] = array_merge($response['items'], $public_agendas['agendas']);
					foreach ($public_agendas['agendas'] as $public_agenda) {
						CacheHelper::set("openagenda$api_key" . $public_agenda['slug'], $public_agenda);
					}
				}
			}
			foreach ($response['items'] as $item) {
				$color = CalendarAction::get_random_color();
				$openagenda = [
					'value'  => 'openagenda' . $item['uid'],
					'name'   => $item['title'],
					'color'  => $color,
					'count'  => 0,
					'info'   => '',
					'events' => [],
				];
				$url = 'https://api.openagenda.com/v2/agendas/' . $item['uid'] . '/events?key=' . $api_key . '&longDescriptionFormat=HTML&includeFields[]=description&includeFields[]=title&includeFields[]=timings.0&includeFields[]=slug&includeFields[]=status&includeFields[]=longDescription&includeFields[]=uid&includeFields[]=image&size=';
				// recevoir la liste des events de l'agenda en question
				$request = curl_init($url . '0');
				curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
				$events = json_decode(curl_exec($request), true);
				curl_close($request);

				if (!empty($events['success'])) {
					$total = $events['total'];
					$request = curl_init($url . $total);
					$events = CacheHelper::get("events$url$total");
					if (!$events) {
						curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
						$events = json_decode(curl_exec($request), true);
						curl_close($request);
						CacheHelper::set("events$url$total", $events);
					}

					$openagenda['count'] = count($events['events']);
					foreach ($events['events'] as $event) {
						$openagenda['events'][] = [
							'id'               => $event['uid'],
							'parent'           => 'openagenda' . $item['uid'],
							'group'            => $item['title'],
							'slug'             => $event['slug'],
							'name'             => $event['title'][Yii::app()->language],
							'shortDescription' => $event['description'][Yii::app()->language] ?? '',
							'description'      => $event['longDescription'][Yii::app()->language] ?? '',
							'startDate'        => $event['lastTiming']['begin'],
							'endDate'          => $event['lastTiming']['end'],
							'profilImageUrl'   => !empty($event['image']['base']) && !empty($event['image']['filename']) ? $event['image']['base'] . $event['image']['filename'] : '',
							'url'              => 'https://openagenda.com/' . $item['slug'] . '/events/' . $event['slug'] . '?lang=' . Yii::app()->language,
							'color'            => $color,
						];
					}
				}
				$openagendas[] = $openagenda;
			}
		}
	}

	// traitement de l'ical
	if (!empty($context['agendas']['icalendars'])) {
		$db_agendas = $context['agendas']['icalendars'];
		foreach ($db_agendas as $db_agenda) {
			$icalendar = $db_agenda;
			$color = CalendarAction::get_random_color();
			$icalendar['color'] = $color;
			$icalendar['value'] = 'ical' . strtolower(str_replace(' ', '-', $icalendar['name']));
			$icalendar['events'] = [];

			if (!empty($icalendar['url'])) {
				$response = file_get_contents($icalendar['url']);

				if ($response !== false) {
					$ical_object = new ZCiCal($response);
					$timezone = date_default_timezone_get();
					foreach ($ical_object->tree->child as $node) {
						if (empty($node)) continue;
						if ($node->getName() === 'VTIMEZONE') {
							foreach ($node->data as $key => $value) {
								if ($key == 'TZID') {
									$timezone = $value->getValues();
									break;
								}
							}
							break;
						} else if ($node->getName() === 'VEVENT') {
							foreach ($node->data as $key => $value) {
								if ($key == 'TZID') {
									$timezone = $value->getValues();
									break;
								}
							}
							break;
						}
					}
					foreach ($ical_object->tree->child as $node) {
						if (empty($node)) continue;
						if ($node->getName() === 'VEVENT') {
							$event = [
								'parent'         => $icalendar['value'],
								'group'          => $icalendar['name'],
								'description'    => '',
								'profilImageUrl' => '',
								'color'          => $color,
							];
							foreach ($node->data as $key => $value) {
								switch ($key) {
									case 'SUMMARY':
										$event['name'] = $value->getValues();
										$event['slug'] = str_replace(' ', '', ucwords($event['name']));
										break;
									case 'URL':
										$event['url'] = $value->getValues();
										break;
									case 'UID':
										$event['id'] = $value->getValues();
										break;
									case 'DESCRIPTION':
										$event['shortDescription'] = nl2br($value->getValues());
										break;
									case 'DTEND':
										$end_datetime = new DateTime();
										if (!empty($value->parameter['value']) && $value->parameter['value'] === 'DATE') $end_datetime->setTimeZone(new DateTimeZone('UTC'));
										else $end_datetime->setTimezone(new DateTimeZone($timezone));
										$end_datetime->setTimestamp(strtotime($value->getValues()));
										$event['endDate'] = $end_datetime->format('c');
										break;
									case 'DTSTART':
										$start_datetime = new DateTime();
										if (!empty($value->parameter['value']) && $value->parameter['value'] === 'DATE') $start_datetime->setTimeZone(new DateTimeZone('UTC'));
										else $start_datetime->setTimezone(new DateTimeZone($timezone));
										$start_datetime->setTimestamp(strtotime($value->getValues()));
										$event['startDate'] = $start_datetime->format('c');
										break;
								}
							}
							$icalendar['events'][] = $event;
						}
					}
				}
			}

			$icalendar['count'] = count($icalendar['events']);
			$icalendars[] = $icalendar;
		}
	}
}

$costum = $this->costum;
$show_filter = isset($show_filter) ? $show_filter : in_array($cms_data['content_type'], ['events', "event_actions"]) || $costum['contextType'] === 'organizations' && $cms_data['content_type'] === 'projects';

$scripts_and_css = [
	'/plugins/jquery-timeline/jquery.timeline.min.css',
	'/plugins/dhtmlxgantt/dhtmlxgantt.css',
	'/plugins/jquery-timeline/jquery.timeline.min.js',
	'/plugins/jquery-confirm/jquery-confirm.min.css',
	'/plugins/jquery-confirm/jquery-confirm.min.js',
	'/plugins/select2/select2.css',
	'/plugins/select2/select2.min.js',
	'/plugins/dhtmlxgantt/dhtmlxgantt.js',
	'/plugins/knightlab_timeline3/css/timeline.css',
	'/plugins/knightlab_timeline3/js/timeline.js',
	'/plugins/vertical-timeline/css/vertical-timeline.css',
	'/plugins/vertical-timeline/js/vertical-timeline.js',
	'/plugins/vertical-timeline/js/vertical-timeline.js',
	'/css/rinelfi/modal.css',
	'/js/rinelfi/modal.js',
];
HtmlHelper::registerCssAndScriptsFiles($scripts_and_css, Yii::app()->request->baseUrl);
$scripts_and_css = [
	'/js/forms/conditional-forms.js',
	'/js/genericagenda/generic_agenda.js',
];
HtmlHelper::registerCssAndScriptsFiles($scripts_and_css, Yii::app()->getModule('costum')->getAssetsUrl());
?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Oxygen:wght@300;400;700&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
<style>
	.timeline::before {
		all: initial;
		all: unset;
	}

	#gantt-title {
		margin-top: 25px;
	}

	#cpl<?= $blockKey ?> {
		display: flex;
		flex-direction: column;
		max-height: 388px;
		overflow-y: auto;
		gap: 10px;
	}

	.cpl,
	#cpl-select<?= $blockKey ?> {
		padding: 9px 10px;
		display: flex;
		align-items: center;
		gap: 5px;
		font-family: Inter, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Open Sans", "Helvetica Neue";
	}

	.cpl {
		border-radius: 4px;
		border: hsla(0, 0%, 82%, .9) 1px solid;
	}

	#cpl-search<?= $blockKey ?> {
		position: relative;
	}

	#cpl-search<?= "$blockKey " ?>>input,
	#cpl-search<?= "$blockKey " ?>>input:focus {
		border: 1px solid #cacaca;
		border-radius: 20px;
		line-height: 1.8rem;
		padding: 10px 10px 10px 35px;
		width: 100%;
		outline: none;
	}

	#cpl-search<?= "$blockKey " ?>>.fa {
		position: absolute;
		top: 50%;
		left: 10px;
		transform: translateY(-50%);
		color: #cacaca;
		font-size: 2rem;
	}

	.cpl-text {
		flex: 1;
		-moz-user-select: none;
		-webkit-user-select: none;
		user-select: none;
		cursor: default;
		display: block;
		margin-bottom: 0;
		font-weight: 500;
		font-style: normal;
	}

	.card {
		display: flex;
		flex-direction: column;
		border: 1px solid #cfcfcf;
		border-radius: 10px;
	}

	.card>div:first-child {
		border-radius: 10px 10px 0 0;
	}

	.card>div:last-child {
		border-radius: 0 0 10px 10px;
	}

	.card-header {
		padding: 10px;
		margin-bottom: 0;
		background-color: #f7f7f7;
		border-bottom: 1px solid rgba(0, 0, 0, .125);
	}

	.card-body {
		padding: 10px;
		background-color: #fff;
	}

	.gantt_tree_content,
	.cpl-text {
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}

	.cpl-actions {
		display: flex;
		flex-direction: row;
		position: relative;
	}

	.checkmark {
		display: block;
		height: 25px;
		width: 25px;
		background-color: #fff;
		margin-bottom: 0;
	}

	.checkmark:after,
	.checkmark:before {
		content: "";
		position: absolute;
		display: none;
	}

	/* Show the checkmark when checked */
	input:checked~.checkmark:after {
		display: block;
	}

	input:indeterminate~.checkmark:before {
		display: block;
	}

	/* Style the checkmark/indicator */
	.checkmark:after {
		left: 10px;
		top: 7px;
		width: 5px;
		height: 10px;
		border: solid white;
		border-width: 0 3px 3px 0;
		-webkit-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		transform: rotate(45deg);
	}

	.checkmark:before {
		left: 50%;
		top: 50%;
		width: 15px;
		height: 1px;
		border: solid white;
		border-width: 0 0 3px 0;
		-webkit-transform: translateX(-50%) translateY(50%);
		-ms-transform: translateX(-50%) translateY(50%);
		transform: translateX(-50%) translateY(50%);
	}

	.checkmark.default:after,
	.checkmark.default:before {
		border-color: #000;
	}

	.cpl-check {
		position: absolute;
		opacity: 0;
		cursor: pointer;
		height: 0;
		width: 0;
	}

	.cpl-check~.checkmark {
		border: 3px solid #000;
	}

	.cpl-check:checked~.checkmark {
		background-color: #fff;
	}

	#custom_tooltip<?= $blockKey ?> {
		display: none;
		position: fixed;
		border: 1px solid #2c2c2c;
		background-color: #2c2c2c;
		padding: 5px 15px;
		border-radius: 10px;
		color: white;
		font-size: 1.2rem;
		font-weight: bold;
		z-index: 11;
	}

	#custom_tooltip<?= "$blockKey " ?>.show {
		display: block;
	}

	.modal {
		z-index: 999999;
		font-family: Raleway, Fallback, sans-serif;
	}

	/* vertical timeline */

	#vertical_timeline {
		flex: 1;
	}

	#detail_contribution_indicator {
		color: #ffc900;
		display: block;
		text-align: center;
		gap: 5px;
	}

	.new_task_form_container {
		display: flex;
		flex-direction: row;
		align-items: center;
		gap: 10px;
	}

	.new_task_form_container>input {
		border: none;
		outline: none;
		flex: 1;
		font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
		font-weight: normal;
	}

	.new_task_form_container>button {
		border: none;
		outline: none;
		background: transparent;
	}

	.vertical_timeline_content .title {
		text-transform: inherit !important;
		font-weight: initial !important;
		font-size: medium !important;
		line-height: 1.9rem !important;
	}

	#generic_agenda<?= $blockKey ?> {
		margin: 20px auto;
		padding-left: 30px;
		padding-right: 30px;
		position: relative;
		user-select: none;
	}

	#generic_agenda<?= "$blockKey " ?>.nav-tabs>li.active>a:focus {
		color: #555;
		cursor: default;
		background-color: #fff;
		border: 1px solid #ddd;
		border-bottom-color: transparent;
	}

	#generic_agenda<?= "$blockKey " ?>.tab-content {
		border-left: 1px solid #ddd;
		border-bottom: 1px solid #ddd;
		border-right: 1px solid #ddd;
		border-radius: 0 0 3px 3px;
		padding: 10px;
	}

	#horizontal_timeline_container<?= $blockKey ?> {
		width: 100%;
		height: 600px;
	}

	.agenda-viewer {
		display: flex;
		flex-direction: row;
		gap: 20px;
		align-items: flex-start;
	}

	.agenda-viewer>.filter {
		min-width: 300px;
		width: 300px;
	}

	.agenda-viewer>.content {
		width: 100%;
	}

	#gantt_container<?= $blockKey ?> {
		min-height: 600px;
	}

	.fc-scroller.fc-day-grid-container {
		height: auto !important;
	}

	#filter-xs<?= $blockKey ?> {
		display: none;
		position: absolute;
		z-index: 50;
	}

	#cpl_container<?= $blockKey ?> {
		background-color: white;
	}

	#cpl_container<?= "$blockKey " ?>.card-header {
		display: flex;
		align-items: center;
		gap: 10px;
	}

	#cpl_container<?= "$blockKey " ?>.card-header .card-title {
		flex: 1;
	}

	#cpl_container<?= "$blockKey " ?>.card-header .card-action {
		display: none;
	}

	#card-overlay-modal<?= $blockKey ?> {
		display: none;
		position: fixed;
		top: 0;
		left: 0;
		width: 100vw;
		height: 100vh;
		z-index: 999990;
		background-color: rgba(0, 0, 0, .5);
	}

	@media screen and (max-width: 1024px) {
		#cpl_container<?= $blockKey ?> {
			display: none;
			overflow-y: auto;
			z-index: 999990;
		}

		#cpl_container<?= "$blockKey " ?>.card-header .card-action {
			display: block;
		}

		#filter-xs<?= $blockKey ?> {
			display: block;
		}

		.agenda-viewer>.content {
			margin-top: 50px;
		}
	}

	.source-management {
		list-style: none;
		border-bottom: 1px solid #cfcfcf;
	}

	.source-management-item {
		padding: 5px 0;
	}

	.source-management-item:not(.source-management-item:last-child) {
		border-bottom: 1px solid #cfcfcf;
	}

	.source-management-item a:hover {
		text-decoration: none;
	}

	.action-panels {
		margin: 5px 5px 0;
	}
</style>
<div id="action-preview<?= $blockKey ?>"></div>
<div class="rin-modal rin-closeable" id="event-detail">
	<div class="rin-modal-dialog">
		<div class="rin-modal-header">
			<div class="rin-modal-actions">
				<a class="rin-modal-action lbh-preview-element" href="#" target="_blank" data-action="preview">
					<span class="fa fa-eye"></span>
				</a>
				<div class="rin-modal-action rin-modal-close">
					<span class="fa fa-times"></span>
				</div>
			</div>
		</div>
		<div class="rin-modal-body">
			<div style="display: flex; flex-direction: row; align-items: flex-start; margin: 5px 5px 0; gap: 5px;">
				<span style="width: 30px; height: 30px;border-radius: 3px;" id="event-color<?= $blockKey ?>"></span>
				<span style="flex: 1;">
					<span style="font-size: 30px;line-height: 1;" id="event-name<?= $blockKey ?>"></span><br>
					<span id="event-date<?= $blockKey ?>"></span>
				</span>
			</div>
			<div style="display: flex; flex-direction: row; align-items: flex-start; margin: 5px 5px 0; gap: 5px;">
				<span style="font-size: 30px;" class="fa fa-calendar"></span>
				<span style="flex: 1;">
					<span style="font-size: 20px;" id="event-group<?= $blockKey ?>"></span><br>
					<span id="event-author<?= $blockKey ?>"></span>
				</span>
			</div>
			<div class="panel panel-default action-panels">
				<!-- Default panel contents -->
				<div class="panel-heading"><?= Yii::t('common', 'Activities') ?></div>
				<!-- List group -->
				<ul class="list-group">
					<li class="list-group-item">Cras justo odio</li>
				</ul>
			</div>
			<!-- Mahefa -->
			<!-- <a class="btn btn-default btn-link followBtn active" data-id="<?= $blockKey ?>" data-type=""> Participer </a> -->
			<div class="text-center all-btn-content">
				<?php if (in_array('participateBtn', $cms_data["show"])) { ?>
					<a href="javascript:;" class="btn btn-default btn-link participateUser" data-type="event">
						<i class="fa fa-link fa-rotate-270"></i>
						Participer
					</a>
				<?php }
				if (in_array('allParticipantList', $cms_data["show"])) { ?>
					<a class="btn btn-default btn-link attendeeList">
						<i class="fa fa-list"></i>
						Liste des participants
					</a>
				<?php } ?>
			</div>
			<div class="attendees-list"></div>
		</div>
	</div>
</div>
<div id="generic_agenda<?= $blockKey ?>" class="<?= filter_var($cms_data['width'], FILTER_VALIDATE_BOOLEAN) === true ? 'container-fluid' : 'container' ?>">
	<?php if ($show_filter) : ?>
		<div id="filter-xs<?= $blockKey ?>">
			<button type="button" class="btn btn-default">
				<span class="fa fa-filter"></span>
			</button>
		</div>
		<div class="agenda-viewer">
			<div id="card-overlay-modal<?= $blockKey ?>"></div>
			<div id="cpl_container<?= $blockKey ?>" class="card filter">
				<div class="card-header">
					<span class="card-title">
						<?php
						switch ($cms_data['content_type']) {
							case 'events':
								echo Yii::t('common', 'Events types');
								break;
							case "event_actions":
								echo Yii::t('common', 'Events');
								break;
							case 'projects':
								echo Yii::t('common', 'Projects list');
								break;
						}
						?>
					</span>
					<?php if ($is_admin && $cms_data['content_type'] === 'events') : ?>
						<button class="btn btn-default btn-sm" data-target="event_sources">
							<span class="fa fa-plus"></span>
						</button>
					<?php endif ?>
					<div class="card-action">
						<div class="fa fa-times"></div>
					</div>
				</div>
				<?php if ($cms_data['content_type'] === 'events') : ?>
					<ul class="card-body source-management" style="display: none">
						<?php if ($cms_data['allow_external_sources']) : ?>
							<li class="source-management-item">
								<a href="#openagenda"><span class="fa fa-calendar" aria-hidden="true"></span> OpenAgenda</a>
							</li>
							<li class="source-management-item">
								<a href="#icalendar"><span class="fa fa-calendar" aria-hidden="true"></span> iCalendar</a>
							</li>
						<?php endif; ?>
						<li class="source-management-item">
							<a href="#event">
								<span class="fa fa-calendar" aria-hidden="true"></span> <?= Yii::t('common', 'Create a new event') ?>
							</a>
						</li>
						<li class="source-management-item">
							<a href="#cointerevents">
								<span class="fa fa-calendar" aria-hidden="true"></span> <?= Yii::t('common', 'Internal events in Communecter') ?>
							</a>
						</li>
					</ul>
				<?php endif; ?>
				<div class="card-body co-scroll" id="cpl<?= $blockKey ?>">
					<div id="cpl-search<?= $blockKey ?>">
						<i class="fa fa-search"></i>
						<input type="search" placeholder="<?= Yii::t('common', 'Search') ?>">
					</div>
					<div id="cpl-select<?= $blockKey ?>">
						<div class="cpl-actions">
							<input class="cpl-check" type="checkbox" id="cpl_filter_select_all<?= $blockKey ?>" checked>
							<label for="cpl_filter_select_all<?= $blockKey ?>" class="checkmark default"></label>
						</div>
						<label for="cpl_filter_select_all<?= $blockKey ?>" class="cpl-text"><?= Yii::t('common', 'Select all') ?></label>
					</div>
				</div>
			</div>
			<div class="content">
			<?php endif ?>
			<ul class="nav nav-tabs" role="tablist" id="agenda_view_tabs<?= $blockKey ?>">
				<?php
				foreach ($cms_data['available_views'] as $view) : ?>
					<?php if (in_array($cms_data['content_type'], ['events', 'event_actions']) && $view !== 'gantt' || $cms_data['content_type'] === 'projects') : ?>
						<li><a href="#<?= $view ?><?= $blockKey ?>" role="tab"><?= $view_table[$view] ?></a></li>
					<?php endif ?>
				<?php endforeach ?>
				<?php if ($is_admin && !$readonly_content) : ?>
					<li><a href="#setting<?= $blockKey ?>" role="tab"><i class="fa fa-cog"></i> Paramètres</a></li>
				<?php endif ?>
			</ul>

			<div class="tab-content">
				<?php foreach ($cms_data['available_views'] as $view) : ?>
					<?php if (in_array($cms_data['content_type'], ['events', 'event_actions']) && $view !== 'gantt' || $cms_data['content_type'] === 'projects') : ?>
						<div class="tab-pane load" id="<?= $view ?><?= $blockKey ?>">
							<div id="<?= $view ?>_container<?= $blockKey ?>"></div>
							<?php if ($view === 'gantt') : ?>
								<small style="display: inline-block; margin-top: 5px;">Si vous avez du mal à naviguer horizontalement essayez
									la combinaison
									<button class="btn btn-primary">Shift</button>
									puis défiler avec la molette de votre souris.
								</small>
							<?php endif ?>
						</div>
					<?php endif ?>
				<?php endforeach ?>
				<?php if ($is_admin && !$readonly_content) : ?>
					<div class="tab-pane" id="setting<?= $blockKey ?>">
						<form role="form">
							<div class="form-group">
								<label>Largeur du bloc</label>
								<div class="radio">
									<label>
										<input type="radio" name="width" id="width_true<?= $blockKey ?>" value="true" <?= $cms_data['width'] === true ? 'checked' : '' ?>>
										Large
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="width" id="width_false<?= $blockKey ?>" value="false" <?= $cms_data['width'] === false ? 'checked' : '' ?>>
										Retrécie
									</label>
								</div>
							</div>
							<div class="form-group">
								<label>Type de contenu</label>
								<div class="radio">
									<label>
										<input type="radio" name="content_type" data-conditional-target="#source-manager-container" id="content_type_events<?= $blockKey ?>" value="events" <?= $cms_data['content_type'] === 'events' ? 'checked' : '' ?>>
										Evénement
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="content_type" data-conditional-target="#source-manager-container" id="content_type_projects<?= $blockKey ?>" value="projects" <?= $cms_data['content_type'] === 'projects' ? 'checked' : '' ?>>
										Projet
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="content_type" data-conditional-target="#source-manager-container" id="content_type_event_action<?= $blockKey ?>" value="event_actions" <?= $cms_data['content_type'] === 'event_actions' ? 'checked' : '' ?>>
										Action des événements
									</label>
								</div>
								<div class="checkbox" <?= $cms_data['content_type'] !== 'events' ? 'style="display: none;"' : '' ?> id="source-manager-container" data-show-condition="events">
									<label>
										<input type="checkbox" name="allow_external_sources" id="allow_external_sources<?= $blockKey ?>" <?= $cms_data['allow_external_sources'] ? 'checked' : '' ?>>
										Prendre en charge les sources extérieurs
									</label>
								</div>
							</div>
							<div class="form-group">
								<label>Liste des vues</label>
								<?php
								foreach ($view_table as $key => $view) { ?>
									<div class="checkbox" <?= $key === 'gantt' && $cms_data['content_type'] !== 'projects' ? 'style="display: none;"' : 'style' ?>>
										<label>
											<input type="checkbox" name="available_views" id="available_views_<?= $key ?><?= $blockKey ?>" value="<?= $key ?>" <?= !empty($cms_data['available_views']) && in_array($key, $cms_data['available_views']) ? ($key !== 'gantt' || $cms_data['content_type'] !== 'events' ? 'checked' : '') : '' ?>>
											<?= $view ?>
										</label>
									</div>
								<?php
								} ?>
							</div>
							<div class="form-group">
								<label>Afficher</label>
								<?php
								foreach ($show_el as $key => $view) { ?>
									<div class="checkbox" <?= $key === 'gantt' && $cms_data['content_type'] === 'events' ? 'style="display: none;"' : 'style' ?>>
										<label>
											<input type="checkbox" name="show" id="show_<?= $key ?><?= $blockKey ?>" value="<?= $key ?>" <?= !empty($cms_data['show']) && in_array($key, $cms_data['show']) ? ($key !== 'gantt' || $cms_data['content_type'] !== 'events' ? 'checked' : '') : '' ?>>
											<?= $view ?>
										</label>
									</div>
								<?php
								} ?>
							</div>
							<button type="submit" class="btn btn-primary"><?= Yii::t('common', 'Save') ?></button>
						</form>
					</div>
				<?php endif ?>
			</div>
			<?php if ($show_filter) : ?>
			</div>
		</div>
	<?php endif ?>
</div>
<div id="custom_tooltip<?= $blockKey ?>"></div>
<script>
	$(function() {
		var interval = setInterval(function() {
			if (typeof TL !== 'undefined') {
				clearInterval(interval);
				genericAgenda(gantt, jQuery, window, '<?= $blockKey ?>', '<?= $kunik ?>', {
					openagendaUrls: JSON.parse(JSON.stringify(<?= json_encode($context['agendas']['openagenda']['urls'] ?? []) ?>)),
					defaultFilters: JSON.parse(JSON.stringify(<?= json_encode($default_filter_keys ?? []) ?>)),
					color: JSON.parse(JSON.stringify(<?= json_encode($predefinedColor ?? null) ?>)),
					isCostumAap: typeof costum !== 'undefined' && typeof costum.type !== 'undefined' && costum.type === 'aap',
					storageVersion: 'v2',
					cmsData: JSON.parse('<?= json_encode($cms_data) ?>'),
					openagendaData: JSON.parse(JSON.stringify(<?= json_encode($openagendas) ?>)),
					icalendarData: JSON.parse(JSON.stringify(<?= json_encode($icalendars) ?>)),
					openagendaApiKey: JSON.parse(JSON.stringify(<?= json_encode($context['agendas']['openagenda']['api_key'] ?? '') ?>)),
					isAdmin: JSON.parse(JSON.stringify(<?= json_encode($is_admin) ?>)),
					verticalTimeline: vertical_timeline,
					moment: moment,
					chronology: TL,
					internalEvents: JSON.parse(JSON.stringify(<?= json_encode($context['agendas']['communecter'] ?? null) ?>)),
					organizationMembers: JSON.parse(JSON.stringify(<?= json_encode($organizationMembers) ?>)),
					filterContributors: JSON.parse(JSON.stringify(<?= json_encode($memberFilters ?? []) ?>)),
					filterContentName: JSON.parse(JSON.stringify(<?= json_encode($contentName ?? '') ?>))
				});
			}
		});
	})
</script>