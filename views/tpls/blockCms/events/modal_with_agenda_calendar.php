<?php
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $objectCss = $blockCms["css"] ?? [];
  $styleCss  = (object) [$kunik => $objectCss];
?>

<style type="text/css" id="#css-<?= $kunik ?>">
    .textBack_<?=$kunik?> h2{
        margin-bottom: 10px;
        margin-top: 10px;
    }
    div#calendar-event .fc-event {
        background-color: <?=$blockCms["css"]["eventCss"]["backgroundColor"]?> !important;
    }
    div#agenda-calendar<?=$kunik?> td.fc-today {
        background: <?=$blockCms["css"]["todayCss"]["backgroundColor"]?>!important;
    }
    .button_<?=$kunik?> {
        background-color: <?=$blockCms["css"]["buttonCss"]["backgroundColor"]?>;
        border:1px solid <?=$blockCms["css"]["buttonCss"]["borderColor"]?>;
        color: <?=$blockCms["css"]["buttonCss"]["color"]?>;
        margin-bottom: 4%;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        padding: 10px;
        font-size: 16px;
        cursor: pointer;
        padding: 8px 8px;
    }

    .button_<?=$kunik?>:hover {
        background-color: <?=$blockCms["css"]["hover"]["buttonCss"]["backgroundColor"]?>;
        color: <?=$blockCms["css"]["hover"]["buttonCss"]["color"]?>;
        font-weight:700;
    }

    @media (max-width: 414px) {
        .button_<?=$kunik?> {
            padding: 8px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 11px;
            cursor: pointer;
            border-radius: 20px;
        }
    }

    @media screen and (min-width: 1500px){
        .button_<?=$kunik?> {
            padding: 10px 20px;
            margin-bottom: 2%;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 25px;
            cursor: pointer;
            border-radius: 35px;
        }
    }

    #modal_<?=$kunik?> .modal-dialog,
    #modal_<?=$kunik?> .modal-content {
        position: fixed;
        top: 10%;
        right: 5%;
        left: 5%;
        bottom: 10%;
        box-shadow: none;
        height: auto;
        padding-top: 10px !important;
        width: auto !important;
    }
    #modal_<?=$kunik?>.modal {
        margin: 0;
        padding: 0;
        overflow: hidden;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.5);
        border-left: 3px solid #333;
        z-index: 200000;
    }

    #modal_<?=$kunik?> .modal-body {
        height: 100%;
    }
    <?php if(isset($blockCms["image"]) && !empty($blockCms["image"])){ ?>
        .contain_image-bg {
            background-image: url(<?php echo $blockCms["image"] ?>);
            width: 100%;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            /* Firefox */
            height: -moz-calc(100% - 70px);
            /* WebKit */
            height: -webkit-calc(100% - 70px);
            /* Opera */
            height: -o-calc(100% - 70px);
            /* Standard */
            height: calc(100% - 70px);
        }
    <?php } ?>
</style>

<?php $idBtn = ($blockCms["showType"] == "calendar") ? "modal_agenda_".$kunik : "" ?>
<div class="col-xs-12 textBack_<?=$kunik?> text-center container"  >
    <button  id="<?=$idBtn?>"  class=" button_<?=$kunik?> btn btn-lg btn-primary"  data-toggle="modal" data-target="#modal_<?=$kunik?>">
        <?=$blockCms["labelButton"]?>
    </button>
    <div class="modal fade" id="modal_<?=$kunik?>" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="btn btn-default btn-sm text-dark pull-right close-modal margin-left-10" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </button>
                <h4><i class="fa fa-<?=$blockCms["iconAgenda"]?>"></i> <?=$blockCms["titreAgenda"]?></h4>
            </div>
            <div class="modal-body">
                <?php if ($blockCms["showType"] == "calendar") { ?>
                    <div id="calendar-event" class="calendar-event">
                        <div id="agenda-calendar<?=$kunik?>" class="no-padding"></div>
                    </div>
                <?php } else { ?>
                    <div class="col-xs-12 contain_image-bg no-padding">
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    sectionDyf.<?php echo $kunik ?>blockCms.iconAgenda = sectionDyf.<?php echo $kunik ?>blockCms.iconAgenda == "" ||
    sectionDyf.<?php echo $kunik ?>blockCms.iconAgenda == "fa fa-" ? "" : "fa fa-"+sectionDyf.<?php echo $kunik ?>blockCms.iconAgenda;
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>blockCms;
        var modalCalendarInput = {
            configTabs : {
                general : {
                    inputsConfig : [
                        {
                            type : "inputSimple",
                            options : {
                                name : "titreAgenda",
                                label : tradCms.title,
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                name : "labelButton",
                                label : tradCms.buttonLabel,
                                collection : "cms"
                            }
                        },
                        {
                            type: "inputIcon",
                            options: {
                                name: "iconAgenda",
                                label: tradCms.selectIcon,
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "showType",
                                label : tradCms.displayType,
                                tabs: [
                                    {
                                        value:"calendar",  
                                        label: tradCms.calendar,
                                        inputs : [
                                            
                                        ]
                                    },
                                    {
                                        value:"photo"  , 
                                        label: tradCms.image,   
                                        inputs :[
                                            {
                                                type : "inputFileImage",
                                                options: {
                                                    name : "image",
                                                    label : tradCms.uploadeImage,
                                                    collection : "cms"
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    ]
                },
                style : {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "buttonCss",
                                label : tradCms.buttonColor,
                                inputs : [
                                    "color",
                                    "backgroundColor",
                                    "borderColor"
                                ]
                            }
                        },
                        {
                            type : "section",
                            options : {
                                name : "eventCss",
                                label : tradCms.css,
                                inputs : [
                                    "backgroundColor"
                                ]
                            }
                        },
                        {
                            type : "section",
                            options : {
                                name : "todayCss",
                                label : tradCms.backgroundColorDay,
                                inputs : [
                                    "backgroundColor"
                                ]
                            }
                        }
                    ]
                },
                hover: {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "buttonCss",
                                label : tradCms.buttonColorHover,
                                inputs : [
                                    "color",
                                    "backgroundColor"
                                ]
                            }
                        }
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.modal_with_agenda_calendar<?= $myCmsId ?> = modalCalendarInput;
    }
    
    var domCal = {
        container : "#agenda-calendar<?=$kunik?>",
        options :{
            header: {
                left: 'prev,next',
                center: 'title',
                right: 'today'
            },
        },
        eventBackgroundColor: '#2D328D',
    };

    $('#modal_agenda_<?=$kunik?>').on('click', function(e) {
        setTimeout(function() {
            var eventCalendarModal={};
            eventCalendarModal = calendarObj.init(domCal);
            eventCalendarModal.options.personalAgenda = true
            eventCalendarModal.results.add(eventCalendarModal,myContacts.events);
        }, 2000);
    });

</script>