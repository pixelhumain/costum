<?php
$default = "black";
$structField = "structags";

$keyTpl = "blockProgramme";

$paramsData = [
    "title" =>  "",
    "color" => "",
    "defaultcolor" => "#354C57",
    "tags" => "structags"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style type="text/css">
    @font-face{
        font-family: "DINAlternate-Bold";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/coevent/DINAlternate-Bold.ttf")
    }

    .dinalternate{
        font-family: 'DINAlternate-Bold';
    }

    .none{
        display: none;
    }

    .md-date-other h3{
        color: #EE302C;
        font-family: 'DINAlternate-Bold';
    }

    .carousel-inner {
        position: initial;
    }
</style>
<!-- Le programme -->
<div style="background-color: white;" class="text-center agenda">
    <div style="border-right: #EE302C solid;margin-left: 2%;text-align: start;" class="col-md-6">
        <div class="col-md-12">
            <h1 class="dinalternate" style="color: black;">
                <?php echo Yii::t('cms', 'The program')?>
          </h1>

          <div class="col-md-3" style="border: solid #EE302C 4px;border-radius: 11px;width: 17%;"></div><br>
          <div class="pull-right containStartHour" style="color:#EE302C">

          </div>
          <div id="containdescriptionevent" class="col-md-11 pull-left dinalternate">
            <?php
            $i = 0;
            foreach ($subevent as $k => $v) {
                    // if ($v["type"] == "festival") {
                $debut_jour = date('d', strtotime($v["startDate"]));
                $hidden = ($i >= 1 ) ? "none" : "rien";
                if (count ( Cms::getCmsByStruct($cmsList,"minDesc".$debut_jour,$structField ) ) != 0){
                    $dataType = Cms::getCmsByStruct($cmsList,"minDesc".$debut_jour,$structField)[0];
                    ?>
                    <div class="<?= $v["type"]?> type col-md-12 <?= $hidden ?> debut-jour debut-jour<?= $debut_jour ?>">
                        <span style="text-align: left;" class="markdown dinalternate col-md-12 md-date-first  md-date<?= date("d",$i) ?>"><?= @$dataType["description"]; ?></span>
                        <?php 
                        $edit = "update";
                    }else{
                        $edit = "create";
                        ?>
                        <div class="col-md-12 <?= $hidden ?> debut-jour debut-jour<?= $debut_jour ?>">
                            <div class="col-xs-12 description description<?= $debut_jour ?> text-center">
                                <?php echo Yii::t('cms', 'Add a program by clicking on create content')?>
                            </div>
                            <?php
                        }
                        ?>
                        <center>
                            <div class="col-md-12 button-type-prg button-type-prg<?= $debut_jour ?>">
                                <?php
                                echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                    array(
                                        'edit' => $edit,
                                        'tag' => "minDesc".$debut_jour,
                                        'id' => (string)@$dataType["_id"]),true); 

                                echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS");
                                ?>
                            </div>
                        </center>
                    </div>
                    <?php
                    $i++;   
                }
                ?>
            </div>
        </div>

        <div style="margin-top: 1%;" class="col-md-12">
            <h1 style="color: black;margin-top: 2%;"><?php echo Yii::t('cms', 'Days')?></h1>
            <div class="col-md-3" style="border: solid #EE302C 4px;border-radius: 11px;width: 17%;"></div><br>
            <p class="pull-right containEndHour" style="color:#EE302C"></p>
            <div style="color: white;margin-top: 2%;" class="col-md-11 text-center dataEvent">
                <!-- Contain the date off an event -->
                <div id="secondCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner itemctrDate">
                        <div class="item active">
                            <?php
                            $count= 0;
                            foreach ($subevent as $key => $value) {
                            // if ($value["type"] == "festival") {
                                $debut_jour = date('d', strtotime($value["startDate"]));
                                $debut_mois = date('m', strtotime($value["startDate"]));
                                $debut_annee = date('y', strtotime($value["startDate"]));
                                $debut_heure = date('G', strtotime($value["startDate"]));

                                // var_dump($debut_heure);exit;

                                $fin_jour = date('d', strtotime($value["endDate"]));
                                $fin_mois = date('m', strtotime($value["endDate"]));
                                $fin_annee = date('y', strtotime($value["endDate"]));
                                $fin_heure = date('G', strtotime($value["endDate"]));

                                $debut_date = mktime(0, 0, 0, $debut_mois, $debut_jour, $debut_annee);
                                $fin_date = mktime(0, 0, 0, $fin_mois, $fin_jour, $fin_annee);
                                // var_dump($debut_date);exit;
                                $day = array();
                                if ( $count >= 4) { ?>
                                </div>
                                <div class="item">
                                    <?php 
                                }
                                if ( $debut_jour == $fin_jour){
                                    ?>
                                    <div class='<?= $value["type"] ?> type col-md-2 containjourney'>
                                        <?php
                                        for($i = $debut_date; $i <= $fin_date; $i+=86400){
                                            ?>
                                            <p data-key="<?= date("d",$i) ?>" class="date dinalternate active"><?= date("d",$i) ?></p>
                                            <p class="journey dinalternate"><?= date("D.",$i) ?></p>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }else{
                                    ?>
                                    <div class='<?= $value["type"] ?> type col-md-2 containjourney'>
                                        <p data-key="<?= $debut_jour ?>" class="date dinalternate"><?= $debut_jour ?></p>
                                        <p class="journey dinalternate"><?= date("D.",$i) ?></p>
                                    </div>
                                    <?php  
                                }
                                $count++;
                            // }
                            }
                            ?>
                        </div>
                        <?php if ($count >= 4) { ?>            
                            <div id="arrow-caroussel">
                                <a style="opacity: 1;width: 5%;margin-top: 2%;margin-left: -3%;" class="carousel-control" href="#secondCarousel" data-slide="prev">
                                    <?php echo $this->renderPartial("costum.assets.images.coevent.backLeft"); ?>
                                    <span class="sr-only"><?php echo Yii::t('cms', 'Previous')?></span>
                                </a>
                                <a style="opacity:1;width: 5%;margin-top: 2%;margin-left: 65%;" class="carousel-control" href="#secondCarousel" data-slide="next">
                                 <?php echo $this->renderPartial("costum.assets.images.coevent.backRight"); ?>
                                 <span class="sr-only"><?php echo Yii::t('cms', 'Next')?></span>
                             </a>
                         </div>
                     <?php } ?>  
                 </div>
             </div>
         </div>
     </div>
 </div>
 <div style="text-align: initial;" class="col-md-5">
    <?php
    $y = 0;
    foreach ($subevent as $key => $value) {
            // if ($value["type"] == "festival") {
        $hidden = ($y >= 1 ) ? "none" : "rien";
        $debut_jour = date('d', strtotime($value["startDate"]));
        $debut_mois = date('m', strtotime($value["startDate"]));
        $debut_annee = date('y', strtotime($value["startDate"]));
        $debut_heure = date('G', strtotime($value["startDate"]));

                // var_dump($debut_heure);exit;

        $fin_jour = date('d', strtotime($value["endDate"]));
        $fin_mois = date('m', strtotime($value["endDate"]));
        $fin_annee = date('y', strtotime($value["endDate"]));
        $fin_heure = date('G', strtotime($value["endDate"]));

        $debut_date = mktime(0, 0, 0, $debut_mois, $debut_jour, $debut_annee);
        $fin_date = mktime(0, 0, 0, $fin_mois, $fin_jour, $fin_annee);
                // var_dump($debut_date);exit;
        $day = array();
        ?>
        <div class="<?= $value["type"] ?> type col-md-12">
            <?php
            for($i = $debut_date; $i <= $fin_date; $i+=86400){
                $hidden = ($y >= 1) ? "none" : ""; 
                if ( $debut_jour == $fin_jour){
                    ?>
                    <p class="<?= $hidden ?> longDate longdate<?= date("d",$i) ?> dinalternate <?= $y ?>"><?= date("d.m.y",$i) ?></p>
                    <?php
                }else{?>
                    <p class="<?= $hidden ?> longDate longdate<?= date("d",$i) ?> dinalternate <?= $y ?>"><?= $debut_jour.".".$debut_mois.".".$debut_annee ?> <?php echo Yii::t('cms', 'to')?> <?= $fin_jour.".".$fin_mois.".".$fin_annee ?></p>
                    <?php
                }
            } 
            $y++;

            if (count ( Cms::getCmsByStruct($cmsList,"longDesc".$debut_jour,$structField ) ) != 0){
                $dataType = Cms::getCmsByStruct($cmsList,"longDesc".$debut_jour,$structField)[0];
                ?>
                <span style="text-align: left;" class="markdown col-md-12 md-date<?= date("d",$debut_date) ?> dinalternate md-date-other <?= $hidden ?>"><?= @$dataType["description"]; ?></span>
                <?php
                $edit ="update";
            }else{
                $edit ="create"
                ?>
                <div class="col-xs-12 dinalternate description md-date<?= date("d",$debut_date) ?> text-center <?= $hidden ?>">
                    <?php echo Yii::t('cms', 'Enter the course of the event by clicking on create content')?>
                </div>
                <?php
            }
            ?>
            <center>
                <div class="col-md-12 <?= $hidden ?> button-type buttonDay<?= $debut_jour ?>">
                    <?php
                    echo $this->renderPartial("costum.views.tpls.openFormBtn",
                        array(
                            'edit' => $edit,
                            'tag' => "longDesc".$debut_jour,
                            'id' => (string)@$dataType["_id"]),true); 

                    $hidden = ($y >= 2) ? "none" : "";

                    echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS");
                    ?>
                </div>
            </center>
        </div>
        <?php
        $i++;
    }
    ?>
</div>

<?php         
$y = 0;
foreach ($subevent as $key => $value) {
    $hidden = ($y >= 1 ) ? "none" : "rien";
    if (empty($type) || $type != $value["type"]) {
        $type = $value["type"];
        ?>
        <div style="margin-top: 4%;" class="">
            <a data-type="<?= $value["type"] ?>" class="<?= $hidden ?> dinalternate generate-pdf pdf-<?= $value["type"]?>" style="background-color:#EE302C;color:white;border-style: solid;border-color: #EE302C;border-radius: 51px;padding: 14px;" href="#"><?php echo Yii::t('cms', 'Download the PDF')?></a>
        </div>
        <?php 
        $y++;
    }
}?>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function(){
        mylog.log("---------- Render blockprogramme","costum.views.event.blockprogramme");
       
        thematique = "festival";

        $(".generate-pdf").click(function(){
            window.open(baseUrl+'/costum/coevent/getpdfaction/thematique/'+ $(this).attr("data-type") + "/parentId/" + contextId + "/parentType/" + contextType , '_blank');
            getAjax(null, baseUrl +"/costum/coevent/getpdfaction" , function () {}, "html");
        });

        $(".containjourney").click(function(){
            $(".date").css("background-color","#EE302C");
            $(".journey").css("color","black");
            $(this).children(".date").css("background-color","black");
            $(this).children(".journey").css("color","#EE302C");
        });

        $(".date").click(function(){
            $(".debut-jour").css('display','none');
            $(".longDate").css('display','none');
            $(".md-date-other").css('display','none');
            $(".description").css('display','none');
            $(".button-type-prg").css('display','none');
            $(".button-type").css('display','none');

            $(".debut-jour"+$(this).attr("data-key")).css('display','initial');
            $(".longdate"+$(this).attr("data-key")).css('display','initial');
            $(".md-date"+$(this).attr("data-key")).css('display', 'initial');
            $(".description"+$(this).attr("data-key")).css('display', 'initial');
            $(".buttonDay"+$(this).attr("data-key")).css('display', 'initial');
            $(".button-type-prg"+$(this).attr("data-key")).css('display','initial');

        });

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "title" : {
                        label : "<?php echo Yii::t('cms', 'Title')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                    },
                    "color" : {
                        label : "<?php echo Yii::t('cms', 'Text color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.color

                    },
                    defaultcolor : {
                        label : "<?php echo Yii::t('cms', 'Color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.defaultcolor
                    },
                    tags : {
                        inputType : "tags",
                        label : "<?php echo Yii::t('cms', 'Tags')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.tags
                    }
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) { 
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

    });

$(".editThisBtn").off().on("click",function (){
    mylog.log("editThisBtn");
    var id = $(this).data("id");
    var type = $(this).data("type");
    dyFObj.editElement(type,id,null,dynFormCostumCMS)
});
$(".createBlockBtn").off().on("click",function (){
    mylog.log("createBtn");
    dyFObj.openForm('cms',null,{structags:$(this).data("tag")},null,dynFormCostumCMS)
});

$(".deleteThisBtn").off().on("click",function (){
    mylog.log("deleteThisBtn click");
    $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
    var btnClick = $(this);
    var id = $(this).data("id");
    var type = $(this).data("type");
    var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;

    bootbox.confirm(trad.areyousuretodelete,
        function(result) 
        {
            if (!result) {
              btnClick.empty().html('<i class="fa fa-trash"></i>');
              return;
          } else {
              ajaxPost( 
                null,
                urlToSend
            )
              .done(function (data) {
                if ( data && data.result ) {
                  toastr.info("élément effacé");
                  $("#"+type+id).remove();
              } else {
               toastr.error("something went wrong!! please try again.");
           }
       });
          }
      });
});


</script>