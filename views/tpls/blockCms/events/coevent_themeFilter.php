<?php
    $costum = !empty($this->costum) ? $this->costum : $this->context->config['costum'];
    if(!empty($costum) && !empty($costum["css"]) && !empty($costum["css"]["color"])){
        $mainKey=array_keys($costum["css"]["color"])[0];
        $mainColor=$costum["css"]["color"][$mainKey];
    }

    $paramsData = [
        // 'mainTitle'    => 'Qui sommes-nous?',
        // 'aboutTitle'   => 'titre',
        // 'aboutContent' => 'Contenu',
        'primaryColor' => (!empty($mainColor)) ? $mainColor : '#EE302C'
    ];
    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e])) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>
<style>
    a.event-type-filter {
        display: block;
        cursor: pointer;
        text-decoration: none;
        color: white;
        padding: 0 2rem 0 0;
    }

    a.event-type-filter.active {
        color: white;
        text-decoration: none;
        padding: 0 0 0 2rem;
    }

    a.event-type-filter:hover {
        color: white;
        text-decoration: underline;
    }

    a.event-type-filter.active:hover {
        color: white;
        text-decoration: none;
    }

    @media only screen and (max-width: 425px) {
        .about-image {
            display: none;
        }
    }
</style>
<div id="themeFilter<?= $blockKey ?>" data-anchor-target="themeFilter" style="margin-bottom: 3%;" class="row infos">
    <div class="col-xs-12">

        <div id="about-link-container<?= $blockKey ?>" class="col-xs-12"
                     style="padding:15px; border-radius: 20px; background: <?= $paramsData['primaryColor'] ?>; color: white; font-size: 1.5rem; ">
                    <span class="sp-text" data-field="text" style="font-variant: small-caps;font-weight: bold;font-size: 1.6em; display: block; margin-bottom: 10px;">Typologie des événements : </span>

        </div>
    </div>
</div>
<script>
    (function ($) {
        function get_events() {
            var back_events = baseUrl + '/costum/agenda/events/request/all/reorder/1';
            return new Promise(function (__resolve, __reject) {
                ajaxPost(null, back_events, {
                    costumType: costum.contextType,
                    costumId  : costum.contextId
                }, function (__data) {
                    __resolve(__data)
                }, function (__error) {
                    __reject(__error)
                }, 'json');
            });
        }

        function about_link_refresh() {
            var element_type_filter_elements = $('a.event-type-filter');
            element_type_filter_elements.on('click', function (__e) {
                __e.preventDefault();
                var filter_text_element = $('.active_filter_text:first');
                var active_filter_container_element = $('.active_filter_container:first');
                var main_nav = $('#mainNav');
                var is_active = $(this).hasClass('active');
                var that = this;

                element_type_filter_elements.removeClass('active');
                if (!is_active) {
                    $(this).addClass('active');
                    var type = this.getAttribute('href');
                    filter_text_element.text(tradCategory[type]);
                    filter_text_element.attr('data-type', type);
                    active_filter_container_element.css({
                        'display': '',
                        top      : `${main_nav.outerHeight()}px`
                    });

                    active_filter_container_element.fadeIn(800, function () {
                        $(this).off('click').on('click', function () {
                            $(that).trigger('click');
                        });
                        // smooth scroll
                        if ($('.program-container').length) {
                            $('html, body').animate({
                                scrollTop: $('.program-container:first').offset().top - main_nav.outerHeight()
                            }, 500);
                            $('.program-container').trigger('coeventfilter');
                        }
                    });
                } else {
                    active_filter_container_element.fadeOut(800);
                    filter_text_element.data('type', '');
                    if ($('.program-container').length) $('.program-container').trigger('coeventfilter');
                }
            });
        }

        $(function () {
            get_events().then(function (__events) {
                var html = '';
                var about_link_container = $('#about-link-container<?= $blockKey ?>');
                for (var i = 0; i < __events['groups']['length']; i++) {
                    var group = __events['groups'][i];
                    html += '<a class="event-type-filter" href="' + group.value + '">' + tradCategory[group.value] + '</a>';
                }
                html = about_link_container.html() + html;
                about_link_container.html(html);
                about_link_refresh();
            })
        });
    })(jQuery)
</script>