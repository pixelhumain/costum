<?php 
  $keyTpl ="eventEnPod";
  $kunik = $keyTpl.(string)$blockCms["_id"];
  $blockKey = $blockCms["_id"];
   $paramsData = [
       "showType" => "événements"
  ];
  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if ( !empty($blockCms[$e]) && isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>
 <style type="text/css">
    .add<?= $kunik?> {
      padding: 5px;
      font-size: 18px;
      background: #000091;
      margin-left: 12px;
    }
    .<?= $kunik?> .divEndOfresults{
      display: none !important;
    }
    .<?= $kunik?>  .searchEntityContainer.events .event-short-description {
      color: white;
    } 
    .container-filters-menu:not(#central-container .container-filters-menu, #content-admin-panel .container-filters-menu){
      box-shadow : none;
    }
 </style>
  <div class="<?= $kunik?> eventEnPod">
    <?php if($costum["editMode"] == "true"){ ?>
      <a href="javascript:;" class="btn btn-primary btn-xs add<?= $kunik?> hiddenPreview">
        <i class="fa fa-plus"></i>  <?php echo Yii::t('cms', 'Add an event')?>
      </a>
    <?php } ?>
    <div class="filtersActive<?= $kunik?>"> </div>
    <div id="<?= $kunik?>" ></div>
  </div>

 <script type="text/javascript" >
  contextData = <?php echo json_encode(Element::getByTypeAndId($costum["contextType"], $costum["contextId"]))  ?>;
  contextData.id = costum.contextId
  var paramsFilter = {  
    container : ".filtersActive<?= $kunik?>",
    results :{
      dom : '#<?= $kunik?>',
      renderView : "eventObj.eventElement<?= $kunik?>",
      smartGrid:true
    },
    loadEvent : {
	 		default : "scroll"
	 	},
    filters :{ 
      
    },  
    defaults : {
      types : ["events"],
      sortBy:{"startDate":-1},
      indexStep : 150, 
       forced :{
        filters : {
          tags:["infos-events","<?= $paramsData["showType"]?>"]
        }
       }
    }
  };
  paramsFilter.defaults.forced.filters["organizer."+costum.contextId] = {'$exists' :true};
            
  var eventObj ={
    eventElement<?= $kunik?>:function (params){
      mylog.log("eventPanelHtml",params);
      params.id=params._id.$id;
      params.hash="#page.type."+params.collection+".id."+params.id;

      if(directory.dirLog) mylog.log('-----------eventPanelHtml', params);
      var str = '';
      str += '<div id="entity_'+params.collection+'_'+params.id+'" class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer smartgrid-slide-element events contain_'+params.collection+'_'+params.id+' \'>';
      str +=    '<div class=\'searchEntity card-event\' id=\'entity'+params.id+'\'>';
      var dateFormated = directory.getDateFormated(params);

      var countSubEvents = ( params.links && params.links.subEvents ) ? '<br/><i class=\'fa fa-calendar\'></i> '+Object.keys(params.links.subEvents).length+' '+trad['subevent-s']  : '' ; 

      str += '<div class="card-image">'+params.imageProfilHtml;
      str +=  '<div class="box-content co-scroll">';
      if(typeof params.type != "undefined"){      
        var typeEvent = (typeof tradCategory[params.type] != 'undefined') ? tradCategory[params.type] : params.type;
        str += '<h5 class="text-white lbh add2fav"><i class="fa fa-angle-right text-orange"></i>&nbsp; '+
        typeEvent;
        str += '</h5>';
      }

      if(notNull(params.descriptionStr))
        str +=  '<p class="event-short-description  col-xs-12 margin-top-10 margin-bottom-10">'+params.descriptionStr+'</p>';
      if(typeof params.tagsHtml != "undefined")
        str +=  '<ul class="tag-list">'+params.tagsHtml+'</ul>';
      if (isInterfaceAdmin && costum.editMode) {
        str += 
        '<a href="javascript:;" onclick="eventObj.edit<?= $kunik?>('+'\''+params._id.$id+'\''+')" class="btn  bg-green hiddenPreview "> <i class="fa fa-edit"></i>'+
        '</a>'+
        '<a href="javascript:;" onclick="eventObj.delete<?= $kunik?>('+'\''+params._id.$id+'\''+')" class="btn  bg-red hiddenPreview "> <i class="fa fa-trash"></i>'+
        '</a>';
      }
      '</div>';

      str +=  '</div>';
      str +=  '</div>';
      str +=  '<div class="col-xs-12 card-content entityName">';
      str += '<h4 class=""><a href="'+params.hash+'" class="lbh add2fav letter-turq">'+
      params.name + 
      '</a></h4>';
      str += '</div>';
      str += '<div class="col-xs-12 card-date">'+dateFormated+countSubEvents;
      str += '</div>';

      str += '</div>';
      str += '</div>';
      coInterface.bindLBHLinks();
      
      return str;
    },
    addEvents<?= $kunik?>:function(){
      var dyfEvent={
        "beforeBuild":{
          "properties" : {
            organizer: {
                inputType: "finder",  
                initContext : true
            },
            "tags" : {
              "label" : "<?php echo Yii::t('cms', 'key word')?> ",
              "inputType" : "tags",
              "tagsList" : "motClef",
              "value" :['<?= $paramsData["showType"]?>'],
              "rules":{
                "required":true
              },
              "order" : 4
            },
          }
        },
        "onload" : {
          "actions" : {
            "setTitle" : "<?php echo Yii::t('cms', 'Add an event')?>",
            "src" : { 
              "infocustom" : "Remplir le champ"
            },
            "hide" : {
              "breadcrumbcustom" : 1,
              "urlsarrayBtn" : 1,
              "parentfinder" : 1,
              "removePropLineBtn" : 1
            }
          }
        }

      }
      dyfEvent.afterSave = function(data){
        dyFObj.commonAfterSave(data, function(){
          mylog.log("dataaaa", data);
          urlCtrl.loadByHash(location.hash);
        });
      }
      dyFObj.openForm('event',null, null,null,dyfEvent);
    },
    delete<?= $kunik?>:function(idEvent) {
      bootbox.confirm(trad.areyousuretodelete, function(result) {
        if (result) {
          var url = baseUrl + "/" + moduleId + "/element/delete/id/" + idEvent + "/type/events";
          var param = new Object;
          ajaxPost(null, url, param, function(data) {
            if (data.result) {
              toastr.success(data.msg);
              urlCtrl.loadByHash(location.hash);
            } else {
              toastr.error(data.msg);
            }
          })
        }
      })
    }, 
    edit<?= $kunik?>:function(idEvent){
      dyFObj.editElement("events",idEvent);
    }
    

  }
  
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {

    filterSearch = searchObj.init(paramsFilter);
    filterSearch.search.init(filterSearch,0);
    $(".add<?= $kunik?>").click(function() {
      eventObj.addEvents<?= $kunik?>();
    });
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
          "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
          "icon" : "fa-cog",
        "properties" : {
            "showType":{
                "label" : "<?php echo Yii::t('cms', 'Display type')?>",
                inputType : "select",
                options : {
                    "événements" : "<?php echo Yii::t('common', 'Events')?>",
                    "formation" : "<?php echo Yii::t('common', 'course')?>"
                },
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.showType
            },
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });
          mylog.log("save tplCtx",tplCtx);
          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                  toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
              });
            } );
          }
        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
    });

  })
 </script>