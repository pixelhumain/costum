<?php
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
 
$keyTpl ="agendaCarousel3";
$paramsData = [ 
	"title" => "AGENDA",
	"contenu" => " Lorem ipsum dolor sit amet, consectetur adipiscing",
	"contenuColor"=>"#327753",
	"background" => "#e84d4c"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
} 
?>
<style>
	
	.container<?php echo $kunik ?> .btn-edit-delete{
		display: none;
		z-index: 9999;
	}
	.container<?php echo $kunik ?>:hover .btn-edit-delete{
		display: block;
		position: absolute;
		top:40%;
		left: 50%;
		transform: translate(-50%,-50%);
	}
	.title<?php echo $kunik ?>{
		padding: 10px;
		text-transform: none !important;
		background-color:  <?= $paramsData["background"]?>;
	}
	.content_<?= $kunik?> p {
		font-size: 20px;
		color: <?= $paramsData["contenuColor"]?>;
		margin-top: 2%;
		margin-bottom: -1%;
	}

	.container<?php echo $kunik ?> .swiper-slide img{
		vertical-align: middle;
		width: 100%;
		height: 240px;
		border: 2px solid white;
	}
	
	.container<?php echo $kunik ?> .swiper-slide {
		transition: all .2s ease-in-out;
		
	}

	.container<?php echo $kunik ?> .event-place{
		color:#bca87d;
		text-transform: none !important;
	}
	.container<?php echo $kunik ?> .event-name{
		color: #000000;
		text-transform: none !important;
	}
	.container<?php echo $kunik ?> .letter-orange{
	
		text-transform: capitalize !important;
		font-weight: normal !important;
	}

	.education-blog_style1 {
		margin-bottom: 40px;
		width: 80%;
	}
	.education-blog_style1 .education-blog_text {
		padding-top: 0px;
		padding-bottom: 0px;
	}
	.education-blog_text {
		background: #fff;
		padding: 115px 10px 10px 10px;
	}

	.education-blog_style1 .education-blog_text .education-blog_text_inner {
		background: #fff;
		position: relative;
		-webkit-box-shadow: 0px 10px 20px 0px rgba(153, 153, 153, 0.1);
		box-shadow: 0px 10px 20px 0px rgba(153, 153, 153, 0.1);
	}
	.education-blog_style1 .education-blog_text .education-blog_text_inner .cat {
		margin-bottom: 25px;
	}
	.education-blog_style1.small .education-blog_text .education-blog_text_inner .cat .cat_btn {
		display: block;
		width: 86px;
		margin-bottom: 12px;
	}

	.education-blog_style1 .education-blog_text .education-blog_text_inner .cat .cat_btn {
		background: #f9f9ff;
		color: #000;
		border: 1px solid #e9e9e9;
	}
	.education-blog_text .cat .cat_btn {
		background: #000000;
		line-height: 30px;
		color: #fff;
		padding: 0px 20px;
		border-radius: 15px;
		display: inline-block;
		font-size: 12px;
		font-family: "Inconsolata", monospace;
	}
	.education-blog_text .cat a {
		margin-right: 15px;
	}
	.education-blog_text .cat .title_<?= $kunik?>{
		color: #464646;
		font-size: 20px;
	}
	.education-blog_text .cat .locality_<?= $kunik?> i{
		font-size: 28px;
		padding: 2px;
	}
	.education-blog_text .cat  i{
		color: #000000;
	}

	.blog-img img{
		width: 100%;
	}

  .has-shadow{
    box-shadow: 0px 0px 10px 0px #00000087;
    text-align: left;
    padding-bottom: 15px;
  }

  .bottom-btn{
    text-align: center;
    padding: 20px;
  } 

  .bottom-btn a{
    background-color: #028fa1;
    color: white !important;
    border-radius: 20px;
    padding-left: 20px;
    padding-right: 20px;
  }
  .container<?php echo $kunik ?> .row{
    text-align: -webkit-center;
  }
</style>
<div class="container<?php echo $kunik ?>">
	<!------------------BEGIN BOUTONS EDIT ------------------------>
	<?php if(Authorisation::isInterfaceAdmin()){ ?>
		<div class="btn-edit-delete text-center">		
			<button class="btn btn-primary add-event-caroussel" onclick="dyFObj.openForm('event')"><?php echo Yii::t('cms', 'Add an event')?></button>
		</div>
	<?php } ?>
	<!------------------ END BOUTONS EDIT  ------------------------>
	<h2 class="title<?php echo $kunik ?> title">
		<i class="fa fa-calendar"></i>
		<?php echo $paramsData["title"] ?>
	</h2>
	
	<p class="content_<?= $kunik?> text-center sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="contenu"> <?= $paramsData["contenu"]?></p>
	<div class="padding-20">
		<div class="swiper-container">
			<div class="swiper-wrapper"></div>
			<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your agenda.')?>",
                "description" : "<?php echo Yii::t('cms', 'Personalize your agenda')?>",
				"icon" : "fa-cog",

				"properties" : {
					
					"title" : {
						"inputType" : "text",
						"label" : "<?php echo Yii::t('cms', 'Title')?>",

						values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
					},
					"background" : {
						"inputType" : "colorpicker",
						"label" : "<?php echo Yii::t('cms', 'Title color')?>",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
					} ,   
				},
            	beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
				save : function (data) {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent")
							tplCtx.value[k] = formData.parent;

						if(k == "description")
							tplCtx.value[k] = data.description;
					});
					console.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
								toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}

				}
			}
		};

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		});
	});

</script>

<script>

	$(function ($) {

		var htmlContent= "";
		getAjax('',baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+costum.contextType+
			'/id/'+costum.contextId+'/dataName/events',
			function(data){ 
				$.each(data,function(k,v){
					htmlContent += 
					'<div class="swiper-slide education-blog_style1">'+
			            '<div class="has-shadow">'+
			              '<div style="line-height: 0;border: 2px solid #e9e9e9;margin: 5px;">'+
			               '<div class="">'+directory.showDatetimePost(v.collection, v.id, v.startDateSec)+'</div>'+
								     '<div class="blog_img">'+
								       '<img class="img-fluid" src="'+v.profilImageUrl +'" alt="">'+
								     '</div>'+
								     '<div class="education-blog_text">'+
								       '<div class="education-blog_text_inner">'+
								         '<div class="cat" >'+
								         '<h4>'+
								         '<a href="#page.type.'+v.collection+'.id.'+v.id+'" class="lbh-preview-element text-center event-name title_<?= $kunik?>" ">'+v.name+'</a>'+
								         '</h4>'+
								         '<h4>'+
								           '<a style="color: gray; font-size: 16px" href="#page.type.'+v.collection+'.id.'+v.id+'" class=" locality_<?= $kunik?> lbh-preview-element">'+
								           '<i style="color: gray;" class="fa fa-home "></i>'+'   '+
			            					v.address.postalCode+' '+  v.address.addressLocality +' '+' '+
			            					((typeof v.address.level1Name !="undefined") ? v.address.level1Name : "<?php echo Yii::t('cms', 'Address not given')?>")+
			            					'</a> '+
								         '</h4>'+
			                  '<font style="text-transform: uppercase;font-weight: bold; line-height: 0 !important;">'+v.typeEvent+'</font>'+
								directory.getDateCustomize(v)+
								       '</div>'+
			                '</div>'+
			              '</div>'+
			            '</div>'+
			          '</div>'+
			          '<div class="bottom-btn">'+
			            '<a href="#page.type.'+v.collection+'.id.'+v.id+'" class="btn lbh-preview-element text-center event-name title_<?= $kunik?>" "><b><?php echo Yii::t("cms", "See more")?></b></a>'+
			          '</div>'+
					'</div>';
				});
				$('.container<?php echo $kunik ?> .swiper-wrapper').append(htmlContent);
				var swiper = new Swiper(".container<?php echo $kunik ?> .swiper-container", {
	                slidesPerView: 1,
	                spaceBetween: 0,
	                // init: false,
	                pagination: {
	                  el: '.swiper-pagination',
	                  clickable: true,
	                },
	                navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
	                keyboard: {
	                  enabled: true,
	                },
	               /* autoplay: {
	                  delay: 2500,
	                  disableOnInteraction: false,
	                },*/
	                breakpoints: {
	                  640: {
	                    slidesPerView: 1,
	                    spaceBetween: 20,
	                  },
	                  768: {
	                    slidesPerView: 2,
	                    spaceBetween: 40,
	                  },
	                  1024: {
	                    slidesPerView: 3,
	                    spaceBetween: 50,
	                  },
	                }
	              });
	              coInterface.bindLBHLinks();
				}
			,"");
	});
</script>

<script type="text/javascript">
    
   directory.getDateCustomize= function(params, onlyStr, allInfos){
    if(typeof params.recurrency != 'undefined' && params.recurrency){
      return directory.initOpeningHours(params, allInfos);
    }else{
      params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
      params.startDay = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
      params.startMonth = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
      params.startYear = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
      params.startDayNum = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('d') : '';
      params.startTime = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
        
      params.endDateDB = notEmpty(params.endDate) ? params.endDate: null;
      params.endDay = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
      params.endMonth = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
      params.endYear = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
      params.endDayNum = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).format('d') : '';
      params.endTime = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
      params.startDayNum = directory.getWeekDayName(params.startDayNum);
      params.endDayNum = directory.getWeekDayName(params.endDayNum);

      params.startMonth = directory.getMonthName(params.startMonth);
      params.endMonth = directory.getMonthName(params.endMonth);
      params.color='orange';
        

      var startLbl = (params.endDay != params.startDay) ? trad['fromdate'] : '';
      var endTime = ( params.endDay == params.startDay && params.endTime != params.startTime) ? ' - ' + params.endTime : '';
      mylog.log('params.allDay', !notEmpty(params.allDay), params.allDay);
       
        
      var str = '';
      var dStart = params.startDay + params.startMonth + params.startYear;
      var dEnd = params.endDay + params.endMonth + params.endYear;
      mylog.log('DATEE', dStart, dEnd);

      if(params.startDate != null){
        if(notNull(onlyStr)){
          str+='<h4 class="text-bold letter-orange no-margin">';
          if(params.endDate != null && dStart != dEnd)
            str +=  '<small class="">'+trad.fromdate+'</small> ';
          str += '<span class="letter-'+params.color+'">'+params.startDay+'</span>';
          if(params.endDate == null || dStart == dEnd || (params.startMonth != params.endMonth || params.startYear != params.endYear))
            str += ' <small class="">'+ params.startMonth+'</small>';
          if(params.endDate == null || dStart == dEnd || params.startYear != params.endYear)
            str += ' <small class="">'+ params.startYear+'</small>';
          if(params.endDate != null && dStart != dEnd)
            str += ' <small class="">'+trad.todate+'</small> <span class="letter-'+params.color+'">'+params.endDay +'</span> <small class="">'+ params.endMonth +' '+ params.endYear+'</small>';
          str+='</h4>';

          str +=  '<small class="margin-top-5"><b><i class="fa fa-clock-o"></i> '+
                                params.startTime+endTime+'</b></small>';
        
        }else{ 
          str += '<h3 style="font-size:20px;">'+
                    '<small>'+startLbl+' </small>'+
                    '<small class="letter-'+params.color+'">'+params.startDayNum+'</small> '+
                    params.startDay + ' ' + params.startMonth + 
                    ' <small class="letter-'+params.color+'">' + params.startYear + '</small>';
          if(params.collection == "events"){
          str +=  ' <small class="pull-right margin-top-5"><b><i class="fa fa-clock-o margin-left-10"></i> '+
                                params.startTime+'</b></small>';
              }
          str +=  '</h3>';

        }
      }    
        
      if(params.endDate != null && dStart != dEnd && !notNull(onlyStr)){
        str += '<h3 class="letter-'+params.color+' text-bold no-margin" style="font-size:20px;">'+
                        '<small>'+trad['todate']+' </small>'+
                        '<small class="letter-'+params.color+'">'+params.endDayNum+'</small> '+
                        params.endDay + ' ' + params.endMonth + 
                        ' <small class="letter-'+params.color+'">' + params.endYear + '</small>';
        if(params.collection == "events"){
        str += ' <small class="pull-right margin-top-5"><b><i class="fa fa-clock-o margin-left-10"></i> ' + 
                                  params.endTime+'</b></small>';
        }
        str +=  '</h3>';
      }   
      return str;
    }
  }
</script>



