<?php
$defaultcolor = "black";
$structags = "structags";
$keyTpl = "wizardCarousselNew";

$paramsData = [ "title" => "",
                "icon"  =>  "",
                "color" => "black",
                "background" => "",
                "nbStep"    =>  3,
                "tags" => "structags"
                ];
$listStep = array();

$p = null;

if( isset($this->costum["tpls"][$keyTpl]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset($this->costum["tpls"][$keyTpl][$i]) ) 
            $paramsData[$i] =  $this->costum["tpls"][$keyTpl][$i];      
    }
}
// $i = 1;
// $y = 0;
// $z= 1;
// while($i < 8){
//     $z++;
    // $i++;
//     if($y < 3){
        // $listStep = Cms::getCmsByStruct($cmsList, "stepCaroussel".$i,$structags );
        // $y++;
        // if($y > 3) $y=0;
    // }
    // echo $z; echo $y;
// }
// var_dump($listStep);exit;
?>

<div id="wizardCaroussel" class="row" style="margin-top: 2%;">
    <div id="carousel"  class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <?php
            //foreach ($listStep as $k => $v) {
                //if($k % 3 == 0) echo '<div class="item">';
        if( count(Cms::getCmsByStruct($cmsList, "stepCaroussel",$structags ) ) != 0 ) { 
        $p = Cms::getCmsByStruct($cmsList, "stepCaroussel",$structags);
        $i = 1;

        foreach ($p as $key => $value) {

        $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $value["_id"]));

        if ($i == 1) {
        ?>
        <div class="item<?php echo $i ?> item active">
            <div class="col-md-6">
                <div style="position: absolute;z-index: 10;background: #EE302C;width: 49vw;max-height: 46vw;overflow: auto" class="blockwithimg-txt col-md-12">
                    <h1 style="position: relative;top: 4vw;" class="<?php if(isset($default)) echo 'text-'.$default.''; else echo 'text-white'; ?>"><?php echo $value["name"]; ?></h1>
                    <span style="position: relative;top: 5vw;" class="markdown text-white col-md-12"><?= @$value["description"]; ?></span>
                </div>
            </div>

            <?php if(isset($img)){ ?>
                <div style="height: 45vw;" class="blockwithimg-img col-md-12">
                    <img src="<?php echo Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"]; ?>" class="img-responsive">
                </div>
            <?php } ?>
        </div>
        <?php } else{ ?>
            <div class="item<?php echo $i ?> item">
                <div style="position: absolute;z-index: 10;top: 11vw;left: 7vw;" class="blockwithimg-txt col-md-6">
                    <h1 class="<?php if(isset($default)) echo 'text-'.$default.''; else echo 'text-red'; ?>"><?php echo $value["name"]; ?></h1>
                    <span class="markdown"><?= @$value["description"]; ?></span>
                </div>

                <?php if(isset($img)){ ?>
                    <div style="height: 45vw;" class="blockwithimg-img col-md-12">
                        <img src="<?php echo Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"]; ?>" class="img-responsive">
                    </div>
                <?php } ?>
            </div>
        <?php }
        $i++;
        } ?>
        </div>
       <!--  <a class="left carousel-control" href="#carousel" data-slide="prev">
            <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel" data-slide="next">
            <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a> -->
    </div>
    <?php
    $edit ="update";
        }else{ ?>
            <div class="col-xs-12 text-center">
                TODO : <br />
                as POI type cms + tag : blockimg0;
                <?php 
                    $edit = "create"; 
            echo"</div>";
        }
    echo $this->renderPartial("costum.views.tpls.openFormBtn",
        array(
            'edit' => $edit,
            'tag' => 'stepCaroussel',
            'id' => (string)@$value["_id"]
         ),true);
    echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
//    echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => true, "keyTpl"=>$keyTpl]);
        ?>
</div>

<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.wizard");
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section bloc un texte et une image",
            "description" : "Personnaliser votre section sur les blocs d'un texte et une image",
            "icon" : "fa-cog",
            "properties" : {
                title : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                color : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color

                },
                defaultcolor : {
                    label : "Couleur",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.defaultcolor
                },
                nbStep : {
                    label : "Nombres de slide dans le carouselle",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.nbStep
                },
                tags : {
                    inputType : "tags",
                    label : "Tags",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.tags
                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                 });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });
});
        
    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    

    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });

    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('cms',null,{structags:$(this).data("tag")},null,dynFormCostumCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result){
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                    ajaxPost(
                        null,
                        urlToSend,
                        null,
                        function(data) {
                            if ( data && data.result ) {
                                toastr.info("élément effacé");
                                $("#"+type+id).remove();
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                        },
                        null,
                        "json"
                    );
                }
            });
    });
</script>