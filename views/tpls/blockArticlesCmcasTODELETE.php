<?php
$keyTpl = "blockArticlesCmcas";

$paramsData = [
	"title"					=>	"Nos Actualités",
	"Textcolor"				=>	"#2c407a",
	"descriptionColor"		=>	"#2c407a",
	"description"			=>	"Toute l'actualité de la CMCAS : reportages, portraits et agenda des activités sociales et culturelles."
];

if (isset($blockCms)) {
    $tplsCms = ( !empty($blockCms["tpls"][$keyTpl]) && isset($blockCms["tpls"][$keyTpl]) ) ? $blockCms["tpls"][$keyTpl] : $blockCms;
    foreach ($paramsData as $e => $v) {
        if ( !empty($tplsCms[$e]) && isset($tplsCms[$e]) ) {
            $paramsData[$e] = $tplsCms[$e];
        }
    }
}
?>

<div id="actu-cmcas" class="container text-center col-md-10 col-sm-12 col-xs-12 col-md-offset-1" style="margin-top: 100px;">
	<h2 style="color:<?= $Textcolor ?>"><?= $title; ?></h2>
	<p style="color:<?= $descriptionColor; ?>" class="text-blue-cmcas">
		<?= $description; ?>
	</p>
	<div id="result-actus" class="featured-services-grids">
		<!-- contain the actus at the end off the file -->
		
	</div>
</div>

<?php 
     echo $this->renderPartial("costum.views.tpls.editTplBtns", ["path" => @$path.$keyTpl , "canEdit" => $canEdit, "keyTpl"=>$keyTpl, "page" => @$page, "id" => (String) @$blockCms["_id"]]);
?>

<script type="text/javascript">
sectionDyf = {};
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
	sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la des articles",
            "description" : "Personnaliser votre section sur les articles",
            "icon" : "fa-cog",
             onLoads : {
                onload : function(){
                    $(".parentfinder").css("display","none");
                }
            },
            "properties" : {
                "title" : {
                    label : "Titre",
                    inputType : "text",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                Textcolor : {
                    label : "Couleur en arrière plan du titre",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.Textcolor
                },
                descriptionColor : {
                    label : "Couleur du texte dans la carte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.descriptionColor
                },
                description : {
                    label : "Couleur de la carte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.description
                },
                parent : {
                    inputType : "finder",
                    label : tradDynForm.whoiscarrytheproject,
                    multiple : true,
                    rules : { required : true, lengthMin:[1, "parent"]}, 
                    initType: ["organizations"],
                    openSearch :true
                }
            },
            save : function () {  
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent") {
                        tplCtx.value["parent"] = formData.parent ;
                    }
                });

                tplCtx.value["page"] = '<?= $page ?>';
                tplCtx.value["type"] = 'tpls.blockCms.article.blockArticlesCmcas.<?= $keyTpl ?>';
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("élement mis à jour");
                        location.reload();
                    } );
                }
            }
        }
    };

	$(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";

        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
	});

	params = {
		"sourceKey" : costum.contextSlug
	};

	ajaxPost(
		null, 
		baseUrl+"/costum/cmcashautebretagne/getarticles", 
		params,
		function(results){ 
			mylog.log("articles :",results);

			if (results.dataArticles = true) {

				var ctr = "";
				var ctCol = 0;
				var color = [
					"#e36f41",
					"#791880",
					"#00a890",
					"#dc094b"
				];
				
				if(typeof results.article != "undefined" && results.article != null){
					$.each(results.article,function(key,value){

						var imgMedium = (value.imgMedium != "none") ? value.imgMedium : url;

						ctr += '<div class="col-md-6 col-sm-6 col-xs-12 grid-actu">';
							ctr += '<div class="well">';
								ctr += '<div class="contain">';
									ctr += '<div class="date-actu">';
										ctr += '<a href="#" class="actu-link"  style="background-color: '+color[ctCol]+'">';
											ctr += value.dCreate+' '+value.mCreate+' '+value.yCreate;
										ctr += '</a>';
									ctr += '</div>';
									ctr += '<div class="actu-name">';
										ctr += value.name;
									ctr += '</div>';
									ctr += '<a href="#page.type.'+value.collection+'.id.'+value.id+'" class="lbh-preview-element btn btn-default btn-more-actu">En savoir plus</a>';
								ctr += '</div>';
								ctr += '<div class="featured-services-grid pull-right">';
									ctr += '<div class="featured-services-grd" style="background: url('+baseUrl+value.imgMedium+') center;">';
									ctr += '</div>';
								ctr += '</div>';
							ctr += '</div>';
						ctr += '</div>';
						ctCol++;
					});
				}
				
				
			}else{
				ctr += "Aucune actualité pour le moment";
			}

			$("#result-actus").html(ctr);
			coInterface.bindLBHLinks();
		},
		function(){
			mylog.log("error articles");
		}
	);
});
</script>