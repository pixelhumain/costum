<script type="text/javascript">
var dynFormCostumForum = {
    "beforeBuild":{
        "properties" : {
            "structags" : {
                "inputType" : "hidden"
            }
        }
    },
   "onload" : {
        "actions" : {
            "setTitle" : "Forum Data",
            "html" : {
                "nametext>label" : "Nom du forum",
                "infocustom" : "<br/>Une entité comme base d'un forum de discussion"
            },
            "presetValue" : {
                "type" : "forum"
            },
            "hide" : {
                "formLocalityformLocality" : 1,
                "breadcrumbcustom" : 1,
                "parentfinder" : 1,
                "urlsarray" : 1,
                "tagstags":1
            }
        }
    }
};

jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.elementBlock");
    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    // /alert("costum.views.tpls.text");
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn",$(this).data("id"),$(this).data("type"));
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumForum)
    });

    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dynFormCostumForum.afterSave = function() { 
            window.location.reload();
         };
        dyFObj.openForm('poi',null,{structags:$(this).data("tag") ,type:'forum'},null,dynFormCostumForum)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                    ajaxPost(
                        null,
                        urlToSend,
                        null,
                        function(data){
                            if ( data && data.result ) {
                                toastr.info("élément effacé");
                                $("#"+type+id).remove();
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                        },
                        null,
                        "json"
                    );
                }
            });

    });
});

</script>