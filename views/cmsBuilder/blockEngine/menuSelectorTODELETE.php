 <div class="row panel-display-cms hiddenPreview">
      <div id="sp-main-cms" style="height:100%;background-color: #ffffffdb;"> 
          <div style="height: 70px;display: flex;justify-content: space-around;padding-left: 24px;background-color: #ffffff85;">
            <div class="options-area" style="width: 10px;padding: 10px;">
              <a href="javascript:void(0)" class="close-list-cms">&times;</a>
            </div>
            <div class="searchBar-filters" style="width: 75%;margin-top: 10px;">
              <input id="searchInput" onkeyup="cmsBuilder.block.menuSelector.search()" type="text" class="form-control text-center main-search-bar search-bar" data-field="text" placeholder="Que recherchez-vous ?">
              <span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="text">
                <i class="fa fa-arrow-circle-right"></i>
              </span>
            </div>
            <div class="options-area" style="width: 5%;">
              <a href="javascript:;" class="createCMS btn btn-success" title="Créer un bloc"><i class="fa fa-plus padding-right-10"></i></a>
            </div>
          </div>
          <div class="floop" style="height:94%;background-color: #7b7b7b12;text-shadow: 0px 0px 1px #000f10;">
            <nav class="tpl-engine-right">
              <ul id="list-cms-disponible"></ul>
            </nav>
          </div>
        </div>
    </div>