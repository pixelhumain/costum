<?php $assetsUrl = Yii::app()->getModule(Costum::MODULE)->getAssetsUrl();  ?>
<style type="text/css">
	.underconst-main {
        background-image: url("<?= $assetsUrl ?>/cmsBuilder/img/page/underconst.gif");
        height: 640px;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: right;
        background-color: #fcfcff;

    }
    .underconst-left{
        align-content: center;
        padding-left: 10%;
    }
    .underconst-info{        
        background-color: #fcfcff;
        border-radius: 10px;
        padding: 15px;
    }
    .underconst-back-home{        
        box-shadow: 0px 1px 3px 0px #b4b4b4;            
    }

</style>
<div class="sp-cms-100 underconst-main">
    <div class="sp-cms-50 underconst-left">
        <div class="underconst-info">
            <div class="sp-cms-100">
                <span style="font-family: Homestead-Display;font-size: 40px;color: #92bf34;">Page en construction</span>
            </div>
            <div class="sp-cms-100" style="font-family: 'Marianne-Regular';font-size: 17px;text-align: justify;">Nous sommes ravis de vous accueillir sur notre site web. Actuellement, nous sommes en train de travailler dur pour vous offrir une expériance en ligne exceptionnelle. Notre équipe dévouée met tout en oeuvre pour créer un site web qui répondra à vos besoins et attentes.</div>
        </div>        
        <div class="sp-cms-100" style="padding-top: 50px;padding-left: 15px;">
            <a href="#welcome" class="lbh btn underconst-back-home">Retour à la page d'accueil</a>
        </div>
    </div>

</div>
    