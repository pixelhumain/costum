<?php 

    use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;
    
    $base_url = Yii::app()->getBaseUrl(true);
    $host = @$_GET["host"];

    $sw_scope = str_contains($base_url, $host) ? "./":"/";

    $costum["langCostumActive"] = Costum::setLanguageActiveInCostum($costum);

?>
<script>

    //ajouter mylog ici pour n'est pas attendre co.js se charge pour le pouvoir utiliser
    var mylog = (function () {
        return {
            log: function() {
            if(debug){
                var args = Array.prototype.slice.call(arguments);
                console.log.apply(console, args);
                }
            },
            warn: function() {
                if( debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.warn.apply(console, args);
                }
            },
            debug: function() {
                if(debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.debug.apply(console, args);
                }
            },
            info: function() {
                if(debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.info.apply(console, args);
                }
            },
            dir: function() {
                if(debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.warn.apply(console, args);
                }
            },
            error: function() {
                if(debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.error.apply(console, args);
                }
            }
        }
    }());
</script>

<script>
    costum["langCostumActive"] = "<?= $costum["langCostumActive"] ?>";
    var cmsChildren = {},
        sectionDyf = {},
        tplCtx = {},
        callByName = {},
        tplKey = '<?= @$tplKey ?>',
        isInterfaceAdmin = false,
        isMember = false,
        isSuperAdmin = false,
        isSuperAdminCms = false,
        page = "<?=$page?>",
        divSelection,
        current_full_Url = window.location.href,
        hideOnPreview = '.editSectionBtns,.editBtn,.editThisBtn,.editQuestion,.addQuestion,.command-block-pos,.previewTpl,.openListTpls,.content-btn-action,.hiddenPreview';
        var intervalToCheckLink = setInterval(function(){
            if(typeof convAndCheckLink == 'function'){
                clearInterval(intervalToCheckLink)                
                convAndCheckLink(".sp-text")
            }   
            //Remove all element useless in preview mode
            if (costum.editMode != true) {
              $(hideOnPreview).remove();
          }
        }, 100)

    <?php if(Authorisation::isInterfaceAdmin()){ ?>
        isInterfaceAdmin = true;
    <?php }
      if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])){  ?>
        isSuperAdmin = true;
        isSuperAdminCms = true;
    <?php }     
      if (Authorisation::isUserSuperAdminCms()){  ?>
        isSuperAdminCms = true;
    <?php } 
    if(Authorisation::isElementMember($costum["contextId"],$costum["contextType"],Yii::app()->session["userId"])){?>
        isMember = true;
    <?php } ?>

    $(function(){
        cmsBuilder.config={
            tpl : {
                activeId : "",
                description : "<?php echo isset($insideTplInUse["description"]) ? $insideTplInUse["description"] : "" ?>",
                name : "<?php echo isset($insideTplInUse["name"]) ? $insideTplInUse["name"] : "" ?>",
                // category : "<?php echo isset($insideTplInUse["category"]) ? htmlspecialchars_decode($insideTplInUse["category"]) : "" ?>",
                // listCategories : {
                //     "Art & Culture"    :  tradCms["Art & Culture"],
                //     "Animals & Pets" :  tradCms["Animals & Pets"],
                //     "Design & Photography"  : tradCms["Design & Photography"],
                //     "Electronics"  : tradCms["Electronics"],
                //     "Education & Books"  :tradCms["Education & Books"],
                //     "Business & Services"  :tradCms["Business & Services"],
                //     "Cars & Motorcycles"  : tradCms["Cars & Motorcycles"],
                //     "Sports,_Outdoors & Travel"  :tradCms["Sports, Outdoors & Travel"],
                //     "Fashion & Beauty"  :tradCms["Fashion & Beauty"],
                //     "Computers & Internet"  :tradCms["Computers & Internet"],
                //     "Food & Restaurant"  :tradCms["Food & Restaurant"],
                //     "Home & Family"  :tradCms["Home & Family"],
                //     "Entertainment,_Games & Nightlife"  :tradCms["Entertainment, Games & Nightlife"],
                //     "Holidays,_Gifts & Flowers"  :tradCms["Holidays, Gifts & Flowers"],
                //     "Society & People"  :tradCms["Society & People"],
                //     "Medical_(Healthcare)"  :tradCms["Medical (Healthcare)"],
                //     "Other"  :tradCms["Other"]
                // }
            },
            context : {
                type : costum.contextType ,
                id :  costum.contextId ,
                slug :  costum.contextSlug 
            },
            page : "<?=$page?>",
            block:{
            cmsInUseId : <?php echo json_encode(@$cmsInUseId) ?>,
            newCmsId : <?php echo json_encode(@$newCmsId) ?>
            }
        };

        setTimeout(function(){     
            $("#sortablee").removeClass("row")
        },10)
    })
</script>
<div class="swipe-navigation-page swipe-prev">
		<i class="fa fa-arrow-left">Prev</i>
</div>
<div class="swipe-navigation-page swipe-next">
        Next<i class="fa fa-arrow-right"></i>
</div>


<button id="scrollTopBtn">
        <i class="fa fa-angle-double-up" style="font-size:50px"></i>
</button>
<div id="costum-image-viewer">
    <span class="co-close">&times;</span>
    <button class="co-prev">&lt;</button>
    <button class="co-next">&gt;</button>
    <div style="display: flex; justify-content: center; align-items: center; height: inherit;">
        <img class="cp-modal-content" id="id-full-image">
    </div>
</div>

<div id="all-block-container" class="col-xs-12 no-padding">
    <?php
    $i = 0;
    if ($page && !Authorisation::isInterfaceAdmin() && count($cmsList)>0 && count($cmsList) < 2) {
        $firstSection = $cmsList[array_keys($cmsList)[0]];
        if (empty($firstSection["blockChildren"])) {
            echo $this->renderPartial("costum.views.cmsBuilder.blockEngine.partials.underConstruct");
        }
    }
    
    foreach ($cmsList as $e => $v) {
        // ??????
        if(isset($v["dontRender"]) && $v["dontRender"] == "true"){
          continue;
        }
        $data = array(
          "v" => $v,
          "el" => [],
          "page" => $page,
          "cmsList" => $cmsList,
          "costumData" => $costum,
          "canEdit"    => @$costum["editMode"]
        );
        // if ((isset($v["advanced"]) && !empty($v["advanced"]))) {
        //     $internalLinkClass = (isset($v["advanced"]["typeUrl"]) && $v["advanced"]["typeUrl"] === "internalLink") || (!isset($v["advanced"]["typeUrl"])) ? " lbh" : "";
        //     $targetBlank = (isset($v["advanced"]["targetBlank"]) && $v["advanced"]["targetBlank"]) ? 'target="_blank"' : '';
        // }

        $typeUrl =  $blockCms["advanced"]["typeUrl"] ??  "internalLink";
        $link =  $blockCms["advanced"]["link"] ??  "";
        $targetBlank = $blockCms["advanced"]["targetBlank"] ?? false ;

        if (isset($v["path"])) {
            $path = $v["path"];
            $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
            $pathExplode = explode('.', $v["path"]);
            $count = count($pathExplode);
            $content = isset($v['content']) ? $v['content'] : [];
            $kunik = $pathExplode[$count - 1] . $v["_id"];
            $blockKey = (string)$v["_id"];
            $blockName = (string)@$v["name"];
            $params = [
                "cmsList"          => $cmsList,
                "blockKey"         => $blockKey,
                "blockCms"         => $v,
                "page"             => $page,
                "canEdit"          => @$costum["editMode"],
                "type"             => $path,
                "kunik"            => $kunik,
                "content"          => $content,
                'blockName'        => $blockName,
                "costum"           => $costum,
                "el"               => [],
                'range'     => $i,
                "defaultImg" => Yii::app()->controller->module->assetsUrl . "/images/thumbnail-default.jpg",
                "clienturi" => @$_POST["clienturi"]
            ];

            $width = [
                "modeLg" =>  isset($v["modeLg"]) ? $v["modeLg"] : "12",
                "modeMd" =>  isset($v["modeMd"]) ? $v["modeMd"] : "12",
                "modeSm" =>  isset($v["modeSm"]) ? $v["modeSm"] : "12",
                "modeXs" =>  isset($v["modeXs"]) ? $v["modeXs"] : "12"
            ];

            if (is_file($this->getViewFile("costum.views." . $path, $params))) {
                $dataAnchorTarget = (!empty($v["advanced"]["anchorTarget"])) ? " data-anchor-target='" . substr($v["advanced"]["anchorTarget"], 1) . "' " : ""; ?>
                <div 
                    id="<?= $v["_id"] . "-" . $i ?>" 
                    class="cmsbuilder-block <?= ($v["path"]==="tpls.blockCms.superCms.container")?"cmsbuilder-block-droppable":"" ?> sortable-<?= $kunik ?> custom-block-cms col-xs-12 no-padding col-lg-<?= $width["modeLg"] ?> col-md-<?= $width["modeMd"] ?> col-sm-<?= $width["modeSm"] ?> col-xs-<?= $width["modeXs"] ?> block-container-<?= $kunik ?>"  
                    data-blocktype="section"
                    data-sptarget="background" 
                    data-path="<?= $v["path"] ?>"  
                    data-id="<?= $v["_id"]?>" 
                    data-test="<?= $v["_id"]?>" 
                    <?php echo  $dataAnchorTarget; ?>
                    data-kunik="<?= $kunik ?>" 
                    data-name="<?= ($v["path"]==="tpls.blockCms.superCms.container")?"Section":$blockName ?>"
                  >
                  
                    <div class="block-container-html">
                        <?php 
                            echo BlockCmsWidget::widget([
                                "path" => @$v["path"],
                                "notFoundViewPath" => "costum.views." . @$v["path"],
                                "config" => $params
                            ]);
                        ?>
                    </div>
                </div>
<?php 
            } else { 
?>
                <div class="col-xs-12 text-center" id="<?php echo (string)$v["_id"]; ?>">
                  <?php echo $this->renderPartial("costum.views.tpls.blockNotFound", ["blockKey" => $blockKey]) ?>
                </div>
<?php       }
        }
        $i++;
    }
?>
</div>

<script>

    
    if(window.cmsConstructor){
            var hash = location.hash.replace("#","").match(/^[\w\d\-_]{1,}/);
            page = hash && typeof hash[0] === 'string' ? hash[0] : "welcome";
            cmsConstructor.init();
            cmsConstructor.cmsList = <?php echo json_encode(array_keys(@$cmsList)) ?>;
            if (!notEmpty(cmsConstructor.cmsList) && typeof costum.app["#"+page].linked === "undefined") {
                if(typeof costum.app["#"+page].note !== "undefined")
                    costumizer.template.actions.use("6670480eb2f3d45415475c4c", "templates","")
                else{
                    costumizer.getCategoryUsedByTemplate(function () {                        
                        var params = {
                            space:"template",
                            showSaveButton:false,
                            key:"page"
                        }
                        costumizer.template.views.initAjax(params)
                        var suggestTemplate = `<div style="width:100%" class="padding-20"><center style="width:100%"><p>Bienvenue sur votre nouvelle page! Explorez notre collection de superbes templates de page et donnez vie à votre costum en un clic.<br>
                        Choisissez sur la liste suivant celui qui correspond à votre style et commencez à créer dès maintenant!</p><p><button class="btn btn-default" onclick="cmsConstructor.builder.actions.insertBlock('in',null,true);$('#btn-hide-modal-blockcms').click()">Je n'a pas besoin de template</button></p></center></div>`
                        $(".information").html(suggestTemplate)
                        $(".information").show()
                        $("#btn-hide-modal-blockcms").hide()
                    });           
                }
            }else if(typeof costum.app["#"+page] != "undefined" && typeof costum.app["#"+page].linked == "undefined" && typeof Object.keys(cmsConstructor.sp_params).find(id => (cmsConstructor.cmsList).includes(id)) != "undefined"){                
                const parentObject = cmsConstructor.sp_params[Object.keys(cmsConstructor.sp_params).find(id => (cmsConstructor.cmsList).includes(id))].parent;
                const parentId = Object.keys(parentObject)[0];
                if(parentId != costum.contextId){                          
                    costumizer.promptToReload()
                }
            }
            // SokeckIO on page ready
            // Reveal element focused
            if (typeof costum.members    != "undefined" && typeof costum.members.focused != "undefined") {
                $.each(costum.members.focused, function(key, value) {  
                    var params = {
                        userId:key,
                        elementTarget:value.elementTarget,
                    }          
                    if (typeof value.page != "undefined") {
                        params["page"] = value.page
                    }       
                    costumSocket.socketListener.elementFocused(params);   
                }) 
            }
            // Check if page alias
            costumizer.checkCostumAlias()
            $(".cmsbuilder-left-content,.cmsbuilder-right-content,.empty-section-blocks").removeClass("hidden")
            $(".editMenuStandalone").removeClass("hidden")
    }else{
        $(".cmsbuilder-left-content,.cmsbuilder-right-content,.empty-section-blocks").addClass("hidden")
    }


    $(document).ajaxStop(function () {
        if($("#all-block-container").length == 0){
            var costumInfo = ""
            costumInfo = '<i class="fa fa-lock padding-right-20" aria-hidden="true"></i> Page non CMS. Edition '+trad.disabled
            $(".cmsbuilder-left-content,.cmsbuilder-right-content,.empty-section-blocks").addClass("hidden")
            $(".costum-info").html(costumInfo).removeClass("hidden")
            $(".editMenuStandalone").addClass("hidden")
        }
    })

    $(document).ready(function () {
            window.onscroll = function() {
                if ( jsonHelper.getValueByPath(costum.app, "#" + page + ".btnScrollToTop") === true && (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100 )) {
                    $("#scrollTopBtn").css("display","flex")
                } else {
                    $("#scrollTopBtn").css("display","none")
                }
            };

            $("#scrollTopBtn").off("click").on("click", function(e) {
                e.stopPropagation();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            });

            if ( typeof jsonHelper.getValueByPath(costum, "htmlConstruct.header.menuTop.left.buttonList.xsMenu.buttonList.app.buttonList") !== "undefined" && jsonHelper.getValueByPath(costum.costumOptions, "slidePageMobile") === true ) {
                var x_start, x_end, max_drop = 120;
                var indexToLoad = null;
                var keyPage = Object.keys(costum.htmlConstruct.header.menuTop.left.buttonList.xsMenu.buttonList.app.buttonList).filter(key => key.startsWith('#'));
                keyPage.unshift('#welcome');
                var location =  "#" + page ;
                var indexActualPage = keyPage.indexOf(location);
                // Fonction pour déterminer si le balayage est suffisant pour déclencher un changement de page
                function handleSwipe(diff) {
                    return Math.abs(diff) >= max_drop;
                }
    
                // Désactiver d'anciens gestionnaires d'événements pour éviter les doublons
    
                $("#all-block-container").off("touchstart touchmove")
                .on("touchstart", function(e) {
                    e.stopImmediatePropagation();
                    var touch;  
                    touch = e.originalEvent.touches[0];
                    x_start = touch.clientX; 
                    mylog.log("eto start",keyPage,indexActualPage)
                })
                .on("touchmove", function(e) {
                    e.stopImmediatePropagation();
                    var touch = e.originalEvent.touches[0];
                    x_end = touch.clientX;  // Enregistrer le point actuel du toucher
                    diff = Math.abs(x_end - x_start);  // Calculer la différence
                    // Déterminer la direction du balayage et mettre à jour l'interface utilisateur
                    var navigation_dom , room;
                    $(".swipe-navigation-page").removeClass("active");
                    if ( indexActualPage < keyPage.length - 1 && x_end < x_start) {  // Balayage vers la gauche (page suivante)
                        room = $(window).outerWidth(true) - Math.min(diff, max_drop) + 40;
                        navigation_dom = $(".swipe-navigation-page.swipe-next");
                        indexToLoad = indexActualPage + 1;
                    } else if (indexActualPage > 0 && x_end > x_start) {  // Balayage vers la droite (page précédente)
                        room = Math.min(diff, max_drop) - 40;
                        navigation_dom = $(".swipe-navigation-page.swipe-prev");
                        indexToLoad = indexActualPage - 1;
                    }
    
                    // Si un élément de navigation est trouvé, ajuster sa position
                    if (navigation_dom) {
                        navigation_dom.css("left", room);
                        // navigation_dom.find("i").css("font-size","20px");
                        if (handleSwipe(diff)) {
                            navigation_dom.addClass("active");  // Activer la navigation si le balayage est suffisant
                        }
                    }
                })
                .off("touchend").on("touchend", function(e) {
                    e.stopImmediatePropagation();
                    diff = Math.abs(x_end - x_start);
    
                    // Si le balayage est suffisant, charger la nouvelle page
                    if (indexToLoad !== null && typeof keyPage[indexToLoad] !== 'undefined' && handleSwipe(diff)) {
                        // Récupérer l'URL sans le fragment (partie après #)
                        var baseUrl = window.location.href.split('#')[0];

                        var fragment = keyPage[indexToLoad];

                        if (!window.location.href.includes("#")) {
                            urlCtrl.loadByHash(fragment);
                        } else {
                            var link = baseUrl + fragment;
    
                            var virtualLink = document.createElement('a');
        
                            virtualLink.href = link;
        
                            virtualLink.style.display = 'none';
        
                            document.body.appendChild(virtualLink);
        
                            virtualLink.click();
        
                            virtualLink.remove();
                        }
    
                    } else {
                        console.error('Index non valide ou keyPage[indexToLoad] est indéfini.');
                    }
    
                    // Réinitialiser les éléments de navigation
                    $(".swipe-navigation-page.swipe-next").animate({
                        left: $(window).outerWidth(true) + 40
                    }, 200, function() {
                        $(this).css("left", "").removeClass("active");
                    });
    
                    $(".swipe-navigation-page.swipe-prev").animate({
                        left: -40
                    }, 200, function() {
                        $(this).css("left", "").removeClass("active");
                    });
                });
            }

            cmsEngine.particlePage(page);


            if(!costum.editMode) {

                // $(".costum-img-preview").click(function(){
                //     $("#id-full-image").attr("src", $(this).attr("src"));
                //     $('#costum-image-viewer').show();
                // });
        
                // $("#costum-image-viewer .co-close").click(function(){
                //     $('#costum-image-viewer').hide();
                // });
                let images = [];
                let currentIndex = 0;

                $(".costum-img-preview").each(function () {
                    mylog.log('tonga ato', $(this).attr("src"));
                    images.push($(this).attr("src"));
                });


                $(".costum-img-preview").click(function () {
                    currentIndex = images.indexOf($(this).attr("src"));
                    showImage(currentIndex);
                    $('#costum-image-viewer').show();
                });

                $("#costum-image-viewer .co-close").click(function () {
                    $('#costum-image-viewer').hide();
                });

                // Afficher l'image à un index donné
                function showImage(index) {
                    if (index >= 0 && index < images.length) {
                        $("#id-full-image").attr("src", images[index]);
                    }
                }

                $("#costum-image-viewer .co-prev").click(function () {
                    navigateLeft();
                });

                $("#costum-image-viewer .co-next").click(function () {
                    navigateRight();
                });

                
                $(document).keyup(function (e) {
                    if ($('#costum-image-viewer').is(":visible")) {
                        switch (e.which) {
                            case 37:
                                navigateLeft();
                                break;
                            case 39:
                                navigateRight();
                                break;
                            case 27:
                                $('#costum-image-viewer').hide();
                                break;
                        }
                    }
                });

                function navigateLeft() {
                    currentIndex = (currentIndex > 0) ? currentIndex - 1 : images.length - 1;
                    showImage(currentIndex);
                }

                function navigateRight() {
                    currentIndex = (currentIndex < images.length - 1) ? currentIndex + 1 : 0;
                    showImage(currentIndex);
                }
            }
    });

</script>