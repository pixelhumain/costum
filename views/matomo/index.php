<!-- Start Matomo Code -->
<script type="text/javascript">
    var _paq = _paq || [];
    var loadTime = new Date().getTime();
    _paq.push(['trackPageView']);
    _paq.push(['enableHeartBeatTimer', 30]);
    <?php
    $userId = Yii::app()->session["userId"];
    if (isset($userId)) {
        echo sprintf("_paq.push(['setUserId', '%s']);", $userId);
    } else {
        echo sprintf("_paq.push(['resetUserId']);_paq.push(['appendToTrackingUrl', 'new_visit=1']); _paq.push(['trackPageView']); _paq.push(['appendToTrackingUrl', '']);");
    }
    ?>
    _paq.push(['enableLinkTracking']);
    (function() {
        var u = "<?= $matomoUrl ?>";
        _paq.push(['setTrackerUrl', u + 'matomo.php']);
        _paq.push(['setSiteId', '<?= $siteId ?>']);
        var d = document,
            g = d.createElement('script'),
            s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.async = true;
        g.defer = true;
        g.src = u + 'matomo.js';
        s.parentNode.insertBefore(g, s);
    })();
    // Tracking des navigations
    window.addEventListener('popstate', function() {
        const restOfUrl = window.location.href.split('?')[0].split('#')[0];
        const url = restOfUrl + window.location.search + window.location.hash;
        if (window.location.hash.includes('?')) {
            const params = window.location.hash.split('?')[1];
            const searchParams = new URLSearchParams(params);
            const searchParamsArray = Array.from(searchParams.entries());
            if (searchParamsArray.length > 0) {
                const keywords = [];
                searchParamsArray.forEach(param => {
                    keywords.push(param[1]);
                });
                _paq.push(['trackSiteSearch', keywords, false, false, false]);
            }
        }
        _paq.push(['setCustomUrl', url]);
        _paq.push(['setDocumentTitle', window.document.title]);
        _paq.push(['trackPageView']);
    });
    // Tracking evenement des formulaires
    document.querySelectorAll('form').forEach(function(form) {
        form.addEventListener('submit', function(event) {
            event.preventDefault();
            const formId = form.getAttribute('id');
            const formName = form.getAttribute('name');
            _paq.push(['trackEvent', 'Form', 'Submit', formId || formName]);
            form.submit();
        });
    });
    // Tracking des clicks
    document.addEventListener('click', function(event) {
        const element = event.target;
        const elementTagName = element.tagName.toLowerCase();
        const elementText = element.textContent.trim();
        if (elementTagName === 'a') {
            if (element.href.match(/^https?:\/\//i)) {
                // Lien externe
                _paq.push(['trackEvent', 'Click', 'Lien externe', elementText]);
            } else {
                // Lien interne
                _paq.push(['trackEvent', 'Click', 'Lien interne', elementText]);
            }
        } else if (elementTagName === 'button') {
            _paq.push(['trackEvent', 'Click', 'Bouton', elementText]);
        } else if (elementTagName === 'img') {
            _paq.push(['trackEvent', 'Click', 'Image', element.alt || element.src]);
        } else if (element.href && /\.(pdf|doc|docx|xls|xlsx|csv|zip|rar)$/i.test(element.href)) {
            // Téléchargement de fichier
            _paq.push(['trackEvent', 'Téléchargement', 'Fichier', elementText]);
        }
    });
</script>

<!-- End Matomo Code -->